<?php

require_once __DIR__ . '/../../vendor/autoload.php';

class Connection {

	public $isPolling;

	public $response;

	public function __construct($response, $isPolling = false) {
		$this->response = $response;
		$this->isPolling = $isPolling;
	}

}

class RealTimeServer {

	private $databaseConfig;
	private $appConfig;

	private $pdo;
	private $stat;

	private $usersConnections = [];

	public function __construct() {
		require_once __DIR__ . '/../../bootstrap/start.php'; // For initialization
		$this->databaseConfig = require __DIR__ . '/../config/database.php';
		$this->appConfig = require __DIR__ . '/../config/app.php';

		$dbHost = $this->databaseConfig['connections']['mysql']['host'];
		$dbName = $this->databaseConfig['connections']['mysql']['database'];
		$dbUser = $this->databaseConfig['connections']['mysql']['username'];
		$dbPassword = $this->databaseConfig['connections']['mysql']['password'];

		$this->pdo = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUser, $dbPassword);
		$this->stat = $this->pdo->prepare('SELECT * FROM sessions WHERE id = ?');
	}

	public function run() {
		$loop = React\EventLoop\Factory::create();

		$context = new React\ZMQ\Context($loop);

		$pull = $context->getSocket(ZMQ::SOCKET_PULL);
		$pull->bind('tcp://127.0.0.1:5555');

		$pull->on('error', function ($e) {
			var_dump($e->getMessage());
		});

		$pull->on('message', function ($msg) {
			$this->handleMessage($msg);
		});

		$socket = new React\Socket\Server($loop);
		$http = new React\Http\Server($socket, $loop);

		$http->on('request', function($request, $response) {
			$this->handleRequest($request, $response);
		});

		$port = 1337;
		$host = '0.0.0.0';
		$socket->listen($port, $host);
		echo "RealTimeServer running at http://$host:$port/\n";

		$loop->addPeriodicTimer(10, function() {
			$this->sendHeartbeat();
		});

		$loop->run();
	}

	private function handleRequest(React\Http\Request $request, React\Http\Response $response) {
		// NOT EVENTS REQUEST
		if ($request->getPath() === '/s') {
			$isPolling = false;
		} else if ($request->getPath() === '/p') {
			$isPolling = true;
		} else {
			$response->writeHead(404, ['X-Powered-By' => '']);
			$response->write('INVALID REQUEST.');
			$response->end();
			return;
		}

		$headers = $request->getHeaders();
		print_r($headers);

		// NO COOKIE IN REQUEST
		if (! isset($headers['Cookie'])) {
			$response->writeHead(404, ['X-Powered-By' => '']);
			$response->write('NO COOKIE FOUND.');
			$response->end();
			return;
		}

		$cookies = $this->parseCookie($headers['Cookie']);
		$sessionId = '';
		foreach ($cookies as $cookie) {
			if ($cookie['name'] === 'cw_session') {
				$sessionId = $cookie['value'];
				break;
			}
		}

		// NO SESSION_ID IN COOKIE
		if (! $sessionId) {
			$response->writeHead(404, ['X-Powered-By' => '']);
			$response->write('NO SESSION_ID IN COOKIE.');
			$response->end();
			return;
		}

		$decodedSessionId = base64_decode($sessionId);
		$payLoad = json_decode($decodedSessionId);
		if (! $payLoad) {
			$pos = strrpos($decodedSessionId, '}', -1);
			$decodedSessionId = substr($decodedSessionId, 0, $pos + 1);
			$payLoad = json_decode($decodedSessionId);
		}

		if (! $payLoad) {
			$response->writeHead(404, ['X-Powered-By' => 'RTS']);
			$response->write('INVALID COOKIE.');
			$response->end();
			return;
		}

		$value = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->appConfig['key'], base64_decode($payLoad->value), MCRYPT_MODE_CBC, base64_decode($payLoad->iv));
		$pad = ord($value[($len = strlen($value)) - 1]);
		$padding = substr($value, 0, strlen($value) - $pad);
		$realSessionId = unserialize($padding);
		$session = $this->query($realSessionId);

		// NO SESSION FOUND IN DATABASE
		if (! $session) {
			$response->writeHead(404, ['X-Powered-By' => '']);
			$response->write('INVALID SESSION_ID IN COOKIE.');
			$response->end();
			return;
		}

		// NO USER_ID IN SESSION
		$sessions = unserialize(base64_decode($session));
		if (! isset($sessions['user_id'])) {
			$response->writeHead(404, ['X-Powered-By' => '']);
			$response->write('NOT LOGIN.');
			$response->end();
			return;
		}

		$userId = $sessions['user_id'];
		echo "USERID: $userId\n";

		$response->writeHead(200, [
			'Access-Control-Allow-Origin' => $this->appConfig['url'],
			'Access-Control-Allow-Credentials' => 'true',
			'X-Powered-By' => 'RTS'
		]);

		for ($i = 0; $i < 1024; $i++) {
			$response->write(' ');
		}

		if (! isset($this->usersConnections[$userId])) {
			$userConnections = new SplObjectStorage();
			$this->usersConnections[$userId] = $userConnections;
		}

		$userConnections = $this->usersConnections[$userId];
		$connection = new Connection($response, $isPolling);
		$userConnections->attach($connection);

		$response->on('close', function() use ($userConnections, $connection) {
			$userConnections->detach($connection);
		});
	}

	private function handleMessage($msg) {
		echo "RECEIVED: $msg\n";
		$message = json_decode($msg);
		if (! isset($message->type)) {
			return;
		}
		switch ($message->type) {
			case 'notification_count':
				$this->sendMessage($message->user_id, $msg);
				break;
			case '':
				break;
			default:
				break;
		}
	}

	private function parseCookie($strHeaders) {
		$result = array();

		if (!empty($strHeaders)) {
			$aHeaders = explode("\n", trim($strHeaders));

			foreach ($aHeaders as $line) {
				$aTmp = array();

				$aPairs = explode(';', $line);
				foreach ($aPairs as $pair) {
					$aKeyValues = explode('=', trim($pair), 2);
					if (count($aKeyValues) == 2) {
						switch ($aKeyValues[0]) {
							case 'path':
							case 'domain':
								$aTmp[trim($aKeyValues[0])] = urldecode(trim($aKeyValues[1]));
								break;
							case 'expires':
								$aTmp[trim($aKeyValues[0])] = strtotime(urldecode(trim($aKeyValues[1])));
								break;
							default:
								$aTmp['name'] = trim($aKeyValues[0]);
								$aTmp['value'] = trim($aKeyValues[1]);
								break;
						}
					}
				}

				$result[] = $aTmp;
			}
		}

		return $result;
	}

	private function query($id) {
		$this->stat->execute([$id]);
		return $this->stat->fetch()[1];
	}

	private function sendMessage($userId, $msg) {
		/* @var $userConnections SplObjectStorage */
		/* @var $response React\Http\Response */
		$userConnections = $this->usersConnections[$userId];
		$needToDetach = [];
		foreach ($userConnections as $connection) {
			$response = $connection->response;
			$response->write($msg);
			if ($connection->isPolling) {
				$response->end();
				$needToDetach[] = $connection;
			}
		}

		foreach ($needToDetach as $connection) {
			$userConnections->detach($connection);
		}
	}

	private function sendHeartbeat() {
		/* @var $userConnections SplObjectStorage */
		/* @var $response React\Http\Response */
		$userCount = 0;
		$connectionsCount = 0;
		$time = date('H:i:s');
		foreach ($this->usersConnections as $userConnections) {
			if ($userConnections->count() > 0) {
				$userCount++;
			}

			$needToDetach = [];
			foreach ($userConnections as $connection) {
				$connectionsCount++;
				$response = $connection->response;
				$response->write('CURRENT_TIME: ' . $time . '<br>');
				if ($connection->isPolling) {
					$response->end();
					$needToDetach[] = $connection;
				}
			}

			foreach ($needToDetach as $connection) {
				$userConnections->detach($connection);
			}
		}

		echo "-----\nUSER_COUNT: $userCount\nCONNECTION_COUNT: $connectionsCount\n";
	}

}

// Start server
$server = new RealTimeServer();
$server->run();