<?php namespace App\Modules;
use App\Models\ExpertComment;

/**
 * 专家点评模块
 * Class ExpertCommentModule
 * @package App\Modules
 */
class ExpertCommentModule extends BaseModule {

	public static function getExpertCommentById($id){
		return ExpertComment::find($id);
	}

	/**
	 * 发布专家点评
	 * @param int $userId
	 * @param int $targetType
	 * @param int $targetId
	 * @param string $content
	 * @param int $score
	 *
	 * @return int 专家点评ID
	 */
	public static function publishExpertComment($userId, $targetType, $targetId, $score, $content, $targetSubId = 0) {

		$model = new ExpertComment();
		$model->user_id = $userId;
		$model->target_type = $targetType;
		$model->target_id = $targetId;
		$model->content = $content;
		$model->score = $score;
		$model->target_sub_id = $targetSubId;
		$model->create_time = time();
		$model->save();

		
		\Event::fire('expert-comment.publish', array($targetType,$targetId, $targetSubId,$score,$userId));

		return $model->id;
	}

	/**
	 * 最新专家点评
	 * @param int $targetType
	 * @param int $targetId
	 * @param int $offset
	 * @param int $limit
	 *
	 * @return mixed 专家点评信息
	 */
	public static function getExpertComments($targetType, $targetId, $targetSubId = 0 ,$offset = 0, $limit = 10) {
		$model = ExpertComment::whereRaw('target_type = ? and target_id = ?',array($targetType,$targetId));
		if($targetSubId){
			$model = $model->where('target_sub_id',$targetSubId);
		}
		return $model->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
	}
	
	/**
	 * 获取最新专家点评
	 * @param int $offset
	 * @param int $limit
	 * 
	 * @return array
	 */
	public static function getTopExpertComments($offset = 0, $limit = 3) {
	    return ExpertComment::where('user_id','>',0)->offset($offset)->limit($limit)->orderBy('id','desc')->get();
	}
	
	/**
	 * 判断专家是否已点评
	 * @param unknown $targetType
	 * @param unknown $userId
	 * @param unknown $targetId
	 * @param number $targetSubId
	 */
	public static function hasCommented($targetType, $userId, $targetId, $targetSubId = 0) {
	    $hasComment = ExpertComment::where('target_type', $targetType)->where('target_id', $targetId)->where('user_id', $userId);
	    if($targetSubId) {
	        $hasComment->where('target_sub_id', $targetSubId);
	    }
	    
	    return $hasComment->count() ? true : false;
	}

}