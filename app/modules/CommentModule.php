<?php namespace App\Modules;
use App\Models\Comment;

/**
 * 交流区(评论)模块
 * Class CommentModule
 * @package App\Modules
 */
class CommentModule extends BaseModule {

	public static function getCommentById($id){
		return Comment::find($id);
	}
	/**
	 * 发表评论
	 * @param int $userId
	 * @param string $content
	 * @param int $targetType
	 * @param int $targetId
	 * @return int 评论ID
	 */
	public static function publishComment($userId, $targetType, $targetId, $content) {

		$model = new Comment();
		$model->user_id = $userId;
		$model->target_type = $targetType;
		$model->target_id = $targetId;
		$model->content = $content;
		$model->create_time = time();
		$model->save();
		\Event::fire('comment.publish', array($userId, $targetType,$targetId));

		return $model->id;

	}

	/**
	 * 查询评论
	 * @param int $targetType
	 * @param int $targetId
	 * @param int $offset
	 * @param int $limit
	 *
	 * @return 评论信息
	 */
	public static function getComments($targetType, $targetId, $offset = 0, $limit = 10){

		return Comment::whereRaw('target_type = ? and target_id = ?',array($targetType,$targetId))->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();

	}
	
	/**
	 * 统计评论数
	 * 
	 * @param int $targetType
	 * @param int $targetId
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getCommentsCount($targetType, $targetId) {
	    return Comment::whereRaw('target_type = ? and target_id = ?',array($targetType,$targetId))->count();
	}

	
	/**
	 * 是否已发放创创币
	 * @param unknown $userId
	 * @param unknown $targetType
	 * @param unknown $targetId
	 * @return boolean
	 */
	public static function isCoined($userId,$targetType,$targetId){
		$flag = Comment::whereRaw('user_id = ? and target_type = ? and target_id =? ',array($userId,$targetType,$targetId))->count();
		return $flag==1?true:false;
	}

}