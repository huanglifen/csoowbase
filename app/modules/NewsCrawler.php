<?php
	set_time_limit(0);
	use App\Common\Utils;
	use App\Modules\NewsModule;

	require_once 'simple_html_dom.php';

	class NewsCrawler
	{

		private static $baseURL = 'http://news.sina.com.cn/area/';
		private static $fileBasePath = 'data/news/area/';

		private static $areaNames = array(
			'bj'  => 10,
			'tj'  => 11,
			'hb'  => 12,
			'sx'  => 13,
			'nm'  => 14,
			'ln'  => 15,
			'jl'  => 16,
			'hlj' => 17,
			'sh'  => 18,
			'js'  => 19,
			'zj'  => 20,
			'ah'  => 21,
			'fj'  => 22,
			'jx'  => 23,
			'sd'  => 24,
			'hen' => 25,
			'hub' => 26,
			'hun' => 27,
			'gd'  => 28,
			'gx'  => 29,
			'han' => 30,
			'cq'  => 31,
			'sc'  => 32,
			'gz'  => 33,
			'yn'  => 34,
			'xz'  => 35,
			'shx' => 36,
			'gs'  => 37,
			'qh'  => 38,
			'nx'  => 39,
			'xj'  => 40,
			'hkm' => 41,
			'hkm' => 42,
			'tw'  => 43,

		);

		private static function getContent($url)
		{
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$ret = curl_exec($ch);
			curl_close($ch);

			return $ret;
		}

		public static function crawlAreaNews()
		{
			foreach (self::$areaNames as $area => $id) {
				$url     = self::$baseURL . $area . '.shtml';
				$content = self::getContent($url);

				Utils::createDirectoryIfNotExist(self::$fileBasePath);
				$handle = fopen(self::$fileBasePath . $area . '.shtml', 'w');
				fwrite($handle, $content);
				fclose($handle);

				self::parseListContent($area);
			}
		}

		public static function parseListContent($area)
		{
			$html = file_get_html(self::$fileBasePath . $area . '.shtml');
			if (in_array($area, array('bj', 'tj'))) {
				$lists = $html->find('li');
			} else {
				$lists = $html->find('span.f15');
			}
			if (is_dir(self::$fileBasePath . $area)) {
				$files = scandir(self::$fileBasePath . $area);
				foreach ($files as $file) {
					if ($file != "." && $file != "..") {
						unlink(self::$fileBasePath . $area . '/' . $file);
					}
				}
			}
			Utils::createDirectoryIfNotExist(self::$fileBasePath . $area);

			$i = 0;
			foreach ($lists as $list) {
				if (in_array($area, array('bj', 'tj'))) {
					$node    = str_get_html($list->innertext);
					$a       = $node->find('a');
					$newsURL = $a[0]->href;
				} else {
					$node    = str_get_html($list->next_sibling());
					$a       = $node->find('a');
					$newsURL = $a[0]->href;
				}

				if (substr($newsURL, -6) != '.shtml') {
					continue;
				}

				$newsContent = self::getContent($newsURL);

				$filename = substr($newsURL, strrpos($newsURL, '/') + 1);
				$file     = $i . '_' . $filename;

				$handle = fopen(self::$fileBasePath . $area . '/' . $file, 'w');
				fwrite($handle, $newsContent);
				fclose($handle);

				$result = self::parseNewsContent($area, $file, $newsURL);

				$i++;
				if ($i == 9) {
					break;
				}
			}
		}

		public static function parseNewsContent($area, $file, $sourceURL)
		{
			$result = NewsModule::searchNewsBySourceUrl($sourceURL);
			if ($result) {
				return false;
			}

			$html = file_get_html(self::$fileBasePath . $area . '/' . $file);
			if (!$html || !is_object($html)) {
				return false;
			}
			$titles = $html->find('h1[id=artibodyTitle]');

			if (isset($titles[0])) {
				$title = $titles[0]->plaintext;


				$contents = $html->find('div[id=artibody]');
				$content  = $contents[0]->innertext;
				$content  = str_replace('<script type="text/javascript" src="http://news.sina.com.cn/js/87/20130816/content/355.js"></script>', '', $content);
				$content  = strip_tags($content, '<p><b>');
				$title    = mb_convert_encoding($title, 'UTF-8', array('UTF-8', 'GB2312', 'GBK'));
				$content  = mb_convert_encoding($content, 'UTF-8', array('UTF-8', 'GB2312', 'GBK'));

				NewsModule::createNews($title, $sourceURL, self::$areaNames[$area]);

				return true;
			}

			return false;
		}

	}