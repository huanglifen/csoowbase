<?php namespace App\Modules;

use App\Modules\ItemModule;

/**
 * 喜欢和赞模块
 */
class LikeModule extends BaseModule {


	/**
	 * 保存喜欢
	 * @param int $userId
	 * @param int $targetType
	 * @param int $targetId
	 *
	 * @return ID
	 */
	public static function saveLike($userId,$targetType,$targetId){

		$id = ItemModule::saveItem($userId,ItemModule::ACTION_TYPE_LIKE,$targetType,$targetId,$title = '');
		
		\Event::fire('like.save', array($userId, $targetType, $targetId));
		
		return $id;

	}


	/**
	 * 是否已经喜欢
	 * @param int $userId
	 * @param int $targetType
	 * @param int $targetId
	 *
	 * @return bool
	 */
	public static function isLiked($userId,$targetType,$targetId){

		 return ItemModule::isExist($userId,ItemModule::ACTION_TYPE_LIKE,$targetType,$targetId);
	}

}