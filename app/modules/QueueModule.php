<?php namespace App\Modules;

/**
 * 队列模块
 *
 * 推送信息到队列，向实时连接发送信息
 *
 * @package App\Modules
 */
class QueueModule extends BaseModule {

	private $loop;
	private $push;

	private static $instance = null;

	public function __construct() {
		$this->loop = \React\EventLoop\Factory::create();
		$context = new \React\ZMQ\Context($this->loop);
		$this->push = $context->getSocket(\ZMQ::SOCKET_PUSH);
		$this->push->connect('tcp://127.0.0.1:5555');
	}

	public static function push($message) {
		if (! static::$instance) {
			static::$instance = new static;
		}
		static::$instance->push->send(json_encode($message));
		static::$instance->loop->run();
	}

}