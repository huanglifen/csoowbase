<?php

namespace App\Modules;

use App\Models\User;
use App\Common\Utils;
use App\Modules\Utils\MailModule;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use App\Models\Region;
use App\Models\Organization;
use App\Models\School;
use App\Models\Contest;

/**
 * 用户模块
 *
 * 处理与用户账号相关事务，如登录、注册、找回密码等，亦提供获取用户信息的接口。
 */
class UserModule extends BaseModule {
	
	// 组ID，0 未激活，1 普通用户 2 专家
	const GROUP_NOACTIVE = 0;
	const GROUP_COMMON_USER = 1;
	const GROUP_EXPERT = 2;
	const GROUP_ADMIN = 10;
	
	/**
	 * 获取用户和专家计数
	 *
	 * @param string $selectType        	
	 * @param number $selectId        	
	 *
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getCount($selectType = '', $selectId = 0, $isExpert = 0) {
		if ($isExpert) {
			if ($selectType) {
				$count = User::where ( $selectType, $selectId )->where ( 'group_id', UserModule::GROUP_EXPERT )->count ();
			} else {
				$count = User::where ( 'group_id', UserModule::GROUP_EXPERT )->count ();
			}
		} else {
			if ($selectType) {
				$count = User::where ( $selectType, $selectId )->count ();
			} else {
				$count = User::count ();
			}
		}
		
		return $count ? $count : 0;
	}
	
	// 官方账号
	public static $officialAccount = array (
			'创意世界',
			'创创',
			'创意世界大赛' 
	);
	public static function listenEvent() {
		\Event::listen ( 'project.publish', function ($userId, $projectId) {
			$user = self::getUserById ( $userId );
			$user->contest_publish_count = ContestModule::getCount ( 'user_id', $userId );
			$user->save ();
		} );
		
		\Event::listen ( 'task.publish', function ($userId, $taskId) {
			$user = self::getUserById ( $userId );
			$user->contest_publish_count = ContestModule::getCount ( 'user_id', $userId );
			$user->save ();
		} );
		
		\Event::listen ( 'hiring.publish', function ($userId, $hiringId) {
			$user = self::getUserById ( $userId );
			$user->contest_publish_count = ContestModule::getCount ( 'user_id', $userId );
			$user->save ();
		} );
		
		\Event::listen ( 'trade.publish', function ($userId, $tradeId) {
			$user = self::getUserById ( $userId );
			$user->trade_publish_count = ExchangeModule::getCount ( 'user_id', $userId );
			$user->save ();
		} );
	}
	
	/**
	 * 用户注册
	 *
	 * @param string $name
	 *        	用户名
	 * @param string $email
	 *        	邮箱
	 * @param string $password
	 *        	密码
	 *        	
	 * @return int 注册的用户Id
	 */
	public static function register($name, $email, $password) {
		$user = new User ();
		
		$user->name = $name;
		$user->cc_no = 0;
		$user->email = $email;
		$user->email_is_verified = 0;
		$user->email_verify_code = '';
		$user->password = self::hash ( $password );
		$user->password_reset_code = '';
		$user->activation_code = '';
		$user->group_id = 0;
		$user->avatar = '';
		
		$user->creative_index = 1;
		$user->coin = 0;
		$user->creative_index_bonus_amount = 0;
		$user->gender = 0;
		$user->birthday = 0;
		$user->introduction = '';
		$user->tags = '';
		$user->mobile_phone_no = '';
		$user->mobile_phone_is_verified = 0;
		$user->mobile_phone_verify_code = '';
		
		$user->nation_id = 1;
		$user->province_id = 0;
		$user->city_id = 0;
		$user->district_id = 0;
		$user->school_id = 0;
		$user->organization_id = 0;
		$user->expert_organization_id = 0;
		$user->expert_duty_id = 0;
		$user->expert_field_id = 0;
		$user->expert_industries = '';
		$user->expert_title = '';
		$user->follower_count = 0;
		$user->contest_publish_count = 0;
		$user->trade_publish_count = 0;
		$user->register_time = time ();
		$user->register_ip = '';
		$user->info = json_encode(array('level'=>1));
		
		$user->save ();
		
		$user->cc_no = $user->id + 100000;
		$user->save ();
		CircleModule::createDefaultCircle ( $user->id );
		CircleModule::followOfficialAccount ( $user->id );
		
		\Event::fire ( 'user.register', [ 
				$user 
		] );
		
		return $user->id;
	}
	
	/**
	 * 用户登录
	 *
	 * @param string $loginName        	
	 * @param string $password        	
	 *
	 * @return User bool User 对象，密码验证失败返回 false
	 */
	public static function login($loginName, $password) {
		$user = self::getUserByLoginName ( $loginName );
		if (! $user) {
			return false;
		}
		// Already checked exist at controller
		if (self::checkHash ( $password, $user->password )) {
			if (! empty ( $user->last_login_time )) {
				if (date ( 'Y-m-d', $user->last_login_time ) == date ( 'Y-m-d' )) {
				} else if (date ( 'Y-m-d', $user->last_login_time ) == date ( 'Y-m-d', strtotime ( '-1 day' ) )) {
					$user->consistent_login_days ++;
					\Event::fire ( 'user.login', array (
							$user->id 
					) );
				} else {
					$user->consistent_login_days = 1;
					\Event::fire ( 'user.login', array (
							$user->id 
					) );
				}
			} else {
				$user->consistent_login_days = 1;
				\Event::fire ( 'user.login', array (
						$user->id 
				) );
			}
			$user->last_login_time = time ();
			$user->save ();
			
			\Session::set ( 'user_id', $user->id );
			
			return $user;
		}
		
		return false;
	}
	
	/**
	 * 激活用户
	 *
	 * @param int $userId        	
	 */
	public static function activateAccount($userId) {
		$user = User::find ( $userId );
		if (! $user) {
			return false;
		}
		$user->activation_code = self::generateCode ();
		$user->save ();
		
		$activateUrl = URL::to ( '/' ) . '/user/activate-account-success/' . $userId . '/' . $user->activation_code;
		
		MailModule::send ( $user->email, '激活账号', $activateUrl );
		return true;
	}
	
	/**
	 * 激活用户成功
	 *
	 * @param int $userId        	
	 *
	 * @return bool
	 */
	public static function activateAccountSuccess($userId) {
		$user = User::find ( $userId );
		if (! $user) {
			return false;
		}
		
		$user->group_id = self::GROUP_COMMON_USER;
		$user->save ();
		
		\Event::fire ( 'user.activate', array (
				$user->id 
		) );
		
		return true;
	}
	
	/**
	 * 找回密码
	 *
	 * @param string $loginName        	
	 */
	public static function findPassword($loginName) {
		$user = self::getUserByLoginName ( $loginName );
		if (! $user) {
			return false; // Already checked exist at controller
		}
		
		$user->password_reset_code = self::generateCode ();
		$user->save ();
		
		$resetUrl = URL::to ( '/' ) . '/user/forget-password/' . $user->id . '/' . $user->password_reset_code;
		MailModule::send ( $user->email, '重置密码', $resetUrl );
		return true;
	}
	
	/**
	 * 重设密码
	 *
	 * @param int $userId        	
	 * @param string $password
	 *
	 * @return bool
	 */
	public static function resetPassword($userId, $password) {
		$user = User::find ( $userId );
		
		$user->password = self::hash ( $password );
		$user->save ();
		
		\Session::set ( 'user_id', '' );
		
		return true;
	}
	
	/**
	 * 修改密码
	 *
	 * @param int $userId
	 *        	用户Id
	 * @param string $oldPassword
	 *        	旧密码
	 * @param string $newPassword
	 *        	新密码
	 */
	public static function changePassword($userId, $oldPassword, $newPassword) {
		$user = self::getUserById ( $userId );
	}
	
	/**
	 * 填充用户信息（单个）
	 *
	 * @param object $object        	
	 * @param array $userIdProperties        	
	 */
	public static function fillUser($collection, array $userIdProperties = ['user_id']) {
		if (! $collection) {
			return;
		}
		
		foreach ( $userIdProperties as &$userIdProperty ) {
			$property = substr ( $userIdProperty, 0, strlen ( $userIdProperty ) - 3 ); // Remove last '_id' for user object key
			$user = UserModule::getUserDetailByUserId ( $collection->$userIdProperty );
			
			$collection->$property = json_decode ( json_encode ( $user->toArray () ) );
		}
	}
	
	/**
	 * 填充用户信息（多个）
	 *
	 * @param mixed $collections        	
	 * @param array $userIdProperties        	
	 */
	public static function fillUsers($collections, array $userIdProperties = ['user_id']) {
		if (! $collections) {
			return;
		}
		
		$userIds = [ ];
		foreach ( $userIdProperties as &$userIdProperty ) {
			foreach ( $collections as &$record ) {
				$userIds [] = $record->$userIdProperty;
			}
		}
		
		$userIds = array_unique ( $userIds );
		if (count ( $userIds )) {
			$users = self::getUserByIds ( $userIds );			
			foreach($userIdProperties as &$userIdProperty) {
			    $property = substr ( $userIdProperty, 0, strlen ( $userIdProperty ) - 3 ); // Remove last '_id' for user object key
			    foreach($users as &$user) {
			        $user = json_decode ( json_encode ( $user->toArray () ) );
			        	
			        foreach($collections as $key => &$record) {
			            if ($record->$userIdProperty == $user->id) {
			                $record->$property = $user;
			            }
			        }
			    }
			
			}
		
		}
	}
	
	/**
	 * 根据用户Id获取用户
	 *
	 * @param int $id        	
	 *
	 * @return User null
	 */
	public static function getUserById($id) {
		return User::find ( $id );
	}
	
	/**
	 * 根据用户Id获取用户（多个）
	 *
	 * @param array $ids        	
	 *
	 * @return array User
	 */
	public static function getUserByIds(array $ids) {
		return User::findMany ( $ids );
	}
	
	/**
	 * 根据登录名获取用户
	 *
	 * @param string $loginName        	
	 *
	 * @return User null
	 */
	public static function getUserByLoginName($loginName) {
		if (Utils::isNatureNumber ( $loginName )) {
			$loginField = 'cc_no';
		} else if (Utils::isEmail ( $loginName )) {
			$loginField = 'email';
		} else {
			$loginField = 'name';
		}
		
		return User::where ( $loginField, $loginName )->first ();
	}
	
	/**
	 * 根据邮箱地址获取用户
	 *
	 * @param string $email        	
	 *
	 * @return User null
	 */
	public static function getUserByEmail($email) {
		return User::where ( 'email', $email )->first ();
	}

	public static function getUserByMobile($mobile){
		return User::where ( 'mobile_phone_no', $mobile )->first ();
	}
	
	/**
	 * 根据用户名获取用户
	 *
	 * @param string $name        	
	 *
	 * @return User null
	 */
	public static function getUserByName($name) {
		return User::where ( 'name', $name )->first ();
	}
	
	/**
	 * 获取“你可能感兴趣的人”
	 *
	 * @return array null
	 */
	public static function getCarePeople($userId = 0) {
		$members = CircleModule::getAllMembersByUserId ( $userId );
		$ids = array ();
		foreach ( $members as $member ) {
			$ids [] = $member ['user_id'];
		}
		$ids [] = $userId;
		
		$idsStr = '(' . implode ( ',', $ids ) . ')';
		
		$users = \DB::select ( 'SELECT * FROM `users` AS t1
JOIN (
SELECT ROUND( RAND() * (( SELECT MAX(id) FROM `users`)-(SELECT MIN(id) FROM `users`) ) + (SELECT MIN(id) FROM `users`) ) AS id from `users`  where id not in '.$idsStr.' limit 100
) AS t2
on t1.id = t2.id
LIMIT 10' );
		return $users;
	}
	
	/**
	 * 搜索结果--用户信息
	 *
	 * @param string $keyword        	
	 * @param int $type        	
	 * @param int $offset        	
	 * @param int $limit        	
	 *
	 * @return mixed 用户信息
	 */
	public static function searchUsersByKeyword($keyword, $type = BaseModule::TYPE_USER, $offset = 0, $limit = 3) {
		if ($type == BaseModule::GROUP_EXPERT) {
			$datas = User::where ( 'group_id', self::GROUP_EXPERT )->where ( 'name', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->get ();
		} else {
			$datas = User::where ( 'name', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->get ();
		}
		return $datas;
	}
	public static function searchUsersByKeywordCount($keyword, $type = BaseModule::TYPE_USER) {
		if ($type == BaseModule::GROUP_EXPERT) {
			$count = User::where ( 'group_id', self::GROUP_EXPERT )->where ( 'name', 'like', '%' . $keyword . '%' )->count ();
		} else {
			$count = User::where ( 'name', 'like', '%' . $keyword . '%' )->count ();
		}
		return $count;
	}
	
	/**
	 * 根据地区信息获取专家
	 *
	 * @param int $pId        	
	 * @param int $cId        	
	 * @param int $dId        	
	 *
	 * @return Ambigous <\Illuminate\Database\Query\Builder, static, \Illuminate\Database\Query\static, \Illuminate\Database\Query\Builder>
	 */
	public static function getExpertsByRegionId($id = 0, $offset = 0, $limit = 24) {
		if (strlen ( $id ) == 1) {
			$data = new User ();
		} else if (strlen ( $id ) == 2) {
			$data = User::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = User::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = User::where ( 'district_id', $id );
		} else {
			return array ();
		}
		
		return $data->where ( 'group_id', self::GROUP_EXPERT )->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}
	public static function getExpertsByRegionIdCount($id = 0) {
		if (strlen ( $id ) == 1) {
			$data = new User ();
		} else if (strlen ( $id ) == 2) {
			$data = User::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = User::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = User::where ( 'district_id', $id );
		} else {
			return 0;
		}
		
		return $data->where ( 'group_id', self::GROUP_EXPERT )->count ();
	}
	
	/**
	 * 根据地区信息获一般用户
	 *
	 * @param int $pId        	
	 * @param int $cId        	
	 * @param int $dId        	
	 *
	 * @return Ambigous <\Illuminate\Database\Query\Builder, static, \Illuminate\Database\Query\static, \Illuminate\Database\Query\Builder>
	 */
	public static function getCommonUsersByRegionId($id, $offset = 0, $limit = 24) {
		if (strlen ( $id ) == 1) {
			$data = new User ();
		} else if (strlen ( $id ) == 2) {
			$data = User::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = User::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = User::where ( 'district_id', $id );
		} else {
			return array ();
		}
		
		return $data->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}
	public static function getCommonUsersByRegionIdCount($id) {
		if (strlen ( $id ) == 1) {
			$data = new User ();
		} else if (strlen ( $id ) == 2) {
			$data = User::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = User::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = User::where ( 'district_id', $id );
		} else {
			return array ();
		}
		
		return $data->count ();
	}
	
	/**
	 * 根据学校ID获取专家
	 *
	 * @param int $id        	
	 *
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getExpertsBySchoolId($id, $offset = 0, $limit = 24) {
		return User::where ( 'group_id', self::GROUP_EXPERT )->where ( 'school_id', $id )->offset ( $offset )->limit ( $limit )->get ();
	}
	public static function getExpertsBySchoolIdCount($id) {
		return User::where ( 'group_id', self::GROUP_EXPERT )->where ( 'school_id', $id )->count ();
	}
	
	/**
	 * 根据政企ID获取专家
	 *
	 * @param int $id        	
	 *
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getExpertsByOrganizationId($id, $offset = 0, $limit = 24) {
		return User::where ( 'group_id', self::GROUP_EXPERT )->where ( 'organization_id', $id )->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}
	public static function getExpertsByOrganizationIdCount($id) {
		return User::where ( 'group_id', self::GROUP_EXPERT )->where ( 'organization_id', $id )->count ();
	}
	
	/**
	 * 根据学校ID获取
	 *
	 * @param int $id        	
	 *
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getCommonUsersBySchoolId($id, $offset = 0, $limit = 24) {
		return User::where ( 'school_id', $id )->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}
	public static function getCommonUsersBySchoolIdCount($id) {
		return User::where ( 'school_id', $id )->count ();
	}
	
	/**
	 * 根据政企ID获取交易
	 *
	 * @param int $id        	
	 *
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getCommonUsersByOrganizationId($id, $offset = 0, $limit = 24) {
		return User::where ( 'organization_id', $id )->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}
	public static function getCommonUsersByOrganizationIdCount($id) {
		return User::where ( 'organization_id', $id )->count ();
	}
	
	/**
	 * 填充用户地区学校政企信息
	 *
	 * @param int $userId        	
	 *
	 * @return Ambigous <\App\Models\User, \Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\Collection, \Illuminate\Database\Eloquent\static, NULL, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getUserDetailByUserId($userId) {
		if(\Cache::has('user_'.$userId)){
			$user = \Cache::get('user_'.$userId);
		}else{
			$user = UserModule::getUserById ( $userId );

			$province = Region::find ( $user->province_id );
			$city = Region::find ( $user->city_id );
			$district = Region::find ( $user->district_id );
			$school = School::find ( $user->school_id );
			$organization = Organization::find ( $user->organization_id );

			$user->province_name = $province ? $province->name : '';
			$user->city_name = $city ? $city->name : '';
			$user->district_name = $district ? $district->name : '';
			$user->school_name = $school ? $school->name : '';
			$user->organization_name = $organization ? $organization->name : '';

			\Cache::put('user_'.$userId,$user,Carbon::now()->addMinutes(10));
		}
		return $user;
	}
	
	/**
	 * 获取用户组创意指数排行
	 *
	 * @param array $userIds        	
	 * @param int $offset        	
	 * @param int $limit        	
	 */
	public static function getCreativeRankByUserIds($userIds, $offset = 0, $limit = 10) {
		$users = User::offset ( $offset )->limit ( $limit );
		if (is_array ( $userIds ) && count ( $userIds )) {
			$users->whereIn ( 'id', $userIds );
		}
		return $users->orderBy ( 'creative_index', 'desc' )->get ();
	}
	
	/**
	 * 创创召 召一下
	 *
	 * @param int $id        	
	 *
	 * @return boolean
	 */
	public static function useCcz($id) {
		$user = User::find ( $id );
		if ($user) {
			\Event::fire ( 'user.ccz', array (
					$user 
			) );
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 获取创意世界所有官方账号
	 *
	 * @return array
	 */
	public static function getOfficialAccounts() {
		return User::whereIn ( 'name', self::$officialAccount )->get ();
	}
	
	/**
	 * 计算用户的统计数据
	 *
	 * @param
	 *        	$userId
	 */
	public static function calUserCount($userId) {
		$user = self::getUserById ( $userId );
		$contestCreativeIndex = ContestModule::getContestCreativeIndex ( $userId );
		$exchangeTradeCreativeIndex = ExchangeModule::getExchangeTradeCreativeIndexByUserId ( $userId );
		$encyclopediaCreativeIndex = EncyclopediaModule::getEncyclopediaCreativeIndexByUserId ( $userId );
		$user->creative_index = $contestCreativeIndex + $exchangeTradeCreativeIndex + $encyclopediaCreativeIndex;
		$user->contest_publish_count = ContestModule::getContestPublishCount ( $userId );
		$user->trade_publish_count = ExchangeModule::getTradePublishCount ( $userId );
		$user->follower_count = CircleModule::getCircleMeNum ( $userId );
		$user->save ();
	}
	
	/**
	 * 计算所有的统计数据
	 */
	public static function calAllCount() {
		$users = User::all ();
		foreach ( $users as $user ) {
			self::calUserCount ( $user->id );
			ContestModule::updateAreaInfo ( $user->id, $user->province_id, $user->city_id, $user->district_id, $user->school_id, $user->organization_id );
			ExchangeModule::updateAreaInfo ( $user->id, $user->province_id, $user->city_id, $user->district_id, $user->school_id, $user->organization_id );
			EncyclopediaModule::updateAreaInfo ( $user->id, $user->province_id, $user->city_id, $user->district_id, $user->school_id, $user->organization_id );
		
			AreaModule::calAreaCount ( BaseModule::TYPE_CONTEST,$user->province_id, $user->city_id, $user->district_id, $user->school_id, $user->organization_id );
			AreaModule::calAreaCount ( BaseModule::TYPE_EXCHANGE, $user->province_id, $user->city_id, $user->district_id, $user->school_id, $user->organization_id );
			AreaModule::calAreaCount ( BaseModule::TYPE_USER, $user->province_id, $user->city_id, $user->district_id, $user->school_id, $user->organization_id );
		}
		
		AreaModule::calChinaCount ();
	}
}