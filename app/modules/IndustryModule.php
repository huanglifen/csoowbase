<?php

namespace App\Modules;

use App\Models\Industries;
use Carbon\Carbon;
use App\Models\IndustriesExperts;

/**
 * 创意百科模块
 */
class IndustryModule extends ItemModule {
	const FIRST_CLASS = 0; // 表示行业大类的级别，0为大类行业

	/**
	 * 根据行业id获取行业
	 */
	public static function getIndustryById($id) {
		return Industries::find ( $id );
	}
	
	/**
	 * 获取大类行业
	 *
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return array
	 */
	public static function getFirstIndustries($offset = 0, $limit = 20) {
		return Industries::where ( 'class_id', self::FIRST_CLASS )->offset ( $offset )->limit ( $limit )->get ();
	}
	
	/**
	 * 获取二类行业
	 *
	 * @param int $id        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return array
	 */
	public static function getSecondIndustries($id = 0, $offset = 0, $limit = 150) {
		if ($id) {
			return Industries::where ( 'class_id', $id )->offset ( $offset )->limit ( $limit )->get ();
		} else {
			return Industries::where ( 'class_id', '>', 0 )->offset ( $offset )->limit ( $limit )->get ();
		}
	}
	
	public static function updateIndustryName($id, $name) {
		$industry = self::getIndustryById($id);
		$industry->name = $name; 
		$industry->save(); 
		
		if (!$name) {
			if (!$industy->class_id) {
				IndustriesExperts::where('industry_id', $id)->delete();
			} else {
				IndustriesExperts::where('industry_sub_id', $id)->delete();
			}
		}
		
		return true;
	}
	
	public static function createIndustry($name, $firstId = 0) {
		$industry = new Industries();
		$industry->title = $name;
		$industry->class_id = $firstId;
		$industry->description = $name;
		$industry->is_recommended = 0;
		
		$industry->save();
		
		return $industry->id;
	}
	
	public static function recommendedIndustry($id, $isRecommended = 1) {
		$industry = self::getIndustryById($id);
		
		$industry->is_recommended = $isRecommended;
		$industry->save();
		
		return true;
	}

	/**
	 * 获取推荐行业和行业的专家
	 * @param int $limit
	 *
	 * @return array|mixed|static
	 */
	public static function getRecommendIndustriesAndExperts($limit = 6) {
		if(\Cache::has('recommend_industries_and_experts')){
			$value = \Cache::get('recommend_industries_and_experts');
		}else{
			$value = self::getRecommendIndustries ($limit);
			foreach ( $value as $k=>$recommendIndustry ) {
				$users = ExpertModule::getIndustryExperts($recommendIndustry->id);
				$value[$k]->users = $users;
			}
			\Cache::put('recommend_industries_and_experts',$value,Carbon::now()->addMinutes(10));
		}
		return $value;
	}

	/**
	 * 获取推荐的子行业和行业的专家
	 * @param int $limit
	 *
	 * @return array|mixed|static
	 */
	public static function getRecommendSubIndustriesAndExperts($limit = 6) {
		if(\Cache::has('recommend_sub_industries_and_experts')){
			$value = \Cache::get('recommend_sub_industries_and_experts');
		}else{
			$value = self::getRecommendSubIndustries ($limit);
			foreach ( $value as $k=>$recommendIndustry ) {
				$users = ExpertModule::getSubIndustryExperts($recommendIndustry->id);
				$value[$k]->users = $users;
			}
			\Cache::put('recommend_sub_industries_and_experts',$value,Carbon::now()->addMinutes(10));
		}
		return $value;
	}


	public static function getRecommendIndustries( $limit = 6) {
		return Industries::where ( 'is_recommended', 1 )->where ( 'class_id', 0 )->limit ( $limit )->orderBy ( 'id', 'asc' )->get ();
	}

	public static function getRecommendSubIndustries( $limit = 16) {
		return Industries::where ( 'is_recommended', 1 )->where ( 'class_id', '>', 0 )->limit ( $limit )->orderBy ( 'id', 'asc' )->get ();
	}

}