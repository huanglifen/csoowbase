<?php

namespace App\Modules;

use App\Models\Contest;
use App\Models\Project;
use App\Models\ProjectInvestments;
use App\Models\TaskWork;
use App\Models\HiringParticipators;

class ContestModule extends ItemModule {
	const TYPE_PROJECT = 1;
	const TYPE_TASK = 2;
	const TYPE_HIRING = 3;
	
	/**
	 * 根据Id获取一个比赛
	 * @param int $id
	 * @return contest
	 */
	public static function getContestById($id) {
		return Contest::find($id);
	}
	
	/**
	 * 获取统计计数
	 * 
	 * @param string $selectType        	
	 * @param number $selectId        	
	 *
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getCount($selectType = '', $selectId = 0) {
		if ($selectType) {
			$count = Contest::where ( $selectType, $selectId )->count ();
		} else {
			$count = Contest::count ();
		}
		return $count ? $count : 0;
	}
	
	/**
	 * 获取热门比赛
	 *
	 * @return array null
	 */
	public static function getHotContest() {
		$datas = \DB::select ( 'SELECT t1.id,t1.type,t1.status,t1.title,t1.cover,t1.creative_index FROM `contests` AS t1
JOIN (
SELECT ROUND( RAND() * (( SELECT MAX(id) FROM `contests`)-(SELECT MIN(id) FROM `contests`) ) + (SELECT MIN(id) FROM `contests`) ) AS id from `contests` limit 40
) AS t2
on t1.id = t2.id and t1.status = '.ItemModule::STATUS_GOING.'
LIMIT 4' );
		foreach ( $datas as $k => $data ) {
			if ($data->type == self::TYPE_PROJECT) {
				$datas [$k]->displayNum = ProjectInvestments::where ( 'project_id', $data->id )->whereIn ( 'status', array (
						ProjectModule::INVEST_STATUS_ACCEPT,
						ProjectModule::INVEST_STATUS_SUBMIT,
						ProjectModule::INVEST_STATUS_DEPOSIT,
						ProjectModule::INVEST_STATUS_PAY_FINAL 
				) )->count ();
			} elseif ($data->type == self::TYPE_TASK) {
				$datas [$k]->displayNum = TaskWork::where ( 'task_id', $data->id )->whereIn ( 'status', array (
						TaskModule::TASK_WORK_STATUS_SUBMIT,
						TaskModule::TASK_WORK_STATUS_WINNER 
				) )->count ();
			} elseif ($data->type == self::TYPE_HIRING) {
				$datas [$k]->displayNum = HiringParticipators::where ( 'hiring_id', $data->id )->whereIn ( 'status', array (
						HiringModule::PARTICIPATOR_STATUS_APPLY,
						HiringModule::PARTICIPATOR_STATUS_WINNER 
				) )->count ();
			}
		}
		return $datas;
	}
	
	/**
	 * 搜索比赛
	 *
	 * @param string $keyword        	
	 * @param number $type        	
	 * @param number $offset        	
	 * @param number $limit        	
	 *
	 * @return unknown
	 */
	public static function searchContestsByKeyword($keyword, $type = 0, $offset = 0, $limit = 16) {
		if (! $type) {
			$datas = Contest::where ( 'title', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->orderBy ( 'id', 'desc' )->get ();
		} else {
			$datas = Contest::where ( 'type', $type )->where ( 'title', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->orderBy ( 'id', 'desc' )->get ();
		}
		return $datas;
	}
	public static function searchContestsByKeywordCount($keyword, $type = 0) {
		if (! $type) {
			$count = Contest::where ( 'title', 'like', '%' . $keyword . '%' )->count ();
		} else {
			$count = Contest::where ( 'type', $type )->where ( 'title', 'like', '%' . $keyword . '%' )->count ();
		}
		return $count;
	}
	
	/**
	 * 根据地区信息获取比赛
	 *
	 * @param int $pId        	
	 * @param int $cId        	
	 * @param int $dId        	
	 *
	 * @return Ambigous <\Illuminate\Database\Query\Builder, static, \Illuminate\Database\Query\static, \Illuminate\Database\Query\Builder>
	 */
	public static function getContestsByRegionId($id = 0, $offset = 0, $limit = 12) {
		if (strlen ( $id ) == 1) {
			$data = new Contest ();
		} else if (strlen ( $id ) == 2) {
			$data = Contest::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = Contest::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = Contest::where ( 'district_id', $id );
		} else {
			return array ();
		}
		
		return $data->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}
	public static function getContestsByRegionIdCount($id = 0) {
		if (strlen ( $id ) == 1) {
			$data = new Contest ();
		} else if (strlen ( $id ) == 2) {
			$data = Contest::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = Contest::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = Contest::where ( 'district_id', $id );
		} else {
			return 0;
		}
		
		return $data->count ();
	}
	
	/**
	 * 根据学校ID获取比赛
	 *
	 * @param int $id        	
	 *
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getContestsBySchoolId($id, $offset = 0, $limit = 12) {
		return Contest::where ( 'school_id', $id )->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}
	public static function getContestsBySchoolIdCount($id) {
		return Contest::where ( 'school_id', $id )->count ();
	}
	
	/**
	 * 根据政企ID获取比赛
	 *
	 * @param int $id        	
	 *
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getContestsByOrganizationId($id, $offset = 0, $limit = 12) {
		return Contest::where ( 'organization_id', $id )->offset ( $offset )->limit ( $limit )->orderBy ( 'id', 'desc' )->get ();
	}
	public static function getContestsByOrganizationIdCount($id) {
		return Contest::where ( 'organization_id', $id )->count ();
	}
	
	/**
	 * 更改用户的地区信息
	 * 
	 * @param unknown $userId        	
	 * @param unknown $provinceId        	
	 * @param unknown $cityId        	
	 * @param unknown $districtId        	
	 * @param unknown $schoolId        	
	 * @param unknown $organizationId        	
	 */
	public static function updateAreaInfo($userId, $provinceId, $cityId, $districtId, $schoolId, $organizationId) {
		$datas = Contest::where ( 'user_id', $userId )->get ();
		foreach ( $datas as $data ) {
			$provinceId ? $data->province_id = $provinceId : '';
			$cityId ? $data->city_id = $cityId : '';
			$districtId ? $data->district_id = $districtId : '';
			$schoolId ? $data->school_id = $schoolId : '';
			$organizationId ? $data->organization_id = $organizationId : '';
			$data->save ();
		}
	}
	
	/**
	 * 获取用户发布的比赛数量
	 * 
	 * @param
	 *        	$userId
	 *        	
	 * @return int
	 */
	public static function getContestPublishCount($userId) {
		return Contest::where ( 'user_id', $userId )->count ();
	}
	
	/**
	 * 计算比赛和作品的创意指数
	 * 
	 * @param
	 *        	$userId
	 * @param int $type        	
	 *
	 * @return mixed
	 */
	public static function getContestCreativeIndex($userId, $type = 0) {
		if ($type == self::TYPE_PROJECT) {
			return Project::where ( 'user_id', $userId )->sum ( 'creative_index' );
		} elseif ($type == self::TYPE_TASK) {
			return TaskWork::where ( 'user_id', $userId )->sum ( 'creative_index' );
		} elseif ($type == self::TYPE_HIRING) {
			return HiringParticipators::where ( 'user_id', $userId )->sum ( 'creative_index' );
		} else {
			$d1 = Project::where ( 'user_id', $userId )->sum ( 'creative_index' );
			$d2 = TaskWork::where ( 'user_id', $userId )->sum ( 'creative_index' );
			$d3 = HiringParticipators::where ( 'user_id', $userId )->sum ( 'creative_index' );
			return $d1 + $d2 + $d3;
		}
	}
	
	/**
	 * 审核比赛
	 * @param int $id
	 * @param string $checkRemark
	 * @param int $status
	 * @return Ambigous <number, \Illuminate\Database\mixed>
	 */
	public static function checkContest($id, $checkRemark,$status) {
		$flag =  Contest::where('id', $id)->update(array('check_remark'=>$checkRemark, 'status' => $status));
		if($flag && $status == ItemModule::STATUS_NEED_UPDATE){
			\Event::fire('check.contest',array($id,$checkRemark));
		}
	}
}