<?php namespace App\Modules;

use App\Models\Contest;
use App\Models\Encyclopedia;
use App\Models\ExchangeTrade;
use App\Models\Notification;
use App\Models\ProjectInvestments;
use Illuminate\Support\Facades\Event;

/**
 * 系统消息模块
 * 处理系统消息的功能，如消息中心
 */
class NotificationModule extends BaseModule {

    const STATUS_UNREAD = 0; //未读

    const STATUS_READED = 1; //已读

    const LINK_PROJECT = 10; //投资比赛首页

    const LINK_PROJECT_INVESTORS = 11; //投资比赛洽谈的投资者页

    const LINK_PROJECT_AGREEMENT = 12; //投资比赛协议页

    const LINK_PROJECT_DYNAMIC = 13; //投资比赛动态更新页
    
    const LINK_PROJECT_EXPERT = 14; // 投资比赛专家点评页

	const LINK_PROJECT_EDIT = 15;//投资比赛更新页

    const LINK_TASK = 20; //任务人才首页
    
    const LINK_TASK_WORK = 21; //作品详情页

    const LINK_TASK_APPLY = 22; //任务人才报名页

    const LINK_TASK_AGREEMENT = 23; //任务人才协议页

    const LINK_TASK_DYNAMIC = 24; //

	const LINK_TASK_EDIT = 25;//任务人才更新页

    const LINK_HIRING = 30; //公益明星比赛首页
    
    const LINK_HIRING_WORK = 31; //明星参赛作品

    const LINK_HIRING_APPLY = 32; //公益明星比赛参赛页

    const LINK_HIRING_DYNAMIC = 33; //公益明星比赛动态页

	const LINK_HIRING_EDIT = 34;//公益明星更新页

    const LINK_EXCHANGE = 40; //交易所

	const LINK_EXCHANGE_EDIT = 41;//交易所交易更新页

    const LINK_ENCYCLOPEDIA = 50; //创意百科

	const LINK_ENCYCLOPEDIA_EDIT = 51;//创意百科更新页

    const LINK_USER_DYNAMIC = 60; //个人主页动态

    const LINK_CIRCLE_ME = 61; //圈我的人

    const LINK_EXPERT = 70; //智库

    const LINK_INDEX = 80; //创意世界首页
    
    const TYPE_COIN = 90;//创创币
    
    const ACTION_APPLY_PUBLISHER = 1; //比赛提交申请-发布者
    
    const ACTION_PUBLISH_UPDATE = 2; //比赛动态更新

    const ACTION_EXPERT_COMMENT_PUBLISHER = 3; //发表专家点评-发布者
    
    const ACTION_EXPERT_COMMENT_PLAYERS = 4; //发表专家点评-参赛者
    
    const ACTION_COMMENT_PUBLISHER = 5; //发表评论-发布者
    
    const ACTION_COMMENT_WORK = 6; //发表评论-作品
    
    const ACTION_COMMON = 7; //其他操作

    public static $listenEvents = array(
        'dynamic.dynamic-forward' => array(
            self::LINK_USER_DYNAMIC,
            '有人转发了您的动态',
        ),
        'circle.add-circle-member' => array(
            self::LINK_CIRCLE_ME,
            '您有新的粉丝',
        ),
        'expert.apply' => array(
            self::LINK_INDEX,
            '您已成功提交创意世界智库专家申请，请耐心等待工作人员回复'
        ),
        'expert.apply-fail' => array(
            self::LINK_EXPERT,
            '很遗憾，您未能通过创意世界智库专家审核，请重新核查您提供的资料'
        ),
        'expert.apply-success' => array(
            self::LINK_EXPERT,
            '恭喜您！您已通过审核，成为创意世界智库专家！'
        )
    );

    /**
	 * 生成消息
	 *
	 * @param int $userId
	 * @param string $content
	 * @param int $targetType
	 * @param string $info
	 *
	 * @return int 消息的ID
	 */
    public static function sendNotification($userId, $content, $targetType, $info) {
        $notification = new Notification();
        $notification->user_id = $userId;
        $notification->content = $content;
        $notification->target_type = $targetType;
        $notification->info = $info;
        $notification->status = self::STATUS_UNREAD;
        $notification->create_time = time();
        $notification->save();
        return $notification->id;
    }

    /**
	 * 更新一条消息状态为已读
	 * 
	 * @param int $id
	 */
    public static function updateNotificationToReaded($id) {
       
        $notification = Notification::find($id);
        
        if($notification->status == self::STATUS_UNREAD && $notification->target_type == self::TYPE_COIN){
        	\Event::fire('notification.update-to-readed', array($notification->user_id, $notification->info,$notification->content));
        }
        
        $notification->status = self::STATUS_READED;
        $notification->save();
        
        return true;
    }
    
    /**
	 * 用户的未读的消息数
	 *
	 * @param int $userId
	 * @return int 用户的未读消息数
	 */
    public static function getNotificationNum($userId,$status = self::STATUS_UNREAD) {
        return Notification::where('user_id', (int) $userId)->where('status', $status)->count();
    }

    /**
	 * 获取用户的消息
	 * 
	 * @param int $userId
	 * @param int $offset
	 * @param int $number
	 * @param int $status
	 *
	 * @return array 用户的消息
	 */
    public static function getNotifications($userId, $status = self::STATUS_UNREAD, $offset = 0, $limit = BaseModule::COMMENT_PAGE_SIZE) {
        $notifications = Notification::where('user_id', $userId)->where('status', $status)->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
        return $notifications;
    }
    
    public static function getCoinByEvent($notification) {
        if($notification->status == self::STATUS_UNREAD) {
            $info = json_decode($notification->info,true);
            $amount = $info['target_id'];
            $content = $info['coin_info'];
            AccountModule::getCoinByEvent($notification->user_id, $amount, AccountModule::REASON_COIN_PRIZE, $content, $notification->create_time);
        }
    }

    /**
	 * 消息事件监听
	 */
    public static function listenEvent() {
        //公共部分
        foreach (self::$listenEvents as $events => $value) {
            Event::listen($events, function ($targetId, $userId) use($value) {
                self::sendNotificationPrepare($userId, $value[1], $value[0], $targetId);
            });
        }
        /**
	     * 收到邀请专家点评
	     */
        Event::listen('expert.invite-expert', function ($userId, $expertUserId, $targetType, $targetId, $targetSubId = 0) {
            $user = UserModule::getUserById($userId);     
            $targetName = '';      
            switch ($targetType) {
                case BaseModule::TYPE_PROJECT:
                    $info = $targetId;
                    $linkType = self::LINK_PROJECT;
                    $target = ProjectModule::getProjectById($info);
                    $targetName = $target->title;
                    break;
                case BaseModule::TYPE_TASK :case BaseModule::TYPE_TASK_WORK:
                    $info = $targetSubId;
                    $linkType = self::LINK_TASK_WORK;
                    $target = TaskModule::getTaskById($targetId);
                    $targetName = $target->title;
                    break;
                case BaseModule::TYPE_HIRING :case BaseModule::TYPE_HIRING_WORK:
                    $info = $targetSubId;
                    $linkType = self::LINK_HIRING_WORK;
                    $target = HiringModule::getHiringById($targetId);
                    $targetName = $target->title;
                    break;
                case BaseModule::TYPE_EXCHANGE :
                    $info = $targetId;
                    $linkType = self::LINK_EXCHANGE;
                    $target = ExchangeModule::getTradeById($targetId);
                    $targetName = $target->title;
                    break;
                case BaseModule::TYPE_ENCYCLOPEDIA :
                    $info = $targetId;
                    $linkType = self::LINK_ENCYCLOPEDIA;
                    $target = EncyclopediaModule::getEncyclopediaById($targetId);
                    $targetName = $target->title;
                    break;                   
            }
            $content = $user->name."邀请您点评《" . $targetName . "》";
            self::sendNotificationPrepare($expertUserId, $content, $linkType, $info);
        });
        /**
	     * 专家发表点评
	     */
        Event::listen('expert-comment.publish', function ($targetType, $targetId,$targetSubId,$score,$expertUserId) {
            $invitors = array();
            $expert = UserModule::getUserById($expertUserId);
            switch ($targetType) {
                case BaseModule::TYPE_PROJECT:
                    //publisher
                    $target = ProjectModule::getProjectById($targetId);
                    $content = "您发布的创意投资项目比赛《" . $target->title . "》获得了新的专家点评";
                    $linkType = self::LINK_PROJECT_EXPERT;
                    self::sendNotificationPrepare($target->user_id, $content, $linkType, $targetId, self::ACTION_EXPERT_COMMENT_PUBLISHER);
                    //investors
                    $investors = ProjectModule::getProjectInvestors($targetId);
                    $contentInvestors = "您参与的创意投资项目比赛《" . $target->title . "》获得了新的专家点评";
                    self::sendNotificationsToUsers($investors, $contentInvestors, $linkType, $targetId, 0, self::ACTION_EXPERT_COMMENT_PLAYERS);
                    //invitors
                    $invitors = ExpertModule::getExpertInvitors($expertUserId, $targetType, $targetId);               
                    break;
                case BaseModule::TYPE_TASK:
                    $target = TaskModule::getTaskById($targetId);
                    $work = TaskModule::getTaskWorkById($targetSubId);
                    $linkType = self::LINK_TASK_WORK;
                    //publisher
                    $content = "您发布的创意任务人才比赛《" . $target->title . "》有作品获得了新的专家点评";                    
                    self::sendNotificationPrepare($target->user_id, $content, $linkType, $targetId, self::ACTION_EXPERT_COMMENT_PLAYERS);
                    //player
                    $contentPlayer  = "您发布在创意任务人才比赛《" . $work->title . "》中的作品获得了新的专家点评";
                    self::sendNotificationPrepare($work->user_id, $contentPlayer, $linkType, $targetSubId, self::ACTION_EXPERT_COMMENT_PUBLISHER);
                    //invitors
                    $invitors = ExpertModule::getExpertInvitors($expertUserId, $targetType, $targetId,$targetSubId);
                    break;
                case BaseModule::TYPE_HIRING:
                    $target = HiringModule::getHiringById($targetId);
                    $participator = HiringModule::getPaticipatorById($targetSubId);
                    $linkType = self::LINK_HIRING_WORK;
                    //publisher
                    $content = "您发布的创意公益明星比赛《" . $target->title . "》有作品获得了新的专家点评";
                    self::sendNotificationPrepare($target->user_id, $content, $linkType, $targetId, self::ACTION_EXPERT_COMMENT_PUBLISHER);
                    //player
                    $contentPlayer = "您发布在创意公益明星比赛《" . $target->title . "》中的作品获得了新的专家点评";
                    self::sendNotificationPrepare($participator->user_id, $contentPlayer, $linkType, $targetSubId, self::ACTION_EXPERT_COMMENT_PLAYERS);
                    //invitors
                    $invitors = ExpertModule::getExpertInvitors($expertUserId, $targetType,$targetId,$targetSubId);
                    break;
                case BaseModule::TYPE_EXCHANGE:
                    $target = ExchangeModule::getTradeById($targetId);
                    $linkType = self::LINK_EXCHANGE;
                    //publisher
                    $content = "您发布的创意交易《" . $target->title . "》获得了新的专家点评";
                    self::sendNotificationPrepare($target->user_id, $content, $linkType, $targetId, self::ACTION_EXPERT_COMMENT_PUBLISHER);
                    //invitors
                    $invitors = ExpertModule::getExpertInvitors($expertUserId, $targetType, $targetId);
                    break;
                case BaseModule::TYPE_ENCYCLOPEDIA:
                    $target = EncyclopediaModule::getEncyclopediaById($targetId);
                    $linkType = self::LINK_ENCYCLOPEDIA;
                    //publisher
                    $content = "您展示的创意《" . $target->title . "》获得了新的专家点评";
                    self::sendNotificationPrepare($target->user_id, $content, $linkType, $targetId, self::ACTION_EXPERT_COMMENT_PUBLISHER);
                    //invitors
                    $invitors = ExpertModule::getExpertInvitors($expertUserId, $targetType, $targetId);
                    break;
            }
            $contentInvitor = $expert->name . "已点评《". $target->title ."》";
            self::sendNotificationsToUsers($invitors, $contentInvitor, $linkType, $targetId);
        });
        /**
	     * 发布评论
	     */
        Event::listen('comment.publish', function ($userId, $targetType, $targetId) {
            if($targetType == BaseModule::TYPE_DYNAMIC) {
                $dynamic = DynamicModule::getDynamicById($targetId);
                if($userId == $dynamic->user_id) {
                    return true;
                }
                $content = "您收到了新的评论";
                self::sendNotificationPrepare($dynamic->user_id, $content, self::LINK_USER_DYNAMIC, $targetId);
                return true;
            }
            $actionType = self::ACTION_COMMENT_PUBLISHER;
            $targetUserId = 0;
            switch ($targetType) {
                case BaseModule::TYPE_PROJECT :
                    $linkType = self::LINK_PROJECT;
                    $target = ProjectModule::getProjectById($targetId);
                    $targetUserId = $target->user_id;
                    $content = "您发布的创意投资项目比赛《" . $target->title . "》收到了新的评论";
                    break;
                case BaseModule::TYPE_TASK :
                    $linkType = self::LINK_TASK;
                    $target = TaskModule::getTaskById($targetId);
                    $targetUserId = $target->user_id;
                    $content = "您发布的创意任务人才比赛《" . $target->title . "》收到了新的评论";
                    break;
                case BaseModule::TYPE_TASK_WORK :
                    $linkType = self::LINK_TASK_WORK;
                    $work = TaskModule::getTaskWorkById($targetId);
                    $target = TaskModule::getTaskById($work->task_id);
                    $targetUserId = $work->user_id;
                    $content = "您发布在创意任务人才比赛《" . $target->title . "》中的作品收到了新的评论";
                    $actionType = self::ACTION_COMMENT_WORK;
                    break;
                case BaseModule::TYPE_HIRING :
                    $linkType = self::LINK_HIRING;
                    $target = HiringModule::getHiringById($targetId);
                    $targetUserId = $target->user_id;
                    $content = "您发布的创意公益明星比赛《" . $target->title . "》收到了新的评论";
                    break;
                case BaseModule::TYPE_HIRING_WORK :
                    $linkType = self::LINK_HIRING_WORK;
                    $work = HiringModule::getPaticipatorById($targetId);
                    $target = HiringModule::getHiringById($work->hiring_id);
                    $targetUserId = $work->user_id;
                    $content = "您发布在创意公益明星比赛《" . $target->title . "》中的作品收到了新的评论";
                    $actionType = self::ACTION_COMMENT_WORK;
                    break;
                case BaseModule::TYPE_EXCHANGE :
                    $linkType = self::LINK_EXCHANGE;
                    $target = ExchangeModule::getTradeById($targetId);
                    $targetUserId = $target->user_id;
                    $content = "您发布的创意交易《" . $target->title . "》收到了新的评论";
                    break;
                case BaseModule::TYPE_ENCYCLOPEDIA :
                    $linkType = self::LINK_ENCYCLOPEDIA;
                    $target = EncyclopediaModule::getEncyclopediaById($targetId);
                    $targetUserId = $target->user_id;
                    $content = "您展示的创意《" . $target->title . "》收到了新的评论";
                    break;
                default:;break;
                     
            }
            if($targetType != BaseModule::TYPE_REGION && $targetType != BaseModule::TYPE_SCHOOL && $targetType != BaseModule::TYPE_ORGANIZATION && $targetUserId != $userId){
            	self::sendNotificationPrepare($targetUserId, $content, $linkType, $targetId, $actionType);
            }
           
        });
        
        //投资项目比赛
        /**
	     * 比赛到期还剩3天
	     */
        Event::listen('project.deadline-three', function ($projectId, $userId) {
            $project = ProjectModule::getProjectById($projectId);
            $content = "您发布的创意投资项目比赛《" . $project->title . "》，还剩3天就将到期，请尽快选出投资者";
            self::sendNotificationPrepare($userId, $content, self::LINK_PROJECT_INVESTORS, $projectId);
        });
        /**
	     * 比赛到期未选出投资者
	     */
        Event::listen('project.past-due', function ($projectId, $userId) {
            //publisher
            $project = ProjectModule::getProjectById($projectId);
            $content = "您发布的创意投资项目比赛《" . $project->title . "》，在限定时间内未选出投资者，现已关闭";
            self::sendNotificationPrepare($userId, $content, self::LINK_PROJECT, $projectId);
            //investors
            $investors = ProjectModule::getProjectInvestors($projectId);
            $contentInvestor = "您参与的创意投资项目比赛《" . $project->title . "》，在限定时间内未选出投资者，现已关闭";
            self::sendNotificationsToUsers($investors, $contentInvestor, self::LINK_PROJECT, $projectId);
            
            //竞猜 guess
            $guesses = GuessModule::getGuessByTargetIdAndTypeAndStatus($projectId, BaseModule::TYPE_PROJECT, 1);
            $contentGuess = "很遗憾，您在《". $project->title ."》中竞猜失败";
            self::sendNotificationsToUsers($guesses, $contentGuess, self::LINK_PROJECT, $projectId);
        });
        /**
	     * 提交投资申请
	     */
        Event::listen('project.invest', function ($investorUserId, $projectId) {
            //publisher 
            $project = ProjectModule::getProjectById($projectId);
            $content = "您发布的创意投资项目比赛《" . $project->title . "》获得了新的投资申请";
            self::sendNotificationPrepare($project->user_id, $content, self::LINK_PROJECT_INVESTORS, $projectId, self::ACTION_APPLY_PUBLISHER);
            //investor
            $contentInvestor = "您的投资申请已经成功提交，请持续关注比赛进程";
            self::sendNotificationPrepare($investorUserId, $contentInvestor, self::LINK_PROJECT, $projectId);
        });
        /**
	     * 接受投资意向
	     */
        Event::listen('project.accept-invest', function ($projectId, $investId, $userId) {
            //publisher
            $invest = ProjectInvestments::find($investId);
            $user = UserModule::getUserById($invest->user_id);
            $content = "您已经接受了" . $user->name . "的投资，请遵守协议履行责任及义务";
            self::sendNotificationPrepare($userId, $content, self::LINK_PROJECT_AGREEMENT, $invest->project_id);
            //investor
            $project = ProjectModule::getProjectById($projectId);
            $contentInvesotr = "恭喜您！创意投资项目比赛《" . $project->title . "》的发布者已经接受了您的投资申请，请遵守协议履行责任及义务";
            self::sendNotificationPrepare($invest->user_id, $contentInvesotr, self::LINK_PROJECT_AGREEMENT, $invest->project_id);
            //others
            $others = ProjectModule::getProjectInvestors($projectId, ProjectModule::INVEST_STATUS_SUBMIT);
            $contentOthers = "您参与的创意投资项目比赛《" . $project->title . "》，已经选出了投资者";
            self::sendNotificationsToUsers($others, $contentOthers, self::LINK_PROJECT, $invest->project_id,$invest->user_id);
            
            //竞猜 guess
            $guesses = GuessModule::getGuessByTargetIdAndTypeAndStatus($projectId, BaseModule::TYPE_PROJECT, 0);
            $contentGuess = "很遗憾，您在《". $project->title ."》中竞猜失败";
            self::sendNotificationsToUsers($guesses, $contentGuess, self::LINK_PROJECT, $invest->project_id);
        });
        /**
         * 发布进度更新
         */
        Event::listen('project.update', function ($userId, $projectId) {
            $project = ProjectModule::getProjectById($projectId);
            $content = "您参与的创意投资项目比赛《" . $project->title . "》有了新的进度更新";
            $investors = ProjectModule::getProjectInvestors($projectId);
            self::sendNotificationsToUsers($investors, $content, self::LINK_PROJECT_DYNAMIC, $projectId, 0, self::ACTION_PUBLISH_UPDATE);
        });
        /**
	     * 离支付投资全款、定金、尾款、项目完工剩余24小时提醒
	     */
        Event::listen('project.invest-deadline', function ($projectId, $userId, $investorUserId, $type = 1) {
            $content = '';
            $contentInvestor = '';
            switch ($type) {
                case 1:
                    $content = "按照协议约定，距离全额支付投资款项的剩余时间仅有24小时，请您提醒投资者注意把握时间";
                    $contentInvestor = "按照协议约定，距离全额支付投资款项的剩余时间仅有24小时，请您注意把握时间";
                    break;
                case 2:
                    $content = "按照协议约定，距离支付投资款项定金的剩余时间仅有24小时，请您提醒投资者注意把握时间";
                    $contentInvestor = "按照协议约定，距离支付投资款项定金的剩余时间仅有24小时，请您注意把握时间";
                    break;
                case 3:
                    $content = "按照协议约定，距离支付投资款项尾款的剩余时间仅有24小时，请您提醒投资者注意把握时间";
                    $contentInvestor = "按照协议约定，距离支付投资款项尾款的剩余时间仅有24小时，请您注意把握时间";
                    break;
                case 4:
                    $content = "按照协议约定，距离项目完工的剩余时间仅有24小时，请您注意把握时间";
                    $contentInvestor = "按照协议约定，距离项目完工的剩余时间仅有24小时，请您提醒投资者注意把握时间";
                    break;
            }
            //publisher
            self::sendNotificationPrepare($userId, $content, self::LINK_PROJECT, $projectId);
            //invesotr
            self::sendNotificationPrepare($investorUserId, $contentInvestor, self::LINK_PROJECT, $projectId);
        });
        
        //任务人才比赛
        /**
	     * 比赛剩余3天提醒
	     */
        Event::listen('task.deadline-three', function ($taskId, $userId) {
            $task = TaskModule::getTaskById($taskId);
            $content = "您发布的创意任务人才比赛《" . $task->title . "》，还剩3天就将到期，请尽快选出任务获胜者";
            self::sendNotificationPrepare($userId, $content, self::LINK_TASK_APPLY, $taskId);
        });
        /**
	     * 任务到期未选出获胜参赛者
	     */
        Event::listen('task.past-due', function ($taskId, $userId) {
            $task = TaskModule::getTaskById($taskId);
            //publisher	        
            $content = "您发布的创意任务人才比赛《" . $task->title . "》，在限定时间内未选出获胜人才，现已关闭";
            self::sendNotificationPrepare($userId, $content, self::LINK_TASK, $taskId);
            //players 
            $players = TaskModule::getTaskPlayers($taskId);
            $contentPlayers = "您参与的创意任务人才比赛《" . $task->title . "》，在限定时间内未选出获胜人才，现已关闭";
            self::sendNotificationsToUsers($players, $contentPlayers, self::LINK_TASK, $taskId);
            
            //竞猜 guess
            $guesses = GuessModule::getGuessByTargetIdAndTypeAndStatus($taskId, BaseModule::TYPE_TASK, 1);
            $contentGuess = "很遗憾，您在《". $task->title ."》中竞猜失败";
            self::sendNotificationsToUsers($guesses, $contentGuess, self::LINK_TASK, $taskId);
        });
        /**
	     * 提交参赛作品
	     */
        Event::listen('task.submit-work', function ($playerUserId, $taskId) {
            $task = TaskModule::getTaskById($taskId);
            //publisher
            $content = "您发布的创意任务人才比赛《" . $task->title . "》获得了新的参赛作品";
            self::sendNotificationPrepare($task->user_id, $content, self::LINK_TASK_APPLY, $taskId, self::ACTION_APPLY_PUBLISHER);
            //player
            $content = "您的参赛作品已经成功提交，请持续关注比赛进程";
            self::sendNotificationPrepare($playerUserId, $content, self::LINK_TASK_APPLY, $taskId);
        });
        /**
	     * 确认选为获胜作品
	     */
        Event::listen('task.choose-winner', function ($taskId, $workId, $userId, $winnerUserId) {
            $task = TaskModule::getTaskById($taskId);
            $winner = UserModule::getUserById($winnerUserId);
            //publisher
            $content = "您已经将" . $winner->name . "选为获胜任务人才，请遵守协议履行责任及义务";
            self::sendNotificationPrepare($userId, $content, self::LINK_TASK_AGREEMENT, $taskId);
            //winner
            $contentWinner = "恭喜您！创意任务人才比赛《" . $task->title . "》的发布者已经将你选为了获胜人才，请遵守协议履行责任及义务";
            self::sendNotificationPrepare($winnerUserId, $contentWinner, self::LINK_TASK_AGREEMENT, $taskId);
            //others	        
            $players = TaskModule::getTaskPlayers($taskId, TaskModule::TASK_WORK_STATUS_SUBMIT);
            $contentOthers = "您参与的创意任务人才比赛《" . $task->title . "》，已经选出了获胜人才";
            self::sendNotificationsToUsers($players, $contentOthers, self::LINK_TASK, $taskId, $winnerUserId);
            
            //竞猜 guess
            $guesses = GuessModule::getGuessByTargetIdAndTypeAndStatus($taskId, BaseModule::TYPE_TASK, 0);
            $contentGuess = "很遗憾，您在《". $task->title ."》中竞猜失败";
            self::sendNotificationsToUsers($guesses, $contentGuess, self::LINK_TASK, $taskId);
            
            
        });
        /**
	     * 任务完工剩余24小时提醒
	     */
        Event::listen('task.work-deadline', function ($taskId, $userId, $winnerUserId) {
            //publisher
            $content = "按照协议约定，距离任务完工的剩余时间仅有24小时，请您提醒获胜人才注意把握时间";
            self::sendNotificationPrepare($userId, $content, self::LINK_TASK, $taskId);
            //winner
            $contentWinner = "按照协议约定，距离任务完工的剩余时间仅有24小时，请您注意把握时间";
            self::sendNotificationPrepare($winnerUserId, $contentWinner, self::LINK_TASK, $taskId);
        });
        /**
         * 发布进度更新
         */
        Event::listen('task.update', function ($taskId) {
            $task = TaskModule::getTaskById($taskId);
            $content = "您参与的创意任务人才比赛《" . $task->title . "》有了新的进度更新";
            $players = TaskModule::getTaskPlayers($taskId);
            self::sendNotificationsToUsers($players, $content, self::LINK_TASK_DYNAMIC, $taskId, 0, self::ACTION_PUBLISH_UPDATE);
        });
        
        //公益明星比赛
        /**
	     * 比赛剩余3天提醒
	     */
        Event::listen('hiring.deadline-three', function ($hiringId, $userId) {
            $hiring = HiringModule::getHiringById($hiringId);
            $content = "您发布的创意公益明星比赛《" . $hiring->title . "》，还剩3天就将到期，请尽快选出获奖明星";
            self::sendNotificationPrepare($userId, $content, self::LINK_HIRING_APPLY, $hiringId);
        });
        /**
	     * 比赛到期未选完获奖明星
	     */
        Event::listen('hiring.past-due', function ($hiringId, $userId) {
            $hiring = HiringModule::getHiringById($hiringId);
            //publisher
            $content = "您发布的创意公益明星比赛《" . $hiring->title . "》，在限定时间内未选出获胜人才，现已关闭";
            self::sendNotificationPrepare($userId, $content, self::LINK_HIRING, $hiringId);
            //players
            $contentPlayers = "您参与的创意公益明星比赛《" . $hiring->title . "》，在限定时间内未选出获胜人才，现已关闭";
            $players = HiringModule::getHiringParticipators($hiringId);
            self::sendNotificationsToUsers($players, $contentPlayers, self::LINK_HIRING, $hiringId);
            
            //竞猜 guess
            $guesses = GuessModule::getGuessByTargetIdAndTypeAndStatus($hiringId, BaseModule::TYPE_HIRING, 1);
            $contentGuess = "很遗憾，您在《". $hiring->title ."》中竞猜失败";
            self::sendNotificationsToUsers($guesses, $contentGuess, self::LINK_HIRING, $hiringId);
        });
        /**
	     * 我要参赛
	     */
        Event::listen('hiring.apply', function ($playerUserId, $hiringId) {
            $hiring = HiringModule::getHiringById($hiringId);
            //publisher
            $content = "您发布的创意公益明星比赛《" . $hiring->title . "》有了新的参赛明星";
            self::sendNotificationPrepare($hiring->user_id, $content, self::LINK_HIRING_APPLY, $hiringId, self::ACTION_APPLY_PUBLISHER);
            //player
            $contentPlayer = "您已经成功参赛，请持续关注比赛进程";
            self::sendNotificationPrepare($playerUserId, $contentPlayer, self::LINK_HIRING_APPLY, $hiringId);
        });
        /**
	     * 选出获胜明星
	     */
        Event::listen('hiring.choose-winner', function ($hiringId, $participatorId, $winnerUserId, $userId) {
            $hiring = HiringModule::getHiringById($hiringId);
            $winner = UserModule::getUserById($winnerUserId);
            //publisher
            $content = "您已经将" . $winner->name . "选为获奖明星";
            self::sendNotificationPrepare($hiring->user_id, $content, self::LINK_HIRING, $hiringId);
            //winner
            $contentWinner = "恭喜您！创意公益明星比赛《" . $hiring->title . "》的发布者已经将你选为了获奖明星";
            self::sendNotificationPrepare($winnerUserId, $contentWinner, self::LINK_HIRING, $hiringId);
            //others
            $others = HiringModule::getHiringParticipators($hiringId);
            $contentOthers = "您参与的创意公益明星比赛《" . $hiring->title . "》，已经选出了获奖明星";
            self::sendNotificationsToUsers($others, $contentOthers, self::LINK_HIRING, $hiringId, $winnerUserId); 
            
            //竞猜 guess
            $guesses = GuessModule::getGuessByTargetIdAndTypeAndStatus($hiringId, BaseModule::TYPE_HIRING, 0);
            $contentGuess = "很遗憾，您在《". $hiring->title ."》中竞猜失败";
            self::sendNotificationsToUsers($guesses, $contentGuess, self::LINK_HIRING, $hiringId);
        });
        /**
         * 发布进度更新
         */
        Event::listen('hiring.update', function ($userId, $hiringId) {
            $hiring = HiringModule::getHiringById($hiringId);
            $content = "您参与的创意公益明星比赛《" . $hiring->title . "》有了新的进度更新";
            $players = HiringModule::getHiringParticipators($hiringId);
            self::sendNotificationsToUsers($players, $content, self::LINK_HIRING_DYNAMIC, $hiringId, 0, self::ACTION_PUBLISH_UPDATE);
        });

	    /**
	     * 审核比赛、交易 、百科
	     */
	    Event::listen('check.contest', function ($contestId,$reason) {
		    $model = Contest::find($contestId);
		    if($model){
			    if($model->type == ContestModule::TYPE_PROJECT){
				    $typeName = '投资项目';
				    $typeLink = self::LINK_PROJECT_EDIT;
			    }elseif($model->type == ContestModule::TYPE_TASK){
				    $typeName = ' 任务人才';
				    $typeLink = self::LINK_TASK_EDIT;
			    }else{
				    $typeName = '公益明星';
				    $typeLink = self::LINK_HIRING_EDIT;
			    }
			    $content = "您参与的创意".$typeName."比赛《" . $model->title . "》未通过审核.原因是：".$reason;
			    self::sendNotification($model->user_id, $content, $typeLink, $contestId);
		    }
	    });

	    Event::listen('check.exchange', function ($tradeId,$reason) {
		    $model = ExchangeTrade::find($tradeId);
		    if($model){
			    $content = "您参与的创意交易《" . $model->title . "》未通过审核.原因是：".$reason;
			    self::sendNotification($model->user_id, $content, self::LINK_EXCHANGE_EDIT, $tradeId);
		    }
	    });

	    Event::listen('check.encyclopedia', function ($encyclopediaId,$reason) {
		    $model = Encyclopedia::find($encyclopediaId);
		    if($model){
			    $content = "您参与的创意百科《" . $model->title . "》未通过审核.原因是：".$reason;
			    self::sendNotification($model->user_id, $content, self::LINK_ENCYCLOPEDIA_EDIT, $encyclopediaId);
		    }
	    });
    }

    /**
	 * 给多个用户发消息
	 * 
	 * @param array $userObjects
	 * @param string $content
	 * @param int $targetType
	 * @param string $info
	 * @param number $detachUserId
	 * @return int
	 */
    protected static function sendNotificationsToUsers($userObjects, $content, $targetType, $info, $detachUserId = 0, $actionType = self::ACTION_COMMON) {
        $userIds = array();
        if ($detachUserId) {
            $userIds[] = $detachUserId;
        }
        
        foreach ($userObjects as $object) {
            if (! in_array($object->user_id, $userIds)) {
                $userIds[] = $object->user_id;
                self::sendNotificationPrepare($object->user_id, $content, $targetType, $info, $actionType);
            }
        }
    }
    
    /**
     * 发送消息前去重
     *
     * @param int $userId
     * @param string $content
     * @param int $targetType
     * @param int $targetId
     * @param int $actionType
     *
     * @return bool|int
     */
    public static function sendNotificationPrepare($userId, $content, $targetType, $targetId, $actionType = self::ACTION_COMMON) {
        $info = array(
                'target_id' => $targetId,
                'action_type' => $actionType
        );
        $info = json_encode($info);
    
        if($actionType != self::ACTION_COMMON) {
            $notification = Notification::where('user_id', $userId)->where('target_type', $targetType)->where('info', $info)->where('status',self::STATUS_UNREAD)->first();
            if(! empty($notification)) {
                return true;
            }
        }
        self::sendNotification($userId, $content, $targetType, $info);
    }
}