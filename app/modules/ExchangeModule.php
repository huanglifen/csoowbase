<?php

namespace App\Modules;

use App\Models\ExchangeTrade;
use App\Models\TagsAffairs;

/**
 * 创意世界交易所模块
 *
 * 处理创意世界交易所功能。
 */
class ExchangeModule extends ItemModule {
	const LIST_SEARCH_TYPE_HOT = 1;
	const LIST_SEARCH_TYPE_GOING = 2;
	
	/**
	 * 获取统计计数
	 * @param string $selectType
	 * @param number $selectId
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getCount($selectType = '',$selectId = 0){
		if($selectType){
			$count =  ExchangeTrade::where($selectType,$selectId)->count();
		}else{
			$count =  ExchangeTrade::count();
		}
		return $count ? $count : 0;
	}
	
	/**
	 * 获取热门交易
	 *
	 * @return array null
	 */
	public static function getHotExchange() {
		return \DB::select ( 'SELECT t1.id,t1.status,t1.title,t1.cover,t1.creative_index FROM `exchange_trades` AS t1
JOIN (
SELECT ROUND( RAND() * (( SELECT MAX(id) FROM `exchange_trades`)-(SELECT MIN(id) FROM `exchange_trades`) ) + (SELECT MIN(id) FROM `exchange_trades`) ) AS id from `exchange_trades` limit 40
) AS t2
on t1.id = t2.id
LIMIT 4' );
	}
	public static function getRencentlyExchange($offset = 0, $limit = 3) {
		return ExchangeTrade::where ( 'status', '>=', self::STATUS_GOING )->orderBy ( 'id', 'desc' )->offset ( $offset )->limit ( $limit )->get ();
	}
	
	/**
	 * 获取最后一个交易编号
	 *
	 * @return string
	 */
	protected static function getLastItemNo() {
		$last = ExchangeTrade::orderBy ( 'id', 'desc' )->first ();
		
		return $last ? $last->no : 'T';
	}
	
	/**
	 * 创建一个创意世界交易
	 *
	 * @param array $common        	
	 * @param int $price        	
	 * @param int $tagNames        	
	 * @param int $userId        	
	 * @return int $tradeId
	 */
	public static function publish(array $common, $price, $tagNames, $userId) {
		$trade = new ExchangeTrade ();
		
		self::fillPublishCommon ( $trade, $common, $userId, self::STATUS_WAITING_CHECK );
		
		$trade->price = $price;
		
		$trade->save ();
		
		$tags = self::addItemTags ( $trade->id, BaseModule::TYPE_EXCHANGE, $tagNames );
		
		$trade->tags = json_encode ( $tags );
		$trade->check_remark = '';
		$trade->save ();
		
		\Event::fire ( 'trade.publish', array (
				$userId,
				$trade->id,
				$common ['title'] 
		) );
		
		return $trade->id;
	}
	
	public static function update($id, $common, $price, $tagsNames, $userId) {
	    $trade = self::getTradeById($id);
	    
	    if(! $trade || $trade->user_id != $userId) {
	        return false;
	    }
	    
	    $trade->title = $common['title'] ? $common['title'] : $trade->title;
	    $trade->cover = $common['cover'] ? $common['cover'] : $trade->cover;
	    $trade->description = $common['description'] ? $common['description'] : $trade->description;
	    $trade->price = $price ? $price : $trade->price;
	    $trade->status = self::STATUS_CHECK_AGAIN;
	    
	    $tags = self::addItemTags($trade->id, BaseModule::TYPE_EXCHANGE, $tagsNames);
	    
	    $trade->tags = json_encode($tags);
	    
	    $trade->save();
	    
	    \Event::fire ( 'trade.edit', array ($userId,$trade->id,$common ['title']) );
	    
	    return $trade->id;	    
	}
	
	/**
	 * 根据ID获取交易
	 *
	 * @param int $id        	
	 * @return ExchangeTrade null
	 */
	public static function getTradeById($id) {
		return ExchangeTrade::find ( $id );
	}
	
	/**
	 * 根据状态获取交易
	 *
	 * @param int $status        	
	 * @param number $offset        	
	 * @param number $limit
	 *        	return array $trades
	 */
	public static function getListsByStatus($status = 0, $offset = 0, $limit = BaseModule::ITEM_NUMBER_INDEX) {
		$trades = ExchangeTrade::offset ( $offset )->limit ( $limit )->orderBy ( 'id', 'desc' );
		
		if ($status) {
			$trades->where ( 'status', $status );
		} else {
			$trades->where ( 'status', '>', 0 );
		}
		
		return $trades->get ();
	}
	
	/**
	 * 根据标签和查询类型获取交易
	 *
	 * @param number $status        	
	 * @param number $tagId        	
	 * @param number $offset        	
	 * @param number $limit        	
	 * @return array $trades
	 */
	public static function getListsByTagIdAndStatus($status = 0, $tagId = 0, $offset = 0, $limit = BaseModule::ITEM_NUMBER_INDEX) {
		$tradeIds = array ();
		
		if ($tagId) {
			$affairs = TagsAffairs::where ( 'tag_id', $tagId )->where ( 'affair_type', BaseModule::TYPE_EXCHANGE )->get ();
			foreach ( $affairs as $affair ) {
				$tradeIds [] = $affair->affair_id;
			}
		}
		
		if ($status == self::LIST_SEARCH_TYPE_GOING) {
			$trades = ExchangeTrade::where ( 'status', self::STATUS_GOING );
		} elseif ($status == self::LIST_SEARCH_TYPE_HOT) {
			$trades = ExchangeTrade::where ( 'status', '>', self::STATUS_WAITING )->orderBy ( 'creative_index', 'desc' );
		} else {
			$trades = ExchangeTrade::where ( 'status', '>', self::STATUS_WAITING );
		}
		
		if (count ( $tradeIds )) {
			$trades->whereIn ( 'id', $tradeIds );
		}
		$return = $trades->orderBy ( 'id', 'desc' )->offset ( $offset )->limit ( $limit )->get ();
		
		if ($status == self::LIST_SEARCH_TYPE_HOT) {
			
			$return = self::randSortTrades ( $return );
		}
		
		return $return;
	}
	
	/**
	 * 获取某个标签和状态下的比赛总条数
	 * 
	 * @param number $tagId        	
	 * @param number $status        	
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getTotalByTagIdAndStatus($tagId = 0, $status = 0) {
		$model = new ExchangeTrade ();
		if ($tagId) {
			$Ids = array ();
			
			$tagsAffairs = TagsAffairs::where ( 'tag_id', $tagId )->where ( 'affair_type', BaseModule::TYPE_EXCHANGE )->get ();
			
			foreach ( $tagsAffairs as $tagsAffair ) {
				$Ids [] = $tagsAffair->affair_id;
			}
			
			$model->whereIn ( 'id', $Ids );
		}
		
		if ($status) {
			$model->where ( 'status', $status );
		} else {
			$model->where ( 'status', '>', 0 );
		}
		
		return $model->count ();
	}
	
	/**
	 * 对获取的交易进行随机排序
	 *
	 * @param array $trades        	
	 * @return arry $trades
	 */
	private static function randSortTrades($trades) {
		$ids = array ();
		
		foreach ( $trades as $key => $t ) {
			$ids [] = $t;
		}
		shuffle ( $ids );
		
		foreach ( $ids as $key => $value ) {
			$trades [$key] = $value;
		}
		
		return $trades;
	}
	
	/**
	 * 获取交易所排行榜
	 *
	 * @param number $offset        	
	 * @param number $limit        	
	 * @return array
	 */
	public static function getTradeRank($offset = 0, $limit = 5) {
		return ExchangeTrade::where ( 'status', '>', ItemModule::STATUS_WAITING )->orderBy ( 'creative_index', 'desc' )->offset ( $offset )->limit ( $limit )->get ();
	}
	
	/**
	 * 搜索结果--交易所
	 *
	 * @param string $keyword        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return array 交易所信息
	 */
	public static function searchExchangesByKeyword($keyword, $offset = 0, $limit = 12) {
		return ExchangeTrade::where ( 'title', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->get ();
	}
	
	public static function searchExchangesByKeywordCount($keyword){
		return ExchangeTrade::where ( 'title', 'like', '%' . $keyword . '%' )->count();
	}
	
	/**
	 * 根据地区信息获取交易所交易
	 *
	 * @param int $pId        	
	 * @param int $cId        	
	 * @param int $dId        	
	 * @return Ambigous <\Illuminate\Database\Query\Builder, static, \Illuminate\Database\Query\static, \Illuminate\Database\Query\Builder>
	 */
	public static function getExchangeTradesByRegionId($id = 0, $offset = 0, $limit = 12) {
		if (strlen ( $id ) == 1) {
			$data = new ExchangeTrade ();
		} else if (strlen ( $id ) == 2) {
			$data = ExchangeTrade::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = ExchangeTrade::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = ExchangeTrade::where ( 'district_id', $id );
		} else {
			return array ();
		}
		
		return $data->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}

	public static function getExchangeTradesByRegionIdCount($id = 0) {
		if (strlen ( $id ) == 1) {
			$data = new ExchangeTrade ();
		} else if (strlen ( $id ) == 2) {
			$data = ExchangeTrade::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = ExchangeTrade::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = ExchangeTrade::where ( 'district_id', $id );
		} else {
			return 0;
		}

		return $data->count ();
	}
	
	/**
	 * 根据学校ID获取交易
	 *
	 * @param int $id        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getExchangeTradesBySchoolId($id,$offset = 0,$limit= 12) {
		return ExchangeTrade::where ( 'school_id', $id )->offset($offset)->limit($limit)->orderBy('id','desc')->get ();
	}

	public static function getExchangeTradesBySchoolIdCount($id) {
		return ExchangeTrade::where ( 'school_id', $id )->count ();
	}
	
	/**
	 * 根据政企ID获取交易
	 *
	 * @param int $id        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getExchangeTradesByOrganizationId($id,$offset = 0,$limit= 12) {
		return ExchangeTrade::where ( 'organization_id', $id )->offset($offset)->limit($limit)->orderBy('id','desc')->get ();
	}
	public static function getExchangeTradesByOrganizationIdCount($id) {
		return ExchangeTrade::where ( 'organization_id', $id )->count ();
	}
	/**
	 * 获取交易
	 * 
	 * @param int $userId        	
	 * @param number $offset        	
	 * @param number $limit        	
	 */
	public static function getExchangeTradesByUserId($userId, $offset = 0, $limit = 12) {
		return ExchangeTrade::where ( 'user_id', $userId )->offset ( $offset )->limit ( $limit )->get ();
	}

	public static function getExchangeTradesByUserIdCount($userId) {
		return ExchangeTrade::where ( 'user_id', $userId )->count();
	}
	
	/**
	 * 更改用户的地区信息
	 * 
	 * @param unknown $userId        	
	 * @param unknown $provinceId        	
	 * @param unknown $cityId        	
	 * @param unknown $districtId        	
	 * @param unknown $schoolId        	
	 * @param unknown $organizationId        	
	 */
	public static function updateAreaInfo($userId, $provinceId, $cityId, $districtId, $schoolId, $organizationId) {
		$datas = ExchangeTrade::where ( 'user_id', $userId )->get ();
		foreach ( $datas as $data ) {
			$provinceId ? $data->province_id = $provinceId : '';
			$cityId ? $data->city_id = $cityId : '';
			$districtId ? $data->district_id = $districtId : '';
			$schoolId ? $data->school_id = $schoolId : '';
			$organizationId ? $data->organization_id = $organizationId : '';
			$data->save ();
		}
	}

	/**
	 * 获取用户发布的交易数量
	 * @param $userId
	 *
	 * @return int
	 */
	public static function getTradePublishCount($userId){
		return ExchangeTrade::where('user_id',$userId)->count();
	}

	/**
	 * 计算交易的创意指数
	 * @param unknown $userId
	 */
	public static function getExchangeTradeCreativeIndexByUserId($userId){
		return ExchangeTrade::where('user_id',$userId)->sum('creative_index');
	}

    /**
     * 根据状态获取审核交易
     */
    public static function getCheckExchange ($status = 0, $offset = 0, $limit = 12)
    {
        $trades = ExchangeTrade::offset($offset)->limit($limit)->orderBy('id', 'desc')->orderBy('status', 'desc');
        if($status == 0 || is_array($status) && count($status) == 0) {
            $status = array(self::STATUS_WAITING_CHECK,self::STATUS_CHECK_AGAIN);
        }
        if (is_array($status)) {
            $trades->whereIn('status', $status);
        } else {
            $trades->where('status', $status);
        }
                
        return $trades->get();
    }
    
    /**
     * 根据状态获取审核交易统计数
     * 
     * @param int $status
     * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
     */
    public static function getCheckExchangeCount($status = 0) {
        $trades = ExchangeTrade::orderBy('id', 'desc')->orderBy('status', 'desc');
        if($status == 0 || is_array($status) && count($status) == 0) {
            $status = array(self::STATUS_WAITING_CHECK,self::STATUS_CHECK_AGAIN);
        }
        if (is_array($status)) {
            $trades->whereIn('status', $status);
        } else {
            $trades->where('status', $status);
        }
        
        return $trades->count();
    }
    
    /**
     * 审核交易
     * 
     * @param int $id
     * @param string $checkRemark
     * @param int $status
     * @return Ambigous <number, \Illuminate\Database\mixed>
     */
    public static function checkExchange($id, $checkRemark, $status) {
        $flag =  ExchangeTrade::where('id', $id)->update(array('check_remark'=>$checkRemark, 'status' => $status));
	    if($flag && $status == ItemModule::STATUS_NEED_UPDATE){
		    \Event::fire('check.exchange',array($id,$checkRemark));
	    }
    }
}