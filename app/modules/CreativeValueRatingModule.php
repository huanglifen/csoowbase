<?php

namespace App\Modules;

use App\Models\CreativeValueRating;

/**
 * 评分模块
 * Class CreativeValueRatingModule
 *
 * @package App\Modules
 */
class CreativeValueRatingModule extends BaseModule {
	const STAR_1 = 1;
	const STAR_2 = 2;
	const STAR_3 = 3;
	const STAR_4 = 4;
	const STAR_5 = 5;
	
	/**
	 * 保存评分
	 *
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $score        	
	 *
	 * @return mixed
	 */
	public static function saveCreativeValueRating($userId, $targetType, $targetId, $score) {
		$model = new CreativeValueRating ();
		$model->user_id = $userId;
		$model->target_type = $targetType;
		$model->target_id = $targetId;
		$model->score = $score;
		$model->create_time = time ();
		$model->save ();
		
		\Event::fire ( 'creative-value-rating.publish', array (
				$targetType,
				$targetId,
				$score,
		        $userId
		) );
		
		
		return $model->id;
	}
	
	/**
	 * 是否已经评分
	 * 
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @return boolean
	 */
	public static function isExist($userId, $targetType, $targetId) {
		$cnt = CreativeValueRating::whereRaw ( 'user_id = ? and target_type = ? and target_id = ?', array (
				$userId,
				$targetType,
				$targetId 
		) )->count ();
		return $cnt > 0 ? true : false;
	}
	
	public static function getCreativeVauleRating($userId, $targetType, $targetId) {
		return CreativeValueRating::whereRaw ( 'user_id = ? and target_type = ? and target_id = ?', array (
				$userId,
				$targetType,
				$targetId 
		) )->first ();
	}
	
}