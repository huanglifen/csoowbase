<?php namespace App\Modules;
use App\Models\Project;
use App\Models\TagsAffairs;
use App\Models\Update;
use App\Models\ProjectInvestments;
/**
 * 创意投资项目比赛模块
 *
 * 处理创意投资项目比赛功能。
 */

class ProjectModule extends ItemModule {

	const INVEST_TYPE_TOTAL  = 1;
	const INVEST_TYPE_STAGES = 2;
	
	const INVEST_STATUS_SUBMIT    = 1; //提交
	const INVEST_STATUS_ACCEPT    = 2; //被接受
	const INVEST_STATUS_DEPOSIT	  = 3; //支付定金
	const INVEST_STATUS_PAY_FINAL = 4; //支付全款/定金
	const INVEST_STATUS_CANCLE    = 0; //撤回投资
	
	const PAY_LIMIT_TIME = 3; //支付全款、定金时间
	
	/**
	 * 获取最后一个比赛编号
	 *
	 * @return string
	 */
	protected static function getLastItemNo() {
		$last = Project::orderBy('id', 'desc')->first();

		return $last ? $last->no : 'P';
	}

	/**
	 * 发布比赛
	 *
	 * @param array $common
	 * @param int $userId 发布比赛的用户ID
	 * @param int $investmentEndTime
	 * @param int $goalAmount
	 * @param int $rewardGiveTime
	 * @param string $rewardDescription
	 * @param string $rewardPics
	 * @param array $tagsNames
	 * @return int 生成的比赛ID
	 */
	public static function publish(array $common, $userId, $investmentEndTime, $goalAmount, $tagsNames = '') {
		$project = new Project();

		self::fillPublishCommon($project, $common, $userId, self::STATUS_WAITING_CHECK);

		$project->investment_end_time = $investmentEndTime;
		$project->goal_amount = $goalAmount;
		$project->save();

		$tags = self::addItemTags($project->id, BaseModule::TYPE_PROJECT, $tagsNames);
		
		$project->tags = json_encode($tags);
		$project->save();
		
		\Event::fire('project.publish', array($userId, $project->id, $common['title']));

		return $project->id;
	}
	
	/**
	 * 修改投资项目比赛
	 * @param array $common
	 * @param int $projectId
	 * @param int $investmentEndTime
	 * @param int $goalAmount
	 * @param string $tagsNames
	 * @return projectId
	 */
	public static function updateProject(array $common, $projectId, $userId, $investmentEndTime, $goalAmount, $tagsNames = '') {
		$project = self::getProjectById($projectId);
		
		if ($project->user_id != $userId) {
			return false;
		}
		
		self::fillPublishCommon($project, $common, $project->user_id, self::STATUS_CHECK_AGAIN);
		
		$project->investment_end_time = $investmentEndTime;
		$project->goal_amount = $goalAmount;
		
		$tags = self::addItemTags($project->id, BaseModule::TYPE_PROJECT, $tagsNames);
		
		$project->tags = json_encode($tags);
		$project->save();
		
		self::deleteItemTags($projectId, self::TYPE_PROJECT);
		
		\Event::fire('project.update', array($projectId));
		
		return $projectId;
	}
	
	/**
	 * 添加一个project投资
	 * @param int $projectId
	 * @param int $userId
	 * @param int $amount
	 * @param int $type
	 * @param int $depositPercentage
	 * @param int $finalPaymentPayTime
	 * @param int $requireCompleteTime
	 * @param string $requirement
	 * @return int invsetmentId
	 */
	public static function invest($projectId, $userId, $amount, $type, $depositPercentage, $finalPaymentPayTime, $requireCompleteTime, $requirement) {
		if (!self::getProjectById($projectId)) {
			return false;
		}
		
		$invest = new ProjectInvestments();
		
		$invest->project_id = $projectId;
		$invest->user_id    = $userId;
		$invest->amount     = $amount;
		$invest->type       = $type;
		$invest->deposit_percentage = $depositPercentage;
		$invest->final_payment_pay_time = $finalPaymentPayTime;
		$invest->require_complete_time = $requireCompleteTime ? $requireCompleteTime : 0;
		$invest->requirement = $requirement;
		$invest->status = self::INVEST_STATUS_SUBMIT;
		$invest->accept_time = 0;
		$invest->create_time = time();
		
		$invest->save();
		
		\Event::fire('project.invest', array($userId, $projectId));
		
		return $invest->id;
	}

	/**
	 * 根据ID获取比赛
	 *
	 * @param int $id
	 * @return Project|null
	 */
	public static function getProjectById($id) {
		return Project::find($id);
	}
	
	/**
	 * 获取投资项目比赛排行
	 * @param int $num
	 * @return projects
	 */
	public static function getProjectRank($num = 5) {
		return Project::where('status', '>', 0)->orderBy('creative_index', 'desc')->offset(0)->limit($num)->get();
	}
	
	/**
	 * 生成一个项目更新
	 * 
	 * @param int $projectId
	 * @param string $title
	 * @param string $content
	 * @param int $userId
	 * @return int @updateId
	 */
	public static function createProjectUpdate($projectId, $title, $content, $userId) {
		$project = self::getProjectById($projectId);
		
		if (!$project) {		
			return false;
		}
		
		$update = new Update();
		
		$update->target_type = BaseModule::TYPE_PROJECT;
		$update->target_id   = $projectId;
		$update->title       = $title;
		$update->content     = $content;
		$update->user_id     = $userId;
		$update->create_time = time();
		
		$update->save();

		\Event::fire('project.update', array($userId, $projectId));

		return $update->id;
	}
	
	/**
	 * 获取投资比赛的动态
	 * 
	 * @param int $projectId
	 * @param number $offset
	 * @param number $limit
	 * @return boolean|updates
	 */
	public static function getUpdatesByProjectId($projectId, $offset = 0, $limit = 10) {
		$project = self::getProjectById($projectId);
		
		if (!$project) {		
			return false;
		}
		
		$updates = Update::where('target_type', BaseModule::TYPE_PROJECT)->where('target_id', $projectId)->limit($limit)->offset($offset)->orderBy('id', 'desc')->get();
		
		return $updates;
	}
	
	/**
	 * 撤回投资
	 * @param int $investId
	 * @param int $userId
	 * @return boolean
	 */
	public static function cancelInvest($investId, $userId) {
		$invest = self::checkInvest($investId, $userId);
		
		if (!$invest) {
			return false;
		}
		
		$invest->status = self::INVEST_STATUS_CANCLE;
		$invest->save();
		
		\Event::fire('project.cancel-invest', array($invest->project_id, $investId));
		
		return true;
	}
	
	/**
	 * 接受投资
	 * @param int $investId
	 * @param int $userId
	 * @return boolean
	 */
	public static function acceptInvest($investId, $userId) {
		$invest = ProjectInvestments::find($investId);
		
		if (!$invest || $invest->status != self::INVEST_STATUS_SUBMIT) {
			return false;
		}

		$project = self::getProjectById($invest->project_id);
		
		if (!$project || $project->user_id != $userId || $project->status != self::STATUS_GOING) {
			return false;
		}
		
		$invest->status = self::INVEST_STATUS_ACCEPT;
		$invest->accept_time = time();
		$invest->save();
		
		$project->status = ItemModule::STATUS_CONFIRM_INVEST;
		$project->save();
		\Event::fire('project.accept-invest', array($project->id, $investId, $userId));

		return true;
	}

	/**
	 * 修改投资信息
	 * @param int $investId
	 * @param int $userId
	 * @param int $investInfo
	 * @return $invest
	 */
	public static function updateInvest($investId, $userId, array $investInfo) {
		$invest = self::checkInvest($investId, $userId);
		
		if (!$invest) {
			return false;
		}
		
		$invest->amount = $investInfo['amount'] ? $investInfo['amount'] : $invest->amount;
		$invest->type = $investInfo['type'] ? $investInfo['type'] : $invest->type;
		$invest->deposit_percentage = $investInfo['deposit_percentage'] ? $investInfo['deposit_percentage'] : $invest->deposit_percentage;
		$invest->final_payment_pay_time = $investInfo['final_payment_pay_time'] ? $investInfo['final_payment_pay_time'] : $invest->final_payment_pay_time;
		$invest->require_complete_time = $investInfo['require_complete_time'];
		$invest->requirement = $investInfo['requirement'] ? $investInfo['requirement'] : $invest->requirement;
		$invest->save();
		
		return $invest;
	}
	
	/**
	 * 根据投资比赛ID获取该比赛的投资
	 * @param int $projectId
	 * @param number $offset
	 * @param number $limit
	 * @return boolean|void
	 */
	public static function getInvestsByProjectId($projectId, $offset = 0, $limit = 10, $isAccept = 0) {
		$project = self::getProjectById($projectId);
		
		if (!$project) {
			return false;
		}
		
		$invests = ProjectInvestments::where('project_id', $projectId)->limit($limit)->offset($offset)->orderBy('id', 'desc');
		
		if ($isAccept) {
			$invests->where('status', '>=', self::INVEST_STATUS_ACCEPT);
		} else {
			$invests->where('status', '>', 0);
		}
		
		return $invests->get();
	} 
	
	public static function getInvestNumByProjectId($projectId, $isAccept = 0) {
	    $project = self::getProjectById($projectId);
	    
	    if (!$project) {
	        return false;
	    }
	    
	    $invests = ProjectInvestments::where('project_id', $projectId);
	    
	    if ($isAccept) {
	        $invests->where('status', '>=', self::INVEST_STATUS_ACCEPT);
	    } else {
	        $invests->where('status', '>', 0);
	    }
	    
	    return $invests->count();
	}
	
	/**
	 * 获取排行榜
	 * @param int $num
	 *
	 * @return array|null 排行榜
	 */

	public static function getRanklist(int $num){

		return  Project::orderBy('create_index','desc')->limit($num)->get();

	}

	/**
	 * 根据状态获取列表
	 * @param int $status
	 * @param int $offset
	 * @param int $limit
	 *
	 * @return array|static
	 */
	public static function getListsByStatus($status, $offset, $limit){
		$project = Project::offset($offset)->limit($limit)->orderBy('id','desc');
		if($status){
			if ($status == 11) {
				$project->where('status' , '>' , 10);
			} else {
				$project->where('status',$status);
			}
		}else{
			$project->where('status','>',0);
		}
		return $project->get();
	}

	/**
	 * 根据标签获取列表
	 * @param int $tagId
	 * @param int $page
	 * @param int $limit
	 *
	 * @return array|static
	 */
	public static function getListsByTagId($tagId = 0, $status = 0, $offset = 0, $limit = 12){
		$projects = Project::offset($offset)->limit($limit)->orderBy('id','desc');
		
		if ($tagId) {
			$projectIds = array();
			
			$tagsAffairs = TagsAffairs::where('tag_id',$tagId)->where('affair_type',BaseModule::TYPE_PROJECT)->get();
	
			foreach($tagsAffairs as $tagsAffair){
				$projectIds[] = $tagsAffair->affair_id;
			}
			
			$projects->whereIn('id',$projectIds);
		}
				
		if ($status) {
			if ($status == 11) {
				$projects->where('status' , '>' , 10);
			} else {
				$projects->where('status',$status);
			}
		} else {
			$projects->where('status','>',0);
		}
		
		
		return $projects->get();
	}
	
	/**
	 * 获取某个标签和状态下的比赛总条数
	 * @param number $tagId
	 * @param number $status
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getTotalByTagIdAndStatus($tagId = 0,$status =0){
		$projects = Project::orderBy('id','desc');
		if ($tagId) {
			$projectIds = array();
				
			$tagsAffairs = TagsAffairs::where('tag_id',$tagId)->where('affair_type',BaseModule::TYPE_PROJECT)->get();
		
			foreach($tagsAffairs as $tagsAffair){
				$projectIds[] = $tagsAffair->affair_id;
			}
				
			$projects->whereIn('id',$projectIds);
		}
		
		if ($status) {
			if ($status == 11) {
				$projects->where('status' , '>' , 10);
			} else {
				$projects->where('status',$status);
			}
		} else {
			$projects->where('status','>',0);
		}
		
		return $projects->count();
		$result = $projects->get();
		return count($result);
	}


	/**
	 * 根据状态获取投资比赛投资者
	 *
	 * @param int $projectId
	 * @param int $status
	 * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\static, NULL>|Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getProjectInvestors($projectId, $status = array(self::INVEST_STATUS_ACCEPT, self::INVEST_STATUS_SUBMIT)) {
	    $investors = ProjectInvestments::where('project_id', $projectId)->where('user_id', '>', 0);
	    if(is_array($status)) {
	        $investors->whereIn('status', $status);
	    }elseif($status == self::INVEST_STATUS_SUBMIT) {
	        $investors->where('status', $status);
	    }elseif($status == self::INVEST_STATUS_ACCEPT) {
	        return $investors->where('status', '>=', $status)->first();
	    }
	    return $investors->get();
	}
	
	/**
	 * 支付定金
	 * @param int $investId
	 * @param int $userId
	 * @return boolean
	 */
	public static function payDeposit($investId, $userId) {
		$invest = ProjectInvestments::find($investId);
		
		if (!$invest || $invest->user_id != $userId || $invest->paid_money > 0) {
			return false;
		}
		
		$invest->paid_money = $invest->deposit_percentage * $invest->invest_money;
		$invest->status = self::INVEST_STATUS_DEPOSIT;		
		$invest->save();
		
		$project = self::getProjectById($invest->project_id);
		$project->status = self::STATUS_PAYED;
		$project->pay_time = time();
		$project->save();
		
		return true;
	}
	
	/**
	 * 支付尾款
	 * @param int $investId
	 * @param int $userId
	 * @return boolean
	 */
	public static function payFinal($investId, $userId) {
		$invest = ProjectInvestments::find($investId);
		
		if (!$invest || $invest->user_id != $userId) {
			return false;
		}
		
		$invest->paid_money = (100 - $invest->deposit_percentage) * $invest->invest_money;
		$invest->status = self::INVEST_STATUS_PAY_FINAL;
		$invest->save();
		
		$project = self::getProjectById($invest->project_id);
		$project->status = self::STATUS_PAYED;
		$project->pay_final_time = time();
		$project->save();
		
		return true;
	}
	
	/**
	 * 支付全款
	 * @param Int $investId
	 * @param int $userId
	 * @return boolean
	 */
	public static function payTotal($investId, $userId) {
		$invest = ProjectInvestments::find($investId);
		
		if (!$invest || $invest->user_id != $userId) {
			return false;
		}
		
		$invest->paid_money = $invest->invest_money;
		$invest->status = self::INVEST_STATUS_PAY_FINAL;
		$invest->save();
		
		$project = self::getProjectById($invest->project_id);
		$project->status = self::STATUS_PAYED;
		$project->pay_time = time();
		$project->save();
		
		return true;
	}
	
	/**
	 * 对投资的合法性进行验证
	 * @param int $investId
	 * @param int $userId
	 * @return $invest
	 */
	protected static function checkInvest($investId, $userId) {
		$invest = ProjectInvestments::find($investId);
        
        if (! $invest || $invest->user_id != $userId) {
            return false;
        }
        
        return $invest;
    }
    
    /**
     * 判断是否是洽谈投资者
     * 
     * @param int $id
     * @param int $userId
     * @return boolean
     */
    public static function isPlayer($id, $userId) {
        if($userId == 0) {
            return false;
        }
        return ProjectInvestments::where('project_id', $id)->where('user_id', $userId)->count()? true : false;
    }

    /**
     * 对投资信息进行排序
     * 
     * @param array $invests
     * @param int $projectId
     * @param bool $isPublisher
     * @param object $winner
     * @param int $userId
     */
    public static function sortInvestors(&$invests, $projectId, $isPublisher, $winner, $userId) {
        if ($isPublisher) {
            if (! empty($winner)) {
                foreach ($invests as $key => $invest) {
                    if ($invest->status == self::INVEST_STATUS_ACCEPT) {
                        if ($key != 0) {
                            $temp = $invest;
                            for ($i = $key - 1; $i >= 0; $i --) {
                                $invests[$i + 1] = $invests[$i];
                            }
                            $invests[0] = $temp;
                        }
                        break;
                    }
                }
            }
        } else {
            $isPlayer = self::isPlayer($projectId, $userId);
            if ($isPlayer) {
                $length = count($invests);
                foreach ($invests as $key => $invest) {
                    if ($invest->user_id != $userId) {
                        for ($i = $key + 1; $i < $length; $i ++) {
                            if ($invests[$i]->user_id == $userId) {
                                
                                $temp = $invest;
                                $invests[$key] = $invests[$i];
                                $invests[$i] = $temp;
                            }
                        }
                    }
                }
            }
        }
    }
    
    public static function fillProjects($projects) {

    }

    /**
	 * 处理到期
	 */
    public static function processExpiration() {
        $projects = Project::whereIn('status', array(self::STATUS_GOING, self::STATUS_CONFIRM_INVEST, self::STATUS_PAYED, self::STATUS_WAITING_CONFIRM_RETURN))->get();
        $nowTime = time();
        foreach ($projects as $project) {
            if ($project->status == self::STATUS_GOING) {
                if($project->investment_end_time) {
                    if ($project->investment_end_time <= $nowTime + 86400 * 3 && $project->investment_end_time > $nowTime + 86400 * 2) {
                        //比赛到期三天提醒
                        if(! isset($project->end_soon_remind) || $project->end_soon_remind == 0) {
                            \Event::fire('project.deadline-three', array($project->id, $project->user_id));
                            $project->end_soon_remind = 1;
                            $project->save();
                        }                   
                    } elseif ($project->investment_end_time <= $nowTime) {
                        //比赛关闭
                        $project->status = self::STATUS_END_FAIL;
                        $project->close_time = time();
                        $project->save();
                        \Event::fire('project.past-due', array($project->id, $project->user_id));
                    }
                }
            } else {
                $investment = self::getProjectInvestors($project->id, self::INVEST_STATUS_ACCEPT);
                if (! empty($investment)) {
                    if ($project->status == self::STATUS_CONFIRM_INVEST) {
                        if ($investment->type == self::INVEST_TYPE_TOTAL) {
                            //支付全款剩余24小时提醒
                            $payEndTime = $investment->accept_time + self::PAY_LIMIT_TIME * 86400;                          
                            if($payEndTime <= $nowTime + 86400 && $payEndTime > $nowTime) {
                                if(!isset($project->pay_final_remind) || $project->pay_final_remind == 0) {
                                    \Event::fire('project.invest-deadline',array($project->id, $project->user_id, $investment->user_id, 1));
                                    $project->pay_final_remind = 1;
                                    $project->save();
                                }
                            }        
                            //全款到期                   
                            if($payEndTime <= $nowTime) {
								$investment->status = self::INVEST_STATUS_PAY_FINAL;
								$investment->save();
								
								$project->status = self::STATUS_PAYED;
								$project->pay_time = time();
								$project->save();
                            }
                        } elseif ($investment->type == self::INVEST_TYPE_STAGES) {
                            $payEndTime = $investment->accept_time + self::PAY_LIMIT_TIME * 86400;
                            $finalPaymentTime = $investment->accept_time + $investment->final_payment_pay_time * 86400;
                            
                            //支付定金剩余24小时提醒                          
                            if($payEndTime <= $nowTime + 86400 && $payEndTime > $nowTime) {
                                if(!isset($project->pay_deposit_remind) || $project->pay_deposit_remind == 0) {
                                    \Event::fire('project.invest-deadline',array($project->id, $project->user_id, $investment->user_id, 2));
                                    $project->pay_deposit_remind = 1;
                                    $project->save();
                                }
                            }
                           
                            //支付尾款剩余24小时提醒
                            if ($finalPaymentTime <= $nowTime + 86400 && $finalPaymentTime > $nowTime) {
                                if(!isset($project->pay_final_remind) || $project->pay_final_remind == 0) {
                                    \Event::fire('project.invest-deadline', array($project->id, $project->user_id, $investment->user_id, 3));
                                    $project->pay_final_remind = 1;
                                    $project->save();
                                }
                            }
                            //定金到期
                            if($payEndTime <= $nowTime) {
								$investment->status = self::INVEST_STATUS_DEPOSIT;		
								$investment->save();
								
								$project->pay_time = time();
								$project->save();
                            }
                            //尾款到期
                            
                            if ($finalPaymentTime <= $nowTime) {
								$investment->status = self::INVEST_STATUS_PAY_FINAL;
								$investment->save();
								
								$project->status = self::STATUS_PAYED;
								$project->pay_final_time = time();
								$project->save();
                            }                           
                        }
                    }
                    //项目完工剩余24小时
                    $requireCompleteTime = 0;
                    if($investment->require_complete_time) {
                        if($investment->type == self::INVEST_TYPE_TOTAL) {
                            $requireCompleteTime = $investment->accept_time + self::PAY_LIMIT_TIME * 86400 + $investment->require_complete_time*86400;
                        }else{
                            $requireCompleteTime = $investment->accept_time + $investment->final_payment_pay_time * 86400 + $investment->require_complete_time*86400;
                        }
                    }
                    
                    if ($requireCompleteTime && $requireCompleteTime <= $nowTime + 86400 && $requireCompleteTime > $nowTime) {
                        if(! isset($project->finish_remind) || $project->finish_remind == 0) {
                            \Event::fire('project.invest-deadline', array($project->id, $project->user_id, $investment->user_id, 4));
                            $project->finish_remind = 1;
                            $project->save();
                        }
                    }
                    
                    if($project->status == self::STATUS_PAYED) {
                        if(! $requireCompleteTime || ($requireCompleteTime&& $requireCompleteTime <= $nowTime)) {
                            $project->status = self::STATUS_END_SUCCESS;
                            $project->complete_time = time();
                            $project->save();
                        }
                    }
                }
            }
        }	   
	}
	
	public static function getProjectInvestById($id) {
	    return ProjectInvestments::find($id);
	}

}