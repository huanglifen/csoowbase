<?php

namespace App\Modules;

use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
/**
 * 学校
 *
 * @package App\Modules
 */
class SchoolModule extends BaseModule {
	
	/**
	 * 获取学校信息
	 *
	 * @param unknown $id        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\Collection, \Illuminate\Database\Eloquent\static, NULL, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getSchoolInfoById($id) {
		return School::find ( intval ( $id ) );
	}
	
	/**
	 * 获取某地区下的学校
	 *
	 * @param int $id        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
	 */
	public static function getSchoolsById($id, $offset = 0, $limit = 10) {
		$id = intval ( $id );

		if (\Cache::has('schools_'.$id)){
			$value = \Cache::get('schools_'.$id);
		}else{
			if ($id == 0) {
				$model = new School ();
			}elseif (strlen ( $id ) == 1) {
				$model = School::where ( 'nation_id', $id );
			} else if (strlen ( $id ) == 2) {
				$model = School::where ( 'province_id', $id );
			} else if (strlen ( $id ) == 4) {
				$model = School::where ( 'city_id', $id );
			} else {
				$model = new School ();
			}

			$value =  $model->offset ( $offset )->limit ( $limit )->orderBy ( 'creative_index', 'desc' )->get ();
			\Cache::put('schools_'.$id, $value,Carbon::now()->addMinutes(10));
		}
		return $value;
	}
	
	/**
	 * 加入学校
	 *
	 * @param unknown $userId        	
	 * @param unknown $id        	
	 */
	public static function joinSchool($userId, $id) {

		$user = UserModule::getUserById($userId);
		$userOldArea = array('province_id' => 0, 'city_id' => 0, 'district_id' => 0,'school_id'   => $user['school_id'], 'organization_id' => 0);
		$user->school_id = $id;
		$user->save();

		$userNewArea = array('province_id' => 0, 'city_id' => 0, 'district_id' => 0,'school_id'   => $id, 'organization_id' => 0);
		AccountModule::updateCalInfo($user,$userOldArea,$userNewArea);

		\Event::fire ( 'user.area-profile-completed', array (
				$userId 
		) );
	}
	
	/**
	 * 更新学校的计数信息
	 *
	 * @param unknown $schoolId        	
	 * @param array $count        	
	 * @param string $isAdd        	
	 */
	public static function updateSchoolCount($schoolId, array $count, $isAdd = '+') {
		AreaModule::updateAreaCount ( AreaModule::TYPE_SCHOOL, $schoolId, $count, $isAdd );
	}
	
	/**
	 * 用户是否加入某学校
	 *
	 * @param int $userId        	
	 * @param int $id        	
	 * @return number
	 */
	public static function isJoined($userId, $id) {
		$count = User::whereRaw ( 'id = ? and school_id = ?', array (
				$userId,
				$id 
		) )->count ();
		return $count > 0 ? 1 : 0;
	}
	
	/**
	 * 根据地区信息获取学校
	 *
	 * @param int $pId        	
	 * @param int $cId        	
	 * @param int $dId        	
	 * @return Ambigous <\Illuminate\Database\Query\Builder, static, \Illuminate\Database\Query\static, \Illuminate\Database\Query\Builder>
	 */
	public static function getSchoolsByRegionId($id, $offset = 0, $limit = 24) {
		if (strlen ( $id ) == 1) {
			$data = new School ();
		} else if (strlen ( $id ) == 2) {
			$data = School::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = School::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = School::where ( 'district_id', $id );
		} else {
			return array ();
		}
		
		return $data->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}

	public static function getSchoolsByRegionIdCount($id) {
		if (strlen ( $id ) == 1) {
			$data = new School ();
		} else if (strlen ( $id ) == 2) {
			$data = School::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = School::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = School::where ( 'district_id', $id );
		} else {
			return 0;
		}

		return $data->count();
	}
	
}