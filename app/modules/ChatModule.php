<?php namespace App\Modules;

use App\Models\Chat;
use App\Models\Circle;
use App\Models\CircleMember;
/**
 * 好友聊天模块
 * Class ChatModule
 * @package App\Modules
 */
class ChatModule extends BaseModule {

	const STATUS_UNREAD = 0; //未读
	const STATUS_READED = 1; //已读
	const STATUS_READED_ON_LINE = 2; //在线读取

	const MESSAGE_PER_PAGE = 15; //每页显示聊天消息数
	const CHAT_PER_PAGE  = 20; //每页显示聊天记录数
	const CCZ_CHAT_PER_PAGE = 5;

	/**
	 * 发送一条聊天记录
	 *
	 * @param int $userId
	 * @param int $toUserId
	 * @param int $content
	 */
	public static function sendChat($userId, $toUserId, $content) {
	    $chat = new Chat();
	
	    $chat->user_id = $userId;
	    $chat->session_id = self::getSessionId($userId, $toUserId);
	    $chat->to_user_id = $toUserId;
	    $chat->content = $content;
	    $chat->status = self::STATUS_UNREAD;
	    $chat->create_time = time();
	
	    $chat->save();	
	    return $chat->id;
	}
	
	/**
	 * 获取聊天记录
	 * 
	 * @param int $userId
	 * @param int $toUserId
	 * @param int $topChatId
	 * @param int $offset
	 * @param int $limit
	 * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
	 */
	public static function getChats($userId, $toUserId, $topChatId = 0, $offset = 0, $limit = self::CHAT_PER_PAGE) {
	    $sessionId = self::getSessionId($userId, $toUserId);
	    $chat = Chat::where('session_id', $sessionId)->offset($offset)->limit($limit)->orderBy('id', 'desc');
	
	    if($topChatId) {
	        $chat->where('id', '<', $topChatId);
	    }
	
	    return $chat->get();
	}
	
	public static function getChatNum($userId, $toUserId) {
	    $sessionId = self::getSessionId($userId, $toUserId);
	    return Chat::where('session_id', $sessionId)->count();;
	}
	
	/**
	 * 获取未读消息条数
	 * 
	 * @param unknown $userId
	 * @return int
	 */
	public static function getUnreadChatNum($userId) {
	    return Chat::where('to_user_id', $userId)->where('status', self::STATUS_UNREAD)->distinct('user_id')->count('user_id');
	}
	
	/**
	 * 获取对每个用户的未读消息条数
	 * 
	 * @param int $userId
	 */
	public static function getUnreadChatByUser($userId, $offset = 0, $limit = self::CCZ_CHAT_PER_PAGE) {
	    return Chat::where('to_user_id', $userId)->where('status', self::STATUS_UNREAD)->groupby('user_id')->select(\DB::raw('count(*) as num, user_id'))->offset($offset)->limit($limit)->get();
	}
	
	/**
	 * 获取未读/已读的聊天消息
	 *
	 * @param int $userId
	 * @param int $offset
	 * @param int $limit
	 * @return array
	 */
	public static function getChatsByStatus($toUserId, $status = self::STATUS_UNREAD, $offset = 0, $limit = self::MESSAGE_PER_PAGE) {
	    return Chat::getChatsByStatus($toUserId, $status, $offset, $limit);
	}
	
	public static function getChatsNumByStatus($toUserId, $status = self::STATUS_UNREAD) {
	    $result =  Chat::where('to_user_id', $toUserId)->where('status', $status)->select(\DB::raw('count(distinct user_id) as num'))->get();
	    return $result[0]->num;
	}
	
	/**
	 * 更新用户对某个发送者的聊天消息为已读
	 *
	 * @param int $userId
	 * @param int $toUserId
	 * @return Ambigous <\App\Modules\Ambigous, number, \Illuminate\Database\mixed>
	 */
	public static function updateChatReaded($sendUserId, $toUserId) {
	    return Chat::where('user_id', $sendUserId)->where('to_user_id', $toUserId)->where('status', self::STATUS_UNREAD)->update(array('status' => self::STATUS_READED));
	}
	
	/**
	 * 获取当前聊天记录
	 * 
	 * @param int $userId
	 * @param int $toUserId
	 * @param number $lastChatId
	 * @return array $chats
	 */
	public static function getCurrentChat($userId, $toUserId, $lastChatId = 0) {
	    $chats = Chat::where('user_id', $userId)->where('to_user_id', $toUserId)->where('status', self::STATUS_UNREAD)->orderBy('id', 'desc');
	    
	    if($lastChatId) {
	        $chats->where('id', '>', $lastChatId);
	    }
	    
	    $updates = $chats;
	    $result = $chats->get();
	    
	    $updates->update(array('status'=> self::STATUS_READED_ON_LINE));
	    return $result;
	    
	   
	}
	
	/**
	 * 获取所有聊天联系人
	 * 
	 * @param int $userId
	 * @return string
	 */
	public static function getAllChaters($userId) {
	    $chaters = Chat::where('user_id', $userId)->orWhere('to_user_id', $userId)->orderBy('id', 'desc')->distinct()->get(array('user_id','to_user_id'));
	    $members = array();
	    foreach($chaters as $chater) {
	        if($chater->user_id != $userId) {
	            $members[] = $chater->user_id;
	        }
	        
	        if($chater->to_user_id != $userId) {
	            $members[] = $chater->to_user_id;
	        }
	    }
	    
	    $members = array_unique($members);
	    
	    return $members;
	}
	
	/**
	 * 获取所有聊天联系人和圈子成员
	 *
	 * @param int $userId
	 * @return string
	 */
	public static function getAllContacter($userId) {
	    $chaters = self::getAllChaters($userId);
	    
	    $members = CircleModule::getAllMembersByUserId($userId);

	    foreach($members as $member) {
	        if(! in_array($member->user_id, $chaters)) {
	            $chaters[] = $member->user_id;
	        }
	    }
	    
	    return $chaters;
	}
	
	/**
	 * 获取陌生人
	 * 
	 * @param unknown $userId
	 * @return array
	 */
	public static function getStranger($userId) { 
	    $chaters = self::getAllChaters($userId);
	    
	    $members = CircleModule::getAllMembersByUserId($userId);
	    $cMembers = array();
	    
	    foreach($members as $member) {
	        $cMembers[] = $member->user_id;
	    }
	    
	    $strangers = array();
	    foreach($chaters as $chater) {
	        if(! in_array($chater, $cMembers)) {
	            $strangers[] = $chater;
	        }
	    }
	    
	    return $strangers;
	}
	
	/**
	 * 填充联系人的最近一次消息内容
	 * 
	 * @param array $members
	 * @param int $userId
	 */
	public static function fillNotification(&$members, $userId) {
        $userIds = array();
        foreach ($members as $member) {
            $userIds[] = $member->id;
        }
        $userIds = array_unique($userIds);
        if (count($userIds)) {
            $chats = self::getLateChatByUserIds($userIds, $userId);
            foreach ($members as $member) {
                $member->chat = '';
                foreach ($chats as $key => $chat) {
                    if ($member->id == $chat->user_id) {
                        $member->chat = $chat;
                        unset($chats[$key]);
	                break;
	            }
	        }
	    }
	    }
	}
	
	/**
	 * 获取和一个或者多个用户的最新聊天消息内容
	 * 
	 * @param array $userIds
	 */
	public static function getLateChatByUserIds($userIds, $userId) {
	    if(count($userIds)) {
	        return Chat::getLateChatByUserIds($userIds, $userId);
	    }else{
	        return array();
	    }
	}
	/**
	 * 生成seesionId
	 * 
	 * @param int $userId
	 * @param int $toUserId
	 * @return string
	 */
	protected static function getSessionId($userId, $toUserId) {	    
	    return $userId < $toUserId ? $userId . '-' . $toUserId : $toUserId . '-' . $userId;
	}



}