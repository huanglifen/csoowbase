<?php


namespace App\Modules;

use App\Models\ExpertApplication;
use App\Models\ExpertInvitesExternal;
use App\Models\ExpertInvitesInternal;
use App\Models\User;
use App\Common\Utils;
use App\Models\ExpertComment;
use App\Models\IndustriesExperts;

/**
 * 智库(专家)模块
 * 申请加入智库、邀请专家点评、删除被邀请的站内专家等
 */
class ExpertModule extends BaseModule {
	/**
	 * 身份认证
	 *
	 * @param int $userId        	
	 * @param string $name        	
	 * @param string $identityCard        	
	 * @return int
	 */
	public static function identify($userId, $name, $identityCard) {
		$application = new ExpertApplication ();
		
		$application->user_id = $userId;
		$application->name = $name;
		$application->id_card_number = $identityCard;
		$application->industries = '';
		$application->enterprise_name = '';
		$application->enterprise_duty = '';
		$application->organization_id = 0;
		$application->duty_id = 0;
		$application->attachments = '';
		$application->explain = '';
		$application->create_time = time ();
		$application->save ();
		
		return $application->id;
	}
	
	/**
	 * 发送认证信息到第三方返回认证信息
	 */
	public static function sendCMS() {
	}
	
	/**
	 * 申请加入智库
	 *
	 * @param array $candidateInfo
	 *        	申请信息
	 * @return int
	 */
	public static function applyExpert($userId, $candidateInfo = array()) {
		$application = ExpertApplication::where ( 'user_id', $userId )->orderBy('id','desc')->first ();
		if (!$application) {
			return false;
		} else {
			$application->industries = $candidateInfo ['industries'];
			$application->enterprise_name = $candidateInfo ['enterprise_name'];
			$application->enterprise_duty = $candidateInfo ['enterprise_duty'];
			$application->organization_id = $candidateInfo ['organization_id'];
			$application->duty_id = $candidateInfo ['duty_id'];
			$application->attachments = $candidateInfo ['attachments'];
			$application->explain = $candidateInfo ['explain'];
			$application->save ();
			\Event::fire ( 'expert.apply', array (
					$application->id,
					$userId 
			) );
			
			return $application->id;
		}
	}
	
	/**
	 * 邀请站内专家
	 * 
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $targetSubId        	
	 * @param int $experUserId        	
	 *
	 * @return mixed
	 */
	public static function inviteInternalExpert($userId, $targetType, $targetId, $expertUserId, $targetSubId = 0) {
		$isExist = self::isExist ( $userId, $targetType, $targetId, $expertUserId, $targetSubId );
		$isCommented = self::isCommented($targetType, $targetId, $expertUserId,$targetSubId);
		if ($userId == $expertUserId || $isExist || $isCommented) {
			return 0;
		}
		
		$model = new ExpertInvitesInternal ();
		$model->user_id = $userId;
		$model->target_type = $targetType;
		$model->target_id = $targetId;
		$model->target_sub_id = $targetSubId;
		$model->create_time = time ();
		$model->expert_user_id = $expertUserId;
		$model->save ();
		
		\Event::fire ( 'expert.invite-expert', array (
				$userId,
				$expertUserId,
				$targetType,
				$targetId,
				$targetSubId 
		) );
		
		return $model->id;
	}
	
	/**
	 * 是否已经邀请
	 * 
	 * @param unknown $userId        	
	 * @param unknown $targetType        	
	 * @param unknown $targetId        	
	 * @param unknown $expertUserId        	
	 * @param number $targetSubId        	
	 * @return number
	 */
	public static function isExist($userId, $targetType, $targetId, $expertUserId, $targetSubId = 0) {
		$cnt = ExpertInvitesInternal::whereRaw ( 'user_id = ? and target_type = ? and target_id = ? and target_sub_id = ? and expert_user_id = ? ', array (
				$userId,
				$targetType,
				$targetId,
				$targetSubId,
				$expertUserId 
		) )->count ();
		return $cnt > 0 ? 1 : 0;
	}
	
	/**
	 * 是否已经点评
	 * @param unknown $targetType
	 * @param unknown $targetId
	 * @param unknown $expertUserId
	 * @param number $targetSubId
	 * @return number
	 */
	public static function isCommented( $targetType, $targetId, $expertUserId, $targetSubId = 0){
		$cnt  = ExpertComment::whereRaw ( 'target_type = ? and target_id = ? and target_sub_id = ? and user_id = ? ', array (
				$targetType,
				$targetId,
				$targetSubId,
				$expertUserId 
		) )->count ();
		return $cnt > 0 ? 1 : 0;
		
	}
	/**
	 * 删除被邀请的站内专家
	 * 
	 * @param
	 *        	$id
	 *        	
	 * @return bool null
	 */
	public static function deleteInternalExpert($id) {
		return ExpertInvitesInternal::destroy ( $id );
	}
	
	/**
	 * 邀请站外专家
	 * 
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $targetSubId        	
	 * @param array $expertInfo        	
	 *
	 * @return 主键ID
	 */
	public static function inviteExternalExpert($userId, $targetType, $targetId, array $expertInfo, $targetSubId = 0) {
		$model = new ExpertInvitesExternal ();
		$model->user_id = $userId;
		$model->target_type = $targetType;
		$model->target_id = $targetId;
		$model->target_sub_id = $targetSubId;
		$model->create_time = time ();
		$model->name = $expertInfo ['name'];
		$model->industry = $expertInfo ['industry'];
		$model->email = $expertInfo ['email'];
		$model->mobile_phone_no = $expertInfo ['mobile_phone_no'];
		$model->contact_info = ! empty ( $expertInfo ['contact_info'] ) ? $expertInfo ['contact_info'] : '';
		$model->save ();
		return $model->id;
	}
	
	/**
	 * 根据ID查找一个申请记录
	 *
	 * @param int $id        	
	 * @return object
	 */
	public static function searchApplicationByID($id) {
		return ExpertApplication::find ( $id );
	}
	
	/**
	 * 获取专家风采
	 *
	 * @param int $offset        	
	 * @param int $number        	
	 * @return array
	 */
	public static function getExpertsIntroduction($offset = 0, $number = 4) {
		$experts = User::where ( 'group_id', UserModule::GROUP_EXPERT )->orderBy ( 'id', 'desc' )->offset ( $offset )->limit ( $number )->get ();
		return $experts;
	}
	
	/**
	 * 根据条件筛选专家
	 *
	 * @param int $provinceId        	
	 * @param int $cityId        	
	 * @param int $districtId        	
	 * @param int $organizationId        	
	 * @param int $dutyId        	
	 * @param int $fieldId        	
	 * @param int $industryId        	
	 * @return array
	 */
	public static function getExpertsByCondition($provinceId = 0, $cityId = 0, $districtId = 0, $organizationId = 0, $dutyId = 0, $fieldId = 0, $industryId = 0, $offset = 0, $limit = self::USER_NUMBER_SIZE) {
		$experts = User::where ( 'group_id', UserModule::GROUP_EXPERT );
		if ($provinceId) {
			$experts->where ( 'province_id', $provinceId );
		}
		if ($cityId) {
			$experts->where ( 'city_id', $cityId );
		}
		if ($districtId) {
			$experts->where ( 'district_id', $districtId );
		}
		if ($organizationId) {
			$experts->where ( 'expert_organization_id', $organizationId );
		}
		if ($dutyId) {
			$experts->where ( 'expert_duty_id', $dutyId );
		}
		if ($industryId) {
			$industriesExperts = IndustriesExperts::where ( 'industry_id', $industryId )->get ();
			$userIds = array ();
			if (count ( $industriesExperts )) {
				foreach ( $industriesExperts as $industriesExpert ) {
					$userIds [] = $industriesExpert->user_id;
				}
				$experts = $experts->whereIn ( 'id', $userIds );
			} else {
				return array ();
			}
		}
		if ($fieldId) {
			$experts = $experts->where ( 'expert_field_id', $fieldId );
		}
		
		return $experts->offset ( $offset )->limit ( $limit )->get ();
	}
	
	/**
	 * 根据条件筛选专家总数
	 *
	 * @param int $provinceId        	
	 * @param int $cityId        	
	 * @param int $districtId        	
	 * @param int $organizationId        	
	 * @param int $dutyId        	
	 * @param int $fieldId        	
	 * @param int $industryId        	
	 * @return array
	 */
	public static function getExpertsByConditionCount($provinceId = 0, $cityId = 0, $districtId = 0, $organizationId = 0, $dutyId = 0, $fieldId = 0, $industryId = 0) {
		$experts = User::where ( 'group_id', UserModule::GROUP_EXPERT );
		if ($provinceId) {
			$experts->where ( 'province_id', $provinceId );
		}
		if ($cityId) {
			$experts->where ( 'city_id', $cityId );
		}
		if ($districtId) {
			$experts->where ( 'district_id', $districtId );
		}
		if ($organizationId) {
			$experts->where ( 'expert_organization_id', $organizationId );
		}
		if ($dutyId) {
			$experts->where ( 'expert_duty_id', $dutyId );
		}
		if ($industryId) {
			$industriesExperts = IndustriesExperts::where ( 'industry_id', $industryId )->get ();
			$userIds = array ();
			if (count ( $industriesExperts )) {
				foreach ( $industriesExperts as $industriesExpert ) {
					$userIds [] = $industriesExpert->user_id;
				}
				$experts = $experts->whereIn ( 'id', $userIds );
			} else {
				return 0;
			}
		}
		if ($fieldId) {
			$experts = $experts->where ( 'expert_field_id', $fieldId );
		}
		
		return $experts->count ();
	}
	
	/**
	 * 统计站内专家数
	 *
	 * @return int
	 */
	public static function countexperts() {
		return User::where ( 'group_id', UserModule::GROUP_EXPERT )->count ();
	}
	public static function getExperts($offset = 0, $limit = 40) {
		return User::where ( 'group_id', UserModule::GROUP_EXPERT )->offset ( $offset )->limit ( $limit )->get ();
	}
	/**
	 * 查看用户是否有申请专家的资格
	 *
	 * @param int $userId        	
	 * @return boolean
	 */
	public static function canApplyExpert($userId) {
		$user = User::find ( $userId );
		$creativeLevel = Utils::getCreativeLevel ( $user->creative_index );
		if ($user->mobile_phone_is_verified && $user->avatar && $creativeLevel >= 20) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 获取邀请某专家的用户
	 *
	 * @param int $expertUserId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param number $targetSubId        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getExpertInvitors($expertUserId, $targetType, $targetId = 0, $targetSubId = 0) {
		$invitors = ExpertInvitesInternal::where ( 'expert_user_id', $expertUserId )->where ( 'target_type', $targetType );
		if ($targetId) {
			$invitors->where ( 'target_id', $targetId );
		}
		if ($targetSubId) {
			$invitors->where ( 'target_sub_id', $targetSubId );
		}
		return $invitors->get ();
	}
	public static function getExpertComments($targetId, $targetType) {
		$expertComments = ExpertComment::where ( 'target_type', $targetType )->where ( 'target_id', $targetId )->get ();
		
		return $expertComments;
	}
	
	/**
	 * 根据专家id获取专家评论
	 *
	 * @param array $userIds        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return array
	 */
	public static function getExpertCommentsByUserId(array $userIds, $offset = 0, $limit = 6) {
		if ($userIds) {
			return ExpertComment::whereIn ( 'user_id', $userIds )->offset ( $offset )->limit ( $limit )->get ();
		} else {
			return array ();
		}
	}
	
	/**
	 * 随机获取专家
	 *
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return array
	 */
	public static function getExpertsByRandom($offset = 0, $limit = 14) {
		return User::where ( 'group_id', 2 )->offset ( $offset )->limit ( $limit )->orderBy ( 'creative_index', 'desc' )->get ();
	}
	public static function searchInternalExpert($name, $limit = 5) {
		return User::where ( 'group_id', 2 )->where ( 'name', 'like', "%$name%" )->limit ( $limit )->get ();
	}
	
	/**
	 * 02 * 验证身份证号
	 * 03 * @param $vStr
	 * 04 * @return bool
	 * 05
	 */
	public static function isCreditNo($vStr) {
		$vCity = array (
				'11',
				'12',
				'13',
				'14',
				'15',
				'21',
				'22',
				'23',
				'31',
				'32',
				'33',
				'34',
				'35',
				'36',
				'37',
				'41',
				'42',
				'43',
				'44',
				'45',
				'46',
				'50',
				'51',
				'52',
				'53',
				'54',
				'61',
				'62',
				'63',
				'64',
				'65',
				'71',
				'81',
				'82',
				'91' 
		);
		
		if (! preg_match ( '/^([\d]{17}[xX\d]|[\d]{15})$/', $vStr ))
			return false;
		
		if (! in_array ( substr ( $vStr, 0, 2 ), $vCity ))
			return false;
		
		$vStr = preg_replace ( '/[xX]$/i', 'a', $vStr );
		$vLength = strlen ( $vStr );
		
		if ($vLength == 18) {
			$vBirthday = substr ( $vStr, 6, 4 ) . '-' . substr ( $vStr, 10, 2 ) . '-' . substr ( $vStr, 12, 2 );
		} else {
			$vBirthday = '19' . substr ( $vStr, 6, 2 ) . '-' . substr ( $vStr, 8, 2 ) . '-' . substr ( $vStr, 10, 2 );
		}
		
		if (date ( 'Y-m-d', strtotime ( $vBirthday ) ) != $vBirthday)
			return false;
		if ($vLength == 18) {
			$vSum = 0;
			
			for($i = 17; $i >= 0; $i --) {
				$vSubStr = substr ( $vStr, 17 - $i, 1 );
				$vSum += (pow ( 2, $i ) % 11) * (($vSubStr == 'a') ? 10 : intval ( $vSubStr, 11 ));
			}
			
			if ($vSum % 11 != 1)
				return false;
		}
		
		return true;
	}
	
	/**
	 * 获取单个主行业下的专家
	 */
	public static function getIndustryExperts($industryId){
	    return \DB::select('select users.* from users join industries_experts on users.id = industries_experts.user_id where industries_experts.industry_id = '.$industryId.' order by creative_index desc limit 0,3');
	}
	
	public static function getSubIndustryExperts($subIndustryId,$limit = 3) {
	    return \DB::select('select users.* from users join industries_experts on users.id = industries_experts.user_id where industries_experts.industry_sub_id = '.$subIndustryId.' order by creative_index desc limit '.$limit);
	}
	
	/**
	 * 按group_id查询用户
	 * 
	 * @param int $groupId
	 * @param int $offset
	 * @param int $limit
	 * 
	 * @return array
	 */
	public static function getExpertApplications($offset, $limit) {
	    return ExpertApplication::where('user_id','>',0)->offset($offset)->limit($limit)->get();
	}
	
	/**
	 * 获取普通用户
	 * 
	 * @param int $offset
	 * @param int $limit
	 */
	public static function getNormalUsers($offset, $limit) {
	    return User::where('group_id',UserModule::GROUP_COMMON_USER)->offset($offset)->limit($limit)->get();
	}
	
	public static function getNormalUsersCount() {
	    return User::where('group_id',UserModule::GROUP_COMMON_USER)->count();
	}
	
	public static function getExpertsCount() {
	    return User::where('group_id',UserModule::GROUP_EXPERT)->count();
	}
}