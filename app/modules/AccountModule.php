<?php

	namespace App\Modules;

	use App\Models\Contest;
	use App\Models\User;
	use App\Models\AccountChangeLog;
	use App\Modules\RegionModule;
	use App\Models\School;
	use App\Models\Organization;
	use App\Modules\UserModule;

	/**
	 * 账号设置模块
	 *
	 * 用于处理用户创意指数,创创币,头像等功能
	 */
	class AccountModule extends BaseModule
	{
		const ACCOUNT_TYPE_COIN = 1; // 创创币
		const ACCOUNT_TYPE_CREATIVE = 2; // 创意指数
		const REASON_FOR_RECHARGE = 1; // 充值
		const REASON_FOR_CONVERT = 2; // 兑换
		const REASON_COIN_PRIZE = 3; //领取创创币奖励


		/**
		 * 通过旧密码修改用户密码
		 *
		 * @param string $currentPassword
		 * @param string $newPassword
		 * @param int    $userID
		 *
		 * @return boolean
		 */
		public static function updateUserPassword($currentPassword, $newPassword, $userID)
		{
			$user = User::find($userID);

			if ($user) {
				$user->password = self::hash($newPassword);
				$user->save();
			} else {
				return false;
			}
		}

		/**
		 * 修改用户基本信息
		 *
		 * @param array $userBasicInfo
		 * @param int   $userId
		 */
		public static function updateUserBasicInfo(array $userBasicInfo, $userId)
		{
			$user = User::find($userId);
			if ($user) {

				$userOldArea = array('province_id' => $user['province_id'], 'city_id' => $user['city_id'], 'district_id' => $user['district_id'],
				                     'school_id'   => $user['school_id'], 'organization_id' => $user['organization_id']);

				$userNewArea = array('province_id' => $userBasicInfo['province_id'], 'city_id' => $userBasicInfo['city_id'], 'district_id' => $userBasicInfo['district_id'],
				                     'school_id'   => $userBasicInfo['school_id'], 'organization_id' => $userBasicInfo['organization_id']);

				$birthday              = strtotime($userBasicInfo ['birthday']);
				$user->birthday        = $birthday;
				$user->gender          = $userBasicInfo ['gender'];
				$user->tags            = $userBasicInfo ['tags'];
				$user->introduction    = $userBasicInfo ['introduction'];
				$user->province_id     = $userBasicInfo ['province_id'];
				$user->city_id         = $userBasicInfo ['city_id'];
				$user->district_id     = $userBasicInfo ['district_id'];
				$user->school_id       = $userBasicInfo ['school_id'];
				$user->organization_id = $userBasicInfo ['organization_id'];
				$user->save();

				self::updateCalInfo($user, $userOldArea, $userNewArea);

				/*self::updateRegionByUserId ( $userId, 1 ,$userBasicInfo ['province_id'], $userBasicInfo ['city_id'], $userBasicInfo ['district_id'], 0, $user );

				self::updateSchoolByUserId ( $userId, $userBasicInfo ['school_id'], $user );

				self::updateOrganizationByUserId ( $userId, $userBasicInfo ['organization_id'], $user );

				// 更新比赛信息
				\Event::fire('user.join-region',array($userId,$userBasicInfo ['province_id'], $userBasicInfo ['city_id'], $userBasicInfo ['district_id']));
				\Event::fire ('user.join-school', array ($userId,$userBasicInfo ['school_id']) );
				\Event::fire('user.join-organization',array($userId,$userBasicInfo ['organization_id']));*/

				if (self::isAreaProfileCompleted($user->id)) {
					\Event::fire('user.area-profile-completed', array($user->id));
				}
				return true;
			} else {
				return false;
			}
	}

		/**
		 * 更新统计数据，账号设置，加入这里等使用
		 * @param $user
		 * @param $userOldArea
		 * @param $userNewArea
		 */
		public static function updateCalInfo($user, $userOldArea, $userNewArea)
		{
			if ($userOldArea['province_id'] == $userNewArea['province_id']) {
				$userOldArea['province_id'] = 0;
				$userNewArea['province_id'] = 0;
			}
			if ($userOldArea['city_id'] == $userNewArea['city_id']) {
				$userOldArea['city_id'] = 0;
				$userNewArea['city_id'] = 0;
			}
			if ($userOldArea['district_id'] == $userNewArea['district_id']) {
				$userOldArea['district_id'] = 0;
				$userNewArea['district_id'] = 0;
			}
			if ($userOldArea['school_id'] == $userNewArea['school_id']) {
				$userOldArea['school_id'] = 0;
				$userNewArea['school_id'] = 0;
			}
			if ($userOldArea['organization_id'] == $userNewArea['organization_id']) {
				$userOldArea['organization_id'] = 0;
				$userNewArea['organization_id'] = 0;
			}

			ContestModule::updateAreaInfo($user->id, $userNewArea ['province_id'], $userNewArea ['city_id'], $userNewArea ['district_id'], $userNewArea ['school_id'], $userNewArea ['organization_id']);
			ExchangeModule::updateAreaInfo($user->id, $userNewArea ['province_id'], $userNewArea ['city_id'], $userNewArea ['district_id'], $userNewArea ['school_id'], $userNewArea ['organization_id']);
			EncyclopediaModule::updateAreaInfo($user->id, $userNewArea ['province_id'], $userNewArea ['city_id'], $userNewArea ['district_id'], $userNewArea ['school_id'], $userNewArea ['organization_id']);

			AreaModule::calAreaCount(BaseModule::TYPE_CONTEST, $userNewArea ['province_id'], $userNewArea ['city_id'], $userNewArea ['district_id'], $userNewArea ['school_id'], $userNewArea ['organization_id']);
			AreaModule::calAreaCount(BaseModule::TYPE_EXCHANGE, $userNewArea ['province_id'], $userNewArea ['city_id'], $userNewArea ['district_id'], $userNewArea ['school_id'], $userNewArea ['organization_id']);
			AreaModule::calAreaCount(BaseModule::TYPE_USER, $userNewArea ['province_id'], $userNewArea ['city_id'], $userNewArea ['district_id'], $userNewArea ['school_id'], $userNewArea ['organization_id']);


			AreaModule::calAreaCount(BaseModule::TYPE_CONTEST, $userOldArea ['province_id'], $userOldArea ['city_id'], $userOldArea ['district_id'], $userOldArea ['school_id'], $userOldArea ['organization_id']);
			AreaModule::calAreaCount(BaseModule::TYPE_EXCHANGE, $userOldArea ['province_id'], $userOldArea ['city_id'], $userOldArea ['district_id'], $userOldArea ['school_id'], $userOldArea ['organization_id']);
			AreaModule::calAreaCount(BaseModule::TYPE_USER, $userOldArea ['province_id'], $userOldArea ['city_id'], $userOldArea ['district_id'], $userOldArea ['school_id'], $userOldArea ['organization_id']);

			CreativeIndexModule::addAreaCreativeIndex($user,$user->creative_index,$userNewArea);
			CreativeIndexModule::subAreaCreativeIndex($user,$userOldArea);
		}


		/**
		 * 修改用户头像
		 *
		 * @param int    $userId
		 * @param string $newUserAvatar
		 */
		public static function updateAvatar($userId, $newUserAvatar)
		{
			$user = User::find($userId);
			if ($user) {
				$user->avatar = $newUserAvatar;
				$user->save();
				return true;
			} else {
				return false;
			}
		}

		/**
		 * 用户领取未领取的创意指数
		 *
		 * @param int $userId
		 *
		 * @return bool
		 */
		public static function getUserCreativeIndexAward($userId)
		{
			$user = User::find($userId);

			if (!$user) {
				return false;
			}
			$amount = $user->creative_index_bonus_amount;
			$user->coin += $amount;
			$user->creative_index_bonus_amount = 0;
			$user->save();

			// Create coin event log
			if ($amount != 0) {
				self::createAccountChangeLog($userId, $amount, $user->coin, self::ACCOUNT_TYPE_COIN, self::REASON_FOR_CONVERT, '创意指数奖励');;
			}
			return true;
		}

		/**
		 * 创建用户账户流水记录
		 *
		 * @param int     $userID
		 * @param decimal $amount
		 * @param decimal $balance
		 * @param int     $type
		 * @param int     $reason
		 *
		 * @return int
		 */
		public static function createAccountChangeLog($userID, $amount, $balance, $type, $reason, $content = '', $createTime = 0)
		{
			$accountChangeLog = new AccountChangeLog ();

			$accountChangeLog->user_id      = $userID;
			$accountChangeLog->account_type = $type;
			$accountChangeLog->amount       = !empty($amount) ? $amount : 0;
			$accountChangeLog->balance      = $balance;
			$accountChangeLog->reason       = $reason;
			$accountChangeLog->related_info = !empty($content) ? $content : '';
			if (!$createTime) {
			    $accountChangeLog->create_time  = time();
			}else{
			    $accountChangeLog->create_time = $createTime;
			}

			$accountChangeLog->save();

			return $accountChangeLog->id;
		}

		/**
		 * 用户在创意世界网站活动获取创创币
		 *
		 * @param int    $userId
		 * @param int    $amount
		 * @param string $reason
		 *
		 * @return boolean
		 */
		public static function getCoinByEvent($userId, $amount, $reason, $content, $createTime = 0)
		{
			$user = User::find($userId);

			if (!$user) {
				return false;
			}
			$user->coin += $amount;
			$user->save();

			self::createAccountChangeLog($userId, $amount, $user->coin, self::ACCOUNT_TYPE_COIN, $reason, $content, $createTime);

			return true;
		}

		public static function getCreativeIndexByEvent($userId, $amount, $reason, $content)
		{
			$user = User::find($userId);

			if (!$user) {
				return false;
			}
			self::createAccountChangeLog($userId, $amount, $user->creative_index, self::ACCOUNT_TYPE_CREATIVE, $reason, $content);

			return true;
		}

		/**
		 * 根据userId查询账户变更记录
		 *
		 * @param int $userID
		 * @param int $startTime
		 * @param int $endTime
		 * @param int $eventNumber
		 * @param int $offsetNumber
		 * @param int $type
		 *            区别创创币记录,创意指数记录
		 * @param int $ways
		 *            区别增加指数,减少指数
		 *
		 * @return object
		 */
		public static function searchAccountLogByUserID($userID, $type, $ways = 0, $startTime = 0, $endTime = 0, $eventNumber = 10, $offsetNumber = 0)
		{
			$logs = AccountChangeLog::where('user_id', $userID)->where('account_type', $type)->offset($offsetNumber)->limit($eventNumber)->orderBy('create_time', 'desc');

			if ($startTime) {
				$logs->where('create_time', '>', $startTime);
			}

			if ($endTime) {
				$endTime += 86400;
				$logs->where('create_time', '<', $endTime);
			}

			if ($ways > 0) {
				$logs->where('amount', '>', 0);
			} else if ($ways < 0) {
				$logs->where('amount', '<', 0);
			}

			$accountLogInfo = $logs->get();
			return $accountLogInfo;
		}

		/**
		 * 统计流水记录条数
		 *
		 * @param int $userID
		 * @param int $startTime
		 * @param int $endTime
		 * @param int $type
		 * @param int $ways
		 *
		 * @return int
		 */
		public static function searchAccountLogByUserIDCount($userID, $startTime = 0, $endTime = 0, $type = 0, $ways = 0)
		{
			$logs = AccountChangeLog::where('user_id', $userID)->where('account_type', $type);

			if ($startTime) {
				$logs->where('create_time', '>', $startTime);
			}

			if ($endTime) {
				$endTime += 86400;
				$logs->where('create_time', '<', $endTime);
			}

			if ($ways > 0) {
				$logs->where('amount', '>', 0);
			} else if ($ways < 0) {
				$logs->where('amount', '<', 0);
			}

			return $logs->count();
		}

		/**
		 * 更新用户的关注数
		 * @param int $userId
		 * @param int $count
		 *
		 * @return Ambigous <number, \Illuminate\Database\mixed>
		 */
		public static function updateFollowerCount($userId, $count)
		{
			return User::where('id', $userId)->update(array(
				'follower_count' => $count
			));
		}

		/**
		 * 更新用户所属地区(省市县)的信息、加入地区
		 *
		 * @param int    $userId
		 * @param int    $provinceId
		 * @param int    $cityId
		 * @param int    $districtId
		 * @param number $newRegionId
		 * @param string $user
		 */
		public static function updateRegionByUserId($userId, $nationId, $provinceId, $cityId, $districtId, $newRegionId = 0, $user = null)
		{
			$user = (!$user) ? User::find($userId) : $user;

			// 获取老地区
			$oldNationId   = $user->nation_id;
			$oldProvinceId = $user->province_id;
			$oldCityId     = $user->city_id;
			$oldDistrictId = $user->district_id;

			// 更新用户的地区信息(省市县)
			$user->nation_id   = $nationId;
			$user->province_id = $provinceId;
			$user->city_id     = $cityId;
			$user->district_id = $districtId;
			$user->save();


			$count = self::getCountByUser($user);

			// 新地区计数增加
			$nationId ? RegionModule::updateRegionCount($nationId, $count) : '';
			$provinceId ? RegionModule::updateRegionCount($provinceId, $count) : '';
			$cityId ? RegionModule::updateRegionCount($cityId, $count) : '';
			$districtId ? RegionModule::updateRegionCount($districtId, $count) : '';

			// 老地区计数减少
			$oldNationId ? RegionModule::updateRegionCount($oldNationId, $count, '-') : '';
			$oldProvinceId ? RegionModule::updateRegionCount($oldProvinceId, $count, '-') : '';
			$oldCityId ? RegionModule::updateRegionCount($oldCityId, $count, '-') : '';
			$oldDistrictId ? RegionModule::updateRegionCount($oldDistrictId, $count, '-') : '';

		}

		/**
		 * 更新用户的学校信息
		 *
		 * @param int    $userId
		 * @param int    $newSchoolId
		 * @param string $user
		 */
		public static function updateSchoolByUserId($userId, $newSchoolId, $user = null)
		{
			$user = (!$user) ? User::find($userId) : $user;

			$oldSchoolId = $user->school_id;

			$user->school_id = $newSchoolId;
			$user->save();

			$count = self::getCountByUser($user);

			SchoolModule::updateSchoolCount($newSchoolId, $count);

			$oldSchoolId ? SchoolModule::updateSchoolCount($oldSchoolId, $count, '-') : '';

		}

		/**
		 * 更新用户的政企信息
		 *
		 * @param int    $userId
		 * @param int    $newOrganizationId
		 * @param string $user
		 */
		public static function updateOrganizationByUserId($userId, $newOrganizationId, $user = null)
		{
			$user = (!$user) ? User::find($userId) : $user;

			$oldOrganizationId = $user->organization_id;

			$user->organization_id = $newOrganizationId;
			$user->save();

			$count = self::getCountByUser($user);

			OrganizationModule::updateOrganizationCount($newOrganizationId, $count);

			$oldOrganizationId ? OrganizationModule::updateOrganizationCount($oldOrganizationId, $count, '-') : '';

		}

		/**
		 * 获取用户的计数信息
		 *
		 * @param unknown $user
		 *
		 * @return multitype:number NULL
		 */
		public static function getCountByUser($user)
		{
			$count                    = array();
			$count ['creative_index'] = $user->creative_index;
			$count ['contests_count'] = $user->contest_publish_count;
			$count ['trades_count']   = $user->trade_publish_count;
			$count ['experts_count']  = ($user->group_id == UserModule::GROUP_EXPERT) ? 1 : 0;
			return $count;
		}


		public static function listenEvent()
		{

			//发放奖励
			\Event::listen('notification.update-to-readed', function ($userId, $amount, $reason) {

				self::getCoinByEvent($userId, $amount, $reason);

			});

		}

		/**
		 * 检查用户地区信息是否完善
		 *
		 * @param int $userId
		 *
		 * @return boolean
		 */
		public static function isAreaProfileCompleted($userId)
		{
			$user = UserModule::getUserById($userId);

			$areaArray = ['10', '11', '18', '31'];

			if ((in_array($user->province_id, $areaArray) && $user->city_id) || ($user->province_id && $user->city_id && $user->district_id)) {
				$user->fill_only_area_profile = 1;
				$user->save();
			}

			if (in_array($user->province_id, $areaArray) && $user->city_id && $user->school_id && $user->organization_id) {
				return true;
			} elseif ($user->province_id && $user->city_id && $user->district_id && $user->school_id && $user->organization_id) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * 设置用户完善地区资料
		 *
		 * @param int $userId
		 */
		public static function fillAreaProfile($user)
		{
			$user->fill_area_profile = 1;
			$user->save();
		}

		/**
		 * 根据学校名查找学校
		 *
		 * @param string $name
		 *
		 * @return array
		 */
		public static function getSchoolsByName($name)
		{
			return School::where('name', 'like', $name . '%')->get();
		}

		/**
		 * 根据政企名查找学校
		 *
		 * @param string $name
		 *
		 * @return array
		 */
		public static function getOrganizationsByName($name)
		{
			return Organization::where('name', 'like', $name . '%')->get();
		}
		
		/**
		 * 保存邮箱验证码和验证时间
		 * @param unknown $userId
		 * @param unknown $verifyCode
		 */
		public static function modifyUserEmailCodeAndTime($userId, $verifyCode, $newEmail) {
		    
		    $user = UserModule::getUserById($userId);
		    
		    if ($user) {
		        $user->email_modify_code = $verifyCode;
		        $user->email_modify_time = time();
		        $user->new_modify_email = $newEmail;
		        $user->save();
		    }
		    
		    return true;
		}


		public static function modifyUserMobileCodeAndTime($userId, $verifyCode, $newMobile) {

			$user = UserModule::getUserById($userId);
			if ($user) {
				$user->mobile_modify_code = $verifyCode;
				$user->mobile_modify_time = time();
				$user->mobile_modify_new = $newMobile;
				$user->save();
			}

			return true;
		}
		
		/**
		 * 根据验证码修改用户邮箱
		 * @param mixed $user
		 * @param int $verifyCode
		 * @param string $newEmail
		 * @return boolean
		 */
		public static function verifyEmail($user, $verifyCode, $newEmail) {
		    if($user->email_modify_code == $verifyCode && time() <= $user->email_modify_time + 3600000) {
		        $user->email = $newEmail;
		        $user->email_modify_code = '';
		        $user->email_modify_time = '';
		        $user->new_modify_email = '';
		        $user->save();
		        return true;
		    }else{
		        return false;
		    }
		}

		public static function verifyMobile($user, $verifyCode, $newMobile) {
			if($user->mobile_modify_code == $verifyCode && time() <= $user->mobile_modify_time + 3600000) {
				$user->mobile_phone_no = $newMobile;
				$user->mobile_modify_code = '';
				$user->mobile_modify_time = '';
				$user->mobile_modify_new = '';
				$user->save();
				return true;
			}else{
				return false;
			}
		}

	}