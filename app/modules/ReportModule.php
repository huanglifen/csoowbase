<?php

namespace App\Modules;

use App\Models\Report;

/**
 * 举报模块
 */
class ReportModule extends BaseModule {
	
	// 举报类型，1 广告 2 违法 3 抄袭 4 色情 5 恶意灌水
	const TYPE_ADS = 1;
	const TYPE_ILLEGAL = 2;
	const TYPE_COPY = 3;
	const TYPE_SEX = 4;
	const TYPE_SPAM = 5;
	
	// 状态 0 待确认 1 不属实 2 属实
	const STATUS_WAITING = 0;
	const STATUS_NOT_SURE = 1;
	const STATUS_IS_SURE = 2;
	
	/**
	 * 保存举报
	 *
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $type
	 * @return ID
	 */
	public static function saveReport($userId, $targetType, $targetId, $type) {
		$model = new Report ();
		$model->user_id = $userId;
		$model->target_type = $targetType;
		$model->target_id = $targetId;
		$model->type = $type;
		$model->status = self::STATUS_WAITING;
		$model->create_time = time ();
		$model->cleared_user_id = 0;
		$model->cleared_creative_index = 0;
		$model->save ();
		return $model->id;
	}
	
	/**
	 * 是否已经举报
	 *
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 *
	 * @return bool
	 */
	public static function isExist($userId, $targetType, $targetId) {
		$cnt = Report::whereRaw ( 'user_id = ? and target_type = ? and target_id = ?', array (
				$userId,
				$targetType,
				$targetId 
		) )->count ();
		return $cnt > 0 ? true : false;
	}
	
	/**
	 * 举报属实，记录用户此时的创意指数
	 *
	 * @param int $reportId        	
	 * @param int $userId        	
	 * @param int $creativeIndex        	
	 *
	 * @return mixed
	 */
	public static function clearedCreativeIndex($reportId, $userId, $creativeIndex) {
		return Report::find ( $reportId )->update ( array (
				'cleared_user_id' => $userId,
				'cleared_creative_index' => $creativeIndex 
		) );
	}
}