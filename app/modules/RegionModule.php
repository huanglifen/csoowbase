<?php

namespace App\Modules;

use App\Models\Region;
use App\Models\User;
use Carbon\Carbon;
/**
 * 地区
 *
 * @package App\Modules
 */
class RegionModule extends BaseModule {
	const TYPE_NATION = 1;
	const TYPE_PROVINCE = 2;
	const TYPE_CITY = 3;
	const TYPE_DISTINCT = 4;
	public static function getRegionsByParentId($parentId = 0) {
		return Region::where ( 'province_id', $parentId )->get ();
	}
	/**
	 * 获取地区信息
	 *
	 * @param int $id        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\Collection, \Illuminate\Database\Eloquent\static, NULL, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getRegionInfoById($id) {
		return Region::find ( intval ( $id ) );
	}
	
	/**
	 * 获取某地区下的地区
	 *
	 * @param int $id        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
	 */
	public static function getRegionsById($id, $offset = 0, $limit = 10) {
		$id = intval ( $id );
		if (\Cache::has('regions_'.$id)){
			$value = \Cache::get('regions_'.$id);
		}else{
			if ($id == 0) {
				$model = Region::where ( 'nation_id', $id );
			}elseif (strlen ( $id ) == 1) {
				$model = Region::where ( 'nation_id', $id )->where('type',self::TYPE_PROVINCE);
			}else if (strlen ( $id ) == 2) {
				$model = Region::where ( 'province_id', $id )->where('type',self::TYPE_CITY);
			} else if (strlen ( $id ) == 4) {
				$model = Region::where ( 'city_id', $id )->where('type',self::TYPE_DISTINCT);
			} else {
				$model = new Region ();
			}
			$value = $model->offset ( $offset )->limit ( $limit )->orderBy ( 'creative_index', 'desc' )->get ();
			\Cache::put('regions_'.$id, $value,Carbon::now()->addMinutes(10));
		}
		return $value;
	}
	
	/**
	 * 加入地区
	 *
	 * @param int $userId        	
	 * @param int $regionId        	
	 */
	public static function joinRegion($userId, $regionId) {
		$regionId = intval ( $regionId );
		$nationId =  1;
		$provinceId = 0;
		$cityId = 0;
		$districtId = 0;
		if (strlen ( $regionId ) == 1) {
			$nationId = $regionId;
		}elseif (strlen ( $regionId ) == 2) {
			$provinceId = $regionId;
			$region = Region::find ( $regionId );
			$nationId = $region ['nation_id'];
		} else if (strlen ( $regionId ) == 4) {
			$cityId = $regionId;
			$region = Region::find ( $regionId );
			$nationId = $region ['nation_id'];
			$provinceId = $region ['province_id'];
		} else {
			$districtId = $regionId;
			$region = Region::find ( $regionId );
			$nationId = $region ['nation_id'];
			$provinceId = $region ['province_id'];
			$cityId = $region ['city_id'];
		}

		$user = UserModule::getUserById($userId);
		$userOldArea = array('province_id' => $user['province_id'], 'city_id' => $user['city_id'], 'district_id' => $user['district_id'],'school_id'   => 0, 'organization_id' => 0);
		$user->nation_id = $nationId;
		$user->province_id = $provinceId;
		$user->city_id = $cityId;
		$user->district_id = $districtId;
		$user->save();

		$userNewArea = array('province_id' => $provinceId, 'city_id' => $cityId, 'district_id' => $districtId,'school_id'   => 0, 'organization_id' => 0);
		AccountModule::updateCalInfo($user,$userOldArea,$userNewArea);

		
		\Event::fire ( 'user.area-profile-completed', array (
				$userId 
		) );
	}
	
	/**
	 * 更新地区的计数：创意指数、比赛数、交易数、专家数、用户数
	 *
	 * @param int $regionId        	
	 * @param array $count        	
	 * @param string $isAdd        	
	 */
	public static function updateRegionCount($regionId, array $count, $isAdd = '+') {
		AreaModule::updateAreaCount ( AreaModule::TYPE_REGION, $regionId, $count, $isAdd );
	}
	
	/**
	 * 用户是否已加入某地区
	 *
	 * @param int $userId        	
	 * @param int $id        	
	 * @return number
	 */
	public static function isJoined($userId, $id) {
		
		if (strlen ( $id ) == 1) {
			$user = User::where ( 'nation_id', $id );
		}else if (strlen ( $id ) == 2) {
			$user = User::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$user = User::where ( 'city_id', $id );
		} else if (strlen ( $id ) == 6) {
			$user = User::where ( 'district_id', $id );
		} else{
			return 0;
		}
		
		$count = $user->where('id',$userId)->count();
		return $count > 0 ? 1 : 0;
	}
	
	/**
	 * 根据地区信息获取地区
	 *
	 * @param int $pId        	
	 * @param int $cId        	
	 * @return Ambigous <\Illuminate\Database\Query\Builder, static, \Illuminate\Database\Query\static, \Illuminate\Database\Query\Builder>
	 */
	public static function getRegionsByRegionId($id,$offset = 0,$limit = 24) {
		
		if (strlen ( $id ) == 1) {
			$data = Region::where ( 'nation_id', $id )->where('province_id',0);
		}else if (strlen ( $id ) == 2) {
			$data = Region::where ( 'province_id', $id )->where('city_id',0);
		} else if (strlen ( $id ) == 4) {
			$data = Region::where ( 'city_id', $id );
		} else{
			return array();
		}
		
		return $data->offset($offset)->limit($limit)->orderBy('id','desc')->get();
	}

	public static function getRegionsByRegionIdCount($id) {

		if (strlen ( $id ) == 1) {
			$data = Region::where ( 'nation_id', $id )->where('province_id',0);
		}else if (strlen ( $id ) == 2) {
			$data = Region::where ( 'province_id', $id )->where('city_id',0);
		} else if (strlen ( $id ) == 4) {
			$data = Region::where ( 'city_id', $id );
		} else{
			return 0;
		}

		return $data->count();
	}
}