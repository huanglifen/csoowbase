<?php

namespace App\Modules;

use App\Models\Organization;
use App\Models\User;
use Carbon\Carbon;
/**
 * 政企
 *
 * @package App\Modules
 */
class OrganizationModule extends BaseModule {
	
	/**
	 * 获取政企信息
	 *
	 * @param unknown $id        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\Collection, \Illuminate\Database\Eloquent\static, NULL, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getOrganizationInfoById($id) {
		return Organization::find ( intval ( $id ) );
	}
	
	/**
	 * 获取某地区下的行政机构 企业
	 *
	 * @param int $id        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
	 */
	public static function getOrganizationsById($id, $type = Organization::TYPE_GOVERNMENT, $offset = 0, $limit = 10) {
		$id = intval ( $id );

		if (\Cache::has('organizations_'.$id.'_'.$type)){
			$value = \Cache::get('organizations_'.$id.'_'.$type);
		}else{
			if ($id == 0) {
				$model = new Organization ();
			}elseif (strlen ( $id ) == 1) {
				$model = Organization::where ( 'nation_id', $id );
			} else if (strlen ( $id ) == 2) {
				$model = Organization::where ( 'province_id', $id );
			} else if (strlen ( $id ) == 4) {
				$model = Organization::where ( 'city_id', $id );
			} else {
				$model = new Organization ();
			}

			$value =  $model->where ( 'type', $type )->offset ( $offset )->limit ( $limit )->orderBy ( 'creative_index', 'desc' )->get ();
			\Cache::put('organizations_'.$id.'_'.$type, $value,Carbon::now()->addMinutes(10));
		}
		return $value;
	}
	
	/**
	 * 用户是否加入某学校
	 *
	 * @param int $userId        	
	 * @param int $id        	
	 * @return number
	 */
	public static function isJoined($userId, $id) {
		$count = User::whereRaw ( 'id = ? and organization_id = ?', array (
				$userId,
				$id 
		) )->count ();
		return $count > 0 ? 1 : 0;
	}
	
	/**
	 * 加入政企
	 *
	 * @param int $userId        	
	 * @param int $id        	
	 */
	public static function joinOrganization($userId, $id) {
		$user = UserModule::getUserById ( $userId );
		$userOldArea = array (
				'province_id' => 0,
				'city_id' => 0,
				'district_id' => 0,
				'school_id' => 0,
				'organization_id' => $user ['organization_id'] 
		);
		$user->organization_id = $id;
		$user->save ();
		
		$userNewArea = array (
				'province_id' => 0,
				'city_id' => 0,
				'district_id' => 0,
				'school_id' => 0,
				'organization_id' => $id 
		);
		AccountModule::updateCalInfo ( $user, $userOldArea, $userNewArea );
		
		\Event::fire ( 'user.area-profile-completed', array (
				$userId 
		) );
	}
	
	/**
	 * 根据地区信息获取政企
	 *
	 * @param int $pId        	
	 * @param int $cId        	
	 * @param int $dId        	
	 * @return Ambigous <\Illuminate\Database\Query\Builder, static, \Illuminate\Database\Query\static, \Illuminate\Database\Query\Builder>
	 */
	public static function getOrganizationsByRegionId($id, $offset = 0, $limit = 24) {
		if (strlen ( $id ) == 1) {
			$data = new Organization ();
		} else if (strlen ( $id ) == 2) {
			$data = Organization::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = Organization::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = Organization::where ( 'district_id', $id );
		} else {
			return array ();
		}
		return $data->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}
	public static function getOrganizationsByRegionIdCount($id) {
		if (strlen ( $id ) == 1) {
			$data = new Organization ();
		} else if (strlen ( $id ) == 2) {
			$data = Organization::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = Organization::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = Organization::where ( 'district_id', $id );
		} else {
			return 0;
		}
		return $data->count ();
	}
	
	/**
	 * 更新地区的计数：创意指数、比赛数、交易数、专家数、用户数
	 *
	 * @param int $regionId        	
	 * @param array $count        	
	 * @param string $isAdd        	
	 */
	public static function updateOrganizationCount($regionId, array $count, $isAdd = '+') {
		AreaModule::updateAreaCount ( AreaModule::TYPE_ORGANIZATION, $regionId, $count, $isAdd );
	}
}