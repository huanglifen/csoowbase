<?php namespace App\Modules;

use App\Models\Guess;
use App\Models\Hiring;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use App\Modules\AccountModule;
class GuessModule extends BaseModule {

	const ITEM_FAIL = 0; //竞猜项：不成功
	const ITEM_SUCCESS = 1; //竞猜项：成功

	/**
	 * 参与竞猜
	 * @param int $userId
	 * @param int $targetType
	 * @param int $targetId
	 * @param int $item 竞猜项：0不成功，1成功
	 * @param int $amount 投入的创创币
	 *
	 * @return int 竞猜ID
	 */
	public static function playGuess($userId,$targetType,$targetId,$item,$amount){

		$model = new Guess();
		$model->user_id = $userId;
		$model->target_type = $targetType;
		$model->target_id = $targetId;
		$model->item = $item;
		$model->amount = $amount;
		$model->create_time = time();
		$model->save();

		if($targetType == BaseModule::TYPE_PROJECT){
			$data = Project::find($targetId);
		}elseif($targetType == BaseModule::TYPE_TASK){
			$data = Task::find($targetId);
		}elseif($targetType == BaseModule::TYPE_HIRING){
			$data = Hiring::find($targetId);
		}
		$data->guess_total_amount = $data->guess_total_amount + $amount;
		if($item == self::ITEM_SUCCESS){
			$data->guess_success_amount = $data->guess_success_amount + $amount;
		}
		$data->save();

		$user = User::find($userId);
		$user->coin = $user->coin-$amount;
		$user->save();
        AccountModule::createAccountChangeLog($userId, -$amount, $user->coin, AccountModule::ACCOUNT_TYPE_COIN, 1, '竞猜投注《'.$data->title.'》');
		return $model->id;

	}


	/**
	 * 预计获得的创创币
	 * @param int $targetType
	 * @param int $targetId
	 * @param int $item 竞猜项：0不成功，1成功
	 * @param int $amount 投入的创创币
	 *
	 * @return int 预计获得的创创币
	 */
	public static function expectAward($targetType,$targetId,$item,$amount){
		$data = '';
		if($targetType == BaseModule::TYPE_PROJECT){
			$data = Project::find($targetId);
		}elseif($targetType == BaseModule::TYPE_TASK){
			$data = Task::find($targetId);
		}elseif($targetType == BaseModule::TYPE_HIRING){
			$data = Hiring::find($targetId);
		}else{
			return 0;
		}
		
		if(!$data){
			return 0;
		}
		
		$total = $data->guess_total_amount;
		$success = $data->guess_success_amount;

		if($total == $success || $success == 0){
			return round($amount*1.01);
		}else{
			if($item == self::ITEM_SUCCESS){
				return round($amount * ($total/$success - 0.05));
			}else{
				return round($amount * ($total/($total-$success) - 0.05));
			}
		}
	}
    
	/**
	 * 根据竞猜比赛类型,ID和状态获取竞猜
	 * @param int $targetId
	 * @param int $targetType
	 * @param int $status
	 * @return array
	 */
	public static function getGuessByTargetIdAndTypeAndStatus($targetId, $targetType, $status) {
	    return Guess::where('target_id',$targetId)->where('target_type',$targetType)->where('item', $status)->get();
	}

}