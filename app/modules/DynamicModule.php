<?php namespace App\Modules;
use App\Models\Dynamic;
use Illuminate\Support\Facades\Event;
use App\Models\Region;

/**
 * 动态模块
 *
 * 用于处理相关动态,如发布动态,搜索动态接口
 */
class DynamicModule extends BaseModule {

    const TYPE_ORIGINAL = 1; //原创
    const TYPE_FORWARD = 2; //转发

    const TYPE_FOLLOWER = 11;
    const TYPE_PUBLISH = 12;
    const TYPE_APPLY = 13;
    const TYPE_PUBLISH_UPDATE = 14;
    const TYPE_COMMENT = 15;
    const TYPE_LIKE = 16;
    const TYPE_EXPERT_COMMENT = 21;
	const TYPE_BECOME = 22;
    const TYPE_JOIN = 23;
   

    const DYNAMICS_PER_PAGE = 30;
    const DYNAMIC_COMMENTS_PER_PAGE = 10;
    
    /**
     * 发布动态
     * 
     * @param int $userId 用户id
     * @param string $content 动态内容
     * @param string $pic 动态图片
     * @param string $video 动态视频
     * @return int 返回用户动态id
     */
    public static function createDynamic($userId, $content = '', $pic = '', $video = '', $type = self::TYPE_ORIGINAL, $target_type = 0, $target_id = 0) {
        $dynamic = new Dynamic();
        
        $dynamic->user_id = $userId;
        $dynamic->content = $content;
        $dynamic->pic = $pic;
        $dynamic->video = $video;
        $dynamic->type = $type;       
        $dynamic->like_count = 0;
        $dynamic->forward_count = 0;
        $dynamic->comment_count = 0;
        $dynamic->target_type = $target_type;
        $dynamic->target_id = $target_id;
        $dynamic->create_time = time();
        
        $dynamic->save();
        
        return $dynamic;
    }

	/**
	 * 保存行为动态
	 * @param int    $userId
	 * @param int    $type
	 * @param int    $targetType
	 * @param int    $targetId
	 * @param string $content
	 */
	public static function saveActionDynamic($userId, $type, $targetType, $targetId){
		return self::createDynamic($userId,'','','',$type,$targetType,$targetId);
	}
	
	/**
	 * 获取一条动态
	 * 
	 * @param int $id
	 * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\Collection, \Illuminate\Database\Eloquent\static, NULL, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getDynamicById($id) {
	    return Dynamic::find($id);
	}

    /**
     * 创建一条转发
     * 
     * @param int $userId
     * @param int $fowardId
     * @return boolean
     */
    public static function createForward($userId, $fowardId, $content) {
        $dynamic = Dynamic::find($fowardId);
        if(empty($dynamic)) {
            return false;
        }
        
        $result = self::createForwardDynamic($userId, $content, $fowardId);
        
        if ($result) {
            $dynamic->forward_count ++;            
            $dynamic->save();
            \Event::fire('dynamic.dynamic-forward', array($dynamic->id, $dynamic->user_id));
        }
        return $result;
    }
    
    private static function createForwardDynamic($userId, $content, $forwardId) {
        return self::createDynamic($userId, $content, '', '', self::TYPE_FORWARD, 0, $forwardId);
    }
    
    /**
     * 创建一条动态评论
     * 
     * @param int $userId
     * @param int $targetId
     * @param int $content
     * @return int commentId
     */
    public static function createDynamicComment($userId, $targetId, $content) {
        $dynamic = Dynamic::find($targetId);
        if(empty($dynamic)) {
            return false;
        }
        
        $result = CommentModule::publishComment($userId, BaseModule::TYPE_DYNAMIC, $targetId, $content);
        
        if ($result) {
            $dynamic->comment_count ++;       
            $dynamic->save();
        }
        return $result;
    }
    
    /**
     * 赞一条动态
     * 
     * @param int $userId
     * @param int $targetId
     * @return \App\Modules\ID
     */
    public static function saveLike($userId, $targetId) {
        $actionType = ItemModule::ACTION_TYPE_LIKE;
        $targetType = BaseModule::TYPE_DYNAMIC;
        $count = ItemModule::isExist($userId, $actionType, $targetType, $targetId);
        if( $count) {
           return false; 
        }
        $dynamic = Dynamic::find($targetId);
        
        if(empty($dynamic)) {
            return false;
        }
           
        $title = '';
        $result = ItemModule::saveItem($userId, $actionType, $targetType, $targetId, $title);
        
        if ($result) {
            $dynamic->like_count ++;        
            $dynamic->save();
        }
        return $result;
    }
    
    /**
     * 获取一个或者多个用户发布的动态
     * 
     * @param int $userIds
     * @param int $offset
     * @param int $limit
     * @param int $type 0全部 1原创 2转发
     * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
     */
    public static function getDynamcicsByUserIds($userIds, $offset = 0, $limit = self::DYNAMICS_PER_PAGE,$type = 0) {
        if (is_array($userIds)) {
            $dynamics = Dynamic::whereIn('user_id', $userIds);
        } else {
            $dynamics = Dynamic::where('user_id', $userIds);
        }
	    if($type){
		    $dynamics->where('type',$type);
	    }
        return $dynamics->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
    }
    
    /**
     * 获取官方账号微博
     * 
     * @param number $offset
     * @param number $limit
     * @return array
     */
    public static function getOfficialDynamics($offset = 0, $limit = 10) {
        $officials = UserModule::getOfficialAccounts();
        
        $userIds = array();
        foreach($officials as $official) {
            $userIds[] = $official->id;
        }
        if(count($userIds)) {
           return self::getDynamcicsByUserIds($userIds, $offset, $limit);
        }
        
        return array();
    }
    
    /**
     * 获取用户组动态数
     * 
     * @param array $userIds
     * @param number $type
     * @return number
     */
    public static function getDynamicsByUserIdsNum($userIds, $type = 0) {
        if (is_array($userIds)) {
            $dynamics = Dynamic::whereIn('user_id', $userIds);
        } else {
            $dynamics = Dynamic::where('user_id', $userIds);
        }
        if($type){
            $dynamics->where('type',$type);
        }
        return $dynamics->count();
    }
    
    /**
     * 填充转发内容
     * 
     * @param array $dynamics
     */
    public static function fillForwards(&$dynamics) {
        $forwardIds = array();
        $forwardDynamics = array();
        foreach($dynamics as $dynamic) {
            if($dynamic->type == self::TYPE_FORWARD) {
                $forwardIds[] = $dynamic->target_id;
                $forwardDynamics[] = $dynamic;
            }
        }
        $forwardIds = array_unique($forwardIds);
        if(count($forwardIds)) {
            $forwards = Dynamic::findMany($forwardIds);
            UserModule::fillUsers($forwards);
            foreach($forwardDynamics as $forwardDynamic) {
                foreach($forwards as $forward) {
                    if($forwardDynamic->target_id == $forward->id) {
                        
                        $forwardDynamic->forward = $forward;
                        break;
                    }
                }
                
            }
            
        }
    }
    
    /**
     * 填充当前用户赞情况
     * @param unknown $dynamics
     */
    public static function fillLikeInfo ($dynamics, $userId)
    {
        if ($userId == 0) {
            foreach ($dynamics as $dynamic) {
                $dynamic->like = 0;
            }
            return true;
        }
        
        $targetIds = array();
        foreach ($dynamics as $dynamic) {
            $targetIds[] = $dynamic->id;
        }
        $targetIds = array_unique($targetIds);
        
        if (count($targetIds)) {
            $actionType = ItemModule::ACTION_TYPE_LIKE;
            $targetType = BaseModule::TYPE_DYNAMIC;
            $likes = ItemModule::getItemByTargetIds($userId, $actionType, 
                    $targetType, $targetIds);
            
            foreach ($dynamics as $dynamic) {
                $dynamic->like = 0;
                foreach ($likes as $like) {
                    if ($dynamic->id == $like->target_id) {
                        $dynamic->like = 1;
                        break;
                    }
                }
            }
        }      
    }
    
    /**
     * 获取动态评论
     * 
     * @param int $targetId
     * @param int $page
     * @param int $limit
     * @return Ambigous <\App\Modules\评论信息, multitype:, multitype:\Illuminate\Database\Query\static , array>
     */
    public static function getDynamicComments($targetId, $offset = 0, $limit = self::DYNAMIC_COMMENTS_PER_PAGE) {
        return CommentModule::getComments(BaseModule::TYPE_DYNAMIC, $targetId, $offset, $limit);
    }


	/**
	 * 事件监听:录入行为动态
	 */
	public static function listenEvent(){

		//用户A关注用户B
		Event::listen('circle.add-circle-member', function($ownerUserId,$userId) {
			self::saveActionDynamic($ownerUserId, self::TYPE_FOLLOWER, BaseModule::TYPE_CIRCLE_ME, $userId);
		});


		//发布了投资项目比赛
		Event::listen('project.publish', function($userId,$projectId,$title) {
			self::saveActionDynamic($userId, self::TYPE_PUBLISH, BaseModule::TYPE_PROJECT, $projectId);
		});

		//参与投资项目
		Event::listen('project.invest', function($userId,$projectId) {
			self::saveActionDynamic($userId, self::TYPE_APPLY, BaseModule::TYPE_PROJECT, $projectId);
		});

		//更新投资项目比赛
		Event::listen('project.update', function($userId,$projectId) {
			self::saveActionDynamic($userId, self::TYPE_PUBLISH_UPDATE, BaseModule::TYPE_PROJECT, $projectId);
		});


		//发布任务人才
		Event::listen('task.publish', function($userId,$taskId,$title) {
			self::saveActionDynamic($userId, self::TYPE_PUBLISH, BaseModule::TYPE_TASK, $taskId);
		});

		//任务人才提交作品
		Event::listen('task.submit-work', function($userId,$taskId) {
			self::saveActionDynamic($userId, self::TYPE_APPLY, BaseModule::TYPE_TASK, $taskId);
		});

		//更新任务人才比赛
		Event::listen('task.update', function($userId,$taskId) {
			self::saveActionDynamic($userId, self::TYPE_PUBLISH_UPDATE, BaseModule::TYPE_TASK, $taskId);
		});


		//发布公益明星比赛
		Event::listen('hiring.publish', function($userId,$hiringId,$title) {
			self::saveActionDynamic($userId, self::TYPE_PUBLISH, BaseModule::TYPE_HIRING, $hiringId);
		});

		//公益明星提交作品
		Event::listen('hiring.apply', function($userId,$hiringId) {
			self::saveActionDynamic($userId, self::TYPE_APPLY, BaseModule::TYPE_HIRING, $hiringId);
		});

		//更新了公益明星比赛
		Event::listen('hiring.update', function($userId,$hiringId) {
			self::saveActionDynamic($userId, self::TYPE_PUBLISH_UPDATE, BaseModule::TYPE_HIRING, $hiringId);
		});


		//发布交易所交易
		Event::listen('trade.publish', function($userId,$tradeId,$title) {
			self::saveActionDynamic($userId, self::TYPE_PUBLISH, BaseModule::TYPE_EXCHANGE, $tradeId);
		});


		//分享创意(百科)
		Event::listen('encyclopedia.share', function($userId,$encyclopediaId,$title) {
			self::saveActionDynamic($userId, self::TYPE_PUBLISH, BaseModule::TYPE_ENCYCLOPEDIA, $encyclopediaId);
		});

		//评论了百科
		Event::listen('encyclopedia.publish-comment', function($userId,$encyclopediaId) {
			self::saveActionDynamic($userId, self::TYPE_COMMENT, BaseModule::TYPE_ENCYCLOPEDIA, $encyclopediaId);
		});

		//喜欢了比赛或交易或作品
		Event::listen('like.save', function($userId,$targetType,$targetId) {
			
			$likeArray = array(BaseModule::TYPE_PROJECT,BaseModule::TYPE_TASK,BaseModule::TYPE_TASK,BaseModule::TYPE_HIRING,BaseModule::TYPE_HIRING_WORK,BaseModule::TYPE_EXCHANGE,BaseModule::TYPE_ENCYCLOPEDIA);
			if(in_array($targetType,$likeArray)){
				self::saveActionDynamic($userId, self::TYPE_LIKE, $targetType, $targetId);
			}
			
		});
			

		//评论了比赛或交易
		Event::listen('comment.publish', function($userId,$targetType,$targetId) {
			self::saveActionDynamic($userId, self::TYPE_COMMENT, $targetType, $targetId);
		});


		//专家点评了比赛或交易
		Event::listen('expert-comment.publish', function($userId,$targetType,$targetId) {
			self::saveActionDynamic($userId, self::TYPE_EXPERT_COMMENT, $targetType, $targetId);
		});

		
		
		//用户A加入某地区，学校，政企 ......
		Event::listen('area.join-region', function($userId,$targetType,$targetId) {
			self::saveActionDynamic($userId, self::TYPE_JOIN, $targetType, $targetId);
		});
		
		Event::listen('area.join-school', function($userId,$targetType,$targetId) {
			self::saveActionDynamic($userId, self::TYPE_JOIN, $targetType, $targetId);
		});
		
		Event::listen('area.join-organiztion', function($userId,$targetType,$targetId) {
			self::saveActionDynamic($userId, self::TYPE_JOIN, $targetType, $targetId);
		});
		
		//用户A成为了创意世界智库专家 ......

	}
	
	/**
	 * 填充行为动态内容
	 * 
	 * @param array $dynamics
	 */
	public static function fillActionContent(&$dynamics) {
	    foreach($dynamics as $dynamic) {
	        if($dynamic->target_id != 0 && ! in_array($dynamic->type, array(self::TYPE_ORIGINAL,self::TYPE_FORWARD))) { 
	            $dynamic->content = self::getActionDynamicContent($dynamic->user_id, $dynamic->type, $dynamic->target_type, $dynamic->target_id);
	        }
	    }
	    
	}

	/**
	 * 生成行为动态的内容
	 * @param int $userId
	 * @param int $type
	 * @param int $targetType
	 * @param int $targetId
	 *
	 * @return string
	 */
	public static function getActionDynamicContent($userId,$type,$targetType,$targetId){
		$action = '';
		switch($type){
			case self::TYPE_FOLLOWER : $action='关注了';break;
			case self::TYPE_PUBLISH : $action='发布了';break;
			case self::TYPE_APPLY : $action='参与了';break;
			case self::TYPE_PUBLISH_UPDATE : $action='发布了';break;
			case self::TYPE_COMMENT : $action='评论了';break;
			case self::TYPE_EXPERT_COMMENT : $action='点评了';break;
			case self::TYPE_BECOME : $action= '成为了';break;
			case self::TYPE_JOIN : $action='加入了';break;
			case self::TYPE_LIKE: $action = '喜欢了';break;
			default:;
		}

		$content = '';
		switch($targetType){
		    case BaseModule::TYPE_CIRCLE_ME:
		        $model = UserModule::getUserById($targetId);
		        $content = $action.'<a class="n" href="'.\URL::to("/").'/user/home/'.$targetId.'">'.$model->name.'</a>';
		        break;
			case BaseModule::TYPE_PROJECT:
				$model = ProjectModule::getProjectById($targetId);
				$content = $action.'<a class="n" href="'.\URL::to("/").'/project/detail/'.$targetId.'">《'.$model->title.'》</a>';
				break;
			case BaseModule::TYPE_TASK:
				$model = TaskModule::getTaskById($targetId);
				$content = $action.'<a class="n" href="'.\URL::to("/").'/task/detail/'.$targetId.'">《'.$model->title.'》</a>';
				break;
			case BaseModule::TYPE_HIRING:
			    $model = HiringModule::getHiringById($targetId);
			    $content = $action.'<a class="n" href="'.\URL::to("/").'/hiring/detail/'.$targetId.'">《'.$model->title.'》</a>';
			    break;
			case BaseModule::TYPE_EXCHANGE:
			    $model = ExchangeModule::getTradeById($targetId);
			    $content = $action.'<a class="n" href="'.\URL::to("/").'/exchange/detail/'.$targetId.'">《'.$model->title.'》</a>';
			    break;
			case BaseModule::TYPE_TASK_WORK:
			    $work = TaskModule::getTaskWorkById($targetId);
			    $targetUser = UserModule::getUserById($work->user_id);
			    $model = TaskModule::getTaskById($work->task_id);
			    $content = $action.$targetUser->name.'在《'.$model->title.'》提交的<a class="n" href="'.\URL::to("/").'/task/work/'.$targetId.'">作品</a>';
			    break;
			case BaseModule::TYPE_HIRING_WORK:
			    $work = HiringModule::getPaticipatorById($targetId);
			    $targetUser = UserModule::getUserById($work->user_id);
			    $model = HiringModule::getHiringById($work->task_id);
			    $content = $action.$targetUser->name.'在《'.$model->title.'》提交的<a class="n" href="'.\URL::to("/").'/hiring/work/'.$targetId.'">作品</a>';
			    break;
			case BaseModule::TYPE_ENCYCLOPEDIA:
			    $model = EncyclopediaModule::getEncyclopediaById($targetId);
			    $content = $action.'<a class="n" href="'.\URL::to("/").'/encyclopedia/detail/'.$targetId.'">《'.$model->title.'》</a>';
			    break;
			case BaseModule::TYPE_EXPERT:
			    $content = $action.'<a class="n" href="'.\URL::to("/").'/thinktank">创意世界智库专家</a>';
			    break;
			case BaseModule::TYPE_REGION:
			    $model = RegionModule::getRegionsById($targetId);
			    $content = $action.'<a class="n" href="'.\URL::to("/").'/area/region">'.$model->name.'</a>';
			    break;			    
			case BaseModule::TYPE_SCHOOL:
			    $model = SchoolModule::getSchoolInfoById($targetId);
			    $content = $action.'<a class="n" href="'.\URL::to("/").'/area/school">'.$model->name.'</a>';
			    break;			    
		    case BaseModule::TYPE_ORGANIZATION:
		        $model = OrganizationModule::getOrganizationsById($targetId);
		        $content = $action.'<a class="n" href="'.\URL::to("/").'/area/Organization">'.$model->name.'</a>';
		        break;		 
			default:;
		}

		if($type == self::TYPE_PUBLISH_UPDATE){
			$content = $content.'动态';
		}
		return $content;
	}

}
