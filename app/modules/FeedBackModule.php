<?php namespace App\Modules;
use App\Models\FeedBack;

/**
 * 意见反馈模块
 * 
 * 用于用户提交反馈意见
 */
class FeedBackModule extends BaseModule {
    
    /**
     * 提交反馈意见
     * 
     * @param int $userId 用户id
     * @param string $content 反馈内容
     * @return int 反馈意见ID
     */
    public static function createFeedback($userId, $content,$url) {
        
        $feedBack = New FeedBack();
        $feedBack->user_id = $userId;
        $feedBack->content = $content;
	    $feedBack->url = $url;
        $feedBack->create_time = time();
        
        $feedBack->save();
        return $feedBack->id;
    }
}
