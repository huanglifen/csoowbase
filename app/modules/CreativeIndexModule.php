<?php

namespace App\Modules;

use Illuminate\Support\Facades\Event;
use App\Models\Project;
use App\Models\TaskWork;
use App\Models\HiringParticipators;
use App\Models\ExchangeTrade;
use App\Models\Encyclopedia;
use App\Models\Task;
use App\Models\Hiring;
use App\Models\User;
use App\Models\Region;
use App\Models\School;
use App\Models\Organization;
use App\Common\Utils;
use App\Modules\AccountModule;

class CreativeIndexModule extends BaseModule {
	public static function listenEvent() {
		
		// 评分
		Event::listen ( 'creative-value-rating.publish', function ($targetType, $targetId, $score) {
			
			$userId = self::addCreativeIndex ($targetType, $targetId, $score );
			AccountModule::getCreativeIndexByEvent($userId, $score, 1, '获得创意价值投票');
			
		} );
		
		// 专家点评
		Event::listen ('expert-comment.publish', function($targetType, $targetId,$targetSubId,$score) {
			$userId = self::addCreativeIndex($targetType, $targetId, $score,$targetSubId);
			AccountModule::getCreativeIndexByEvent($userId, $score, 2, '收到专家点评');
		});
		
		// 确认举报时惩罚......
		/*
		 * Event::listen ( 'report.sure', function ($userId, $targetType, $targetId,$reportId) { self::subCreativeIndex ( $userId, $targetType, $targetId,$reportId ); } );
		 */
	}
	
	/**
	 * 增加创创币
	 *
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $score        	
	 */
	private static function addCreativeIndex($targetType, $targetId, $score,$targetSubId = 0) {
		
		// 增加基础的创创币
		$data = self::addBaseCreativeIndex ( $targetType, $targetId, $score,$targetSubId);
		
		// 增加比赛的创创币
		self::addContestCreativeIndex ( $targetType, $data, $score );
		
		// 增加用户的创创币
		$user = self::addUserCreativeIndex ( $data->user_id, $score );
		
		// 增加地区学校政企的创创币
		$userNewArea  = array('province_id' => $user->province_id, 'city_id' => $user->city_id, 'district_id' => $user->district_id, 'school_id'   => $user->school_id, 'organization_id' => $user->organization_id);

		self::addAreaCreativeIndex ( $user, $score,$userNewArea);
		
		return $data->user_id;
	}
	
	/**
	 * 增加基础的创创币：1 创意投资项目比赛 21 创意任务人才比赛作品 31 创意公益明星比赛参赛明星作品 4 创意世界交易所 5 创意百科
	 *
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $score        	
	 */
	private static function addBaseCreativeIndex($targetType, $targetId, $score,$targetSubId = 0) {
		switch ($targetType) {
			
			case BaseModule::TYPE_PROJECT :
				$model = new Project ();
				break;
			case BaseModule::TYPE_TASK_WORK :
			case BaseModule::TYPE_TASK:
				$model = new TaskWork ();
				break;
			case BaseModule::TYPE_HIRING_WORK :
			case BaseModule::TYPE_HIRING:
				$model = new HiringParticipators ();
				break;
			case BaseModule::TYPE_EXCHANGE :
				$model = new ExchangeTrade ();
				break;
			case BaseModule::TYPE_ENCYCLOPEDIA :
				$model = new Encyclopedia ();
				break;
			default :
				;
		}
		if($targetSubId){
			$targetId = $targetSubId;
		}
		$data = $model::find ( $targetId );
		$data->creative_index += $score;
		$data->save ();
		
		return $data;
	}
	
	/**
	 * 增加比赛(任务人才和公益明星)的创创币
	 *
	 * @param int $targetType        	
	 * @param class $data        	
	 * @param int $score        	
	 */
	private static function addContestCreativeIndex($targetType, $data, $score) {
		if ($targetType == BaseModule::TYPE_TASK_WORK) {
			$model = Task::find($data->task_id);
			$model->creative_index += $score;
			$model->save ();
		} elseif ($targetType == BaseModule::TYPE_HIRING_WORK) {
			$model = Hiring::find($data->hiring_id);
			$model->creative_index += $score;
			$model->save ();
		}
		return true;
	}
	
	/**
	 * 增加用户的创创币
	 *
	 * @param int $userId        	
	 * @param int $score        	
	 */
	private static function addUserCreativeIndex($userId, $score) {
		$user = User::find ( $userId );
		
		$oldLevel = Utils::getCreativeLevel ( $user->creative_index );
		
		$user->creative_index += $score;
		$user->save ();
		
		$newLevel = Utils::getCreativeLevel ( $user->creative_index );
		if ($oldLevel != $newLevel) {
			\Event::fire ( 'user.update-level', array (
					$userId,
					$oldLevel,
					$newLevel 
			) );
		}
		
		return $user;
	}
	
	/**
	 * 增加地区学校政企的创创币
	 *
	 * @param class $user        	
	 * @param int $score        	
	 */
	public  static function addAreaCreativeIndex($user, $score,$userNewArea) {
		if ($userNewArea['province_id']) {
			$data = Region::find ( $userNewArea['province_id']);
			$data->creative_index += $score;
			$data->save ();
		}
		
		if ($userNewArea['city_id']) {
			$data = Region::find ($userNewArea['city_id']);
			$data->creative_index += $score;
			$data->save ();
		}
		
		if ($userNewArea['district_id']) {
			$data = Region::find ($userNewArea['district_id']);
			$data->creative_index += $score;
			$data->save ();
		}
		
		if ($userNewArea['school_id']) {
			$data = School::find ($userNewArea['school_id']);
			$data->creative_index += $score;
			$data->save ();
		}
		
		if ($userNewArea['organization_id']) {
			$data = Organization::find ($userNewArea['organization_id']);
			$data->creative_index += $score;
			$data->save ();
		}
	}
	
	/**
	 * 减掉创创币(被举报属实)
	 *
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 */
	private static function subCreativeIndex($userId, $targetType, $targetId, $reportId) {
		
		// 减掉基础的创创币
		$data = self::subBaseCreativeIndex ( $targetType, $targetId );
		
		// 减掉比赛的创创币
		self::subContestCreativeIndex ( $targetType, $data );
		
		// 减掉用户的创创币
		$user = self::subUserCreativeIndex ( $userId, $reportId );
		
		// 减掉地区学校政企的创创币
		$userOldArea  = array('province_id' => $user->province_id, 'city_id' => $user->city_id, 'district_id' => $user->district_id, 'school_id'   => $user->school_id, 'organization_id' => $user->organization_id);

		self::subAreaCreativeIndex ( $user,$userOldArea);
	}
	
	/**
	 * 减掉基础的创创币
	 * 
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\Collection, \Illuminate\Database\Eloquent\static, NULL, multitype:\Illuminate\Database\Eloquent\static >
	 */
	private static function subBaseCreativeIndex($targetType, $targetId) {
		switch ($targetType) {
			
			case BaseModule::TYPE_PROJECT :
				$model = new Project ();
				break;
			case BaseModule::TYPE_TASK_WORK :
				$model = new TaskWork ();
				break;
			case BaseModule::TYPE_HIRING_WORK :
				$model = new HiringParticipators ();
				break;
			case BaseModule::TYPE_EXCHANGE :
				$model = new ExchangeTrade ();
				break;
			case BaseModule::TYPE_ENCYCLOPEDIA :
				$model = new Encyclopedia ();
				break;
			default :
				;
		}
		
		$data = $model::find ( $targetId );
		$data->status = ItemModule::STATUS_SURE_REPORT;
		$data->save ();
		return $data;
	}
	
	/**
	 * 减掉比赛的创创币
	 * 
	 * @param int $targetType        	
	 * @param class $data        	
	 */
	private static function subContestCreativeIndex($targetType, $data) {
		if ($targetType == TYPE_TASK_WORK) {
			$task = Task::find ( $data->task_id );
			$task->creative_index = $data->creative_index;
			$task->save ();
		} elseif ($targetType == TYPE_HIRING_WORK) {
			
			$hiring = Hiring::find ( $data->hiring_id );
			$hiring->creative_index -= $data->creative_index;
			$hiring->save ();
		}
	}
	
	/**
	 * 减掉用户的创创币
	 * 
	 * @param int $userId        	
	 *
	 * @return \Illuminate\Database\Eloquent\Collection \Illuminate\Database\Eloquent\Model static
	 */
	private static function subUserCreativeIndex($userId, $reportId) {
		$user->oldCreativeIdex = $user = User::find ( $userId );
		$user->oldCreativeIdex = $user->creative_index;
		$user->creative_index = 0;
		$user->save ();
		
		ReportModule::clearedCreativeIndex ( $reportId, $userId, $user->oldCreativeIdex );
		return $user;
	}
	
	/**
	 * 减掉地区学校政企的创创币
	 * 
	 * @param class $user        	
	 */
	public  static function subAreaCreativeIndex($user,$userOldArea) {
		if ($userOldArea['province_id']) {
			$data = Region::find ( $userOldArea['province_id']);
			$data->creative_index -= $user->creative_index;
			$data->save ();
		}
		
		if ($userOldArea['city_id']) {
			$data = Region::find ($userOldArea['city_id']);
			$data->creative_index -= $user->creative_index;
			$data->save ();
		}
		
		if ($userOldArea['district_id']) {
			$data = Region::find ($userOldArea['district_id']);
			$data->creative_index -= $user->creative_index;
			$data->save ();
		}
		
		if ($userOldArea['school_id']) {
			$data = School::find ( $userOldArea['school_id'] );
			$data->creative_index -= $user->creative_index;
			$data->save ();
		}
		
		if ($userOldArea['organization_id']) {
			$data = Organization::find ($userOldArea['organization_id']);
			$data->creative_index -= $user->creative_index;
			$data->save ();
		}
	}
}