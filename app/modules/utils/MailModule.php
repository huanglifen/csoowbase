<?php namespace App\Modules\Utils;

/**
 * 邮件模块
 *
 * 工具模块，用于发送邮件。
 */
class MailModule {

	/**
	 * 发送邮件
	 *
	 * @param string $to 收件人邮箱
	 * @param string $title 邮件标题
	 * @param string $content 邮件内容
	 */
	public static function send($to, $title, $content, $cc = []) {
		if (! \Config::get('mail.is_sendmail')) {
			return;
		}

		\Mailgun::message(function($mail) use ($to, $title, $content, $cc) {
			$mail->from = 'noreply@mail.csoow.com';
			$mail->to = $to;
			$mail->cc = $cc;
			$mail->subject = $title;
			$mail->text = $content;
		})->deliver();
	}

}