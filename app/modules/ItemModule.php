<?php

namespace App\Modules;

use App\Models\Item;
use App\Models\Project;
use App\Models\Task;
use App\Models\TaskWork;
use App\Models\Hiring;
use App\Models\HiringParticipators;
use App\Models\Encyclopedia;
use Illuminate\Support\Facades\Event;
use App\Modules\CommentModule;
use App\Models\ExpertComment;
use App\Models\Comment;
use App\Models\Contest;
use App\Models\Update;
use App\Common\Utils;
use App\Models\TagsAffairs;
use App\Models\Tags;

/**
 * 比赛/交易基础模块
 *
 * 处理比赛/交易公共功能。
 */
abstract class ItemModule extends BaseModule {
	
	// project task hiring exchange通用
	const STATUS_WAITING = 1; // 初始状态（发布失败|未发布） 0:预留给全部
	const STATUS_GOING = 2; // 进行中
	const STATUS_END_SUCCESS = 3; // 结束或过期(成功{project:投资完成；task和hiring:选出获胜人才;exchange:有人购买})
	const STATUS_END_FAIL = - 1; // 结束或过期(失败{project:投资失败或无人投资;task和hiring:未选出获胜人才或无人参赛;exchange:无人购买})
	const STATUS_SURE_REPORT = - 2; // 举报属实
	const STATUS_WAITING_CHECK = -3; //待审核
	const STATUS_NEED_UPDATE = -4; //需要修改
	const STATUS_CHECK_AGAIN = -5; //再次审核
	                                
	// project使用
	const STATUS_CONFIRM_INVEST = 11; // 确认投资，等待付款阶段
	const STATUS_PAYED = 12; // 已付款，等待发放回报阶段
	const STATUS_WAITING_CONFIRM_RETURN = 13; // 已经发放回报，等待确认收到回报阶段
	                                          
	// task使用
	const STATUS_WAITING_CONFIRM = 21; // 等待确认成果
	const ACTION_TYPE_PUBLISH = 1; // 发起 发布
	const ACTION_TYPE_APPLY = 2; // 参与
	const ACTION_TYPE_LIKE = 3; // 喜欢 赞
	
	public static function fillContests($contests) {
		foreach ( $contests as $contest ) {
			$info = json_decode ( $contest->info );
			$tags = json_decode ( $contest->tags );
			
			$contest->info = $info;
			
			if (! $tags) {
				$tags = array ();
			}
			
			$contest->tags = $tags;
		}
		
		if (count ( $contests ) == 1) {
			return $contests [0];
		}
		
		return $contests;
	}
	
	/**
	 * 获取最后一个比赛/交易编号
	 *
	 * @return string
	 */
	// protected abstract static function getLastItemNo();
	
	/**
	 * 生成比赛/交易编号
	 *
	 * @return string
	 */
	protected static function generateItemNo() {
		$lastNumber = static::getLastItemNo ();
		$no = 1;
		
		$today = date ( 'Ymd' );
		if (strlen ( $lastNumber ) > 1) {
			$prefix = substr ( $lastNumber, 0, 1 );
			if ($today === substr ( $lastNumber, 1, 8 )) {
				$no = ( int ) (substr ( $lastNumber, 9 )) + 1;
			}
		} else {
			$prefix = $lastNumber;
		}
		
		return $prefix . $today . sprintf ( "%05d", $no );
	}
	
	/**
	 * 填充比赛发布时公共项
	 *
	 * @param object $item        	
	 * @param array $common        	
	 * @param int $userId        	
	 * @param int $initialStatus        	
	 */
	protected static function fillPublishCommon($item, array $common, $userId, $initialStatus) {
		$item->no = self::generateItemNo ();
		$item->user_id = $userId;
		$item->title = $common ['title'];
		//$item->description = ItemModule::ueditorImageToLocal($common ['description']);
		 $item->description = $common ['description']; 
		
		$item->cover = $common ['cover'];
		
		$user = UserModule::getUserById ( $userId );
		
		$item->province_id = ! empty ( $user->province_id ) ? $user->province_id : 0;
		$item->city_id = ! empty ( $user->city_id ) ? $user->city_id : 0;
		$item->district_id = ! empty ( $user->district_id ) ? $user->district_id : 0;
		$item->school_id = ! empty ( $user->school_id ) ? $user->school_id : 0;
		$item->organization_id = ! empty ( $user->organization_id ) ? $user->organization_id : 0;
		
		$item->guess_total_amount = 0;
		$item->guess_success_amount = 0;
		
		$item->status = $initialStatus;
		$item->start_time = time();
		$item->tags = '';
		$item->creative_index = 0;
		$item->check_remark = '';
	}
	
	/**
	 * 创创召搜索结果--大赛和交易所
	 *
	 * @param int $keyword        	
	 * @param number $limit        	
	 */
	public static function searchItemsByKeyword($keyword, $limit = 5) {
		return Item::where ( 'title', 'like', '%' . $keyword . '%' )->whereIn ( 'target_type', array (
				BaseModule::TYPE_PROJECT,
				BaseModule::TYPE_TASK,
				BaseModule::TYPE_HIRING,
				BaseModule::TYPE_EXCHANGE 
		) )->where ( 'action_type', ItemModule::ACTION_TYPE_PUBLISH )->limit ( $limit )->orderBy ( 'id', 'desc' )->get ();
	}
	
	/**
	 * 根据状态获取比赛
	 * @param int $status
	 * @param int $offset
	 * @param int $limit
	 */
	public static function getContestByStatus($status = self::STATUS_GOING, $offset = 0, $limit = 12) {
		return Contest::where('status', $status)->offset($offset)->$limit($limit)->orderBy('id', 'asc')->get();
	}
	
	/**
	 * 获取审核中的比赛
	 */
	public static function getInCheckContests($status ,$offset = 0, $limit = 12) {
		return Contest::where('status', $status)->offset($offset)->limit($limit)->orderBy('id', 'desc')->orderBy('status', 'desc')->get();
	}
	
	/**
	 * 获取审核中比赛的数量
	 * @param unknown $status
	 */
	public static function getInCheckContestsCount($status) {
		return Contest::where('status', $status)->count();
	}
	
	/**
	 * 添加比赛标签
	 *
	 * @param int $itemId        	
	 * @param int $itemType        	
	 * @param array $tagsNames        	
	 * @return array $tags
	 */
	public static function addItemTags($itemId, $itemType, $tagsNames) {
		if (! $tagsNames) {
			return '';
		}
		
		$tagsResult = TagModule::createTags ( $itemType, $tagsNames );
		TagModule::createTagsAffairs ( $itemType, $itemId, $tagsResult ['ids'] );
		
		return $tagsResult ['tags'];
	}
	
	/**
	 * 根据ItemId 删除相应的标签
	 * @param unknown $itemId
	 * @param unknown $itemType
	 */
	public static function deleteItemTags($itemId, $itemType) {
		$tags = TagsAffairs::where('affair_id', $itemId)->where('affair_type', $itemType)->get();
		
		TagsAffairs::where('affair_id', $itemId)->where('affair_type', $itemType)->delete();
		
		$tagIds = array();
		
		foreach ($tags as $tag) {
			$tagIds[] = $tag->tag_id;
		}
		
		return Tags::whereIn('id', $tagIds)->delete();
	}
	
	/**
	 * 保存item信息(用于搜索和个人主页查询)
	 *
	 * @param int $userId        	
	 * @param int $actionType
	 *        	行为类型
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param string $title        	
	 *
	 * @return int itemID
	 */
	public static function saveItem($userId, $actionType, $targetType, $targetId, $title = '') {
		$model = new Item ();
		$model->user_id = $userId;
		$model->action_type = $actionType;
		$model->target_type = $targetType;
		$model->target_id = $targetId;
		$model->title = $title;
		$model->create_time = time ();
		$model->save ();
		return $model->id;
	}
	
	/**
	 * 删除一条Item信息
	 *
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @return boolean
	 */
	public static function deleteItem($targetType, $targetId, $actionType = self::ACTION_TYPE_APPLY) {
		return Item::where ( 'action_type', $actionType )->where ( 'target_type', $targetType )->where ( 'target_id', $targetId )->delete ();
	}
	
	/**
	 * 某条件的item是否已经存在
	 *
	 * @param int $userId        	
	 * @param int $actionType        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 *
	 * @return bool
	 */
	public static function isExist($userId, $actionType, $targetType, $targetId) {
		$cnt = Item::whereRaw ( 'user_id = ? and action_type = ? and target_type = ? and target_id = ?', array (
				$userId,
				$actionType,
				$targetType,
				$targetId 
		) )->count ();
		return $cnt > 0 ? true : false;
	}
	public static function getItemByTargetIds($userId, $actionType, $targetType, $targetIds) {
		return Item::where ( 'action_type', $actionType )->where ( 'target_type', $targetType )->where ( 'user_id', $userId )->whereIn ( 'target_id', $targetIds )->get ();
	}
	
	/**
	 * 获取创意比赛--个人中心
	 *
	 * @param int $userId        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @param int $type        	
	 * @param int $isMe        	
	 *
	 * @return mixed
	 */
	public static function getContestsByActionType($userId, $offset = 0, $limit = 12, $actionType = 0, $isMe = 1) {
		$item = Item::where ( 'user_id', $userId )->whereIn ( 'target_type', array (
				BaseModule::TYPE_PROJECT,
				BaseModule::TYPE_TASK,
				BaseModule::TYPE_HIRING 
		) );
		
		if ($actionType) {
			$item = $item->where ( 'action_type', $actionType );
		} else {
			if (! $isMe) {
				$item = $item->where ( 'action_type', '!=', self::ACTION_TYPE_LIKE );
			}
		}
		$items = $item->distinct ( 'target_id' )->offset ( $offset )->limit ( $limit )->get ();
		
		$itemIds = array ();
		foreach ( $items as $k => $item ) {
			$itemIds [] = $item->target_id;
		}
		
		$datas = array ();
		if ($itemIds) {
			$datas = Contest::whereIn ( 'id', $itemIds )->orderBy ( 'id', 'desc' )->get ();
		}
		
		return $datas;
	}
	public static function getContestsByActionTypeCount($userId, $actionType = 0, $isMe = 1) {
		$item = Item::where ( 'user_id', $userId )->whereIn ( 'target_type', array (
				BaseModule::TYPE_PROJECT,
				BaseModule::TYPE_TASK,
				BaseModule::TYPE_HIRING 
		) );
		
		if ($actionType) {
			$item = $item->where ( 'action_type', $actionType );
		} else {
			if (! $isMe) {
				$item = $item->where ( 'action_type', '!=', self::ACTION_TYPE_LIKE );
			}
		}
		$result = $item->select ( \DB::raw ( 'count(distinct target_id) as num' ) )->get ();
		
		return $result [0]->num;
	}
	public static function getContestByUserIds($userIds, $offset = 0, $limit = 4, $actionType = 0) {
		if (! count ( $userIds )) {
			return array ();
		}
		$item = Item::whereIn ( 'user_id', $userIds )->whereIn ( 'target_type', array (
				BaseModule::TYPE_PROJECT,
				BaseModule::TYPE_TASK,
				BaseModule::TYPE_HIRING 
		) );
		
		if ($actionType) {
			$item = $item->where ( 'action_type', $actionType );
		}
		
		$items = $item->offset ( $offset )->limit ( $limit )->orderBy ( 'id', 'desc' )->distinct ()->get ( [ 
				'target_id' 
		] );
		
		$itemIds = array ();
		foreach ( $items as $k => $item ) {
			$itemIds [] = $item->target_id;
		}
		$datas = array ();
		if ($itemIds) {
			$datas = Contest::whereIn ( 'id', $itemIds )->get ();
		}
		return $datas;
	}
	
	/**
	 * 获取创意作品--个人中心
	 *
	 * @param int $userId        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @param int $actionType        	
	 * @param int $isMe        	
	 *
	 * @return mixed
	 */
	public static function getWorksByActionType($userId, $offset = 0, $limit = 12, $pageType = 0, $isMe = 1) {
		$datas = array ();
		switch ($pageType) {
			case 5 :
				return $datas = Encyclopedia::where ( 'user_id', $userId )->offset ( $offset )->limit ( $limit )->get ();
				break;
			case 21 :
				return $datas = TaskWork::where ( 'user_id', $userId )->offset ( $offset )->limit ( $limit )->get ();
				break;
			case 31 :
				return $datas = HiringParticipators::where ( 'user_id', $userId )->offset ( $offset )->limit ( $limit )->get ();
				break;
			case 4 :
				$item = Item::whereIn ( 'target_type', array (
						BaseModule::TYPE_ENCYCLOPEDIA,
						BaseModule::TYPE_TASK_WORK,
						BaseModule::TYPE_HIRING_WORK 
				) )->where ( 'action_type', ItemModule::ACTION_TYPE_LIKE );
				break;
			default :
				$item = Item::whereIn ( 'target_type', array (
						BaseModule::TYPE_ENCYCLOPEDIA,
						BaseModule::TYPE_TASK_WORK,
						BaseModule::TYPE_HIRING_WORK 
				) );
				break;
		}
		
		$items = $item->where ( 'user_id', $userId )->offset ( $offset )->limit ( $limit )->get ();
		$items = $items->toArray ();
		// 去重
		$len = count ( $items );
		$tmps = array ();
		foreach ( $items as $k => $item ) {
			$start = ($k + 1 < $len) ? $k + 1 : $len - 1;
			for($j = $start; $j < $len; $j ++) {
				$data = $items [$j];
				if ($item ['user_id'] == $data ['user_id'] && $item ['target_type'] = $data ['target_type'] && $item ['target_id'] == $data ['target_id'] && $k != $j) {
					$tmps [] = $j;
				}
			}
		}
		foreach ( $tmps as $tmp ) {
			unset ( $items [$tmp] );
		}
		
		foreach ( $items as $k => $item ) {
			if ($item ['target_type'] == BaseModule::TYPE_ENCYCLOPEDIA) {
				$datas [$k] = Encyclopedia::find ( $item ['target_id'] );
				$datas [$k] ['type'] = BaseModule::TYPE_ENCYCLOPEDIA;
			} elseif ($item ['target_type'] == BaseModule::TYPE_TASK_WORK) {
				$datas [$k] = TaskWork::find ( $item ['target_id'] );
				$datas [$k] ['type'] = BaseModule::TYPE_TASK_WORK;
			} elseif ($item ['target_type'] == BaseModule::TYPE_HIRING_WORK) {
				$datas [$k] = HiringParticipators::find ( $item ['target_id'] );
				$datas [$k] ['type'] = BaseModule::TYPE_HIRING_WORK;
			}
		}
		return $datas;
	}
	public static function getWorksByActionTypeCount($userId, $pageType = 0) {
		switch ($pageType) {
			case 5 :
				return $datas = Encyclopedia::where ( 'user_id', $userId )->count ();
				break;
			case 21 :
				return $datas = TaskWork::where ( 'user_id', $userId )->count ();
				break;
			case 31 :
				return $datas = HiringParticipators::where ( 'user_id', $userId )->count ();
				break;
			case 4 :
				$item = Item::whereIn ( 'target_type', array (
						BaseModule::TYPE_ENCYCLOPEDIA,
						BaseModule::TYPE_TASK_WORK,
						BaseModule::TYPE_HIRING_WORK 
				) )->where ( 'action_type', ItemModule::ACTION_TYPE_LIKE );
				break;
			default :
				$item = Item::whereIn ( 'target_type', array (
						BaseModule::TYPE_ENCYCLOPEDIA,
						BaseModule::TYPE_TASK_WORK,
						BaseModule::TYPE_HIRING_WORK 
				) );
				break;
		}
		
		$items = $item->where ( 'user_id', $userId )->get ();
		$items = $items->toArray ();
		// 去重
		$len = count ( $items );
		$tmps = array ();
		foreach ( $items as $k => $item ) {
			$start = ($k + 1 < $len) ? $k + 1 : $len - 1;
			for($j = $start; $j < $len; $j ++) {
				$data = $items [$j];
				if ($item ['user_id'] == $data ['user_id'] && $item ['target_type'] = $data ['target_type'] && $item ['target_id'] == $data ['target_id'] && $k != $j) {
					$tmps [] = $j;
				}
			}
		}
		foreach ( $tmps as $tmp ) {
			unset ( $items [$tmp] );
		}
		
		return count ( $items );
	}
	
	/**
	 * 发布评论
	 *
	 * @param unknown $userId        	
	 * @param unknown $targetType        	
	 * @param unknown $targetId        	
	 * @param unknown $content        	
	 * @return number
	 */
	public static function publishComment($userId, $targetType, $targetId, $content) {
		return CommentModule::publishComment ( $userId, $targetType, $targetId, $content );
	}
	
	/**
	 * 获取评论
	 *
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return Ambigous <\App\Modules\评论信息, multitype:, multitype:\Illuminate\Database\Query\static , array>
	 */
	public static function getComments($targetType, $targetId, $offset, $limit) {
		return CommentModule::getComments ( $targetType, $targetId, $offset, $limit );
	}
	public static function getCommentNum($targetType, $targetId) {
		return Comment::where ( 'target_type', $targetType )->where ( 'target_id', $targetId )->count ();
	}
	
	/**
	 * 专家点评
	 *
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $score        	
	 * @param string $content        	
	 * @return number
	 */
	public static function publishExpertComment($userId, $targetType, $targetId, $score, $content, $targetSubId = 0) {
		return ExpertCommentModule::publishExpertComment ( $userId, $targetType, $targetId, $score, $content, $targetSubId );
	}
	
	/**
	 * 获取专家点评
	 *
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return Ambigous <\App\Modules\mixed, multitype:, multitype:\Illuminate\Database\Query\static , array>
	 */
	public static function getExpertComments($targetType, $targetId, $targetSubId = 0, $offset = 0, $limit = 3) {
		return ExpertCommentModule::getExpertComments ( $targetType, $targetId, $targetSubId, $offset, $limit );
	}
	
	/**
	 * 获取专家点评的数量
	 *
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $targetSubId        	
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getExpertCommentsNum($targetType, $targetId) {
		return ExpertComment::whereRaw ( 'target_type = ? and target_id = ?', array (
				$targetType,
				$targetId 
		) )->orderBy ( 'id', 'desc' )->count ();
	}
	
	/**
	 * 根据ID获取一条更新
	 *
	 * @param int $updateId        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\Collection, \Illuminate\Database\Eloquent\static, NULL, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getUpdateById($updateId) {
		return Update::find ( $updateId );
	}
	
	/**
	 * 邀请站内专家
	 *
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $targetSubId        	
	 * @param int $expertUserId        	
	 * @return \App\Modules\mixed
	 */
	public static function inviteInternalExpert($userId, $targetType, $targetId, $expertUserId, $targetSubId = 0) {
		return ExpertModule::inviteInternalExpert ( $userId, $targetType, $targetId, $expertUserId, $targetSubId );
	}
	
	/**
	 * 删除邀请的站内专家
	 *
	 * @param int $id        	
	 * @return Ambigous <boolean, NULL, number>
	 */
	public static function deleteInternalExpert($id) {
		return ExpertModule::deleteInternalExpert ( $id );
	}
	
	/**
	 * 邀请站外专家
	 *
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $expertInfo        	
	 * @param int $targetSubId        	
	 * @return \App\Modules\主键ID
	 */
	public static function inviteExternalExpert($userId, $targetType, $targetId, $expertInfo, $targetSubId = 0) {
		return ExpertModule::inviteExternalExpert ( $userId, $targetType, $targetId, $expertInfo, $targetSubId );
	}
	
	/**
	 * 喜欢
	 *
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @return number
	 */
	public static function addLike($userId, $targetType, $targetId) {
		return self::saveItem ( $userId, self::ACTION_TYPE_LIKE, $targetType, $targetId );
	}
	
	/**
	 * 举报
	 *
	 * @param int $userId        	
	 * @param int $targetType        	
	 * @param int $targetId        	
	 * @param int $type        	
	 */
	public static function saveReport($userId, $targetType, $targetId, $type) {
		return ReportModule::saveReport ( $userId, $targetType, $targetId, $type );
	}
	
	/**
	 * 通过类型和id获取相应的比赛或创意
	 *
	 * @param int $targetId        	
	 * @param int $targetType        	
	 */
	public static function getItemByTargetIdAndType($targetId, $targetType) {
		switch ($targetType) {
			case self::TYPE_PROJECT :
				return ProjectModule::getProjectById ( $targetId );
				break;
			case self::TYPE_TASK :
				return TaskModule::getTaskById ( $targetId );
				break;
			case self::TYPE_HIRING :
				return HiringModule::getHiringById ( $targetId );
				break;
			case self::TYPE_EXCHANGE :
				return ExchangeModule::getTradeById ( $targetId );
				break;
			case self::TYPE_ENCYCLOPEDIA :
				return EncyclopediaModule::getEncyclopediaById ( $targetId );
				break;
		}
	}
	/**
	 * 事件监听:往item表写数据，为了搜索和个人主页查询
	 */
	public static function listenEvent() {
		// 发布比赛
		Event::listen ( 'project.publish', function ($userId, $projectId, $title) {
			self::saveItem ( $userId, self::ACTION_TYPE_PUBLISH, BaseModule::TYPE_PROJECT, $projectId, $title );
		} );
		
		// 参与投资项目
		Event::listen ( 'project.invest', function ($userId, $projectId) {
			$data = Project::find ( $projectId );
			self::saveItem ( $userId, self::ACTION_TYPE_APPLY, BaseModule::TYPE_PROJECT, $projectId, $data ['title'] );
		} );
		
		// 发布任务人才
		Event::listen ( 'task.publish', function ($userId, $taskId, $title) {
			self::saveItem ( $userId, self::ACTION_TYPE_PUBLISH, BaseModule::TYPE_TASK, $taskId, $title );
		} );
		
		// 任务人才提交作品
		Event::listen ( 'task.submit-work', function ($userId, $taskId, $taskWorkId) {
			$data = Task::find ( $taskId );
			$flag = self::isExist ( $userId, self::ACTION_TYPE_PUBLISH, BaseModule::TYPE_TASK_WORK, $taskWorkId );
			if (! $flag) {
				self::saveItem ( $userId, self::ACTION_TYPE_PUBLISH, BaseModule::TYPE_TASK_WORK, $taskWorkId );
			}
			self::saveItem ( $userId, self::ACTION_TYPE_APPLY, BaseModule::TYPE_TASK, $taskId, $data ['title'] );
		} );
		
		// 发布公益明星比赛
		Event::listen ( 'hiring.publish', function ($userId, $hiringId, $title) {
			self::saveItem ( $userId, self::ACTION_TYPE_PUBLISH, BaseModule::TYPE_HIRING, $hiringId, $title );
		} );
		
		// 公益明星提交作品
		Event::listen ( 'hiring.apply', function ($userId, $hiringId, $participatorId) {
			$data = Hiring::find ( $hiringId );
			$flag = self::isExist ( $userId, self::ACTION_TYPE_PUBLISH, BaseModule::TYPE_HIRING_WORK, $participatorId );
			if (! $flag) {
				self::saveItem ( $userId, self::ACTION_TYPE_PUBLISH, BaseModule::TYPE_HIRING_WORK, $participatorId );
			}
			self::saveItem ( $userId, self::ACTION_TYPE_APPLY, BaseModule::TYPE_HIRING, $hiringId, $data ['title'] );
		} );
		
		// 发布交易所交易
		Event::listen ( 'trade.publish', function ($userId, $tradeId, $title) {
			self::saveItem ( $userId, self::ACTION_TYPE_PUBLISH, BaseModule::TYPE_EXCHANGE, $tradeId, $title );
		} );
		
		// 分享创意(百科)
		Event::listen ( 'encyclopedia.share', function ($userId, $encyclopediaId, $title) {
			self::saveItem ( $userId, self::ACTION_TYPE_PUBLISH, BaseModule::TYPE_ENCYCLOPEDIA, $encyclopediaId, $title );
		} );
		
		// 撤回投资
		Event::listen ( 'project.cancel-invest', function ($projectId, $investId) {
			self::deleteItem ( self::TYPE_PROJECT, $projectId );
			self::deleteItem ( self::TYPE_PROJECT, $investId, self::ACTION_TYPE_PUBLISH );
		} );
		
		// 撤回人才作品
		Event::listen ( 'task.cancel-work', function ($taskId, $workId) {
			self::deleteItem ( self::TYPE_TASK, $taskId );
			self::deleteItem ( self::TYPE_TASK_WORK, $workId, self::ACTION_TYPE_PUBLISH );
		} );
		
		// 撤回明星比赛作品
		Event::listen ( 'hiring.cancel-apply', function ($hiringId, $participatorId) {
			self::deleteItem ( self::TYPE_HIRING, $hiringId );
			self::deleteItem ( self::TYPE_HIRING_WORK, $participatorId, self::ACTION_TYPE_PUBLISH );
		} );
	}
	
	/**
	 * 将ueditor的图片保存到本地
	 *
	 * @param unknown $content        	
	 * @return unknown
	 */
	public static function ueditorImageToLocal($content) {
		preg_match_all ( "@<img(.*)src=['|\"](.*)['|\"](.*)>@imsU", $content, $match );
		
		$imagesHrefArr = $match [2];
		
		$url = \URL::to ( '/' ).'/';
		
		$uploadPath = "data/ueditor/" . date ( "Ymd" );
		if (! file_exists ($uploadPath )) {
			@mkdir ( $uploadPath, 0777, true );
		}
		
		foreach ( $imagesHrefArr as $key => $imageurl ) {
			
			$ext = explode ( '?', $imageurl );
			if (count ( $ext ) > 0) {
				$ext = $ext [0];
				$ext = strtolower ( strrchr ( $ext, '.' ) );
			}
			else {
				$ext = strtolower ( strrchr ( $imageurl, '.' ) );
			}
			
			$fileName = time () . rand ( 1, 10000 ) . $ext;
			$saveName = $uploadPath . '/' . $fileName;
			@copy ( $imageurl, $saveName );
			$content = str_replace ( $match [0] [$key], "<img src=".$url.$saveName." />", $content );
		}
		return $content;
	}
}