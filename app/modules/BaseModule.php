<?php

namespace App\Modules;

abstract class BaseModule {
	const TYPE_CONTEST = 0;
	const TYPE_PROJECT = 1; // 投资比赛
	const TYPE_TASK = 2; // 任务人才
	const TYPE_TASK_WORK = 21; // 创意任务人才比赛作品
	const TYPE_HIRING = 3; // 公益明星比赛
	const TYPE_HIRING_WORK = 31; // 创意公益明星比赛参赛明星作品
	const TYPE_EXCHANGE = 4; // 交易所
	const TYPE_ENCYCLOPEDIA = 5; // 创意百科
	const TYPE_USER = 6; // 用户
	const TYPE_DYNAMIC = 61; // 朋友圈动态、我的动态
	const TYPE_CIRCLE_ME = 62; // 圈子
	const GROUP_COMMON_USER = 63;
	const GROUP_EXPERT = 64;
	const TYPE_AREA = 7; // 大地区
	const TYPE_REGION = 71; // 地区
	const TYPE_SCHOOL = 72; // 学校
	const TYPE_ORGANIZATION = 73; // 政企
	const TYPE_GOVERNMENT = 74; // 行政机构
	const TYPE_CORPORATE = 75; // 企业
	const TYPE_EXPERT = 8; // 智库
	const EXPERT_CHAIR = 1;// 创意世界智库主席团
	const EXPERT_COUNCIL = 2; //专家理事会
	const EXPERT_ADVISORY = 3;  //创意世界智库专家顾问团
	
	const COMMENT_PAGE_SIZE = 10;//评论和消息页码大小
	const DYNAMICS_PER_PAGE = 10;//动态页码大小
	const ITEM_NUMBER_INDEX = 12;//比赛交易百科页码大小
	const USER_NUMBER_SIZE = 24;//用户专家地区页码大小
	const ACCOUNT_PER_PAGE = 10;//账号设置页码大小
	const HOT_TAG_NUMBER = 20;//热门标签页码大小

	const COMMENT_LEN_AREA = 140;
	const COMMENT_LEN_NO_AREA = 1000;
	
	public function __construct() {
	}
	
	/**
	 * 哈希，自带盐
	 *
	 * @param string $string        	
	 * @return string
	 */
	protected static function hash($string) {
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$salt = substr ( str_shuffle ( str_repeat ( $pool, 5 ) ), 0, 16 );
		
		return $salt . hash ( 'sha256', $salt . $string );
	}
	
	/**
	 * 检查 $string hash 后是否等于 $hashedString，盐从 $hashedString 中取
	 *
	 * @param string $string        	
	 * @param string $hashedString        	
	 * @return bool
	 */
	public static function checkHash($string, $hashedString) {
		$salt = substr ( $hashedString, 0, 16 );
		
		return ($salt . hash ( 'sha256', $salt . $string )) === $hashedString;
	}
	
	/**
	 * 生成校验码，如密码重置码，形如 XXXXX-XXXXX-XXXXX-XXXXX-XXXXX
	 *
	 * @return string
	 */
	protected static function generateCode() {
		$block = 5;
		
		$result = '';
		for($z = 0; $z < $block; $z ++) {
			$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			for($i = 0; $i < $block; $i ++) {
				$result .= $chars [(mt_rand ( 0, (strlen ( $chars ) - 1) ))];
			}
			$result .= "-";
		}
		
		$result = substr ( $result, 0, - 1 );
		return $result;
	}
}