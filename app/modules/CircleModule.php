<?php namespace App\Modules;

use App\Models\Circle;
use App\Models\CircleMember;
use App\Models\User;

/**
 * 朋友圈模块
 *
 * 处理朋友圈相关功能
 */
class CircleModule extends BaseModule {
    const TYPE_MY_CIRCLE = 1;  //显示我的圈子
    const TYPE_CIRCLE_ME = 2;  //显示圈我的人
    const TYPE_CIRCLE_DEFAULT = 1; //默认圈子
    const TYPE_CIRCLE_NOT_DEFAULT = 0; //非默认圈子
    const CIRCLE_DEFAULT_NAME = '朋友'; //优先圈子名
    const USER_CIRCLE_CARD = 1; //用户圈子卡片
    const CIRCLE_CARD = 2;//圈子中的卡片

    /**
     * 创建一个圈子
     * 
     * @param int $userId
     * @param int $name
     * @return $circleId
     */
    public static function createCircle($userId, $name, $isDefault = 0) {
        $hasCircle = self::countCircleByName($name, $userId);
        if($hasCircle) {
            return false;
        }
        
        $circle = new Circle();
        
        $circle->user_id = $userId;
        $circle->name = $name;
        $circle->member_count = 0;
        $circle->is_default = $isDefault;
        $circle->create_time = time();
        
        $circle->save();
        
        return $circle->id;
    }
    
    /**
     * 创建默认圈子
     * 
     * @param int $userId
     */
    public static function createDefaultCircle($userId) {
        $nameOne = '朋友';
        self::createCircle($userId, $nameOne,1);
        $nameTwo = '同学';
        self::createCircle($userId, $nameTwo,1);
        $nameThree = '同事';
        self::createCircle($userId, $nameThree, 1);
    }
    
    /**
     * 修改一个圈子的名字
     * 
     * @param int $id
     * @param int $name
     * @param int $userId
     * @return boolean
     */
    public static function modifyCircleName($id, $name, $userId) {
        $circle = self::getCircleAuthority($id, $userId);
        
        if(! $circle || $circle->is_default == self::TYPE_CIRCLE_DEFAULT) {
            return false;
        }
        
        if($circle->name == $name) {
            return true;
        }
        
        $hasCircle = self::countCircleByName($name, $userId);
        if($hasCircle) {
            return false;
        }
        
        $circle->name = $name;
        $circle->save();
        
        return true;
    }
    
    /**
     * 获取一个圈子的圈子信息
     * 
     * @param int $id
     * @return object $circle
     */
    public static function getCircle($id) {
        return Circle::find($id);
    }
        
    /**
     * 获取我的所有圈子信息
     * 
     * @param unknown $userId
     * @return array $circles
     */
    public static function getCirclesByUserId($userId) {
        return Circle::where('user_id', $userId)->get();
    }
    
    /**
     * 获取我对某个圈子的权限
     * 
     * @param int $circleId
     * @param int $userId
     * @return boolean
     */
    public static function getCircleAuthority($circleId, $userId) {
        $circle = self::getCircle($circleId);
    
        if(!empty($circle) && $circle->user_id == $userId) {
            return $circle;
        }else{
            return false;
        }
    }
    
    /**
     * 添加用户到默认圈子,关注用户
     *
     * @param int $ownerUserId
     * @param int $userId
     */
    public static function addCircleMemberToDefaultCircle($ownerUserId, $userId) {
        $isCircle = self::isCircleSomebody($userId, $ownerUserId);
        if($isCircle) {
            return false;
        }
        $circle = self::getDefaultCIrcle($ownerUserId);
        
        if($circle) {
           $result =  self::addCircleMember($circle->id, $ownerUserId, $userId);
           if($result) {
               \Event::fire('circle.add-circle-member', array($ownerUserId, $userId));
           }
           return $result;
        }
        return false;
    }
    
    
    /**
     * 注册时关注官方账号
     * 
     * @param int $ownerUserId
     */
    public static function followOfficialAccount($ownerUserId) {
        $officials = UserModule::getOfficialAccounts();
        
        foreach($officials as $official) {
           self::addCircleMemberToDefaultCircle($ownerUserId, $official->id);
        }
        return true;
    }
    
    /**
     * 取消关注某人
     * 
     * @param int $owerUserId
     * @param int $userId
     * @return Ambigous <number, \Illuminate\Database\mixed>
     */
    public static function cancelFollow($owerUserId, $userId) {
        return CircleMember::where('circle_owner_user_id', $owerUserId)->where('user_id', $userId)->delete();
    }
    
    /**
     * 将一个用户添加到某个圈子中
     * 
     * @param int $userId
     * @param int $ownerUserId
     * @param int $circleId
     * @return boolean
     */
    public static function addCircleMember($circleId, $ownerUserId, $userId) {
        if($userId == $ownerUserId) {
            return false;
        }
        
        $circle = self::getCircleAuthority($circleId, $ownerUserId);
        if (! $circle) {
            return false;
        }
        
        $hasMember = self::getMemberByCircleIdAndUserId($circleId, $userId);
        if (empty($hasMember)) {
            $member = new CircleMember();
            
            $member->circle_id = $circleId;
            $member->circle_owner_user_id = $ownerUserId;
            $member->user_id = $userId;
            $member->create_time = time();
            
            $member->save();
            
            $circle->member_count ++;
            $circle->save();
            self::plusUsersFollowerCount(array($userId), $ownerUserId);
        }

        return true;        
    }
    
    /**
     * 创建圈子并将用户添加到圈子
     * 
     * @param int $ownerUserId
     * @param int $userId
     */
    public static function createCircleAndAddMember($ownerUserId, $userId, $name) {
        $circleId = self::createCircle($ownerUserId, $name);
        
        if($circleId) {
            $result = self::addCircleMember($circleId, $ownerUserId, $userId);
			if($result) {
				return $circleId;
			}
			return $result;
        }else{
            return -1;
        }
    }

    /**
     * 为关注的用户选择分组
     * 
     * @param array $circleIds
     * @param int $ownerUserId
     * @param int $userId
     * @return boolean
     */
    public static  function chooseGroup(array $circleIds, $ownerUserId, $userId) {  
        $flag = false;             
        foreach($circleIds as $circleId) {
            $result = self::addCircleMember($circleId, $ownerUserId, $userId);
            
            if(! $flag && $result) {
                $flag = true;
            }
        }
        
        $defaultCircle = self::getDefaultCIrcle($ownerUserId);
        
        if($flag && $defaultCircle) {
            if(! in_array($defaultCircle->id, $circleIds)) {
                self::removeCircleMember($defaultCircle->id, $ownerUserId, $userId);
            }
        }
        
        return $result;
    }

    /**
     * 将一个成员从某圈子中移除
     * 
     * @param int $circleId
     * @param int $ownerUserId
     * @param int $userId
     */
    public static function removeCircleMember($circleId, $ownerUserId, $userId) {
        $circle = self::getCircleAuthority($circleId, $ownerUserId);
        if(! $circle) {
            return false;
        }
        
        $result = self::removeCircleMembers($circleId, $ownerUserId, $userId);
        
        if($result) {
            $circle->member_count--;
            $circle->save();
        }
        return $result;
    }

    /**
     * 删除一个圈子并移除该圈子的所有成员
     * 
     * @param int $circleId
     * @param int $userId
     * @return boolean
     */
    public static function deleteCircleAndMembers($circleId, $userId) {
        $circle = self::getCircleAuthority($circleId, $userId);
        if (! $circle || $circle->is_default == self::TYPE_CIRCLE_DEFAULT) {
            return false;
        }
        
        $result = self::deleteCircle($circleId);
        self::removeCircleMembers($circleId, $userId);
        
        if($result) {
            return true;
        }else {
            return false;
        }
    }
    
    /**
     * 获取我的所有成员
     * 
     * @param int $userId
     * @return array
     */
    public static function getAllMembersByUserId($userId) {
        return CircleMember::where('circle_owner_user_id', $userId)->distinct()->get(array('user_id'));
    }
    
    /**
     * 获取一个圈子的成员信息
     * 
     * @param int $circleId
     * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
     */
    public static function getMembersByCircleId($circleId) {
        return CircleMember::where('circle_id', $circleId)->get();
    }
    
    /**
     * 获取一个圈子的某个成员
     * 
     * @param int $userId
     * @param int $circleId
     * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\static, NULL>
     */
    public static function getMemberByCircleIdAndUserId($circleId, $userId) {
        return CircleMember::where('circle_id', $circleId)->where('user_id', $userId)->first();
    }
    
    /**
     * 是否圈了某人，是否关注了某人
     * 
     * @param int $userId
     * @param int $ownerUserId
     * @return boolean
     */
    public static function isCircleSomebody($userId, $ownerUserId) {
        $count = CircleMember::where('circle_owner_user_id', $ownerUserId)->where('user_id', $userId)->count();
        
        return $count ? true :false;
    }
    
    /**
     * 查看某个圈子的成员及这些成员在我的所有圈子的情况
     * 
     * @param int $circleId
     * @param int $userId
     * @return boolean|Ambigous <\App\Modules\Ambigous, multitype:, multitype:\Illuminate\Database\Query\static , array>
     */
    public static function getMembersAndCircleInfoByCircleId($circleId, $userId, $offset = 0, $limit = self::USER_NUMBER_SIZE) {
        $members = self::getMembersByCircleId($circleId);
        
        $userIds = array();
        foreach($members as $member) {
            $userIds[] = $member->user_id;
        }
        if(count($userIds)) {
            return self::getCirclesInfoOfMembers($userId, $userIds, $offset, $limit);
        }else{
            return array();
        }
    }
    
    /**
     * 查看我的全部圈子的成员情况
     * 
     * @param int $userId
     * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
     */
    public static function getAllMembersAndCircleInfo($userId, $offset = 0, $limit = self::USER_NUMBER_SIZE) {
        return self::getCirclesInfoOfMembers($userId, 0, $offset, $limit);
    }
    
    /**
     * 查看一个成员在我的所有圈子的具体情况
     * 
     * @param int $memberUserId
     * @param int $ownerUserId
     * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
     */
    public static function getMemberAndCircleInfoByMemberUserId($memberUserId, $ownerUserId) {
        $memberInfo = \DB::table('circles')->leftJoin('circle_members', function($join) use ($memberUserId, $ownerUserId) {
            $join->on('circle_members.circle_id', '=', 'circles.id')->where('circle_owner_user_id','=', $ownerUserId)->where('circle_members.user_id', '=', $memberUserId);
        });
        
        $memberInfo->where('circles.user_id', $ownerUserId);
        
        $select = "circles.*,ifnull(circle_members.user_id, 0) as member_id";
        return $memberInfo->select(\DB::raw($select))->get();
    }
    
    /**
     * 对圈子进行排序，默认圈子排在前面
     * 
     * @param array $circles
     */
    public static function sortCirclesByDefault(&$circles) {
        foreach($circles as $key => $circle) {
            if($key == 0){
                continue;
            }
            
            if($circle->is_default == self::TYPE_CIRCLE_DEFAULT) {
                if($circles[$key-1]->is_default == self::TYPE_CIRCLE_DEFAULT && $circles[$key-1]->id > $circle->id) {
                    continue;
                }
                
                $temp = $circles[$key-1];
                $circles[$key-1] = $circle;
                $circles[$key] = $temp;
            }
        }
    }
    
    /**
     * 查看圈我的人
     * 
     * @param int $userId
     * @return array $ownerUserIds
     */
    public static function getCircleMe($userId, $offset = 0, $limit = self::USER_NUMBER_SIZE) {
        $circleMeUsers = CircleMember::where('user_id', $userId)->offset($offset)->limit($limit)->distinct()->get(array('circle_owner_user_id'));
        
        $circleUsers = array();
        $notCircleUsers = array();
        $MyMembers = self::getAllMembersByUserId($userId);
        
        $memberIds = array();
        foreach($MyMembers as $member) {
            $memberIds[] = $member->user_id;
        }

        foreach($circleMeUsers as $user) {
            $ownerUserId = $user->circle_owner_user_id;
            
            if(count($memberIds) && in_array($ownerUserId, $memberIds)) {
                $user = new \stdClass();
                $user->user_id = $ownerUserId;
                $user->type = self::TYPE_MY_CIRCLE;
                $circleUsers[] = $user;
            }else{
                $user = new \stdClass();
                $user->user_id = $ownerUserId;
                $user->type = self::TYPE_CIRCLE_ME;
                $notCircleUsers[] = $user;
            }
        }
        $result = array_merge($notCircleUsers, $circleUsers);       

        return $result;
    }
    
    /**
     * 获取圈我的人个数
     * 
     * @param int $userId
     * @return int
     */
    public static function getCircleMeNum($userId) {
        $result =  CircleMember::where('user_id', $userId)->select(\DB::raw('count(distinct circle_owner_user_id) as num'))->get();
        return $result[0]->num;
    }
    
    /**
     * 获取我的所有成员数或者某个圈子的成员数
     * @param unknown $userId
     * @param unknown $circleId
     */
    public static function getMembersNum($userId, $circleId = 0) {
        $members = CircleMember::where('circle_owner_user_id', $userId);
    
        if($circleId) {
            $members->where('circle_id', $circleId);
        }
    
       $result =  $members->select(\DB::raw("count(distinct user_id) as num"))->get();
        return $result[0]->num;
    }
    
    /**
     * 填充关注信息
     * 
     * @param int $collection
     * @param int $loginUserId
     * @param array $propertys
     */
    public static function fillIsFollow(&$collection, $loginUserId, $propertys = array('user')) {
        if (empty($collection)) {
            return false;
        }
        if ($loginUserId == 0) {
            foreach ($propertys as $property) {
                $collection->$property->is_follow = false;
            }
        } else {
            foreach ($propertys as $property) {
                $collection->$property->is_follow = self::isCircleSomebody($collection->$property->id, $loginUserId);
            }
        }
    }
    
    /**
     * 填充用户组的关注信息
     * 
     * @param array $collections
     * @param int $loginUserid
     * @param array|string $propertys 为空则直接填充到元素本身
     */
    public static function fillIsFollows(&$collections, $loginUserid, $propertys = array('user')) {
        if (! $collections || count($collections) == 0) {
            return false;
        }
        
        if ($loginUserid == 0) {
            foreach ($collections as $collection) {
                if (is_array($propertys)) {
                    foreach ($propertys as $property) {
                        $collection->$property->is_follow = false;
                    }
                }else{
                    $collection->is_follow = false;
                }
            }
        } else {
            $members = self::getAllMembersByUserId($loginUserid);
            $userIds = array();
            foreach ($members as $member) {
                $userIds[] = $member->user_id;
            }
            foreach ($collections as $collection) {
                if (is_array($propertys)) {
                    foreach ($propertys as $property) {
                        $collection->$property->is_follow = false;
                        if (in_array($collection->$property->id, $userIds)) {
                            $collection->$property->is_follow = true;
                        }
                    }
                }else{
                    
                    if(in_array($collection->id, $userIds)) {
                        $collection->is_follow = true;
                    }
                }
            }
        }
    }
    
    /**
     * 获取朋友圈创意排行
     * 
     * @param array $userIds
     * @param number $offset
     * @param number $limit
     * @return multitype:
     */
    public static function getCreativeRankByUserIds($userIds = array(), $offset = 0, $limit = 10) {
          return UserModule::getCreativeRankByUserIds($userIds, $offset, $limit);
    }
    
    public static function getMembersContests($userIds) {
        if(count($userIds)) {
            return ItemModule::getContestByUserIds($userIds);
        }else{
            return array();
        }
    }
    
    /**
     * 删除一个圈子
     *
     * @param int $id
     * @param int $userId
     * @return boolean
     */
    protected static function deleteCircle($id) {
        return Circle::destroy($id);
    }
    
    /**
     * 移除一个圈子中的一个或者多个成员
     *
     * @param int $circleId
     * @param int $ownerUserId
     * @param number $userId
     * @return boolean
     */
    protected static function removeCircleMembers($circleId, $ownerUserId, $userId = 0) {
        $member = CircleMember::where('circle_id', $circleId);
        
        $userIds = array();
        if ($userId) {
            $member->where('user_id', $userId);
            $userIds = array($userId);
        }else{
            $users = CircleMember::where('circle_id', $circleId)->distinct()->get(['user_id']);
            foreach($users as $user) {
                $userIds[] = $user->user_id;
            }
        }
        $num = $member->delete();
               
        if ($num) {
            self::minusUsersFollowerCount($userIds, $ownerUserId);
            return true;          
        }
        
        return false;
    }
    
    /**
     * 查看圈子名是否存在
     * 
     * @param unknown $name
     * @param unknown $userId
     * @return array
     */
    protected static function countCircleByName($name, $userId) {
        return Circle::where('name', $name)->where('user_id', $userId)->count();
    }
    
    /**
     * 获取一个或者多个用户在我的圈子中的情况
     *
     * @param int $ownerUserId
     * @param int|array $userIds
     * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
     */
    protected static function getCirclesInfoOfMembers($ownerUserId, $userIds = array(), $offset = 0, $limit = self::USER_NUMBER_SIZE) {
        $membersInfo = \DB::table('circle_members')->leftJoin('circles', 'circle_members.circle_id', '=', 'circles.id');
        $membersInfo->where('circle_owner_user_id', $ownerUserId);
    
        if (is_array($userIds)) {
            $userIds = array_unique($userIds);
    
            if (count($userIds)) {
                $membersInfo->whereIn('circle_members.user_id', $userIds);
            }
        } elseif ($userIds) {
            $membersInfo->where('circle_members.user_id', $userIds);
        }
    
        $membersInfo->groupBy('circle_members.user_id');
    
        $select = 'count(circle_members.circle_id)as circle_num, circle_members.circle_id,circle_members.user_id,circles.name';
        
        $membersInfo->offset($offset)->limit($limit);
    
        return $membersInfo->select(\DB::raw($select))->get();
    }

    /**
     * 一个或者多个用户的关注数加一
     * 
     * 添加用户到圈子需要更新用户关注数
     * 
     * @param array $userIds
     * @return boolean
     */
    protected static function plusUsersFollowerCount($userIds, $ownerUserId) {
        return self::updateUsersFollowerCount($userIds, $ownerUserId, 1);
        
    }

    /**
     * 一个或者多个用户的关注数减一
     * 
     * 删除用户出圈子、删除圈子需要减少用户关注数
     * 
     * @param array $userIds
     * @return boolean
     */
    protected static function minusUsersFollowerCount($userIds, $ownerUserId) {
       return self::updateUsersFollowerCount($userIds, $ownerUserId, 0);
    }
    
    /**
     * 获取默认圈子
     * 
     * @param int $ownerUserId
     * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\static, NULL>
     */
    protected static function getDefaultCIrcle($ownerUserId) {
        return Circle::where('user_id', $ownerUserId)->where('is_default', self::TYPE_CIRCLE_DEFAULT)->where('name', self::CIRCLE_DEFAULT_NAME)->first();
    }
    
    /**
     * 更新一个或者多个用户的关注数
     * 
     * @param array $userIds
     * @param number $flag
     * @return boolean
     */
    private static function updateUsersFollowerCount($userIds, $ownerUserId, $flag = 0) {
        $select = "count(*) as count, user_id";
        $followers = CircleMember::whereIn('user_id', $userIds)->where('circle_owner_user_id', $ownerUserId)->groupBy('user_id')->select(\DB::raw($select))->get();
        
        foreach ($userIds as $userId) {
            $count = 0;
            foreach ($followers as $follow) {
                if ($follow->user_id == $userId) {
                    $count = $follow->count;
                    break;
                }
            }
            if ($count == $flag) {
                $follownum = CircleMember::where('user_id', $userId)->select(\DB::raw('count(distinct circle_owner_user_id) as num'))->first();
                AccountModule::updateFollowerCount($userId, $follownum->num);
            }
        }
        return true;
    }
}
