<?php

namespace App\Modules;

use App\Models\Encyclopedia;
use App\Models\Industries;
use App\Models\IndustriesExperts;
use App\Models\User;
use App\Models\Tags;
use App\Models\TagsAffairs;
use Carbon\Carbon;

/**
 * 创意百科模块
 */
class EncyclopediaModule extends ItemModule {

	
	/**
	 * 搜索结果-百科
	 *
	 * @param string $keyword        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return 百科信息
	 */
	public static function searchEncyclopediasByKeyword($keyword, $offset = 0, $limit = 5) {
		return Encyclopedia::where ( 'title', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->get ();
	}
	
	public static function searchEncyclopediasByKeywordCount($keyword){
		return Encyclopedia::where ( 'title', 'like', '%' . $keyword . '%' )->count();
	}
	
	/**
	 * 随机获取热门创意
	 *
	 * @param int $num        	
	 *
	 * @return array 热门创意
	 */
	public static function getHotEncyclopedias($num) {
		return \DB::select ( 'select * from encyclopedias order by rand() limit ' . $num );
	}

	/**
	 * 根据行业id获取创意
	 *
	 * @param int $industryId        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @return array
	 */
	public static function getEncyclopediasByIndustryId($industryId, $offset = 0, $limit = 16) {
		return Encyclopedia::where ( 'industry_id', $industryId )->where('status',2)->offset ( $offset )->limit ( $limit )->get ();
	}
	
	/**
	 * 根据行业id统计创意数
	 *
	 * @param int $id        	
	 * @return int
	 */
	public static function getEncyclopediasByIdCount($id) {
		return Encyclopedia::where ( 'industry_id', $id )->count ();
	}
	
	/**
	 * 根据行业id获取标签
	 *
	 * @param int $id        	
	 * @return array
	 */
	public static function getTagsByIndustryId($id, $offset = 0, $limit = 20) {
		return Tags::where ( 'type', $id )->offset ( $offset )->limit ( $limit )->get ();
	}
	/**
	 * 分享创意
	 *
	 * @param string $title        	
	 * @param string $cover        	
	 * @param string $content        	
	 * @param string $tagNames        	
	 *
	 * @return int
	 */
	public static function shareEncyclopedia($userId, $title, $cover, $content, $industryId, $tagsNames = '') {
		$creativity = new Encyclopedia ();
		
		$creativity->user_id = $userId;
		$creativity->item_type = BaseModule::TYPE_ENCYCLOPEDIA;
		$creativity->item_id = 0;
		$creativity->title = $title;
		$creativity->cover = $cover;
		$creativity->content = $content;
		$creativity->industry_id = $industryId;

		$user = UserModule::getUserById ( $userId );

		$creativity->province_id = !empty($user->province_id)?$user->province_id:0;
		$creativity->city_id = !empty($user->city_id)?$user->city_id:0;
		$creativity->district_id = !empty($user->district_id)?$user->district_id:0;
		$creativity->school_id = !empty($user->school_id)?$user->school_id:0;
		$creativity->organization_id = !empty($user->organization_id)?$user->organization_id:0;

		$creativity->creative_index = 0;
		$creativity->tags = '';
		$creativity->status = self::STATUS_WAITING_CHECK;
		$creativity->create_time = time ();
		$creativity->check_remark = '';
		$creativity->save ();
		
		$tags = self::addItemTags ( $creativity->id, $industryId, $tagsNames );
		
		$creativity->tags = json_encode ( $tags );
		$creativity->save ();
		
		\Event::fire ( 'encyclopedia.share', array (
				$userId,
				$creativity->id,
				$title 
		) );
		
		return $creativity->id;
	}
	
	/**
	 * 修改创意百科
	 * 
	 * @param int $userId
	 * @param int $encyclopediaId
	 * @param string $title
	 * @param string $cover
	 * @param string $content
	 * @param int $industryId
	 * @param string $tagsNames
	 * @return boolean|int
	 */
	public static function updateEncyclopedia($userId, $encyclopediaId, $title, $cover, $content, $industryId, $tagsNames = '') {
	    
	    $model = self::getEncyclopediaById($encyclopediaId);
	
	    if ($model->user_id != $userId) {
	        return false;
	    }

		$model->title = $title;
		$model->cover = $cover;
		$model->content = $content;
		$model->industry_id = $industryId;
	
	    self::deleteItemTags($encyclopediaId, $industryId);
	
	    $tags = self::addItemTags($model->id, $industryId, $tagsNames);

		$model->tags = json_encode($tags);
		$model->save();
	
	    \Event::fire('encyclopedia.update', array($encyclopediaId));
	
	    return $model->id;
	}

	/**
	 * 获取审核中的创意
	 */
	public static function getCheckEncyclopedias($status = 0,$offset = 0, $limit = 12) {
		$model = Encyclopedia::offset($offset)->limit($limit)->orderBy('id', 'desc')->orderBy('status', 'desc');
		if($status == 0 || is_array($status) && count($status) == 0) {
			$status = array(self::STATUS_WAITING_CHECK,self::STATUS_CHECK_AGAIN);
		}
		if (is_array($status)) {
			$model->whereIn('status', $status);
		} else {
			$model->where('status', $status);
		}
		return $model->get();
	}

	/**
	 * 通过id获取创意
	 *
	 * @param int $id        	
	 *
	 * @return object
	 */
	public static function getEncyclopediaById($id) {
		return Encyclopedia::find ( $id );
	}
	
	/**
	 * 根据地区信息获取百科
	 *
	 * @param int $pId        	
	 * @param int $cId        	
	 * @param int $dId        	
	 * @return Ambigous <\Illuminate\Database\Query\Builder, static, \Illuminate\Database\Query\static, \Illuminate\Database\Query\Builder>
	 */
	public static function getEncyclopediasByRegionId($id = 0, $offset = 0, $limit = 12) {
		if (strlen ( $id ) == 1) {
			$data = new Encyclopedia ();
		} else if (strlen ( $id ) == 2) {
			$data = Encyclopedia::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = Encyclopedia::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = Encyclopedia::where ( 'district_id', $id );
		} else {
			return array ();
		}
		
		return $data->offset ( $offset )->limit ( $limit )->orderBy('id','desc')->get ();
	}
	
	/**
	 * 
	 * @param unknown $id
	 * @return number|Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getEncyclopediasByRegionIdCount($id){
		if (strlen ( $id ) == 1) {
			$data = new Encyclopedia ();
		} else if (strlen ( $id ) == 2) {
			$data = Encyclopedia::where ( 'province_id', $id );
		} else if (strlen ( $id ) == 4) {
			$data = Encyclopedia::where ( 'city_id', $id );
		} elseif (strlen ( $id ) == 6) {
			$data = Encyclopedia::where ( 'district_id', $id );
		} else {
			return 0;
		}
		
		return $data->count ();
	}
	
	/**
	 * 根据学校ID获取百科
	 *
	 * @param int $id        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getEncyclopediasBySchoolId($id,$offset = 0,$limit= 12) {
		return Encyclopedia::where ( 'school_id', $id )->offset($offset)->limit($limit)->orderBy('id','desc')->get ();
	}

	public static function getEncyclopediasBySchoolIdCount($id) {
		return Encyclopedia::where ( 'school_id', $id )->count ();
	}
	
	/**
	 * 根据政企ID获取百科
	 *
	 * @param int $id        	
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getEncyclopediasByOrganizationId($id,$offset = 0,$limit= 12) {
		return Encyclopedia::where ( 'organization_id', $id )->offset($offset)->limit($limit)->orderBy('id','desc')->get ();
	}

	public static function getEncyclopediasByOrganizationIdCount($id) {
		return Encyclopedia::where ( 'organization_id', $id )->count ();
	}
	
	/**
	 * 根据标签获取列表
	 *
	 * @param int $tagId        	
	 * @param int $offset        	
	 * @param int $limit        	
	 * @param int $targetType        	
	 * @return array
	 */
	public static function getListsByTagId($tagId, $offset, $limit, $targetType = 0) {
		$datas = Encyclopedia::where ( 'industry_id', $targetType )->offset ( $offset )->limit ( $limit )->orderBy ( 'id', 'desc' );
		
		if ($tagId) {
			$tagAffairs = TagsAffairs::where ( 'tag_id', $tagId )->where ( 'affair_type', $targetType )->get ();
			$encyclopediaIds = array ();
			
			foreach ( $tagAffairs as $tagAffair ) {
				$encyclopediaIds [] = $tagAffair->affair_id;
			}
			
			$datas->whereIn ( 'id', $encyclopediaIds );
		}
		return $datas->get ();
	}
	
	/**
	 * 获取某个标签和状态下的百科总条数
	 * 
	 * @param number $tagId        	
	 * @param number $status        	
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getTotalByTagIdAndStatus($id = 0, $tagId = 0, $status = 0) {
		$model = Encyclopedia::where ( 'industry_id', $id );
		if ($tagId) {
			$Ids = array ();
			
			$tagsAffairs = TagsAffairs::where ( 'tag_id', $tagId )->where ( 'affair_type', BaseModule::TYPE_ENCYCLOPEDIA )->get ();
			
			foreach ( $tagsAffairs as $tagsAffair ) {
				$Ids [] = $tagsAffair->affair_id;
			}
			
			if($Ids) {
			    $model->whereIn ( 'id', $Ids );
			}
		}
		
		return $model->count ();
	}
	
	/**
	 * 更改用户的地区信息
	 * 
	 * @param unknown $userId        	
	 * @param unknown $provinceId        	
	 * @param unknown $cityId        	
	 * @param unknown $districtId        	
	 * @param unknown $schoolId        	
	 * @param unknown $organizationId        	
	 */
	public static function updateAreaInfo($userId, $provinceId, $cityId, $districtId, $schoolId, $organizationId) {
		$datas = Encyclopedia::where ( 'user_id', $userId )->get ();
		foreach ( $datas as $data ) {
			$provinceId ? $data->province_id = $provinceId : '';
			$cityId ? $data->city_id = $cityId : '';
			$districtId ? $data->district_id = $districtId : '';
			$schoolId ? $data->school_id = $schoolId : '';
			$organizationId ? $data->organization_id = $organizationId : '';
			$data->save ();
		}
	}

	/**
	 * 计算百科的创意指数
	 * @param unknown $userId
	 */
	public static function getEncyclopediaCreativeIndexByUserId($userId){
		return Encyclopedia::where('user_id',$userId)->sum('creative_index');
	}
	
	/**
	 * 获取审核中的创意
	 */
	public static function getInCheckEncyclopeidas($offset = 0, $limit = 12) {
	    return Encyclopedia::whereIn('status', array(self::STATUS_WAITING_CHECK, self::STATUS_CHECK_AGAIN))->offset($offset)->limit($limit)->orderBy('id', 'desc')->orderBy('status', 'desc')->get();
	}
	
	/**
	 * 根据状态获取审核创意统计数
	 * 
	 * @param int $status
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getCheckEncyclopediasCount($status) {
	    $encyclopedias = Encyclopedia::orderBy('id', 'desc')->orderBy('status', 'desc');
	    if($status == 0 || is_array($status) && count($status) == 0) {
	        $status = array(self::STATUS_WAITING_CHECK,self::STATUS_CHECK_AGAIN);
	    }
	    if (is_array($status)) {
	        $encyclopedias->whereIn('status', $status);
	    } else {
	        $encyclopedias->where('status', $status);
	    }
	    
	    return $encyclopedias->count();
	}
	
	public static function checkEncyclopedia($id, $checkRemark, $status) {
	    $flag =  Encyclopedia::where('id', $id)->update(array('check_remark' => $checkRemark, 'status' => $status));

		if($flag && $status == ItemModule::STATUS_NEED_UPDATE){
			\Event::fire('check.encyclopedia',array($id,$checkRemark));
		}

	}
}