<?php namespace App\Modules;
use App\Models\Hiring;
use App\Models\HiringPrizes;
use App\Models\ExpertInvitesInternal;
use App\Models\HiringParticipators;
use App\Models\TagsAffairs;
use App\Models\Update;
/**
 * 创意公益明星比赛模块
 *
 * 处理创意公益明星比赛功能。
 */

class HiringModule extends ItemModule {

	const PARTICIPATOR_STATUS_APPLY  = 1;
	const PARTICIPATOR_STATUS_CANCEL = 2;
	const PARTICIPATOR_STATUS_WINNER = 3;
	
	/**
	 * 获取最后一个比赛编号
	 *
	 * @return string
	 */
	protected static function getLastItemNo() {
		$last = Hiring::orderBy('id', 'desc')->first();

		return $last ? $last->no : 'C';
	}
	
	/**
	 * 根据HiringId获取参赛者
	 * @param int $hiringId
	 * @param number $offset
	 * @param number $limit
	 * @return boolean|void
	 */
	public static function getParticipatorsByHiringId($hiringId, $offset = 0, $limit = 12) {
		if (!Hiring::find($hiringId)) {
			return false;
		}
		
		$participators = HiringParticipators::where('hiring_id', $hiringId)->whereIn('status',array (self::PARTICIPATOR_STATUS_APPLY, self::PARTICIPATOR_STATUS_WINNER))->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
		
		return $participators;
	}
	
	/**
	 * 获取热门明星比赛
	 * @param number $num
	 * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
	 */
	public static function getHotHiring($num = 3) {
		return Hiring::where('status', '>', '0')->orderBy('creative_index', 'desc')->offset(0)->limit($num)->get();
	}
	
	/**
	 * 获取获取明星比赛排行榜
	 * @param number $num
	 * @return Ambigous <multitype:, \Illuminate\Database\mixed>
	 */
	public static function getParticipatorRank($num = 5) {
		$ranks = \DB::select("select id,user_id,SUM(creative_index) as total_creative_index,declaration from hiring_participators where user_id>0 and status in (1,3) group by user_id order by total_creative_index desc limit $num;");
		return $ranks;
	}
	
	/**
	 * 查询一个参赛者
	 * @param int $participatorId
	 */
	public static function getParticipator($participatorId) {
		$participator = HiringParticipators::find($participatorId);
		
		return $participator;
	}
	
	/**
	 * 添加一个明星比赛动态
	 * @param unknown $hiringId
	 * @param unknown $userId
	 * @param unknown $title
	 * @param unknown $content
	 * @return boolean
	 */
	public static function addUpdate($hiringId, $userId, $title, $content) {
		if(!Hiring::find($hiringId)) {
			return false;
		}
		
		$update = new Update();
		$update->target_type = ItemModule::TYPE_HIRING;
		$update->target_id = $hiringId;
		$update->user_id = $userId;
		$update->title = $title;
		$update->content = $content;
		$update->create_time = time();
		
		$update->save();
		\Event::fire('hiring.update', array($userId, $hiringId));
		return $update->id;
	} 
	
	/**
	 * 通过HiringId查询明星比赛动态
	 * @param int $hiringId
	 * @param number $offset
	 * @param number $limit
	 * @return boolean
	 */
	public static function getUpdatesByHiringId($hiringId, $offset = 0, $limit = 12) {
		if(!Hiring::find($hiringId)) {
			return false;
		}
		
		return Update::where('target_type', ItemModule::TYPE_HIRING)->where('target_id', $hiringId)->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
	}
	
	/**
	 * 创建一个明星比赛
	 * 
	 * @param array $common
	 * @param int $endTime
	 * @param int $hiringType
	 * @param int $userId
	 * @return int $hiringId
	 */
	public static function publish(array $common, $endTime, $hiringType, $prizes, $tagNames = '', $userId) {
		$hiring = new Hiring();

		self::fillPublishCommon($hiring, $common, $userId, self::STATUS_WAITING_CHECK);

		$hiring->start_time = time();
		$hiring->choose_time = 0;
		$hiring->end_time   = $endTime;
		
		$hiring->save();
		
		$totalAmount = static::addHiringPrize($prizes, $hiring->id);
		
		if (!$totalAmount) {
			return false;
		}
		
		$tags = self::addItemTags($hiring->id, BaseModule::TYPE_HIRING, $tagNames);
		
		$hiring->tags = json_encode($tags);
		$hiring->save();
		
		\Event::fire('hiring.publish', array($userId, $hiring->id, $common['title']));

		return $hiring->id;
	}
	
	/**
	 * 修改明星比赛
	 * @param array $common
	 * @param int $hiringId
	 * @param int $userId
	 * @param int $endTime
	 * @param int $hiringType
	 * @param array $prizes
	 * @param string $tagNames
	 * @return boolean
	 */
	public static function updateHiring(array $common, $hiringId, $userId, $endTime, $hiringType, $prizes, $tagNames) {
		$hiring = self::getHiringById($hiringId);
		
		if ($hiring->user_id != $userId) {
			return false;
		}
		
		self::fillPublishCommon($hiring, $common, $userId, self::STATUS_WAITING_CHECK);
		
		$hiring->start_time = time();
		$hiring->choose_time = 0;
		$hiring->end_time   = $endTime;
		
		self::deleteHiringPrize($hiringId);
		
		$totalAmount = static::addHiringPrize($prizes, $hiring->id);
		
		if (!$totalAmount) {
			return false;
		}
		
		$tags = self::addItemTags($hiring->id, BaseModule::TYPE_HIRING, $tagNames);
		
		$hiring->tags = json_encode($tags);
		$hiring->save();
		
		\Event::fire('hiring.update', array($hiringId));
		
		return $hiring->id;
	}
	
	/**
	 * 创建一个明星比赛奖项
	 * 
	 * @param array $prizes
	 * @param int $hiringId
	 * @return boolean|Ambigous <number, unknown>
	 */
	private static function addHiringPrize(array $prizes, $hiringId) {
		$hiring = self::getHiringById($hiringId);

		if (!$hiring) {
			return false;
		}
		
		$totalAmount = 0;
		
		foreach ($prizes as $prize) {
			$hiringPrize = new HiringPrizes();
			$hiringPrize->hiring_id = $hiringId;
			$hiringPrize->title = $prize['title'];
			$hiringPrize->bonus_amount = $prize['amount'];
			$hiringPrize->award = $prize['award'];
			$hiringPrize->winner_user_id = 0;
			$hiringPrize->winner_participator_id = 0;
			
			$hiringPrize->save();
			
			$totalAmount += $prize['amount'];
		}
		
		return $totalAmount;
		
	}
	
	private static function deleteHiringPrize($hiringId) {
		return HiringPrizes::where('hiring_id', $hiringId)->delete();
	}
	
	/**
	 * 创建一个明星比赛参赛者
	 * 
	 * @param array $participatorInfo
	 * @param int $hiringId
	 * @param int $userId
	 * @return int $participatorId
	 */
	public static function applyHiringParticipator(array $participatorInfo, $hiringId, $userId) {
		$hiring = self::getHiringById($hiringId);
		
 		if (!$hiring) {
			return false;
		} 
		
		$participator = new HiringParticipators();
		$participator->hiring_id = $hiringId;
		$participator->works = $participatorInfo['works'];
		$participator->declaration = $participatorInfo['declaration'];
		$participator->cover = $participatorInfo['cover'];
		$participator->user_id = $userId;
		$participator->title = $hiring->title;
		$participator->status = self::PARTICIPATOR_STATUS_APPLY;
		$participator->creative_index = 0;
		$participator->create_time = time();
		
		$participator->save();
		$participatorId = $participator->id;
		
		\Event::fire('hiring.apply', array($userId, $hiringId,$participatorId));
		
		return $participator->id;
	}
	
	/**
	 * 撤回明星作品
	 * @param int $participatorId
	 * @param int $userId
	 * @return boolean
	 */
	public static function cancelApply($participatorId, $userId) {
		$participator = HiringParticipators::find($participatorId);
		
		if (!$participator || $userId != $participator->user_id || !Hiring::find($participator->hiring_id)) {
			return false;
		}
		
		$participator->status = self::PARTICIPATOR_STATUS_CANCEL;
		$participator->save();
		
		\Event::fire('hiring.cancel-apply', array($participator->hiring_id, $participatorId));
		
		return true;
	}
	
	/**
	 * 修改明星作品
	 * @param int $participatorId
	 * @param array $participatorInfo
	 * @param int $userId
	 * @return boolean
	 */
	public static function updateParticipator($participatorId, array $participatorInfo, $userId) {
		$participator = HiringParticipators::find($participatorId);
		
		if (!$participator || $userId != $participator->user_id || !Hiring::find($participator->hiring_id)) {
			return false;
		}
		
		$participator->cover = $participatorInfo['cover'] ? $participatorInfo['cover'] : $participator->cover;
		$participator->works = $participatorInfo['works'] ? $participatorInfo['works'] : $participator->works;
		$participator->declaration = $participatorInfo['declaration'] ? $participatorInfo['declaration'] : $participator->declaration;
		
		$participator->save();
		
		return true;
	}

	/**
	 * 选取获胜明星
	 * @param int $participatorId
	 * @param int $userId
	 * @return boolean
	 */
	public static function chooseWinner($participatorId, $userId, $prizeId) {
		$participator = HiringParticipators::find($participatorId);
		
		if (!$participator) {
			return false;
		}
		
		$hiring = Hiring::find($participator->hiring_id);

		if (!$hiring || $hiring->user_id != $userId || $hiring->status != self::STATUS_GOING) {
			return false;
		}
		
		$prize = HiringPrizes::find($prizeId);
		
		if (!$prize || $prize->hiring_id != $hiring->id) {
			return false;
		}
		
		$prize->winner_user_id = $participator->user_id;
		$prize->winner_participator_id = $participatorId;
		$prize->save();
		
		$participator->status = self::PARTICIPATOR_STATUS_WINNER;
		$participator->save();
				
		$isOver = HiringPrizes::where('hiring_id', $hiring->id)->where('winner_user_id', 0)->first();
		
		if (!$isOver) {
			$hiring->choose_time = time();
			$hiring->status = ItemModule::STATUS_END_SUCCESS;
			$hiring->save();
		}
			
		\Event::fire('hiring.choose-winner', array($hiring->id, $participatorId, $participator->user_id, $userId));

		return true;
	}
	
	/**
	 * 根据ID获取比赛
	 *
	 * @param int $id
	 * @return Hiring|null
	 */
	public static function getHiringById($id) {
		return Hiring::find($id);
	}
	
	/**
	 * 判断用户是否能参赛
	 * @param int $hiringId
	 * @param int $userId
	 * @return boolean
	 */
	public static function canApply($hiringId, $userId) {
		$hiring = self::getHiringById($hiringId);
		
		if (!$hiring || $hiring->user_id == $userId || $hiring->status != ItemModule::STATUS_GOING) {
			return false;
		}
		
		$isApply = HiringParticipators::where('hiring_id', $hiringId)->where('user_id', $userId)->whereIn('status', array(self::PARTICIPATOR_STATUS_APPLY, self::PARTICIPATOR_STATUS_WINNER))->first();
		
		if ($isApply) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * 根据状态获取列表
	 * @param int $status
	 * @param int $page
	 * @param int $limit
	 *
	 * @return array|static
	 */
	public static function getListsByStatus($status, $offset, $limit){
		$hiring = Hiring::offset($offset)->limit($limit)->orderBy('id','desc');
		if($status){
			$hiring->where('status',$status);
		}else{
			$hiring->where('status','>',0);
		}
		return $hiring->get();
	}
	
	
	/**
	 * 根据标签筛选列表页
	 * 
	 * @param int $tagId
	 * @param int $page
	 * @param int $limit
	 * @return array
	 */
	public static function getListsByTagId($tagId = 0, $status = 0, $offset = 0, $limit = 12) {
	    $hirings = Hiring::offset($offset)->limit($limit)->orderBy('id', 'desc');
		
	    if ($tagId) {
		    $tagAffairs = TagsAffairs::where('tag_id', $tagId)->where('affair_type', BaseModule::TYPE_HIRING)->get();
		    $hiringIds = array();
		    
		    foreach ($tagAffairs as $tagAffair) {
		        $hiringIds[] = $tagAffair->affair_id;
		    }
	    	
		    $hirings->whereIn('id', $hiringIds);
	    }
	    
	    if ($status) {
	    	$hirings->where('status', $status);
	    } else {
    		$hirings->where('status', '>', self::STATUS_WAITING);
    	}
	    
	    return $hirings->get();
	} 
	
	/**
	 * 获取某个标签和状态下的比赛总条数
	 * @param number $tagId
	 * @param number $status
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getTotalByTagIdAndStatus($tagId = 0,$status =0){
		$hirings = Hiring::orderBy('id','desc');
		if ($tagId) {
			$projectIds = array();
	
			$tagsAffairs = TagsAffairs::where('tag_id',$tagId)->where('affair_type',BaseModule::TYPE_HIRING)->get();
	
			foreach($tagsAffairs as $tagsAffair){
				$projectIds[] = $tagsAffair->affair_id;
			}
	
			$hirings->whereIn('id',$projectIds);
		}
	
		if ($status) {
			$hirings->where('status',$status);
		} else {
			$hirings->where('status','>',0);
		}
	
		return $hirings->count();
	}
	
	/**
	 * 获取明星比赛奖项
	 * @param int $hiringId
	 */
	public static function getHiringPrizes($hiringId) {
		return HiringPrizes::where('hiring_id', $hiringId)->orderBy('bonus_amount', 'desc')->get();
	}
	
	/**
	 * 获取明星比赛参赛明星数量
	 */
	public static function getParticipatorNum($hiringId) {
		return HiringParticipators::where('hiring_id', $hiringId)->whereIn('status', array(self::PARTICIPATOR_STATUS_APPLY, self::PARTICIPATOR_STATUS_WINNER))->count();
	}
	
	/**
	 * 根据状态获取参赛明星
	 *
	 * @param int $hiringId
	 * @param int $status
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getHiringParticipators($hiringId, $status = array(self::PARTICIPATOR_STATUS_APPLY, self::PARTICIPATOR_STATUS_WINNER)) {
	    $participators = HiringParticipators::where('hiring_id', $hiringId)->where('user_id', '>', 0);
	    if(is_array($status)) {
	        $participators->whereIn('status', $status);
	    }else{
	        $participators->where('status', $status);
	    }
	    return $participators->get();
	}

	public static function getPaticipatorById($id) {
	    return HiringParticipators::find($id);
	}
	
	public static function getPaticipatorByIds($ids) {
	    return HiringParticipators::findMany($ids);
	}
	
	/**
	 * 处理到期
	 */
	public static function processExpiration() {
	    $hirings = Hiring::where('status', self::STATUS_GOING)->get();
	    $nowTime = time();
	    
	    foreach($hirings as $hiring) {
	    	if ($hiring->end_time >0) {
		        if($hiring->end_time <= $nowTime + 86400 * 3 && $hiring->end_time > $nowTime + 86400 * 2) {
		            //比赛剩余3天提醒
		            if(! isset($hiring->end_soon_remind) || $hiring->end_soon_remind == 0) {
		                \Event::fire('hiring.deadline-three', array($hiring->id, $hiring->user_id));
		                $hiring->end_soon_remind = 1;
		                $hiring->save();
		            }
		        }elseif($hiring->end_time <= $nowTime ) {
		            //比赛关闭
		            $hiring->status = self::STATUS_END_FAIL;
		            $hiring->save();
		            \Event::fire('hiring.past-due', array($hiring->id, $hiring->user_id));
		        }
	    	}
	    }
	}


}