<?php

namespace App\Modules;

use Illuminate\Support\Facades\Event;
use App\Modules\AccountModule;
use App\Modules\NotificationModule;
use App\Modules\GuessModule;
use App\Models\User;
use App\Models\Notification;
class CoinModule extends BaseModule {
	protected static function getUserIdByTypeAndId($targetType, $targetId) {
		switch ($targetType) {
			case self::TYPE_PROJECT :
				$project = ProjectModule::getProjectById ( $targetId );
				return $project->user_id;
				break;
			case self::TYPE_TASK :
				$task = TaskModule::getTaskById ( $targetId );
				return $task->user_id;
				break;
			case self::TYPE_TASK_WORK :
				$taskWork = TaskModule::getTaskWorkById ( $targetId );
				return $taskWork->user_id;
				break;
			case self::TYPE_HIRING :
				$hiring = HiringModule::getHiringById ( $targetId );
				return $hiring->user_id;
				break;
			case self::TYPE_HIRING_WORK :
				$participator = HiringModule::getPaticipatorById ( $targetId );
				return $participator->user_id;
				break;
			case self::TYPE_EXCHANGE :
				$exchange = ExchangeModule::getTradeById ( $targetId );
				return $exchange->user_id;
				break;
			case self::TYPE_ENCYCLOPEDIA :
				$encyclopedia = EncyclopediaModule::getEncyclopediaById ( $targetId );
				return $encyclopedia->user_id;
				break;
		}
	}
	public static function listenEvent() {
		
		// 评论
		Event::listen ( 'comment.publish', function ($userId, $targetType, $targetId) {
		    $targetUserId = self::getUserIdByTypeAndId($targetType, $targetId);
              if ($userId == $targetUserId) {
                 return true;
              }                    
              $coinArr = array(
                      BaseModule::TYPE_PROJECT,
                      BaseModule::TYPE_TASK,
                      BaseModule::TYPE_HIRING,
                      BaseModule::TYPE_TASK_WORK,
                      BaseModule::TYPE_HIRING_WORK,
                      BaseModule::TYPE_EXCHANGE,
                      BaseModule::TYPE_ENCYCLOPEDIA
              );
		    if(! in_array($targetType, $coinArr)) {
		        return true;
		    }
		    
			$flag = CommentModule::isCoined ( $userId, $targetType, $targetId );
			if ($flag) {
				 self::sendNotificationCoin ( $userId, '您获得了提建议奖励，点击领取奖励', NotificationModule::TYPE_COIN, 5 ,'提建议奖励');
			}
		});
		
		// 评分
		Event::listen ( 'creative-value-rating.publish', function ($targetType, $targetId, $score, $userId) {
			
			self::sendNotificationCoin ( $userId, '您获得了创意价值投票奖励，点击领取奖励', NotificationModule::TYPE_COIN, 5, '创意价值投票奖励奖励' );
		} );
		
		// 专家点评了比赛或交易
		Event::listen ( 'expert-comment.publish', function ($targetType, $targetId,$targetSubId,$score,$userId) {
			
			self::sendNotificationCoin ( $userId, '您获得了发表专家点评奖励，点击领取奖励', NotificationModule::TYPE_COIN, 15, '发表专家点评奖励');
			
			$userId = self::getUserIdByTypeAndId ( $targetType, $targetId );
			
			self::sendNotificationCoin ( $userId, '恭喜您获得了新的专家点评，点击领取奖励', NotificationModule::TYPE_COIN, 15, '获得专家点评奖励');
		} );
		
		// 投资项目比赛接受投资
		
		Event::listen ( 'project.accept-invest', function ($projectId, $investId, $userId) {
			
			$experts = ExpertModule::getExpertComments ( $projectId, self::TYPE_PROJECT );
			if ($experts) {
				foreach ( $experts as $expert ) {
					self::sendNotificationCoin ( $expert->user_id, '您点评的项目成功获得了投资，马上领取您的奖励', NotificationModule::TYPE_COIN, 15, '您点评的项目成功获得了投资');
				}
			}
		} );
		
		// 任务人才比赛选出获胜人才
		
		Event::listen ( 'task.choose-winner', function ($taskId, $workId, $userId) {
			
			$experts = ExpertCommentModule::getExpertComments ( self::TYPE_TASK, $taskId, $workId, 0, 1000 );
			if ($experts) {
				foreach ( $experts as $expert ) {
					self::sendNotificationCoin ( $expert->user_id, '您点评的任务人才被选为了获胜人才，马上领取您的奖励', NotificationModule::TYPE_COIN, 15, '您点评的任务人才被选为了获胜人才');
				}
			}
		} );
		// 明星比赛选取获胜明星
		
		Event::listen ( 'hiring.choose-winner', function ($hiringId, $participatorId, $participatorUserId, $userId) {
			
			$experts = ExpertCommentModule::getExpertComments (self::TYPE_HIRING, $hiringId, $participatorId,0, 1000);
			if ($experts) {
				foreach ( $experts as $expert ) {
					self::sendNotificationCoin ( $expert->user_id, '您点评的参赛明星被选为了获奖明星，马上领取您的奖励', NotificationModule::TYPE_COIN, 15, '您点评的参赛明星被选为了获奖明星');
				}
			}
		} );
		
		// 连续登陆
		Event::listen ( 'user.login', function ($userId) {
			$user = UserModule::getUserById ( $userId );
			$countDays = $user->consistent_login_days;
			if ($countDays > 4) {
				$countDays = 4;
			}
			switch ($countDays) {
				case 1 :
					self::sendNotificationCoin ( $userId, '您连续1天登录，点击领取奖励，明天将获得更多奖励', NotificationModule::TYPE_COIN, 5, '连续登录奖励');
					break;
				case 2 :
					self::sendNotificationCoin ( $userId, '您连续2天登录，点击领取奖励，明天将获得更多奖励', NotificationModule::TYPE_COIN, 10, '连续登录奖励');
					break;
				case 3 :
					self::sendNotificationCoin ( $userId, '您连续3天登录，点击领取奖励，明天将获得更多奖励', NotificationModule::TYPE_COIN, 15, '连续登录奖励');
					break;
				case 4 :
					self::sendNotificationCoin ( $userId, '您获得了连续登录奖励，点击领取奖励', NotificationModule::TYPE_COIN, 30, '连续登录奖励');
					break;
				default:;
			}
		} );
		
		// 使用创创召
		Event::listen ( 'user.ccz', function ($user) {
		    if (empty($user->ccz_used)) {
		        self::sendNotificationCoin ( $user->id, '您获得了使用“创一下”奖励，点击领取奖励', NotificationModule::TYPE_COIN, 10, '使用“创一下”奖励');;
		        $user->ccz_used = 1;
		        $user->save();
		    }
		} );
		
		// 注册激活成功
		Event::listen ( 'user.activate', function ($userId) {
			self::sendNotificationCoin ( $userId, '恭喜您，踏上创意之旅，点击领取奖励', NotificationModule::TYPE_COIN, 10, '踏上创意之旅奖励');
		} );
		
		// 完善地区资料
		Event::listen ( 'user.area-profile-completed', function ($userId) {
			
			$user = User::find($userId);
			if (! $user->fill_area_profile) {
				self::sendNotificationCoin ( $userId, '您获得了完善创意世界地区资料奖励，点击领取奖励', NotificationModule::TYPE_COIN, 30, '完善创意世界地区资料奖励');
				
				AccountModule::fillAreaProfile ( $user );
			}
		} );
		
		// 升级
		Event::listen ( 'user.update-level', function ($userId,$oldLevel,$newLevel) {
			self::sendNotificationCoin ( $userId, '恭喜您从'.$oldLevel.'升级到'.$newLevel.'，点击领取奖励', NotificationModule::TYPE_COIN, $newLevel * 10, '创意等级提升奖励');
		} );
		
		//竞猜
		
		//竞猜投资项目比赛成功获胜
		Event::listen('project.accept-invest', function ($projectId, $investId, $userId) {
		    $guess = GuessModule::getGuessByTargetIdAndTypeAndStatus($projectId, BaseModule::TYPE_PROJECT, 1);
		    
		    $project = ProjectModule::getProjectById($projectId);
		    
		    $total = $project->guess_total_amount;
		    $success = $project->guess_success_amount;
		    
		    if (count($guess)) {
		        foreach ($guess as $value) {
		            $amount = self::getGuessAward($total, $success, $value->amount, 1);
		            self::sendNotificationCoin($value->user_id, '恭喜您，您在《'.$project->title.'》中竞猜获胜，点击领取奖励', NotificationModule::TYPE_COIN, $amount, '《'.$project->title.'》竞猜获胜');
		        }
		    }
		} );
		
		//竞猜投资项目比赛失败获胜
		    Event::listen('project.past-due', function ($projectId, $userId) {
		        $guess = GuessModule::getGuessByTargetIdAndTypeAndStatus($projectId, BaseModule::TYPE_PROJECT, 0);
		    
		        $project = ProjectModule::getProjectById($projectId);
		    
		        $total = $project->guess_total_amount;
		        $success = $project->guess_success_amount;
		    
		        if (count($guess)) {
		            foreach ($guess as $value) {
		                $amount = self::getGuessAward($total, $success, $value->amount, 0);
		                self::sendNotificationCoin($value->user_id, '恭喜您，您在《'.$project->title.'》中竞猜获胜，点击领取奖励', NotificationModule::TYPE_COIN, $amount, '《'.$project->title.'》竞猜获胜');
		            }
		        }
		    });
		//竞猜任务人才比赛成功获胜
		        Event::listen('task.choose-winner', function ($taskId, $workId, $userId, $winnerUserId) {
		            $guess = GuessModule::getGuessByTargetIdAndTypeAndStatus($taskId, BaseModule::TYPE_TASK, 1);
		        
		            
		            $task = TaskModule::getTaskById($taskId);
		            
		            $total = $task->guess_total_amount;
		            $success = $task->guess_success_amount;
		        
		            if (count($guess)) {
		                foreach ($guess as $value) {
		                    $amount = self::getGuessAward($total, $success, $value->amount, 1);
		                    self::sendNotificationCoin($value->user_id, '恭喜您，您在《'.$task->title.'》中竞猜获胜，点击领取奖励', NotificationModule::TYPE_COIN, $amount, '《'.$task->title.'》竞猜获胜');
		                }
		            }
		        });
		        
		//竞猜任务人才比赛失败获胜
		        Event::listen('task.past-due', function ($taskId, $userId) {
		            $guess = GuessModule::getGuessByTargetIdAndTypeAndStatus($taskId, BaseModule::TYPE_TASK, 0);
		        
		            
		            $task = TaskModule::getTaskById($taskId);
		            
		            $total = $task->guess_total_amount;
		            $success = $task->guess_success_amount;
		        
		            if (count($guess)) {
		                foreach ($guess as $value) {
		                    $amount = self::getGuessAward($total, $success, $value->amount, 0);
		                    self::sendNotificationCoin($value->user_id, '恭喜您，您在《'.$task->title.'》中竞猜获胜，点击领取奖励', NotificationModule::TYPE_COIN, $amount, '《'.$task->title.'》竞猜获胜');
		                }
		            }
		        });
		        
		//竞猜公益明星比赛成功获胜
		        Event::listen('hiring.choose-winner', function ($hiringId, $participatorId, $winnerUserId, $userId) {
		            $guess = GuessModule::getGuessByTargetIdAndTypeAndStatus($hiringId, BaseModule::TYPE_HIRING, 1);
		        
		            
		            $hiring = HiringModule::getHiringById($hiringId);
		            
		            $total = $hiring->guess_total_amount;
		            $success = $hiring->guess_success_amount;
		        
		            if (count($guess)) {
		                foreach ($guess as $value) {
		                    $amount = self::getGuessAward($total, $success, $value->amount, 1);
		                    self::sendNotificationCoin($value->user_id, '恭喜您，您在《'.$hiring->title.'》中竞猜获胜，点击领取奖励', NotificationModule::TYPE_COIN, $amount, '《'.$hiring->title.'》竞猜获胜');
		                }
		            }
		        });
		        
		//竞猜公益明星比赛成功获胜
		        Event::listen('hiring.past-due', function ($hiringId, $userId) {
		            $guess = GuessModule::getGuessByTargetIdAndTypeAndStatus($hiringId, BaseModule::TYPE_HIRING, 0);
		        
		            
		            $hiring = HiringModule::getHiringById($hiringId);
		            
		            $total = $hiring->guess_total_amount;
		            $success = $hiring->guess_success_amount;
		        
		            if (count($guess)) {
		                foreach ($guess as $value) {
		                    $amount = self::getGuessAward($total, $success, $value->amount, 0);
		                    self::sendNotificationCoin($value->user_id, '恭喜您，您在《'.$hiring->title.'》中竞猜获胜，点击领取奖励', NotificationModule::TYPE_COIN, $amount, '《'.$hiring->title.'》竞猜获胜');
		                }
		            }
		        });
		        
		        
	}
	
	/**
	 * 生成消息
	 *
	 * @param int $userId
	 * @param string $content
	 * @param int $targetType
	 * @param string $info
	 *
	 * @return int 消息的ID
	 */
	public static function sendNotificationCoin($userId, $content, $targetType, $amount, $info) {
	    
	    $value = array(
	        'target_id' => $amount,
	        'coin_info' => $info
	        );
	    $notification = new Notification();
	    $notification->user_id = $userId;
	    $notification->content = $content;
	    $notification->target_type = $targetType;
	    $notification->info = json_encode($value);
	    $notification->status = NotificationModule::STATUS_UNREAD;
	    $notification->create_time = time();
	    $notification->save();
	    return $notification->id;
	}
	
	/**
	 * 计算竞猜奖金
	 * 
	 * @param int $total
	 * @param int $success 猜竞猜获胜的奖金
	 * @param int $status
	 * @return number
	 */
	public static function getGuessAward($total, $success ,$amount, $status) {
	    
	    if($total == $success || $success == 0){
	        return round($amount*1.01);
	    }else{
	        if($status == 1){
	            return round($amount * ($total/$success - 0.05));
	        }else{
	            return round($amount * ($total/($total-$success) - 0.05));
	        }
	    }
	}
}