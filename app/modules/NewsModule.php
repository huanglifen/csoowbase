<?php namespace App\Modules;

use App\Models\News;
use App\Models\School;
use App\Models\Region;
use App\Models\Organization;
class NewsModule extends BaseModule {

	const TYPE_COMMON = 1;  //普通新闻
	const TYPE_HEADLINE    = 2; //头条新闻

	const CATEGORY_HOT = 1; // 热门
	const CATEGORY_CREATIVE = 2; // 创意
	const CATEGORY_POLITICAL = 3; // 政治
	const CATEGORY_MONEY = 4; // 财政
	const CATEGORY_SPORTS = 5; // 体育
	const CATEGORY_WAR = 6; // 军事
	const CATEGORY_TECH = 7; // 科技
	const CATEGORY_EXPERT = 8; // 专家
	const CATEGORY_AREA = 9; //地区

	/**
	 * 添加新闻
	 * @param        $title
	 * @param int    $areaId
	 * @param string $sourceUrl
	 */
	public static function createNews($title,$sourceUrl = '', $areaId = 0) {
	    $new = new News();
	    $new->title = $title;
		$new->source_url = $sourceUrl;
		$new->area_id = $areaId;
	    $new->content = '';
	    $new->cover = '';
	    $new->type = 0;
	    $new->category = 0;
	    $new->source = '';
	    $new->create_time = time();
	    $new->save();
	}

	public static function searchNewsBySourceUrl($sourceUrl){
		return News::where('source_url',$sourceUrl)->count() > 0?1:0;
	}

	/**
	 * 随机获取7条热点
	 * @param int $limit
	 * @return array
	 */
	public static function getHeadlineNews($limit = 7) {
	    $news = \DB::select ( 'SELECT * FROM `news` AS t1
JOIN (
SELECT ROUND( RAND() * (( SELECT MAX(id) FROM `news`)-(SELECT MIN(id) FROM `news`) ) + (SELECT MIN(id) FROM `news`) ) AS id from `news`  where area_id = 0 limit 70
) AS t2
on t1.id = t2.id
LIMIT '.$limit);
	    return $news;
	}
	
	/**
	 * 根据schoolid获取新闻
	 * @param int $schoolId
	 * @param number $offset
	 * @param number $limit
	 */
	public static function getNewsBySchoolId($schoolId, $offset = 0, $limit = 4) {
		$school = School::find($schoolId);
		
		return self::getNewsByProvinceId($school->province_id, $offset, $limit);
	}
	
	/**
	 * 根据RegionId获取新闻
	 * @param int $regionId
	 * @param number $offset
	 * @param number $limit
	 */
	public static function getNewsByRegionId($regionId, $offset = 0, $limit = 4) {
		$region = Region::find($regionId);
		
		$provinceId = $region->province_id ? $region->province_id : $region->id;
		
		return self::getNewsByProvinceId($provinceId, $offset, $limit);
	}
	
	/**
	 * 根据organizationId获取新闻
	 * @param int $organizationId
	 * @param number $offset
	 * @param number $limit
	 */
	public static function getNewsByOrganizationId($organizationId, $offset = 0, $limit = 4) {
		$organization = Organization::find($organizationId);
		
		return self::getNewsByProvinceId($organization->province_id, $offset, $limit);
	}
	
	/**
	 * 根据省ID获取4条新闻
	 * @param int $provinceId
	 * @param number $offset
	 * @param number $limit
	 */
	public static function getNewsByProvinceId($provinceId, $offset = 0, $limit = 4) {
		return News::where('area_id', $provinceId)->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
	}
}