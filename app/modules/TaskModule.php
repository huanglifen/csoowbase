<?php namespace App\Modules;
use App\Models\Task;
use App\Models\TagsAffairs;
use App\Models\TaskWork;
use App\Models\Update;
/**
 * 创意任务人才比赛模块
 *
 * 处理创意任务人才比赛功能。
 */

class TaskModule extends ItemModule {

	const TASK_WORK_STATUS_SUBMIT    = 1; //发布成功的作品
	const TASK_WORK_STATUS_CANCEL    = 2; //撤回
	const TASK_WORK_STATUS_ELIMINATE = 3; //淘汰
	const TASK_WORK_STATUS_WINNER    = 4; //获胜
	
	/**
	 * 获取最后一个比赛编号
	 *
	 * @return string
	 */
	protected static function getLastItemNo() {
		$last = Task::orderBy('id', 'desc')->first();

		return $last ? $last->no : 'M';
	}
	
	/**
	 * 获取任务人才排行榜
	 * @param number $num
	 * @return Ambigous <multitype:, \Illuminate\Database\mixed>
	 */
	public static function getTaskRanks($num = 5) {
		$ranks = \DB::select("select id,user_id,SUM(creative_index) as total_creative_index from task_works where user_id>0 and status in (1,4) group by user_id order by total_creative_index desc limit $num;");
		
		foreach ($ranks as $rank) {
			$rank->task_work_num = TaskWork::where('user_id', $rank->user_id)->count();
		}
		
		return $ranks;
	}
	
	/**
	 * 获取热门人才比赛
	 * @param number $num
	 * @return Ambigous <multitype:, multitype:\Illuminate\Database\Query\static , array>
	 */
	public static function getHotTasks($num = 3) {
		return Task::where('status', '>', '0')->orderBy('creative_index', 'desc')->offset(0)->limit($num)->get();
	}

	/**
	 * 创建一个创意任务人才比赛
	 * 
	 * @param array $common
	 * @param int $bonusAmount
	 * @param int $workEndTime
	 * @param int $userId
	 * 
	 * @return int $taskId
	 */
	public static function publish(array $common, $awards, $workEndTime, $tagNames = '', $userId) {
		$task = new Task();

		self::fillPublishCommon($task, $common, $userId, self::STATUS_WAITING_CHECK);
		$task->end_time = $workEndTime;
		$task->awards   = $awards;
				
		$task->save();
		
		$tags = self::addItemTags($task->id, BaseModule::TYPE_TASK, $tagNames);

		$task->tags = json_encode($tags);
		$task->save();
		
		\Event::fire('task.publish', array($userId, $task->id, $common['title']));
		
		return $task->id;
	}
	
	/**
	 * 修改任务人才比赛
	 * @param array $common
	 * @param string $awards
	 * @param int $workEndTime
	 * @param int $userId
	 * @param int $taskId
	 * @param string $tagNames
	 * @return boolean
	 */
	public static function updateTask(array $common, $awards, $workEndTime, $userId, $taskId, $tagNames = '') {
		$task = $this->getTaskById($taskId);
		
		if ($task->user_id != $userId) {
			return false;
		}
		
		self::fillPublishCommon($task, $common, $userId, self::STATUS_WAITING_CHECK);
		$task->end_time = $workEndTime;
		$task->awards   = $awards;
		
		self::deleteItemTags($taskId, self::TYPE_TASK);
		
		$tags = self::addItemTags($task->id, BaseModule::TYPE_TASK, $tagNames);
		
		$task->tags = json_encode($tags);
		$task->save();
		
		\Event::fire('task.update', array($taskId));
		
		return $task->id;
	}
	
	/**
	 * 创建一个任务作品
	 * @param int $taskId
	 * @param string $cover
	 * @param string $works
	 * @param int $endTime
	 * @param int $userId
	 * @return boolean
	 */
	public static function submitWork($taskId, $cover, $works, $endTime, $userId) {
		$task = self::getTaskById($taskId);
		
		if (!self::canApply($taskId, $userId)) {
			return false;
		}
		
		$taskWork = new TaskWork();
		$taskWork->task_id = $taskId;
		$taskWork->cover = $cover;
		$taskWork->works = $works;
		$taskWork->promise_complete_time = $endTime ? $endTime : 0;
		$taskWork->user_id = $userId;
		$taskWork->status = self::TASK_WORK_STATUS_SUBMIT;
		$taskWork->creative_index = 0;
		$taskWork->winning_time = 0;
		$taskWork->title = $task->title;
		$taskWork->create_time = time();
		
		$taskWork->save();
		$taskWorkId = $taskWork->id;
		
		\Event::fire('task.submit-work', array($userId, $taskId,$taskWorkId));
		
		return $taskWork->id;
	} 
	
	/**
	 * 根据Taskid获取参赛作品
	 * @param int $taskId
	 * @param number $offset
	 * @param number $limit
	 * @param boolean $isWinner
	 * @return boolean
	 */
	public static function getTaskWorkByTaskId($taskId, $offset = 0, $limit = 12, $isWinner = false) {
		if (!Task::find($taskId)){
			return false;
		}
		
		$works = TaskWork::where('task_id', $taskId)->offset($offset)->limit($limit)->orderBy('id', 'desc');
		
		if ($isWinner) {
			$works->where('status', self::TASK_WORK_STATUS_WINNER);
		} else {
			$works->whereIn('status', array(self::TASK_WORK_STATUS_SUBMIT, self::TASK_WORK_STATUS_WINNER));
		}
		
		return $works->get();
	}
	
	/**
	 * 获取一个人才比赛的作品数量
	 * @param unknown $taskId
	 * 
	 * @return num
	 */
	public static function getWorkNumByTaskId($taskId) {
		return TaskWork::where('task_id', $taskId)->whereIn('status', array(self::TASK_WORK_STATUS_SUBMIT, self::TASK_WORK_STATUS_WINNER))->orderBy('id', 'desc')->count();
	}
	
	/**
	 * 撤回作品
	 * @param int $workId
	 * @param int $userId
	 * @return boolean
	 */
	public static function cancelWork($workId, $userId) {
		$work = TaskWork::find($workId);
		
		if (!$work || $work->user_id != $userId) {
			return false;
		}
		
		$work->status = self::TASK_WORK_STATUS_CANCEL;
		$work->save();
		
		\Event::fire('task.cancel-work', array($work->task_id, $workId));
		
		return true;
	}
	
	/**
	 * 修改作品
	 * @param int $workId
	 * @param int  $userId
	 * @param array $workInfo
	 * @return boolean
	 */
	public static function updateWork($workId, $userId, array $workInfo) {
		$work = TaskWork::find($workId);
		
		if (!$work || $work->user_id != $userId) {
			return false;
		}
		
		$work->cover = $workInfo['cover'] ? $workInfo['cover'] : $work->cover;
		$work->works = $workInfo['works'] ? $workInfo['works'] : $work->works;
		$work->promise_complete_time = $workInfo['endTime'] ? $work->promise_complete_time : 0;
		
		$work->save();
		
		return true;
	}
	
	/**
	 * 选出获胜者
	 * @param int $workId
	 * @param int $userId
	 * @return boolean
	 */
	public static function chooseWinner($workId, $userId) {
		$work = TaskWork::find($workId);
		
		if (!$work) {
			return false;
		}
		
		$task = self::getTaskById($work->task_id);
		
		if (!$task || $task->user_id != $userId || $task->status != self::STATUS_GOING) {
			return false;
		}
		
		$work->winning_time = time();
		$work->status = self::TASK_WORK_STATUS_WINNER;
		$work->save();
		
		$task->status = ItemModule::STATUS_END_SUCCESS;
		$task->save();
		
		\Event::fire('task.choose-winner', array($task->id, $workId, $userId, $work->user_id));

		return true;
	}
	
	/**
	 * 获取人才比赛获胜者
	 * @param unknown $taskId
	 * @return boolean
	 */
	public static function getTaskWinner($taskId) {
		$task = self::getTaskById($taskId);
		
		if (!$task || !in_array($task->status, array(ItemModule::STATUS_END_SUCCESS, ItemModule::STATUS_WAITING_CONFIRM))) {
			return false;
		}
		
		return TaskWork::where('task_id', $taskId)->where('status', self::TASK_WORK_STATUS_WINNER)->first();
	}
	
	/**
	 * 判断用户能否参赛
	 * @param int $taskId
	 * @param int $userId
	 * @return boolean
	 */
	public static function canApply($taskId, $userId) {
		$task = self::getTaskById($taskId);
		
		if ($task->status != ItemModule::STATUS_GOING) {
			return false;
		}
		
		if (!$task || $task->user_id == $userId) {
			return false;
		}
		
		$isApply = TaskWork::where('task_id', $taskId)->where('user_id', $userId)->whereIn('status', array(self::TASK_WORK_STATUS_SUBMIT, self::TASK_WORK_STATUS_WINNER))->first();
		
		if ($isApply) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * 创建一个任务人才比赛动态
	 * @param int $taskId
	 * @param int $userId
	 * @param string $title
	 * @param string $content
	 * @return boolean
	 */
	public static function createTaskUpdate($taskId, $userId, $title, $content) {
		$task = self::getTaskById($taskId);
		
		if (!$task || $task->user_id != $userId) {
			return false;
		}
		
		$update = new Update();
		$update->target_type = ItemModule::TYPE_TASK;
		$update->target_id   = $taskId;
		$update->title = $title;
		$update->content = $content;
		$update->user_id = $userId;
		$update->create_time = time();
		
		$update->save();
		\Event::fire('task.update', array($taskId));
		return $update->id;
	}
	
	/**
	 * 根据taskid获取人才比赛的动态
	 * @param int $taskId
	 * @param number $offset
	 * @param number $limit
	 * @return boolean
	 */
	public static function getTaskUpdates($taskId, $offset = 0, $limit = 10) {
		if (!Task::find($taskId)){
			return false;
		}
		
		return Update::where('target_type', ItemModule::TYPE_TASK)->where('target_id', $taskId)->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
	}

	/**
	 * 根据ID获取比赛
	 *
	 * @param int $id
	 * @return Task|null
	 */
	public static function getTaskById($id) {
		return Task::find($id);
	}

	/**
	 * 根据状态获取列表
	 * @param int $status
	 * @param int $page
	 * @param int $limit
	 *
	 * @return array|static
	 */
	public static function getListsByStatus($status, $offset = 0, $limit = 16){
		$tasks = Task::offset($offset)->limit($limit)->orderBy('id','desc');
		
	    if ($status) {
	    	if ($status == ItemModule::STATUS_WAITING_CONFIRM) {
	    		$tasks->where('status', '>', 2);
	    	} else {
    			$tasks->where('status', $status);
	    	}   		
    	} else {
    		$tasks->where('status', '>', self::STATUS_WAITING);
    	}
    	
		$result = $tasks->get();
		
		return $result;
	}
	
	
	/**
	 * 根据标签筛选比赛
	 * 
	 * @param int $tagId
	 * @param int $page
	 * @param int $limit
	 * @return array
	 * 
	 */
    public static function getListsByTagId($tagId = 0, $status = 0, $offset = 0, $limit = 12){
    	$tasks = Task::offset($offset)->limit($limit)->orderBy('id','desc');
    	
    	if ($tagId) { 	
			$tagsAffairs = TagsAffairs::where('tag_id',$tagId)->where('affair_type',BaseModule::TYPE_TASK)->get();
			$taskIds = array();
			foreach($tagsAffairs as $tagsAffair){
				$taskIds[] = $tagsAffair->affair_id;
			}
			if(count($taskIds) == 0) {
			    return array();
			}
			$tasks->whereIn('id',$taskIds);
    	}
    	
    	if ($status) {
	    	if ($status == ItemModule::STATUS_WAITING_CONFIRM) {
	    		$tasks->where('status', '>', 2);
	    	} else {
    			$tasks->where('status', $status);
	    	}   		
    	} else {
    		$tasks->where('status', '>', self::STATUS_WAITING);
    	}
    	
    	$tasks = $tasks->get();
    	
		return $tasks;
	}
	
	/**
	 * 获取某个标签和状态下的比赛总条数
	 * @param number $tagId
	 * @param number $status
	 * @return Ambigous <number, \Illuminate\Database\Query\mixed, array>
	 */
	public static function getTotalByTagIdAndStatus($tagId = 0,$status =0){
		$tasks = Task::orderBy('id','desc');
		if ($tagId) {
			$taskIds = array();
	
			$tagsAffairs = TagsAffairs::where('tag_id',$tagId)->where('affair_type',BaseModule::TYPE_TASK)->get();
	
			foreach($tagsAffairs as $tagsAffair){
				$taskIds[] = $tagsAffair->affair_id;
			}
	
			if($taskIds) {
				$tasks->whereIn('id',$taskIds);
			}
		}
	
		if ($status) {
			$tasks->where('status',$status);
		} else {
			$tasks->where('status','>',0);
		}
	
		return $tasks->count();
	}
	
	/**
	 * 根据状态获取任务人才参赛者
	 *
	 * @param int $taskId
	 * @param int $status
	 * @return Ambigous <\Illuminate\Database\Eloquent\Model, \Illuminate\Database\Eloquent\static, NULL>|Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getTaskPlayers($taskId, $status = array(self::TASK_WORK_STATUS_SUBMIT, self::TASK_WORK_STATUS_WINNER)) {
	    $works = TaskWork::where('task_id', $taskId)->where('user_id', '>', 0);
	    if (is_array($status)) {
	        $works->whereIn('status', $status);
	    } elseif ($status == self::TASK_WORK_STATUS_SUBMIT) {
	        $works->where('status', $status);
	    } elseif ($status == self::TASK_WORK_STATUS_WINNER) {
	        return $works->where('status', $status)->first();
	    }
	    return $works->get();
	}
	
	public static function getTaskWorkById($id) {
	    return TaskWork::find($id);
	}
	
	public static function getWorksByIds($ids) {
	    return $ids ? TaskWork::findMany($ids) : array();
	}

    /**
	 * 到期处理
	 */
    public static function processExpiration() {       
        $tasks = Task::whereIn('status', array( self::STATUS_GOING, self::STATUS_WAITING_CONFIRM))->get();
        $nowTime = time();
        
        foreach ($tasks as $task) {
        	$info = json_decode($task->info);
            if ($task->status == self::STATUS_GOING && $info->end_time > 0) {
                if ($task->end_time <= $nowTime + 86400 * 3 && $task->end_time > $nowTime + 86400 * 2) {
                    //比赛到期剩余3天
                    if(!isset($task->end_soon_remind) || $task->end_soon_remind == 0) {
                        \Event::fire('task.deadline-three', array($task->id, $task->user_id));
                        $task->end_soon_remind = 1;
                        $task->save();
                    }
                } elseif ($task->end_time <= $nowTime) {
                    //比赛关闭
                    $task->status = self::STATUS_END_FAIL;
                    $task->save();                   
                    \Event::fire('task.past-due', array($task->id, $task->user_id));
                }
            } elseif ($task->status == self::STATUS_WAITING_CONFIRM) {
                //任务完工剩余24小时提醒
                $winner = self::getTaskPlayers($task->id, self::TASK_WORK_STATUS_WINNER);                
                if($winner->promise_complete_time <= $nowTime + 86400 && $winner->promise_complete_time > $nowTime) {
                    if(!isset($task->finish_remind) || $task->finish_remind == 0) {
                        \Event::fire('task.work-deadline', array($task->id, $task->user_id, $winner->user_id));
                        $task->finish_remind = 1;
                        $task->save();
                    }
                }
            }
        }
    }

}