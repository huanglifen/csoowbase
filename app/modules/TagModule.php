<?php namespace App\Modules;

use App\Models\Tags;
use App\Models\TagsAffairs;

class TagModule extends BaseModule {

	/**
	 * 批量添加标签
	 * @param int   $affairType
	 * @param array $tagNames
	 *
	 * @return array 标签ID组成的数组
	 */
	public static function createTags($affairType,array $tagsNames){

		$tagsIds = array();
		$tagsvalues = array();

		foreach($tagsNames as $tagName){
			$flag = Tags::where('type',$affairType)->where('name', $tagName)->first();

			if(! $flag && $tagName){
				$tags = new Tags();
				$tags->type = $affairType;
				$tags->name = $tagName;
				$tags->save();
				$tagsIds[] = $tags->id;
				
				$tagsvalues[$tags->id] = $tagName; 
			}else if($flag){
				$tagsIds[] = $flag->id;
				$tagsvalues[$flag->id] = $flag->name;
			}
		}
		
       return array('ids' => $tagsIds, 'tags' => $tagsvalues);
	}

	/**
	 * 添加标签与事务的关系
	 *
	 * @param int   $affairType
	 * @param int   $affairId
	 * @param array $tagIds
	 */
	public static function createTagsAffairs($affairType, $affairId, array $tagIds) {		
		foreach($tagIds as $tagId){
			$tagsAffairs = new TagsAffairs();
			$tagsAffairs->tag_id = $tagId;
			$tagsAffairs->affair_type = $affairType;
			$tagsAffairs->affair_id = $affairId;
			$tagsAffairs->save();
		}
	}

	/**
	 * 根据标签获取事务
	 * @param int $tagId
	 * @param int $affairType
	 * @param int $offset
	 * @param int $number
	 *
	 * @return array|null
	 */
	public static function getAffairsBytagIdAndType($tagId, $affairType, $offset, $number) {

		return TagsAffairs::whereRaw('tag_id = ? and affair_type = ?',array($tagId,$affairType))->offset($offset)->limit($number)->get();

	}
	
	/**
	 * 随机获取热门标签
	 * @param int $num
	 *
	 * @return array 热门标签
	 */
	public static function getHotTags($targetType, $num){
	    return  \DB::select('select * from tags where type = ? order by rand() limit ? ', array($targetType,$num));
	}


}