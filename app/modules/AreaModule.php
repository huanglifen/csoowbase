<?php

namespace App\Modules;

use App\Models\Organization;
use App\Models\Region;
use App\Models\School;
use App\Models\Area;
use App\Models\Project;
use App\Models\ExchangeTrade;
use App\Models\Task;
use App\Models\Hiring;
use App\Models\Item;

/**
 * 地区模块--地区、学校、政企的汇总
 * Class AreaModule
 *
 * @package App\Modules
 */
class AreaModule extends BaseModule {
	const TYPE_REGION = 1; // 地区
	const TYPE_SCHOOL = 2; // 学校
	const TYPE_ORGANIZATION = 3; // 政企

	                             
	// 地区统计数据的变更
	public static function listenEvent() {
		
		// 发布投资项目比赛时
		\Event::listen ( 'project.publish', function ($userId, $projectId) {
			$project = Project::find ( $projectId );
			self::calAreaCount ( ItemModule::TYPE_PROJECT, $project->province_id, $project->city_id, $project->district_id, $project->school_id, $project->organization_id );
		} );
		
		// 发布任务人才比赛时
		\Event::listen ( 'task.publish', function ($userId, $taskId) {
			$task = Task::find ( $taskId );
			self::calAreaCount ( ItemModule::TYPE_TASK, $task->province_id, $task->city_id, $task->district_id, $task->school_id, $task->organization_id );
		} );
		
		// 发布公益明星比赛时
		\Event::listen ( 'hiring.publish', function ($userId, $hiringId) {
			$hiring = Hiring::find ( $hiringId );
			self::calAreaCount ( ItemModule::TYPE_HIRING, $hiring->province_id, $hiring->city_id, $hiring->district_id, $hiring->school_id, $hiring->organization_id );
		} );
		
		// 发布交易所交易时
		\Event::listen ( 'trade.publish', function ($userId, $tradeId) {
			$trade = ExchangeTrade::find ( $tradeId );
			self::calAreaCount ( ItemModule::TYPE_EXCHANGE, $trade->province_id, $trade->city_id, $trade->district_id, $trade->school_id, $trade->organization_id );
		} );
	}
	
	/**
	 * 发布比赛时计算地区的统计数据
	 *
	 * @param unknown $targetType        	
	 * @param unknown $provinceId        	
	 * @param unknown $cityId        	
	 * @param unknown $districtId        	
	 * @param unknown $schoolId        	
	 * @param unknown $organizationId        	
	 */
	public static function calAreaCount($targetType, $provinceId, $cityId, $districtId, $schoolId, $organizationId) {
		$province = $provinceId ? Region::find ( $provinceId ) : '';
		$city = $cityId ? Region::find ( $cityId ) : '';
		$district = $districtId ? Region::find ( $districtId ) : '';
		$school = $schoolId ? School::find ( $schoolId ) : '';
		$organization = $organizationId ? Organization::find ( $organizationId ) : '';
		
		self::calChinaCount ( $targetType );
		
		switch ($targetType) {
			case BaseModule::TYPE_CONTEST :
			case BaseModule::TYPE_PROJECT :
			case BaseModule::TYPE_TASK :
			case BaseModule::TYPE_HIRING :
				self::calContestCount ( $province, $city, $district, $school, $organization );
				break;
			case BaseModule::TYPE_EXCHANGE :
				self::calExchangeCount ( $province, $city, $district, $school, $organization );
				break;
			case BaseModule::TYPE_USER :
			case BaseModule::TYPE_EXPERT :
				self::calUserCount ( $province, $city, $district, $school, $organization );
				break;
			default :
				;
		}
	}
	
	/**
	 * 计算中国地区统计数据
	 *
	 * @param string $type        	
	 *
	 * @return boolean
	 */
	public static function calChinaCount() {
		$china = Region::find ( 1 );
		$creative_index = Region::where('type',2)->sum('creative_index');
		$china->creative_index = $creative_index;
		$china->contests_count = ContestModule::getCount ();
		$china->trades_count = ExchangeModule::getCount ();
		$china->users_count = UserModule::getCount ();
		$china->experts_count = UserModule::getCount ( '', 0, 1 );
		$china->save ();
	}
	
	/**
	 * 计算比赛统计数据
	 *
	 * @param unknown $province        	
	 * @param unknown $city        	
	 * @param unknown $district        	
	 * @param unknown $school        	
	 * @param unknown $organization        	
	 */
	public static function calContestCount($province, $city, $district, $school, $organization) {
		if ($province) {
			$province->contests_count = ContestModule::getCount ( 'province_id', $province->id );
			$province->save ();
		}
		
		if ($city) {
			$city->contests_count = ContestModule::getCount ( 'city_id', $city->id );
			$city->save ();
		}
		
		if ($district) {
			$district->contests_count = ContestModule::getCount ( 'district_id', $district->id );
			$district->save ();
		}
		
		if ($school) {
			$school->contests_count = ContestModule::getCount ( 'school_id', $school->id );
			$school->save ();
		}
		
		if ($organization) {
			$organization->contests_count = ContestModule::getCount ( 'organization_id', $organization->id );
			$organization->save ();
		}
	}
	
	/**
	 * 计算交易统计数据
	 *
	 * @param unknown $province        	
	 * @param unknown $city        	
	 * @param unknown $district        	
	 * @param unknown $school        	
	 * @param unknown $organization        	
	 */
	public static function calExchangeCount($province, $city, $district, $school, $organization) {
		if ($province) {
			$province->trades_count = ExchangeModule::getCount ( 'province_id', $province->id );
			$province->save ();
		}
		
		if ($city) {
			$city->trades_count = ExchangeModule::getCount ( 'city_id', $city->id );
			$city->save ();
		}
		
		if ($district) {
			$district->trades_count = ExchangeModule::getCount ( 'district_id', $district->id );
			$district->save ();
		}
		
		if ($school) {
			$school->trades_count = ExchangeModule::getCount ( 'school_id', $school->id );
			$school->save ();
		}
		
		if ($organization) {
			$organization->trades_count = ExchangeModule::getCount ( 'organization_id', $organization->id );
			$organization->save ();
		}
	}
	
	/**
	 * 计算用户和专家统计数据
	 *
	 * @param unknown $province        	
	 * @param unknown $city        	
	 * @param unknown $district        	
	 * @param unknown $school        	
	 * @param unknown $organization        	
	 */
	public static function calUserCount($province, $city, $district, $school, $organization) {
		if ($province) {
			$province->users_count = UserModule::getCount ( 'province_id', $province->id );
			$province->experts_count = UserModule::getCount ( 'province_id', $province->id, 1 );
			$province->save ();
		}
		
		if ($city) {
			$city->users_count = UserModule::getCount ( 'city_id', $city->id );
			$city->experts_count = UserModule::getCount ( 'city_id', $city->id, 1 );
			$city->save ();
		}
		
		if ($district) {
			$district->users_count = UserModule::getCount ( 'district_id', $district->id );
			$district->experts_count = UserModule::getCount ( 'district_id', $district->id, 1 );
			$district->save ();
		}
		
		if ($school) {
			$school->users_count = UserModule::getCount ( 'school_id', $school->id );
			$school->experts_count = UserModule::getCount ( 'school_id', $school->id, 1 );
			$school->save ();
		}
		
		if ($organization) {
			$organization->users_count = UserModule::getCount ( 'organization_id', $organization->id );
			$organization->experts_count = UserModule::getCount ( 'organization_id', $organization->id, 1 );
			$organization->save ();
		}
	}
	
	/**
	 * 获取全部地区
	 *
	 * @return Ambigous <\Illuminate\Database\Eloquent\Collection, multitype:\Illuminate\Database\Eloquent\static >
	 */
	public static function getAllAreas() {
		return Area::all ();
	}
	
	/**
	 * 搜索结果--地区
	 *
	 * @param string $keyword        	
	 * @param int $type        	
	 * @param int $offset        	
	 * @param int $limit        	
	 *
	 * @return array null
	 */
	public static function searchAreasByKeyword($keyword, $type = BaseModule::TYPE_AREA, $offset = 0, $limit = 5) {
		if ($type == BaseModule::TYPE_AREA) {
			$datas = Area::where ( 'name', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->get ();
		} elseif ($type == BaseModule::TYPE_REGION) {
			$datas = Region::where ( 'name', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->get ();
		} elseif ($type == BaseModule::TYPE_SCHOOL) {
			$datas = School::where ( 'name', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->get ();
		} elseif ($type == BaseModule::TYPE_ORGANIZATION) {
			$datas = Organization::where ( 'name', 'like', '%' . $keyword . '%' )->offset ( $offset )->limit ( $limit )->get ();
		}
		return $datas;
	}
	
	/**
	 * 获取某关键词下的地区数量
	 *
	 * @param unknown $keyword        	
	 * @param unknown $type        	
	 *
	 * @return unknown
	 */
	public static function searchAreasByKeywordCount($keyword, $type = BaseModule::TYPE_AREA) {
		if ($type == BaseModule::TYPE_AREA) {
			$count = Area::where ( 'name', 'like', '%' . $keyword . '%' )->count ();
		} elseif ($type == BaseModule::TYPE_REGION) {
			$count = Region::where ( 'name', 'like', '%' . $keyword . '%' )->count ();
		} elseif ($type == BaseModule::TYPE_SCHOOL) {
			$count = School::where ( 'name', 'like', '%' . $keyword . '%' )->count ();
		} elseif ($type == BaseModule::TYPE_ORGANIZATION) {
			$count = Organization::where ( 'name', 'like', '%' . $keyword . '%' )->count ();
		}
		return $count;
	}
	
	/**
	 * 更新地区(地区学校政企)的计数
	 *
	 * @param int $type        	
	 * @param int $id        	
	 * @param array $count        	
	 * @param string $isAdd        	
	 */
	public static function updateAreaCount($type, $id, array $count, $isAdd = '+') {
		if ($type == self::TYPE_REGION) {
			$model = Region::find ( $id );
		} else if ($type == self::TYPE_SCHOOL) {
			$model = School::find ( $id );
		} else if ($type == self::TYPE_ORGANIZATION) {
			$model = Organization::find ( $id );
		} else {
			return true;
		}
		
		if (! $model) {
			return true;
		}
		
		if ($isAdd == '+') {
			$model->creative_index += $count ['creative_index'];
			$model->contests_count += $count ['contests_count'];
			$model->trades_count += $count ['trades_count'];
			$model->experts_count += $count ['experts_count'];
			$model->users_count += 1;
			$model->save ();
		} else {
			$model->creative_index = ($model->creative_index - $count ['creative_index']) > 0 ? ($model->creative_index - $count ['creative_index']) : 0;
			$model->contests_count = ($model->contests_count - $count ['contests_count']) > 0 ? ($model->contests_count - $count ['contests_count']) : 0;
			$model->trades_count = ($model->trades_count - $count ['trades_count']) > 0 ? ($model->trades_count - $count ['trades_count']) : 0;
			$model->experts_count = ($model->experts_count - $count ['experts_count']) ? ($model->experts_count - $count ['experts_count']) : 0;
			$model->users_count = ($model->users_count - 1) ? ($model->users_count - 1) : 0;
			$model->save ();
		}
		return true;
	}
	
	/**
	 * 获取排名(全县、全市、全省、全国)
	 *
	 * @param int $type        	
	 * @param int $id        	
	 * @param int $provinceId        	
	 */
	public static function getRank($type, $id, $provinceId = 0) {
		if ($type == self::TYPE_REGION) {
			$model = new Region ();
		} else if ($type == self::TYPE_SCHOOL) {
			$model = new School ();
		} else if ($type == self::TYPE_ORGANIZATION) {
			$model = new Organization ();
		}
		
		$thisRank = $model->find ( $id );
		$thisRank = $thisRank->creative_index;
		
		$model = $model->where ( 'creative_index', '>', $thisRank );
		if ($provinceId) {
			$model = $model->where ( 'province_id', $provinceId );
		}
		$rank = $model->count ();
		return $rank + 1;
	}
	
	/**
	 * 喜欢地区统计
	 *
	 * @param unknown $userId        	
	 * @param unknown $targetType        	
	 * @param unknown $targetId        	
	 */
	public static function addLikeCount($targetType, $targetId) {
		if ($targetType == BaseModule::TYPE_REGION) {
			$model = Region::find ( $targetId );
		} elseif ($targetType == BaseModule::TYPE_SCHOOL) {
			$model = School::find ( $targetId );
		} else {
			$model = Organization::find ( $targetId );
		}
		$model->likes_count = Item::where ( 'action_type', ItemModule::ACTION_TYPE_LIKE )->where ( 'target_type', $targetType )->where ( 'target_id', $targetId )->count ();
		$model->save ();
	}
	
	
	/**
	 * 生成城市列表
	 * @return Ambigous <multitype:multitype:string  NULL , multitype:multitype:string multitype:number   multitype:multitype:string  NULL  >
	 */
	public static function geneCityList() {
		$cityList = array (
				array (
						'name' => '选择省份',
						'attr' => array (
								'id' => 0 
						) 
				) 
		);
		
		$provinces = Region::where ( 'type', 2 )->orderBy ( 'id', 'asc' )->get ();
		$i = 1;
		foreach ( $provinces as $province ) {
			$cityList [$i] = array (
					'name' => $province->name,
					'attr' => array (
							'id' => $province->id . '|00|00' 
					)
			);
			
			$cities = Region::where ( 'type', 3 )->where ( 'province_id', $province->id )->orderBy ( 'id', 'asc' )->get ();
			if(count($cities)){
				$cityList [$i] ['nextLevel'][0] = 	array (
					'name' => '选择城市',
					'attr' => array (
						'id' => $province->id . '|00|00'
					)
				);
			}
			$j = 1;
			foreach ( $cities as $city ) {

				$cityList [$i] ['nextLevel'][$j] = array (
					'name' => $city->name,
					'attr' => array (
						'id' => substr ( $city->id, 0, 2 ) . '|' . substr ( $city->id, 2, 2 ) . '|00'
					)
				);

				$districts = Region::where ( 'type', 4 )->where ( 'city_id', $city->id )->orderBy ( 'id', 'asc' )->get ();
				if(count($districts)){
					$cityList [$i] ['nextLevel'][$j]['nextLevel'][0] =array (
						'name' => '选择区县',
						'attr' => array (
							'id' => substr ( $city->id, 0, 2 ) . '|' . substr ( $city->id, 2, 2 ) . '|00'
						)
					);
				}
				$k = 1;
				foreach ( $districts as $district ) {
					$cityList [$i] ['nextLevel'][$j] ['nextLevel'] [$k] = array (
							'name' => $district->name,
							'attr' => array (
									'id' => substr ( $district->id, 0, 2 ) . '|' . substr ( $district->id, 2, 2 ) . '|' . substr ( $district->id, 4, 2 )
							)
					);
					$k = $k+1;
				}

				$j = $j + 1;

			}
			$i = $i + 1;
		}
		return $cityList;
	}
}