<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', 'App\Controllers\HomeController@getIndex');

Route::controller('home', 'App\Controllers\HomeController');
Route::controller('user', 'App\Controllers\UserController');
Route::controller('account', 'App\Controllers\AccountController');
Route::controller('circle', 'App\Controllers\CircleController');

Route::controller('project', 'App\Controllers\ProjectController');
Route::controller('task', 'App\Controllers\TaskController');
Route::controller('hiring', 'App\Controllers\HiringController');
Route::controller('exchange', 'App\Controllers\ExchangeController');
Route::controller('encyclopedia', 'App\Controllers\EncyclopediaController');

Route::controller('area', 'App\Controllers\AreaController');
Route::controller('thinktank', 'App\Controllers\ThinktankController');

Route::controller('search', 'App\Controllers\SearchController');
Route::controller('notification', 'App\Controllers\NotificationController');
Route::controller('chat', 'App\Controllers\ChatController');
Route::controller('dynamic', 'App\Controllers\DynamicController');
Route::controller('notification', 'App\Controllers\NotificationController');
Route::controller('news', 'App\Controllers\NewsController');


Route::controller('admin', 'App\Controllers\Admin\AdminController');
Route::controller('check', 'App\Controllers\Admin\CheckController');
Route::controller('expert', 'App\Controllers\Admin\ExpertController');
Route::controller('industry', 'App\Controllers\Admin\IndustryController');