<?php

return [

	'error_title_empty' => '请填写比赛名称',
	'error_description_empty' => '请填写比赛详细介绍',
	'error_hiring_end_time_empty' => '请填写寻找公益明星的限制时间',
	'error_hiring_end_time_invalid' => '请设置天数为1-9999天的时间',		
	'error_hiring_amount_invalid' => '请添加比赛奖项',
	'error_prizes_award_too_big' => '比赛奖项描述太长',
	
	'error_hiring_apply_works_empty' => '请填写代表作品',
	'error_hiring_apply_cover_empty' => '请上传作品封面',
	'error_hiring_apply_declaration_empty' => '请填写一句话公益宣言',
	'error_hiring_apply_declaration_too_long' => '一句话公益宣言应小于140个字',
	'error_hiring_apply_declaration_too_short' => '一句话公益宣言应大于2个字',
	
	'error_hiring_update_title_empty' => '请填写比赛动态标题',
	'error_hiring_update_title_too_long' => '比赛动态标题应小于60个字',
	'error_hiring_update_title_too_short' => '比赛动态标题应大于2个字',
	'error_hiring_update_content_empty' => '请填写比赛动态内容'
	

];