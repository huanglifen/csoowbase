<?php

return [
	'error_title_empty' => '请填写交易名称',
	'error_description_empty' => '请填写交易详细介绍',
    'error_cover_empty' => '请上传封面图片',
    
    'error_price_empty' => '请填写售价',
    'error_price_not_integer' => '售价必须为正整数',
    
    'error_pirce_over_10_billons' => '售价不能超过10亿',
    
    'error_has_no_right' => '没有权限',
	
];