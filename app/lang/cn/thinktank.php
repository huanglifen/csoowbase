<?php

return [
     'error_organization_duty_empty' => '请选择组织或行业',
     'error_duty_empty' => '请选择职务',
     'error_please_upload_certification_data' => '请上传证明材料',
     'error_explain_empty' => '请填写申请说明',
     'error_name_empty' => '请填写名字',
     'error_contact_info_empty' => '请填写联系方式',
     'error_name_short' => '请输入4-16字符的名字',
     'error_name_long' => '请输入4-16字符的名字',
     'error_name_illegal' => '名字不能包含非法字符',
     'error_id_card_empty' => '请输入身份证号码',
     'error_enterprise_name_empty' => '请填写公司名称',
     'error_enterprise_duty_empty' => '请填写公司职务',
     'error_id_card_number' => '请输入有效的身份证号',
     'error_industry_empty' => '请输入申请行业'
];