<?php

return [
    'error_title_empty' => '请输入创意名称',
    'error_description_empty' => '请填写创意详细介绍'
];