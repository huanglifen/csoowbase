<?php

return [
  'error_empty_content' => '请输入内容',
  'error_not_empty' => '不能为空',
  'error_like_failure' => '不能多次点赞',
  'error_max_length_140' => '最多只能输入140个字',
];
