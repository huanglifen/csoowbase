<?php

return [
    'error_must_be_chinense' => '请输入中文',
    'error_must_less_than_10' => '圈子名称须小于20字符',
    'error_message_over_1000_word' => '消息内容不能超过1000字',
    'error_content_empty' => '消息内容不能为空',
    'error_already_followed' => '已经关注该用户',
    'error_invalid_circle_ids' => '选择的分组无效',
    'error_empty_name' => '请填写圈子名',
    'error_already_cabcel_followed' => '没有关注该用户',
    'error_have_the_circle' => '已存在该圈子',
    'error_add_member_failure' => '添加用户到圈子失败',
    'error_already_have_the_circle' => '已有该圈子',
    'error_have_no_right' => '没有权限进行该操作',
];
