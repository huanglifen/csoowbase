
<?php

return [

	'error_title_empty' => '请填写项目名称',
	'error_description_empty' => '请填写项目详细介绍',

	'error_goal_amount_empty' => '请填写项目投资金额',
	'error_goal_amount_is_zero' => '项目投资金额不能为0',
	'error_goal_amount_too_big' => '请填写低于10亿的金额',
	'error_goal_amount_not_integer' => '项目投资金额应该为整数',

	'error_investment_end_time_empty' => '请设置天数为1-9999天的时间',
	'error_investment_end_time_invalid' => '请设置天数为1-9999天的时间',

	'error_reward_give_time' => '请填写项目投资回报兑现的时间',

	'error_reward_description_empty' => '请填写投资回报详细介绍',
	
	'error_invest_amount_empty' => '请填写投资金额',
	'error_invest_amount_invalid' => '请填写正确的投资金额',
	'error_invest_amount_too_big' => '投资金额太大',
	'error_invest_amount_too_small' => '投资金额应大于1元',
	'error_invest_type_empty' => '请选择投资类型',
	'error_deposit_percentage_empty' => '请填写投资定金支付比例',
	'error_deposit_percentage_invalid' => '请填写正确的投资定金支付比例',
	'error_final_pay_time_empty' => '请填写尾款支付时间',
	'error_final_pay_time_invalid' => '请设置天数为4-9999天的时间',
	'error_invest_requirement_empty' => '请填写您的投资述求',
	'error_require_complete_time_invalid' => '请设置天数为1-9999天的时间',

	'error_reward_pics_empty' => '请上传投资回报图片',

	'error_update_title_empty' => '请填写标题',
	'error_update_content_empty' => '请填写更新内容',

];
