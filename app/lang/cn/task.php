<?php

return [

	'error_title_empty' => '请填写任务名称',
	'error_description_empty' => '请填写任务详细介绍',

	'error_bonus_amount_empty' => '请填写比赛奖励',
	'error_bonus_amount_is_zero' => '奖励金额不能为0',
	'error_bonus_amount_too_big' => '请填写低于120字符的比赛奖励',
	'error_bonus_amount_not_integer' => '奖励金额应该为正整数',

	'error_submit_work_end_time_empty' => '请填写寻找任务人才的限制时间',
	'error_submit_work_end_time_invalid' => '请设置天数为1-9999天的时间',
	'error_work_works_empty' => '请填写任务作品描述',
	'error_work_cover_empty' => '请上传任务作品封面',
	'error_submit_work_end_time_empty' => '请填写承诺完成任务时间',
	'error_submit_work_end_time_too_long' => '承诺完成任务时间应小于9999天',
	'error_submit_work_end_time_too_short' => '承诺完成任务时间应大于1天',
	
	'error_task_update_title_empty' => '请填写动态标题',
	'error_task_update_title_too_long' => '标题太长',
	'error_task_update_content_empty' => '请填写动态内容',
	
];