<?php

return [

	'error_please_input' => '请填写',
	'email' => '邮箱',
	'user_id' => '用户ID',
	'password_reset_code' => '密码重设码',

	'error_title_too_short_or_long' => '请输入2-60字符的名称',
	'error_cover_empty' => '请上传封面图片',
    'error_phone_number_empty' => '请填写手机号码',
    'error_phone_number_error' => '请填写正确的手机号码',
        
    'error_feedback_empty' => '请填写意见反馈内容',
    'error_dynamic_empty' => '请填写文字内容',
    
    'error_tag_name_invalid' =>  '请输入只含有中文，英文，数字的标签',
    'error_tag_length_invalid' => '请输入2-12字符的标签',
    'error_tags_number_cant_bigger_than_five' => '标签数量不能大于5个',

	'target_id' =>'必须是自然数',
	
	'error_password_empty' => '请输入密码',
	'error_password_short' => '密码太短，请输入6-16字符的密码',
	'error_password_long' => '密码太长，请输入6-16字符的密码',
	'error_password_illegal' => '密码不能包含非法字符',
	'error_password_wrong' => '您输入的密码和帐户名不匹配',
	
	'error_password_again_empty' => '请再次输入密码',
	'error_password_again_error' => '两次密码不一致',
	
	'error_current_password_empty'=>'请输入旧密码',
	'error_invalid_current_password'=>'旧密码错误',


	//竞猜
	'error_end_time_out'=>'竞猜已截止',
	'error_coin_empty'=>'请填写需要投注的创创币',
	'error_coin_not_enough'=>'您的账户创创币余额不足',
	
	//账号设置
	'error_introduction_long' => '请填写200字符内的个人简介',
	'phone_number_verify_wrong' => '请输入正确的旧手机号',
	'email_verify_wrong' => '请输入正确的旧邮箱',
	'error_email_empty' => '请填写邮箱',
	'error_email_error' => '请填写正确的邮箱',
	'error_email_exist' => '该邮箱已注册',
	'error_verify_code_empty' => '请输入验证码',
	'new_email_not_equal' => '新邮箱和接收验证码邮箱不符',
	'error_verify_code_error' => '验证码错误',
	'error_verify_code_time_out' => '验证码已过期,请重新发送',

	'error_mobile_empty' => '请填写手机号码',
	'error_mobile_error' => '请填写正确的手机号码',
	'error_mobile_exist' => '该手机号码已注册',
	'error_mobile_not_exist' => '该手机号码未注册',
	'error_new_mobile_not_equal'=>'新手机号码不匹配',
	'error_old_mobile_not_equal'=>'旧手机号码不匹配',

	//比赛动态
	'error_update_content_too_big' => '请填写1-1000字的内容',
	'error_update_content_too_short' => '请填写1-1000字的内容',
	'error_update_content_too_many_line' => '请填写少于15行的内容',
	
	//评论
	'error_comment_too_many_line' => '请填写少于15行的内容',
	
	//专家点评
	'error_expert_comment_too_many_line' => '请填写少于15行的内容',
	'error_expert_comment_no_empty'=>'请输入你的作品评语',
	
];