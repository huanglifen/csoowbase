<?php

return [

	'error_email_empty' => '请填写邮箱',
	'error_email_error' => '请填写正确的邮箱',
	'error_email_exist' => '该邮箱已注册',
	'error_email_not_exist' => '该邮箱未注册',
	'error_user_no_active'=>'请激活您的帐号',

	'error_name_empty' => '请填写昵称',
	'error_name_short' => '昵称太短，请输入4-16字符的昵称',
	'error_name_long' => '昵称太长，请输入4-16字符的昵称',
	'error_name_illegal' => '昵称不能包含非法字符',
	'error_name_is_number' => '昵称不能为纯数字',
	'error_name_exist' => '该昵称已注册',
	'error_name_not_exist' => '该昵称未注册',

	'error_login_name_empty' => '请填写邮箱/创创号/昵称',

	'error_cc_no_not_exist' => '该创创号不存在',
	'error_not_accept_agreement' => '您还未接受《创意世界用户注册协议》及《版权声明》'

];