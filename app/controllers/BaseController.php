<?php namespace App\Controllers;
use App\Common\Utils;
use App\Models\User;
use App\Modules\UserModule;
use App\Modules\ChatModule;
use App\Modules\NotificationModule;
use App\Modules\CreativeIndexModule;
use App\Modules\CoinModule;
use App\Modules\ProjectModule;
use App\Modules\TaskModule;
use App\Modules\HiringModule;
use App\Modules\ItemModule;
use App\Modules\ContestModule;
use App\Modules\AreaModule;
use App\Modules\ExchangeModule;
use App\Modules\EncyclopediaModule;
use App\Modules\DynamicModule;
/**
 * 控制器基类
 *
 * 提供公共方法，如参数检查，显示视图等。
 */

class BaseController extends \Controller {

	const RULE_REQUIRED = 'required';
	const RULE_MIN_LENGTH = 'minLength';
	const RULE_MAX_LENGTH = 'maxLength';
	const RULE_NATURE_NUMBER = 'natureNumber';
	const RULE_NATURE_NUMBER_AND_ZERO = 'natureNumberAndZero';
	const RULE_EMAIL = 'email';
	const RULE_IP_V4 = 'ip_v4';
	const RULE_DATE = 'date';
	const RULE_NOT_NUMBER = 'notNumber';
	const RULE_CHARACTER = 'character';
	const RULE_PHONE_NUMBER = 'phoneNumber';
	const RULE_ZIP_CODE = 'zipCode';
	
	const IMAGE_RESIZE_WIDTH = 280;
	const IMAGE_RESIZE_HEIGHT = 220;
	
	const AVATAR_RESIZE_WIDTH = 360;
	const AVATAR_RESIZE_HEIGHT = 360;
	
	const DYNAMIC_RESIZE_WIDTH = 120;//动态缩略图等比缩放
	
	const IMAGE_CONTENT_RESIZE_WIDTH = 137;
	const IMAGE_CONTENT_RESIZE_HEIGHT = 127;
	
	protected $targetTypeString;

	protected $ruleErrorMessage = array(
		self::RULE_REQUIRED => 'error_is_null',
		self::RULE_MIN_LENGTH => 'error_min_length',
		self::RULE_MAX_LENGTH => 'error_max_length',
		self::RULE_NATURE_NUMBER => 'error_not_nature_number',
		self::RULE_NATURE_NUMBER_AND_ZERO => 'error_not_nature_number_and_zero',
		self::RULE_EMAIL => 'error_not_email',
		self::RULE_IP_V4 => 'error_not_ip_v4',
		self::RULE_DATE => 'error_not_date',
		self::RULE_NOT_NUMBER => 'error_is_number',
		self::RULE_CHARACTER => 'error_illegal_character',
		self::RULE_PHONE_NUMBER => 'error_invalid_phone_number',
		self::RULE_ZIP_CODE => 'error_invalid_zip_code'
	);

	protected $langFile;

	protected $paramError = [];

	protected $data = [];

	protected $imageFormatExt = array(
		IMAGETYPE_GIF => 'gif|imagegif',
		IMAGETYPE_JPEG => 'jpg|imagejpeg',
		IMAGETYPE_PNG => 'png|imagepng',
	);

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = \View::make($this->layout);
		}
	}
	
	function __construct()
	{
		CreativeIndexModule::listenEvent();
		CoinModule::listenEvent();
		ItemModule::listenEvent();
		NotificationModule::listenEvent();
		AreaModule::listenEvent();
		UserModule::listenEvent();
		DynamicModule::listenEvent();
	}

	// PARAM CHECK

	protected function getParam($param, $ruleString = '') {
		$paramValue = \Input::get($param);

		return $this->checkParam($param, $paramValue, $ruleString);
	}
	
	protected function getParamDate($param, $ruleString = '', $add = false) {
		$date = $this->getParam($param, $ruleString);
		if(! $date) {
		    $add = false;
		}
		
		if($add) {
		    return (strtotime($date) + 86400 - 1);
		}else{
		    return strtotime($date);
		}
		
	}

	protected function checkParam($param, $paramValue, $ruleString) {
		if ($ruleString == '') {
			return $paramValue;
		}
		$originalRules = explode('|', $ruleString);
		$rules = array();
		foreach ($originalRules as &$originalRule) {
			$rule = $originalRule;
			$ruleValue = false;
			$ruleErrorMessage = false;
			// Parse rule error message, eg: required{error_not_accept_protocol}
			if (preg_match("/(.*?)\\{(.*)\\}/", $originalRule, $match)) {
				$rule = $match[1];
				$ruleErrorMessage = $match[2];
			}
			// Parse rule value, eg: minLength[6]
			if (preg_match("/(.*?)\\[(.*)\\]/", $originalRule, $match)) {
				$rule = $match[1];
				$ruleValue = $match[2];
			}
			$rules[$rule] = array('value' => $ruleValue, 'errorMessage' => $ruleErrorMessage);
		}

		// Param is blank, but NOT required, return
		if (! array_key_exists(self::RULE_REQUIRED, $rules) && !$this->checkRequired($paramValue)) {
			return false;
		}

		foreach ($rules as $rule => &$valueError) {
			// Run rule check
			$ruleMethod = 'check' . $rule;
			if (method_exists($this, $ruleMethod)) {
				$result = $this->$ruleMethod($paramValue, $valueError['value']);
			} else {
				\Log::debug('Unable to find rule method: ' . $ruleMethod);
				continue;
			}

			if ($result === false) {
				$this->setParamError($param, $valueError['errorMessage'], $rule, $valueError['value']);
				return false;
			}
		}

		return $paramValue;
	}

	protected function setParamError($field, $errorMessage, $rule = '', $ruleValue = false) {
		if ($errorMessage) {
			$langKey = $this->getLangFile() . '.' . $errorMessage;
			$error = sprintf(\Lang::get($langKey), $ruleValue);
			if ($error == $langKey) {
				$error = sprintf(\Lang::get('common.' . $errorMessage), $ruleValue);
			}
		} else {
			$promptPrefix = \Lang::get('common.error_please_input');
			$specialRules = array(self::RULE_MIN_LENGTH, self::RULE_MAX_LENGTH, self::RULE_NOT_NUMBER, self::RULE_CHARACTER);
			$langKey = $this->getLangFile() . '.' . $field;
			$promptSuffix = \Lang::get($langKey);
			if ($promptSuffix == $langKey) {
				$promptSuffix = \Lang::get('common.' . $field);
			}
			$error = $promptPrefix . $promptSuffix;
		}
		$this->paramError[$field] = $error;
	}
	
	protected function replaceLine($value) {
		return preg_replace('/\r|\n/', '<br />', $value);
	}

	protected function hasParamError($field) {
		return array_key_exists($field, $this->paramError);
	}
	
	protected function getOffset($page, $limit) {
		return ($page - 1) * $limit;
	}

	private function getRuleErrorMessage($rule) {
		if (array_key_exists($rule, $this->ruleErrorMessage)) {
			return $this->ruleErrorMessage[$rule];
		} else {
			\Log::debug("Unable to find error message for rule: " . $rule);
			return '';
		}
	}

	private function getLangFile() {
		if ($this->langFile) {
			return $this->langFile;
		}

		$className = get_class($this);
		return strtolower(str_replace('Controller', '', substr(strrchr($className, '\\'), 1)));
	}

	// RULES

	protected function checkRequired($value) {
		return strlen(trim($value)) > 0;
	}

	protected function checkMinLength($value, $minLength) {
		return strlen( mb_convert_encoding($value, 'GB18030', 'UTF-8') ) >= $minLength;
	}

	protected function checkMaxLength($value, $maxLength) {
		return strlen( mb_convert_encoding($value, 'GB18030', 'UTF-8') ) <= $maxLength;
	}
	
	protected function checkLine($value, $line) {
		return preg_match_all('/\r|\n/', $value) < $line;
	}
	
	protected function checkMbMaxLength($value, $maxLength) {
	    return mb_strlen($value) <= $maxLength;
	}

	protected function checkNatureNumber($value) {
		return Utils::isNatureNumber($value);
	}

	protected function checkNatureNumberAndZero($value) {
		return Utils::isNatureNumberAndZero($value);
	}

	protected function checkNotNumber($value) {
		return ! is_numeric($value);
	}

	protected function checkEmail($email) {
		return Utils::isEmail($email);
	}

	protected function checkDate($date) {
		return Utils::isDate($date);
	}

	/**
	 * 检查邮箱不存在
	 *
	 * @param string $email
	 *
	 * @return bool
	 */
	protected function checkEmailNotExist($email)
	{
	    return is_null(UserModule::getUserByEmail($email));
	}

	protected function checkMobileExist($mobile)
	{
		return is_null(UserModule::getUserByMobile($mobile));
	}
	
	protected function checkEndTimeLength($endTime, $endTimeStr, $ErrorMsg) {
		if (!$endTime) {
			return true;
		}

		$endDay = ceil(($endTime - time()) / 86400);
		
		if($endDay > 9999) {
			$this->setParamError("$endTimeStr", $ErrorMsg['bigger']);
		} else if ($endDay < 1) {
			$this->setParamError("$endTimeStr", $ErrorMsg['small']);
		}		
	}
	
	/**
	 * check phone number
	 */
	protected  function checkPhoneNumber($phone)
	{
	    return Utils::isPhoneNumber($phone);
	}
	
	protected function checkCharacter($userName) {
		$legalCharacter = array('-', '_', ' ');
		$legalCharacterRange = array(
			array(48, 57), // 0 - 9
			array(65, 90), // A - Z
			array(97, 122), // a - z
		);
		for ($i = 0; $i < strlen($userName); $i++) {
			$char = $userName [$i];
			$code = ord($char);

			if ($code > 122) {
				continue;
			}

			if (in_array($char, $legalCharacter)) {
				continue;
			}

			foreach ($legalCharacterRange as $range) {
				if ($code >= $range[0] && $code <= $range[1]) {
					continue 2;
				}
			}

			return false;
		}

		return true;
	}
	
	protected function checkChinese($value) {
	    return preg_match("/^[\x7f-\xff]+$/", $value) > 0;
	}

	protected function execute(callable $callback) {
		try {
			return $callback();
		} catch(ParamErrorException $e) {
			return $this->outputError('param_error', $e->getErrors());
		} catch(UserNotLoginException $e) {
			return $this->outputError('not_login');
		}
	}

	// OUTPUT

	protected function outputParamErrorIfExist() {
		if ($this->paramError) {
			throw new ParamErrorException($this->paramError);
		}
	}

	protected function outputError($code, $errorInfo = null) {
		$output = array();

		if (! is_null($errorInfo)) {
			$output['error'] = $errorInfo;
		}

		return $this->output($code, $output);
	}

	protected function outputContent($info = null) {
		$output = array();

		if (! is_null($info)) {
			$output['content'] = $info;
		}

		return $this->output('ok', $output);
	}

	protected function output($status, array $output = array()) {
		$result = array('status' => $status);
		return json_encode(array_merge($result, $output));
		return \Response::json(array_merge($result, $output));
	}

	
	protected static function getLastPage(){
		$lastPage = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : \URL::to('/');
		return $lastPage = (strpos($lastPage, 'user/register')!== false || strpos($lastPage, 'user/login')!== false || strpos($lastPage, 'user/forget-password')!== false|| strpos($lastPage, 'activate-account-success')!== false) ? \URL::to('/'): $lastPage;
	}
	
	// OTHERS

	protected function showView($view) {
		$this->data['baseURL'] = \URL::to('/');
		$this->data['lastPage'] =  self::getLastPage();
		$this->data['currentPage'] = \URL::current();
		$this->data['isLogin'] = $this->getLoginUserId();
		$this->data['curPageName'] = strtolower(substr(substr(get_class($this), 16), 0, -10));

		$userId = $this->getLoginUserId();
		if($userId){
			$this->data['user'] = UserModule::getUserById($userId);
			$this->data ['chatNum'] = ChatModule::getUnreadChatNum ( $userId );
			$this->data ['notificationNum'] = NotificationModule::getNotificationNum ( $userId );
		}else{
			$this->data['user'] = $this->data ['chatNum'] = $this->data ['notificationNum'] = 0;
		}
		return \View::make($view, $this->data);
	}

	protected function getLoginUserId() {
		return \Session::get('user_id') ? \Session::get('user_id') : 0;
	}

	protected function isLogin() {
		return $this->getLoginUserId() > 0;
	}
	protected function isAdmin(){

		$userId = $this->getLoginUserId();
		if($userId){
			$user = UserModule::getUserById($userId);
			if($user->group_id == UserModule::GROUP_ADMIN){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	protected function requireAjaxLogin() {
		if (! $this->isLogin()) {
			throw new UserNotLoginException();
		}
	}
	
	protected function requireLogin() {
		if (! $this->isLogin()) {
			return \Redirect::to(\URL::to('/') . '/user/login');
		}
	}

	/*
	 *
	 * 上传图片
	 */
	public function postUploadImage() {

		$file = \Input::file('imagefile');
		if (empty($file)) {
			return $this->outputError('param_error', '图片太大');
		}
		$uploadPath = 'data/'.$this->targetTypeString.'/';
		Utils::createDirectoryIfNotExist($uploadPath);
		$imageSize = getimagesize($file->getFileInfo()->getPathname());
		$size = $file->getSize();
		if (! $imageSize) {
			return $this->outputError('param_error', '不合法的图片');
		}elseif (! array_key_exists( $imageSize[2], $this->imageFormatExt)) {
			return $this->outputError('param_error', '不支持此图片格式');
		}elseif($size > 5120000){
			return $this->outputError('param_error', '上传图片不能超过5M');
		}else{
			if($this->targetTypeString == 'account'){
				if($imageSize[0] < self::DYNAMIC_RESIZE_WIDTH || $imageSize[1] < self::DYNAMIC_RESIZE_WIDTH){
					return $this->outputError('param_error', '上传图片太小');
				}
			}else{
				if ($imageSize[0] < self::IMAGE_RESIZE_WIDTH || $imageSize[1] < self::IMAGE_RESIZE_HEIGHT){
					return $this->outputError('param_error', '上传图片太小');
				}
			}
		}
		$imageFormat = $imageSize[2];
		$ext = $this->imageFormatExt[$imageFormat];
		list ($imageExt, $imageMethod) = explode('|', $ext);

		$fileName = (int) $this->getLoginUserId() . '_' . ip2long(\Input::getClientIp()) . '_' . time() . '_';
		$fileNewName = $fileName . md5_file($file->getRealPath()) . '.' . $imageExt;
		$file->move($uploadPath, $fileNewName);
		$fileNewPath = $uploadPath  . $fileNewName;

		if($this->targetTypeString == 'account'){
			//$fileName = $this->resizeImage($fileNewPath,'account');
			//$fileUrl = $uploadPath . $fileName;
			$fileUrl = $fileNewPath;
		}elseif($this->targetTypeString == 'dynamic'){
			$fileName = $this->resizeDynamicImage($fileNewPath);
			$fileUrl = $fileNewPath;
		}else{
			$fileName = $this->resizeImage($fileNewPath);
			$fileUrl = $uploadPath . $fileName;
		}
		return $this->outputContent($fileUrl);
	}

	//等比缩放动态图
	protected function resizeDynamicImage($filePath){
		if (! file_exists($filePath)) {
			return false;
		}
		
		$filePath = $this->adjustPictureOrientation($filePath);
		
		$imageSize = getimagesize($filePath);
		$imageFormat = $imageSize[2];
		
		$pathinfo = pathinfo($filePath);
		
		$imageWidth = $imageSize[0];
		$imageHeight = $imageSize[1];
		
		$resizeWidth = self::DYNAMIC_RESIZE_WIDTH;
		$resizeHeight = floor($imageHeight / $imageWidth * $resizeWidth);
		
		$resizeImage = imagecreatetruecolor($resizeWidth, $resizeHeight);
		$image = imagecreatefromstring(file_get_contents($filePath));
		imagecopyresampled($resizeImage, $image, 0, 0, 0, 0, $resizeWidth, $resizeHeight, $imageWidth, $imageHeight);
		
		$ext = $this->imageFormatExt[$imageFormat];
		list ($imageExt, $imageMethod) = explode('|', $ext);
		$fileName = $pathinfo['filename'] .'.'.$imageExt.'_s.jpg';
		$resizeImageFilePath = $pathinfo['dirname'] . DIRECTORY_SEPARATOR . $fileName;
		
		$imageMethod($resizeImage, $resizeImageFilePath);
		imagedestroy($resizeImage);
		
		return $fileName;
	}
	
	/**
	 * 缩放图片
	 */
	protected function resizeImage($filePath,$type='') {
		if (! file_exists($filePath)) {
			return false;
		}
		if($type=='account'){
			$resizeWidth = self::AVATAR_RESIZE_WIDTH;
			$resizeHeight = self::AVATAR_RESIZE_HEIGHT;
		}else{
			$resizeWidth = self::IMAGE_RESIZE_WIDTH;
			$resizeHeight = self::IMAGE_RESIZE_HEIGHT;
		}
		
		$filePath = $this->adjustPictureOrientation($filePath);

		$imageSize = getimagesize($filePath);
		$imageFormat = $imageSize[2];

		$pathinfo = pathinfo($filePath);

		$imageWidth = $imageSize[0];
		$imageHeight = $imageSize[1];

		$imageResizeWidth = floor($resizeWidth/ $resizeHeight * $imageHeight);
		if ($imageResizeWidth < $imageWidth) {
			$startY = 0;
			$startX = floor(($imageWidth - $imageResizeWidth) / 2);
			$imageResizeHeight = $imageHeight;
		} else {
			$startX = 0;
			$imageResizeWidth = $imageWidth;
			$imageResizeHeight = floor($resizeHeight / $resizeWidth * $imageWidth);
			$startY = floor(($imageHeight - $imageResizeHeight) / 2);
		}

		$resizeImage = imagecreatetruecolor($resizeWidth, $resizeHeight);
		$image = imagecreatefromstring(file_get_contents($filePath));
		imagecopyresampled($resizeImage, $image, 0, 0, $startX, $startY, $resizeWidth, $resizeHeight, $imageResizeWidth, $imageResizeHeight);

		$ext = $this->imageFormatExt[$imageFormat];
		list ($imageExt, $imageMethod) = explode('|', $ext);
		$fileName = $pathinfo['filename'] . '_s.' . $imageExt;
		$resizeImageFilePath = $pathinfo['dirname'] . DIRECTORY_SEPARATOR . $fileName;

		$imageMethod($resizeImage, $resizeImageFilePath);
		imagedestroy($resizeImage);

		return $fileName;
	}

	/**
	 * Adjust picture orientation by EXIF information
	 */
	protected function adjustPictureOrientation($picPath)
	{
		$paths = pathinfo($picPath);
		$rotateFilePath = $picPath;
		// Read EXIF for IMAGETYPE_JPEG
		$imageSize = getimagesize($picPath);
		$imageFormat = $imageSize [2];
		$orientationAngle = array(
			3 => 180,
			6 => -90,
			8 => 90
		);
		if ($imageFormat == IMAGETYPE_JPEG) {
			$exif = exif_read_data($picPath);
			if (isset($exif['Orientation'])) {
				if (array_key_exists($exif['Orientation'], $orientationAngle)) {
					$angle = $orientationAngle[$exif['Orientation']];
					$image = imagecreatefromstring(file_get_contents($picPath));
					$image = imagerotate($image, $angle, 0);

					$fileName = $paths['filename'] . '_adjust' . '.' . $paths['extension'];
					$rotateFilePath = $paths['dirname'] . DIRECTORY_SEPARATOR . $fileName;
					imagejpeg($image, $rotateFilePath);
				}
			}
		}

		return $rotateFilePath;
	}

}