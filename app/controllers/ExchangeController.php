<?php

namespace App\Controllers;

use App\Modules\CommentModule;
use App\Modules\ExchangeModule;
use App\Modules\UserModule;
use App\Modules\ItemModule;
use App\Modules\TagModule;
use App\Modules\BaseModule;
use App\Modules\ExpertCommentModule;
use App\Modules\CreativeValueRatingModule;
use App\Modules\LikeModule;
use App\Modules\ReportModule;
use App\Common\Page;
use App\Common\Utils;

/**
 * 创意世界交易所控制器
 *
 * 处理创意世界交易所相关功能，如发布交易等。
 */
class ExchangeController extends ItemController {
	protected $targetTypeString = 'exchange';
	
	/**
	 * 显示比赛发布页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getPublish($id = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		if(! Utils::isNatureNumber($id)) {
		    $id = 0;
		}
		$trade = 0;
		if($id) {	    		    
		    $trade = $this->getItem($id);
		    $userId = $this->getLoginUserId();
		    if(!$trade || $trade->user_id != $userId) {
		        return \Redirect::to ( \URL::to ( '/' ) . '/exchange/index' );
		    }
		}
		
		$this->data = compact('trade', 'id');
		return $this->showView ( 'exchange.publish' );
	}
	
	/**
	 * 提交交易发布请求
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postPublish() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$common = $this->getParamItemPublishCommon ();
			
			$price = $this->getParam ( 'price', 'required{error_price_empty}|natureNumber{error_price_not_integer}' );
			
			$id = $this->getParam('id');
			
			if($price && $price > 1000000000) {
			    $this->setParamError('price', 'error_pirce_over_10_billons');
			}
			
			$tagsNames = $this->getParam ( 'tags' );
			if ($tagsNames) {
				$tagsNames = $this->checkTags ( $tagsNames );
			}
			
			$this->outputParamErrorIfExist ();
			
			if(Utils::isNatureNumber($id)) {
			    $tradeId = ExchangeModule::update($id, $common, $price, $tagsNames, $this->getLoginUserId ());
			    
			    if(! $tradeId) {
			        $this->outputError('id', 'error_has_no_right');
			    }
			}else{
			$tradeId = ExchangeModule::publish ( $common, $price, $tagsNames, $this->getLoginUserId () );
			}
			
			return $this->outputContent ( $tradeId );
		} );
	}
	
	/**
	 * 显示比赛列表页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getIndex() {
		$status = ItemModule::STATUS_GOING;
		
		$ranks = ExchangeModule::getTradeRank ();
		UserModule::fillUsers ( $ranks );
		
		$status = ExchangeModule::LIST_SEARCH_TYPE_HOT;
		$lists = ExchangeModule::getListsByTagIdAndStatus ( $status );
		UserModule::fillUsers ( $lists );
		
		$tags = TagModule::getHotTags ( BaseModule::TYPE_EXCHANGE, BaseModule::HOT_TAG_NUMBER );
		
		$type = BaseModule::TYPE_EXCHANGE;
		
		$total = ExchangeModule::getTotalByTagIdAndStatus ( 0, $status );
		$pageTotal = ceil ( $total / BaseModule::ITEM_NUMBER_INDEX );
		
		$this->data = compact ( 'lists', 'pageTotal', 'ranks', 'tags', 'status', 'type' );
		return $this->showView ( 'exchange.list' );
	}
	
	/**
	 * 按查询条件显示列表
	 *
	 * @return \Illuminate\View\View
	 */
	public function getList() {
		$type = $this->getParam ( 'type', 'required|natureNumberAndZero' );
		$tagId = $this->getParam ( 'tag_id', 'required|natureNumberAndZero' );
		$page = $this->getParam ( 'page', 'required|natureNumberAndZero' );
		
		$this->outputParamErrorIfExist ();
		
		if (! $page) {
			$page = 1;
		}
		
		$offset = ($page - 1) * BaseModule::NUMBER_PER_PAGE;
		$lists = ExchangeModule::getListsByTagIdAndStatus ( $type, $tagId, $offset, BaseModule::NUMBER_PER_PAGE );
		UserModule::fillUsers ( $lists );
		
		$this->data = compact ( 'lists' );
		return $this->showView ( 'exchange.m-list' );
	}
	
	/**
	 * 显示交易详情页
	 *
	 * @param int $id
	 *        	交易ID
	 * @return \Illuminate\View\View
	 */
	public function getDetail($id = 0, $page = 1) {
		$trade = $this->checkItem ( $id );
		$targetId = $id;
		$targetType = BaseModule::TYPE_EXCHANGE;
		
		$limit = 10;
		$offset = ($page - 1) * $limit;
		$comments = CommentModule::getComments ( $targetType, $id, $offset, $limit );
		UserModule::fillUsers ( $comments );
		$commentNum = CommentModule::getCommentsCount ( $targetType, $id );
		
		$pageHtml = '';
		if ($commentNum > $limit) {
			$pageTotal = ceil ( $commentNum / $limit );
			$url = \URL::to ( '/' ) . '/exchange/detail/' . $id;
			$pageHtml = Page::genePageHtml ( $url, $page, $pageTotal ,'#comments');
		}
		
		$expertComments = ExpertCommentModule::getExpertComments ( $targetType, $id, 0,$offset, $limit );
		UserModule::fillUsers ( $expertComments );
		$this->fillTargetUserInfo ( $expertComments, $id );
		
		$userId = $this->getLoginUserId ();
		
		$creativeValueRating = 0;
		if ($userId) {
			$creativeValueRating = CreativeValueRatingModule::getCreativeVauleRating ( $userId, $targetType, $targetId );
		}
		$isLiked = LikeModule::isLiked ( $userId, $targetType, $targetId );
		$isReported = ReportModule::isExist ( $userId, $targetType, $targetId );
		
		$canExpertComment = $this->canExpertComment($userId, BaseModule::TYPE_EXCHANGE, $id, $trade);	
		
		$this->data = compact ( 'trade', 'commentNum', 'comments', 'pageHtml', 'expertComments', 'targetId', 'targetType', 'creativeValueRating', 'isLiked', 'isReported','canExpertComment' );
		return $this->showView ( 'exchange.detail' );
	}
	protected function getItem($id) {
		return ExchangeModule::getTradeById ( $id );
	}
	
	/**
	 * 填充专家点评对象用户信息
	 *
	 * @param array $expertComments        	
	 */
	protected function fillTargetUserInfo(&$expertComments, $targetId, $targetSubId = 0) {
		$trade = $this->getItem($targetId);
        
        if (! empty($trade)) {
            $user = UserModule::getUserById($trade->user_id);
            
            foreach ($expertComments as $expertComment) {
                $expertComment->target_user = $user;
            }
        }
    }

}