<?php namespace App\Controllers;
use App\Modules\HiringModule;
use App\Modules\ItemModule;
use App\Modules\UserModule;
use App\Modules\TagModule;
use App\Modules\BaseModule;
use App\Modules\CreativeValueRatingModule;
use App\Modules\ExpertCommentModule;
use App\Modules\LikeModule;
use App\Modules\ReportModule;
use App\Modules\CircleModule;
use App\Common\Page;
use App\Models\HiringParticipators;
use App\Models\User;
/**
 * 创意公益明星比赛控制器
 *
 * 处理创意公益明星比赛功能，如发布比赛、报名参赛等。
 */

class HiringController extends ItemController {

	const PARAM_HIRING_END_TIME = 'end_time';
	
	const DETAIL_INDEX_PARTICIPATOR_NUMBER = 3;
		
	protected $targetTypeString = 'hiring';
	
	/**
	 * 显示比赛列表页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getIndex($status = 0) {
		$lists = HiringModule::getListsByStatus ( $status, 0, BaseModule::ITEM_NUMBER_INDEX );
		UserModule::fillUsers($lists);
	    
		$ranks = HiringModule::getParticipatorRank();
		UserModule::fillUsers($ranks);
		CircleModule::fillIsFollows($ranks, $this->getLoginUserId());
		
		$tags = TagModule::getHotTags(BaseModule::TYPE_HIRING, BaseModule::HOT_TAG_NUMBER);
		
		$total = HiringModule::getTotalByTagIdAndStatus ( 0, $status );
		$pageTotal = ceil ( $total / BaseModule::ITEM_NUMBER_INDEX );
		
		$page = 1;
		
	    $this->data = compact('lists','pageTotal', 'ranks', 'tags','status', 'page');
		
		return $this->showView('hiring.list');                                                                                                                                                                                                                                                 
	}
	
	

	/**
	 * 显示比赛发布页
	 *                                                                                                
	 * @return \Illuminate\View\View
	 */
	public function getPublish($hiringId = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		if ($hiringId) {
			$hiring = $this->checkItem($hiringId);
			$hiring = ItemModule::fillContests([$hiring]);
			$prizes = HiringModule::getHiringPrizes($hiringId);
			$this->data = compact('hiring', 'prizes');
		}
		
		return $this->showView('hiring.publish');
	}
	
	/**
	 * 显示我要参赛页
	 */
	public function getApply($id = 0, $participatorId = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		if (!$this->checkItem($id)) {
			return \Redirect::to(\URL::to('/').'/hiring/index');
		}
		
		$hiring = $this->getItem($id);
		
		$participator = array();
		if ($participatorId) {
			$participator = HiringModule::getParticipator($participatorId);
		}
		
		$publisher = UserModule::getUserById($hiring->user_id);
		$prizes = HiringModule::getHiringPrizes($id);
		
		$this->data = compact('hiring', 'id', 'participator', 'publisher', 'prizes');
	
		return $this->showView ( 'hiring.apply' );
	}

	/**
	 * 提交比赛发布请求
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postPublish() {
		return $this->execute(function() {
			$this->requireAjaxLogin();
			
			$common = $this->getParamItemPublishCommon();

			$endTime = $this->getParamDate('end_time');
			
			$this->checkEndTimeLength($endTime, 'end_time', array('bigger' => 'error_hiring_end_time_invalid', 'small' => 'error_hiring_end_time_invalid'));
			
			$hiringType = $this->getParam('hiring_type');
			
			$prizes = $this->getParam('prizes');
			$this->checkPrize($prizes);
			if(! $prizes) {
			    $prizes = array();
			    $this->setParamError('prizes', 'error_hiring_amount_invalid');			    
			}
			
			$tagsNames = $this->getParam('tags');
			if ($tagsNames) {
				$tagsNames = $this->checkTags($tagsNames);
			}
			
			$this->outputParamErrorIfExist();
			
			$id = HiringModule::publish($common, $endTime, $hiringType, $prizes, $tagsNames, $this->getLoginUserId());
			
			if (!$id) {								
				$this->setParamError('prizes', 'error_hiring_amount_invalid');
			}
			
			$this->outputParamErrorIfExist();
			
			return $this->outputContent($id);
		});
	}
	
	/**
	 * 提交修改明星比赛请求
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updateHiring() {
		return $this->execute(function() {
			$this->requireAjaxLogin();
			
			$common = $this->getParamItemPublishCommon();

			$hiringId = $this->getParam('hiring_id', 'required|natrueNumber');
			$endTime = $this->getParamDate('end_time');
			
			$this->checkEndTimeLength($endTime, 'end_time', array('bigger' => 'error_hiring_end_time_invalid', 'small' => 'error_hiring_end_time_invalid'));
			
			$hiringType = $this->getParam('hiring_type');
			
			$prizes = $this->getParam('prizes');
			$this->checkPrize($prizes);
			if(! $prizes) {
			    $prizes = array();
			    $this->setParamError('prizes', 'error_hiring_amount_invalid');			    
			}
			
			$tagsNames = $this->getParam('tags');
			if ($tagsNames) {
				$tagsNames = $this->checkTags($tagsNames);
			}
			
			$this->outputParamErrorIfExist();
			
			$id = HiringModule::updateHiring($common, $hiringId, $this->getLoginUserId(), $endTime, $hiringType, $prizes, $tagsNames);
			
			if (!$id) {								
				$this->setParamError('prizes', 'error_hiring_amount_invalid');
			}
			
			$this->outputParamErrorIfExist();
			
			return $this->outputContent($id);
		});
	}
	
	/**
	 * 详情页公共数据
	 * @param int $id
	 * @return array:
	 */
	protected function getDetailData($id = 0,$type=1,$page = 1) {
		$hiring = HiringModule::getHiringById($id);

		
		$targetId = $id;
		$targetType = ItemModule::TYPE_HIRING;

		UserModule::fillUser($hiring);
		$userId = $this->getLoginUserId ();
		CircleModule::fillIsFollow($hiring, $userId);
		
		$prizes = HiringModule::getHiringPrizes($id);
		
		$participatorNum = HiringModule::getParticipatorNum($id);
		
		$expertCommentNum = ItemModule::getExpertCommentsNum($targetType, $id);
		
		$limit =  BaseModule::COMMENT_PAGE_SIZE ;
		$offset = ($page - 1) * $limit;
		$comments = HiringModule::getComments($targetType, $id, $offset, $limit);
		UserModule::fillUsers($comments);
		$commentNum = ItemModule::getCommentNum($targetType, $id);
		$pageHtml ='';
		if ($commentNum > $limit) {
			$pageTotal = ceil ( $commentNum / $limit );
			switch ($type){
				case 1:$tpl='detail';break;
				case 2:$tpl='hiring-detail';break;
				case 3:$tpl='expert-comments';break;
				case 4:$tpl='participators';break;
				case 5:$tpl='updates';break;
				case 6:$tpl='update';break;
				default:$tpl='detail';break;
			}
			$url = \URL::to ( '/' ) . '/hiring/'.$tpl.'/' . $id;
			$pageHtml = Page::genePageHtml ( $url, $page, $pageTotal ,'#comments');
		}
		
		$canApply = HiringModule::canApply($id, $this->getLoginUserId());
		
		$isLiked = LikeModule::isLiked($userId, $targetType, $targetId);
		$isReported = ReportModule::isExist($userId, $targetType, $targetId);
		
		return compact('id','hiring', 'prizes', 'participatorNum', 'expertCommentNum', 'commentNum','comments','pageHtml', 'targetId', 'targetType','isLiked','isReported', 'canApply');
	}

	/**
	 * 显示比赛详情页
	 *
	 * @param int $id 比赛ID
	 * @return \Illuminate\View\View
	 */
	public function getDetail($id = 0,$page = 1) {
		if (!HiringModule::getHiringById($id)) {
			return \Redirect::to(\URL::to('/').'/hiring/index');
		}
		
		$this->data = $this->getDetailData($id,1,$page);

		$this->data['updates'] = HiringModule::getUpdatesByHiringId($id);
		$winners = array();
		
		if ($this->data['hiring']->status == ItemModule::STATUS_END_SUCCESS) {
			$winners = HiringModule::getHiringPrizes($id);		
			UserModule::fillUsers($winners, array('winner_user_id')); 
		}
		$expertComments = ExpertCommentModule::getExpertComments(ItemModule::TYPE_HIRING, $id, 0,0, ItemModule::COMMENT_PAGE_SIZE);
		UserModule::fillUsers ( $expertComments );
		self::fillTargetUserInfo($expertComments);

		$participators = HiringModule::getParticipatorsByHiringId($id, 0, self::DETAIL_INDEX_PARTICIPATOR_NUMBER);
		UserModule::fillUsers($participators);
		
		$this->data['expertComments'] = $expertComments;
		$this->data['winners'] = $winners;
		$this->data['participators'] = $participators;
		$this->data['pageName'] = 'index';
		//echo '<pre>';print_r($this->data['winners']);exit;
		return $this->showView('hiring.detail');
	}
	
	/**
	 * 明星比赛详情页内页
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function getHiringDetail($id,$page = 1) {			
		if (!HiringModule::getHiringById($id)) {
			return \Redirect::to(\URL::to('/').'/hiring/index');
		}
		
		$this->data = $this->getDetailData($id,2,$page);
		$this->data['pageName'] = 'detail';
		
		return $this->showView('hiring.detail-index');
	}
	
	public function getExpertComments($id,$page = 1) {
		if (!HiringModule::getHiringById($id)) {
			return \Redirect::to(\URL::to('/').'/hiring/index');
		}
	
		$this->data = $this->getDetailData($id,3,$page);
	
		$expertComments = ItemModule::getExpertComments(ItemModule::TYPE_HIRING, $id, null, 0, BaseModule::COMMENT_PAGE_SIZE);
		UserModule::fillUsers ( $expertComments );
		self::fillTargetUserInfo($expertComments);
	
		$this->data['expertComments'] = $expertComments;
		$this->data['pageName'] = 'expertComment';
	
		return $this->showView('hiring.detail-expert-comments');
	}

	/**
	 *参赛者详情页（多个）
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function getParticipators($id = 0,$page = 1) {
		if (!HiringModule::getHiringById($id)) {
			return \Redirect::to(\URL::to('/').'/hiring/index');
		}
		
		$this->data = $this->getDetailData($id,4,$page);
		
		$participators = HiringModule::getParticipatorsByHiringId($id);
		UserModule::fillUsers($participators);
		
		$this->data['participators'] = $participators;
		$this->data['pageName'] = 'participators';
		
		return $this->showView('hiring.detail-participators');
	}
		
	/**
	 * 比赛动态页
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function getUpdates($id = 0,$page = 1) {
		if (!HiringModule::getHiringById($id)) {
			return \Redirect::to(\URL::to('/').'/hiring/index');
		}
		
		$this->data = $this->getDetailData($id,5,$page);
		
		$updates = HiringModule::getUpdatesByHiringId($id);
		
		$this->data['updates'] = $updates;
		$this->data['pageName'] = 'updates';
		
		return $this->showView('hiring.detail-updates');
	}
	
	/**
	 * 比赛单独动态页
	 */
	public function getUpdate($id, $updateId, $page = 1) {
		if (!HiringModule::getHiringById($id)) {
			return \Redirect::to(\URL::to('/').'/hiring/index');
		}
		
		$this->data = $this->getDetailData($id,6,$page);
		
		$update = ItemModule::getUpdateById($updateId);
		
		$this->data['update'] = $update;
		$this->data['pageName'] = 'updates';
		
		return $this->showView('hiring.detail-update-single');
	}

	/**
	 * 提交我要参赛请求接口
	 */
	public function postApply() {
		return $this->execute(function() {
			$this->requireAjaxLogin();
			
			$id    = $this->getParam('hiring_id', 'required');
			$works 	     = $this->getParam('works', 'required{error_hiring_apply_works_empty}');
			$cover       = $this->getParam('imagefile', 'required{error_hiring_apply_cover_empty}');
			$declaration = $this->getParam('declaration', 'maxLength[140]{error_hiring_apply_declaration_too_long}|minLength[2]{error_hiring_apply_declaration_too_short}');
			
			$this->outputParamErrorIfExist();
			
			$participatorInfo = compact('works', 'cover', 'declaration');
			
			$participatorId = HiringModule::applyHiringParticipator($participatorInfo, $id, $this->getLoginUserId());
			
			return $this->outputContent($participatorId);
		});
	}
	
	/**
	 * 参赛者详情页 （单个）
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function getParticipator($participatorId) {
		$participator = HiringModule::getParticipator($participatorId);
	
		if (!$participator) {
			return \Redirect::to(\URL::to('/').'/hiring/index');
		}
		UserModule::fillUser($participator);
		CircleModule::fillIsFollow($participator, $this->getLoginUserId());
	
		$hiring = $this->getItem($participator->hiring_id);
		UserModule::fillUser($hiring);
	
		$comments = ItemModule::getComments(ItemModule::TYPE_HIRING_WORK, $participatorId, 0, ItemModule::COMMENT_PAGE_SIZE);
		UserModule::fillUsers($comments);
	
		$expertComments = ItemModule::getExpertComments(ItemModule::TYPE_HIRING, $hiring->id, $participatorId, 0, BaseModule::COMMENT_PAGE_SIZE);
		UserModule::fillUsers($expertComments);
	
		$targetType = ItemModule::TYPE_HIRING_WORK;
		$targetId = $hiring->id;
		$targetSubId = $participatorId;
		$userId = $this->getLoginUserId ();
		$creativeValueRating = 0;
	
		$commentNum = ItemModule::getCommentNum ( $targetType, $targetId );
		$pageHtml = '';
		if ($this->isLogin ()) {
			$creativeValueRating = CreativeValueRatingModule::getCreativeVauleRating ( $userId,  $targetType ,$participatorId);
		}
		$isLiked = LikeModule::isLiked($userId, $targetType, $targetId);
		$isReported = ReportModule::isExist($userId, $targetType, $targetId);
		$canExpertComment = $this->canExpertComment ( $userId,BaseModule::TYPE_HIRING,$participator->hiring_id, $hiring,$participatorId);
		
		$prizes = HiringModule::getHiringPrizes($hiring->id);
		
		$this->data = compact('participator', 'hiring', 'expertComments','canExpertComment', 'comments', 'commentNum','pageHtml','targetType',  'targetId', 'targetSubId','isLiked','isReported','creativeValueRating', 'prizes');
	
		return $this->showView('hiring.detail-participator');
	}
	
	/**
	 * 提交撤回明星作品接口
	 */
	public function getCancelApply($participatorId) {
		if (!$this->isLogin()) {
			return $this->requireLogin ();
		}
		
		$participator = HiringModule::getParticipator($participatorId);
		
		if (!$participator) {
			return \Redirect::to(\URL::to('/').'/hiring/index');
		}

		HiringModule::cancelApply($participatorId, $this->getLoginUserId());
		
		return \Redirect::to(\URL::to('/').'/hiring/detail/' . $participator->hiring_id);
	}
	
	/**
	 * 提交修改明星作品接口
	 */
	public function postUpdateParticipator() {
		return $this->execute(function() {
			$this->requireAjaxLogin();
			
			$participatorId = $this->getParam('participator_id', 'required|natureNumber');
			$works = $this->getParam('works', 'required{error_hiring_apply_works_empty}');
			$cover = $this->getParam('imagefile', 'required{error_hiring_apply_cover_empty}');
			$declaration = $this->getParam('declaration', 'required{error_hiring_apply_declaration_empty}|maxLength[140]{error_hiring_apply_declaration_too_long}|minLength[2]{error_hiring_apply_declaration_too_short}');
			
			$this->outputParamErrorIfExist();
			
			$participatorInfo = compact('works', 'cover', 'declaration');
			
			HiringModule::updateParticipator($participatorId, $participatorInfo, $this->getLoginUserId());
			
			return $this->outputContent($participatorId);
		});
	}
	
	/**
	 * 提交选取获胜明星接口
	 */
	public function postChooseWinner() {
		return $this->execute(function() {
			$this->requireAjaxLogin();
			
			$participatorId = $this->getParam('participator_id', 'required|natureNumber');
			$prizeIds = $this->getParam('prize_id');
			
			$this->outputParamErrorIfExist();
				
			foreach ($prizeIds as $id) {
				$result = HiringModule::chooseWinner($participatorId, $this->getLoginUserId(), $id);
			}
			
			return $this->outputContent($result);
		});
	}
	
	/**
	 * 提交发布比赛动态接口
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postUpdate() {
		return $this->execute(function() {
			$this->requireAjaxLogin();
			
			$id = $this->getParam('contest_id', 'required|natureNumber');
			$title = $this->getParam('title', 'required{error_hiring_update_title_empty}|maxLength[60]{error_hiring_update_title_too_long}|minLength[2]{error_hiring_update_title_too_short}');
			$content = $this->getParam('content', 'required{error_hiring_update_content_empty}|maxLength[1000]{error_update_content_too_big}|minLength[1]{error_update_content_too_short}|line[15]{error_update_content_too_many_line}');
			
			$this->outputParamErrorIfExist();
			
			$content = $this->replaceLine($content);
			
			HiringModule::addUpdate($id, $this->getLoginUserId(), $title, $content);
			
			return $this->outputContent();
		});
	}
	
	/**
	 * 根据标签筛选赛
	 * 
	 * @return \Illuminate\Http\JsonResponse
	 */                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
	public function postChooseTag() {
	    return $this->execute(function() {
	        $tagId = $this->getParam('tag_id', 'required');
	        
	        $this->outputParamErrorIfExist();
	        
	        $lists = HiringModule::getListsByTagId($tagId);
	        
	        $this->data = compact('lists');
	        
	        return $this->showView('hiring.m-lists');
	    });
	}
	
	protected function getItem($id) {
		return HiringModule::getHiringById($id);
	}
	

	/**
	 * 填充专家点评对象用户信息
	 *
	 * @param array $expertComments
	 */
	protected function fillTargetUserInfo(&$expertComments,  $targetSubId = 0) {
	    if($targetSubId) {
	        $work = HiringModule::getPaticipatorById($targetSubId);
	        $user = UserModule::getUserById($work->user_id);
	        foreach($expertComments as $expertComment) {
	            $expertComment->target_user = $user;
	        }
	    }else{
	        $workIds = array();
	        foreach($expertComments as $expertComment) {
	            $workIds[] = $expertComment->target_sub_id;
	        }
	        if($workIds){
		        $participators = HiringModule::getPaticipatorByIds($workIds);
		        UserModule::fillUsers($participators);
	        }

	       
	        foreach($expertComments as $expertComment) {
	            foreach($participators as $participator) {
	                if($expertComment->target_sub_id == $participator->id) {
	                    $expertComment->target_user = $participator->user;
	                }
	            }
	        }
	    }
	}

}