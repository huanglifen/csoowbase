<?php
namespace App\Controllers;

use App\Modules\DynamicModule;
use App\Modules\CircleModule;
use App\Modules\UserModule;
use App\Modules\ContestModule;
use App\Modules\ExchangeModule;
use App\Modules\CommentModule;
use App\Modules\BaseModule;
use App\Common\Utils;
use App\Common\Page;

class DynamicController extends BaseController {

    protected $targetTypeString = 'dynamic';

    /**
     * 朋友圈动态页面
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIndex($circleId = 0, $page = 1) {
        if (! $this->isLogin()) {
            return $this->requireLogin();
        }
        $userId = $this->getLoginUserId();
        $datas = array();
        
        if (intval($circleId)) {
            $users = CircleModule::getMembersByCircleId($circleId);
        } else {
            $circleId = 0;
            $users = CircleModule::getAllMembersByUserId($userId);
        }
        
        $userIds = array();
        $totalNum = 0;
        
        foreach ($users as $user) {
            $userIds[] = $user->user_id;
        }
        if(!$circleId) {
            $userIds[]=$userId;
        }
        
        if (count($userIds)) {
            $offset = (intval($page) - 1) * BaseModule::DYNAMICS_PER_PAGE;
            $datas = DynamicModule::getDynamcicsByUserIds($userIds, $offset);
            $totalNum = DynamicModule::getDynamicsByUserIdsNum($userIds);
            DynamicModule::fillForwards($datas);           
            DynamicModule::fillActionContent($datas);
            DynamicModule::fillLikeInfo($datas, $userId);
            UserModule::fillUsers($datas);
        }
        
        $cur = $page;
        $totalPage = ceil($totalNum/BaseModule::DYNAMICS_PER_PAGE);
        $url = \URL::to('/') . '/dynamic/index/'.$circleId;
        $pageHtml = Page::genePageHtml($url,$cur,$totalPage);
        
        $circles = CircleModule::getCirclesByUserId($userId);
        
        $contests = ContestModule::getHotContest();
        $exchanges = ExchangeModule::getHotExchange();
        $carepeople = UserModule::getCarePeople();
        $isMe = 1;
        
        $this->data = compact('datas', 'circles', 'contests', 'exchanges', 'carepeople', 'isMe', 'circleId', 'totalPage','pageHtml');
        return $this->showView('circle.dynamic');
    }
    

    /**
     * 发布动态
     * 
     * @return \Illuminate\Http\JsonResponse
     */
	public function postPublishDynamic() {
		return $this->execute(function () {

			$this->requireAjaxLogin();
			$content = $this->getParam('content');
			$pic = $this->getParam('pic');
			$video = $this->getParam('video');

			if (! $content && ! $pic && ! $video) {
				$this->setParamError('content', 'error_empty_content');
			}

			$this->outputParamErrorIfExist();
			$dynamic = DynamicModule::createDynamic($this->getLoginUserId(), $content, $pic, $video);
			UserModule::fillUser($dynamic);
			$datas = array($dynamic);
			
			$isAddBdShareJs = false;
			$this->data = compact('datas','isAddBdShareJs');
			$result = $this->showView('common.dynamic')->__toString();

			return $this->outputContent($result);
		});
	}

    /**
     * 赞动态
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLike() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
            
            $userId = $this->getLoginUserId();
            $targetId = $this->getParam('target_id', 'required{error_not_empty}|natureNumber{target_id}');
            
            $this->outputParamErrorIfExist();
            
            $result = DynamicModule::saveLike($userId, $targetId);
            
            if (!$result) {								
				$this->setParamError('target_id', 'error_like_failure');
			}
			
			$this->outputParamErrorIfExist();
            
            return $this->outputContent($result);
        });
    }

    /**
     * 添加动态评论
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postComment() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
            
            $userId = $this->getLoginUserId();
            $content = $this->getParam('content', 'required');
            $targetId = $this->getParam('target_id', 'required{error_not_empty}|natureNumber{target_id}');
            $isForward = $this->getParam('is_forward', 'required');
            
            $this->outputParamErrorIfExist();
            
            $dynamicId = DynamicModule::createDynamicComment($userId, $targetId, $content);
            
            if ($isForward) {
                $dyContent = $content;
                
                $dynamic = DynamicModule::getDynamicById($targetId);
                if($dynamic->type == DynamicModule::TYPE_FORWARD) {
                    $forwardId = $dynamic->target_id;
                }else{
                    $forwardId = $targetId;
                }
                DynamicModule::createForward($userId, $forwardId, $dyContent);
            }

            $user = UserModule::getUserById($userId);
            $level = Utils::getLevelStyle($user->creative_index);
            
            $result = array('id' => $dynamicId, 'content' => $content, 'user' => $user->toArray(), 'level' => $level, 'create_time' => Utils::formatTime(time()));
            return $this->outputContent($result);
        });
    }

    /**
     * 转发
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postForward() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
            
            $userId = $this->getLoginUserId();
            $forwardId = $this->getParam('forward_id', 'required|natureNumber');
            $content = $this->getParam('content', 'required|mbmaxlength[140]{error_max_length_140}');
            
            $this->outputParamErrorIfExist();
            
            $dynamic = DynamicModule::createForward($userId, $forwardId, $content);
            
            UserModule::fillUser($dynamic);
		  
            $datas = array($dynamic);
			DynamicModule::fillForwards($datas);  
            	
            $this->data = compact('datas');
            $result = $this->showView('common.dynamic')->__toString();
            
            return $this->outputContent($result);
        });
    }

    /**
     * 获取动态评论
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postComments() {
        return $this->execute(function () {
            
            $targetId = $this->getParam('target_id', 'required|natureNumber');
            $page = $this->getParam('page', 'natureNumber');
            $type = $this->getParam('type');
            
            $this->outputParamErrorIfExist();
            
            if (! $page) {
                $page = 1;
            }
            
            if(! $type ) {
                $type = false;
            }

            $offset = ($page - 1) * DynamicModule::DYNAMIC_COMMENTS_PER_PAGE;
            $comments = DynamicModule::getDynamicComments($targetId, $offset);
            UserModule::fillUsers($comments);
            
            $commentNum = CommentModule::getCommentsCount(BaseModule::TYPE_DYNAMIC, $targetId);
            $totalPage = ceil($commentNum/DynamicModule::DYNAMIC_COMMENTS_PER_PAGE);
            if(! $type) {
                $tpl = 'common.dynamic-comment';
            }else{
                $tpl = 'common.m-dynamic-comment';
            }
            
            $this->data = compact('comments', 'page', 'totalPage');
            $comments = $this->showView($tpl)->__toString();
            
            return $this->outputContent($comments);
        });
    }

    /**
     * 生成分享到微博链接
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postShareWeibo() {
        return $this->execute(function () {
            $appkey = \Config::get('social.WEIBO_API_KEY');
            
            $url = $this->getParam('url', 'required');
            $title = $this->getParam('title', 'required');
            
            $this->outputParamErrorIfExist();
            
            $source = "创意世界";
            $sourceUrl = \URL::to("/");
            $pic = $this->getParam('pic');
            
            $shareUrl = "http://service.weibo.com/share/share.php?appkey=";
            $shareUrl .= $appkey . "&url=" . $url . "&title=" . $title . "&source=" . $source . "&sourceUrl=" . $sourceUrl . "&content=utf8";
            
            if ($pic) {
                $shareUrl .= "&pic=" . $pic;
            }
            
            return $this->outputContent($shareUrl);
        });
    }

    /**
     * 生成分享到微信的二维码地址
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postShareWeixin() {
        return $this->execute(function () {
            $userId = $this->getParam('user_id', 'required|natureNumber');
            
            $this->outputParamErrorIfExist();
            
            $url = \URL::to('/') . '/home/index/' . $userId;
            $shareUrl = "http://s.jiathis.com/qrcode.php?url=" . $url;
            
            return $this->outputContent($shareUrl);
        });
    }

    /**
     * 解析视频地址
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postParseVideoUrl() {
        return $this->execute(function () {
            
            $url = $this->getParam('url', 'required');
            $this->outputParamErrorIfExist();
            
            $result = $this->getVidioContentByUrl($url);
            return $this->outputContent($result);
        });
    }


	public function getVidioContentByUrl($url) {
		$patterns = array ();
		$patterns [0] = '/http:\/\/www\.tudou\.com\/programs\/view\/([\w\-]+)\/?/i';
		$patterns [1] = '/http:\/\/www\.tudou\.com\/listplay\/([\w\-]+)[\/\w\-]+/i';
		$patterns [6] = '/http:\/\/www\.tudou\.com\/albumplay\/([\w\-]+)[\/\w\-]+/i';
		$patterns [2] = '/http:\/\/v\.youku\.com\/v_show\/id_([\w\-=]+)\.html(.*)/i';
		$patterns [3] = '/http:\/\/www\.56\.com\/u\d+\/v_([\w\-]+)\.html/i';
		$patterns [4] = '/http:\/\/www\.56\.com\/w\d+\/play_album\-aid\-\d+_vid\-([^.]+)\.html/i';
		$patterns [5] = '/http:\/\/v\.ku6\.com\/.+\/([\w\-\.]+)\.html(.*)/i';
		$replacements = array ();
		$replacements [0] = 'http://www.tudou.com/v/$1';
		$replacements [1] = 'http://www.tudou.com/l/$1';
		$replacements [6] = 'http://www.tudou.com/a/$1';
		$replacements [2] = 'http://player.youku.com/player.php/sid/$1/v.swf';
		$replacements [3] = 'http://player.56.com/v_$1.swf';
		$replacements [4] = 'http://player.56.com/v_$1.swf';
		$replacements [5] = 'http://player.ku6.com/refer/$1/v.swf&auto=0';//http://player.ku6.com/refer/9aW01xMqQh-Nf5LhZgzEkA../v.swf
		$playUrl = preg_replace ( $patterns, $replacements, $url );

		if($playUrl == $url || $playUrl == NULL){
			return '';
		}else{
			$pos = strpos($playUrl,'?');
			if($pos){
				$playUrl = substr($playUrl,$pos);
			}
			return '<embed width="574" height="363" align="center" allowfullscreen="true" allowscriptaccess="always" menu="false" loop="false" play="false" wmode="transparent" src="' . $playUrl . '" pluginspage="http://www.macromedia.com/go/getflashplayer" class="edui-faked-video" type="application/x-shockwave-flash">';
		}
	}
}
