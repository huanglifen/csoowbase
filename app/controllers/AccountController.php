<?php

namespace App\Controllers;

use App\Common\Sms;
use App\Modules\AccountModule;
use App\Modules\UserModule;
use App\Modules\SchoolModule;
use App\Modules\OrganizationModule;
use App\Common\Page;
use App\Modules\BaseModule;
use App\Modules\Utils\MailModule;

/**
 * 账号控制器
 *
 * 处理与用户创意指数、现金、创创币相关事务。
 */
class AccountController extends BaseController {
	protected $targetTypeString = 'account';
	const AVATAR_RESIZE_WIDTH = 120;
	const AVATAR_RESIZE_HEIGHT = 120;
	public function getIndex() {
		$this->showView ( 'account.index' );
	}
	
	/**
	 * 创创币账户页
	 */
	public function getCoin() {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		$page = 1;
		$limit = AccountModule::ACCOUNT_PER_PAGE;
		$offset = ($page - 1) * $limit;
		
		$startTime = strtotime ( '-1 months' );
		$endTime = strtotime ( '+ 23 hours 59 minutes 59 seconds' );
		
		$coinLogsCount = AccountModule::searchAccountLogByUserIDCount ( $this->getLoginUserId (), $startTime, $endTime, AccountModule::ACCOUNT_TYPE_COIN );
		$pageHtml = Page::geneAjaxPageHtml($page,ceil($coinLogsCount/$limit));
		
		$coinLogs = AccountModule::searchAccountLogByUserID ( $this->getLoginUserId (), AccountModule::ACCOUNT_TYPE_COIN, 0, $startTime, $endTime, AccountModule::ACCOUNT_PER_PAGE, $offset );
		
		$this->data = compact('coinLogsCount', 'coinLogs', 'pageHtml');
		return $this->showView ( 'account.coin' );
	}
	
	/**
	 * 创意指数账户页
	 */
	public function getCreativeIndex() {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		$page = 1;
		$limit = AccountModule::ACCOUNT_PER_PAGE;
		$offset = ($page - 1) * $limit;
		
		$startTime = strtotime ( '-1 months' );
		$endTime = strtotime ( '+ 23 hours 59 minutes 59 seconds' );
		
		$creativeIndexLogsCount = AccountModule::searchAccountLogByUserIDCount ( $this->getLoginUserId (), $startTime, $endTime, AccountModule::ACCOUNT_TYPE_CREATIVE );
		$pageHtml = Page::geneAjaxPageHtml($page,ceil($creativeIndexLogsCount/$limit));
		$creativeIndexLogs = AccountModule::searchAccountLogByUserID ( $this->getLoginUserId (), AccountModule::ACCOUNT_TYPE_CREATIVE, 0, $startTime, $endTime, AccountModule::ACCOUNT_PER_PAGE, $offset );
		
		$this->data = compact('creativeIndexLogsCount', 'pageHtml', 'creativeIndexLogs');
		return $this->showView ( 'account.creative-index' );
	}
	
	/**
	 * ajax获取账户分页
	 * 
	 */
	public function postCoin() {
	    return $this->execute(function() {
	        
	        $this->requireAjaxLogin();
	        
            $way = $this->getParam('way');
            $startTime = $this->getParam('startTime');
            $endTime = $this->getParam('endTime'); 
            $page = $this->getParam('page');
            $typeTime = $this->getParam('type_time');
            
            if($typeTime){
                switch ($typeTime){
                    case 1:
                        $startTime = strtotime(date('Y-m-d'));
                        break;
                    case 2:
                        $startTime = strtotime ( '-1 months' );
                        break;
                    case 3:
                        $startTime = strtotime( ' -3 months' );
                        break;
                    case 4:
                        $startTime = strtotime(' last year');
                        break;
                    
                }
                $endTime = strtotime(date('Y-m-d',strtotime('+1 day')));
            }else{
                
                if (!$startTime) {
                    $startTime = strtotime(date('Y-m-d'));
                }else {
                    $startTime = strtotime($startTime);
                }
                if(!$endTime) {
                    $endTime = strtotime(date('Y-m-d',strtotime('+1 day')));
                }else {
                    $endTime = strtotime($endTime);
                }
            }
            
            if(!$way) {
                $way = 0;
            }
            
            if (!$page) {
                $page = 1;
            } 
            
            $limit = AccountModule::ACCOUNT_PER_PAGE;
            $offset = ($page - 1) * $limit;
            
            $coinLogsCount = AccountModule::searchAccountLogByUserIDCount ( $this->getLoginUserId (), $startTime, $endTime, AccountModule::ACCOUNT_TYPE_COIN, $way);
            $pageHtml = Page::geneAjaxPageHtml($page,ceil($coinLogsCount/$limit));
            
            $coinLogs = AccountModule::searchAccountLogByUserID ( $this->getLoginUserId (), AccountModule::ACCOUNT_TYPE_COIN, $way, $startTime, $endTime, AccountModule::ACCOUNT_PER_PAGE, $offset );
            
            $this->data = compact('coinLogs', 'pageHtml');
            
            $result = $this->showView ( 'account.m-coin' )->__toString ();
            
            $startTime = date('Y/m/d',$startTime);
            $endTime = date('Y/m/d',$endTime);
            return $this->output('ok',['content'=>$result,'startTime'=>$startTime,'endTime'=>$endTime]);
            
	    });
	} 
	/**
	 * ajax获取创意指数分页
	 *
	 */
	public function postCreative() {
	    return $this->execute(function() {
	         
	        $this->requireAjaxLogin();
	         
	        $way = $this->getParam('way');
	        $startTime = $this->getParam('startTime');
	        $endTime = $this->getParam('endTime');
	        $page = $this->getParam('page');
	        $typeTime = $this->getParam('type_time');
	
	        if($typeTime){
	            switch ($typeTime){
	                case 1:
	                    $startTime = strtotime(date('Y-m-d'));
	                    break;
	                case 2:
	                    $startTime = strtotime ( '-1 months' );
	                    break;
	                case 3:
	                    $startTime = strtotime( ' -3 months' );
	                    break;
	                case 4:
	                    $startTime = strtotime(' last year');
	                    break;
	
	            }
	            $endTime = strtotime(date('Y-m-d',strtotime('+1 day')));
	        }else{
	
	            if (!$startTime) {
	                $startTime = strtotime(date('Y-m-d'));
	            }else {
	                $startTime = strtotime($startTime);
	            }
	            if(!$endTime) {
	                $endTime = strtotime(date('Y-m-d',strtotime('+1 day')));
	            }else {
	                $endTime = strtotime($endTime);
	            }
	        }
	
	        if(!$way) {
	            $way = 0;
	        }
	
	        if (!$page) {
	            $page = 1;
	        }
	
	        $limit = AccountModule::ACCOUNT_PER_PAGE;
	        $offset = ($page - 1) * $limit;
	
	        $creativeIndexLogsCount = AccountModule::searchAccountLogByUserIDCount ( $this->getLoginUserId (), $startTime, $endTime, AccountModule::ACCOUNT_TYPE_CREATIVE, $way);
	        $pageHtml = Page::geneAjaxPageHtml($page,ceil($creativeIndexLogsCount/$limit));
	
	        $creativeIndexLogs = AccountModule::searchAccountLogByUserID ( $this->getLoginUserId (), AccountModule::ACCOUNT_TYPE_CREATIVE, $way, $startTime, $endTime, AccountModule::ACCOUNT_PER_PAGE, $offset );
	
	        $this->data = compact('creativeIndexLogs', 'pageHtml');
	
	        $result = $this->showView ( 'account.m-creative-index' )->__toString ();
	        
	        $startTime = date('Y/m/d',$startTime);
	        $endTime = date('Y/m/d',$endTime);
	        
	        return $this->output('ok',['content'=>$result,'startTime'=>$startTime,'endTime'=>$endTime]);
	
	    });
	}
	/**
	 * 创意指数用户信息页
	 *
	 * @return \Illuminate\Http\RedirectResponse \Illuminate\View\View
	 */
	public function getUserInfo() {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		$userInfo = UserModule::getUserById ( $this->getLoginUserId () );
		$userInfo->school = SchoolModule::getSchoolInfoById ( $userInfo->school_id );
		$userInfo->organization = OrganizationModule::getOrganizationInfoById ( $userInfo->organization_id );
		
		$this->data ['userInfo'] = $userInfo;
		return $this->showView ( 'account.info' );
	}
	
	/**
	 * 账号安全页
	 *
	 * @return \Illuminate\Http\RedirectResponse \Illuminate\View\View
	 */
	public function getAccountSafe() {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		return $this->showView ( 'account.account-safe' );
	}
	
	/**
	 * 密码修改页
	 * 
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function getUserPassword() {
	    if (! $this->isLogin ()) {
	        return $this->requireLogin ();
	    }
	    
	    return $this->showView ( 'account.password' );
	}
	
	/**
	 * 邮箱修改页
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function getUserEmail() {
	    if (! $this->isLogin ()) {
	        return $this->requireLogin ();
	    }
	     
	    return $this->showView ( 'account.email' );
	}
	
	/**
	 * 电话号码修改页
	 * 
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function getUserPhone() {
	    if (! $this->isLogin ()) {
	        return $this->requireLogin ();
	    }

	    return $this->showView ( 'account.phone' );
	}
	/**
	 * 修改个人资料
	 * 
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postPersonal() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userBasicInfo ['birthday'] = $this->getParam ( 'birthday' );
			$userBasicInfo ['gender'] = $this->getParam ( 'gender' );
			$userBasicInfo ['school_id'] = $this->getParam ( 'school_id' );
			$userBasicInfo ['organization_id'] = $this->getParam ( 'organization_id' );
			$userBasicInfo ['tags'] = $this->getParam ( 'tags' );
			$userBasicInfo ['introduction'] = $this->getParam ( 'introduction','mbMaxLength[200]{error_introduction_long}');
			
			$this->outputParamErrorIfExist ();
			
			$areaId = $this->getParam ( 'area_id' );
			if (! $areaId) {
				$userBasicInfo ['province_id'] = 0;
				$userBasicInfo ['city_id'] = 0;
				$userBasicInfo ['district_id'] = 0;
			} else {
				$areaIds = explode ( '|', $areaId );
				$userBasicInfo ['province_id'] = $areaIds [0];
				if ($areaIds [1] == '00') {
				    $userBasicInfo ['city_id'] = 0;
				    $userBasicInfo ['district_id'] = 0;
				}elseif ($areaIds [2] == '00' ) {
				    $userBasicInfo ['city_id'] = $areaIds [0].$areaIds [1];
				    $userBasicInfo ['district_id'] = 0;
				}else {
				    $userBasicInfo ['city_id'] = $areaIds [0].$areaIds [1];
				    $userBasicInfo ['district_id'] = $areaIds [0].$areaIds [1].$areaIds [2];
				}
			}
			
			
			if (! $userBasicInfo ['school_id']) {
				$userBasicInfo ['school_id'] = 0;
			}
			if (! $userBasicInfo ['organization_id']) {
				$userBasicInfo ['organization_id'] = 0;
			}
			if (! $userBasicInfo ['tags']) {
				$userBasicInfo ['tags'] = '';
			} else {
				$userBasicInfo ['tags'] = str_replace ( '，', ',', $userBasicInfo ['tags'] );
			}
			
			if (! $userBasicInfo ['introduction']) {
				$userBasicInfo ['introduction'] = '';
			}
			
			if (! $userBasicInfo ['birthday']) {
				$userBasicInfo ['birthday'] = '1970/1/1';
			}
			
			if (! $userBasicInfo ['gender']) {
				$userBasicInfo ['gender'] = 0;
			}
			
			AccountModule::updateUserBasicInfo ( $userBasicInfo, $this->getLoginUserId () );
			
			
			return $this->outputContent ();
		} );
	}
	
	/*
	 * 修改密码
	 */
	public function postPassword() {
		return $this->execute ( function () {
			
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$currentPassword = $this->getParam ( 'current_password', 'required{error_current_password_empty}' );
			$newPassword = $this->getParam ( 'new_password', 'required{error_password_empty}|minLength[6]{error_password_short}|maxLength[16]{error_password_long}' );
			$newPasswordAgain = $this->getParam ( 'new_password_again', 'required{error_password_again_empty}' );
			if (! $this->hasParamError ( 'new_password' )) {
			    if ($newPasswordAgain != $newPassword) {
			        $this->setParamError ( 'new_password_again', 'error_password_again_error' );
			    }
			}
			$user = UserModule::getUserById ( $userId );
			
			if (! $this->hasParamError ( 'current_password' )) {
			if (! AccountModule::checkHash ( $currentPassword, $user->password )) {
				$this->setParamError ( 'current_password', 'error_invalid_current_password' );
			}
			}
			$this->outputParamErrorIfExist ();
			
			AccountModule::updateUserPassword ( $currentPassword, $newPassword, $userId );
			
			return $this->outputContent ();
		} );
	}
	
	/**
	 * 根据学校名称搜索学校
	 *
	 * @return
	 *
	 *
	 */
	public function postSchool() {
		return $this->execute ( function () {
			
			$this->requireAjaxLogin ();
			
			$school = $this->getParam ( 'school' );
			
			$schools = AccountModule::getSchoolsByName ( $school );
			
			$this->data ['schools'] = $schools;
			
			$result = $this->showView ( 'account.m-school' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	/**
	 * 根据政企名称搜索学校
	 *
	 * @return
	 *
	 *
	 */
	public function postOrganization() {
		return $this->execute ( function () {
			
			$this->requireAjaxLogin ();
			
			$organization = $this->getParam ( 'organization' );
			
			$organizations = AccountModule::getOrganizationsByName ( $organization );
			
			$this->data ['organizations'] = $organizations;
			
			$result = $this->showView ( 'account.m-organization' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	
	public function getAwardCoin() {
	    
	    $this->requireAjaxLogin();
	    AccountModule::getUserCreativeIndexAward($this->getLoginUserId());
	    return \Redirect::to(\URL::to('/').'/account/coin');
	}
	
	public function getAwardCreative() {
	     
	    $this->requireAjaxLogin();
	    AccountModule::getUserCreativeIndexAward($this->getLoginUserId());
	    return \Redirect::to(\URL::to('/').'/account/creative-index');
	}
	/**
	 * 裁剪头像
	 */
	public function postCutAvatar() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$file = $this->getParam ( 'file_name', 'required' );
			$startX = $this->getParam ( 'start_x', 'required' );
			$startY = $this->getParam ( 'start_y', 'required' );
			$endX = $this->getParam ( 'end_x', 'required' );
			$endY = $this->getParam ( 'end_y', 'required' );
			
			$this->outputParamErrorIfExist ();
			
			$imageSize = getimagesize ( $file );
			$imageFormat = $imageSize [2];
			
			$pathInfo = pathinfo ( $file );
			
			$imageWidth = $imageSize [0];
			$imageHeight = $imageSize [1];
			
			$uploadWidth = 360;
			$uploadHeight = 360;
			
			if (($imageWidth > $uploadWidth) || ($imageHeight > $uploadHeight)) {
				if (($imageWidth / $imageHeight) > ($uploadWidth / $uploadHeight)) {
					$uploadHeight = round ( $uploadWidth * $imageHeight / $imageWidth );
				} elseif (($imageWidth / $imageHeight) < ($uploadWidth / $uploadHeight)) {
					$uploadWidth = round ( $uploadHeight * $imageWidth / $imageHeight );
				}
			} else {
				$uploadWidth = $imageWidth;
				$uploadHeight = $imageHeight;
			}
			
			$resizeWidth = self::AVATAR_RESIZE_WIDTH;
			$resizeHeight = self::AVATAR_RESIZE_HEIGHT;
			
			$resizeImage = imagecreatetruecolor ( $resizeWidth, $resizeHeight );
			$image = imagecreatefromstring ( file_get_contents ( $file ) );
			imagecopyresampled ( $resizeImage, $image, 0, 0, round ( ($startX / $uploadWidth) * $imageWidth ), round ( ($startY / $uploadHeight) * $imageHeight ), $resizeWidth, $resizeHeight, round ( (($endX - $startX) / $uploadWidth) * $imageWidth ), round ( (($endY - $startY) / $uploadHeight) * $imageHeight ) );
			
			$ext = $this->imageFormatExt [$imageFormat];
			list ( $imageExt, $imageMethod ) = explode ( '|', $ext );
			$fileName = $pathInfo ['filename'] . '_' . $resizeWidth . '.' . $imageExt;
			
			$resizeImageFilePath = $pathInfo ['dirname'] . '/' . $fileName;
			// echo '<pre>';print_r(DIRECTORY_SEPARATOR);exit;
			$imageMethod ( $resizeImage, $resizeImageFilePath );
			imagedestroy ( $resizeImage );
			
			$result = AccountModule::updateAvatar ( $this->getLoginUserId (), $resizeImageFilePath );
			
			return $this->outputContent ( $resizeImageFilePath );
		} );
	}
	

	
	/**
	 * 验证邮箱
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postOldEmail() {
	    return $this->execute(function() {
	         
	        $this->requireAjaxLogin();
	        $email = $this->getParam('email','required{error_email_empty}');
	         
	        $user = UserModule::getUserById($this->getLoginUserId());
	         
	        if (!$this->hasParamError('email') && $user->email != $email) {
	            $this->setParamError('email','email_verify_wrong');
	        }
	         
	        $this->outputParamErrorIfExist();
	         
	        return $this->outputContent();
	    });
	}
	
	/**
	 * 修改邮箱发送验证码
	 * 
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postSendEmail() {
	    return $this->execute(function() {
	        
	        $this->requireAjaxLogin();
	        $newEmail = $this->getParam('new_email', 'required{error_email_empty}|email{error_email_error}|emailNotExist{error_email_exist}');
	        
	        $this->outputParamErrorIfExist();
	        
	        $verifyCode = mt_rand(100000, 999999);
	        AccountModule::modifyUserEmailCodeAndTime($this->getLoginUserId(), $verifyCode, $newEmail);
	        MailModule::send($newEmail, '修改邮箱验证码', '您的修改邮箱验证码为:'.$verifyCode.',请在1小时内完成修改。如非本人操作，请注意账号安全。创意世界。');
	        
	        return $this->outputContent();
	    });
	}
	
	/**
	 * 修改用户邮箱
	 * 
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postUpdateEmail() {
	    return $this->execute(function() {
	        
	        $this->requireAjaxLogin();
	        $oldEmail = $this->getParam('old_email','required{error_email_empty}');
	        $user = UserModule::getUserById($this->getLoginUserId());
	         
	        if (!$this->hasParamError('old_email') && $user->email != $oldEmail) {
	            $this->setParamError('old_email','email_verify_wrong');
	        }
	        $newEmail = $this->getParam('new_email', 'required{error_email_empty}|email{error_email_error}|emailNotExist{error_email_exist}');
	        if (!$this->hasParamError('new_email') && $user->new_modify_email != $newEmail) {
	            $this->setParamError('new_email','new_email_not_equal');
	        }
	        $verifyCode = $this->getParam('verify_code', 'required{error_verify_code_empty}');
	        
	        if (!$this->hasParamError('verify_code') && $user->email_modify_code != $verifyCode) {
	            $this->setParamError('verify_code','error_verify_code_error');
	        }elseif (!$this->hasParamError('verify_code') && time() > $user->email_modify_time + 3600000) {
	            $this->setParamError('verify_code','error_verify_code_time_out');
	        }
	        $this->outputParamErrorIfExist();
	        
	        AccountModule::verifyEmail($user, $verifyCode, $newEmail);
	        
	        return $this->outputContent();
	        
	    });
	}


	/**
	 * 验证手机号是否正确
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postOldMobile() {
		return $this->execute(function() {

			$this->requireAjaxLogin();
			$oldMobile = $this->getParam('old_mobile', 'required{error_mobile_empty}|phoneNumber{error_mobile_err}');
			$user = UserModule::getUserById($this->getLoginUserId());
			if (!empty($user->mobile_phone_no) && $user->mobile_phone_no != $oldMobile) {
				$this->setParamError('old_mobile','error_old_mobile_not_equal');
			}

			$this->outputParamErrorIfExist();

			return $this->outputContent();
		});
	}

	/**
	 * 发送手机验证码
	 * @return \Illuminate\Http\JsonResponse|string
	 */
	public function postSendMobile() {
		return $this->execute(function() {

			$this->requireAjaxLogin();
			$newMobile = $this->getParam('new_mobile', 'required{error_mobile_empty}|phoneNumber{error_mobile_error}|mobileExist{error_mobile_exist}');

			$this->outputParamErrorIfExist();

			$verifyCode = mt_rand(100000, 999999);
			AccountModule::modifyUserMobileCodeAndTime($this->getLoginUserId(), $verifyCode, $newMobile);
			Sms::sendSms($newMobile,'您正在申请修改手机号码，验证码:'.$verifyCode.',请在1小时内完成修改。如非本人操作，请注意账号安全。创意世界。');

			return $this->outputContent();
		});
	}


	/**
	 * 保存和更新新手机号码
	 * @return \Illuminate\Http\JsonResponse|string
	 */
	public function postUpdateMobile() {
		return $this->execute(function() {

			$this->requireAjaxLogin();

			$user = UserModule::getUserById($this->getLoginUserId());

			if(!empty($user->mobile_phone_no)){
				$oldMobile = $this->getParam('old_mobile', 'required{error_mobile_empty}|phoneNumber{error_mobile_err}');
			}else{
				$oldMobile = $this->getParam('old_mobile');
			}
			$newMobile = $this->getParam('new_mobile', 'required{error_mobile_empty}|phoneNumber{error_mobile_error}|mobileExist{error_mobile_exist}');
			$verifyCode = $this->getParam('verify_code', 'required{error_verify_code_empty}');


			if (!empty($user->mobile_phone_no) && $user->mobile_phone_no != $oldMobile) {
				$this->setParamError('old_mobile','error_old_mobile_not_equal');
			}else{

				if ($user->mobile_modify_new != $newMobile) {
					$this->setParamError('new_mobile','error_new_mobile_not_equal');
				}elseif ($user->mobile_modify_code != $verifyCode) {
					$this->setParamError('verify_code','error_verify_code_error');
				}elseif (time() > $user->mobile_modify_time + 3600000) {
					$this->setParamError('verify_code','error_verify_code_time_out');
				}
			}
			$this->outputParamErrorIfExist();

			AccountModule::verifyMobile($user, $verifyCode, $newMobile);

			return $this->outputContent();
		});
	}

}