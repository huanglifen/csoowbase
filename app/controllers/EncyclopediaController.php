<?php

namespace App\Controllers;

use App\Modules\EncyclopediaModule;
use App\Modules\IndustryModule;
use App\Modules\UserModule;
use App\Modules\CommentModule;
use App\Modules\ExpertCommentModule;
use App\Modules\BaseModule;
use App\Modules\CreativeValueRatingModule;
use App\Modules\LikeModule;
use App\Modules\ReportModule;
use App\Common\Page;
use App\Modules\ExpertModule;

/**
 * 创意百科控制器
 *
 * 处理创意领域,分享创意功能
 */
class EncyclopediaController extends ItemController {
	protected $targetTypeString = 'encyclopedia';
	/**
	 * 显示创意百科列表页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getIndex() {
		$recommendIndustries = IndustryModule::getRecommendSubIndustriesAndExperts ();
		$firstIndustries = IndustryModule::getFirstIndustries ();
		$secondIndustries = IndustryModule::getSecondIndustries ();
		
		$this->data = compact ( 'recommendIndustries', 'firstIndustries', 'secondIndustries' );
		return $this->showView ( 'encyclopedia.index' );
	}
	
	/**
	 * 显示创意百科行业内页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getList($id = 0) {
		$industry = IndustryModule::getIndustryById ( $id );
		if (! $industry) {
			\App::abort ( 404 );
		}
		
		$industryUsers = ExpertModule::getSubIndustryExperts($id,10);
		
		$lists = EncyclopediaModule::getEncyclopediasByIndustryId ( $id );
		UserModule::fillUsers ( $lists );
		
		$tags = EncyclopediaModule::getTagsByIndustryId ( $id );
		
		$encyclopediasCount = EncyclopediaModule::getTotalByTagIdAndStatus ($id);
		$pageTotal = ceil ( $encyclopediasCount / BaseModule::ITEM_NUMBER_INDEX );
		
		$type = BaseModule::TYPE_ENCYCLOPEDIA;
		$this->data = compact ( 'industry', 'industryUsers', 'lists', 'encyclopediasCount', 'tags', 'pageTotal', 'type' );
		
		return $this->showView ( 'encyclopedia.list' );
	}
	/**
	 * 显示分享创意页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getPublish($id = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		$result = IndustryModule::getIndustryById ( $id );
		if (! $result) {
			return \Redirect::to ( \URL::to ( '/' ) . '/encyclopedia' );
		} else {
			$this->data ['industryId'] = $id;
			return $this->showView ( 'encyclopedia.publish' );
		}
	}
	
	/**
	 * 提交分享比赛请求
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postPublish() {
		return $this->execute ( function () {
			
			$this->requireAjaxLogin ();
			$title = $this->getParam ( 'title', 'required{error_title_empty}' );
			$cover = $this->getParam ( 'imagefile', 'required{error_cover_empty}' );
			$content = $this->getParam ( 'description', 'required{error_description_empty}' );
			$industryId = $this->getParam ( 'industry_id' );
			
			$tagsNames = $this->getParam ( 'tags' );
			if ($tagsNames) {
				$tagsNames = $this->checkTags ( $tagsNames );
			}
			
			$this->outputParamErrorIfExist ();
			
			$encyclopediaId = EncyclopediaModule::shareEncyclopedia ( $this->getLoginUserId (), $title, $cover, $content, $industryId, $tagsNames );
			
			return $this->outputContent ( $encyclopediaId );
		} );
	}
	
	/**
	 * 显示创意详情页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getDetail($id = 0, $page = 1) {
		$encyclopedia = $this->checkItem ( $id );
		
		$targetId = $id;
		$targetType = BaseModule::TYPE_ENCYCLOPEDIA;
		
		$limit = 10;
		$offset = ($page - 1) * $limit;
		$comments = CommentModule::getComments ( $targetType, $id, $offset, $limit );
		UserModule::fillUsers ( $comments );
		$commentNum = CommentModule::getCommentsCount ( $targetType, $id );
		
		$pageHtml = '';
		if ($commentNum > $limit) {
			$pageTotal = ceil ( $commentNum / $limit );
			$url = \URL::to ( '/' ) . '/encyclopedia/detail/' . $id;
			$pageHtml = Page::genePageHtml ( $url, $page, $pageTotal,'#comments' );
		}
		
		$expertComments = ExpertCommentModule::getExpertComments ( $targetType, $id, 0,$offset, $limit );
		UserModule::fillUsers ( $expertComments );
		
		$userId = $this->getLoginUserId ();
		
		$creativeValueRating = 0;
		if ($userId) {
			$creativeValueRating = CreativeValueRatingModule::getCreativeVauleRating ( $userId, $targetType, $targetId );
		}
		$isLiked = LikeModule::isLiked ( $userId, $targetType, $targetId );
		$isReported = ReportModule::isExist ( $userId, $targetType, $targetId );
		$canExpertComment = $this->canExpertComment($userId, $targetType, $id, $encyclopedia);
		
		$this->data = compact ( 'encyclopedia', 'commentNum', 'comments', 'pageHtml', 'expertComments', 'targetId', 'targetType', 'creativeValueRating', 'isLiked', 'isReported','canExpertComment' );
		
		return $this->showView ( 'encyclopedia.detail' );
	}
	protected function getItem($id) {
		return EncyclopediaModule::getEncyclopediaById ( $id );
	}
	
	/**
	 * ajax获取行业
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postIndustry() {
		return $this->execute ( function () {
			$id = $this->getParam ( 'id' );
			
			$this->outputParamErrorIfExist ();
			
			$secondIndustries = IndustryModule::getSecondIndustries ( $id );
			
			$this->data = compact ( 'secondIndustries' );
			
			$result = $this->showView ( 'encyclopedia.m-industry' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 显示出售创意页
	 *
	 * @param int $id        	
	 * @return \Illuminate\View\View
	 */
	public function getSell($id) {
		$encyclopedia = EncyclopediaModule::getEncyclopediaById ( $id );
		if ($encyclopedia->user_id != $this->getLoginUserId ()) {
			return \Redirect::to ( \URL::to ( '/' ) . '/encyclopedia/detail/' . $id );
		}
		$this->data ['encyclopedia'] = $encyclopedia;
		return $this->showView ( 'encyclopedia.sell' );
	}
	
	/**
	 * 填充专家点评对象用户信息
	 *
	 * @param array $expertComments        	
	 */
	protected function fillTargetUserInfo(&$expertComments, $targetId, $targetSubId = 0) {
		$trade = $this->getItem ( $targetId );
		
		if (! empty ( $trade )) {
			$user = UserModule::getUserById ( $trade->user_id );
			
			foreach ( $expertComments as $expertComment ) {
				$expertComment->target_user = $user;
			}
		}
	}
}
