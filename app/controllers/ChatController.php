<?php

namespace App\Controllers;

use App\Modules\ChatModule;
use App\Modules\UserModule;
use App\Modules\CircleModule;
use App\Modules\ContestModule;
use App\Modules\ExchangeModule;
use App\Common\Page;

class ChatController extends BaseController {
	
	/**
	 * 接口：创创召下的聊天消息--5条未读的最新消息
	 */
	public function postCczChat() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$chats = ChatModule::getUnreadChatByUser ( $userId, 0, 5 );
			UserModule::fillUsers ( $chats );
			
			$this->data = compact ( 'chats' );
			$result = $this->showView ( 'chat.ccz-chat' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 显示聊天消息页面
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getIndex($type = 0, $page = 1) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		$userId = $this->getLoginUserId ();
		
		if (! intval ( $page )) {
			$page = 1;
		}
		
		$offset = (intval ( $page ) - 1) * ChatModule::MESSAGE_PER_PAGE;
		
		$notifications = ChatModule::getChatsByStatus ( $userId, $type, $offset );
		UserModule::fillUsers ( $notifications );
		
		$num = ChatModule::getChatsNumByStatus($userId, $type);
		
		$cur = $page;
		$totalPage = ceil($num/ChatModule::MESSAGE_PER_PAGE);
		
		$url = \URL::to('/') . '/chat/index/'.$type;
		$page = Page::genePageHtml($url,$cur,$totalPage);
		
		$contests = ContestModule::getHotContest ();
		$exchanges = ExchangeModule::getHotExchange ();
		$carepeople = UserModule::getCarePeople ($userId);
		
		$isMe = 1;
		$this->data = compact ( 'notifications', 'contests', 'exchanges', 'carepeople', 'isMe', 'type', 'page' );
		return $this->showView ( 'circle.notification' );
	}
	
	/**
	 * 显示朋友圈联系人页面
	 *
	 * @param number $toUserId        	
	 * @return \Illuminate\Http\JsonResponse \Illuminate\View\View
	 */
	public function getContact($toUserId = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		$userId = $this->getLoginUserId ();
		$membersIds = ChatModule::getAllContacter ( $userId );
		
		$members = array();
		if(count($membersIds)) {
		    $members = UserModule::getUserByIds ( $membersIds );
		    ChatModule::fillNotification ( $members, $userId );
		}
		if ($toUserId == $userId) {
			$toUserId = 0;
		}
		
		if (! $toUserId) {
			if (count ( $members )) {
				$toUserId = $members [0]->id;
			}
		}
		$chats = array ();
		$toUser = 0;
		if ($toUserId) {
			$chats = ChatModule::getChats ( $userId, $toUserId );
			ChatModule::updateChatReaded ( $toUserId, $userId );
			$toUser = UserModule::getUserById ( $toUserId );
			if ($toUser && ! in_array ( $toUserId, $membersIds )) {
				$members[] = $toUser;
			}
		}
		$circles = CircleModule::getCirclesByUserId ( $userId );
		
		$isMe = 1;
		$circleId = 0;
		$show = false;
		
		if(count($chats) >= ChatModule::CHAT_PER_PAGE) {
		    $show = true;
		}
		$this->data = compact ( 'circles', 'members', 'chats', 'toUser', 'isMe', 'circleId', 'chatTotal', 'show');
		return $this->showView ( 'circle.contact' );
	}
	
	/**
	 * 获取圈子联系人或者陌生人
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postCircleMembers() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$circleId = $this->getParam ( 'circle_id', 'required' );
			
			$this->outputParamErrorIfExist ();
			
			$members = array ();
			
			if ($circleId == - 1) {
				$members = ChatModule::getAllContacter ( $userId );
				if(count($members)) {
				$members = UserModule::getUserByIds ( $members );
				}
			} elseif ($circleId) {
				$authority = CircleModule::getCircleAuthority ( $circleId, $userId );
				
				if ($authority) {
					$circleMembers = CircleModule::getMembersByCircleId ( $circleId );
					UserModule::fillUsers ( $circleMembers );
					foreach ( $circleMembers as $m ) {
						$members [] = $m->user;
					}
				}
			} elseif ($circleId == 0) {
				$userIds = ChatModule::getStranger ( $userId );
				if (count ( $userIds )) {
					$members = UserModule::getUserByIds ( $userIds );
				}
			}
			
			$chats = array ();
			$toUser = 0;
			if (count ( $members )) {
				$toUserId = $members [0]->id;
				$toUser = UserModule::getUserById ( $toUserId );
				
				$chats = ChatModule::getChats ( $userId, $toUserId );
				ChatModule::updateChatReaded ( $toUserId, $userId );
				ChatModule::fillNotification ( $members, $userId );
			}
			
			$circles = CircleModule::getCirclesByUserId ( $userId );
			
			$this->data = compact ( 'members', 'chats', 'circles', 'toUser', 'circleId', 'chatTotal', 'showMore' );
			$list =  $this->showView ( 'circle.m-contact-list' )->__toString();
			$content = $this->showView('circle.m-contact')->__toString();
			
			$showMore = false;
			if(count($chats) >= ChatModule::CHAT_PER_PAGE) {
			    $showMore = true;
			}
			return $this->outputContent(array('list' => $list, 'content' => $content, 'show' => $showMore));
		} );
	}
	
	/**
	 * 发送一条聊天消息
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postSendMessage() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			
			$toUserId = $this->getParam ( 'to_user_id', 'required|natureNumber' );
			$content = $this->getParam ( 'content', 'required{error_content_empty}|maxLength[1000]{error_message_over_1000_word}' );
			
			$this->outputParamErrorIfExist ();
			
			$time = time();
			
			$chatId = ChatModule::sendChat ( $userId, $toUserId, $content );
			
			$create_time = date('Y-m-d', $time);
			$send_time = date('H:i', $time);
			
			$result = array('content' => $content, 'chatId' => $chatId, 'create_time' => $create_time, 'send_time' => $send_time, 'time' => $time);
			
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 获取和对方的聊天消息
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postChat() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			
			$toUserId = $this->getParam ( 'to_user_id', 'required|natureNumber' );
			$topChatId = $this->getParam ( 'top_chat_id', 'required|natureNumberAndZero' );
			
			$this->outputParamErrorIfExist ();
			
			$chats = ChatModule::getChats ( $userId, $toUserId, $topChatId );	
			$toUser = UserModule::getUserById ( $toUserId );
			
			$notChange = true;
			if(!$topChatId) {
			    $notChange = false;
			    ChatModule::updateChatReaded ( $toUserId, $userId );
			}
			
			$showMore = false;
			if(count($chats) >= ChatModule::CHAT_PER_PAGE) {
			    $showMore = true;
			}
			$create_time = 0;
			$length = count($chats);
			if($length) {
			    $create_time =date('Y-m-d', $chats[$length-1]->create_time);
			}
			
			$this->data = compact ( 'chats', 'toUser' ,'chatTotal', 'showMore', 'notChange');
			$result = $this->showView('circle.m-contact')->__toString();
			return $this->outputContent ( array('content' => $result, 'show' => $showMore, 'create_time' => $create_time) );
		} );
	}
	
	/**
	 * 获取最新聊天消息
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postCurrentChat() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			
			$sendUserId = $this->getParam ( 'send_user_id', 'required|natureNumber' );
			$lastChatId = $this->getParam ( 'last_chat_id', 'natureNumberAndZero' );
			
			if(! $lastChatId) {
			    $lastChatId = 0;
			}
			$this->outputParamErrorIfExist ();
			
			$chats = ChatModule::getCurrentChat ( $sendUserId, $userId, $lastChatId );
			$toUser = UserModule::getUserById ( $sendUserId );
			
			$current = true;
			$notChange = true;
			$this->data = compact('chats', 'toUser', 'current', 'notChange');
			$result = $this->showView('circle.m-contact')->__toString();
			return $this->outputContent ( $result );
		} );
	}
}