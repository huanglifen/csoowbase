<?php

namespace App\Controllers;

use App\Common\Utils;
use App\Modules\UserModule;
use App\Modules\ItemModule;
use App\Modules\BaseModule;
use App\Modules\CreativeValueRatingModule;
use App\Modules\TagModule;
use App\Modules\ProjectModule;
use App\Modules\TaskModule;
use App\Modules\HiringModule;
use App\Modules\ExchangeModule;
use App\Modules\EncyclopediaModule;
use App\Modules\ExpertModule;
use App\Modules\CommentModule;
use App\Modules\ExpertCommentModule;
use App\Modules\GuessModule;
use App\Modules\CircleModule;
use App\Models\Hiring;
use App\Models\Project;
use App\Models\Task;
use App\Modules\AreaModule;

/**
 * 比赛/交易控制器基类
 *
 * 处理比赛/交易公共部分。
 */
abstract class ItemController extends BaseController {
	abstract protected function getItem($id);
	
	/**
	 * 获取比赛/交易发布公共参数
	 *
	 * @return array
	 */
	protected function getParamItemPublishCommon() {
		$title = $this->getParam ( 'title', 'required{error_title_empty}|minLength[2]{error_title_too_short_or_long}|maxLength[60]{error_title_too_short_or_long}' );
		$description = $this->getParam ( 'description', 'required{error_description_empty}' );
		$cover = $this->getParam ( 'imagefile', 'required{error_cover_empty}' );
		
		return compact ( 'title', 'description', 'cover' );
	}
	
	/**
	 * 根据提交的天数计算结束时间戳
	 */
	protected function getEndTimestamp($dayNum) {
		return time () + $dayNum * 24 * 3600;
	}
	
	/**
	 * 检查比赛/交易是否存在
	 *
	 * @param
	 *        	$id
	 * @return Item
	 */
	protected function checkItem($id) {
		$item = null;
		if (! Utils::isNatureNumber ( $id ) || ! $item = $this->getItem ( $id )) {
			\App::abort ( 404 );
		}
		
		UserModule::fillUser ( $item );
		CircleModule::fillIsFollow ( $item, $this->getLoginUserId () );
		return $item;
	}
	protected function checkTags($tagsStr) {
		$tagsStr = str_replace ( '，', ',', $tagsStr );
		$tags = explode ( ',', $tagsStr );
		
		if ($tags) {
			foreach ( $tags as $tag ) {
				$num = 0;
				if ($tag) {
					if (! Utils::isTag ( $tag )) {
						$this->setParamError ( 'tags', 'error_tag_name_invalid' );
					}
					
					$tag = mb_convert_encoding ( $tag, 'GB18030', 'UTF-8' );
					
					if (strlen ( $tag ) < 2 || strlen ( $tag ) > 12) {
						$this->setParamError ( 'tags', 'error_tag_length_invalid' );
					}
					
					$num ++;
					if ($num > 5) {
						$this->setParamError ( 'tags', 'error_tags_number_cant_bigger_than_five' );
					}
				}
			}
		}
		
		return $tags;
	}
	protected function checkPrize($prize) {
		if (is_array ( $prize )) {
			foreach ( $prize as $pr ) {
				if (strlen ( $pr ['award'] ) > 180) {
					$this->setParamError ( 'prizes', 'error_prizes_award_too_big' );
				}
			}
		}
	}
	
	/**
	 * 给比赛打分
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postScoreItem() {
		return $this->execute ( function () {
			$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'required|natureNumber' );
			$score = $this->getParam ( 'score', 'required|natureNumber|maxLength[10]|minLength[1]' );
			
			$this->outputParamErrorIfExist ();
			
			$score = CreativeValueRatingModule::saveCreativeValueRating ( $this->getLoginUserId (), $targetType, $targetId, $score );
			
			return $this->outputContent ( $score );
		} );
	}
	/**
	 * 发布评论接口
	 */
	public function postPublishComment() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'required|natureNumber' );
			$content = $this->getParam ( 'content', 'required' ); // |line[15]{error_comment_too_many_line}
			
			if (in_array ( $targetType, array (
					BaseModule::TYPE_REGION,
					BaseModule::TYPE_SCHOOL,
					BaseModule::TYPE_ORGANIZATION 
			) )) {
				$flag = 'area';
				$len = BaseModule::COMMENT_LEN_AREA;
			} else {
				$flag = '';
				$len = BaseModule::COMMENT_LEN_NO_AREA;
			}
			
			$content = Utils::truncate_utf8_string ( $content, $len, '...' );
			
			$this->outputParamErrorIfExist ();
			
			$id = ItemModule::publishComment ( $userId, $targetType, $targetId, $content );
			$comment = CommentModule::getCommentById ( $id );
			UserModule::fillUser ( $comment );
			
			$comments [] = $comment;
			$this->data = compact ( 'comments' );
			if ($flag == 'area') {
				$result = $this->showView ( 'area.m-comments' )->__toString ();
			} else {
				$result = $this->showView ( 'common.m-comments' )->__toString ();
			}
			
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 显示评论列表
	 *
	 * @return \Illuminate\Http\JsonResponse \Illuminate\View\View
	 */
	public function postComments() {
		return $this->execute ( function () {
			$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'required|natureNumber' );
			$page = $this->getParam ( 'page' );
			$this->outputParamErrorIfExist ();
			
			$limit = BaseModule::COMMENT_PAGE_SIZE;
			if (! $page) {
				$page = 1;
			}
			$offset = ($page - 1) * $limit;
			
			$comments = ItemModule::getComments ( $targetType, $targetId, $offset, $limit );
			UserModule::fillUsers ( $comments );
			
			$this->data = compact ( 'comments' );
			
			$result = $this->showView ( 'common.comments' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 发布一条专家点评
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postPublishExpertComment() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'required|natureNumber' );
			$targetSubId = $this->getParam ( 'target_sub_id', 'required|natureNumberAndZero' );
			$content = $this->getParam ( 'content', 'required{error_expert_comment_no_empty}' ); // |line[15]{error_expert_comment_too_many_line}
			$score = $this->getParam ( 'score', 'required|natureNumber' );
			$this->outputParamErrorIfExist ();
			
			if (! $targetSubId) {
				$targetSubId = 0;
			}
			$id = ItemModule::publishExpertComment ( $userId, $targetType, $targetId, $score, $content, $targetSubId );
			$expertComment = ExpertCommentModule::getExpertCommentById ( $id );
			UserModule::fillUser ( $expertComment );
			
			$expertComments [] = $expertComment;
			if (function_exists ( 'fillTargetUserInfo' )) {
				$this->fillTargetUserInfo ( $expertComments, $targetId, $targetSubId );
			}
			
			$this->data = compact ( 'expertComments', 'targetType' );
			$result = $this->showView ( 'common.m-expert-comments' )->__toString ();
			
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 获取专家点评
	 *
	 * @return \Illuminate\Http\JsonResponse \Illuminate\View\View
	 */
	public function postExpertComments() {
		return $this->execute ( function () {
			$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'required|natureNumber' );
			$targetSubId = $this->getParam ( 'target_sub_id', 'required|natureNumberAndZero' );
			$page = $this->getParam ( 'page' );
			
			$this->outputParamErrorIfExist ();
			
			$limit = BaseModule::COMMENT_PAGE_SIZE;
			if (! $page) {
				$page = 1;
			}
			$offset = ($page - 1) * $limit;
			
			if (! $targetSubId) {
				$targetSubId = 0;
			}
			$expertComments = ItemModule::getExpertComments ( $targetType, $targetId, $targetSubId, $offset, $limit );
			UserModule::fillUsers ( $expertComments );
			
			if (function_exists ( 'fillTargetUserInfo' )) {
			    if($targetSubId) {
			        $targetUserInfoId = $targetSubId;
			    }else{
			        $targetUserInfoId = $targetId;
			    }
				$this->fillTargetUserInfo ( $expertComments, $targetUserInfoId);
			}
			
			$this->data = compact ( 'expertComments' );
			$result = $this->showView ( 'common.m-expert-comments' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 *
	 * @param number $type
	 *        	0站内 1站外
	 * @param number $targetType
	 *        	功能模块
	 * @param number $targetId        	
	 * @param number $targetSubId        	
	 * @return \Illuminate\View\View
	 */
	public function getInviteExpert($type = 0, $targetType = 0, $targetId = 0, $targetSubId = 0) {
		switch ($targetType) {
			case 1 :
				$urlName = 'project';
				break;
			case 21 :
			case 2 :
				$urlName = 'task';
				break;
			case 31 :
			case 3 :
				$urlName = 'hiring';
				break;
			case 4 :
				$urlName = 'exchange';
				break;
			case 5 :
				$urlName = 'encyclopedia';
				break;
			default :
				;
		}
		$this->data = compact ( 'type', 'urlName', 'targetType', 'targetId', 'targetSubId' );
		return $this->showView ( 'common.invite-expert' );
	}
	
	/**
	 * 搜索站内专家
	 *
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postSearchInternalExpert() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$name = $this->getParam ( 'keyword', 'required' );
			
			$this->outputParamErrorIfExist ();
			
			$this->data ['experts'] = ExpertModule::searchInternalExpert ( $name );
			
			$result = $this->showView ( 'common.search-expert' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 选择要邀请的站内专家
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postSelectInternalExpert() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getParam ( 'user_id', 'required|natureNumber' );
			$this->outputParamErrorIfExist ();
			
			$this->data ['userCard'] = UserModule::getUserDetailByUserId ( $userId );
			$this->data ['invite'] = 1;
			$result = $this->showView ( 'user.user-card' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 邀请站内专家
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postInviteInternalExpert() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'required|natureNumber' );
			$targetSubId = $this->getParam ( 'target_sub_id', 'required|natureNumberAndZero' );
			$userIds = $this->getParam ( 'user_ids', 'required' );
			
			$this->outputParamErrorIfExist ();
			
			$userIds = substr ( $userIds, 0, - 1 );
			$userIds = explode ( ',', $userIds );
			
			foreach ( $userIds as $expertUserId ) {
				ItemModule::inviteInternalExpert ( $userId, $targetType, $targetId, $expertUserId, $targetSubId );
			}
			
			return $this->outputContent ();
		} );
	}
	
	/**
	 * 邀请站外专家
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postInviteExternalExpert() {
		$this->requireAjaxLogin ();
		
		$userId = $this->getLoginUserId ();
		
		$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
		$targetId = $this->getParam ( 'target_id', 'required|natureNumber' );
		$targetSubId = $this->getParam ( 'target_sub_id', 'required|natureNumberAndZero' );
		$lastPage = $this->getParam ( 'lastPage' );
		$expertInfo = array ();
		$expertInfo ['name'] = $this->getParam ( 'name', 'required' );
		$expertInfo ['industry'] = $this->getParam ( 'industry', 'required' );
		$expertInfo ['email'] = $this->getParam ( 'email', 'required|email' );
		$expertInfo ['mobile_phone_no'] = $this->getParam ( 'mobile_phone_no', 'required|PhoneNumber' );
		
		// $this->outputParamErrorIfExist ();
		
		$inviteId = ItemModule::inviteExternalExpert ( $userId, $targetType, $targetId, $expertInfo, $targetSubId );
		
		return \Redirect::to ( $lastPage );
	}
	
	/**
	 * 喜欢
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postLike() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'required|natureNumber' );
			
			$this->outputParamErrorIfExist ();
			
			$endorseId = ItemModule::addLike ( $userId, $targetType, $targetId );
			
			if(in_array($targetType,array(BaseModule::TYPE_REGION,BaseModule::TYPE_SCHOOL,BaseModule::TYPE_ORGANIZATION))){
				AreaModule::addLikeCount($targetType, $targetId );
			}
			
			return $this->outputContent ( $endorseId );
		} );
	}
	
	/**
	 * 举报
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postReport() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'required|natureNumber' );
			$type = $this->getParam ( 'type', 'required|natureNumber' );
			
			$this->outputParamErrorIfExist ();
			
			$reportId = ItemModule::saveReport ( $userId, $targetType, $targetId, $type );
			
			return $this->outputContent ();
		} );
	}
	
	/**
	 * 热门标签刷新接口
	 *
	 * @param unknown $targetType        	
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postHotTags() {
		return $this->execute ( function () {
			$targetType = $this->getParam ( 'targetType', 'required|NatureNumber' );
			
			$this->outputParamErrorIfExist ();
			
			$tags = TagModule::getHotTags ( $targetType, BaseModule::HOT_TAG_NUMBER );
			
			$this->data = compact ( 'tags' );
			
			$result = $this->showView ( 'common.hot-tags' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 列表页AJAX筛选
	 *
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>|\Illuminate\View\View
	 */
	public function postIndex() {
		return $this->execute ( function () {
			$targetType = $this->getParam ( 'targetType', 'natureNumber' );
			$tagId = $this->getParam ( 'tag_id', 'natureNumber' );
			$page = $this->getParam ( 'page', 'natureNumber' );
			if (! $page) {
				$page = 1;
			}
			
			$status = $this->getParam ( 'status', 'natureNumberAndZero' );
			if (! $status) {
				$status = 0;
			}
			
			if (! $targetType) {
				$targetType = BaseModule::TYPE_PROJECT;
			}
			
			$this->outputParamErrorIfExist ();
			
			$limit = BaseModule::ITEM_NUMBER_INDEX;
			$offset = $this->getOffset ( $page, $limit );
			
			switch ($targetType) {
				case ItemModule::TYPE_PROJECT :
					$lists = ProjectModule::getListsByTagId ( $tagId, $status, $offset, $limit );
					$total = ProjectModule::getTotalByTagIdAndStatus ( $tagId, $status );
					$pageTotal = ceil ( $total / $limit );
					$page = ($page >= $pageTotal) ? 0 : $page;
					break;
				case ItemModule::TYPE_TASK :
					$lists = TaskModule::getListsByTagId ( $tagId, $status, $offset, $limit );
					$total = TaskModule::getTotalByTagIdAndStatus ( $tagId, $status );
					$pageTotal = ceil ( $total / $limit );
					$page = ($page >= $pageTotal) ? 0 : $page;
					break;
				case ItemModule::TYPE_HIRING :
					$lists = HiringModule::getListsByTagId ( $tagId, $status, $offset, $limit );
					$total = HiringModule::getTotalByTagIdAndStatus ( $tagId, $status );
					$pageTotal = ceil ( $total / $limit );
					$page = ($page >= $pageTotal) ? 0 : $page;
					break;
				case ItemModule::TYPE_EXCHANGE :
					$lists = ExchangeModule::getListsByTagIdAndStatus ( $status, $tagId, $offset, $limit );
					$total = ExchangeModule::getTotalByTagIdAndStatus ( $tagId, $status );
					$pageTotal = ceil ( $total / $limit );
					$page = ($page >= $pageTotal) ? 0 : $page;
					break;
				default :
					$lists = EncyclopediaModule::getListsByTagId ( $tagId, $offset, $limit, $targetType );
					$total = EncyclopediaModule::getTotalByTagIdAndStatus ( $tagId, $status );
					$pageTotal = ceil ( $total / $limit );
					$page = ($page >= $pageTotal) ? 0 : $page;
					break;
			}
			
			$isEnd = ($pageTotal - $page) ? false : true;
			
			
			UserModule::fillUsers ( $lists );
			$type = $targetType;
			$this->data = compact ( 'lists', 'page', 'pageTotal', 'tagId', 'status', 'type' );
			$result = $this->showView ( 'common.contest-list' )->__toString ();
			return $this->output ( 'ok', array (
					'content' => $result,
					'page' => $page,
					'pageTotal' => $pageTotal ,
					'isEnd' => $isEnd
			) );
			// return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 预计获得的奖励
	 *
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postExpectAward() {
		return $this->execute ( function () {
			$targetType = $this->getParam ( 'target_type', 'natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'natureNumber' );
			$item = $this->getParam ( 'item', 'natureNumber' );
			$amount = $this->getParam ( 'amount', 'natureNumber' );
			
			$num = GuessModule::expectAward ( $targetType, $targetId, $item, $amount );
			return $this->outputContent ( $num );
		} );
	}
	
	/**
	 * 获取一条更新内容
	 */
	public function postUpdateById() {
		return $this->execute ( function () {
			$updateId = $this->getParam ( 'update_id', 'natureNumber' );
			
			$this->outputParamErrorIfExist ();
			
			$update = ItemModule::getUpdateById ( $updateId );
			
			return $this->outputContent ( $update->toArray () );
		} );
	}
	
	/**
	 * 确定竞猜
	 *
	 * @return \Illuminate\Http\JsonResponse string
	 */
	public function postSureGuess() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$targetType = $this->getParam ( 'target_type', 'natureNumber' );
			$targetId = $this->getParam ( 'target_id', 'natureNumber' );
			$item = $this->getParam ( 'item', 'natureNumberAndZero' );
			$amount = $this->getParam ( 'amount', 'required{error_coin_empty}|natureNumber{error_coin_empty}' );
			
			$user = UserModule::getUserById ( $userId );
			if ($user->coin < $amount) {
				$this->setParamError ( 'amount', 'error_coin_not_enough' );
			}
			
			if ($targetType == BaseModule::TYPE_PROJECT) {
				$data = Project::find ( $targetId );
				$endTime = $data->investment_end_time;
			} elseif ($targetType == BaseModule::TYPE_TASK) {
				$data = Task::find ( $targetId );
				$endTime = $data->end_time;
			} elseif ($targetType == BaseModule::TYPE_HIRING) {
				$data = Hiring::find ( $targetId );
				$endTime = $data->end_time;
			}
			
			$endTime = ! $endTime ? '253402271999' : $endTime; // false的时候就是不限制时间，转换成9999-12-31 23:59:59
			if ($endTime < time ()) {
				$this->setParamError ( 'amount', 'error_end_time_out' );
			}
			
			if ($data->status != ItemModule::STATUS_GOING) {
				$this->setParamError('amount', 'error_end_time_out');
			}
			
			$this->outputParamErrorIfExist ();
			
			GuessModule::playGuess ( $userId, $targetType, $targetId, $item, $amount );
			return $this->outputContent ();
		} );
	}
	
	/**
	 * 判断专家是否可以点评：每个专家只能点击某个对象一次
	 * 投资项目比赛投资者不能点评
	 *
	 * @param int $targetType        	
	 * @param int $id        	
	 * @param int $userId        	
	 * @param
	 *        	bool|object
	 * @param int $subId        	
	 * @return bool
	 */
	public function canExpertComment($userId, $targetType, $id, $model = false, $subId = 0) {
		// 检查是否登录
		if ($userId == 0) {
			return false;
		}
		if (! $model) {
			switch ($targetType) {
				case BaseModule::TYPE_PROJECT :
					$model = ProjectModule::getProjectById ( $id );
					break;
				case BaseModule::TYPE_TASK :
					$model = TaskModule::getTaskById ( $id );
					break;
				case BaseModule::TYPE_HIRING :
					$model = HiringModule::getHiringById ( $id );
					break;
				case BaseModule::TYPE_EXCHANGE :
					$model = ExchangeModule::getTradeById ( $id );
					break;
				case BaseModule::TYPE_ENCYCLOPEDIA :
					$model = EncyclopediaModule::getEncyclopediaById ( $id );
					break;
				case BaseModule::TYPE_TASK_WORK :
					$model = TaskModule::getTaskWorkById ( $subId );
					break;
				case BaseModule::TYPE_HIRING_WORK :
					$model = HiringModule::getPaticipatorById ( $subId );
					break;
				default :
					;
			}
		}
		if ($subId) {
			if ($targetType == BaseModule::TYPE_TASK) {
				$model = TaskModule::getTaskWorkById ( $subId );
			} elseif ($targetType == BaseModule::TYPE_HIRING) {
				$model = HiringModule::getPaticipatorById ( $subId );
			}
		}
		
		// 自己不能点评
		if ($model && $model->user_id == $userId) {
			return false;
		}
		// 专家身份才能点评
		$user = UserModule::getUserById ( $userId );
		if ($user && $user->group_id != UserModule::GROUP_EXPERT) {
			return false;
		}
		
		// 每个专家只能点击某个对象一次
		$hasComment = ExpertCommentModule::hasCommented ( $targetType, $userId, $id, $subId );
		if ($hasComment) {
			return false;
		}
		
		// 投资项目比赛投资者不能点评
		if ($targetType == BaseModule::TYPE_PROJECT) {
			$isPlayer = ProjectModule::isPlayer ( $id, $userId );
			if ($isPlayer) {
				return false;
			}
		}
		
		return true;
	}
	
	
}