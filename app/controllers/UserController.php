<?php

	namespace App\Controllers;

	use App\Common\Utils;
	use App\Modules\UserModule;
	use App\Modules\ItemModule;
	use App\Modules\DynamicModule;
	use App\Modules\ExchangeModule;
	use App\Modules\ContestModule;
	use App\Common\Page;
	use App\Modules\CircleModule;
	use App\Modules\BaseModule;
	use App\Modules\ProjectModule;
	use App\Common\Sms;
	use App\Modules\Utils\MailModule;

	/**
	 * 用户控制器
	 *
	 * 处理与用户账号相关事务，如登录、注册、找回密码等。
	 */
	class UserController extends BaseController
	{
		const VERIFY_CODE = 'verify_code';

		/**
		 * 显示注册页
		 *
		 * @return \Illuminate\View\View
		 */
		public function getRegister()
		{
			if ($this->isLogin()) {
				return \Redirect::to(\URL::to('/') . '/user/home');
			}
			return $this->showView('user.register');
		}

		/**
		 * 注册验证邮箱是否可用
		 */
		public function postVerifyEmail()
		{
			$email  = $this->getParam('param');
			$status = $this->checkEmailNotExist($email);
			if ($status) {
				$data ['status'] = 'y';
			} else {
				$data ['status'] = 'n';
				$data ['info']   = '该邮箱已注册';
			}

			echo json_encode($data);
		}

		/**
		 * 注册是验证昵称是否可用
		 */
		public function postVerifyNick()
		{
			$name = $this->getParam('param');

			$data ['status'] = 'n';

			// $name = $this->getParam ( 'name', 'required{error_name_empty}|minLength[4]{error_name_short}|maxLength[16]{error_name_long}|notNumber{error_name_is_number}|character{error_name_illegal}|nameNotExist{error_name_exist}' );
			if (!$this->checkRequired($name)) {
				$data ['info'] = '请填写昵称';
			} elseif (!$this->checkMinLength($name, 4) || !$this->checkMaxLength($name, 16)) {
				$data ['info'] = '请输入4-16字符的昵称';
			} elseif (!$this->checkNotNumber($name)) {
				$data ['info'] = '昵称不能为纯数字';
			} elseif (!$this->checkCharacter($name)) {
				$data ['info'] = '昵称不能包含非法字符';
			} elseif (!$this->checkNameNotExist($name)) {
				$data ['info'] = '该昵称已注册';
			} else {
				$data ['status'] = 'y';
			}

			echo json_encode($data);
		}

		/**
		 * 注册 验证码
		 */
		public function postVerifyCode()
		{
			$verify_code = $this->getParam('param');
			$status      = $this->checkVerifyCodeCorrect($verify_code);
			if ($status) {
				$data ['status'] = 'y';
			} else {
				$data ['status'] = 'n';
				$data ['info']   = '验证码错误';
			}
			echo json_encode($data);
		}

		/**
		 * 提交注册请求
		 *
		 * @return \Illuminate\View\View
		 */
		public function postRegister()
		{
			return $this->execute(function () {
				$email = $this->getParam('email', 'required{error_email_empty}|email{error_email_error}|emailNotExist{error_email_exist}');
				$name  = $this->getParam('name', 'required{error_name_empty}|minLength[4]{error_name_short}|maxLength[16]{error_name_long}|notNumber{error_name_is_number}|character{error_name_illegal}|nameNotExist{error_name_exist}');
				list ($password, $passwordAgain) = $this->getParamPasswordAndPasswordAgain();
				// $verifyCode = $this->getParamVerifyCode ();
				$isAgreeProtocol = $this->getParam('is_agree_protocol', 'required{error_not_accept_agreement}');

				$this->outputParamErrorIfExist();

				$userId = UserModule::register($name, $email, $password);
				\Session::set('userId', $userId);
				$this->data = compact('userId');

				return $this->outputContent();
			});
		}

		/**
		 * 显示注册协议
		 *
		 * @return \Illuminate\View\View
		 */
		public function getRegisterAgreement()
		{
			return $this->showView('user.register-agreement');
		}

		/**
		 * 显示注册成功页
		 *
		 * @return \Illuminate\View\View
		 */
		public function getRegisterSuccess()
		{
			$userId                       = \Session::get('userId');
			$user                         = UserModule::getUserById($userId);
			$this->data ['RegisterCCNo']  = $user->cc_no;
			$this->data ['RegisterEmail'] = $user->email;
			$pos                          = strpos($user->email, '@');
			$this->data ['emailUrl']      = '//mail.' . substr($user->email, $pos + 1);
			UserModule::activateAccount($userId);
			$result = $this->showView('user.register-success');
			\Session::get('userId', '');
			return $result;
		}

		/**
		 * 点击激活成功链接，完成注册
		 *
		 * @param int    $userId
		 * @param string $activationCode
		 *
		 * @return \Illuminate\View\View
		 */
		public function getActivateAccountSuccess($userId = 0, $activationCode = '')
		{
			if (!Utils::isNatureNumber($userId) || !$activationCode) {
				\App::abort(404);
			}

			$user = UserModule::getUserById($userId);
			if(!$user){
			    \App::abort(404);
			}elseif ($user->group_id == 1) {
			    return \Redirect::to(\URL::to('/') . '/user/home');
			}elseif ($user->activation_code != $activationCode) {
				\App::abort(404);
			}
			$result = UserModule::activateAccountSuccess($userId);
			if ($result) {
				\Session::set('userId', $userId);
				return \Redirect::to(\URL::to('/') . '/user/home');
			}
		}

		/**
		 * 显示登录页
		 *
		 * @return \Illuminate\View\View
		 */
		public function getLogin()
		{
			if ($this->isLogin()) {
				return \Redirect::to(\URL::to('/') . '/user/home');
			}
			
			return $this->showView('user.login');
		}

		/**
		 * 提交登录请求
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function postLogin()
		{
			return $this->execute(function () {
				$loginName = $this->getParamLoginName();
				$password  = $this->getParam('password', 'required{error_password_empty}');

				$status = UserModule::login($loginName, $password);
				if (!$status) {
					$error = 'error_password_wrong';
					$this->setParamError('password', $error);
					$this->outputParamErrorIfExist();
				}

				return $this->outputContent();
			});
		}

		/**
		 * 登出
		 *
		 * @return \Illuminate\Http\RedirectResponse
		 */
		public function getLogout()
		{

			\Session::clear();

			return \Redirect::to('/');
		}

		/**
		 * 找回密码页面
		 * @return \Illuminate\View\View
		 */
		public function getForgetPassword()
		{
			\Session::set('userId', '');
			return $this->showView('user.forget-password');
		}

		/**
		 * 找回密码-第一步验证账号是否存在
		 */
		public function postFindEmail()
		{
			$email  = $this->getParam('param');
			$status = $this->checkEmailNotExist($email);
			if (!$status) {
				$data ['status'] = 'y';
			} else {
				$data ['status'] = 'n';
				$data ['info']   = '该邮箱尚未注册';
			}

			echo json_encode($data);
		}

		/**
		 * 找回密码验证用户邮箱,显示发送邮件页
		 *
		 * @return \Illuminate\View\View
		 */
		public function postFindCcoEmail()
		{
			return $this->execute(function () {
				$email = $this->getParam('email', 'required{error_email_empty}|email{error_email_error}'); // |emailExist{error_email_not_exist}
				// $verifyCode = $this->getParamVerifyCode ();
				$this->outputParamErrorIfExist();

				$user           = UserModule::getUserByEmail($email);
				$data ['cc_no'] = $user->cc_no;
				$data ['email'] = Utils::getSecretEmail($email);
				$data ['mobile'] = !empty($user->mobile_phone_no)?substr($user->mobile_phone_no,0,3).'******'.substr($user->mobile_phone_no,9,2):'';

				\Session::set('findPwdUser', $user);
				return $this->outputContent($data);
			});
		}

		/**
		 * 发送找回手机手机验证码
		 * @return \Illuminate\Http\JsonResponse|string
		 */
		public function postFindpwdSendMobileCode(){

			return $this->execute(function () {
				$user = \Session::get('findPwdUser');
				$verifyCode = mt_rand(100000, 999999);
				\Session::set('verify_code_mobile', $verifyCode);
				Sms::sendSms($user->mobile_phone_no,'您正在申请找回密码，验证码:'.$verifyCode.',请尽快完成修改。如非本人操作，请注意账号安全。创意世界。');
				return $this->outputContent();
			});
		}

		/**
		 * 提交手机验证码 找回密码
		 * @return \Illuminate\Http\JsonResponse|string
		 */
		public function postFindpwdSubmitMobileCode(){

			return $this->execute(function () {
				$verifyCode = $this->getParam('verify_code', 'required{error_verify_code_empty}');
				$verify_code = \Session::get('verify_code_mobile');

				if($verifyCode != $verify_code){
					$this->setParamError('verify_code_mobile','error_verify_code_error');
				}

				$this->outputParamErrorIfExist();
				return $this->outputContent();
			});
		}


		/**
		 * 发送邮箱验证码 找回密码
		 * @return \Illuminate\Http\JsonResponse|string
		 */
		public function postFindpwdSendEmailCode(){

			return $this->execute(function () {
				$user = \Session::get('findPwdUser');
				$verifyCode = mt_rand(100000, 999999);
				\Session::set('verify_code_email', $verifyCode);
				MailModule::send($user->email, '找回密码','您正在申请找回密码，验证码:'.$verifyCode.',请尽快完成修改。如非本人操作，请注意账号安全。创意世界。');
				return $this->outputContent();
			});
		}

		/**
		 *
		 * @return \Illuminate\Http\JsonResponse|string
		 */
		public function postFindpwdSubmitEmailCode(){

			return $this->execute(function () {
				$verifyCode = $this->getParam('verify_code', 'required{error_verify_code_empty}');
				$verify_code = \Session::get('verify_code_email');

				if($verifyCode != $verify_code){
					$this->setParamError('verify_code_email','error_verify_code_error');
				}

				$this->outputParamErrorIfExist();
				return $this->outputContent();
			});
		}




		public function postResetPassword()
		{
			return $this->execute(function () {

				$user = \Session::get('findPwdUser');
				$userId = $user->id;
				list ($password, $passwordAgain) = $this->getParamPasswordAndPasswordAgain();
				$this->outputParamErrorIfExist();
				$result = UserModule::resetPassword($userId, $password);

				return $this->outputContent($result);
			});
		}

		private function getParamLoginName($param = 'login_name')
		{
			$loginName = $this->getParam($param, 'required{error_login_name_empty}');
			$user = UserModule::getUserByLoginName($loginName);
			if(empty($user)){
				if (Utils::isNatureNumber($loginName)) { // CC No
					$error = 'error_cc_no_not_exist';
				} else if (Utils::isEmail($loginName)) { // Email
					$error = 'error_email_not_exist';
				} else { // Name
					$error = 'error_name_not_exist';
				}
				$this->setParamError($param, $error);
				$this->outputParamErrorIfExist();
			}elseif(!empty($user) && $user->group_id == 0){
				$error = 'error_user_no_active';
				$this->setParamError($param, $error);
				$this->outputParamErrorIfExist();
			}
			return $loginName;
		}

		private function getParamVerifyCode($param = 'verify_code')
		{
			return $this->getParam($param, 'required{error_verify_code_empty}|verifyCodeCorrect{error_verify_code_error}');
		}

		private function getParamPasswordAndPasswordAgain($paramPassword = 'password', $paramPasswordAgain = 'password_again')
		{
			$password      = $this->getParam($paramPassword, 'required{error_password_empty}|minLength[6]{error_password_short}|maxLength[16]{error_password_long}');
			$passwordAgain = $this->getParam($paramPasswordAgain, 'required{error_password_again_empty}');
			if (!$this->hasParamError($paramPassword)) {
				if ($passwordAgain != $password) {
					$this->setParamError($paramPasswordAgain, 'error_password_again_error');
				}
			}

			return [
				$password,
				$passwordAgain
			];
		}

		/**
		 * 检查用户名不存在
		 *
		 * @param string $name
		 *
		 * @return bool
		 */
		protected function checkNameNotExist($name)
		{
			return is_null(UserModule::getUserByName($name));
		}

		/**
		 * 检查登录名存在
		 *
		 * @param string $loginName
		 *
		 * @return bool
		 */
		protected function checkLoginNameExist($loginName)
		{
			return !is_null(UserModule::getUserByLoginName($loginName));
		}

		protected static function getAuthImage($text) {
			$im_x = 160;
			$im_y = 40;
			$im = imagecreatetruecolor($im_x,$im_y);
			$text_c = imagecolorallocate($im, mt_rand(0,100),mt_rand(0,100),mt_rand(0,100));
			$tmpC0=mt_rand(100,255);
			$tmpC1=mt_rand(100,255);
			$tmpC2=mt_rand(100,255);
			$buttum_c = imagecolorallocate($im,$tmpC0,$tmpC1,$tmpC2);
			imagefill($im, 16, 13, $buttum_c);

			$font = 'font/t1.ttf';

			for ($i=0;$i<strlen($text);$i++)
			{
				$tmp =substr($text,$i,1);
				$array = array(-1,1);
				$p = array_rand($array);
				$an = $array[$p]*mt_rand(1,10);//角度
				$size = 28;
				imagettftext($im, $size, $an, 15+$i*$size, 35, $text_c, $font, $tmp);
			}


			$distortion_im = imagecreatetruecolor ($im_x, $im_y);

			imagefill($distortion_im, 16, 13, $buttum_c);
			for ( $i=0; $i<$im_x; $i++) {
				for ( $j=0; $j<$im_y; $j++) {
					$rgb = imagecolorat($im, $i , $j);
					if( (int)($i+20+sin($j/$im_y*2*M_PI)*10) <= imagesx($distortion_im)&& (int)($i+20+sin($j/$im_y*2*M_PI)*10) >=0 ) {
						imagesetpixel ($distortion_im, (int)($i+10+sin($j/$im_y*2*M_PI-M_PI*0.1)*4) , $j , $rgb);
					}
				}
			}
			//加入干扰象素;
			$count = 160;//干扰像素的数量
			for($i=0; $i<$count; $i++){
				$randcolor = ImageColorallocate($distortion_im,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
				imagesetpixel($distortion_im, mt_rand()%$im_x , mt_rand()%$im_y , $randcolor);
			}

			$rand = mt_rand(5,30);
			$rand1 = mt_rand(15,25);
			$rand2 = mt_rand(5,10);
			for ($yy=$rand; $yy<=+$rand+2; $yy++){
				for ($px=-80;$px<=80;$px=$px+0.1)
				{
					$x=$px/$rand1;
					if ($x!=0)
					{
						$y=sin($x);
					}
					$py=$y*$rand2;

					imagesetpixel($distortion_im, $px+80, $py+$yy, $text_c);
				}
			}

/*			//设置文件头;
			Header("Content-type: image/JPEG");*/

			//以PNG格式将图像输出到浏览器或文件;
			imagepng($distortion_im);

			//销毁一图像,释放与image关联的内存;
			imagedestroy($distortion_im);
			imagedestroy($im);

		}

		protected  static function make_rand($length="4"){//验证码文字生成函数
			$str="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$result="";
			for($i=0;$i<$length;$i++){
				$num[$i]=rand(0,25);
				$result.=$str[$num[$i]];
			}
			return $result;
		}
		/**
		 * 生成图形验证码
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function getVerifyCode()
		{

			//输出调用
			$code = self::make_rand(4);
			\Session::remove(self::VERIFY_CODE);
			\Session::set(self::VERIFY_CODE, strtolower($code));
			self::getAuthImage($code);

			/*$num    = 4; // 验证码个数
			$width  = 80; // 验证码宽度
			$height = 30; // 验证码高度
			$code   = ' ';
			for ($i = 0; $i < $num; $i++) {
				switch (rand(0, 2)) {
					case 0 :
						$code [$i] = chr(rand(48, 57));
						break; // 数字
					case 1 :
						$code [$i] = chr(rand(65, 90));
						break; // 大写字母
					case 2 :
						$code [$i] = chr(rand(97, 122));
						break; // 小写字母
				}
			}

			\Session::remove(self::VERIFY_CODE);
			\Session::set(self::VERIFY_CODE, strtolower($code));

			$image = imagecreate($width, $height);
			imagecolorallocate($image, 255, 255, 255);
			for ($i = 0; $i < $width; $i++) {
				$dis_color = imagecolorallocate($image, rand(0, 2555), rand(0, 255), rand(0, 255));
				imagesetpixel($image, rand(1, $width), rand(1, $height), $dis_color);
			}
			for ($i = 0; $i < $num; $i++) {
				$char_color = imagecolorallocate($image, rand(0, 2555), rand(0, 255), rand(0, 255));
				imagechar($image, 60, ($width / $num) * $i + 5, rand(4, 9), $code [$i], $char_color);
			}
			imagepng($image); // 输出图像到浏览器
			imagedestroy($image); // 释放资源*/

			$response = \Response::make('');
			$response->header('Content-Type', 'image/png');
			return $response;
		}

		/**
		 * 检查验证码正确
		 *
		 * @param string $verifyCode
		 *
		 * @return bool
		 */
		protected function checkVerifyCodeCorrect($verifyCode)
		{
			return \Session::get(self::VERIFY_CODE) == strtolower($verifyCode);
		}

		/**
		 * 个人主页-首页：动态：全部，原创，转摘
		 */
		public function getHome($userId = 0, $pageType = 0, $page = 0)
		{
			$isMe = 0;

			if (!$userId) {
				if (!$this->isLogin()) {
					return \Redirect::to(\URL::to('/') . '/user/login');
				}
				$userId = $this->getLoginUserId();
				$isMe   = 1;
			} elseif ($userId == $this->getLoginUserId()) {
				$isMe = 1;
			}

			$owner = UserModule::getUserById($userId);
			if (empty ($owner)) {
				return \Redirect::to(\URL::to('/') . '/user/home');
			}

			$isFollow = false;
			if (!$isMe) {
				$isFollow = CircleModule::isCircleSomebody($userId, $this->getLoginUserId());
			}

			$limit  = BaseModule::DYNAMICS_PER_PAGE;
			$offset = ($page - 1) * $limit;

			$datas    = DynamicModule::getDynamcicsByUserIds($userId, $offset, $limit, $pageType);
			$totalNum = DynamicModule::getDynamicsByUserIdsNum($userId, $pageType);
			DynamicModule::fillForwards($datas);
			DynamicModule::fillActionContent($datas);
			DynamicModule::fillLikeInfo($datas, $this->getLoginUserId());
			UserModule::fillUsers($datas);

			$contests   = ContestModule::getHotContest();
			$exchanges  = ExchangeModule::getHotExchange();
			$carepeople = UserModule::getCarePeople($userId);

			$cur       = $page;
			$totalPage = $total = ceil($totalNum / $limit);
			$url       = \URL::to('/') . '/user/home/' . $userId . '/' . $pageType;
			$pageHtml  = Page::genePageHtml($url, $cur, $total);

			$this->data = compact('isMe', 'owner', 'datas', 'pageType', 'totalPage', 'pageHtml', 'contests', 'exchanges', 'carepeople', 'isFollow');
			return $this->showView('user.home');
		}

		/**
		 * 个人主页--创意比赛:发布的，参与的，(喜欢的)
		 */
		public function getContests($userId = 0, $pageType = 0, $page = 0)
		{
			$isMe = 0;
			if (!$userId) {
				if (!$this->isLogin()) {
					return \Redirect::to(\URL::to('/') . '/user/login');
				}
				$userId = $this->getLoginUserId();
				$isMe   = 1;
			} elseif ($userId == $this->getLoginUserId()) {
				$isMe = 1;
			}

			$owner = UserModule::getUserById($userId);
			if (empty ($owner)) {
				return \Redirect::to(\URL::to('/') . '/user/home');
			}

			$isFollow = false;
			if (!$isMe) {
				$isFollow = CircleModule::isCircleSomebody($userId, $this->getLoginUserId());
			}

			$limit  = BaseModule::ITEM_NUMBER_INDEX;
			$offset = ($page - 1) * $limit;

			$lists = ItemModule::getContestsByActionType($userId, $offset, $limit, $pageType, $isMe);

			UserModule::fillUsers($lists);


			$contests   = ContestModule::getHotContest();
			$exchanges  = ExchangeModule::getHotExchange();
			$carepeople = UserModule::getCarePeople($userId);

			$totalNum  = ItemModule::getContestsByActionTypeCount($userId, $pageType);
			$pageTotal = ceil($totalNum / $limit);
			$url       = \URL::to('/') . '/user/contests/' . $userId . '/' . $pageType;
			$cur       = $page;
			$pageHtml  = Page::genePageHtml($url, $cur, $pageTotal);

			$this->data = compact('isMe', 'owner', 'lists', 'pageTotal', 'pageType', 'pageHtml', 'page', 'contests', 'exchanges', 'carepeople', 'isFollow');
			return $this->showView('user.contests');
		}

		/**
		 * 个人主页--创意交易
		 */
		public function getExchanges($userId = 0, $pageType = 0, $page = 0)
		{
			$isMe = 0;

			if (!$userId) {
				if (!$this->isLogin()) {
					return \Redirect::to(\URL::to('/') . '/user/login');
				}
				$userId = $this->getLoginUserId();
				$isMe   = 1;
			} elseif ($userId == $this->getLoginUserId()) {
				$isMe = 1;
			}

			$owner = UserModule::getUserById($userId);
			if (empty ($owner)) {
				return \Redirect::to(\URL::to('/') . '/user/home');
			}

			$isFollow = false;
			if (!$isMe) {
				$isFollow = CircleModule::isCircleSomebody($userId, $this->getLoginUserId());
			}

			$limit  = BaseModule::ITEM_NUMBER_INDEX;
			$offset = ($page - 1) * $limit;

			$lists = ExchangeModule::getExchangeTradesByUserId($userId, $offset, $limit);
			UserModule::fillUsers($lists);

			$contests   = ContestModule::getHotContest();
			$exchanges  = ExchangeModule::getHotExchange();
			$carepeople = UserModule::getCarePeople($userId);

			$totalNum  = ExchangeModule::getExchangeTradesByUserIdCount($userId);
			$pageTotal = ceil($totalNum / $limit);
			$url       = \URL::to('/') . '/user/exchanges/' . $userId . '/' . $pageType;
			$cur       = $page;
			$pageHtml  = Page::genePageHtml($url, $cur, $pageTotal);

			$this->data = compact('isMe', 'owner', 'lists', 'pageTotal', 'pageType', 'pageHtml', 'page', 'contests', 'exchanges', 'carepeople', 'isFollow');
			return $this->showView('user.exchanges');
		}

		/**
		 * 个人主页--创意作品：全部，发布的，参与的，(喜欢的)
		 */
		public function getWorks($userId = 0, $pageType = 0, $page = 0)
		{
			$isMe = 0;

			if (!$userId) {
				if (!$this->isLogin()) {
					return \Redirect::to(\URL::to('/') . '/user/login');
				}
				$userId = $this->getLoginUserId();
				$isMe   = 1;
			} elseif ($userId == $this->getLoginUserId()) {
				$isMe = 1;
			}

			$owner = UserModule::getUserById($userId);
			if (empty ($owner)) {
				return \Redirect::to(\URL::to('/') . '/user/home');
			}

			$isFollow = false;
			if (!$isMe) {
				$isFollow = CircleModule::isCircleSomebody($userId, $this->getLoginUserId());
			}

			$limit  = BaseModule::ITEM_NUMBER_INDEX;
			$offset = ($page - 1) * $limit;

			$lists = ItemModule::getWorksByActionType($userId, $offset, $limit, $pageType, $isMe);
			UserModule::fillUsers($lists);

			$contests   = ContestModule::getHotContest();
			$exchanges  = ExchangeModule::getHotExchange();
			$carepeople = UserModule::getCarePeople($userId);

			$totalNum  = ItemModule::getWorksByActionTypeCount($userId, $pageType);
			$pageTotal = ceil($totalNum / $limit);
			$url       = \URL::to('/') . '/user/works/' . $userId . '/' . $pageType;
			$cur       = $page;
			$pageHtml  = Page::genePageHtml($url, $cur, $pageTotal);

			$type       = $pageType;
			$this->data = compact('isMe', 'owner', 'lists', 'pageTotal', 'pageHtml', 'type', 'pageType', 'page', 'contests', 'exchanges', 'carepeople', 'isFollow');
			return $this->showView('user.works');
		}

		// 用户卡片
		public function postUserCard()
		{
			return $this->execute(function () {
				$id = $this->getParam('id', 'required|natureNumber');
				$this->outputParamErrorIfExist();

				$userCard            = UserModule::getUserDetailByUserId($id);
				$userCard->is_follow = 0;
				if ($this->isLogin()) {
					$userCard->is_follow = CircleModule::isCircleSomebody($id, $this->getLoginUserId());
				}
				$showCardCss      = 'Js_show_card_box';
				$postUserCardHtml = true;
				$this->data       = compact('userCard', 'postUserCardHtml');
				$result           = $this->showView('user.user-card')->__toString();
				return $this->outputContent($result);
			});
		}


	}