<?php

namespace App\Controllers;

use App\Modules\BaseModule;
use App\Modules\UserModule;
use App\Modules\ItemModule;
use App\Modules\AreaModule;
use App\Modules\EncyclopediaModule;
use App\Modules\ContestModule;
use App\Modules\ExchangeModule;
use App\Modules\CircleModule;
use App\Common\Page;

/**
 * 搜索控制器
 * 创创召中部的搜索
 */
class SearchController extends BaseController {
	
	/**
	 * 创创召:搜索结果列表
	 * 
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postList() {
		return $this->execute ( function () {
			$keyword = $this->getParam ( 'keyword', 'required' );
			
			$this->outputParamErrorIfExist ();
			
			$users = UserModule::searchUsersByKeyword ( $keyword );
			$items = ItemModule::searchItemsByKeyword ( $keyword );
			$encyclopedias = EncyclopediaModule::searchEncyclopediasByKeyword ( $keyword );
			$areas = AreaModule::searchAreasByKeyword ( $keyword );
			
			$this->data = compact ( 'keyword', 'users', 'items', 'areas', 'encyclopedias' );
			
			$result = $this->showView ( 'search.list' )->__toString ();
			
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 搜索结果页面
	 * 
	 * @param string $keyword        	
	 * @param number $type        	
	 * @param number $page        	
	 * @return \Illuminate\View\View
	 */
	public function getIndex($keyword = '', $type = 0, $page = 1) {
		$totalNum = 1;
		
		$limit = BaseModule::ITEM_NUMBER_INDEX;
		$offset = ($page - 1) * $limit;
		
		if (in_array ( $type, array (
				BaseModule::TYPE_CONTEST,
				BaseModule::TYPE_PROJECT,
				BaseModule::TYPE_TASK,
				BaseModule::TYPE_HIRING 
		) )) {
			$lists = ContestModule::searchContestsByKeyword ( $keyword, $type, $offset, $limit );
			UserModule::fillUsers ( $lists );
			$totalNum = ContestModule::searchContestsByKeywordCount($keyword,$type);
		} else if ($type == BaseModule::TYPE_EXCHANGE) {
			$lists = ExchangeModule::searchExchangesByKeyword ( $keyword, $offset, $limit );
			UserModule::fillUsers ( $lists );
			$totalNum = ExchangeModule::searchExchangesByKeywordCount($keyword);
		} else if ($type == BaseModule::TYPE_ENCYCLOPEDIA) {
			$lists = EncyclopediaModule::searchEncyclopediasByKeyword ( $keyword, $offset, $limit );
			UserModule::fillUsers ( $lists );
			$totalNum = EncyclopediaModule::searchEncyclopediasByKeywordCount($keyword);
		} else if (in_array ( $type, array (
				BaseModule::TYPE_USER,
				BaseModule::GROUP_COMMON_USER,
				BaseModule::GROUP_EXPERT 
		) )) {
			$limit = BaseModule::USER_NUMBER_SIZE;
			$offset = ($page - 1) * $limit;
			$lists = UserModule::searchUsersByKeyword ( $keyword, $type, $offset, $limit );
			CircleModule::fillIsFollows ( $lists, $this->getLoginUserId (), '' );
			$totalNum = UserModule::searchUsersByKeywordCount($keyword);
		} else if (in_array ( $type, array (
				BaseModule::TYPE_AREA,
				BaseModule::TYPE_REGION,
				BaseModule::TYPE_SCHOOL,
				BaseModule::TYPE_ORGANIZATION 
		) )) {
			$limit = BaseModule::USER_NUMBER_SIZE;
			$offset = ($page - 1) * $limit;
			$lists = AreaModule::searchAreasByKeyword ( $keyword, $type, $offset, $limit );
			$totalNum = AreaModule::searchAreasByKeywordCount($keyword,$type);
		} else {
			$lists = EncyclopediaModule::searchEncyclopediasByKeyword ( $keyword, $offset, $limit );
			UserModule::fillUsers ( $lists );
			$totalNum = EncyclopediaModule::searchEncyclopediasByKeywordCount($keyword);
		}
		
		$pageTotal = ceil ( $totalNum / $limit);
		$url = \URL::to ( '/' ) . '/search/index/' . $keyword . '/' . $type ;
		$pageHtml = Page::genePageHtml ( $url, $page, $pageTotal );
		
		$this->data = compact ( 'keyword', 'type', 'pageTotal', 'pageHtml', 'lists' );
		
		return $this->showView ( 'search.index' );
	}
	
	/**
	 * 站外云搜索
	 * 
	 * @param string $keyWord        	
	 * @param number $page        	
	 * @return \Illuminate\View\View
	 */
	public function getOuter($keyWord = '', $page = 1) {
		$page = intval ( $page );
		$page = ($page < 1) ? 1 : $page;
		$number = 10;
		$start = ($page - 1) * $number;
		$start = ($start < 1) ? 1 : $start;
		
		$query = urlencode ( "'{$keyWord}'" );
		
		$acctKey = 'Kn/wYYcM/smnqJVBnXkNd4W51ovsJMCOFLuOlwX1/As';
		
		$auth = base64_encode ( "$acctKey:$acctKey" );
		
		$begin = time ();
		$timeOut = 10;
		$header = array (
				"Authorization: Basic $auth" 
		);
		$url = "https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?Sources=%27web%27&Query=$query&\$top=$number&\$skip=$start&\$format=JSON";
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, $timeOut );
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $header );
		curl_setopt ( $ch, CURLINFO_HEADER_OUT, TRUE );
		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		$result = json_decode ( $result, true );
		$end = time ();
		
		$cost = $end - $begin;
		$searchResult = array ();
		$this->data ['result_number'] = 0;
		$this->data ['result'] = array ();
		$this->data ['keyword'] = $keyWord;
		$this->data ['current_page'] = 1;
		$this->data ['isTimeOut'] = false;
		
		if ($cost > 10) {
			$this->data ['isTimeOut'] = true;
			return $this->showView ( 'search.outer' );
		}
		
		if (is_array ( $result ) && isset ( $result ['d'] ['results'] ['0'] ['Web'] )) {
			foreach ( $result ['d'] ['results'] ['0'] ['Web'] as $key => $value ) {
				$searchResult [$key] ['title'] = $value ['Title'];
				$searchResult [$key] ['link'] = $value ['Url'];
				$searchResult [$key] ['snippet'] = $value ['Description'];
			}
			
			$this->data ['result_number'] = $result ['d'] ['results'] ['0'] ['WebTotal'];
			$this->data ['result'] = $searchResult;
			$this->data ['keyword'] = $keyWord;
			$this->data ['current_page'] = $page;
		}
		$this->data ['use_time'] = $cost;
		
		return $this->showView ( 'search.outer' );
	}
}