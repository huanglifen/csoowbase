<?php

namespace App\Controllers;

use App\Modules\AreaModule;
use App\Modules\BaseModule;
use App\Modules\RegionModule;
use App\Modules\SchoolModule;
use App\Modules\OrganizationModule;
use App\Models\Organization;
use App\Modules\LikeModule;
use App\Modules\EncyclopediaModule;
use App\Modules\ContestModule;
use App\Modules\ExchangeModule;
use App\Modules\UserModule;
use App\Modules\CommentModule;
use App\Modules\CircleModule;
use App\Common\Page;
use App\Modules\NewsModule;

/**
 * 地区控制器
 *
 * 处理创意世界地区相关功能:地区、学校、政企等
 */
class AreaController extends ItemController {
	
	const AREA_NEWS_NUMBER = 4; //地区页面新闻数量
	
	// 获取地区信息
	public function getItem($id) {
		return RegionModule::getRegionInfoById ( $id );
	}
	
	// 带地图的地区
	public function getIndex($id = 0) {

		$data = array ();
		$data ['name'] = '世界';
		
		if (! $id || strlen ( $id ) == 1 || strlen ( $id ) == 2) {
			// 首页(世界)、中国、省级
			if (! $id) {
				$data ['name'] = '世界';
			} elseif ($id == 1) {
				$data ['name'] = '中国';
			} else {
				$data = RegionModule::getRegionInfoById ( $id );
				if ($id && empty ( $data )) {
					return \Redirect::to ( \URL::to ( '/' ) . '/area/index' );
				}
			}
			
			$regions = RegionModule::getRegionsById ( $id );
			$schools = SchoolModule::getSchoolsById ( $id );
			$goverments = OrganizationModule::getOrganizationsById ( $id );
			$coporates = OrganizationModule::getOrganizationsById ( $id, Organization::TYPE_CORPORATE );
			
			$this->data = compact ( 'id', 'data', 'regions', 'schools', 'goverments', 'coporates' );
			return $this->showView ( 'area.index' );
		} else {
			// 市级 、县级
			return \Redirect::to ( \URL::to ( '/' ) . '/area/region/' . $id );
		}
	}
	
	// 地区主页
	public function getRegion($id = 0, $type = 5, $page = 1) {
		$data = RegionModule::getRegionInfoById ( $id );
		if (empty ( $data )) {
			return \Redirect::to ( \URL::to ( '/' ) . '/area/index' );
		}
		
		$userId = $this->getLoginUserId ();
		$targetId = $id;
		$targetType = BaseModule::TYPE_REGION;
		$isLiked = LikeModule::isLiked ( $userId, $targetType, $targetId );
		$isJoined = RegionModule::isJoined ( $userId, $id );
		
		$totalNum = 0;
		$limit = BaseModule::ITEM_NUMBER_INDEX;
		$offset = ($page - 1) * $limit;
		
		switch ($type) {
			case BaseModule::TYPE_ENCYCLOPEDIA :
				$lists = EncyclopediaModule::getEncyclopediasByRegionId ( $id, $offset, $limit );
				$totalNum = EncyclopediaModule::getEncyclopediasByIdCount ( $id );
				UserModule::fillUsers ( $lists );
				break;
			case BaseModule::TYPE_CONTEST :
				$lists = ContestModule::getContestsByRegionId ( $id, $offset, $limit );
				$totalNum = ContestModule::getContestsByRegionIdCount ( $id );
				UserModule::fillUsers ( $lists );
				break;
			case BaseModule::TYPE_EXCHANGE :
				$lists = ExchangeModule::getExchangeTradesByRegionId ( $id, $offset, $limit );
				$totalNum = ExchangeModule::getExchangeTradesByRegionIdCount ( $id );
				UserModule::fillUsers ( $lists );
				break;
			case BaseModule::GROUP_EXPERT :
				$limit = BaseModule::USER_NUMBER_SIZE;
				$offset = ($page - 1) * $limit;
				$lists = UserModule::getExpertsByRegionId ( $id, $offset, $limit );
				$totalNum = UserModule::getExpertsByRegionIdCount ( $id );
				CircleModule::fillIsFollows ( $lists, $userId, '' );
				break;
			case BaseModule::GROUP_COMMON_USER :
				$limit = BaseModule::USER_NUMBER_SIZE;
				$offset = ($page - 1) * $limit;
				$lists = UserModule::getCommonUsersByRegionId ( $id, $offset, $limit );
				$totalNum = UserModule::getCommonUsersByRegionIdCount ( $id );
				CircleModule::fillIsFollows ( $lists, $userId, '' );
				break;
			case BaseModule::TYPE_REGION :
				$limit = BaseModule::USER_NUMBER_SIZE;
				$offset = ($page - 1) * $limit;
				$lists = RegionModule::getRegionsByRegionId ( $id, $offset, $limit );
				$totalNum = RegionModule::getRegionsByRegionIdCount ( $id );
				break;
			case BaseModule::TYPE_SCHOOL :
				$limit = BaseModule::USER_NUMBER_SIZE;
				$offset = ($page - 1) * $limit;
				$lists = SchoolModule::getSchoolsByRegionId ( $id, $offset, $limit );
				$totalNum = SchoolModule::getSchoolsByRegionIdCount ( $id );
				break;
			case BaseModule::TYPE_ORGANIZATION :
				$limit = BaseModule::USER_NUMBER_SIZE;
				$offset = ($page - 1) * $limit;
				$lists = OrganizationModule::getOrganizationsByRegionId ( $id, $offset, $limit );
				$totalNum = OrganizationModule::getOrganizationsByRegionIdCount ( $id );
				break;
			default :
				$lists = EncyclopediaModule::getEncyclopediasByRegionId ( $id, $offset, $limit );
				$totalNum = EncyclopediaModule::getEncyclopediasByIdCount ( $id );
				break;
		}
		
		$listsPageTotal = ceil ( $totalNum / $limit );
		$url = \URL::to ( '/' ) . '/area/region/' . $id . '/' . $type;
		$pageHtml = Page::genePageHtml ( $url, $page, $listsPageTotal );
		
		$comments = CommentModule::getComments ( BaseModule::TYPE_REGION, $id );
		UserModule::fillUsers ( $comments );
		$total = CommentModule::getCommentsCount ( BaseModule::TYPE_REGION, $id );
		$limit = BaseModule::COMMENT_PAGE_SIZE;
		$pageTotal = ceil ( $total / $limit );
		
		if ($id == 1) {
			$news = NewsModule::getHeadlineNews(self::AREA_NEWS_NUMBER);
		} else {
			$news = NewsModule::getNewsByRegionId($id, 0, self::AREA_NEWS_NUMBER);
		}
		
		$this->data = compact ( 'id', 'type', 'data', 'lists', 'listsPageTotal', 'pageHtml', 'comments', 'targetId', 'targetType', 'isLiked', 'isJoined', 'pageTotal', 'news' );
		
		return $this->showView ( 'area.region' );
	}
	
	// 学校主页
	public function getSchool($id = 0, $type = 5, $page = 1) {
		$data = SchoolModule::getSchoolInfoById ( $id );
		if (empty ( $data )) {
			return \Redirect::to ( \URL::to ( '/' ) . '/area/index' );
		}
		
		$userId = $this->getLoginUserId ();
		$targetId = $id;
		$targetType = BaseModule::TYPE_SCHOOL;
		$isLiked = LikeModule::isLiked ( $userId, $targetType, $targetId );
		$isJoined = SchoolModule::isJoined ( $userId, $id );
		
		$totalNum = 0;
		$limit = BaseModule::ITEM_NUMBER_INDEX;
		$offset = ($page - 1) * $limit;
		
		switch ($type) {
			case BaseModule::TYPE_ENCYCLOPEDIA :
				$lists = EncyclopediaModule::getEncyclopediasBySchoolId ( $id, $offset, $limit );
				UserModule::fillUsers ( $lists );
				$totalNum = EncyclopediaModule::getEncyclopediasBySchoolIdCount ( $id );
				break;
			case BaseModule::TYPE_CONTEST :
				$lists = ContestModule::getContestsBySchoolId ( $id, $offset, $limit );
				UserModule::fillUsers ( $lists );
				$totalNum = ContestModule::getContestsBySchoolIdCount ( $id );
				break;
			case BaseModule::TYPE_EXCHANGE :
				$lists = ExchangeModule::getExchangeTradesBySchoolId ( $id, $offset, $limit );
				UserModule::fillUsers ( $lists );
				$totalNum = ExchangeModule::getExchangeTradesBySchoolIdCount ( $id );
				break;
			case BaseModule::GROUP_EXPERT :
				$limit = BaseModule::USER_NUMBER_SIZE;
				$offset = ($page - 1) * $limit;
				$lists = UserModule::getExpertsBySchoolId ( $id, $offset, $limit );
				$totalNum = UserModule::getExpertsBySchoolIdCount ( $id );
				CircleModule::fillIsFollows ( $lists, $userId, '' );
				break;
			case BaseModule::GROUP_COMMON_USER :
				$limit = BaseModule::USER_NUMBER_SIZE;
				$offset = ($page - 1) * $limit;
				$lists = UserModule::getCommonUsersBySchoolId ( $id, $offset, $limit );
				$totalNum = UserModule::getCommonUsersBySchoolIdCount ( $id );
				CircleModule::fillIsFollows ( $lists, $userId, '' );
				break;
			default :
				$lists = EncyclopediaModule::getEncyclopediasBySchoolId ( $id, $offset, $limit );
				$totalNum = EncyclopediaModule::getEncyclopediasBySchoolIdCount ( $id );
				break;
		}
		
		$listsPageTotal = ceil ( $totalNum / $limit );
		$url = \URL::to ( '/' ) . '/area/school/' . $id . '/' . $type;
		$pageHtml = Page::genePageHtml ( $url, $page, $listsPageTotal );
		
		$comments = CommentModule::getComments ( BaseModule::TYPE_SCHOOL, $id );
		UserModule::fillUsers ( $comments );
		
		$total = CommentModule::getCommentsCount ( BaseModule::TYPE_REGION, $id );
		$limit = BaseModule::COMMENT_PAGE_SIZE;
		$pageTotal = ceil ( $total / $limit );
		
		$news = NewsModule::getNewsBySchoolId($id, 0, self::AREA_NEWS_NUMBER);
		
		$this->data = compact ( 'id', 'type', 'data', 'lists', 'listsPageTotal', 'pageHtml', 'comments', 'targetId', 'targetType', 'isLiked', 'isJoined', 'pageTotal', 'news');
		
		return $this->showView ( 'area.school' );
	}
	
	// 政企主页
	public function getOrganization($id = 0, $type = 5, $page = 1) {
		$data = OrganizationModule::getOrganizationInfoById ( $id );
		if (empty ( $data )) {
			return \Redirect::to ( \URL::to ( '/' ) . '/area/index' );
		}
		
		$userId = $this->getLoginUserId ();
		$targetId = $id;
		$targetType = BaseModule::TYPE_ORGANIZATION;
		$isLiked = LikeModule::isLiked ( $userId, $targetType, $targetId );
		$isJoined = OrganizationModule::isJoined ( $userId, $id );
		
		$totalNum = 0;
		$limit = BaseModule::ITEM_NUMBER_INDEX;
		$offset = ($page - 1) * $limit;
		
		switch ($type) {
			case BaseModule::TYPE_ENCYCLOPEDIA :
				$lists = EncyclopediaModule::getEncyclopediasByOrganizationId ( $id, $offset, $limit );
				UserModule::fillUsers ( $lists );
				$totalNum = EncyclopediaModule::getEncyclopediasByOrganizationIdCount ( $id );
				break;
			case BaseModule::TYPE_CONTEST :
				$lists = ContestModule::getContestsByOrganizationId ( $id, $offset, $limit );
				UserModule::fillUsers ( $lists );
				$totalNum = ContestModule::getContestsByOrganizationIdCount ( $id );
				break;
			case BaseModule::TYPE_EXCHANGE :
				$lists = ExchangeModule::getExchangeTradesByOrganizationId ( $id, $offset, $limit );
				$totalNum = ExchangeModule::getExchangeTradesByOrganizationIdCount ( $id );
				UserModule::fillUsers ( $lists );
				break;
			case BaseModule::GROUP_EXPERT :
				$lists = UserModule::getExpertsByOrganizationId ( $id, $offset, $limit );
				$totalNum = UserModule::getExpertsByOrganizationIdCount ( $id );
				CircleModule::fillIsFollows ( $lists, $userId, '' );
				break;
			case BaseModule::GROUP_COMMON_USER :
				$lists = UserModule::getCommonUsersByOrganizationId ( $id, $offset, $limit );
				$totalNum = UserModule::getCommonUsersByOrganizationIdCount ( $id );
				CircleModule::fillIsFollows ( $lists, $userId, '' );
				break;
			default :
				$lists = EncyclopediaModule::getEncyclopediasByOrganizationId ( $id, $offset, $limit );
				UserModule::fillUsers ( $lists );
				$totalNum = EncyclopediaModule::getEncyclopediasByOrganizationIdCount ( $id );
				break;
		}
		
		$listsPageTotal = ceil ( $totalNum / $limit );
		$url = \URL::to ( '/' ) . '/area/organization/' . $id . '/' . $type;
		$pageHtml = Page::genePageHtml ( $url, $page, $listsPageTotal );
		
		$comments = CommentModule::getComments ( BaseModule::TYPE_ORGANIZATION, $id );
		UserModule::fillUsers ( $comments );
		
		$total = CommentModule::getCommentsCount ( BaseModule::TYPE_REGION, $id );
		$limit = BaseModule::COMMENT_PAGE_SIZE;
		$pageTotal = ceil ( $total / $limit );
		
		$news = NewsModule::getNewsByOrganizationId($id, 0, self::AREA_NEWS_NUMBER);
		
		$this->data = compact ( 'id', 'type', 'data', 'lists', 'listsPageTotal', 'pageHtml', 'comments', 'targetId', 'targetType', 'isLiked', 'isJoined', 'pageTotal', 'news' );
		return $this->showView ( 'area.organization' );
	}
	
	// 加入这里--地区学校政企
	public function postJoinArea() {
		return $this->execute ( function () {
			
			$this->requireAjaxLogin ();
			
			$id = $this->getParam ( 'id', 'required|natureNumber' );
			$type = $this->getParam ( 'join_type', 'required|natureNumber' );
			$this->outputParamErrorIfExist ();
			
			$userId = $this->getLoginUserId ();
			if ($type == BaseModule::TYPE_REGION) {
				RegionModule::joinRegion ( $userId, $id );
			} elseif ($type == BaseModule::TYPE_SCHOOL) {
				SchoolModule::joinSchool ( $userId, $id );
			} elseif ($type == BaseModule::TYPE_ORGANIZATION) {
				OrganizationModule::joinOrganization ( $userId, $id );
			} else {
				return $this->outputContent ( 0 );
			}
			
			return $this->outputContent ( 1 );
		} );
	}
	
	// 地区卡片
	public function postRegionCard() {
		$id = $this->getParam ( 'id', 'required|natureNumber' );
		$this->outputParamErrorIfExist ();
		
		$region = RegionModule::getRegionInfoById ( $id );
		$region ['nationRank'] = AreaModule::getRank ( AreaModule::TYPE_REGION, $id );
		$region ['provinceRank'] = AreaModule::getRank ( AreaModule::TYPE_REGION, $id, $region ['province_id'] );
		$this->data = compact ( 'region' );
		$result = $this->showView ( 'area.region-card' )->__toString ();
		return $this->outputContent ( $result );
	}
	
	// 地区交流区
	public function postComments() {
		$targetType = $this->getParam ( 'target_type', 'required|natureNumber' );
		$id = $this->getParam ( 'target_id', 'required|natureNumber' );
		$page = $this->getParam ( 'page', 'required|natureNumber' );
		$this->outputParamErrorIfExist ();
		
		$limit = 10;
		$offset = ($page - 1) * $limit;
		$comments = CommentModule::getComments ( $targetType, $id, $offset, $limit );
		$pageTotal = CommentModule::getCommentsCount ( $targetType, $id );
		UserModule::fillUsers ( $comments );
		
		$this->data = compact ( 'comments', 'pageTotal', 'targetType', 'id', 'page' );
		$result = $this->showView ( 'area.comments-page' )->__toString ();
		return $this->outputContent ( $result );
	}
}