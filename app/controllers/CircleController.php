<?php namespace App\Controllers;

use App\Modules\CircleModule;
use App\Modules\UserModule;
use App\Modules\BaseModule;
use App\Common\Page;
/**
 * 朋友圈控制器
 *
 * 处理朋友圈的相关功能
 */
class CircleController extends BaseController {

    /**
     * 显示我的朋友圈
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIndex($type = CircleModule::TYPE_MY_CIRCLE, $circleId = 0, $page = 1) {
        if (! $this->isLogin()) {
            return $this->requireLogin();
        }
        $userId = $this->getLoginUserId();
        
        $page = $page ? $page : 1;
        $offset = ($page - 1) * CircleModule::USER_NUMBER_SIZE;
        
        $members = array();
        $memberNum = 0;
        
        if ($type == CircleModule::TYPE_CIRCLE_ME) {
            $members = CircleModule::getCircleMe($userId, $offset);
            $memberNum = CircleModule::getCircleMeNum($userId);
        } else { 
            $type = CircleModule::TYPE_MY_CIRCLE;            
            if (! $circleId) {
                $members = CircleModule::getAllMembersAndCircleInfo($userId, $offset);             
                $memberNum = CircleModule::getMembersNum($userId); 
            } else {
                $authority = CircleModule::getCircleAuthority($circleId, $userId);
                
                if($authority) {
                    $members = CircleModule::getMembersAndCircleInfoByCircleId($circleId, $userId, $offset);
                    $memberNum = CircleModule::getMembersNum($userId, $circleId);
                }
                
            }
        }
        
        UserModule::fillUsers($members);
        
        $circles = CircleModule::getCirclesByUserId($userId);
        
        $cur = $page;
        $totalPage = ceil($memberNum/BaseModule::USER_NUMBER_SIZE);
        $url = \URL::to('/') . '/circle/index/'.$type.'/'.$circleId;
        $page = Page::genePageHtml($url,$cur,$totalPage);
        
        $isMe = 1;
        $this->data = compact('circles', 'members', 'type', 'isMe', 'circleId', 'page', 'cur');
        return $this->showView('circle.circle');
    }

    /**
     * 创建一个新圈子
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateCircle() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
            
            $userId = $this->getLoginUserId();
            
            $name = $this->getParam('name', 'required{error_empty_name}|mbmaxlength[10]{error_must_less_than_10}');
            
            $this->outputParamErrorIfExist();
            
            $result = CircleModule::createCircle($userId, $name);
            
            if(! $result) {
                $this->setParamError('name', 'error_already_have_the_circle');
            }
            
            $this->outputParamErrorIfExist();
            
            return $this->outputContent(array('id' =>$result, 'name' => $name));
        });
    }

    /**
     * 删除一个圈子并移除所有成员
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDeleteCircle() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
            
            $userId = $this->getLoginUserId();
            
            $id = $this->getParam('id', 'required|natureNumber');
            
            $this->outputParamErrorIfExist();
            
            $result = CircleModule::deleteCircleAndMembers($id, $userId);
            
            return $this->outputContent($result);
        });
    }

    /**
     * 修改一个圈子的名字
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEditCircle() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
            
            $userId = $this->getLoginUserId();
            
            $id = $this->getParam('id', 'required|natureNumber');
            $name = $this->getParam('name', 'required{error_empty_name}|mbmaxlength[10]{error_must_less_than_10}');
            
            $this->outputParamErrorIfExist();
            
            $result = CircleModule::modifyCircleName($id, $name, $userId);
            
            if(! $result) {
                $this->setParamError('name', 'error_have_no_right');
            }
            $this->outputParamErrorIfExist();
            
            return $this->outputContent(array('id' =>$result, 'name' => $name));
        });
    }

    /**
     * 添加一个用户到某个圈子
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAddMember() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
            
            $ownerUserId = $this->getLoginUserId();
            
            $circleId = $this->getParam('circle_id', 'required|natureNumber');
            $userId = $this->getParam('user_id', 'required|natureNumber');
            
            $this->outputParamErrorIfExist();
            
            $result = CircleModule::addCircleMember($circleId, $ownerUserId, $userId);
            
            return $this->outputContent($result);
        });
    }
    
    /**
     * 创建圈子并将用户添加到圈子
     * 
     * @return Ambigous <string, \Illuminate\Http\JsonResponse>
     */
    public function postCreateAddMember() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
        
            $ownerUserId = $this->getLoginUserId();
            $userId = $this->getParam('user_id', 'required|natureNumber');
            $name = $this->getParam('name', 'required{error_empty_name}|mbmaxlength[10]{error_must_less_than_10}');
        
            $this->outputParamErrorIfExist();
        
            $result = CircleModule::createCircleAndAddMember($ownerUserId, $userId, $name);
            
            if($result == -1) {
                
                $this->setParamError('name', 'error_have_the_circle');
                
            }elseif(! $result) {
                $this->setParamError('user_id', 'error_add_member_failure');
            }
            
            $this->outputParamErrorIfExist();
        
            return $this->outputContent(array('id'=>$result,'name' => $name));
        });
    }
    
    /**
     * 关注后选择分组
     */
    public function postChooseGroup() {
        return $this->execute(function() {
            $this->requireAjaxLogin();
            
            $ownerUserId = $this->getLoginUserId();
            
            $circleIds = \Input::get('circle_ids');
            $userId = $this->getParam('user_id', 'required|natureNumber');           
            
            if(empty($circleIds) || count($circleIds) == 0|| ! is_array($circleIds)) {
                $this->setParamError('circle_ids', 'error_empty_circleIds');
            }
            
            $this->outputParamErrorIfExist();
            
            $result = CircleModule::chooseGroup($circleIds,$ownerUserId, $userId);
            
            if(!$result) {
                $this->setParamError('circle_ids', 'error_invalid_circle_ids');
            }
            $this->outputParamErrorIfExist();
            
            return $this->outputContent($result);
        });
    }
    
    /**
     * 关注用户
     * 
     * @return Ambigous <string, \Illuminate\Http\JsonResponse>
     */
    public function postFollowUser() {
        return $this->execute(function() {
            $this->requireAjaxLogin();
            
            $ownerUserId = $this->getLoginUserId();
            $userId = $this->getParam('user_id', 'required|natureNumber');
            
            $this->outputParamErrorIfExist();
            
            $result = CircleModule::addCircleMemberToDefaultCircle($ownerUserId, $userId);
            
            if(!$result) {
                $this->setParamError('user_id', 'error_already_followed');
            }
            $this->outputParamErrorIfExist();
            
            return $this->outputContent($result);
        });
    }
    
    /**
     * 取消关注
     * 
     * @return Ambigous <string, \Illuminate\Http\JsonResponse>
     */
    public function postCancelFollow() {
        return $this->execute(function() {
            $this->requireAjaxLogin();
            
            $ownerUserId = $this->getLoginUserId();
            $userId = $this->getParam('user_id', 'required|natureNumber');
            
            $this->outputParamErrorIfExist();
            
            $result = CircleModule::cancelFollow($ownerUserId, $userId);
            
            if(!$result) {
                $this->setParamError('user_id', 'error_already_cabcel_followed');
            }
            $this->outputParamErrorIfExist();
            
            return $this->outputContent($result);
        });
    }

    /**
     * 移除一个成员出某个圈子
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRemoveMember() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
            
            $ownerUserId = $this->getLoginUserId();
            
            $circleId = $this->getParam('circle_id', 'required|natureNumber');
            $userId = $this->getParam('user_id', 'required|natureNumber');
            
            $this->outputParamErrorIfExist();
            
            $result = CircleModule::removeCircleMember($circleId, $ownerUserId, $userId);
            
            return $this->outputContent($result);
        });
    }

    /**
     * 获取一个成员在我的所有圈子的具体情况
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postMemberCircleInfo() {
        return $this->execute(function () {
            $this->requireAjaxLogin();
            
            $ownerUserId = $this->getLoginUserId();
            
            $memberUserId = $this->getParam('user_id', 'required|natureNumber');
            $type = $this->getParam('type');
            if(! $type) {
                $type = CircleModule::USER_CIRCLE_CARD;
            }
            
            $this->outputParamErrorIfExist();
            
            $circles = CircleModule::getMemberAndCircleInfoByMemberUserId($memberUserId, $ownerUserId);
            
            CircleModule::sortCirclesByDefault($circles);
            
            if($type == CircleModule::USER_CIRCLE_CARD) {
                $tpl = 'user.user-circle-card';
            }else{
                $tpl = 'circle.circle-card';
            }
            
            $member = UserModule::getUserById($memberUserId);
            $this->data = compact('circles', 'member');
            
            $result = $this->showView($tpl)->__toString();
            
            return $this->outputContent($result);
        });
    }
}
