<?php

namespace App\Controllers;

use App\Common\Utils;
use App\Modules\FeedBackModule;
use App\Modules\IndustryModule;
use App\Modules\UserModule;
use App\Modules\ChatModule;
use App\Modules\NotificationModule;
use App\Modules\NewsModule;
use App\Modules\ProjectModule;
use App\Modules\TaskModule;
use App\Modules\HiringModule;
use App\Modules\CircleModule;
use App\Modules\DynamicModule;
use App\Modules\ExpertModule;
use App\Modules\ExpertCommentModule;
use App\Modules\ItemModule;
use App\Modules\ExchangeModule;
use App\Modules\EncyclopediaModule;
use App\Modules\RegionModule;
use App\Modules\SchoolModule;
use App\Modules\OrganizationModule;
use App\Models\Organization;
use App\Modules\ContestModule;

/**
 * 首页控制器
 */
class HomeController extends BaseController {
	const HOME_HOT_CONTEST_NUM = 3;
	
	/**
	 * 首页
	 *
	 * @return string
	 */
	public function getIndex() {

		$news = NewsModule::getHeadlineNews ();

		$hotProjects = ProjectModule::getProjectRank ( self::HOME_HOT_CONTEST_NUM );
		$hotTasks = TaskModule::getHotTasks ( self::HOME_HOT_CONTEST_NUM );
		$hotHirings = HiringModule::getHotHiring ( self::HOME_HOT_CONTEST_NUM );
		$hotExchanges = ExchangeModule::getRencentlyExchange ();
		$hotExperts = ExpertModule::getExpertsByRandom ( 0, 12 );
		$hotExpertsComments = ExpertCommentModule::getTopExpertComments ();
		if ($hotExpertsComments) {
			foreach ( $hotExpertsComments as $hotExpertsComment ) {
				$hotExpertsComment->user = UserModule::getUserById ( $hotExpertsComment->user_id );
				$hotExpertsComment->item = ItemModule::getItemByTargetIdAndType ( $hotExpertsComment->target_id, $hotExpertsComment->target_type );
			}
		}
		$hotEncyclopedias = IndustryModule::getSecondIndustries ();
		$userId = $this->getLoginUserId ();
		
		$memberRanks = array ();
		$contests = array ();
		$datas = array ();
		
		$regions = RegionModule::getRegionsById ( 1 );
		$schools = SchoolModule::getSchoolsById ( 1 );
		$goverments = OrganizationModule::getOrganizationsById ( 1 );
		$coporates = OrganizationModule::getOrganizationsById ( 1, Organization::TYPE_CORPORATE );
		
		if ($userId) {
			$circleUsers = CircleModule::getAllMembersByUserId ( $userId );
			
			$circleUserIds = array ();
			
			foreach ( $circleUsers as $circleUser ) {
				$circleUserIds [] = $circleUser->user_id;
			}
			if (count ( $circleUserIds )) {
				$datas = DynamicModule::getDynamcicsByUserIds ( $circleUserIds, 0, 10 );
				DynamicModule::fillLikeInfo ( $datas, $userId );
			}
			
			$ranksUserIds = $circleUserIds;
			$ranksUserIds [] = $userId;
			$memberRanks = CircleModule::getCreativeRankByUserIds ( $ranksUserIds );
			
			$contests = CircleModule::getMembersContests ( $circleUserIds );
		} else {
			$contests = ContestModule::getHotContest ();
			$memberRanks = CircleModule::getCreativeRankByUserIds ();
			$datas = DynamicModule::getOfficialDynamics ();
		}
		
		DynamicModule::fillForwards ( $datas );
		DynamicModule::fillActionContent ( $datas );
		UserModule::fillUsers ( $datas );
		
		$this->data = compact ( 'news', 'hotProjects', 'hotTasks', 'hotHirings', 'hotExchanges', 'memberRanks', 'contests', 'datas', 'hotExperts', 'hotExpertsComments', 'hotEncyclopedias', 'regions', 'schools', 'goverments', 'coporates' );
		return $this->showView ( 'index' );
	}

	/**
	 * 首页新闻 换一换
	 * @return \Illuminate\Http\JsonResponse|string
	 */
	public function postNews() {
		return $this->execute ( function () {
			$news = NewsModule::getHeadlineNews ();

			$this->data = compact ( 'news' );

			$result = $this->showView ( 'home.m_news' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}

	/**
	 * 反馈页面
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function getFeedback() {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		//UserModule::calAllCount ();
		
		$this->data ['url'] = urldecode ( \Input::get ( 'url' ) );
		return $this->showView ( 'home.feedback' );
	}

	/**
	 * 提交反馈信息
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postSubmitFeedback() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			$content = $this->getParam ( 'content', 'required{error_feedback_empty}' );
			$url = urldecode ( $this->getParam ( 'url' ) );
			
			$this->outputParamErrorIfExist ();
			
			$result ['id'] = FeedBackModule::createFeedback ( $this->getLoginUserId (), $content, $url );
			$result ['url'] = $url;
			
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 获取未读消息条数
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getNotification() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$chatNum = ChatModule::getUnreadChatNum ( $userId );
			$notificationNum = NotificationModule::getUnreadNotificationNum ( $userId );
			$content = array (
					'chat_num' => $chatNum,
					'notification_num' => $notificationNum 
			);
			
			return $this->outputContent ( $content );
		} );
	}
	
	/**
	 * 创创召 召一下
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postCcz() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$result = UserModule::useCcz ( $this->getLoginUserId () );
			return $this->outputContent ( $result );
		} );
	}



	public function getFrame() {
		return $this->showView ( 'common.frame' );
	}
	public function getAbout() {
		$type = 'about';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
	public function getHelp1() {
		$type = 'help1';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
	public function getHelp2() {
		$type = 'help2';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
	public function getHelp3() {
		$type = 'help3';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
	public function getHelp4() {
		$type = 'help4';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
	public function getAgreement1() {
		$type = 'agreement1';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
	public function getAgreement2() {
		$type = 'agreement2';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
	public function getAgreement3() {
		$type = 'agreement3';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
	public function getAgreement4() {
		$type = 'agreement4';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
	public function getEmploy() {
		$type = 'employ';
		$this->data = compact ( 'type' );
		return $this->showView ( 'home.home-bottom' );
	}
}
		
		