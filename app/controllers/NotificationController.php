<?php

namespace App\Controllers;

use App\Modules\BaseModule;
use App\Modules\NotificationModule;
use App\Modules\UserModule;
use App\Modules\ContestModule;
use App\Modules\ExchangeModule;
use App\Modules\ChatModule;
use App\Common\Page;

/**
 * 系统消息控制器
 */
class NotificationController extends BaseController {
	
	/**
	 * 消息中心
	 */
	public function getIndex($status = 0, $page = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		$limit = BaseModule::COMMENT_PAGE_SIZE;
		$offset = ($page - 1) * $limit;
		
		$userId = $this->getLoginUserId ();
		$notifications = NotificationModule::getNotifications ( $userId, $status, $offset, $limit );
		$total = NotificationModule::getNotificationNum($userId,$status);
		$pageTotal = ceil($total/$limit);
		$contests = ContestModule::getHotContest ();
		$exchanges = ExchangeModule::getHotExchange ();
		$carepeople = UserModule::getCarePeople ($userId);
		
		$url = \URL::to('/').'/notification/index/'.$status;
		
		$pageHtml = Page::genePageHtml($url,$page,$pageTotal);
		
		$this->data = compact ( 'status', 'pageHtml','pageTotal', 'notifications', 'contests', 'exchanges', 'carepeople' );

		$result = $this->showView ( 'notification.index' );
		if($status==0){
		    $length = count($notifications);
			for($i = $length-1; $i >= 0; $i--){
			    if($notifications[$i]->target_type == NotificationModule::TYPE_COIN) {
			        NotificationModule::getCoinByEvent($notifications[$i]);
			    }
				NotificationModule::updateNotificationToReaded($notifications[$i]->id);
			}
		}
		
		return $result;
	}
	
	/**
	 * 接口：创创召下的消息--5条未读的最新消息
	 */
	public function postCczNotification() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$userId = $this->getLoginUserId ();
			$notifications = NotificationModule::getNotifications ( $userId, 0, 0, 5 );
			
			$this->data = compact ( 'notifications' );
			
			$result = $this->showView ( 'notification.ccz-notification' )->__toString ();
			return $this->outputContent ( $result );
		} );
	}
	
	/**
	 * 获取消息数
	 * 
	 * @return Ambigous <string, \Illuminate\Http\JsonResponse>
	 */
	public function postNotification() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			$userId = $this->getLoginUserId ();
			$data ['chatNum'] = ChatModule::getUnreadChatNum ( $userId );
			$data ['notificationNum'] = NotificationModule::getNotificationNum ( $userId );
			return $this->outputContent ( $data );
		} );
	}
}