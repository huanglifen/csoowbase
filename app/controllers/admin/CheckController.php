<?php 
namespace App\Controllers\Admin;

use App\Common\Sms;
use App\Modules\ContestModule;
use App\Modules\UserModule;
use App\Modules\ItemModule;
use App\Modules\ExchangeModule;
use App\Modules\EncyclopediaModule;
use App\Controllers\BaseController;
use App\Common\Page;
use App\Controllers\UserNotLoginException;
use App\Modules\HiringModule;

class CheckController extends BaseController {
	
	const ITEM_PAGE_NUMBER = 12;

	/**
	 * 审核比赛列表页
	 * @param int $page
	 */
	public function getContests($status = ItemModule::STATUS_WAITING_CHECK ,$page = 1) {
		if (! $this->isAdmin ()) {
			return $this->requireLogin ();
		}
		
		$offset = ($page - 1) * self::ITEM_PAGE_NUMBER;
		
		$contests = ContestModule::getInCheckContests($status, $offset, self::ITEM_PAGE_NUMBER);
		UserModule::fillUsers($contests);
		
		$totalNum = ItemModule::getInCheckContestsCount($status);
		$totalPage = ceil($totalNum/self::ITEM_PAGE_NUMBER);
		$url = \URL::to('/check/contests').'/'.$status;
		$pageHtml = Page::genePageHtml($url, $page, $totalPage);
		
		$this->data = compact('contests', 'page', 'pageHtml', 'totalPage', 'status');
		
		return $this->showView('admin.check.contest-list');
	}
	
	/**
	 * 审核比赛内页
	 * @param int $contestId
	 */
	public function getContestDetail($contestId) {
		if (! $this->isAdmin ()) {
			return $this->requireLogin ();
		}
		
		$contest = ContestModule::getContestById($contestId);
		UserModule::fillUser($contest);
		$contest = ItemModule::fillContests([$contest]);
		
		$view = '';
		
		switch ($contest->type) {
			case ItemModule::TYPE_PROJECT : $view = 'project'; break;
			case ItemModule::TYPE_TASK : $view = 'task'; break;
			case ItemModule::TYPE_HIRING : 
			$view = 'hiring';
			$prizes = HiringModule::getHiringPrizes($contest->id);
			$contest->prizes = $prizes;
			break;
			default : $view = 'project';
		}
		
		$this->data = compact('contest');
		
		return $this->showView('admin.check.contest-detail-' . $view);
	}
	

	/**
	 * 审核百科列表页
	 * @param int $page
	 */
	public function getEncyclopedias($status = 0,$page = 1) {
	    if (! $this->isAdmin ()) {
		    return \Redirect::to(\URL::to('/') . '/user/login');
	    }

		$limit = self::ITEM_PAGE_NUMBER;
	    $offset = ($page - 1) * self::ITEM_PAGE_NUMBER;

		$lists = EncyclopediaModule::getCheckEncyclopedias($status,$offset, $limit);
	    UserModule::fillUsers($lists);
	    
	    $totalNum = EncyclopediaModule::getCheckEncyclopediasCount($status);
	    $totalPage = ceil($totalNum/self::ITEM_PAGE_NUMBER);
	    $url = \URL::to('/check/encyclopedias').'/'.$status;
	    $pageHtml = Page::genePageHtml($url, $page, $totalPage);
	
	    $this->data = compact('lists', 'page','status','totalPage', 'pageHtml');
	
	    return $this->showView('admin.check.encyclopedia-list');
	}
	
	/**
	 * 审核百科内页
	 * @param int $contestId
	 */
	public function getEncyclopediaDetail($encyclopediaId) {
	    if (! $this->isAdmin()) {
		    return \Redirect::to(\URL::to('/') . '/user/login');
	    }
	
	    $encyclopedia = EncyclopediaModule::getEncyclopediaById($encyclopediaId);
	    UserModule::fillUser($encyclopedia);
	
	    $this->data = compact('encyclopedia');
        
        return $this->showView('admin.check.encyclopedia-detail');
    }


    /**
     * 审核交易列表页
     *
     * @param string $status            
     * @param number $page            
     */
    public function getExchanges ($status = 0, $page = 1)
    {
        if (! $this->isAdmin()) {
            return \Redirect::to(\URL::to('/') . '/user/login');
        }
        
        $offset = ($page - 1) * self::ITEM_PAGE_NUMBER;
        $lists = ExchangeModule::getCheckExchange($status, $offset, 
                self::ITEM_PAGE_NUMBER);
        UserModule::fillUsers($lists);
        
        $totalNum = ExchangeModule::getCheckExchangeCount($status);
        $totalPage = ceil($totalNum/self::ITEM_PAGE_NUMBER);
     
        $url = \URL::to('/check/exchanges').'/'.$status;
        $pageHtml = Page::genePageHtml($url, $page, $totalPage);
        
        $this->data = compact('lists', 'page', 'status', 'pageHtml', 'totalPage');
        
        return $this->showView('admin.check.exchange-trade-list');
    }

    /**
     * 审核交易内页
     *
     * @param int $id            
     */
    public function getExchangeDetail ($id)
    {
        if (! $this->isAdmin()) {
            return \Redirect::to(\URL::to('/') . '/user/login');
        }
        
        $trade = ExchangeModule::getTradeById($id);
	        UserModule::fillUser($trade);
	         
	        $this->data = compact('trade');
	        return $this->showView('admin.check.exchange-trade-detail');
	}


	/**
	 * 审核比赛
	 * @return \Illuminate\Http\JsonResponse|string
	 */
	public function postCheckOpertion() {
		return $this->execute(function() {
			if(! $this->isAdmin()) {
				throw new UserNotLoginException();
			}
			$type = $this->getParam('type', 'required');
			$id = $this->getParam('id', 'required|natureNumber');
			$reason = $this->getParam('reason');
			$flag = $this->getParam('status', 'required|natureNumber');

			if($flag == 2 && !$reason) {
				$this->setParamError('remark', 'error_not_empty');
			}
			$this->outputParamErrorIfExist();
			
			if($flag == 2) {
				$status = ItemModule::STATUS_NEED_UPDATE;
			}else{
			    $status = ItemModule::STATUS_GOING;
			}
			if($type == 'contest'){
				$result = ContestModule::checkContest($id, $reason,$status);
			}elseif($type == 'exchange'){
				$result = ExchangeModule::checkExchange($id, $reason, $status);
			}elseif($type == 'encyclopedia'){
				$result = EncyclopediaModule::checkEncyclopedia($id, $reason, $status);
			}

			return $this->outputContent($result);
		});
	}



}

