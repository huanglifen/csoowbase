<?php
namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Modules\ExpertModule;
use App\Common\Page;
use App\Modules\UserModule;

class ExpertController extends BaseController {

	const  ITEM_PAGE_NUMBER = 10;
    public function getExpertApplications($page = 1) {
        if (! $this->isAdmin ()) {
            return \Redirect::to(\URL::to('/') . '/user/login');
        }
        
        $limit = self::ITEM_PAGE_NUMBER;
        $offset = ($page - 1) * self::ITEM_PAGE_NUMBER;
        $experts = ExpertModule::getExpertApplications($offset, $limit);
        
    }
    
    public function getUser($page = 1) {
        if (! $this->isAdmin ()) {
            return \Redirect::to(\URL::to('/') . '/user/login');
        }
        
        $limit = self::ITEM_PAGE_NUMBER;
        $offset = ($page - 1) * self::ITEM_PAGE_NUMBER;
        
        $datas = ExpertModule::getNormalUsers($offset, $limit);
        
        $totalNum = ExpertModule::getNormalUsersCount();
        $totalPage = ceil($totalNum/self::ITEM_PAGE_NUMBER);
        $url = \URL::to('/expert/user');
        $pageHtml = Page::genePageHtml($url, $page, $totalPage);
        
        $this->data = compact('datas', 'page','totalPage', 'pageHtml');
        
        return $this->showView('admin.expert.user');
    }

	public function getBecomeExpert($userId) {

		if (! $this->isAdmin ()) {
			return \Redirect::to(\URL::to('/') . '/user/login');
		}


		$user = UserModule::getUserById($userId);
		if ($user) {
			$user->group_id = UserModule::GROUP_EXPERT;
			$user->save();
			return  \Redirect::to(\URL::to('/') . '/expert/expert');
		}else{
			return  \Redirect::to(\URL::to('/') . '/expert/user');
		}
	}


    public function getExpert($page = 1) {
        if (! $this->isAdmin ()) {
            return \Redirect::to(\URL::to('/') . '/user/login');
        }
        
        $limit = self::ITEM_PAGE_NUMBER;
        $offset = ($page - 1) * self::ITEM_PAGE_NUMBER;

	    $datas = ExpertModule::getExperts($offset, $limit);
        
        $totalNum = ExpertModule::getExpertsCount();
        $totalPage = ceil($totalNum/self::ITEM_PAGE_NUMBER);
        $url = \URL::to('/expert/expert');
        $pageHtml = Page::genePageHtml($url, $page, $totalPage);
        
        $this->data = compact('datas', 'page','totalPage', 'pageHtml');
        
        return $this->showView('admin.expert.expert');
    }


	public function getExpertEdit($userId = 0) {
		if (! $this->isAdmin ()) {
			return \Redirect::to(\URL::to('/') . '/user/login');
		}
		$data = UserModule::getUserById($userId);

		$this->data = compact('data');

		return $this->showView('admin.expert.expert-edit');
	}
    

}