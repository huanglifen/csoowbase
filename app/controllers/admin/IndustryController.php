<?php 
namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Modules\IndustryModule;
use App\Models\Industries;

class IndustryController extends BaseController {

	/**
	 * 行业管理
	 * @return mixed
	 */
	public function getIndex(){
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		$datas = IndustryModule::getFirstIndustries();
		
		$this->data = compact('datas');
		
		return $this->showView('admin.industry.index');
	}
	
	public function getSecondIndustry($id = 0) {
			if(! $this->isAdmin()) {
				throw new UserNotLoginException();
			}
			
			$this->outputParamErrorIfExist();

			$industry = IndustryModule::getIndustryById($id);
			$datas = IndustryModule::getSecondIndustries($id);

			$this->data = compact('industry','datas');

			return $this->showView('admin.industry.second-industry');
	}
	
	public function postUpdateIndustry() {
		return $this->execute(function() {
			if(! $this->isAdmin()) {
				throw new UserNotLoginException();
			}
			
			$id = $this->getParam('id', 'required|natureNumberAndZero');
			$name = $this->getParam('name');
			
			$this->outputParamErrorIfExist();
			
			$result = IndustryModule::updateIndustryName($id, $name);
			
			return $this->outputContent($result);
		});
	}
	
	public function postCreateIndustry() {
		return $this->execute(function() {
			if(! $this->isAdmin()) {
				throw new UserNotLoginException();
			}
			
			$name = $this->getParam('name', 'required');
			$firstId = $this->getParam('first_id');
			
			$this->outputParamErrorIfExist();
			
			$result = IndustryModule::createIndustry($name, $firstId);
			
			return $this->outputContent($result);
		});
	}
	
	public function postRecommendedIndustry() {
		return $this->execute(function() {
			if(! $this->isAdmin()) {
				throw new UserNotLoginException();
			}
			
			$id = $this->getParam('id', 'required|natureNumber');
			$this->outputParamErrorIfExist();
			
			$result = IndustryModule::recommendedIndustry($id);
			
			return $this->outputContent($result);
		});
	}
	
	public function getRecommend($id, $isRecommend) {
	    
	    if (! $this->isAdmin ()) {
	        return $this->requireLogin ();
	    }
	    IndustryModule::recommendedIndustry($id,$isRecommend);
	    
	    return  \Redirect::to(\URL::to('/') . '/industry/index');
	}
}