<?php 
namespace App\Controllers\Admin;

use App\Controllers\BaseController;

class AdminController extends BaseController {

	/**
	 * 后台首页
	 * @return mixed
	 */
	public function getIndex(){
		if (! $this->isAdmin ()) {
			return \Redirect::to(\URL::to('/') . '/user/login');
		}
		return $this->showView('admin.common.index');
	}
}