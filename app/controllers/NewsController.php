<?php
	namespace App\Controllers;

	use App\Modules\NewsModule;

	class NewsController extends BaseController
	{

		/**
		 * 采集新闻
		 */
		public function getNews()
		{
			set_time_limit(0);
			if (date('H') >= 1 && date('H') <= 4) {
				News::truncate();
			}

			$this->gatherHeadlineNews();
			\NewsCrawler::crawlAreaNews();

		}

		/**
		 * 首页获取头条新闻
		 */
		public function gatherHeadlineNews()
		{

			$url      = 'http://news.baidu.com/';
			$contents = file_get_contents($url);
			$contents = iconv('gb2312', 'utf-8//IGNORE', $contents);
			$reg      = '|<ul class="ulist focuslistnews"\s*?>(.*?)<\/ul>|s';
			preg_match_all($reg, $contents, $match);

			$matchStr = implode('|', $match [1]);
			$pattern  = '/<a href="(.*?)".*?target="_blank">(.*?)<\/a>/s';

			preg_match_all($pattern, $matchStr, $matchs);

			foreach ($matchs [1] as $key => $value) {
				NewsModule::createNews($matchs [2] [$key], $value);
			}
		}

	}