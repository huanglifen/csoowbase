<?php


namespace App\Controllers;

use App\Modules\TaskModule;
use App\Modules\UserModule;
use App\Modules\ItemModule;
use App\Modules\TagModule;
use App\Modules\BaseModule;
use App\Modules\CreativeValueRatingModule;
use App\Modules\LikeModule;
use App\Modules\ReportModule;
use App\Modules\CircleModule;
use App\Common\Page;
use App\Models\TaskWork;
use App\Models\User;

/**
 * 创意任务人才比赛控制器
 *
 * 处理创意任务人才比赛功能，如发布任务、提交作品等。
 */
class TaskController extends ItemController {
	const PARAM_SUBMIT_WORK_END_TIME = 'submit_work_end_time';
	const DETAIL_INDEX_WORK_NUMBER = 3;
	const TASK_LIST_NUMBER = 12;
	protected $targetTypeString = 'task';
	
	/**
	 * 显示比赛列表页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getIndex($status = 0) {
		$lists = TaskModule::getListsByStatus ( $status, 0, BaseModule::ITEM_NUMBER_INDEX );
		UserModule::fillUsers ( $lists );
		
		$tags = TagModule::getHotTags ( BaseModule::TYPE_TASK, BaseModule::HOT_TAG_NUMBER );
		
		$ranks = TaskModule::getTaskRanks ();
		UserModule::fillUsers ( $ranks );
		CircleModule::fillIsFollows($ranks, $this->getLoginUserId());
		
		$total = TaskModule::getTotalByTagIdAndStatus ( 0, $status );
		$pageTotal = ceil ( $total / BaseModule::ITEM_NUMBER_INDEX );
		
		$this->data = compact ( 'lists','pageTotal', 'tags', 'ranks' );
			
		return $this->showView ( 'task.list' );
	}	
	
	/**
	 * 详情页公共数据
	 * 
	 * @param unknown $taskId        	
	 * @return multitype:
	 */
	protected function getDetailData($taskId,$type=1,$page=1) {
		$task = TaskModule::getTaskById ( $taskId );
		
		$targetType =  ItemModule::TYPE_TASK;
		UserModule::fillUser ( $task );
		
		$userId = $this->getLoginUserId ();
		CircleModule::fillIsFollow($task, $userId);

		$winner = '';
		
		if (in_array ( $task->status, array (
				ItemModule::STATUS_END_SUCCESS,
				ItemModule::STATUS_WAITING_CONFIRM 
		) )) {
			$winner = TaskModule::getTaskWinner ( $taskId );
			UserModule::fillUser ( $winner );
			CircleModule::fillIsFollow($winner, $userId);
		}
		
		$workNum = TaskModule::getWorkNumByTaskId ( $taskId );
		
		$limit =  BaseModule::COMMENT_PAGE_SIZE ;
		$offset = ($page - 1) * $limit;
		$comments = ItemModule::getComments ($targetType, $taskId, $offset,$limit);
		UserModule::fillUsers ( $comments );
		$commentNum = ItemModule::getCommentNum ($targetType, $taskId );
		$pageHtml = '';
		if ($commentNum > $limit) {
			$pageTotal = ceil ( $commentNum / $limit );
			switch ($type){
				case 1:$tpl='detail';break;
				case 2:$tpl='task-detail';break;
				case 3:$tpl='expert-comments';break;
				case 4:$tpl='works';break;
				case 5:$tpl='updates';break;
				case 6:$tpl='update';break;
				default:$tpl='detail';break;
			}
			$url = \URL::to ( '/' ) . '/task/'.$tpl.'/' . $taskId;
			$pageHtml = Page::genePageHtml ( $url, $page, $pageTotal,'#comments');
		}
		
		
		$expertCommentNum = ItemModule::getExpertCommentsNum ($targetType, $taskId );
		
		$targetId = $taskId;
		$targetType = ItemModule::TYPE_TASK;
		$creativeValueRating = 0;
		
		$canApply = TaskModule::canApply($taskId, $userId);
		
		
		
		$isLiked = LikeModule::isLiked($userId, $targetType, $targetId);
		$isReported = ReportModule::isExist($userId, $targetType, $targetId);
		
		return compact ( 'task', 'winner', 'workNum', 'comments','pageHtml', 'expertCommentNum', 'targetId', 'targetType',  'commentNum','isLiked','isReported', 'canApply');
	}
	
	/**
	 * 比赛详情页
	 * 
	 * @param unknown $id        	
	 * @return \Illuminate\View\View
	 */
	public function getDetail($id = 0,$page = 1) {
		if (! TaskModule::getTaskById ( $id )) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
		
		$this->data = $this->getDetailData ( $id,1,$page);
		
		$works = TaskModule::getTaskWorkByTaskId ( $id, 0, self::DETAIL_INDEX_WORK_NUMBER );
		UserModule::fillUsers ( $works );
		
		$updates = TaskModule::getTaskUpdates ( $id );
		
		$expertComments = TaskModule::getExpertComments ( ItemModule::TYPE_TASK, $id,0, 0, ItemModule::COMMENT_PAGE_SIZE );
		UserModule::fillUsers ( $expertComments );
		self::fillTargetUserInfo($expertComments);
		
		$targetId = $id;
		$targetType = BaseModule::TYPE_TASK;
		$userId = $this->getLoginUserId();
		if ($userId) {
			$creativeValueRating = CreativeValueRatingModule::getCreativeVauleRating ( $userId, $targetType, $targetId );
		} else {
			$creativeValueRating = 0;
		}
		
		
		$this->data ['updates'] = $updates;
		$this->data ['expertComments'] = $expertComments;
		$this->data ['works'] = $works;
		$this->data ['pageName'] = 'index';       
		 //echo '<pre>';print_r($works);exit;
		return $this->showView ( 'task.detail' );
	}
	

	/**
	 * 人才比赛详情内页
	 *
	 * @return \Illuminate\Http\JsonResponse \Illuminate\View\View
	 */
	public function getTaskDetail($taskId,$page = 1) {
		if (! TaskModule::getTaskById ( $taskId )) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
	
		$this->data = $this->getDetailData ( $taskId,2,$page);
	
		$this->data ['pageName'] = 'detail';
		return $this->showView ( 'task.detail-index' );
	}

	/**
	 * 人才比赛专家点评页
	 *
	 * @param unknown $taskId
	 * @return \Illuminate\Http\RedirectResponse \Illuminate\View\View
	 */
	public function getExpertComments($taskId,$page = 1) {
		if (! TaskModule::getTaskById ( $taskId )) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
	
		$this->data = $this->getDetailData ( $taskId,3,$page);
	
		$expertComments = TaskModule::getExpertComments ( ItemModule::TYPE_TASK, $taskId );
		UserModule::fillUsers ( $expertComments );
		self::fillTargetUserInfo($expertComments);
		
	
		$this->data ['expertComments'] = $expertComments;
		$this->data ['pageName'] = 'expertComment';
	
		return $this->showView ( 'task.detail-expert-comments' );
	}
	
	/**
	 * 任务作品列表页
	 *
	 * @return \Illuminate\Http\JsonResponse \Illuminate\View\View
	 */
	public function getWorks($taskId,$page = 1) {
		if (! TaskModule::getTaskById ( $taskId )) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
	
		$this->data = $this->getDetailData ( $taskId ,4,$page);
	
		$works = TaskModule::getTaskWorkByTaskId ( $taskId );
		UserModule::fillUsers ( $works );
	
		$this->data ['works'] = $works;
		$this->data ['pageName'] = 'works';
	
		return $this->showView ( 'task.detail-works' );
	}
	
	/**
	 * 人才比赛动态列表页
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getUpdates($taskId,$page = 1) {
		if (! TaskModule::getTaskById ( $taskId )) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
	
		$this->data = $this->getDetailData ( $taskId ,5,$page);
	
		$updates = TaskModule::getTaskUpdates ( $taskId );
	
		$this->data ['updates'] = $updates;
		$this->data ['pageName'] = 'updates';
		return $this->showView ( 'task.detail-updates' );
	}
	
	/**
	 * 人才比赛动态单独页
	 * 
	 * @param unknown $taskId
	 * @param unknown $updateId
	 * @param number $page
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function getUpdate($taskId, $updateId, $page = 1) {
		if (! TaskModule::getTaskById ( $taskId )) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
		
		$this->data = $this->getDetailData ($taskId, 6, $page);
		
		$update = ItemModule::getUpdateById($updateId);
		
		$this->data ['update'] = $update;
		$this->data ['pageName'] = 'updates';
		return $this->showView ( 'task.detail-update-single' );
	}
	
	
	
	/**
	 * 显示比赛发布页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getPublish($taskId = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		if ($taskId) {
			$task = $this->checkItem($taskId);
			$task = ItemModule::fillContests([$task]);
			$this->data = compact('task');
			//echo '<pre>';print_r($this->data);exit;
		}
		
		return $this->showView ( 'task.publish' );
	}
	
	/**
	 * 显示我要参赛页
	 */
	public function getApply($taskId = 0, $workId = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		if (!$this->checkItem($taskId)) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
		
		$task = $this->getItem($taskId);
		
		$work = array();
		if ($workId) {
			$work = TaskModule::getTaskWorkById($workId);
		}

		$publisher = UserModule::getUserById($task->user_id);
		
		$this->data = compact('taskId', 'work', 'task', 'publisher');
		
		return $this->showView ( 'task.apply' );
	}
	

	
	/**
	 * 任务作品详情页
	 * 
	 * @param unknown $workId        	
	 * @return \Illuminate\Http\RedirectResponse \Illuminate\View\View
	 */
	public function getWork($workId) {
		$work = TaskModule::getTaskWorkById ( $workId );
		
		if (! $work) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
		$userId = $this->getLoginUserId ();
		UserModule::fillUser ( $work );
		CircleModule::fillIsFollow($work, $this->getLoginUserId());
		
		$task = self::getItem ( $work->task_id );
		UserModule::fillUser ( $task );
		
		$expertComments = ItemModule::getExpertComments ( BaseModule::TYPE_TASK, $task->id, $workId, 0, BaseModule::COMMENT_PAGE_SIZE );
		UserModule::fillUsers ( $expertComments );
		
		
		$comments = ItemModule::getComments ( ItemModule::TYPE_TASK_WORK, $workId, 0, ItemModule::COMMENT_PAGE_SIZE );
		UserModule::fillUsers ( $comments );
		
		$targetId = $work->task_id;
		$targetType = ItemModule::TYPE_TASK_WORK;
		$targetSubId = $workId;

		
		$commentNum = ItemModule::getCommentNum ( $targetType, $targetId );
		$pageHtml = '';
		$creativeValueRating = 0;
		if ($userId) {
			$creativeValueRating = CreativeValueRatingModule::getCreativeVauleRating ( $userId,  $targetType ,$workId);
		}
		
		$isLiked = LikeModule::isLiked($userId, $targetType, $targetId);
		$isReported = ReportModule::isExist($userId, $targetType, $targetId);
		$canExpertComment = $this->canExpertComment ( $userId,BaseModule::TYPE_TASK,$work->task_id, $task,$workId);
		
		$this->data = compact ( 'work', 'task', 'expertComments','canExpertComment', 'comments', 'commentNum', 'pageHtml','targetId', 'targetType', 'targetSubId' ,'isLiked','isReported','creativeValueRating');
		// echo '<pre>';print_r($this->data);exit;
		return $this->showView ( 'task.detail-work' );
	}
	
	/**
	 * 查看比赛协议页面
	 * @param int $taskId
	 * @return \Illuminate\View\View
	 */
	public function getAgreement($taskId) {
		$task = $this->checkItem($taskId);
		
		$publisher = UserModule::getUserById($task->user_id);
		
		$winnerWork = TaskModule::getTaskWinner($taskId);
		$winner = UserModule::getUserById($winnerWork->user_id);
		
		$this->data = compact('task', 'publisher', 'winnerWork', 'winner');
		
		return $this->showView('task.agreement');
	}
	
	/**
	 * 提交比赛发布请求
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postPublish() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$common = $this->getParamItemPublishCommon ();
			
			$submitWorkEndTime = $this->getParamDate ( self::PARAM_SUBMIT_WORK_END_TIME, '', true );
			
			$this->checkEndTimeLength($submitWorkEndTime, self::PARAM_SUBMIT_WORK_END_TIME, array('bigger' => 'error_submit_work_end_time_invalid', 'small' => 'error_submit_work_end_time_invalid'));
			
			$awards = $this->getParam ( 'bonus_amount', 'required{error_bonus_amount_empty}|maxLength[120]{error_bonus_amount_too_big}' );
			
			$tagsNames = $this->getParam ( 'tags' );
			if ($tagsNames) {
				$tagsNames = $this->checkTags ( $tagsNames );
			}

			$this->outputParamErrorIfExist ();
			
			$taskId = TaskModule::publish ( $common, $awards, $submitWorkEndTime, $tagsNames, $this->getLoginUserId () );
			
			return $this->outputContent ($taskId);
		} );
	}
	
	public function postUpdateTask() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
				
			$common = $this->getParamItemPublishCommon ();

			$taskId = $this->getParam('task_id', 'required|natrueNumber');
		
			$submitWorkEndTime = $this->getParamDate ( self::PARAM_SUBMIT_WORK_END_TIME, '', true );
				
			$this->checkEndTimeLength($submitWorkEndTime, self::PARAM_SUBMIT_WORK_END_TIME, array('bigger' => 'error_submit_work_end_time_invalid', 'small' => 'error_submit_work_end_time_invalid'));
				
			$awards = $this->getParam ( 'bonus_amount', 'required{error_bonus_amount_empty}|maxLength[120]{error_bonus_amount_too_big}' );
				
			$tagsNames = $this->getParam ( 'tags' );
			if ($tagsNames) {
				$tagsNames = $this->checkTags ( $tagsNames );
			}

			$this->outputParamErrorIfExist ();
				
			$taskId = TaskModule::updateTask($common, $awards, $workEndTime, $this->getLoginUserId(), $taskId, $tagsNames);
				
			return $this->outputContent ($taskId);
		} );
	}
	
	/**
	 * 提交任务比赛参赛接口
	 * 
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postSubmitWork() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$taskId = $this->getParam ( 'task_id', 'required|natureNumber' );
			$works = $this->getParam ( 'works', 'required{error_work_works_empty}' );
			$cover = $this->getParam ( 'imagefile', 'required{error_work_cover_empty}' );
			$endTime = $this->getParam ( 'end_time' );
			
			if ($endTime === '0') {
				$this->setParamError('end_time', 'error_submit_work_end_time_too_short');
			}
			
			if ($endTime) {
				if ($endTime > 9999) {
					$this->setParamError('end_time', 'error_submit_work_end_time_too_long');
				} else if ($endTime < 1) {
					$this->setParamError('end_time', 'error_submit_work_end_time_too_short');
				}
			}
			
			$this->outputParamErrorIfExist ();
			
			$workId = TaskModule::submitWork ( $taskId, $cover, $works, $endTime, $this->getLoginUserId () );
			
			return $this->outputContent ($workId);
		} );
	}
	
	/**
	 * 提交发布动态请求
	 * 
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postUpdate() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$taskId = $this->getParam ( 'contest_id', 'required|natureNumber' );
			$title = $this->getParam ( 'title', 'required{error_task_update_title_empty}|maxLength[60]{error_task_update_title_too_long}' );
			$content = $this->getParam ( 'content', 'required{error_task_update_content_empty}|maxlength[1000]{error_update_content_too_big}|minLength[1]{error_update_content_too_short}|line[15]{error_update_content_too_many_line}' );	
			
			$this->outputParamErrorIfExist ();
			
			$content = $this->replaceLine($content);
			
			TaskModule::createTaskUpdate ( $taskId, $this->getLoginUserId (), $title, $content );
			
			return $this->outputContent ();
		} );
	}
	
	/**
	 * 提交撤回作品请求
	 * 
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getCancelWork($workId) {
		if (!$this->isLogin()) {
			return $this->requireLogin ();
		}
		
		$work = TaskModule::getTaskWorkById ( $workId );
		
		if (! $work) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
		
		TaskModule::cancelWork ( $workId, $this->getLoginUserId () );
		
		return \Redirect::to ( \URL::to ( '/' ) . '/task/detail/' . $work->task_id );
	}
	
	/**
	 * 提交修改作品请求
	 * 
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postUpdateWork() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$workId = $this->getParam ( 'work_id', 'required|natureNumber' );
			$works = $this->getParam ( 'works', 'required{error_work_works_empty}' );
			$cover = $this->getParam ( 'imagefile', 'required{error_work_cover_empty}' );
			$endTime = $this->getParam ( 'end_time' );
			
			$this->outputParamErrorIfExist ();
			
			$workInfo = compact ( 'works', 'cover', 'endTime' );
			
			TaskModule::updateWork ( $workId, $this->getLoginUserId (), $workInfo );
			
			return $this->outputContent ($workId);
		} );
	}
	
	/**
	 * 提交选择获胜作品请求
	 * 
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getChooseWinner($workId) {
		if (!$this->isLogin()) {
			return $this->requireLogin ();
		}
		
		$work = TaskModule::getTaskWorkById ( $workId );
		
		if (! $work) {
			return \Redirect::to ( \URL::to ( '/' ) . '/task/index' );
		}
			
		TaskModule::chooseWinner ( $workId, $this->getLoginUserId () );
		
		return \Redirect::to ( \URL::to ( '/' ) . '/task/detail/' . $work->task_id );
	}
	
	protected function getItem($id) {
		return TaskModule::getTaskById ( $id );
	}
	
	/**
	 * 填充专家点评对象用户信息
	 *
	 * @param array $expertComments        	
	 */
	protected function fillTargetUserInfo(&$expertComments, $targetSubId = 0) {
		if ($targetSubId) {
			$work = TaskModule::getTaskWorkById ( $targetSubId );
			$user = UserModule::getUserById ( $work->user_id );
			foreach ( $expertComments as $expertComment ) {
				$expertComment->target_user = $user;
			}
		} else {
			$workIds = array ();
			foreach ( $expertComments as $expertComment ) {
				$workIds [] = $expertComment->target_sub_id;
			}
			
			
			$works = TaskModule::getWorksByIds ( $workIds );
			UserModule::fillUsers ( $works );
			
			foreach ( $expertComments as $expertComment ) {
				foreach ( $works as $work ) {
					if ($expertComment->target_sub_id == $work->id) {
						$expertComment->target_user = $work->user;
					}
				}
			}
		}
	}
}