<?php

namespace App\Controllers;

use App\Modules\ExpertModule;
use App\Modules\EncyclopediaModule;
use App\Modules\IndustryModule;
use App\Modules\ItemModule;
use App\Modules\UserModule;
use App\Common\Page;

/**
 * 创意世界智库控制器
 *
 * 处理创意世界智库功能，如查看智库专家、申请成为智库专家等。
 */
class ThinktankController extends BaseController {
    
    protected $targetTypeString = 'thinktank';

    /**
     * 显示智库首页
     * 
     * @return \Illuminate\View\View
     */
    public function getIndex($page = 0) {
        
        $limit = ExpertModule::USER_NUMBER_SIZE;
        $offset = ($page - 1) * $limit;
        
        $recommendIndustries = IndustryModule::getRecommendIndustriesAndExperts();

        $expertsCount = ExpertModule::countexperts();
        $pageHtml = Page::geneAjaxPageHtml($page,ceil($expertsCount/$limit));
       
        $chooseExperts = ExpertModule::getExperts($offset, ExpertModule::USER_NUMBER_SIZE);
        $industries  = IndustryModule::getFirstIndustries();
        $this->data = compact('recommendIndustries', 'expertsCount', 'chooseExperts','industries', 'pageHtml');
        return $this->showView('thinktank.index');
    }

    /**
	 * 显示申请加入智库页
	 * 
	 * @return \Illuminate\View\View
	 */
    public function getApplyCandidate() {
        
        if ($this->isLogin()) {
            return $this->showView('thinktank.apply');
        }else {
            
            return $this->showView('user.login');
        }
    }

    /**
	 * 认证用户信息
	 * 
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function postIdentify() {
        return $this->execute(function()
        {
            $this->requireAjaxLogin();
            
            $name = $this->getParam('name', 'required{error_name_empty}|character{error_name_illegal}');
            $IdCardNumber = $this->getParam('id_card_number', 'required{error_id_card_empty}');
            
            if (! $this->hasParamError ( 'id_card_number' )) {
                $result = ExpertModule::isCreditNo($IdCardNumber);
                if (!$result) {
                    $this->setParamError('id_card_number', 'error_id_card_number');
                }
            }
            
            $this->outputParamErrorIfExist();
            $result = ExpertModule::identify($this->getLoginUserId(), $name, $IdCardNumber);
            return $this->outputContent($result);
        });
    }
    
    /**
     * 上传申请资料
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postApplyCandidate() {
        return $this->execute(function() {
            
            $this->requireAjaxLogin();
            $candidateInfo['industries'] = $this->getParam('industries', 'required{error_industry_empty}');
            $candidateInfo['enterprise_name'] = $this->getParam('enterprise_name', 'required{error_enterprise_name_empty}');
            $candidateInfo['enterprise_duty'] = $this->getParam('enterprise_duty', 'required{error_enterprise_duty_empty}');
            $ids = $this->getParam('organization_duty_id', 'required{error_organization_duty_empty}');
            $candidateInfo['attachments'] = $this->getParam('imagefile', 'required{error_please_upload_certification_data}');
            $candidateInfo['explain'] = $this->getParam('explain', 'required{error_explain_empty}');
            
            $this->outputParamErrorIfExist();
            
            $ids = explode("|",$ids);
            
            $candidateInfo['organization_id'] = $ids[0];
            $candidateInfo['duty_id'] = $ids[1];
            $result = ExpertModule::applyExpert($this->getLoginUserId(), $candidateInfo);
            
            return $this->outputContent($result);
        });
    }
    
    /**
     * 按条件搜索专家
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function postSearchExperts() {
        return $this->execute(function() {
            
            $areaId = $this->getParam('area_id');
            if (!$areaId) {
                $provinceId = 0;
                $cityId = 0;
                $districtId = 0;
            }else {
                $areaIds = explode('|',$areaId);
                $provinceId = $areaIds[0];
                if ($areaIds[1] == '00') {
                    $cityId = 0;
                    $districtId = 0;
                }elseif ($areaIds[2] == '00') {
                    $cityId =  $areaIds[0].$areaIds[1];
                    $districtId = 0;
                }else {
                    $cityId =  $areaIds[0].$areaIds[1];
                    $districtId =  $areaIds[0].$areaIds[1].$areaIds[2];
                }
            }
            $organizationId = $this->getParam('expert_organization_id');
            if (!$organizationId) {
                $organizationId = 0;
            }
            $dutyId = $this->getParam('expert_duty_id');
            if (!$dutyId) {
                $dutyId = 0;
            }
            $industryId = $this->getParam('expert_industry_id');
            if (!$industryId) {
                $industryId = 0;
            }
            $fieldId = $this->getParam('expert_filed_id');
            if (!$fieldId) {
                $fieldId = 0;
            }
            
            $page = $this->getParam('page');
            if (!$page) {
                $page = 1;
            }
            
            $limit = ExpertModule::USER_NUMBER_SIZE;
            $offset = ($page - 1) * $limit;
            
            $chooseExperts = ExpertModule::getExpertsByCondition($provinceId, $cityId, $districtId, $organizationId, $dutyId, $fieldId, $industryId, $offset);
            $expertsCount = ExpertModule::getExpertsByConditionCount($provinceId, $cityId, $districtId, $organizationId, $dutyId, $fieldId, $industryId);
            $pageHtml = Page::geneAjaxPageHtml($page,ceil($expertsCount/$limit));
            
            $this->data = compact('chooseExperts', 'pageHtml');
            
            $result = $this->showView ( 'thinktank.m-experts' )->__toString ();
            return $this->outputContent ( $result );
        });
    }
    
    /**
     * 智库引导页
     * 
     * @return \Illuminate\View\View
     */
    public function getGuide() {
        $this->data['guideExperts'] = ExpertModule::getExpertsByRandom();
        
        return $this->showView('thinktank.guide');
    }
    
    //推荐专家
    public function getInviteExpert(){
    	return $this->showView('thinktank.invite-expert');
    }
    
}