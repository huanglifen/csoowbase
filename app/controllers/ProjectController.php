<?php

namespace App\Controllers;

use App\Modules\ProjectModule;
use App\Modules\TagModule;
use App\Modules\BaseModule;
use App\Modules\UserModule;
use App\Modules\ItemModule;
use App\Modules\CommentModule;
use App\Modules\ExpertCommentModule;
use App\Modules\CreativeValueRatingModule;
use App\Modules\LikeModule;
use App\Modules\ReportModule;
use App\Modules\CircleModule;
use App\Common\Page;
use App\Models\ProjectInvestments;
/**
 * 创意投资项目比赛控制器
 *
 * 处理创意投资项目比赛功能，如发布项目，提交投资意向等。
 */
class ProjectController extends ItemController {
	protected $targetTypeString = 'project';
	const PARAM_INVESTMENT_END_TIME = 'investment_end_time';
	const PROJECT_LIST_NUMBER = 12;
	
	/**
	 * 显示比赛列表页
	 * 
	 *
	 * @return \Illuminate\View\View
	 */
	public function getIndex($status = 0) {
		$lists = ProjectModule::getListsByStatus ( $status, 0, BaseModule::ITEM_NUMBER_INDEX);
		UserModule::fillUsers ( $lists );
		
		$tags = TagModule::getHotTags ( BaseModule::TYPE_PROJECT, BaseModule::HOT_TAG_NUMBER );
		
		$ranks = ProjectModule::getProjectRank ();
		UserModule::fillUsers ( $ranks );
		
		$total = ProjectModule::getTotalByTagIdAndStatus ( 0, $status );
		$pageTotal = ceil ( $total / BaseModule::ITEM_NUMBER_INDEX );
		
		
		$this->data = compact ( 'status','pageTotal', 'lists', 'tags', 'ranks' );
		return $this->showView ( 'project.list' );
	}
	
	/**
	 * 显示比赛发布页
	 *
	 * @return \Illuminate\View\View
	 */
	public function getPublish($projectId = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		if ($projectId) {
			$project = $this->checkItem($projectId);
			$project = ItemModule::fillContests([$project]);
			$this->data = compact('project');
			//echo '<pre>';print_r($this->data['project']);exit;
		}
				
		return $this->showView ( 'project.publish' );
	}
	
	/**
	 * 查看比赛协议
	 * @param unknown $projectId
	 */
	public function getAgreement($projectId) {
		$project = $this->checkItem($projectId);
		
		$publisher = UserModule::getUserById($project->user_id);
		$invest = ProjectModule::getProjectInvestors($projectId, ProjectModule::INVEST_STATUS_ACCEPT);
		
		$investor = UserModule::getUserById($invest->user_id);
		
		$this->data = compact('project', 'publisher', 'investor', 'invest');

		return $this->showView('project.agreement');
	}
	
	/**
	 * 提交比赛发布请求
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postPublish() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$common = $this->getParamItemPublishCommon ();
			
			$investmentEndTime = $this->getParamDate ( self::PARAM_INVESTMENT_END_TIME, '', true );
			
			$this->checkEndTimeLength($investmentEndTime, self::PARAM_INVESTMENT_END_TIME, array('bigger' => 'error_investment_end_time_invalid', 'small' => 'error_investment_end_time_invalid'));
			
			$goalAmount = $this->getParam ( 'goal_amount', 'required{error_goal_amount_empty}|natureNumber{error_goal_amount_not_integer}|minLength[0]{error_goal_amount_is_zero}' );
			
			if ($goalAmount > 1000000000) {
				$this->setParamError('goal_amount', 'error_goal_amount_too_big');
			}
			
			$tagsNames = $this->getParam ( 'tags' );
			if ($tagsNames) {
				$tagsNames = $this->checkTags ( $tagsNames );
			}
			
			$this->outputParamErrorIfExist ();
			
			$projectId = ProjectModule::publish ( $common, $this->getLoginUserId (), $investmentEndTime, $goalAmount, $tagsNames );
			
			return $this->outputContent ( $projectId );
		} );
	}
	
	/**
	 * 修改投资项目比赛接口
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postUpdateProject() {
		return $this->execute(function() {
			$this->requireAjaxLogin ();
				
			$common = $this->getParamItemPublishCommon ();
				
			$investmentEndTime = $this->getParamDate ( self::PARAM_INVESTMENT_END_TIME, '', true );
				
			$this->checkEndTimeLength($investmentEndTime, self::PARAM_INVESTMENT_END_TIME, array('bigger' => 'error_investment_end_time_invalid', 'small' => 'error_investment_end_time_invalid'));
				
			$projectId = $this->getParam('project_id', 'required|natureNumber');
			$goalAmount = $this->getParam ( 'goal_amount', 'required{error_goal_amount_empty}|natureNumber{error_goal_amount_not_integer}|minLength[0]{error_goal_amount_is_zero}' );
				
			if ($goalAmount > 1000000000) {
				$this->setParamError('goal_amount', 'error_goal_amount_too_big');
			}
				
			$tagsNames = $this->getParam ( 'tags' );
			if ($tagsNames) {
				$tagsNames = $this->checkTags ( $tagsNames );
			}
				
			$this->outputParamErrorIfExist ();
			
			$projectId = ProjectModule::updateProject($common, $projectId, $this->getLoginUserId(), $investmentEndTime, $goalAmount, $tagsNames);
			
			return $this->outputContent($projectId);
		});
	}
	
	/**
	 * 获取投资比赛详情页公共部分
	 *
	 * @param int $id        	
	 * @param int $type        	
	 * @return multitype:
	 */
	protected function getCommon($id, $type,$page = 1) {
		$project = $this->checkItem ( $id );	

		$targetId = $id;
		$targetType = BaseModule::TYPE_PROJECT;
		
		$expertCommentNum = ItemModule::getExpertCommentsNum ($targetType, $id );
		$investNum = ProjectModule::getInvestNumByProjectId ( $id );
		
		$winner = ProjectModule::getProjectInvestors ( $id, ProjectModule::INVEST_STATUS_ACCEPT );
		UserModule::fillUser ( $winner );
		CircleModule::fillIsFollow($winner, $this->getLoginUserId());
		
		$isPublisher = false;
		$isWinner = false;
		
		$userId = $this->getLoginUserId ();
		if ($project->user_id == $userId) {
			$isPublisher = true;
		} elseif (! empty ( $winner ) && $winner->user->id == $userId) {
			$isWinner = true;
		}
		
		$canApply = $this->canInvest ( $id, $userId, $project );
		
		$canExpertComment = $this->canExpertComment ( $userId,BaseModule::TYPE_PROJECT,$id, $project);
		
		$limit =  BaseModule::COMMENT_PAGE_SIZE ;
		$offset = ($page - 1) * $limit;
		$comments = ItemModule::getComments ($targetType, $id, $offset,$limit);
		UserModule::fillUsers ( $comments );
		$commentNum = CommentModule::getCommentsCount ($targetType, $id );
		$pageHtml = '';
		if ($commentNum > $limit) {
			$pageTotal = ceil ( $commentNum / $limit );
			switch ($type){
				case 1:$tpl='detail';break;
				case 2:$tpl='info';break;
				case 3:$tpl='expert';break;
				case 4:$tpl='invest';break;
				case 5:$tpl='updates';break;
				case 6:$tpl='update';break;
				default:$tpl='detail';break;
			}
			$url = \URL::to ( '/' ) . '/project/'.$tpl.'/' . $id;
			$pageHtml = Page::genePageHtml ( $url, $page, $pageTotal ,'#comments');
		}
		
		$creativeValueRating = 0;
		if ($userId) {
			$creativeValueRating = CreativeValueRatingModule::getCreativeVauleRating ( $userId, $targetType, $targetId );
		}
		
		$isReported = ReportModule::isExist($userId, $targetType, $targetId);
		
		$isLiked = LikeModule::isLiked ( $userId, $targetType, $targetId );
		
		return compact ( 'type', 'project', 'expertCommentNum', 'canExpertComment', 'investNum', 'winner', 'canApply', 'isPublisher', 'isWinner', 'comments', 'commentNum','pageHtml', 'targetId', 'targetType', 'creativeValueRating', 'isLiked','isReported' );
	}
	
	/**
	 * 显示比赛详情页
	 *
	 * @param int $id
	 *        	比赛ID
	 * @return \Illuminate\View\View
	 */
	public function getDetail($id = 0,$page = 1) {
		$common = $this->getCommon ( $id, 1 ,$page);
	
		$invests = ProjectModule::getInvestsByProjectId ( $id );
		UserModule::fillUsers ( $invests );
		CircleModule::fillIsFollows($invests, $this->getLoginUserId());
		
		$updates = ProjectModule::getUpdatesByProjectId ( $id );
		
		$expertComments = ItemModule::getExpertComments ( ItemModule::TYPE_PROJECT, $id, 0, 0, BaseModule::COMMENT_PAGE_SIZE );
		UserModule::fillUsers ( $expertComments );
		
		$this->fillTargetUserInfo ( $expertComments, $id );
		
		$this->data = compact ( 'invests', 'updates', 'expertComments' );
		$this->data = array_merge ( $common, $this->data );
		
		return $this->showView ( 'project.detail' );
	}
	
	/**
	 * 显示项目详情页
	 *
	 * @param number $id        	
	 * @return \Illuminate\View\View
	 */
	public function getInfo($id = 0,$page = 1) {
		$common = $this->getCommon ( $id, 2 ,$page);
		
		$this->data = $common;
		
		return $this->showView ( 'project.detail' );
	}
	
	/**
	 * 显示专家点评页面
	 *
	 * @param number $id        	
	 * @return \Illuminate\View\View
	 */
	public function getExpert($id = 0,$page = 1) {
		$common = $this->getCommon ( $id, 3 ,$page);
		
		$expertComments = ItemModule::getExpertComments ( ItemModule::TYPE_PROJECT, $id, 0, 0, BaseModule::COMMENT_PAGE_SIZE );
		UserModule::fillUsers ( $expertComments );
		$this->fillTargetUserInfo ( $expertComments, $id );
		
		$this->data = compact ( 'expertComments' );
		$this->data = array_merge ( $common, $this->data );
		
		return $this->showView ( 'project.detail' );
	}
	
	/**
	 * 显示投资者页面
	 * 
	 * @param number $id        	
	 * @return \Illuminate\View\View
	 */
	public function getInvest($id = 0,$page = 1) {
		$common = $this->getCommon ( $id, 4 ,$page);
		
		$invests = ProjectModule::getInvestsByProjectId ( $id );
		UserModule::fillUsers ( $invests );
		CircleModule::fillIsFollows($invests, $this->getLoginUserId());

		ProjectModule::sortInvestors ( $invests, $id, $common ['isPublisher'], $common ['winner'], $this->getLoginUserId () );
		
		$this->data = compact ( 'invests' );
		$this->data = array_merge ( $common, $this->data );

		return $this->showView ( 'project.detail' );
	}
	/**
	 * 显示项目动态页面
	 *
	 * @param number $id        	
	 * @return \Illuminate\View\View
	 */
	public function getUpdates($id = 0,$page = 1) {
		$common = $this->getCommon ( $id, 5,$page);
		
		$updates = ProjectModule::getUpdatesByProjectId ( $id );
		
		$comments = ItemModule::getComments ( ItemModule::TYPE_PROJECT, $id, 0, BaseModule::COMMENT_PAGE_SIZE );
		UserModule::fillUsers ( $comments );
		
		$commentNum = CommentModule::getCommentsCount ( ItemModule::TYPE_PROJECT, $id );
		$targetId = $id;
		$targetType = BaseModule::TYPE_PROJECT;
		$this->data = compact ( 'commentNum', 'updates', 'comments', 'targetId', 'targetType' );
		$this->data = array_merge ( $common, $this->data );
		
		return $this->showView ( 'project.detail' );
	}
	
	/**
	 * 显示单独更新页
	 * @param number $id
	 * @param number $updateId
	 * @param number $page
	 * @return \Illuminate\View\View
	 */
	public function getUpdate($id = 0, $updateId = 0, $page = 1) {
		$common = $this->getCommon($id, 6, $page);
		$update = ItemModule::getUpdateById($updateId);
	
		$comments = ItemModule::getComments ( ItemModule::TYPE_PROJECT, $id, 0, BaseModule::COMMENT_PAGE_SIZE );
		UserModule::fillUsers ( $comments );
	
		$commentNum = CommentModule::getCommentsCount ( ItemModule::TYPE_PROJECT, $id );
		$targetId = $id;
		$targetType = BaseModule::TYPE_PROJECT;
		$this->data = compact ( 'commentNum', 'update', 'comments', 'targetId', 'targetType' );
		$this->data = array_merge ( $common, $this->data );
	
		return $this->showView('project.detail');
	}
	
	/**
	 * 显示我要投资页面
	 *
	 * @param unknown $id        	
	 * @return \Illuminate\Http\RedirectResponse \Illuminate\View\View
	 */
	public function getApply($projectId = 0, $investId = 0) {
		if (! $this->isLogin ()) {
			return $this->requireLogin ();
		}
		
		if (!$this->checkItem($projectId)) {
			return \Redirect::to ( \URL::to ( '/' ) . '/project/index' );
		}
		
		$project = $this->getItem($projectId);
		
		$invest = array();
		if ($investId) {
			$invest = ProjectModule::getProjectInvestById($investId);
		}
		
		$publisher = UserModule::getUserById($project->user_id);
		
		$this->data = compact('project', 'projectId', 'invest','investId', 'publisher');

		return $this->showView ( 'project.join' );
	}
	
	/**
	 * 提交发布动态请求
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postUpdate() {
		return $this->execute ( function () {
			$projectId = $this->getParam ( 'contest_id', 'required|natureNumber' );
			$title = $this->getParam ( 'title', 'required{error_update_title_empty}|minLength[2]{error_title_too_short_or_long}|maxLength[60]{error_title_too_short_or_long}' );
			$content = $this->getParam ( 'content', 'required{error_update_content_empty}|maxlength[1000]{error_update_content_too_big}|minLength[1]{error_update_content_too_short}|line[15]{error_update_content_too_many_line}' );
			
			$this->outputParamErrorIfExist ();
			
			$content = $this->replaceLine($content);
			
			$this->requireAjaxLogin ();
			
			ProjectModule::createProjectUpdate ( $projectId, $title, $content, $this->getLoginUserId () );
			
			return $this->outputContent ();
		} );
	}
	
	/**
	 * 提交我要投资请求
	 */
	public function postInvest() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$projectId = $this->getParam ( 'project_id', 'required|natureNumber' );
			$amount = $this->getParam ( 'amount', 'required{error_invest_amount_empty}|natureNumber{error_invest_amount_invalid}|minLength[1]{error_invest_amount_too_small}' );
			$type = $this->getParam ( 'type', 'required{error_invest_type_empty}' );
			
			if ($amount >= 1000000000) {
				$this->setParamError('amount', 'error_invest_amount_too_big');
			}
			
			if ($amount < 1) {
				$this->setParamError('amount', 'error_invest_amount_too_small');
			}
			
			if (! in_array ( $type, array (
					ProjectModule::INVEST_TYPE_STAGES,
					ProjectModule::INVEST_TYPE_TOTAL 
			) )) {
				$type = ProjectModule::INVEST_TYPE_TOTAL;
			}
			
			if ($type == ProjectModule::INVEST_TYPE_STAGES) {
				$depositPercentage = $this->getParam ( 'deposit_percentage', 'required{error_deposit_percentage_empty}|natureNumber{error_deposit_percentage_invalid}' );
				if ($depositPercentage >= 100 || $depositPercentage < 1) {
					$this->setParamError('deposit_percentage', 'error_deposit_percentage_invalid');
				}
				
				$finalPaymentPayTime = $this->getParam ( 'final_pay_time', 'required{error_final_pay_time_empty}|natureNumber{error_final_pay_time_invalid}' );
				
				if ($finalPaymentPayTime === '0') {
					$this->setParamError('final_pay_time', 'error_final_pay_time_invalid');
				}
				
				if ($finalPaymentPayTime) {
				    if ($finalPaymentPayTime > 9999) {
				        $this->setParamError('final_pay_time', 'error_final_pay_time_invalid');
				    } else if ($finalPaymentPayTime <= 3) {
				        $this->setParamError('final_pay_time', 'error_final_pay_time_invalid');
				    }
				}
				
			} else {
				$depositPercentage = 0;
				$finalPaymentPayTime = 0;
			}
			
			$requireCompleteTime = $this->getParam( 'require_complete_time' );
			
			if ($requireCompleteTime === '0') {
				$this->setParamError('require_complete_time', 'error_require_complete_time_invalid');
			}
			
			if ($requireCompleteTime) {
			    if ($requireCompleteTime > 9999) {
			        $this->setParamError('require_complete_time', 'error_require_complete_time_invalid');
			    } else if ($requireCompleteTime < 1) {
			        $this->setParamError('require_complete_time', 'error_require_complete_time_invalid');
			    }
			}else{
			    $invest ['require_complete_time'] = 0;
			}
			
			$requirement = $this->getParam ( 'requirement', 'required{error_invest_requirement_empty}' );
			
			$this->outputParamErrorIfExist ();
			
			$investId = ProjectModule::invest ( $projectId, $this->getLoginUserId (), $amount, $type, $depositPercentage, $finalPaymentPayTime, $requireCompleteTime, $requirement );
			
			return $this->outputContent ( $investId );
		} );
	}
	
	/**
	 * 接受投资接口
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getAcceptInvest($investId) {
		if (!$this->isLogin()) {
			return $this->requireLogin();
		}
		
		$invest = ProjectModule::getProjectInvestById($investId);
		
		if (!$invest) {
			return \Redirect::to(\URL::to('/') . '/project/index');
		}
		
			
		$result = ProjectModule::acceptInvest ( $investId, $this->getLoginUserId () );
			
		return \Redirect::to(\URL::to('/') . '/project/detail/' . $invest->project_id);
	}
	
	/**
	 * 取消投资接口
	 */
	public function getCancelInvest($investId) {
		if (!$this->isLogin()) {
			return $this->requireLogin();
		}
		
		$invest = ProjectModule::getProjectInvestById($investId);
		
		if (!$invest) {
			return \Redirect::to(\URL::to('/') . '/project/index');
		}

		$result = ProjectModule::cancelInvest ( $investId, $this->getLoginUserId () );
		
		return \Redirect::to(\URL::to('/') . '/project/detail/' . $invest->project_id);
	}
	
	/**
	 * 支付定金接口
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postDeposit() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$investId = $this->getParam ( 'invest_id', 'required|natureNumber' );
			
			$this->outputParamErrorIfExist ();
			
			$result = ProjectModule::payDeposit ( $investId, $this->getLoginUserId () );
			
			return $this->outputContent ();
		} );
	}
	
	/**
	 * 支付尾款
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postPayFinal() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$investId = $this->getParam ( 'invest_id', 'required|natureNumber' );
			
			$this->outputParamErrorIfExist ();
			
			$result = ProjectModule::payFinal ( $investId, $this->getLoginUserId () );
			
			return $this->outputContent ();
		} );
	}
	
	/**
	 * 支付全款
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postPayTotal() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$investId = $this->getParam ( 'invest_id', 'required|natureNumber' );
			
			$this->outputParamErrorIfExist ();
			
			$result = ProjectModule::payTotal ( $investId, $this->getLoginUserId () );
			
			return $this->outputContent ();
		} );
	}
	
	/**
	 * 修改投资
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postUpdateInvest() {
		return $this->execute ( function () {
			$this->requireAjaxLogin ();
			
			$investId = $this->getParam ( 'invest_id', 'required|natureNumber' );
			
			$invest ['amount'] = $this->getParam ( 'amount', 'natureNumber{error_invest_amount_invalid}|maxLength[1000000000]{error_invest_amount_too_big}|minLength[1]{error_invest_amount_too_small}' );
			$invest ['type'] = $this->getParam ( 'type' );
			
			if ($invest ['type']) {
				if (! in_array ( $invest ['type'], array (
						ProjectModule::INVEST_TYPE_STAGES,
						ProjectModule::INVEST_TYPE_TOTAL 
				) )) {
					$invest ['type'] = ProjectModule::INVEST_TYPE_TOTAL;
				}
			}
			
			if ($invest ['type'] == ProjectModule::INVEST_TYPE_STAGES) {
				$invest ['deposit_percentage'] = $this->getParam ( 'deposit_percentage', 'required{error_deposit_percentage_empty}|natureNumber{error_deposit_percentage_invalid}|minLength[1]{error_deposit_percentage_invalid}|maxLength[99]{error_deposit_percentage_invalid}' );
				$invest ['final_payment_pay_time'] = $this->getParam ( 'final_pay_time', 'required{error_final_pay_time_empty}|natureNumber{error_final_pay_time_invalid}' );
				
				if ($invest ['final_payment_pay_time']) {
				    if ($invest ['final_payment_pay_time'] > 9999) {
				        $this->setParamError('final_pay_time', 'error_final_pay_time_invalid');
				    } else if ($invest ['final_payment_pay_time'] < 1) {
				        $this->setParamError('final_pay_time', 'error_final_pay_time_invalid');
				    }
				}
			} else {
				$invest ['deposit_percentage'] = 0;
				$invest ['final_payment_pay_time'] = 0;
			}
			
			$invest ['require_complete_time'] = $this->getParam( 'require_complete_time' );
			
			if ($invest ['require_complete_time']) {
			    if ($invest ['require_complete_time'] > 9999) {
			        $this->setParamError('require_complete_time', 'error_require_complete_time_invalid');
			    } else if ($invest ['require_complete_time'] < 1) {
			        $this->setParamError('require_complete_time', 'error_require_complete_time_invalid');
			    }
			}else{
			    $invest ['require_complete_time'] = 0;
			}
			
			$invest ['requirement'] = $this->getParam ( 'requirement' );
			
			$this->outputParamErrorIfExist ();
			
			$result = ProjectModule::updateInvest ( $investId, $this->getLoginUserId (), $invest );
			
			return $this->outputContent ($investId);
		} );
	}
	protected function getItem($id) {
		$project = ProjectModule::getProjectById ( $id );
		if (! $project) {
			return $project;
		}
		if ($project->type != BaseModule::TYPE_PROJECT) {
			return false;
		}
		
		if ($project->status == ProjectModule::STATUS_WAITING) {
			return false;
		}
		return $project;
	}
	
	/**
	 * 填充专家点评对象用户信息
	 *
	 * @param array $expertComments        	
	 */
	protected function fillTargetUserInfo(&$expertComments, $targetId, $targetSubId = 0) {
		$trade = $this->getItem ( $targetId );
		
		if (! empty ( $trade )) {
			$user = UserModule::getUserById ( $trade->user_id );
			
			foreach ( $expertComments as $expertComment ) {
				$expertComment->target_user = $user;
			}
		}
	}
	
	
	
	/**
	 * 判断用户是否可以参赛
	 *
	 * @param int $id        	
	 * @param int $userId        	
	 * @param string $project        	
	 * @return boolean
	 */
	protected function canInvest($id, $userId, $project = false) {
		if ($project->status != ItemModule::STATUS_GOING) {
			return false;
		}
				
		if ($userId == 0) {
			return true;
		}
		
		$isInvested = ProjectInvestments::where('project_id', $id)->where('user_id', $userId)->whereIn('status', array(ProjectModule::INVEST_STATUS_ACCEPT, ProjectModule::INVEST_STATUS_SUBMIT, ProjectModule::INVEST_STATUS_DEPOSIT, ProjectModule::INVEST_STATUS_PAY_FINAL))->first();
		
		if ($isInvested) {
			return false;
		}
		
		if (! $project) {
			$project = ProjectModule::getProjectById ( $id );
		}
		
		if ($project->user_id == $userId) {
			return false;
		}
		
		$user = UserModule::getUserById ( $userId );
				
		$hasCommented = ExpertCommentModule::hasCommented ( BaseModule::TYPE_PROJECT, $userId, $id );
		if ($hasCommented) {
			return false;
		}
		
		if ($user->group_id != UserModule::GROUP_EXPERT) {
			return true;
		}
			
		return true;
	}
}