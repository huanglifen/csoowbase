<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Modules\ProjectModule;
use App\Modules\TaskModule;
use App\Modules\HiringModule;
use App\Modules\ExchangeModule;
use App\Modules\NotificationModule;

class CronCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cron';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Execute cron task';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
		NotificationModule::listenEvent();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		ProjectModule::processExpiration();
		TaskModule::processExpiration();
		HiringModule::processExpiration();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}