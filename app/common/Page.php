<?php

namespace App\Common;

class Page {
	
	/**
	 * 分页组件
	 *
	 * @param 链接地址 $url        	
	 * @param 当前页码 $cur        	
	 * @param 总页码 $total        	
	 * @return string
	 */
	public static function genePageHtml($url, $cur = 1, $total = 1,$addOn = '') {
	    if($total == 0) {
	        return '';
	    }
		if ($cur < 1) {
			$cur = 1;
		} elseif ($cur > $total) {
			$cur = $total;
		}
		
		if ($total > 10) {
			$min = ($cur - 5) >= 1 ? ($cur - 5) : 1;
			$max = ($cur + 5) <= $total ? ($cur + 5) : $total;
			$max = ($max < 10) ? 10 : $max;
			$min = ($max - 10) < $min ? ($max - 9) : $min;
		} else {
			$min = 1;
			$max = $total;
		}
		
		$pageLast = $page = $pageNext = $disableLast = $disableNext = '';
		
		$last = $cur - 1;
		$pageLast = $url . '/' . $last.$addOn;
		
		$next = $cur + 1;
		$pageNext = $url . '/' . $next.$addOn;
		
		if ($cur == 1) {
			$disableLast = 'disable';
			$pageLast = 'javascript:';
		}
		
		if ($cur == $total) {
			$disableNext = 'disable';
			$pageNext = 'javascript:;';
		}
		
		for($i = $min; $i < $cur; $i ++) {
			$page .= '<a href="' . $url . '/' . $i .$addOn. '" class="num">' . $i . '</a>';
		}
		
		$page .= '<a href="' . $url . '/' . $cur . $addOn.'" class="num current">' . $cur . '</a>';
		
		for($i = $cur + 1; $i <= $max; $i ++) {
			$page .= '<a href="' . $url . '/' . $i .$addOn. '" class="num">' . $i . '</a>';
		}
		
		$html = '<div style="margin-top: 5px" class="paging Js_page">
	                    <a href="' . $pageLast . '" class="PgUp ' . $disableLast . '">上一页</a>
                        <span class="PgNum">' . $page . '</span>
	                    <a href="' . $pageNext . '" class="PgDn ' . $disableNext . '">下一页</a>
	                </div>';
		
		return $html;
	}
	
	/**
	 * ajax分页组件
	 * @param int $cur
	 * @param  int $total
	 * @return string
	 */
	public static function geneAjaxPageHtml($cur = 1, $total = 1) {
	    if($total == 0) {
	        return '';
	    }
	    if ($cur < 1) {
	        $cur = 1;
	    } elseif ($cur > $total) {
	        $cur = $total;
	    }
	
	    if ($total > 10) {
	        $min = ($cur - 5) >= 1 ? ($cur - 5) : 1;
	        $max = ($cur + 5) <= $total ? ($cur + 5) : $total;
	        $max = ($max < 10) ? 10 : $max;
	        $min = ($max - 10) < $min ? ($max - 9) : $min;
	    } else {
	        $min = 1;
	        $max = $total;
	    }
	
	    $pageLast = $page = $pageNext = $disableLast = $disableNext = '';
	    $flagLast = 'Js_paging';
	    $flagNext = 'Js_paging';
	    $last = $cur - 1;
	
	    $next = $cur + 1;
	
	    if ($cur == 1) {
	        $disableLast = 'disable';
	        $flagLast = '';
	    }
	
	    if ($cur == $total) {
	        $disableNext = 'disable';
	        $flagNext = '';
	    }
	
	    for($i = $min; $i < $cur; $i ++) {
	        $page .= '<a href="javascript:" class="num Js_paging" data_id="'.$i.'">' . $i . '</a>';
	    }
	
	    $page .= '<a href="javascript:" class="num current Js_paging" data_id="'.$cur.'">' . $cur . '</a>';
	
	    for($i = $cur + 1; $i <= $max; $i ++) {
	        $page .= '<a href="javascript:" class="num Js_paging" data_id="'.$i.'">' . $i . '</a>';
	    }
	
	    $html = '<div style="margin-top: 5px" class="paging Js_page">
	                    <a href="javascript:;" class="PgUp ' . $flagLast.' '.$disableLast . '" data_id="'.$last.'">上一页</a>
                        <span class="PgNum">' . $page . '</span>
	                    <a href="javascript:;" class="PgDn ' . $flagNext.' '.$disableNext . '" data_id="'.$next.'">下一页</a>
	                </div>';
	
	    return $html;
	}
	/**
	 * 获取分页最小页和最大页
	 * （动态评论那是交互不是链接，没法用上面那个）
	 * 
	 * @param number $cur
	 * @param number $total
	 */
	public static function genePage($cur = 1, $total = 1) {
	    if ($cur < 1) {
	        $cur = 1;
	    } elseif ($cur > $total) {
	        $cur = $total;
	    }
	    
	    if ($total > 10) {
	        $min = ($cur - 5) >= 1 ? ($cur - 5) : 1;
	        $max = ($cur + 5) <= $total ? ($cur + 5) : $total;
	        $max = ($max < 10) ? 10 : $max;
	        $min = ($max - 10) < $min ? ($max - 9) : $min;
	    } else {
	        $min = 1;
	        $max = $total;
	    }
	    
	    return array('min' => $min, 'max' => $max);
	}
}
