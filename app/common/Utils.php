<?php

namespace App\Common;

use App\Modules\NotificationModule;
use App\Modules\BaseModule;
use App\Models\Notification;

/**
 * 工具类
 *
 * 常用工具方法类
 */
class Utils {
	
	/**
	 * Check is nature number and zero
	 *
	 * @param string|int $number        	
	 * @return bool
	 */
	public static function isNatureNumberAndZero($number) {
		return preg_match ( '/^[0-9]+$/', $number ) > 0;
	}
	
	/**
	 * Check is nature number
	 *
	 * @param string|int $number        	
	 * @return bool
	 */
	public static function isNatureNumber($number) {
		return self::isNatureNumberAndZero ( $number ) && ($number != 0);
	}
	
	/**
	 * Check is email address
	 *
	 * @param string $email        	
	 * @return bool
	 */
	public static function isEmail($email) {
		return preg_match ( "/^([a-z0-9\\+_\\-]+)(\\.[a-z0-9\\+_\\-]+)*@([a-z0-9\\-]+\\.)+[a-z]{2,6}$/ix", $email ) > 0;
	}
	
	/**
	 * Check is IP v4 address
	 *
	 * @param string $ip        	
	 * @return bool
	 */
	public static function isIPv4($ip) {
		$ipSegments = explode ( '.', $ip );
		
		if (count ( $ipSegments ) != 4) {
			return false;
		}
		
		if ($ipSegments [0] [0] == '0') {
			return false;
		}
		
		foreach ( $ipSegments as $ipSegment ) {
			if ($ipSegment == '' || preg_match ( "/[^0-9]/", $ipSegment ) || $ipSegment > 255 || strlen ( $ipSegment ) > 3) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Check is date
	 *
	 * @param string $date        	
	 * @return bool
	 */
	public static function isDate($date) {
		return date_create ( $date ) !== false;
	}
	
	/**
	 * Check is phone number
	 *
	 * @param string $phoneNumber        	
	 * @return bool
	 */
	public static function isPhoneNumber($phoneNumber) {
		//return preg_match ( "/^([0-9 ()-])+$/", $phoneNumber ) > 0;
		return preg_match("/1[3458]{1}\d{9}$/",$phoneNumber) > 0;
	}
	
	/**
	 * Check is ZIP code
	 *
	 * @param string $code        	
	 * @return bool
	 */
	public static function isZipCode($code) {
		return (strlen ( $code ) == 6) && (preg_match ( "/^([0-9]{6})$/", $code ));
	}
	
	/**
	 * Create directory if not exist
	 *
	 * @param string $dir        	
	 */
	public static function createDirectoryIfNotExist($dir) {
		if (! file_exists ( $dir )) {
			mkdir ( $dir, 0777, true );
			chmod ( $dir, 0777 );
		} else if (! is_dir ( $dir )) {
			unlink ( $dir );
			mkdir ( $dir, 0777, true );
			chmod ( $dir, 0777 );
		}
	}
	public static function isTag($tagName) {
		return preg_match ( '/^(?!_|\s\')[A-Za-z0-9\x80-\xff]+$/', $tagName );
	}
	
	/**
	 * 缩放图片
	 *
	 * @param string $filePath        	
	 * @param int $resizeWidth        	
	 * @param int $resizeHeight        	
	 * @return boolean string
	 */
	protected function resizeImage($filePath, $resizeWidth = self::IMAGE_RESIZE_WIDTH, $resizeHeight = self::IMAGE_RESIZE_HEIGHT) {
		if (! file_exists ( $filePath )) {
			return false;
		}
		
		$filePath = $this->adjustPictureOrientation ( $filePath );
		
		$imageSize = getimagesize ( $filePath );
		$imageFormat = $imageSize [2];
		
		$pathinfo = pathinfo ( $filePath );
		
		$imageWidth = $imageSize [0];
		$imageHeight = $imageSize [1];
		
		$imageResizeWidth = floor ( self::IMAGE_RESIZE_WIDTH / self::IMAGE_RESIZE_HEIGHT * $imageHeight );
		if ($imageResizeWidth < $imageWidth) {
			$startY = 0;
			$startX = floor ( ($imageWidth - $imageResizeWidth) / 2 );
			$imageResizeHeight = $imageHeight;
		} else {
			$startX = 0;
			$imageResizeWidth = $imageWidth;
			$imageResizeHeight = floor ( self::IMAGE_RESIZE_HEIGHT / self::IMAGE_RESIZE_WIDTH * $imageWidth );
			$startY = floor ( ($imageHeight - $imageResizeHeight) / 2 );
		}
		
		$resizeImage = imagecreatetruecolor ( $resizeWidth, $resizeHeight );
		$image = imagecreatefromstring ( file_get_contents ( $filePath ) );
		imagecopyresampled ( $resizeImage, $image, 0, 0, $startX, $startY, $resizeWidth, $resizeHeight, $imageResizeWidth, $imageResizeHeight );
		
		$ext = $this->imageFormatExt [$imageFormat];
		list ( $imageExt, $imageMethod ) = explode ( '|', $ext );
		$fileName = $pathinfo ['filename'] . '_s.' . $imageExt;
		$resizeImageFilePath = $pathinfo ['dirname'] . DIRECTORY_SEPARATOR . $fileName;
		
		$imageMethod ( $resizeImage, $resizeImageFilePath );
		imagedestroy ( $resizeImage );
		
		return $fileName;
	}
	
	/**
	 * 缩放图片
	 *
	 * @param string $filePath        	
	 * @return boolean string
	 */
	protected function resizeContentImage($filePath) {
		if (! file_exists ( $filePath )) {
			return false;
		}
		
		$filePath = $this->adjustPictureOrientation ( $filePath );
		
		$imageSize = getimagesize ( $filePath );
		$imageFormat = $imageSize [2];
		
		$pathinfo = pathinfo ( $filePath );
		
		$imageWidth = $imageSize [0];
		$imageHeight = $imageSize [1];
		
		$imageResizeWidth = floor ( self::IMAGE_CONTENT_RESIZE_WIDTH / self::IMAGE_CONTENT_RESIZE_HEIGHT * $imageHeight );
		if ($imageResizeWidth < $imageWidth) {
			$startY = 0;
			$startX = floor ( ($imageWidth - $imageResizeWidth) / 2 );
			$imageResizeHeight = $imageHeight;
		} else {
			$startX = 0;
			$imageResizeWidth = $imageWidth;
			$imageResizeHeight = floor ( self::IMAGE_CONTENT_RESIZE_HEIGHT / self::IMAGE_CONTENT_RESIZE_WIDTH * $imageWidth );
			$startY = floor ( ($imageHeight - $imageResizeHeight) / 2 );
		}
		
		$resizeWidth = self::IMAGE_CONTENT_RESIZE_WIDTH;
		$resizeHeight = self::IMAGE_CONTENT_RESIZE_HEIGHT;
		
		$resizeImage = imagecreatetruecolor ( $resizeWidth, $resizeHeight );
		$image = imagecreatefromstring ( file_get_contents ( $filePath ) );
		imagecopyresampled ( $resizeImage, $image, 0, 0, $startX, $startY, $resizeWidth, $resizeHeight, $imageResizeWidth, $imageResizeHeight );
		
		$ext = $this->imageFormatExt [$imageFormat];
		list ( $imageExt, $imageMethod ) = explode ( '|', $ext );
		$fileName = $pathinfo ['filename'] . '_s.' . $imageExt;
		$resizeImageFilePath = $pathinfo ['dirname'] . DIRECTORY_SEPARATOR . $fileName;
		
		$imageMethod ( $resizeImage, $resizeImageFilePath );
		imagedestroy ( $resizeImage );
		
		return $fileName;
	}
	
	/**
	 * 根据创意指数计算级别
	 *
	 * @param unknown $creativeIndex        	
	 * @return number
	 */
	public static function getCreativeLevel($creativeIndex) {
		$n = strlen ( $creativeIndex );
		$lever = $n * ($n - 1) / 2 + 1 + floor ( (($creativeIndex - pow ( 10, $n - 1 )) / (pow ( 10, $n ) - pow ( 10, $n - 1 )) * $n) );
		
		return $lever;
	}
	
	/**
	 * 获取用户头像
	 *
	 * @param string $avatar        	
	 * @return string
	 */
	public static function getAvatar($avatar) {
		return $avatar ? \URL::to ( '/' ) . '/' . $avatar : \URL::to ( '/' ) . '/images/default.png';
	}
	
	/**
	 * 生成标准时间格式
	 *
	 * @param string $time        	
	 * @return string
	 */
	public static function formatDate($time) {
		return date ( 'Y-m-d', $time );
	}
	public static function formatTime($time) {
		return date ( 'Y-m-d H:i', $time );
	}
	public static function getContestDetailUrl($type, $id) {
		if ($type == BaseModule::TYPE_PROJECT) {
			$tpl = 'project';
		} elseif ($type == BaseModule::TYPE_TASK) {
			$tpl = 'task';
		} elseif ($type == BaseModule::TYPE_HIRING) {
			$tpl = 'hiring';
		} elseif ($type == BaseModule::TYPE_EXCHANGE) {
			$tpl = 'exchange';
		} elseif ($type == BaseModule::TYPE_ENCYCLOPEDIA) {
			$tpl = 'encyclopedia';
		} elseif ($type == BaseModule::TYPE_TASK_WORK) {
			return \URL::to ( '/' ) . '/task/work/' . $id;
		} elseif ($type == BaseModule::TYPE_HIRING_WORK) {
			return \URL::to ( '/' ) . '/hiring/participator/' . $id;
		} else {
			$tpl = 'project';
		}
		return \URL::to ( '/' ) . '/' . $tpl . '/detail/' . $id;
	}
	public static function formatDay($time) {
		return ceil ( $time / 86400 );
	}
	public static function getCreativeValueRatingScoreText($score = 0) {
		if ($score < 0 || $score > 5) {
			$score = 3;
		}
		$scoreTextArr = array (
				'1' => '有一点',
				'2' => '一般',
				'3' => '良好',
				'4' => '优秀',
				'5' => '颠覆性的创新和巨大的价值' 
		);
		
		return $scoreTextArr [$score];
	}
	public static function getRemainTime($time, $format = '还剩D天H小时') {
		if ($time == 0) {
			return '长期';
		}
		
		$remainTime = $time - time ();
		if ($remainTime <= 0) {
			$day = 0;
			$hour = 0;
			$minutes = 0;
			$second = 0;
		} else {
			$day = floor ( $remainTime / 86400 );
			$hour = floor ( ($remainTime - 86400 * $day) / 3600 );
			$minutes = floor ( ($remainTime - 86400 * $day - 3600 * $hour) / 60 );
			$second = $remainTime - 86400 * $day - 3600 * $hour - $minutes * 60;
		}
		if ($day == 0) {
			$format = str_replace ( 'D天', '', $format );
		} else {
			$format = str_replace ( 'D', $day, $format );
		}
		
		$format = str_replace ( 'H', $hour, $format );
		$format = str_replace ( 'M', $minutes, $format );
		$format = str_replace ( 'S', $second, $format );
		return $format;
	}
	public static function getNotificationUrl($targetType, $targetId = 0) {
		$urlArr = array (
				NotificationModule::LINK_PROJECT => 'project/detail/' . $targetId,
				NotificationModule::LINK_PROJECT_AGREEMENT => 'project/agreement/' . $targetId,
				NotificationModule::LINK_PROJECT_DYNAMIC => 'project/update/' . $targetId,
				NotificationModule::LINK_PROJECT_EXPERT => 'project/expert/' . $targetId,
				NotificationModule::LINK_PROJECT_INVESTORS => 'project/invest/' . $targetId,
				NotificationModule::LINK_PROJECT_EDIT=>'project/publish/'.$targetId,
				NotificationModule::LINK_TASK => 'task/detail/' . $targetId,
				NotificationModule::LINK_TASK_AGREEMENT => 'task/agreement/' . $targetId,
				NotificationModule::LINK_TASK_APPLY => 'task/works/' . $targetId,
				NotificationModule::LINK_TASK_DYNAMIC => 'task/updates/' . $targetId,
				NotificationModule::LINK_TASK_WORK => 'task/work/' . $targetId,
				NotificationModule::LINK_TASK_EDIT=>'task/publish/'.$targetId,
				NotificationModule::LINK_HIRING => 'hiring/detail/' . $targetId,
				NotificationModule::LINK_HIRING_APPLY => 'hiring/participators/' . $targetId,
				NotificationModule::LINK_HIRING_DYNAMIC => 'hiring/updates/' . $targetId,
				NotificationModule::LINK_HIRING_WORK => 'hiring/participator/' . $targetId,
				NotificationModule::LINK_HIRING_EDIT => 'hiring/publish/'.$targetId,
				NotificationModule::LINK_EXCHANGE => 'exchange/detail/' . $targetId,
				NotificationModule::LINK_EXCHANGE_EDIT => 'exchange/publish/'.$targetId,
				NotificationModule::LINK_ENCYCLOPEDIA => 'encyclopedia/detail/' . $targetId,
				NotificationModule::LINK_ENCYCLOPEDIA_EDIT => 'encyclopedia/publish/'.$targetId,
				NotificationModule::LINK_USER_DYNAMIC => 'user/home',
				NotificationModule::LINK_CIRCLE_ME => 'circle/index/2',
				NotificationModule::LINK_EXPERT => 'thinktank',
				NotificationModule::LINK_INDEX => '',
				NotificationModule::TYPE_COIN => 'account/coin' 
		);
		
		if (array_key_exists ( $targetType, $urlArr )) {
			$url = $urlArr [$targetType];
			$url = \URL::to ( '/' ) . '/' . $url;
			return $url;
		} else {
			return "#";
		}
	}
	
	/**
	 * 找回密码时邮件加密
	 *
	 * @param unknown $email        	
	 */
	public static function getSecretEmail($email) {

		$emailArray = explode('@',$email);
		$emailStart = $emailArray[0];
		$emailEnd = $emailArray[1];
		$len = strlen ($emailStart);

		if ($len > 2) {
			$emailStart = substr ( $emailStart, 0, 1 ) . str_repeat ( '*', $len - 2 ) . substr ( $emailStart, -1 );
			$email = $emailStart.'@'.$emailEnd;
		}
		return $email;
	}
	/**
	 *
	 * @param unknown $targetType        	
	 */
	public static function getModuleNameByTargetType($targetType) {
		switch ($targetType) {
			case 1 :
				$urlName = 'project';
				break;
			case 21 :
			case 2 :
				$urlName = 'task';
				break;
			case 31 :
			case 3 :
				$urlName = 'hiring';
				break;
			case 4 :
				$urlName = 'exchange';
				break;
			case 5 :
				$urlName = 'encyclopedia';
				break;
			case 7 :
			case 71 :
			case 72 :
			case 73 :
				$urlName = 'area';
				break;
			default :
				;
		}
		return $urlName;
	}
	
	/**
	 * 三大比赛竞猜结束时间的显示
	 * 
	 * @param
	 *        	$endtime
	 *        	
	 * @return bool string
	 */
	public static function getGuessEndtime($endtime, $status = 2) {
		if ($status == 2) {
			if (! $endtime) {
				return '长期';
			} else {
				return date ( 'Y-m-d', $endtime );
			}
		} else {
			return '竞猜已截止';
		}
	}
	
	/**
	 * 字符串截断
	 * 
	 * @param unknown $string        	
	 * @param unknown $length        	
	 * @param string $etc        	
	 * @return string
	 */
	public static function truncate_utf8_string($string, $length, $etc = '...') {
		$result = '';
		
		$string = html_entity_decode ( trim ( strip_tags ( $string ) ), ENT_QUOTES, 'utf-8' );
		
		for($i = 0, $j = 0; $i < strlen ( $string ); $i ++) {
			if ($j >= $length) {
				for($x = 0, $y = 0; $x < strlen ( $etc ); $x ++) {
					if ($number = strpos ( str_pad ( decbin ( ord ( substr ( $string, $i, 1 ) ) ), 8, '0', STR_PAD_LEFT ), '0' )) {
						$x += $number - 1;
						$y ++;
					} else {
						$y += 0.5;
					}
				}
				
				$length -= $y;
				
				break;
			}
			
			if ($number = strpos ( str_pad ( decbin ( ord ( substr ( $string, $i, 1 ) ) ), 8, '0', STR_PAD_LEFT ), '0' )) {
				$i += $number - 1;
				$j ++;
			} else {
				$j += 0.5;
			}
		}
		
		for($i = 0; (($i < strlen ( $string )) && ($length > 0)); $i ++) {
			if ($number = strpos ( str_pad ( decbin ( ord ( substr ( $string, $i, 1 ) ) ), 8, '0', STR_PAD_LEFT ), '0' )) {
				if ($length < 1.0) {
					break;
				}
				
				$result .= substr ( $string, $i, $number );
				
				$length -= 1.0;
				
				$i += $number - 1;
			} else {
				$result .= substr ( $string, $i, 1 );
				
				$length -= 0.5;
			}
		}
		
		$result = htmlentities ( $result, ENT_QUOTES, 'utf-8' );
		
		if ($i < strlen ( $string )) {
			$result .= $etc;
		}
		
		return $result;
	}
	
	/**
	 * 获取等级样式
	 * 
	 * @param int $creativeIndex
	 * @return string
	 */
	public static function getLevelStyle($creativeIndex) {
	    if(! $creativeIndex) {
	        $creativeIndex = 1;
	    }
	    $level = self::getCreativeLevel($creativeIndex);
	    return 'lv'.$level;
	}
	
	/**
	 * 获取专家样式
	 * 
	 * @param int $organizationId
	 * @return string
	 */
	public static function getExpertStyle($organizationId = BaseModule::EXPERT_CHAIR) {
	    $expertArr = array(BaseModule::EXPERT_CHAIR, BaseModule::EXPERT_COUNCIL, BaseModule::EXPERT_ADVISORY);
	    
	    if(! in_array($organizationId, $expertArr)) {
	        $organizationId = BaseModule::EXPERT_CHAIR;
	    }
	    return 'expert'.$organizationId;
	}
	
	public static function getExpertTitle($organizationId = BaseModule::EXPERT_CHAIR) {
	    $expertArr = array(BaseModule::EXPERT_CHAIR, BaseModule::EXPERT_COUNCIL, BaseModule::EXPERT_ADVISORY);
	    $titleArr = array('主席', '理事', '顾问');
	     
	    if(! in_array($organizationId, $expertArr)) {
	        $organizationId = BaseModule::EXPERT_CHAIR;
	    }
	    
	    return $titleArr[$organizationId-1];
	}
	
}