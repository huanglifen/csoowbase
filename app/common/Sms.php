<?php
	namespace App\Common;

	/**
	 * 发送短信接口
	 * Class Sms
	 * @package App\Common
	 */
	class Sms
	{


		/**
		 *  http://www.86sms.cn/
		 *
		 * @param $SendPhone   发送号码（提交多个号码时用英文半角逗号隔开）
		 * @param $SendMessage 发送信息（每条64字，支持长短信，最大字数192字）
		 *
		 * @return 0失败，1成功
		 */
		public static function sendSms($SendPhone, $SendMessage)
		{
			$UserId  = 'cysj';
			$UserPwd = 'cysj123';
			$url     = "http://86api.com/sms/SmsHttpPort.aspx?Action=SendMessage&SendPort=4&UserId={$UserId}&UserPwd={$UserPwd}&SendPhone={$SendPhone}&SendMessage={$SendMessage}";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
			$result = curl_exec($ch);
			curl_close($ch);

			$status = explode(',', $result);
			if ($status[0] == 1) {
				\Log::info($result);
			} else {
				$rs = $status[1];
				switch ($rs) {
					case -99:
						$msg = '参数错误';
						break;
					case -100:
						$msg = '用户名或密码错误';
						break;
					case -101:
						$msg = '用户状态未生效';
						break;
					case -102:
						$msg = '包含敏感字符';
						break;
					case -103:
						$msg = '提交号码错误';
						break;
					case -104:
						$msg = '通道号错误，没有此通道信息';
						break;
					case -105:
						$msg = '发送字数过多';
						break;
					case -106:
						$msg = '用户余额不足';
						break;
					case -107:
						$msg = '数据异常，请联系客服';
						break;
					default :
						$msg = $result;
				}
				\Log::error($msg);
			}
			return $status[0];
		}

	}