<div class="match-nav clearfix">
	<ul class="navlist clearfix fl">
		<li 
		@if ($pageName == 'index')
		class="current"
		@endif
		><a class="c" href="{{{$baseURL}}}/hiring/detail/{{{$hiring->id}}}">首页</a></li>
		<li
		@if ($pageName == 'detail')
		class="current"
		@endif
		><a class="c" href="{{{$baseURL}}}/hiring/hiring-detail/{{{$hiring->id}}}" data-category="">比赛详情</a></li>
		<li
		@if ($pageName == 'expertComment')
		class="current"
		@endif
		><a class="c" href="{{{$baseURL}}}/hiring/expert-comments/{{{$hiring->id}}}" data-category="">专家点评</a><i class="num">{{{$expertCommentNum}}}</i></li>
		<li
		@if ($pageName == 'participators')
		class="current"
		@endif
		><a class="c" href="{{{$baseURL}}}/hiring/participators/{{{$hiring->id}}}" data-category="">参赛的公益明星</a><i class="num">{{{$participatorNum}}}</i></li>
		<li
		@if ($pageName == 'updates')
		class="current"
		@endif
		><a class="c" href="{{{$baseURL}}}/hiring/updates/{{{$hiring->id}}}" data-category="">比赛动态</a></li>
	</ul>
</div>