@extends('common.main')
@section('title')
公益明星比赛 - 创意世界
@stop
@section('content')
<?php $list_contest_type = 3 ?>
@include('common.list-top')
	<div class="list-page content w1180">
		<div class="height_20"></div>
		<div class="cwindex-rank">
			<div class="cwindex-title">
				<p class="line"></p>
				<p class="ctitle"><span >创意指数排行榜</span></p>
			</div>
			<div class="height_10"></div>
			<ul class="clearfix">
			@foreach ($ranks as $key => $rank)
				<li>
					<div class="cwindex-rank-box">
						<a href="{{{$baseURL}}}/user/home/{{{$rank->user->id}}}">
							<div class="atop">
								<div class="avatar-area">
									<img class="avatar" src="{{{App\Common\Utils::getAvatar($rank->user->avatar)}}}" alt="">
									<p class="info">
										<span class="uname">{{{$rank->user->name}}}</span>
										<span class="cindex">创意指数 {{{$rank->total_creative_index}}}</span>
									</p>
								</div>
								<p class="abg"><span class="bg1"></span><span class="bg2"></span></p>
								<i class="num">{{{$key + 1}}}</i>
							</div>
						</a>
							<div class="abar">
								<p class="bc">
									{{{$rank->declaration}}}
								</p>
								
								@if(! $user || ($user&& $user->id != $rank->user->id))
							<p class="btnbar">
					                <?php 
				                      if((isset($rank->user->is_follow)&& $rank->user->is_follow)) {
				                            $class = "att-btn Js_att Js_has_followed_btn";
				                            $followname = '已关注';
			                           }else{
                                            $class = "att-btn Js_att Js_follow_btn";
                                            $followname = '关注';
                                       }
				                     ?>
				                
									<a href="javascript:;"><span class="{{{$class}}}" user-id = "{{{$rank->user->id}}}">{{{$followname}}}</span></a>
								</p>
								@else
								<p class="btnbar" style="padding:0 15px; line-height: 1.4;"><span class="fl">{{{$rank->declaration}}}</span></p>
								@endif
								
							</div>
						
					</div>
				</li>
		    @endforeach
			</ul>
		</div>
		<div class="height_10"></div>
		<div class="Js_list_tag">
			<div class="list_tag_wrap w1180">
				<div class="list-tags clearfix">
					<p class="tags fl Js_tag">
						@include('common.hot-tags')
					</p>
					<p class="refresh fr"><a class="t Js_refreshtag" href="javascript:" title="刷新">刷新</a></p>
				</div>
				<div class="height_10"></div>
				<div class="match-nav clearfix">
					<ul class="navlist clearfix fl Js_list_tab">
						<li class="current"><a class="c" href="javascript:" data-category="">专题大赛</a></li>
						<li><a class="c" href="javascript:" data-category="2">正在寻找明星的比赛</a></li>
						<li><a class="c" href="javascript:" data-category="3">已经找到明星的比赛</a></li>
<!-- 						<li @if($status == 3)class="current" @endif><a href="{{{$baseURL}}}/hiring/index/3">比赛成功</a></li> -->
					</ul>
					<a class="publish-btn fr" href="{{{$baseURL}}}/hiring/publish">发布比赛</a>
				</div>
			</div>
		</div>
		<div class="list_main">
			<ul id="list_content" class="clearfix">
			@include('common.contest-list')
			</ul>
			@if (!empty($pageTotal) && $pageTotal > 1)
			<div class="loadpage"><a class="Js_nextpage loadpage-btn" href="javascript:">加载更多</a></div>
			@endif			
		</div>
		<input type="hidden" value="3" class="Js_list_contest_type" />
	</div>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.scrollstop.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.focusMap.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/list.js"></script>
@stop