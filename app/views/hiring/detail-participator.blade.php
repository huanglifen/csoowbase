@extends('common.main')
@section('title')
{{{$hiring->title}}} - 作品详情
@stop
@section('content')
	<div class="detail-top-wap dworks hiring_dworks">
		<div class="detail-top w1180">
		
			@include('common.like-report',array('targetType'=>31,'targetId'=>$participator->id))
			
			<h1 class="pro-title dworks"><a href="{{$baseURL}}/hiring/detail/{{$hiring->id}}">{{{$hiring->title}}}</a></h1>
			<div class="work-info">
				
				<?php $tags = json_decode($hiring->tags);?>
						@if(!empty($tags))
						<p>比赛标签：
							@foreach($tags as $tag)
							<span class="work-tag">{{{$tag}}}</span> 
							@endforeach 
						</p>
						@endif
				<p>上传时间：{{{date('Y-m-d', $participator->create_time)}}}</p>
			</div>
			<div class="pro-btn dworks">
			@if ($isLogin && $user->id == $hiring->user_id && $hiring->status == \App\Modules\ItemModule::STATUS_GOING)
				<a class="joinbtn Js_select_winner" href="javascript:">选为获胜明星</a>
				<div class="message_box Js_message_box" style="display: none">
				    <div class="box_tittle">
				        <span class="t fl">选择获胜明星</span>
				        <a class="close fr Js_close_btn" href="javascript:" title="关闭"></a>
				    </div>
				    <div class="box_content">
				        <div class="choose_winner">
				            <form class="winner-form" action="#">
				            <ul>
				            @foreach ($prizes as $prize)
				            @if (!$prize->winner_user_id)
				                <li class="winner">
				                    <label class="clearfix">
				                        <input type="checkbox" name="prize_id" value="{{{$prize->id}}}" data-participator-id="{{{$participator->id}}}"/>
				                        <em class="winner_tittle">{{{$prize->title}}}</em>
				                        <ul class="c-awards">
				                            <li class="clearfix"><span class="winner_money">奖金：</span><em>¥{{{$prize->bonus_amount}}}</em></li>
				                            @if ($prize->award)
				                            <li class="clearfix"><span>奖品：</span><p>{{{$prize->award}}}</p></li>
				                       		@endif
				                        </ul>
				                    </label>
				                </li>
				            @endif
				            @endforeach
				            </ul>
				            <input type="hidden" name="participator_id" value="{{{$participator->id}}}">
				            </form>
				        </div>
				        <div class="box_btm_btn clearfix">
				            <a class="ok Js_confirm_winner" href="javascript:">确认</a>
				        </div>
				    </div>
				</div>				
			@endif
			@if ($isLogin && $user->id == $participator->user_id)
				<a class="joinbtn" href="{{{$baseURL}}}/hiring/cancel-apply/{{{$participator->id}}}">撤回作品</a>
				<a class="joinbtn" href="{{{$baseURL}}}/hiring/apply/{{{$participator->hiring_id}}}/{{{$participator->id}}}">修改作品</a>
			@endif
			</div>
            <div class="pro-breadcrumb">当前位置：<a href="{{{$baseURL}}}/hiring/index">创意公益明星比赛</a>&nbsp;/&nbsp;<a href="{{$baseURL}}/hiring/detail/{{$hiring->id}}">比赛详情</a>&nbsp;/&nbsp;作品详情</div>
        </div>
	</div>
	<div class="content w1180 clearfix">
		<div class="detail-content">
			<div class="height_20"></div>
			<div class="pro-detail show-editor">
			{{$participator->works}}
			</div>
			<?php $creative_index = $participator->creative_index;?>
			@include('common.creative-score')
			<div class="des-line"></div>
			<!-- 专家点评 -->
			@include('common.expert-comments',array('showCommentUrl'=>$baseURL."/hiring/participators/".$hiring->id,'commentBtnTitle'=>'查看明星进行点评','showCommentInputBox'=>true,'targetType'=>3))
			<!-- 项目交流 -->
			@include('common.comments',array('targetId'=>$participator->id))
		</div>
		<div class="detail-aside">
			<div class="pro">
				<div class="pro-content swork">
					<strong>作品提交者</strong>
					<div class="clearfix content">
						<div class="user-avatar">
							<a href="{{$baseURL}}/user/home/{{$participator->user->id}}"><img src="{{{App\Common\Utils::getAvatar($participator->user->avatar)}}}" /></a>
						</div>
						<div class="user-info">
							<div class="user-name">
								<span>{{{$participator->user->name}}}</span>
								<div class="level">
									<div class="icon {{{App\Common\Utils::getLevelStyle($participator->user->creative_index)}}}"></div>
									<div class="bar">
										<div class="process" style="width: 22px;"></div>
									</div>
								</div>
							</div>
							<span class="user-city">{{{$participator->user->city_name}}}</span>
							<div class="user-mutual">
								<div class="clearfix mutual-btn">
                                    @if(! $user || $user->id != $participator->user_id)
                                        <?php
                                        if(isset($participator->user->is_follow)&& $participator->user->is_follow){
                                            $class = "Js_has_followed_btn";
                                            $followname = '已关注';
                                            }else{
                                                $class = "Js_follow_btn";
                                                $followname = '关注';
                                            }
                                        ?>
									<a class="{{{$class}}}" href="javascript:;" user-id="{{{$participator->user_id}}}">{{{$followname}}}</a>
									<a href="{{{$baseURL}}}/chat/contact/{{{$participator->user_id}}}" target="_blank">聊天</a>
									@endif
								</div>
							</div>
						</div>
						<div class="height_10"></div>
						<div class="submit-time clear">
							<p>提交作品时间：{{{date('Y-m-d', $participator->create_time)}}}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop