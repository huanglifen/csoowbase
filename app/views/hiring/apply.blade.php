@extends('common.main')
@section('title')
{{{$hiring->title}}} - 提交作品
@stop
@section('content')
<div class="content w1180">
    <div class="height_60"></div>
    <div class="tabs_wraper w980">
        <form id="publish_hire_apply_form" action="#">
            <input type="hidden" class="Js_hiring_id" value="{{{$id}}}" name="hiring_id" />
            <input type="hidden" id="Js_target_type" value="3">
			@if ($participator)
				<input type="hidden" class="Js_participator_id" value="{{{$participator->id}}}" name="participator_id">
			@endif
            <div class="tabs_step Js_tabs_step" style="margin-bottom:30px; margin-left:75px;">
                <div class="hor_line Js_hor_line">
                    <div class="hor_inline Js_hor_inline"></div>
                </div>
                <ul class="Js_tab">
                    <li class="first focustab">
                        <a href="#step-one">
                            <b class="circle">1</b>
                            <span class="tips">基本信息</span>
                        </a>
                    </li>
                    <li class="last">
                        <a href="#step-two">
                            <b class="circle">2</b>
                            <span class="tips">确认协议</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tabs_con Js_tabs_content" style="display:block;">
                        <div class="row">
                            <div class="rt" style="width:130px;">上传作品封面图片：</div>
                            <div class="rc">
                                <div class="con" id="imagefile">
                                    <div class="upload-files Js_cover">
                                        <div class="uploadWrap-single dargArea left">
                                            <a class="Js_uploadbox" style="vertical-align: middle">
                                                <button type="button" class="btn LightGrey"><span class="inner">上传</span></button>
                                            </a>
                                            <span class="des">请上传jpg、gif、png格式，分辨率大于280×220的图片</span>
                                            <span class="Validform_checktip Validform_wrong" style="display:none"></span>
                                            <input class="Js_file_uploadbox_single upload-input" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff">
                                        </div>
                                        <div class="height_10"></div>
                                        <div class="clearfix upload-single">
										@if (isset($participator->cover) && $participator->cover)
											<div class="updateSucImg">
												<div class="imgwrap">
													<p>
														<img src="{{{$baseURL}}}/{{{$participator->cover}}}">
													</p>
												</div>
												<a class="close Js_file_delete" title="删除" href="javascript:;">
												    删除
													<input class="upload_success_cover" type="hidden" value="{{{$participator->cover}}}" name="imagefile">
												</a>
											</div>
										@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="clearfix">
							<div class="row fl">
								<div class="rt" style="width:130px;">代表作品：</div>
								<div class="rc">
									<div class="con" id="works">
										<script type="text/plain" id="matchDetail" style="width:620px;height:215px;"></script>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix">
							<div class="row">
								<div class="rt" style="width:130px;">一句话公益宣言：</div>
								<div class="rc">
									<div class="con" id="declaration">
										<input style="position: relative" type="text" placeholder="" name="declaration" datatype="e" errormsg="" nullmsg="" 
										@if (isset($participator->declaration))
										value="{{{$participator->declaration}}}"
										@endif
										>
										<span class="des">（选填）</span>
									</div>
								</div>
							</div>
						</div>
                        <div class="row">
                            <div class="rc" style="margin-left:138px;">
                                <a class="Js_goto_step2" href="#step-two">
                                    <button type="button" class="btn DeepBlue">下一步</button>
                                </a>
                            </div>
                        </div>
                </ul>
            </div>
            <div class="tabs_con Js_tabs_content" style="display:none;">
                <div class="agreement">
                    <h1>《创意公益明星比赛合作协议》<i>编号：{{{$hiring->no}}}</i></h1>
                    <div class="separator"></div>
                    <p class="preface">本合作协议（“<em>本协议</em>”）由以下双方于{{{date('Y年m月d日', time())}}}日签订：</p>
                    <div class="definition">
                        <div class="party_ab">
                            <h2><em>甲方</em></h2>
                            <p>创意世界用户名：<em>{{{$publisher->name}}}</em></p>
                            <p>身份证号：<em></em></p>
                            <p>电子邮件地址：<em>{{{$publisher->email}}}</em></p>
                        </div>
                        <div class="party_ab">
                            <h2><em>乙方</em></h2>
                            <p>创意世界用户名：<em>{{{$user->name}}}</em></p>
                            <p>身份证号：<em></em></p>
                            <p>电子邮件地址：<em>{{{$user->email}}}</em></p>
                        </div>
                    </div>
                    <div class="separator"></div>
                    <div class="height_30"></div>
                    <div class="info_content">
                        <p>就<em>甲方</em>通过由四川创意世界科技有限公司（“<em>创意世界</em>”）运营管理的<em>创意世界网</em>（域名为<a href="www.csoow.com">www.csoow.com</a>，“<em>创意世界大赛-创意公益明星比赛</em>”）服务向<em>乙方</em>寻求创意明星作品投资事宜，双方根据平等、自愿的原则，达成<em>本协议如</em>下：</p>
                        <ol class="level1">
                            <li>
                                <strong>创意明星作品内容及转让条件</strong>
                                <ul>
                                    <li>
                                        <em>甲方</em>同意通过<em>创意公益明星比赛</em>服务向<em>乙方</em>选拔创意明星并提供比赛奖励，<em>乙方</em>同意通过<em>创意公益明星比赛</em>服务向<em>甲方</em>转让创意明星作品：
                                        <ul class="level3">
                                            <li>创意公益明星比赛的奖励：
                                            	@foreach ($prizes as $prize)
												<div class="awa-rank">
													<p>头衔：{{{$prize->title}}}</p>
													<p><span class="let">奖金：</span><span class="rc">¥{{{$prize->bonus_amount}}}.00</span></p>
													@if ($prize->award)
													<p><span class="let">奖品：</span><span class="rc">{{$prize->award}}</span></p>
													@endif
												</div>
												@endforeach                                           
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <strong>征集创意明星作品流程</strong>
                                <p><em>本协议</em>成立及生效：<em>甲方</em>按照<em>创意公益明星比赛</em>的规则，通过在<em>创意公益明星比赛</em>上对<em>乙方</em>上传的创意明星作品点击“选为获胜明星”按钮确认时，<em>本协议</em>立即成立并且生效。</p>
                            </li>
                            <li>
                                <strong>创意明星作品来源保证</strong>
                                <p><em>乙方</em>保证其所用于投资的资金来源合法，<em>乙方</em>是该资金的合法所有人，如果第三方对资金归属、合法性问题发生争议，由<em>乙方</em>自行负责解决。如<em>乙方</em>未能解决，则放弃享有其所获创意公益明星比赛权益的权利。</p>
                            </li>
                            <li>
                                <strong>知识产权的归属</strong>
                                <ol class="level2">
                                    <li><em>甲方</em>确认<em>乙方</em>完成创意明星作品交接后，该创意明星作品的知识产权即转让给<em>甲方</em>所有。</li>
                                    <li><em>甲方</em>若需要线下签署知识产权转让协议，<em>乙方</em>有义务配合<em>甲方</em>完成协议的签署，若<em>乙方</em>拒绝签署，<em>甲方</em>有权要求放弃享有创意公益明星比赛的奖励的权利。</li>
                                </ol>

                            </li>
                            <li>
                                <strong>逾期完工</strong>
                                <p><em>甲方</em>超过创意明星作品完成的最后期限完成创意明星作品，<em>甲方</em>有权要求撤回创意明星作品投资款项并终止本协议。</p>
                            </li>
                            <li>
                                <strong>变更通知</strong>
                                <ol class="level2">
                                    <li><em>本协议</em>签订之日起至<em>乙方</em>确认创意明星作品完成之日止，<em>甲方</em>有义务在其向<em>乙方</em>提供的任何信息变更3天内通过<em>创意公益明星比赛</em>项目更新后的信息给<em>乙方</em>并提交相应的证明文件，包括但不限于<em>甲方</em>姓名、身份证号码、住址等个人基本信息、其他信息等的变更。</li>
                                    <li>若因<em>甲方</em>不及时提供上述变更信息而带来的<em>乙方</em>的调查及诉讼费用将由<em>甲方</em>承担。</li>
                                </ol>
                            </li>
                            <li>
                                <strong>本协议的转让</strong>
                                <p>未经<em>乙方</em>事先书面（包括但不限于电子邮件等方式）同意，<em>甲方</em>不得将<em>本协议</em>项下的任何权利义务转让给任何第三方。</p>
                            </li>
                            <li>
                                <strong>其他</strong>
                                <ol class="level2">
                                    <li><em>本协议</em>的任何修改、补充均须以<em>创意世界大赛</em>平台电子文本形式作出。</li>
                                    <li>甲乙双方均确认，<em>本协议</em>的签订、生效和履行以不违反法律为前提。如果<em>本协议</em>中的任何一条或多条违反适用的法律，则该条将被视为无效，但该无效条款并不影响<em>本协议</em>其他条款的效力。</li>
                                    <li>如果甲乙双方在<em>本协议</em>履行过程中发生任何争议，应友好协商解决；如协商不成，则须提交<em>甲方</em>或<em>乙方</em>所在地人民法院进行诉讼。</li>
                                    <li>甲乙双方委托<em>创意世界</em>保管所有与<em>本协议</em>有关的书面文件或电子信息。</li>
                                </ol>
                            </li>
                        </ol>
                    </div>
                    <div class="height_30"></div>
                    <a class="Js_goto_step1" href="#step-one">
                        <button type="button" class="btn DeepGrey">上一步</button>
                    </a>&nbsp;&nbsp;
                    <a class="Js_hire_apply_submit" href="javascript:">
                        <button type="button" class="btn DeepBlue">同意</button>
                    </a>
                </div>
            </div>
            <div class="height_20"></div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/editor/umeditor.config.js" charset="utf-8" ></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/editor/_examples/editor_api.js" charset="utf-8" ></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/editor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/uploader_config_single.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/radioitem.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/zebra_datepicker.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/tabs.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/publish.js"></script>
@if (isset($participator->works))
<script type="text/javascript">
$(function(){
	var workContentSource = '{{$participator->works}}';
    var ue = UM.getEditor('matchDetail');
    ue.setContent(workContentSource)
})
</script>
@endif
<script>var uploadPath = "hiring/upload-image";</script>
@stop