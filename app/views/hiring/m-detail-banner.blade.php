	<div class="detail-top-wap top-hiring">
		<div class="detail-top w1180">
		
			@include('common.like-report')
			
			<h1 class="pro-title">{{{$hiring->title}}}</h1>
			<p class="pro-des">
			<span class="">
			@if ($hiring->status == \App\Modules\ItemModule::STATUS_GOING)
			比赛正在进行中
			@elseif ($hiring->status <= \App\Modules\ItemModule::STATUS_WAITING_CHECK)
			审核中
			@elseif ($hiring->status = \App\Modules\ItemModule::STATUS_END_SUCCESS)
			比赛成功
			@else
			比赛关闭
			@endif
			</span><span class="m1">编号：{{{$hiring->no}}}</span></p>
			<div class="pro-user">
				<div class="avatar-wrap"><a href="{{$baseURL}}/user/home/{{$hiring->user->id}}"><img class="avatar" src="{{{App\Common\Utils::getAvatar($hiring->user->avatar)}}}" alt=""></a></div>
				<div class="user-info">
					<div class="low-name">
						<a class="uname" href="{{$baseURL}}/user/home/{{$hiring->user->id}}">{{{$hiring->user->name}}}</a>
						<div class="level">
							<div class="icon {{{App\Common\Utils::getLevelStyle($hiring->user->creative_index)}}}"></div>
							<div class="bar">
								<div class="process" style="width: 60%;"></div>
							</div>
						</div>
				   @if($hiring->user->group_id == App\Modules\UserModule::GROUP_EXPERT)		
					<?php $expertTitle = App\Common\Utils::getExpertTitle($hiring->user->expert_organization_id);
                          $expertStyle = App\Common\Utils::getExpertStyle($hiring->user->expert_organization_id);
                              ?>
			         <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			         @endif
					</div>
				<?php
					$hiring->user->location = $hiring->user->province_name . $hiring->user->city_name . $hiring->user->district_name;
					$separator = '';
					if ($hiring->user->location && $hiring->user->school_name) {
						$separator = '|';
					}
					$delimiter = '';
					if ($hiring->user->school_name && $hiring->user->organization_name) {
						$delimiter = '|';
					}
				?>
					<p class="low-about"><span>{{{$hiring->user->city_name}}}</span> {{{$separator}}} <span>{{{$hiring->user->school_name}}}</span> {{{$delimiter}}} <span>{{{$hiring->user->organization_name}}}</span></p>
					<p class="low-btn">
					@if(! $user || $user->id != $hiring->user_id)
					<?php 
				      if(isset($hiring->user->is_follow)&& $hiring->user->is_follow) {
				        $class = "g Js_has_followed_btn";
				        $followname = '已关注';
			           }else{
                            $class = "g Js_follow_btn";
                            $followname = '关注';
                       }
				     ?>
					<a user-id="{{{$hiring->user_id}}}" class="{{{$class}}}" href="javascript:">{{{$followname}}}</a>
					<a data-user-id="{{{$hiring->user_id}}}" class="g" href="{{{$baseURL}}}/chat/contact/{{{$hiring->user_id}}}" target="_blank">聊天</a></p>
				    @endif
				</div>
			</div>
			@if ($canApply)
			<div class="pro-btn">
				<a class="joinbtn" href="{{{$baseURL}}}/hiring/apply/{{{$hiring->id}}}">我要参赛</a>
			</div>
			@endif
            <div class="pro-breadcrumb">当前位置：<a href="{{{$baseURL}}}/hiring/index">创意公益明星比塞</a>&nbsp;/&nbsp;比赛详情</div>
		</div>
	</div>