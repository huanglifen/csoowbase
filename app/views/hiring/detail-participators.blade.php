@extends('common/main');
@section('title')
{{{$hiring->title}}} - 创意公益明星比赛
@stop
@section('content')
@include('hiring.m-detail-banner')
<div class="content w1180 clearfix">
		<div class="detail-content">
			@include('hiring.m-detail-toplink')
<div class="height_10"></div>
        <!--参赛的公益明星-->
        <div class="dis-top clearfix">
            <span class="t fl">参赛的公益明星</span>
        </div>
        <div class="list_main">
        @if(count($participators))
            <ul id="list_content" class="clearfix">
            @foreach ($participators as $participator)
                <li>
                    <div class="list_block_container">
                        <div class="list_block project">
                            <div class="block_cover">
                                <a class="cover" href="{{{$baseURL}}}/hiring/participator/{{{$participator->id}}}">
                                    <img class="" src="{{{$baseURL}}}/{{{$participator->cover}}}" title="{{{$hiring->title}}}">
                                </a>
                            </div>
                            <div class="block_bottom animate_border_color clearfix">
                                <img class="avatar" src="{{{App\Common\Utils::getAvatar($participator->user->avatar)}}}" alt="">
                                <a class="user animate_color" href="">{{{$participator->user->name}}}</a>
                                <span class="participants">{{{$participator->creative_index}}}</span>
                            </div>
                        </div>
                    </div>
                </li>
           @endforeach
            </ul>
         @else
         <div style="margin-right:20px;">
            <div class="no_content">
	          <div class="tip">暂无</div>
            </div>
            </div>
         @endif
        </div>
        <div class="des-line"></div>
			@include('common.comments')
		</div>
		@include('hiring.m-detail-aside')
	</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop