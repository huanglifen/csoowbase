@extends('common/main');
@section('title')
{{{$hiring->title}}} - 创意公益明星比赛
@stop
@section('content')
@include('hiring.m-detail-banner')
<div class="content w1180 clearfix">
    <div class="detail-content">
		@include('hiring.m-detail-toplink')
		<div class="height_20"></div>
			<!-- 专家点评 -->
			@include('common.expert-comments',array('canExpertComment'=>1,'showCommentUrl'=>$baseURL."/hiring/participators/".$hiring->id,'commentBtnTitle'=>'查看明星进行点评','showCommentInputBox'=>false))
			<div class="des-line"></div>
        <!--任务交流-->
		@include('common.comments')
    </div>
	@include('hiring.m-detail-aside')
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop