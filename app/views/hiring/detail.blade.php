@extends('common/main');
@section('title')
{{{$hiring->title}}} - 创意公益明星比赛
@stop
@section('content')
@include('hiring.m-detail-banner')
<div class="content w1180 clearfix">
		<div class="detail-content">
		@include('hiring.m-detail-toplink')
			<div class="height_10"></div>
			<!-- 获奖明星 -->
			@if ($hiring->status == App\Modules\ItemModule::STATUS_END_SUCCESS)
			<div class="winner-list">
				<div class="winner-list-title">
					<span class="t">获奖明星</span>
				</div>
				<ul class="clearfix">
				@foreach ($winners as $winner)
					<li>
						<div class="cwindex-rank-box">
							<a href="{{{$baseURL}}}/hiring/participator/{{{$winner->winner_participator_id}}}">
								<div class="atop">
									<div class="avatar-area">
										<img class="avatar" src="{{{App\Common\Utils::getAvatar($winner->winner_user->avatar)}}}" alt="">
										<p class="info">
											<span class="uname">{{{$winner->winner_user->name}}}</span>
										</p>
									</div>
									<p class="abg"><span class="bg1"></span><span class="bg2"></span></p>
								</div>
								<div class="abar">
									<p class="bc">
										{{{$winner->title}}}
									</p>
								</div>
							</a>
						</div>
					</li>
				@endforeach
				</ul>
			</div>
			@endif
			<div class="height_30"></div>
			<!-- 项目简介 -->
			<div class="pro-summary">
				<div class="pro-summary-top"><span class="t">比赛简介</span></div>
				<div class="pro-summary-con show-editor">{{$hiring->description}}</div>
				<div class="pro-more"><a href="{{{$baseURL}}}/hiring/hiring-detail/{{{$hiring->id}}}">更多>></a></div>
			</div>
			<div class="des-line"></div>
			<!-- 比赛动态 -->
			<div class="dynamic">
				<div class="header_update">
					<h3>比赛动态</h3>
					@if ($isLogin && $user->id == $hiring->user_id)
					<a class="join Js_update_trend" href="javascript:" data-contest-id="{{{$hiring->id}}}">更新比赛</a>
					@include('common.m-publish-update')
					@endif
				</div>
				@if(count($updates))
				@include('common.m-updates')
				<div class="pro-more"><a href="{{{$baseURL}}}/hiring/expert-comments/{{{$hiring->id}}}">更多>></a></div>
				@else
                    <div class="no_content">
	                      <div class="tip">暂无</div>
                     </div>
				@endif
			</div>
			<div class="des-line"></div>
			<!-- 专家点评 -->
			@include('common.expert-comments',array('canExpertComment'=>1,'showCommentUrl'=>$baseURL."/hiring/participators/".$hiring->id,'commentBtnTitle'=>'查看明星进行点评','showCommentInputBox'=>false))
			<div class="des-line"></div>
			<!-- 最新参赛的公益明星 -->
			<div class="discuss-wrap">
				<div class="dis-top clearfix"><span class="t fl">最新参赛的公益明星</span><a class="ebtn fr" href="{{{$baseURL}}}/hiring/apply/{{{$id}}}">我要参赛</a></div>
				@if(count($participators))
				<ul class="expert-list">
				@foreach ($participators as $participator)
					<li>
						<div class="list-item">
							<a href="{{{$baseURL}}}/hiring/participator/{{{$participator->user->id}}}" @if(! $user || $user->id != $participator->user->id)class="Js_show_user_info_card" @endif data-user-id="{{$participator->user->id}}">
							<img class="avatar" src="{{{App\Common\Utils::getAvatar($participator->user->avatar)}}}" alt="{{{$participator->user->name}}}" />
							</a>
							<div class="item-t">
								<span class="t"><a href="javascript:;" @if(! $user || $user->id != $participator->user->id)class="Js_show_user_info_card" @endif data-user-id="{{$participator->user->id}}">{{{$participator->user->name}}}</a></span>
								<div class="level">
									<div class="icon {{{App\Common\Utils::getLevelStyle($participator->user->creative_index)}}}"></div>
									<div class="bar">
										<div class="process" style="width: 22px;"></div>
									</div>
								</div>
							@if($participator->user->group_id == App\Modules\UserModule::GROUP_EXPERT)
			                    <?php $expertTitle = App\Common\Utils::getExpertTitle($participator->user->expert_organization_id);
                                      $expertStyle = App\Common\Utils::getExpertStyle($participator->user->expert_organization_id);
                                    ?>
			                <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
				@endif
								<span class="date fr">{{{date('Y-m-d', $participator->create_time)}}}</span>
							</div>
							@if ($participator->user->group_id == 2)
							<div class="post"><span>创意世界智库顾问团</span><span>常务理事</span></div>
							@endif
							<div class="item-con">{{{$participator->declaration}}}
							</div>
							<div class="dotline"></div>
						</div>
					</li>
				@endforeach
				</ul>
				<div class="pro-more"><a href="{{{$baseURL}}}/hiring/participators/{{{$hiring->id}}}">更多>></a></div>
				@else
                   <div class="no_content">
	                          <div class="tip">暂无</div>
                    </div>
         @endif
			</div>
			<div class="des-line"></div>

			<!-- 项目交流 -->
			@include('common.comments')
		</div>
		@include('hiring.m-detail-aside')
	</div>
<input type="hidden" class="Js_contest_id" data-target-type="{{{$hiring->type}}}" data-target-id="{{{$hiring->id}}}">
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop