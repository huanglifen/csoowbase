@extends('common/main');
@section('title')
{{{$hiring->title}}} - 创意公益明星比赛
@stop
@section('content')
@include('hiring.m-detail-banner')
<div class="content w1180 clearfix">
    <div class="detail-content">
        @include('hiring.m-detail-toplink')
        <div class="height_20"></div>
            <div class="pro-detail">{{$hiring->description}}</div>
        <div class="des-line"></div>
        <!-- 比赛交流 -->
        @include('common.comments')
    </div>
    @include('hiring.m-detail-aside')
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop