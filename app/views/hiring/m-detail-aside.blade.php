		<div class="detail-aside">
			<div class="awards">
				<p class="t">比赛奖项</p>
				@foreach ($prizes as $prize)
				<div class="awa-rank">
					<p>{{{$prize->title}}}</p>
					<p><span class="let">奖金：</span><span class="rc">¥{{{$prize->bonus_amount}}}.00</span></p>
					@if ($prize->award)
					<p><span class="let">奖品：</span><span class="rc">{{$prize->award}}</span></p>
					@endif
				</div>
				@endforeach
			</div>
			<div class="height_20"></div>
			<div class="pro">
				<div class="pro-content">
					<strong>比赛进程</strong>
					<div class="guide">
						<div class="lt"></div>
						<ul class="guideList">
						@if ($hiring->status == App\Modules\ItemModule::STATUS_GOING)
							<li class="focus"><span>正在寻找明星</span><p>{{{App\Common\Utils::getRemainTime($hiring->end_time)}}}</p></li>
						@elseif ($hiring->status == App\Modules\ItemModule::STATUS_END_SUCCESS)
							<li class="passed"><span>已经选出获胜明星</span><p>{{{date('Y-m-d', $hiring->choose_time)}}}</p></li>
							<li class="passed"><span>比赛成功</span><p>{{{date('Y-m-d', $hiring->choose_time)}}}</p></li>
						@elseif ($hiring->status <= 1 && $hiring->status > -3)
							<li class="passed"><span>比赛已经关闭</span></li>
						@elseif ($hiring->status <= -3)
							<li class="passed"><span>审核中</span></li>
						@endif
						</ul>
					</div>
				</div>
			</div>
			<div class="pro pro-last">
				@include('common.contest-guess',array('endtime'=>$hiring->end_time))
			</div>
		</div>