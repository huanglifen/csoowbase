@extends('common/main');
@section('title')
{{{$hiring->title}}} - 创意公益明星比赛
@stop
@section('content')
@include('hiring.m-detail-banner')
<div class="content w1180 clearfix">
		<div class="detail-content">
			@include('hiring.m-detail-toplink')
			<div class="height_20"></div>
            <!--比赛动态-->
			<div class="dynamic">
				<div class="header_update">
					<h3>比赛动态</h3>
					@if ($isLogin && $user->id == $hiring->user_id)
					<a class="join Js_update_trend" href="javascript:" data-contest-id="{{{$hiring->id}}}">更新比赛</a>
					@include('common.m-publish-update')
					@endif
				</div>
				@include('common.m-contest-update')
			</div>
			<div class="des-line"></div>
			<!-- 比赛交流 -->
			@include('common.comments')
		</div>
		@include('hiring.m-detail-aside')
	</div>
<input type="hidden" class="Js_contest_id" data-target-type="{{{$hiring->type}}}" data-target-id="{{{$hiring->id}}}">
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop