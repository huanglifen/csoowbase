@extends('common.main')

@section('content')

<div id="wrapper">
	@include('home.m_lunbotu')
	
	<div class="content w1180">

	@include('home.m_intro')

	@include('home.m_hot')

	@include('home.m_thinktank')

	@include('home.m_area')

	@include('home.m_circle')

	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.focusMap.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/index.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/friendCircle.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.artZoom.js"></script>
<script>
var shareThisPage = true;
jQuery(function ($) {
	$('.artZoom').artZoom({
		preload: true,		// 设置是否提前缓存视野内的大图片
		blur: true,			// 设置加载大图是否有模糊变清晰的效果
		maxWidth: 650,
		// 语言设置
		left: '左旋转',		// 左旋转按钮文字
		right: '右旋转',		// 右旋转按钮文字
		source: '看原图'		// 查看原图按钮文字
	});
});
</script>
@stop