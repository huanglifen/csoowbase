@extends('common.main')
@section('title')
{{{$task->title}}} - 作品详情
@stop
@section('content')
	<div class="detail-top-wap dworks task_dworks">
		<div class="detail-top w1180">
		
			@include('common.like-report',array('targetType'=>21,'targetId'=>$work->id))
			
			<h1 class="pro-title dworks"><a href="{{$baseURL}}/task/detail/{{$task->id}}">{{{$task->title}}}</a></h1>
			<div class="work-info">
				
				<?php $tags = json_decode($task->tags);?>
				@if(!empty($tags))
				<p>比赛标签：
					@foreach($tags as $tag)
					<span class="work-tag">{{{$tag}}}</span> 
					@endforeach 
				</p>
				@endif
				<p>上传时间：{{{date('Y-m-d', $work->create_time)}}}</p>
			</div>
			<div class="pro-btn dworks">
			@if ($isLogin && $user->id == $task->user_id)
				<a class="joinbtn" href="{{{$baseURL}}}/task/choose-winner/{{{$work->id}}}">选为获胜作品</a>
			@endif
			
			@if ($isLogin && $user->id == $work->user_id)
				<a class="joinbtn" href="{{{$baseURL}}}/task/cancel-work/{{{$work->id}}}">撤回作品</a>
				<a class="joinbtn" href="{{{$baseURL}}}/task/apply/{{{$work->task_id}}}/{{{$work->id}}}">修改作品</a>
			@endif
			</div>
            <div class="pro-breadcrumb">当前位置：<a href="{{{$baseURL}}}/task/index">创意任务人才比赛</a>&nbsp;/&nbsp;<a href="{{$baseURL}}/task/detail/{{$task->id}}">比赛详情</a>&nbsp;/&nbsp;作品详情</div>
		</div>
	</div>
	<div class="content w1180 clearfix">
		<div class="detail-content">
			<div class="height_20"></div>
			<div class="pro-detail show-editor">
			{{$work->works}}
			</div>
			<?php $creative_index = $work->creative_index;?>
			@include('common.creative-score')
			<div class="des-line"></div>
			<!-- 专家点评 -->
			@include('common.expert-comments',array('showCommentUrl'=>$baseURL."/task/works/".$task->id,'commentBtnTitle'=>'查看作品进行点评','showCommentInputBox'=>true,'targetType'=>2))
			<!-- 项目交流 -->
			@include('common.comments',array('targetId'=>$work->id))
		</div>
		<div class="detail-aside">
			<div class="pro">
				<div class="pro-content swork">
					<strong>作品提交者</strong>
					<div class="clearfix content">
						<div class="user-avatar">
							<a href="{{$baseURL}}/user/home/{{$work->user->id}}"><img src="{{{App\Common\Utils::getAvatar($work->user->avatar)}}}" /></a>
						</div>
						<div class="user-info">
							<div class="user-name">
								<span>{{{$work->user->name}}}</span>
								<div class="level">
									<div class="icon {{{App\Common\Utils::getLevelStyle($work->user->creative_index)}}}"></div>
									<div class="bar">
										<div class="process" style="width: 22px;"></div>
									</div>
								</div>							
							</div>
							<span class="user-city">{{{$work->user->city_name}}}</span>
							<div class="user-mutual">
								<div class="clearfix mutual-btn">
								@if(! $isLogin || $user->id != $work->user->id)
							    <?php 
				                  if(isset($work->user->is_follow)&& $work->user->is_follow) {
				                    $class = "Js_has_followed_btn";
				                    $followname = '已关注';
			                   }else{
                                    $class = "Js_follow_btn";
                                    $followname = '关注';
                                   }
				                 ?>
									<a class="{{{$class}}}" href="javascript:;" user-id="{{{$work->user_id}}}">{{{$followname}}}</a>
									<a href="{{{$baseURL}}}/chat/contact/{{{$work->user_id}}}" target="_blank">聊天</a>
								@endif
								</div>
							</div>
						</div>
						<div class="height_10"></div>
						<div class="submit-time clear">
							<p>提交作品时间：{{{date('Y-m-d', $work->create_time)}}}</p>
							@if ($work->promise_complete_time > 0 && $work->status == App\Modules\TaskModule::TASK_WORK_STATUS_WINNER)
							<p>承诺完成时间：{{{date('Y-m-d', ($work->promise_complete_time * 86400 + $work->winning_time))}}}</p>
							@elseif($work->promise_complete_time > 0 && $work->status == App\Modules\TaskModule::TASK_WORK_STATUS_SUBMIT)
							<p>承诺完成时间：作品接受后{{{$work->promise_complete_time}}}天内</p>
							@elseif($work->promise_complete_time <= 0)
							<p>承诺完成时间：不做承诺</p>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop