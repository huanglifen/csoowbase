@extends('common/main');
@section('title')
{{{$task->title}}} - 创意任务人才比赛
@stop
@section('content')
@include('task.m-detail-banner')
	<div class="content w1180 clearfix">
		<div class="detail-content">
			@include('task.m-detail-toplink')
        <div class="height_20"></div>
        <div class="dynamic">
            <div class="header_update">
                <h3>任务动态</h3>
                @if ($isLogin && $user->id == $task->user_id)
                <a class="join Js_update_trend" href="javascript:" data-contest-id="{{{$task->id}}}">更新任务</a>
                @include('common.m-publish-update')
                @endif
            </div>
			@include('common.m-contest-update')
         <div class="des-line"></div>
        </div>
        
        <!--任务交流-->
		@include('common.comments')
	</div>
	@include('task.m-detail-aside')
	</div>
<input type="hidden" class="Js_contest_id" data-target-type="{{{$task->type}}}" data-target-id="{{{$task->id}}}">
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop