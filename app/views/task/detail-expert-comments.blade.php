@extends('common/main');
@section('title')
{{{$task->title}}} - 创意任务人才比赛
@stop
@section('content')
@include('task.m-detail-banner')
<div class="content w1180 clearfix">
    <div class="detail-content">
		@include('task.m-detail-toplink')
		<div class="height_20"></div>
			<!-- 专家点评 -->
			@include('common.expert-comments',array('canExpertComment'=>1,'showCommentUrl'=>$baseURL."/task/works/".$task->id,'commentBtnTitle'=>'查看作品进行点评','showCommentInputBox'=>false))
			<div class="des-line"></div>
        <!--任务交流-->
		@include('common.comments')
    </div>
	@include('task.m-detail-aside')
</div>

<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>

@stop