@extends('common/main')
@section('title')
{{{$task->title}}} - 创意任务人才比赛
@stop
@section('content')
@include('task.m-detail-banner')
	<div class="content w1180 clearfix">
		<div class="detail-content">
		@include('task.m-detail-toplink')
			<div class="height_20"></div>
			<!--任务简介-->
			<div class="pro-summary">
				<div class="pro-summary-top"><span class="t">任务简介</span></div>
				<div class="pro-summary-con show-editor">{{$task->description}}</div>
				<div class="pro-more"><a href="{{{$baseURL}}}/task/task-detail/{{{$task->id}}}">更多>></a></div>
			</div>
			<div class="des-line"></div>
			<!--任务动态-->
			<div class="dynamic">
				<div class="header_update">
					<h3>任务动态</h3>
					@if ($isLogin && $user->id == $task->user_id)
					<a class="join Js_update_trend" href="javascript:" data-contest-id="{{{$task->id}}}">更新任务</a>
					@include('common.m-publish-update')
					@endif
				</div>
				@if(count($updates))
				@include('common.m-updates')
				<div class="pro-more"><a href="{{{$baseURL}}}/task/updates/{{{$task->id}}}">更多>></a></div>
				@else
                <div class="no_content">
	                <div class="tip">暂无</div>
                </div>
                @endif
			</div>
			<div class="des-line"></div>
			<!-- 专家点评 -->
			@include('common.expert-comments',array('canExpertComment'=>1,'showCommentUrl'=>$baseURL."/task/works/".$task->id,'commentBtnTitle'=>'查看作品进行点评','showCommentInputBox'=>false))
			<div class="des-line"></div>
			<!-- 最新报名的人才 -->
			<div class="discuss-wrap">
				<div class="dis-top clearfix"><span class="t fl">最新报名的人才</span>			
				@if ($isLogin && $user->id == $task->user_id)
				@else
					@if ($task->status == App\Modules\ItemModule::STATUS_GOING && $canApply)
				<a class="ebtn fr" href="{{{$baseURL}}}/task/apply/{{{$task->id}}}">我要报名</a>
					@endif
				@endif
				</div>
				@if(count($works))
				<ul class="expert-list">
				@foreach ($works as $work)
				@if(!empty($work->user))
					<li>
						<div class="list-item">
							<a href="{{{$baseURL}}}/task/work/{{{$work->id}}}" @if(!$user || $user->id != $work->user->id)class="Js_show_user_info_card" @endif data-user-id="{{$work->user->id}}">
							<img class="avatar" src="{{{App\Common\Utils::getAvatar($work->user->avatar)}}}" alt="{{{$work->user->name}}}" />
							</a>
							<div class="item-t">
								<span class="t"><a href="{{{$baseURL}}}/task/work/{{{$work->id}}}" @if(!$user || $user->id != $work->user->id)class="Js_show_user_info_card" @endif data-user-id="{{$work->user->id}}">{{{$work->user->name}}}</a></span>
								<div class="level">
									<div class="icon {{{App\Common\Utils::getLevelStyle($work->user->creative_index)}}}"></div>
									<div class="bar">
										<div class="process" style="width: 22px;"></div>
									</div>
								</div>
			    @if($work->user->group_id == App\Modules\UserModule::GROUP_EXPERT)
			        <?php $expertTitle = App\Common\Utils::getExpertTitle($work->user->expert_organization_id);
                          $expertStyle = App\Common\Utils::getExpertStyle($work->user->expert_organization_id);
                    ?>
			        <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
				@endif
								<span class="date fr">提交申请报名时间：{{{date('Y-m-d', $work->create_time)}}}</span>
							</div>
							<div class="post"><span></span></div>
							<div class="dotline"></div>
						</div>
					</li>
					@endif
				@endforeach
				</ul>
				<div class="pro-more"><a href="{{{$baseURL}}}/task/works/{{{$task->id}}}">更多>></a></div>
				@else
                <div class="height_10"></div>
                <div class="no_content">
	                    <div class="tip">暂无</div>
                    </div>
				@endif
			</div>
			<div class="des-line"></div>
		<!--任务交流-->
		@include('common.comments')
		</div>
		@include('task.m-detail-aside')
	</div>
<input type="hidden" class="Js_contest_id" data-target-type="{{{$task->type}}}" data-target-id="{{{$task->id}}}">
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>

@stop