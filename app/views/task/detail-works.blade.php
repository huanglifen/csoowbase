@extends('common/main');
@section('title')
{{{$task->title}}} - 创意任务人才比赛
@stop
@section('content')
@include('task.m-detail-banner')
<div class="content w1180 clearfix">
    <div class="detail-content">
		@include('task.m-detail-toplink')
        <div class="height_20"></div>
        <!--报名的任务人才-->
        <div class="dis-top clearfix">
            <span class="t fl">报名的任务人才</span>
        </div>
        <div class="list_main">
        @if(count($works))
            <ul id="list_content" class="clearfix">
            @foreach ($works as $work)
                <li>
                    <div class="list_block_container">
                        <div class="list_block project">
                            <div class="block_cover">
                                <a class="cover" href="{{{$baseURL}}}/task/work/{{{$work->id}}}">
                                    <img class="" src="{{{$baseURL}}}/{{{$work->cover}}}" title="{{{$task->title}}}">
                                </a>
                            </div>
                            <div class="block_bottom animate_border_color clearfix">
                                <img class="avatar" src="{{{App\Common\Utils::getAvatar($work->user->avatar)}}}" alt="">
                                <a class="user animate_color" href="">{{{$work->user->name}}}</a>
                                <span class="participants">{{{$work->creative_index}}}</span>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
            </ul>
           @else
           <div style="margin-right:20px;">
               <div class="no_content">
	              <div class="tip">暂无</div>
             </div>
            </div>
           @endif
        </div>
        <div class="des-line"></div>
        <!--任务交流-->
		@include('common.comments')
    </div>
	@include('task.m-detail-aside')
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>

@stop