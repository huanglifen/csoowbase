		<div class="detail-aside">
            <div class="awards">
                <p class="t">任务奖励</p>
                <div class="awa-rank">
                @if(!empty($task->awards))
                    <p>{{{$task->awards}}}</p>
                @endif
                </div>
            </div>
            @if ($task->status == 3 || $task->status == 21)
			<div class="pro">
				<div class="pro-content">
					<strong>获胜任务人才</strong>
					<div class="clearfix content">
						<div class="user-avatar">
							<a href="{{$baseURL}}/user/home/{{$winner->user->id}}"><img src="{{{App\Common\Utils::getAvatar($winner->user->avatar)}}}" /></a>
						</div>
						<div class="user-info">
							<div class="user-name">
								<span>{{{$winner->user->name}}}</span>
								<div class="level">
									<div class="icon {{{App\Common\Utils::getLevelStyle($winner->user->creative_index)}}}"></div>
									<div class="bar">
										<div class="process" style="width: 22px;"></div>
									</div>
								</div>
							</div>
							<span class="user-city">{{{$winner->user->city_name}}}</span>
							<div class="user-mutual">
								<div class="clearfix mutual-btn">
								@if(!$isLogin || $user->id != $winner->user->id)
								<?php 
				                  if(isset($winner->user->is_follow)&& $winner->user->is_follow) {
				                    $class = "Js_has_followed_btn";
				                    $followname = '已关注';
			                       }else{
                                    $class = "Js_follow_btn";
                                    $followname = '关注';
                                   }
				         		?>
									<a  class="{{{$class}}}" user-id="{{{$winner->user_id}}}" href="javascript:;">{{{$followname}}}</a>
									<a href="{{{$baseURL}}}/chat/contact/{{{$winner->user_id}}}">聊天</a>
								@else
								<a href="{{{$baseURL}}}/task/agreement/{{{$task->id}}}">查看协议</a> 
								@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@else
			<div class="pro">
				<div class="pro-content">
					<strong>获胜任务人才</strong>
					<div class="clearfix content">
						<div class="user-avatar">
							<a href="#"><img src="{{{App\Common\Utils::getAvatar('')}}}" /></a>
						</div>
						<div class="user-info">
							<div class="user-name">
								<span>虚位以待</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			<div class="pro">
				<div class="pro-content">
					<strong>任务进程</strong>
					<div class="guide">
						<div class="lt"></div>
						<ul class="guideList">
						@if ($task->status <= 0 && $task->status > -3)
						<li class="passed"><span>比赛已经关闭</span></li>
						@elseif ($task->status <= -3)
						<li class="passed"><span>审核中</span></li>
						@elseif ($task->status == App\Modules\ItemModule::STATUS_GOING)
							<li class="focus"><span>正在寻找人才</span><p>{{{App\Common\Utils::getRemainTime($task->end_time)}}}</p></li>
						@elseif ($task->status == App\Modules\ItemModule::STATUS_WAITING_CONFIRM)
							<li class="passed"><span>选出获胜人才</span><p>{{{date('Y-m-d', $winner->winning_time)}}}</p></li>
							<li class="focus"><span>等待提交任务成果</span><p>{{{App\Common\Utils::getRemainTime($winner->promise_complete_time)}}}</li>
						@elseif ($task->status == App\Modules\ItemModule::STATUS_END_SUCCESS)
							<li class="passed"><span>选出获胜人才</span><p>{{{date('Y-m-d', $winner->winning_time)}}}</p></li>
							<li class="passed"><span>比赛成功</span>
							<p>
							@if ($winner->promise_complete_time > 0)
								{{{date('Y-m-d', $winner->create_time + ($winner->promise_complete_time * 86400))}}}
							@else
								{{{date('Y-m-d', $winner->winning_time)}}}
							@endif
							</p>
						@endif
						</ul>
					</div>
				</div>
			</div>
			<div class="pro pro-last">
				@include('common.contest-guess',array('endtime'=>$task->end_time))
			</div>
		
		</div>