	<div class="detail-top-wap top-task">
		<div class="detail-top w1180">
			
			@include('common.like-report')
		
			<h1 class="pro-title">{{{$task->title}}}</h1>
			<p class="pro-des"><span class="">
			@if($task->status == App\Modules\TaskModule::STATUS_END_SUCCESS)
			    比赛成功
			@elseif ($task->status <= \App\Modules\ItemModule::STATUS_WAITING_CHECK)
			审核中
			@elseif($task->status <= 0 && $task->status > -3)
			    已关闭
			@else
			    比赛正在进行中
			@endif
			</span><span class="m1">编号：{{{$task->no}}}</span></p>
			<div class="pro-user">
				<div class="avatar-wrap"><a href="{{$baseURL}}/user/home/{{$task->user->id}}"><img class="avatar" src="{{{App\Common\Utils::getAvatar($task->user->avatar)}}}" alt=""></a></div>
				<div class="user-info">
					<div class="low-name">
						<a class="uname" href="{{$baseURL}}/user/home/{{$task->user->id}}">{{{$task->user->name}}}</a>
						<div class="level">
							<div class="icon {{{App\Common\Utils::getLevelStyle($task->user->creative_index)}}}"></div>
							<div class="bar">
								<div class="process" style="width: 60%;"></div>
							</div>
						</div>
						
					 @if($task->user->group_id == App\Modules\UserModule::GROUP_EXPERT)		
					<?php $expertTitle = App\Common\Utils::getExpertTitle($task->user->expert_organization_id);
                          $expertStyle = App\Common\Utils::getExpertStyle($task->user->expert_organization_id);
                              ?>
			         <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			         @endif
					</div>
				<?php
					$task->user->location = $task->user->province_name . $task->user->city_name . $task->user->district_name;
					$separator = '';
					if ($task->user->location && $task->user->school_name) {
						$separator = '|';
					}
					$delimiter = '';
					if ($task->user->school_name && $task->user->organization_name) {
						$delimiter = '|';
					}
				?>
					<p class="low-about">
					<span>{{{$task->user->city_name}}}</span> {{{$separator}}} <span>{{{$task->user->school_name}}}</span> {{{$delimiter}}} <span>{{{$task->user->organization_name}}}</span></p>

					<p class="low-btn">
					@if(! $user || $user->id != $task->user_id)
					<?php 
				      if(isset($task->user->is_follow)&& $task->user->is_follow) {
				        $class = "g Js_has_followed_btn";
				        $followname = '已关注';
			           }else{
                            $class = "g Js_follow_btn";
                            $followname = '关注';
                       }
				     ?>
					<a class="{{{$class}}}" href="javascript:" user-id="{{{$task->user->id}}}">{{{$followname}}}</a>
					<a class="g" href="{{{$baseURL}}}/chat/contact/{{{$task->user_id}}}" target="_blank">聊天</a></p>
				    @else
				    	@if ($task->status > 2)
				   		<a class="g" href="{{{$baseURL}}}/task/agreement/{{{$task->id}}}">查看协议</a>
				   		@endif
				    @endif
				</div>
			</div>
			<div class="pro-btn">
			@if ($isLogin && $user->id == $task->user_id)
			@else
				@if ($task->status == App\Modules\ItemModule::STATUS_GOING && $canApply)
				<a class="joinbtn" href="{{{$baseURL}}}/task/apply/{{{$task->id}}}">我要报名</a>
				@endif
			@endif
			</div>
            <div class="pro-breadcrumb">当前位置：<a href="{{{$baseURL}}}/task/index">创意任务人才比赛</a>&nbsp;/&nbsp;比赛详情</div>
		</div>
	</div>