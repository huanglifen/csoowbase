			<div class="match-nav clearfix">
                <ul class="navlist clearfix fl">
                    <li
                    @if($pageName == 'index') 
                    class="current" 
                    @endif
                    ><a class="c" href="{{{$baseURL}}}/task/detail/{{{$task->id}}}">首页</a></li>
                    <li
                    @if($pageName == 'detail') 
                    class="current" 
                    @endif
                    ><a class="c" href="{{{$baseURL}}}/task/task-detail/{{{$task->id}}}" data-category="">任务详情</a></li>
                    <li
                    @if($pageName == 'expertComment') 
                    class="current" 
                    @endif
                    ><a class="c" href="{{{$baseURL}}}/task/expert-comments/{{{$task->id}}}" data-category="">专家点评</a><i class="num">{{{$expertCommentNum}}}</i></li>
                    <li
                    @if($pageName == 'works')
                     class="current"
                     @endif
                     ><a class="c" href="{{{$baseURL}}}/task/works/{{{$task->id}}}" data-category="">报名的任务人才</a><i class="num">{{{$workNum}}}</i></li>
                    <li
                    @if($pageName == 'updates')
                     class="current" 
                     @endif
                     ><a class="c" href="{{{$baseURL}}}/task/updates/{{{$task->id}}}" data-category="">任务动态</a></li>
                </ul>
            </div>