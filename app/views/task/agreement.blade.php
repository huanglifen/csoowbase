@extends('common.main') 
@section('title') 
{{{$task->title}}} - 创意任务人才比赛
@stop
@section('content')
<div class="content w1180">
    <div class="agreement">
        <h1>《创意任务人才比赛合作协议》<i>编号：{{{$task->no}}}</i></h1>
        <div class="separator"></div>
        <p class="preface">本合作协议（“<em>本协议</em>”）由以下双方于{{{date('Y年m月d日', time())}}}签订：</p>
        <div class="definition">
            <div class="party_ab">
                <h2><em>甲方</em></h2>
                <p>创意世界用户名：<em>{{{$publisher->name}}}</em></p>
                <p>身份证号：<em></em></p>
                <p>电子邮件地址：<em>{{{$publisher->email}}}</em></p>
            </div>
            <div class="party_ab">
                <h2><em>乙方</em></h2>
                <p>创意世界用户名：<em>{{{$user->name}}}</em></p>
                <p>身份证号：<em></em></p>
                <p>电子邮件地址：<em>{{{$user->email}}}</em></p>
            </div>
        </div>
        <div class="separator"></div>
        <div class="height_30"></div>
        <div class="info_content">
            <p>就<em>甲方</em>通过由四川创意世界科技有限公司（“<em>创意世界</em>”）运营管理的<em>创意世界网</em>（域名为<a href="www.csoow.com">www.csoow.com</a>，“<em>创意世界大赛-创意任务人才比赛</em>”）服务向<em>乙方</em>寻求创意任务作品投资事宜，双方根据平等、自愿的原则，达成<em>本协议如</em>下：</p>
            <ol class="level1">
                <li>
                    <strong>创意任务作品内容及转让条件</strong>
                    <ul>
                        <li>
                            <em>甲方</em>同意通过<em>创意任务人才比赛</em>服务向<em>乙方</em>寻求创意任务作品投资，<em>乙方</em>同意通过<em>创意任务人才比赛</em>服务向<em>甲方</em>发放创意任务作品投资款项：
                            <ul class="level3">
                                <li>创意任务人才比赛的奖励：<em>{{{$task->awards}}}</em></li>
                                <li>转让的创意任务作品：<em></em></li>
                                <li>完成创意任务作品的最后期限：<em class="Js_t1"></em></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <strong>创意任务作品投资流程</strong>
                    <p><em>本协议</em>成立及生效：<em>甲方</em>按照<em>创意任务人才比赛</em>的规则，通过在<em>创意任务人才比赛</em>上对<em>乙方</em>提交的创意任务作品投资申请点击“接受投资”按钮确认时，<em>本协议</em>立即成立并且生效。</p>
                <li>
                    <strong>创意任务作品来源保证</strong>
                    <p><em>乙方</em>保证其所用于投资的资金来源合法，<em>乙方</em>是该资金的合法所有人，如果第三方对资金归属、合法性问题发生争议，由<em>乙方</em>自行负责解决。如<em>乙方</em>未能解决，则放弃享有其所获创意任务人才比赛权益的权利。</p>
                <li>
                    <strong>逾期完工</strong>
                    <p><em>甲方</em>超过创意任务作品完成的最后期限完成创意任务作品，<em>甲方</em>有权要求撤回创意任务作品投资款项并终止本协议。</p>
                </li>
                <li>
                    <strong>变更通知</strong>
                    <ol class="level2">
                        <li><em>本协议</em>签订之日起至<em>乙方</em>确认创意任务作品完成之日止，<em>甲方</em>有义务在其向<em>乙方</em>提供的任何信息变更3天内通过<em>创意任务人才比赛</em>项目更新后的信息给<em>乙方</em>并提交相应的证明文件，包括但不限于<em>甲方</em>姓名、身份证号码、住址等个人基本信息、其他信息等的变更。</li>
                        <li>若因<em>甲方</em>不及时提供上述变更信息而带来的<em>乙方</em>的调查及诉讼费用将由<em>甲方</em>承担。</li>
                    </ol>
                </li>
                <li>
                    <strong>本协议的转让</strong>
                    <p>未经<em>乙方</em>事先书面（包括但不限于电子邮件等方式）同意，<em>甲方</em>不得将<em>本协议</em>项下的任何权利义务转让给任何第三方。</p>
                </li>
                <li>
                    <strong>其他</strong>
                    <ol class="level2">
                        <li><em>本协议</em>的任何修改、补充均须以<em>创意世界大赛</em>平台电子文本形式作出。</li>
                        <li>甲乙双方均确认，<em>本协议</em>的签订、生效和履行以不违反法律为前提。如果<em>本协议</em>中的任何一条或多条违反适用的法律，则该条将被视为无效，但该无效条款并不影响<em>本协议</em>其他条款的效力。</li>
                        <li>如果甲乙双方在<em>本协议</em>履行过程中发生任何争议，应友好协商解决；如协商不成，则须提交<em>甲方</em>或<em>乙方</em>所在地人民法院进行诉讼。</li>
                        <li>甲乙双方委托<em>创意世界</em>保管所有与<em>本协议</em>有关的书面文件或电子信息。</li>
                    </ol>
                </li>
            </ol>
        </div>
        <div class="height_30"></div>
        <a class="Js_goto_step1" href="#step-one">
            <button type="button" class="btn DeepGrey">上一步</button>
        </a>&nbsp;&nbsp;
        <a class="Js_task_apply_submit" href="javascript:">
            <button type="button" class="btn DeepBlue">同意</button>
        </a>
    </div>
</div>
@stop