@if(count($chooseExperts))
<ul class="clearfix">
@foreach($chooseExperts as $chooseExpert)
	<li>
		<div class="u-info">
			<a href="{{{$baseURL}}}/user/home/{{{$chooseExpert->id}}}">
				<img class="avatar @if(! $user || $user->id != $chooseExpert->id)Js_show_user_info_card @endif" data-user-id="{{$chooseExpert->id}}" src="{{{App\Common\Utils::getAvatar($chooseExpert->avatar)}}}" alt="">
				<div class="con">
					<span class="name @if(! $user || $user->id != $chooseExpert->id)Js_show_user_info_card @endif" data-user-id="{{$chooseExpert->id}}">{{{$chooseExpert->name}}}</span>
					<p class="des">{{{$chooseExpert->expert_title}}}</p>
				</div>
			</a>
		</div>
	</li>
@endforeach
</ul>
@else
    <div class="no_content">
	  <div class="tip">暂无</div>
     </div>
@endif

<div class="paging Js_page">
    {{$pageHtml}}
</div>
