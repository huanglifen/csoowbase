@extends('common.main')
@section('title')
加入智库 - 创意世界
@stop 
@section('content')

<div class="detail-imgbg thinktank">
        <div class="detail-img w1180">
        @if(!$user)
           <a href="{{{$baseURL}}}/thinktank/apply-candidate"></a>
        @elseif($user && $user->level > 10 && $user->moblie_phone_no && $user->avatar)
           <a href="{{{$baseURL}}}/thinktank/apply-candidate"></a>
        @else
           <a class="disable" title="未达到申请条件" href="javascript:"></a>
        @endif
        </div>
    </div>
    <div class="thinktank_guide content w1180">
        <div class="height_30"></div>
        <div class="terms_of_apply">
            <h2>满足以下条件即可申请</h2>
            <div class="height_20"></div>
            <ul class="terms clearfix">
                <li class="fl"><img src="{{{$baseURL}}}/images/apply_thinktank_r1_c1.png" alt="申请加入智库专家的条件"/></li>
                <li class="fl"><img src="{{{$baseURL}}}/images/apply_thinktank_r1_c4.png" alt="申请加入智库专家的条件"/></li>
                <li class="fl"><img src="{{{$baseURL}}}/images/apply_thinktank_r1_c6.png" alt="申请加入智库专家的条件"/></li>
            </ul>
        </div>
        <div class="privilege_of_experts">
            <h2>创意世界智库专家特权<span>成为智库专家后您将获得以下特权</span></h2>
            <div class="height_20"></div>
            <ul class="privilege clearfix">
                <li class="fl clearfix">
                    <img class="fl" src="{{{$baseURL}}}/images/apply_thinktank_r3_c1.png" alt="申请加入智库专家的条件"/>
                    <h3>增加关注度</h3>
                    <p>受邀请参加唱一世界线下的重要活动<br>特殊身份标识</p>
                </li>
                <li class="fl clearfix">
                    <img class="fl" src="{{{$baseURL}}}/images/apply_thinktank_r3_c7.png" alt="申请加入智库专家的条件"/>
                    <h3>展现创意价值</h3>
                    <p>被载入创意世界智库名录<br>有机会被载入创意世界创意编年史</p>
                </li>
                <li class="fl clearfix">
                    <img class="fl" src="{{{$baseURL}}}/images/apply_thinktank_r5_c1.png" alt="申请加入智库专家的条件"/>
                    <h3>创福世界</h3>
                    <p>点评创意项目，指点项目走向<br>作为地区创意领袖，提升地区创意能力</p>
                </li>
                <li class="fl clearfix">
                    <img class="fl" src="{{{$baseURL}}}/images/apply_thinktank_r5_c7.png" alt="申请加入智库专家的条件"/>
                    <h3>携手创意领袖</h3>
                    <p>世界级行业创意领袖的智慧宝库<br>全球十万名创意领袖交流的创新平台</p>
                </li>
            </ul></div>
        <div class="join_them">
            <h2>加入他们<span>和他们成为朋友</span></h2>
            <div class="height_20"></div>
            <ul class="experts clearfix">
            @if(isset($guideExperts))
            @foreach($guideExperts as $guideExpert)
                <li class="fl">
                    <img src="{{{App\Common\Utils::getAvatar($guideExpert->avatar)}}}" alt="申请加入智库专家的条件"/>
                    <p>{{{$guideExpert->name}}}</p>
                </li>
            @endforeach
            @endif
            </ul>
        </div>
    </div>
@stop
