@extends('common.main')
@section('title')
智库 - 创意世界
@stop 
@section('content')
<script type="text/javascript" src="{{{$baseURL}}}/js/thinktank.js"></script>
<div class="thinktank-top">
<div class="detail-top w1180">
<div class="pro-btn">
<a href="{{{$baseURL}}}/thinktank/guide" class="joinbtn">申请成为创意世界专家</a>
</div>
</div>
</div>
	<div class="height_30"></div>
	<div class=" thinktank content w1180 clearfix">
		<div class="match-nav clearfix">
			<ul class="navlist Js_tinktankTab clearfix fl">
				<li class="current"><a class="c" href="javascript:">推荐专家</a></li>
				@for($i = 1;$i < count($recommendIndustries) && $i < 6; $i++)
				<li><a class="c" href="javascript:">{{{$recommendIndustries[$i]->title}}}</a></li>
				@endfor
			</ul>
			<a class="more fr Js_mao_more" href="javascript:;">更多</a>
		</div>
		<div class="tt-recommend Js_thinktankCon clearfix">
		@foreach($recommendIndustries as $key => $recommendIndustry)
		   @if($key == 0)
		    <div class="tt-rec-con">
		   @else
		    <div class="tt-rec-con" style="display:none">
		   @endif
		      @foreach($recommendIndustry->users as $industryUser)
				<div class="tt-rec-box">
					<div class="tt-rec-box-peo clearfix">
						<a href="{{{$baseURL}}}/user/home/{{{$industryUser->id}}}"><img class="avatar fl" src="@if(isset($industryUser->expert_avatar)){{{$baseURL}}}/{{{$industryUser->expert_avatar}}}@endif" alt="专家大头像"></a>
						<div class="info fl">
							<span class="t">{{{$industryUser->name}}}</span>
							<span class="des">{{{$industryUser->expert_title}}}</span>
							<ul class="ulist">
								<li title="">@if(isset($industryUser->introduction)){{{$industryUser->introduction}}}@endif</li>
							</ul>
						</div>
					</div>
				</div>
			 @endforeach
			</div>
			@endforeach
		</div>
		<div class="height_20" id="Js_mao_more"></div>
		<div class="tt-filter" id="filter">
			<div class="tt-filter-con">
				<div class="clearfix">
                    <p class="t fl">创意世界智库专家名录</p>
                    <div class="area-sel">
                        <div class="desDropDownMenu tt-area-select fl clearfix">
                            <div class="fl" style="margin-right:5px;">
                                <div class="dropDownMenuInput Js_dropPos">
                                    <s class="dropDownBtn"></s>
                                    <span class="promptInfo"></span>
                                    <ul class="dropDownList"></ul>
                                    <select></select>
                                </div>
                            </div>
                            <div class="fl" style="margin-right:5px;">
                                <div class="dropDownMenuInput Js_dropPos">
                                    <s class="dropDownBtn"></s>
                                    <span class="promptInfo"></span>
                                    <ul class="dropDownList"></ul>
                                    <select></select>
                                </div>
                            </div>
                            <div class="fl" style="margin-right:5px;">
                                <div class="dropDownMenuInput Js_dropPos">
                                    <s class="dropDownBtn"></s>
                                    <span class="promptInfo"></span>
                                    <ul class="dropDownList"></ul>
                                    <select></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="f-list Js_filter_list">
					<b class="ft fl">按组织：</b>
					<p class="fc fl">
						<span><a href="javascript:;"  data-expert-organization-id="0" class="Js_filter_btn Js_show_org" style="color:#0046F7" data-flag="true">全部</a></span>
						<span><a href="javascript:;"  data-expert-organization-id="1" class="Js_filter_btn Js_show_org">创意世界智库主席团</a></span>
						<span><a href="javascript:;"  data-expert-organization-id="2" class="Js_filter_btn Js_show_org">创意世界智库理事会</a></span>
						<span><a href="javascript:;"  data-expert-organization-id="3" class="Js_filter_btn Js_show_org">创意世界智库专家顾问团</a></span>
					</p>
				</div>
				<div class="Js_filter_list">
					<div class="f-list Js_org_item" style="display:none;">
						<b class="ft fl">按职务：</b>
						<p class="fc fl">
							<span><a href="javascript:;" data-expert-duty-id="0" class="Js_filter_btn" style="color:#0046F7" data-flag="true">全部</a></span>
							<span><a href="javascript:;" data-expert-duty-id="11" class="Js_filter_btn">主席</a></span>
							<span><a href="javascript:;" data-expert-duty-id="12" class="Js_filter_btn">副主席</a></span>
						</p>
					</div>
					<div class="f-list Js_org_item" style="display:none;">
						<b class="ft fl">按职务：</b>
						<p class="fc fl">
							<span><a href="javascript:;" data-expert-duty-id="0" class="Js_filter_btn" style="color:#0046F7" data-flag="true">全部</a></span>
							<span><a href="javascript:;" data-expert-duty-id="21" class="Js_filter_btn">理事长 </a></span>
							<span><a href="javascript:;" data-expert-duty-id="22" class="Js_filter_btn">常务理事</a></span>
							<span><a href="javascript:;" data-expert-duty-id="23" class="Js_filter_btn">理事</a></span>
						</p>
					</div>
					<div class="f-list Js_org_item" style="display:none">
						<b class="ft fl">按职务：</b>
						<p class="fc fl">
							<span><a href="javascript:;" data-expert-duty-id="0" class="Js_filter_btn" style="color:#0046F7" data-flag="true">全部</a></span>
						</p>
					</div>
				</div>
				<div class="f-list Js_filter_list">
					<b class="ft fl">按领域：</b>
					<p class="fc fl">
						<span><a href="javascript:;"  data-expert-field-id="0" class="Js_filter_btn" style="color:#0046F7" data-flag="true">全部</a></span>
						<span><a href="javascript:;"  data-expert-field-id="1" class="Js_filter_btn">领导人</a></span>
						<span><a href="javascript:;"  data-expert-field-id="2" class="Js_filter_btn">科学家</a></span>
						<span><a href="javascript:;"  data-expert-field-id="3" class="Js_filter_btn">艺术家</a></span>
						<span><a href="javascript:;"  data-expert-field-id="4" class="Js_filter_btn">企业家</a></span>
						<span><a href="javascript:;"  data-expert-field-id="5" class="Js_filter_btn">慈善家</a></span>
						<span><a href="javascript:;"  data-expert-field-id="6" class="Js_filter_btn">其他</a></span>
					</p>
				</div>
				<div class="f-list Js_filter_list">
					<b class="ft fl">按行业：</b>
					<p class="fc fl">
						<span><a href="javascript:;" data-expert-industry-id="0" class="Js_filter_btn" style="color:#0046F7" data-flag="true">全部</a></span>
						@foreach($industries as $industry)
							<span><a href="javascript:;" data-expert-industry-id="{{$industry->id}}" class="Js_filter_btn">{{$industry->title}}</a></span>
						@endforeach
						<span><a href="javascript:;">更多</a></span>
					</p>
				</div>
			</div>
			<!--<div class="tt-filter-pos">
				<span class="p-block">创意世界智库主席团<a class="close Js_closepos" href="javascript:">X</a></span>
				<span class="lt">></span>
				<span class="p-block">主席<a class="close Js_closepos" href="javascript:">X</a></span>
			</div>-->
		</div>
		<div class="height_30"></div>
		<div class="tt-lists Js_filter_result">
			@include('thinktank.m-experts')
        </div>
	</div>
	<script type="text/javascript" src="{{{$baseURL}}}/js/CityList.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.dropDownMenu.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/thinktank.js"></script>
@stop
