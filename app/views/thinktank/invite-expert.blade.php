@extends('common.main') 
@section('content')
<div class="content w1180 clearfix">
	<div class="height_30"></div>
	<div class="search-content">
		
		<div class="height_40"></div>

	<!--推荐站外专家-->
		<div class="off_site" style="display: block">
		<form id="out_invite_form" action="{{$baseURL}}/project/invite-external-expert" method="post">
			<div class="add_recd_experts">
				<ul>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;">推荐专家姓名：</div>
							<div class="rc">
								<div class="con">
									<input style="position: relative" type="text" placeholder="" name="name" datatype="s" errormsg="" nullmsg="请填写专家姓名!">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;">推荐专家行业：</div>
							<div class="rc">
								<div class="con">
									<input style="position: relative" type="text" placeholder="" name="industry" datatype="s" errormsg="" nullmsg="请填写专家行业!"> 
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;">邮箱：</div>
							<div class="rc">
								<div class="con">
									<input style="position: relative" type="text" placeholder="" name="email" datatype="e" errormsg="" nullmsg="请填写邮箱!">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;">手机：</div>
							<div class="rc">
								<div class="con">
									<input style="position: relative" type="text" placeholder="" name="mobile_phone_no" datatype="m" errormsg="请填写11位的手机号码！" nullmsg="请填写11位的手机号码！">
									<input type="hidden" name="target_type" value="0" />
									<input type="hidden" name="target_id" value="0" />
									<input type="hidden" name="target_sub_id" value="0" />
									<input type="hidden" name="lastPage" value="{{$lastPage}}" />
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;"></div>
							<div class="rc">
								<div class="con">
									<div class="upload-files Js_cover">
										<div class="uploadWrap-single dargArea left">
											<a class="Js_uploadbox" style="vertical-align: middle">
												<button type="submit" class="btn LightGrey Js_out_invite"><span class="inner">上传</span></button>
											</a>
										</div>
										<div class="height_10"></div>
										<div class="clearfix upload-single"></div>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			</form>
			<div class="height_20"></div>
		</div>
	
	
	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/invite.js"></script>
@stop

