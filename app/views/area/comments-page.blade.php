<?php 
if(empty($page)){
	$page = 1;
}
?>


	@include('area.m-comments') 
	
	@if($pageTotal>1)
	<div class="area_discuss_paging">
		@if($page > 1)
		<a class="Js_area_page fl" href="javascript:" data-target-type="{{$targetType}}" data-target-id="{{$id}}" data-page="{{$page-1}}"><i></i>上一页</a>
		@endif
		@if($page != $pageTotal)
		<a class="Js_area_page fr" href="javascript:"  data-target-type="{{$targetType}}" data-target-id="{{$id}}" data-page="{{$page+1}}">下一页<i></i></a>
		@endif
	</div>
	@endif

