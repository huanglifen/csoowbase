<div class="city_creative fl">
	<p class="creative_num">
		<span class="cw_index">创意指数</span> <em>{{$data['creative_index']}}</em>
		<span class="like">{{$data['likes_count']}}用户喜欢</span>
	</p>
	<div class="included clearfix">
		<span class="fl">该地区现有</span>
		<ul class="fl">
			<li class="match"><span>创意世界大赛</span><i>个</i><em>{{$data['contests_count']}}</em></li>
			<li class="line"></li>
			<li class="exchange"><span>创意交易</span><i>个</i><em>{{$data['trades_count']}}</em></li>
			<li class="line"></li>
			<li class="expert"><span>智库专家</span><i>位</i><em>{{$data['experts_count']}}</em></li>
			<li class="line"></li>
			<li class="user"><span>注册用户</span><i>位</i><em>{{$data['users_count']}}</em></li>
		</ul>
	</div>
</div>
