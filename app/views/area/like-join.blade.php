<div class="pro-btn"  data-target-type="{{$targetType}}" data-target-id="{{$targetId}}">
	<a class="like @if($isLiked) on @else Js_likethis @endif" href="javascript:"  title="@if($isLiked) 已喜欢 @else 喜欢 @endif"><span>@if($isLiked) 已喜欢 @else 喜欢 @endif</span></a>
	<a class="join @if($isJoined) on @else Js_join_here  @endif" href="javascript:;" data-id="{{$id}}" data-join-type="{{$joinType}}" title="@if($isJoined) 已加入 @else 加入这里 @endif">@if($isJoined) 已加入 @else 加入这里 @endif</a>
</div>


<script type="text/javascript">
$('.Js_join_here').click(function(){
	var self = $(this),
		id = self.attr('data-id'),
		join_type = $(this).attr('data-join-type');
	WG.fn.ajax('area/join-area',{id:id,join_type:join_type},function(e){
		if(e.status == "ok"){
			self.text('已加入').removeAttr('href').unbind('click');
            window.location.reload()
		}
	});
})
</script>
