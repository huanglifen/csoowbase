<div class="pro pro-last">
	<div class="pro-content">
		<strong>交流区</strong>

		<div class="clearfix content">
			<div class="discuss-wrap">
				
				<div class="dis-sendbox clear">
					<textarea class="inputbox Js_inputbox Js_inputbox_area_comment" name="" id="" cols="30" rows="10"  placeholder="请输入内容..." disabled="disabled"></textarea>
					<div class="dis-bar">
						<a class="dis-subbtn fr Js_dis_btn" href="javascript:" data-target-type="{{$targetType}}" data-target-id="{{$targetId}}">确认</a>
					</div>
				</div>
				<ul class="dis-list Js_dis_list">
					@include('area.comments-page')
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>