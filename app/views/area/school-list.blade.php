@foreach($lists as $list)
 <li class="fl">
    <div class="area_list_item clearfix">
     <a class="area_list_item clearfix" href="{{$baseURL}}/area/school/{{$list->id}}">
       <img class="fl" src="
       @if(!empty($list->logo))
       {{$baseURL}}/{{$list->logo}}
       @else
       {{$baseURL}}/images/icon_area_school.png
       @endif" alt="{{$list->name}}"/>
         <div class="name fl">{{$list->name}}</div>
         </a>
    </div>
</li>
@endforeach