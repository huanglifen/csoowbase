@extends('common.main')

@section('title')
{{$data['name']}} - 创意世界地区
@stop

@section('content')
<?php
	if(!empty($data->district_id)){
		$parentURL = $baseURL.'/area/region/'.$data['district_id'];
	}elseif(!empty($data->city_id)){
		$parentURL = $baseURL.'/area/region/'.$data['city_id'];
	}elseif(!empty($data->province_id)){
		$parentURL = $baseURL.'/area/region/'.$data['province_id'];
	}elseif(!empty($data->nation_id)){
		$parentURL = $baseURL.'/area/region/'.$data['nation_id'];
	}else{
		$parentURL = $baseURL.'/area/index';
	}
?>
<div class="area-top-wap">
	<div class="detail-top w1180">
		@include('area.like-join', array('joinType'=>'72'))
		<div class="city_name">
			<h1 title="{{$data['name']}}">{{$data['name']}}</h1>
            <a class="back_to_parent_area" href="{{$parentURL}}">&lt;&lt;&nbsp;返回上级地区</a>
		</div>
	</div>
</div>
<div class="w1180">
	<div class="city_info_border"></div>
	<div class="city_info clearfix">
		@include('area.area-count')
		<div class="city_intro fl">
			<h2>简介</h2>
            <div class="intro_content">
                <p class="Js_wordLimit">{{$data['intro']}}</p>
            </div>
		</div>
	@include('area.m-news')
	</div>
</div>

<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<ul class="navlist clearfix fl">
				<li class="area_tight @if($type==5)current@endif"><a class="c" href="{{$baseURL}}/area/school/{{$id}}/5" data-category="">创意百科</a></li>
				<li class="area_tight @if($type==0)current@endif"><a class="c" href="{{$baseURL}}/area/school/{{$id}}/0" data-category="">创意世界大赛</a></li>
				<li class="area_tight @if($type==4)current@endif"><a class="c" href="{{$baseURL}}/area/school/{{$id}}/4" data-category="">创意世界交易所</a></li>
				<li class="area_tight @if($type==64)current@endif"><a class="c" href="{{$baseURL}}/area/school/{{$id}}/64" data-category="">创意世界智库专家</a></li>
				<li class="area_tight @if($type==63)current@endif"><a class="c" href="{{$baseURL}}/area/school/{{$id}}/63" data-category="">用户</a></li>
			</ul>
		</div>
		<div class="height_10"></div>
		<!--创意百科-->
		<div class="list_main">
		@if(count($lists))
			<ul id="list_content" class="clearfix">
				@if($type == 5 || $type == 0 || $type == 4)
					@include('common.contest-page')
				@elseif($type == 64 || $type == 63)
					@include('area.user-list')
				@endif
			</ul>
			@if($listsPageTotal > 1)
			<div class="paging">
				{{$pageHtml}}
			</div>
			@endif
	@else
	<div style="margin-right:20px;">
	 <div class="no_content">
	  <div class="tip">暂无</div>
     </div>
     </div>
		@endif
		</div>
	</div>
	<div class="detail-aside">
		@include('area.comment',array('targetType'=>72,'targetId'=>$id))
	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.ellipsis.js" charset="utf-8"></script>
@stop