@foreach($comments as $comment) 
@if(!empty($comment->user))
<li>
	<div class="list-item">
		<div class="item-t">
			<span class="t"><a
				href="{{{$baseURL}}}/user/home/{{{$comment->user->id}}}">{{{$comment->user->name}}}</a></span>

			<div class="level">
				<div class="icon {{{App\Common\Utils::getLevelStyle($comment->user->creative_index)}}}"></div>
				<div class="bar">
					<div class="process" style="width: 22px;"></div>
				</div>
			</div>
		</div>
		<div class="item-con">{{nl2br($comment->content)}}</div>
		<span class="date">{{{App\Common\Utils::formatDate($comment->create_time)}}}</span>
	</div>
</li>
@endif 
@endforeach
