@if($type==7)
	@foreach($lists as $list)
	<?php 
switch ($list->item_type){
	case 2:$tpl = 'school';$img = 'icon_area_school.png';break;
	case 3:$tpl = 'organization';$img = 'icon_area_gov.png';break;
	default:$tpl = 'region';$img = 'icon_area_area.png';
}
    ?>
	<li class="fl">
		<div class="area_list_item clearfix">
			<a class="area_list_item clearfix" href="{{$baseURL}}/area/{{$tpl}}/{{$list->item_id}}">
				<img class="fl" src="
	       @if(!empty($list->logo))
	       {{$list->logo}}
	       @else
	       {{$baseURL}}/images/{{$img}}
	       @endif" alt="{{$list->name}}"/>
				<div class="name fl">{{$list->name}}</div>
			</a>
		</div>
	</li>
	@endforeach
@elseif($type==71)
	@foreach($lists as $list)
	<li class="fl">
		<div class="area_list_item clearfix">
			<a class="area_list_item clearfix" href="{{$baseURL}}/area/region/{{$list->id}}">
				<img class="fl" src="
	       @if(!empty($list->logo))
	       {{$list->logo}}
	       @else
	       {{$baseURL}}/images/icon_area_area.png
	       @endif" alt="{{$list->name}}"/>
				<div class="name fl">{{$list->name}}</div>
			</a>
		</div>
	</li>
	@endforeach
@elseif($type==72)
	@foreach($lists as $list)
	<li class="fl">
		<div class="area_list_item clearfix">
			<a class="area_list_item clearfix" href="{{$baseURL}}/area/school/{{$list->id}}">
				<img class="fl" src="
	       @if(!empty($list->logo))
	       {{$list->logo}}
	       @else
	       {{$baseURL}}/images/icon_area_school.png
	       @endif" alt="{{$list->name}}"/>
				<div class="name fl">{{$list->name}}</div>
			</a>
		</div>
	</li>
	@endforeach
@elseif($type==73)
	@foreach($lists as $list)
	<li class="fl">
		<div class="area_list_item clearfix">
			<a class="area_list_item clearfix" href="{{$baseURL}}/area/organization/{{$list->id}}">
				<img class="fl" src="
	       @if(!empty($list->logo))
	       {{$list->logo}}
	       @else
	       {{$baseURL}}/images/icon_area_gov.png
	       @endif" alt="{{$list->name}}"/>
				<div class="name fl">{{$list->name}}</div>
			</a>
		</div>
	</li>
	@endforeach
@endif