<div class="map-city-list">
	<dl>
		<dt>直辖市</dt>
		<dd class="clearfix">
			<ul>
				<li><a  @if($id==10)class="cur"@endif href="{{{$baseURL}}}/area/index/10">北京</a></li>
				<li><a  @if($id==18)class="cur"@endif href="{{{$baseURL}}}/area/index/18">上海</a></li>
				<li><a  @if($id==11)class="cur"@endif href="{{{$baseURL}}}/area/index/11">天津</a></li>
				<li><a  @if($id==31)class="cur"@endif href="{{{$baseURL}}}/area/index/31">重庆</a></li>
			</ul>
		</dd>
		<dt>省及自治区</dt>
		<dd>
			<ul>
				<li><a @if($id==12)class="cur"@endif href="{{{$baseURL}}}/area/index/12">河北</a></li>
				<li><a @if($id==13)class="cur"@endif href="{{{$baseURL}}}/area/index/13">山西</a></li>
				<li><a @if($id==14)class="cur"@endif href="{{{$baseURL}}}/area/index/14">内蒙古</a></li>
				<li><a @if($id==15)class="cur"@endif href="{{{$baseURL}}}/area/index/15">辽宁</a></li>
				<li><a @if($id==16)class="cur"@endif href="{{{$baseURL}}}/area/index/16">吉林</a></li>
				<li><a @if($id==17)class="cur"@endif href="{{{$baseURL}}}/area/index/17">黑龙江</a></li>
				<li><a @if($id==24)class="cur"@endif href="{{{$baseURL}}}/area/index/24">山东</a></li>
				<li><a @if($id==19)class="cur"@endif href="{{{$baseURL}}}/area/index/19">江苏</a></li>
				<li><a @if($id==21)class="cur"@endif href="{{{$baseURL}}}/area/index/21">安徽</a></li>
				<li><a @if($id==20)class="cur"@endif href="{{{$baseURL}}}/area/index/20">浙江</a></li>
				<li><a @if($id==23)class="cur"@endif href="{{{$baseURL}}}/area/index/23">江西</a></li>
				<li><a @if($id==22)class="cur"@endif href="{{{$baseURL}}}/area/index/22">福建</a></li>
				<li><a @if($id==25)class="cur"@endif href="{{{$baseURL}}}/area/index/25">河南</a></li>
				<li><a @if($id==26)class="cur"@endif href="{{{$baseURL}}}/area/index/26">湖北</a></li>
				<li><a @if($id==27)class="cur"@endif href="{{{$baseURL}}}/area/index/27">湖南</a></li>
				<li><a @if($id==28)class="cur"@endif href="{{{$baseURL}}}/area/index/28">广东</a></li>
				<li><a @if($id==29)class="cur"@endif href="{{{$baseURL}}}/area/index/29">广西</a></li>
				<li><a @if($id==30)class="cur"@endif href="{{{$baseURL}}}/area/index/30">海南</a></li>
				<li><a @if($id==32)class="cur"@endif href="{{{$baseURL}}}/area/index/32">四川</a></li>
				<li><a @if($id==34)class="cur"@endif href="{{{$baseURL}}}/area/index/34">云南</a></li>
				<li><a @if($id==33)class="cur"@endif href="{{{$baseURL}}}/area/index/33">贵州</a></li>
				<li><a @if($id==36)class="cur"@endif href="{{{$baseURL}}}/area/index/36">陕西</a></li>
				<li><a @if($id==37)class="cur"@endif href="{{{$baseURL}}}/area/index/37">甘肃</a></li>
				<li><a @if($id==38)class="cur"@endif href="{{{$baseURL}}}/area/index/38">青海</a></li>
				<li><a @if($id==39)class="cur"@endif href="{{{$baseURL}}}/area/index/39">宁夏</a></li>
				<li><a @if($id==40)class="cur"@endif href="{{{$baseURL}}}/area/index/40">新疆</a></li>
				<li><a @if($id==35)class="cur"@endif href="{{{$baseURL}}}/area/index/35">西藏</a></li>
			</ul>
		</dd>
		<dt>港澳台</dt>
		<dd>
			<ul>
				<li><a @if($id==41)class="cur"@endif href="{{{$baseURL}}}/area/region/41">香港</a></li>
				<li><a @if($id==42)class="cur"@endif href="{{{$baseURL}}}/area/region/42">澳门</a></li>
				<li><a @if($id==43)class="cur"@endif href="{{{$baseURL}}}/area/region/43">台湾</a></li>
			</ul>
		</dd>
	</dl>
</div>

