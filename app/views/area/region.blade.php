@extends('common.main')

@section('title')
{{$data['name']}} - 创意世界地区
@stop

@section('content')
<?php
if(strlen($data->id) == 1){
	$parentURL = $baseURL.'/area/index';
}elseif(strlen($data->id) == 2){
	$parentURL = $baseURL.'/area/region/'.$data['nation_id'];
}elseif(strlen($data->id) == 4){
	$parentURL = $baseURL.'/area/region/'.$data['province_id'];
}elseif(strlen($data->id) == 6){
	$parentURL = $baseURL.'/area/region/'.$data['city_id'];
}else{
	$parentURL = $baseURL.'/area/index';
}
?>
<div class="area-top-wap">
	<div class="detail-top w1180">
		@include('area.like-join', array('joinType'=>'71'))
		<div class="city_name">
			<h1 title="{{$data['name']}}">{{$data['name']}}</h1>
            <a class="back_to_parent_area" href="{{$parentURL}}">&lt;&lt;&nbsp;返回上级地区</a>
		</div>
	</div>
</div>
<div class="w1180">
	<div class="city_info_border"></div>
	<div class="city_info clearfix">
		@include('area.area-count')
		<div class="city_intro fl">
			<h2>简介</h2>
            <div class="intro_content">
                <p class="Js_wordLimit">{{$data['intro']}}</p>
            </div>
            <!-- <a class="back_to_parent_area" href="">&lt;&lt;&nbsp;返回上级地区</a> -->
		</div>
		@include('area.m-news')
	</div>
</div>
<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<ul class="navlist clearfix fl">
				<li class="area_tight @if($type==5)current@endif"><a class="c" href="{{$baseURL}}/area/region/{{$id}}/5" data-category="">创意百科</a></li>
				<li class="area_tight @if($type==0)current@endif"><a class="c" href="{{$baseURL}}/area/region/{{$id}}/0" data-category="">创意世界大赛</a></li>
				<li class="area_tight @if($type==4)current@endif"><a class="c" href="{{$baseURL}}/area/region/{{$id}}/4" data-category="">创意世界交易所</a></li>
				<li class="area_tight @if($type==64)current@endif"><a class="c" href="{{$baseURL}}/area/region/{{$id}}/64" data-category="">创意世界智库专家</a></li>
				<li class="area_tight @if($type==63)current@endif"><a class="c" href="{{$baseURL}}/area/region/{{$id}}/63" data-category="">用户</a></li>
				@if(strlen($id) < 6)
				<li class="area_tight @if($type==71)current@endif"><a class="c" href="{{$baseURL}}/area/region/{{$id}}/71" data-category="">地区</a></li>
				<li class="area_tight @if($type==72)current@endif"><a class="c" href="{{$baseURL}}/area/region/{{$id}}/72" data-category="">学校</a></li>
				<li class="area_tight last @if($type==73)current@endif"><a class="c" href="{{$baseURL}}/area/region/{{$id}}/73" data-category="">政企</a></li>
				@endif
			</ul>
		</div>
		<div class="height_10"></div>
		<!--创意百科-->
		<ul class="list_main">
		@if(count($lists))
            @if($type == 5 || $type == 0 || $type == 4)
            @include('common.contest-page')
            @elseif($type == 64)
            @include('area.user-list')
            @elseif($type == 63)
            @include('area.user-list')
            @elseif($type == 71)
            @include('area.region-list')
            @elseif($type == 72)
            @include('area.school-list')
            @elseif($type == 73)
            @include('area.organization-list')
            @endif
        @if($listsPageTotal > 1)
        <div class="paging">
            {{$pageHtml}}
        </div>
        @endif
		@else
        <div style="margin-right:20px;">
             <div class="no_content">
                  <div class="tip">暂无</div>
             </div>
         </div>
		@endif
		</ul>
	</div>
	<div class="detail-aside">
		@include('area.comment',array('targetType'=>71,'targetId'=>$id))
	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.ellipsis.js" charset="utf-8"></script>
@stop