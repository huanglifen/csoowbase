@if(empty($id))

<div class="map-img-parent">
	<img src="{{{$baseURL}}}/images/map/world.png" />
	<div class="map-img" id="Js_map_city">
		<a href="{{{$baseURL}}}/area/index/1" class="country china" data-id=1>中国</a>
	</div>
</div>
@elseif($id == 1)
<div class="map-img-parent">
	<div class="map-img" id="Js_map_city">
		<img src="{{{$baseURL}}}/images/map/china.png"/>
		<a href="{{{$baseURL}}}/area/index/10" class="country beijing" data-id=10>北京</a>
		<a href="{{{$baseURL}}}/area/index/18" class="country shanghai" data-id=18>上海</a>
		<a href="{{{$baseURL}}}/area/index/11" class="country tianjin" data-id=11>天津</a>
		<a href="{{{$baseURL}}}/area/index/31" class="country chongqing" data-id=31>重庆</a>
		<a href="{{{$baseURL}}}/area/index/12" class="country hebei" data-id=12>河北</a>
		<a href="{{{$baseURL}}}/area/index/13" class="country shanxi" data-id=13>山西</a>
		<a href="{{{$baseURL}}}/area/index/14" class="country neimenggu" data-id=14>内蒙古</a>
		<a href="{{{$baseURL}}}/area/index/15" class="country liaoning" data-id=15>辽宁</a>
		<a href="{{{$baseURL}}}/area/index/16" class="country jilin" data-id=16>吉林</a>
		<a href="{{{$baseURL}}}/area/index/17" class="country heilongjiang" data-id=17>黑龙江</a>
		<a href="{{{$baseURL}}}/area/index/24" class="country shandong" data-id=24>山东</a>
		<a href="{{{$baseURL}}}/area/index/19" class="country jiangsu" data-id=19>江苏</a>
		<a href="{{{$baseURL}}}/area/index/21" class="country anhui" data-id=21>安徽</a>
		<a href="{{{$baseURL}}}/area/index/20" class="country zhejiang" data-id=20>浙江</a>
		<a href="{{{$baseURL}}}/area/index/23" class="country jiangxi" data-id=23>江西</a>
		<a href="{{{$baseURL}}}/area/index/22" class="country fujian" data-id=22>福建</a>
		<a href="{{{$baseURL}}}/area/index/25" class="country henan" data-id=25>河南</a>
		<a href="{{{$baseURL}}}/area/index/26" class="country hubei" data-id=26>湖北</a>
		<a href="{{{$baseURL}}}/area/index/27" class="country hunan" data-id=27>湖南</a>
		<a href="{{{$baseURL}}}/area/index/28" class="country guangdong" data-id=28>广东</a>
		<a href="{{{$baseURL}}}/area/index/29" class="country guangxi" data-id=29>广西</a>
		<a href="{{{$baseURL}}}/area/index/30" class="country hainan" data-id=30>海南</a>
		<a href="{{{$baseURL}}}/area/index/32" class="country sichuan" data-id=32>四川</a>
		<a href="{{{$baseURL}}}/area/index/34" class="country yunnan" data-id=34>云南</a>
		<a href="{{{$baseURL}}}/area/index/33" class="country guizhou" data-id=33>贵州</a>
		<a href="{{{$baseURL}}}/area/index/36" class="country shanxi_s" data-id=36>陕西</a>
		<a href="{{{$baseURL}}}/area/index/37" class="country gansu" data-id=37>甘肃</a>
		<a href="{{{$baseURL}}}/area/index/38" class="country qinghai" data-id=38>青海</a>
		<a href="{{{$baseURL}}}/area/index/39" class="country ningxia" data-id=39>宁夏</a>
		<a href="{{{$baseURL}}}/area/index/35" class="country xizang" data-id=35>西藏</a>
		<a href="{{{$baseURL}}}/area/index/40" class="country xinjiang" data-id=40>新疆</a>
		<a href="{{{$baseURL}}}/area/region/41" class="country hongkong" data-id=41>香港</a>
		<a href="{{{$baseURL}}}/area/region/42" class="country macao" data-id=42>澳门</a>
		<a href="{{{$baseURL}}}/area/region/43" class="country taiwan" data-id=43>台湾</a>
	</div>
</div>
<a href="{{{$baseURL}}}/area/index" class="map-return world"></a>
@else
<?php include('./data/html/province/'.$id.'.php');?>
<a href="{{{$baseURL}}}/area/index/1" class="map-return"></a>
@endif
