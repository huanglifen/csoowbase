<div class="map_popup clearfix" id="Js_city_tag">
    <a href="{{$baseURL}}/area/region/{{$region['id']}}" target="_blank" class="clearfix">
        <img src="{{$baseURL}}/{{$region['logo']}}" alt="{{$region['name']}}"/>

        <div class="popup_content">
            <h3>{{$region['name']}}<span></span></h3>

            <p>创意指数：<span>{{$region['creative_index']}}</span></p>
        </div>
        <i class="popup_array"><em></em><span></span></i>
    </a>
</div>