@extends('common.main')

@section('title')
{{{$data['name']}}} - 创意世界
@stop

@section('content')
<div class="map">
	<div class="map-container w1180">
		@include('area.ditu')
		@if($id)
			@include('area.daohang')
		@endif
	</div>
</div>
<div class="content w1200">
<div class="height_20"></div>
<div class="cwindex-rank" style="margin: 0 10px">
	<div class="cwindex-title">
		<p class="line" style="border-width: 1px"></p>

		<p class="ctitle"><span style="font-weight: normal"><span>{{$data['name']}}</span>创意指数排行榜</span></p>
	</div>
	<div class="height_10"></div>
</div>
<div class="height_10"></div>
<div class="map_ranking clearfix">
<ul class="rank_list fl">
	<li>
		<img src="{{{$baseURL}}}/images/icon_area_map_area_rank_list.jpg" alt="地区排名"/>
		<h2 class="area">地区排名</h2>
		<ul class="nx_lv">
			@if($regions)
				@foreach($regions as $k=>$region)
				<li class="@if($k<3)top3@endif clearfix">
					<i>{{$k+1}}</i>
					<a href="{{$baseURL}}/area/region/{{$region->id}}"><em>{{$region->name}}</em></a>
					<span>{{$region->creative_index}}</span>
				</li>
				@endforeach
			@endif
		</ul>
	</li>
</ul>
<ul class="rank_list fl">
	<li>
		<img src="{{{$baseURL}}}/images/icon_area_map_school_rank_list.jpg" alt="学校排名"/>
		<h2 class="school">学校排名</h2>
		<ul class="nx_lv">
			@if($schools)
				@foreach($schools as $k=>$school)
				<li class="@if($k<3)top3@endif clearfix">
					<i>{{$k+1}}</i>
					<a href="{{$baseURL}}/area/school/{{$school->id}}"><em>{{$school->name}}</em></a>
					<span>{{$school->creative_index}}</span>
				</li>
				@endforeach
			@endif
		</ul>
	</li>
</ul>
<ul class="rank_list fl">
	<li>
		<img src="{{{$baseURL}}}/images/icon_area_map_administrative_machinery_rank_list.jpg" alt="行政机构排名"/>

		<h2 class="administrative">行政机构排名</h2>
		<ul class="nx_lv">
			@if($goverments)
				@foreach($goverments as $k=>$goverment)
				<li class="@if($k<3)top3@endif clearfix">
					<i>{{$k+1}}</i>
					<a href="{{$baseURL}}/area/organization/{{$goverment->id}}"><em>{{$goverment->name}}</em></a>
					<span>{{$goverment->creative_index}}</span>
				</li>
				@endforeach
			@endif
		</ul>
	</li>
</ul>
<ul class="rank_list fl">
	<li>
		<img src="{{{$baseURL}}}/images/icon_area_map_enterprise_rank_list.jpg" alt="企业排名"/>

		<h2 class="enterprise">企业排名</h2>
		<ul class="nx_lv">
			@if($coporates)
				@foreach($coporates as $k=>$coporate)
				<li class="@if($k<3)top3@endif clearfix">
					<i>{{$k+1}}</i>
					<a href="{{$baseURL}}/area/organization/{{$coporate->id}}"><em>{{$coporate->name}}</em></a>
					<span>{{$coporate->creative_index}}</span>
				</li>
				@endforeach
			@endif
		</ul>
	</li>
</ul>
</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/map.js"></script>

@stop