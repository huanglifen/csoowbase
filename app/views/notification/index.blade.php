@extends('common.main')

@section('title')
消息中心 - 创意世界 
@stop

@section('content')

	<div class="content w1180 clearfix">
		<div class="detail-content">
			<div class="infolist_wraper_area">
				<div class="infohead">
					<h3 class="infotitle">我的@if($status==0)未读@else已读@endif消息</h3>
					<a class="infobtn @if($status==0)active@endif" href="{{{$baseURL}}}/notification/index/0">未读</a>
           			<a class="infobtn @if($status==1)active@endif" href="{{{$baseURL}}}/notification/index/1" style="margin-right: 5px">已读</a>
				</div>
				@if(count($notifications))
				<ul>
				@foreach($notifications as $notification)
				<?php  $info = json_decode($notification->info); ?>
				 @if(!is_object($info))
                   <?php  $target_id = $notification->info;?>
                 @else
                  <?php $info = json_decode($notification->info); 
                        $target_id = $info->target_id;
                   ?>  
                 @endif

					<li class="infolist_area">
						<a class="infolink_area" href="{{{App\Common\Utils::getNotificationUrl($notification->target_type, $target_id)}}}">
							<img class="info_icon_area" src="{{{$baseURL}}}/images/icon_info_right.png"/>
							<span class="info_tip_area color_blue_area">{{{$notification->content}}}</span>
							<span class="info_time_area"><em>{{date('Y-m-d H:i:s',$notification->create_time)}}</em></span>
						</a>
					</li>
				@endforeach	
				</ul>
				@if($pageTotal > 1)
				<div class="paging">
					{{$pageHtml}}
				</div>
				@endif
					
	@else
	    <div class="no_content" style="border:0px;">
	  <div class="tip">暂无</div>
     </div>
		@endif
			</div>
		</div>
		<div class="detail-aside" style="padding-top:42px;">
	        
		    @include('common.hot-contests')
	
			@include('common.hot-exchanges')
	
			@include('common.care-people')
	
			@include('common.complete-info')
		        
		</div>
	</div>
@stop
	