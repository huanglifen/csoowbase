@if(count($members))
<div class="area-list-parent clearfix">
@foreach($members as $member)
<div class="card fl card-list">
	<div class="card-bg"></div>
	<div class="card-container">
		<div class="user-info">
			<div class="user-address clearfix">
				<a href="{{{$baseURL}}}/user/home/{{{$member->user->id}}}" class="fl"><img src="{{{App\Common\Utils::getAvatar($member->user->avatar)}}}" /></a>
				<div class="fl">
					<div class="clearfix card-u">
						<a class="user-name" href="{{{$baseURL}}}/user/home/{{{$member->user->id}}}" >{{{$member->user->name}}}</a>
						<div class="level">
							<div class="icon {{{App\Common\Utils::getLevelStyle($member->user->creative_index)}}}"></div>
							<div class="bar">
								<div class="process" style="width: 60%;"></div>
							</div>
						</div>
					@if($member->user->group_id == App\Modules\UserModule::GROUP_EXPERT)		
					<?php $expertTitle = App\Common\Utils::getExpertTitle($member->user->expert_organization_id);
                          $expertStyle = App\Common\Utils::getExpertStyle($member->user->expert_organization_id);
                     ?>
			         <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			         @endif
					</div>
					@if($type == 2)
					@if($member->type == 1)
					<?php $info = '已关注';?>
					@else
					<?php $info = '关注';?>
					@endif
					<a href="#" class="card-btn @if($member->type == 2)Js_follow_btn @else Js_has_followed_btn @endif" style="margin-top: 8px" user-id="{{{$member->user->id}}}">{{{$info}}}</a>
					@else
					<a class="card-area Js_circle_card" href="javascript:" data-user-id = "{{{$member->user->id}}}"><span class="Js_circle_number">{{{$member->circle_num}}}</span>个圈子<i class="array"><em></em><span></span></i></a>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endforeach
</div>
{{$page}}
@elseif($cur == 1)
@if($type==1)
<div class="area-no-friend">
	先往圈子里加点人吧<br />
	查找你可能认识的人，或者查看你圈了的人。
</div>
@else
<div class="area-no-friend">
	还没有人圈你。
</div>
@endif
@endif