<div class="addFriend-box Js_addFriend_box" style="display: block" data-member-id="{{{$member->id}}}">
        <div class="addFriendDiv">
            <ul id="Js_circleList">
            @foreach($circles as $circle)
                <li class="clearfix" data-circle-id="{{{$circle->id}}}">
                    <label class="circle_name" data-type="py"><input type="checkbox" @if($circle->member_id) checked="checked"@endif>{{{$circle->name}}}</label>
                </li>
             @endforeach
            </ul>
        </div>
        <div class="addFormDiv">
            <a href="javascript:" class="lys Js_add_circle" style="display: none">创建新的圈子</a>
            <div style="display:block;">
                <label><input type="text" placeholder="请输入分组名称"><a href="javascript:" class="addBtn Js_addBtn">创建</a></label>
            </div>
        </div>
    </div>
