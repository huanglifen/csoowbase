@foreach($members as $member)
  @if($toUser && $member->id == $toUser->id)
<?php
    $class = "current Js_contact_user";
 ?>
    @else
 <?php $class = "Js_contact_user"; ?>
@endif
<li class="{{{$class}}}" member-id="{{{$member->id}}}">
	<div class="list-item">
		<img class="avatar"
			src="{{{App\Common\Utils::getAvatar($member->avatar)}}}" alt="">
		<div class="item-t">
			<span class="t" title="{{{$member->name}}}"><b>{{{$member->name}}}</b></span>
			<div class="level">
				<div class="icon {{{App\Common\Utils::getLevelStyle($member->creative_index)}}}"></div>
				<div class="bar">
					<div class="process" style="width: 22px;"></div>
				</div>
			</div>
			@if($member->group_id == 2) <span class="mark">专家</span> @endif
		</div>
		<div class="user-des">
			@if($member->chat) <span title="{{{$member->chat->content}}}">{{{$member->chat->content}}}</span>
			@endif
		</div>
	</div>
</li>
@endforeach
