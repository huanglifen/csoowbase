@extends('common.main')
@section('title')
朋友圈 - 创意世界
@stop
@section('content')	
@include('user.user-top-circle')
	<div class="content w1180 clearfix">
		<div class="match-nav clearfix">
			<div class="height_17"></div>
			<ul class="navlist clearfix fl" style="width:100%">
				<li><a class="c" href="{{{$baseURL}}}/dynamic">朋友圈动态</a></li>
				<li><a class="c" href="{{{$baseURL}}}/chat">朋友圈消息</a></li>
				<li class="current"><a class="c" href="{{{$baseURL}}}/chat/contact">朋友圈联系人</a></li>
				<li><a class="c" href="{{{$baseURL}}}/circle">我的朋友圈</a></li>
			</ul>
		</div>
		<div class="height_20"></div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.dropDownMenu.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/circle.js"></script>
<div class="circle-chat">
			<div class="circle-chat-side fl">
				<div class="circle-select">
                    <div class="dropDownMenu w187">
                        <div class="dropDownMenuInput Js_industryMenu">
                            <s class="dropDownBtn"></s>
                            <span class="promptInfo">请选择...</span>
                            <ul class="dropDownList" data-circle-id="{{{$circleId}}}"></ul>
                            <select></select>
                        </div>
                    </div>
				</div>
    
				<ul class="contact-list Js_contact_list">
				@include('circle.m-contact-list')
				</ul>
			</div>
			<div class="circle-chat-con fl">
				<div class="circle-msg-list Js_dialoglist">
				    @if(isset($show)&& $show)			
                    <div class="chat-more-container">
                            <a class="chat-more Js_chat_checkmore" href="javascript:" title="点击查看更多" style="display: block">查看更多聊天记录</a>
                        </div>
                    @endif
					<ul class="Js_contact_chat" style="overflow:hidden; zoom:1">
					@include('circle.m-contact')
					</ul>
				</div>
				<div class="circle-inputbox">
					<textarea class="textbox Js_textarea" name="" id=""></textarea>
					<p class="input-bar">
						<a class="msg-btn Js_sendMsgBtn" href="javascript:">发送</a>
					</p>
				</div>
			</div>
		</div>
<script>
var industryJson = [
                    {
                        name:'全部圈子',
                        attr : {id:'-1'}
                    }
                ];
    <?php foreach($circles as $circle) {
    ?> 
    var name = '<?php echo $circle->name;?>';
    var circleId = '<?php echo $circle->id;?>';
    var data = {name:name, attr:{id:circleId}};
    industryJson.push(data);
    <?php }?>
    var data = {name:"陌生人", attr:{id:'0'}};
    industryJson.push(data);

</script>
</div>
	</div>
@stop