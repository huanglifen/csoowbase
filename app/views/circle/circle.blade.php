@extends('common.main') 
@section('title') 
朋友圈 - 创意世界
@stop
@section('content') 
@include('user.user-top-circle')
<div class="content w1180 clearfix">
	<div class="detail-content" style="width: 1180px;">
		<div class="match-nav clearfix">
			<ul class="navlist clearfix fl" style="width: 100%">
				<li><a class="c" href="{{{$baseURL}}}/dynamic">朋友圈动态</a></li>
				<li><a class="c" href="{{{$baseURL}}}/chat">朋友圈消息</a></li>
				<li><a class="c" href="{{{$baseURL}}}/chat/contact">朋友圈联系人</a></li>
				<li class="current"><a class="c" href="{{{$baseURL}}}/circle">我的朋友圈</a></li>
			</ul>
		</div>
		<div class="sub-nav clearfix">
			<ul class="subnav-list fl clearfix">
				<li @if($type== 1)class="current" @endif><a
					href="{{{$baseURL}}}/circle">我的圈子</a></li>
				<li @if($type== 2)class="current" @endif><a
					href="{{{$baseURL}}}/circle/index/2">圈我的人</a></li>
			</ul>
			@if($circleId > 0)
			<a class="create-new-area fr Js_edit_circle_a" href="javascript:;">编辑圈子</a> 
			<a class="create-new-area fr Js_del_circle_a" href="javascript:;" data-circle-id="{{{$circleId}}}">删除圈子</a> 
			@else
			<a class="create-new-area fr Js_create_circle_a" href="javascript:;">创建新圈子</a> 
			@endif
			@if($type == 1)
			<div class="desDropDownMenu friend-area-select fr clearfix">
				<div class="fl">
					<div class="dropDownMenuInput Js_areaList">
						<s class="dropDownBtn"></s> <span class="promptInfo"></span>
						<ul class="dropDownList"></ul>
						<select></select>
					</div>
				</div>
			</div>
			@endif 
			
			@include('circle.m-circle')
		</div>
	</div>
</div>

<div class="Js_create_content" style="display:none;">
	<div class="message_box">
		<div class="box_tittle">
			<span class="t fl">创建新圈子</span>
			<a class="close fr Js_close_content" href="javascript:" title="关闭"></a>
		</div>
		<div class="box_content">
			<div class="choose_group">
				<ul class="normal_group clearfix">
				@foreach($circles as $circle)
					<li class="fl"><label>{{{$circle->name}}}</label></li>
				@endforeach
				</ul>
				<div class="new_group" style="height: 46px">
					<label>
						<input type="text" placeholder="请输入圈子名称" class="Js_create_circle_name" value=""/>
						<a class="new_group_btn Js_create_circle_btn" href="javascript:">创建</a>
					</label><br/>
					<div class="illus Js_show_create_circle_error" style="display:none;"><span></span></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="Js_edit_content" style="display:none;">
	<div class="message_box">
		<div class="box_tittle">
			<span class="t fl">编辑圈子名称</span>
			<a class="close fr Js_close_content" href="javascript:" title="关闭"></a>
		</div>
		<div class="box_content">
			<div class="choose_group">
				<ul class="normal_group clearfix">
					<li class="fl"><label>当前圈子名称：<span id="Js_current_circle_name">全部圈子</span></label></li>
				</ul>
				<div class="new_group">
					<label>
						<input type="text" placeholder="请输入圈子名称" class="Js_edit_circle_name">
						<a class="new_group_btn Js_edit_circle_name_btn" href="javascript:" data-circle-id="{{{$circleId}}}">确认</a>
					</label><br/>
					<div class="illus Js_show_create_circle_error" style="display:none;"><span></span></div>
				</div>
			</div>
		</div>
		<div style="height:10px;"></div>
	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/myFriendCircle.js"></script>
<script type="text/javascript"
	src="{{{$baseURL}}}/js/jquery.dropDownMenu.js"></script>
@if($type == 1)
<script>
var key = 0;
var areaList = [{name:"全部圈子", attr:{id:0}}]
<?php foreach($circles as $key => $circle) {
    if($circle->id == $circleId) {
    ?> 
    var key = '<?php  echo $key + 1?>';
	$("#Js_current_circle_name").text('<?php echo $circle->name ?>')
    <?php }?>
    var name = '<?php echo $circle->name;?>';
    var circleId = '<?php echo $circle->id;?>';
    var data = {name:name, attr:{id:circleId}};
    areaList.push(data);
    <?php }?>
    var circle_id = '<?php echo $circleId; ?>';
$('.Js_areaList').dropDownMenu({
	data : areaList,
	format : true,
	firstCallback : false,
	id : key,
	callback : function(ev){
		if(ev.id != circle_id) {			
		window.location.href = baseUrl + 'circle/index/1/'+ev.id;
		}
	}	
});
</script>
@endif @stop
