@extends('common.main') 
@section('title') 
朋友圈 - 创意世界
@stop 
@section('content')
@include('user.user-top-circle')
<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<ul class="navlist clearfix fl">
				<li class="current"><a class="c" href="{{{$baseURL}}}/dynamic">朋友圈动态</a></li>
				<li><a class="c" href="{{{$baseURL}}}/chat">朋友圈消息</a></li>
				<li><a class="c" href="{{{$baseURL}}}/chat/contact">朋友圈联系人</a></li>
				<li><a class="c" href="{{{$baseURL}}}/circle">我的朋友圈</a></li>
			</ul>
		</div>
		<div class="sub-nav">
			<ul class="subnav-list clearfix">
				<li @if($circleId == 0)class="current" @endif><a href="{{{$baseURL}}}/dynamic/index">全部</a></li> 
				@foreach($circles as $circle)
				<li @if($circle->id == $circleId) class="current" @endif><a href="{{{$baseURL}}}/dynamic/index/{{{$circle->id}}}">{{{$circle->name}}}</a></li> 
				@endforeach
			</ul>
		</div>
		@if(count($datas))
		<ul class="dis-list Js_dis_list_content">
		@include('common.dynamic')
		</ul>
		@else
			<div class="Js_no_dynamic_data">
		<div class="height_20"></div>
	    <div class="no_content">
	  <div class="tip">暂无</div>
       </div>
       </div>
		@endif
	</div>

	<div class="detail-aside">
		    @include('common.hot-contests')
	        @include('common.hot-exchanges')
			@include('common.care-people')
			@include('common.complete-info')
	</div>

</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/friendCircle.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.artZoom.js"></script>
<script>
<?php if($circleId == 0){?>
var shareThisPage = true;
<?php }?>
jQuery(function ($) {
	$('.artZoom').artZoom({
		preload: true,		// 设置是否提前缓存视野内的大图片
		blur: true,			// 设置加载大图是否有模糊变清晰的效果
		maxWidth: 650,
		// 语言设置
		left: '左旋转',		// 左旋转按钮文字
		right: '右旋转',		// 右旋转按钮文字
		source: '看原图'		// 查看原图按钮文字
	});
});
</script>
@stop

