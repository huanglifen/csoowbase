@foreach($notifications as $notification)
<li class="infolist_area">
<a class="infolink_area" href="{{{$baseURL}}}/chat/contact/{{{$notification->user->id}}}"> 
<img class="info_icon_area" src="{{{App\Common\Utils::getAvatar($notification->user->avatar)}}}" /> 
<span class="info_tip_area color_blue_area">{{{$notification->user->name}}}</span>
<span class="info_tip_area color_black_area">{{{mb_substr($notification->content,0,40)}}}</span> 
<span class="info_time_area"><em>{{{date('Y-m-d H:i:s',$notification->create_time)}}}</em></span>
</a></li>
@endforeach
