@extends('common.main') 
@section('title') 
朋友圈 - 创意世界
@stop 
@section('content')
@include('user.user-top-circle')
<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<ul class="navlist clearfix fl">
				<li><a class="c" href="{{{$baseURL}}}/dynamic">朋友圈动态</a></li>
				<li class="current"><a class="c" href="{{{$baseURL}}}/chat">朋友圈消息</a></li>
				<li><a class="c" href="{{{$baseURL}}}/chat/contact">朋友圈联系人</a></li>
				<li><a class="c" href="{{{$baseURL}}}/circle">我的朋友圈</a></li>
			</ul>
		</div>
		<div class="sub-nav">
			<ul class="subnav-list clearfix">
				<li @if($type == 0)class="current" @endif><a href="{{{$baseURL}}}/chat/index">未读消息</a></li>
				<li @if($type == 1)class="current" @endif><a href="{{{$baseURL}}}/chat/index/1">已读消息</a></li>
			</ul>
		</div>
	
	@if(count($notifications))
	<div class="infolist_wraper_area">	
			<div class="infohead">
				<h3 class="infotitle">我的消息</h3>
			</div>
			
			<ul>
            @include("circle.m-notification")
			</ul>
			{{$page}}
	</div>
	@else
	    <div class="no_content" style="margin-top:25px;">
	  <div class="tip">暂无</div>
     </div>
		@endif
		
	</div>
	<div class="detail-aside">
	    @include('common.hot-contests')
		@include('common.hot-exchanges') 
		@include('common.care-people')
		@include('common.complete-info')
	</div>
</div>
@stop
