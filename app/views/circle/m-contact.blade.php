<?php $length = count($chats); $key = $length-1;?>
@for(;$key>=0; $key--) 
    <?php $chat = $chats[$key];?>
@if($chat->user_id == $user->id)
    <?php $class = 'msg left-msg fl'; $name = $user->name;?>
@else
    <?php $class = 'msg right-msg fr Js_to_user_chat'; $name = $toUser->name ?>
@endif
@if($key < $length-1)
<?php 
    $currentDay = date('Y-m-d', $chat->create_time);
    $lastDay = date('Y-m-d', $chats[$key+1]->create_time);
?>
@endif
@if(($key < $length-1 &&$currentDay != $lastDay) || ($length < App\Modules\ChatModule::CHAT_PER_PAGE && $key==$length-1 && empty($current)))
<li><div class="msg"><span class="time" style="margin-left:400px;">{{{date('Y-m-d', $chat->create_time)}}}</span></div></li>
@endif
<li class="Js_get_more_li" chat-id ="{{{$chat->id}}}" create-time="{{{date('Y-m-d', $chat->create_time)}}}">
	<div class="{{{$class}}}">
		<span class="name">{{{$name}}}</span>
		<span class="time">{{{date('H:i', $chat->create_time)}}}</span>
		<p class="con">{{{$chat->content}}}</p>
		<i class="arr"></i>
	</div>
</li>
@endfor
@if(!$toUser)
<?php $to_user_id = 0 ?>
@else
<?php $to_user_id = $toUser->id ?>
@endif

@if(! isset($notChange) || !$notChange)
<input type="hidden" value="{{{$to_user_id}}}" id="Js_toUser" />
@endif
