@extends('common.main')
@section('title')
{{{$industry->title}}} - 创意百科
@stop
@section('content')
<div class="detail-imgbg bg_baike">
</div>
	<div class="list-page content w1180">
		<div class="height_30"></div>
		<div class="wiki-area-top">
			<p class="title">{{{$industry->title}}}</p>
			<p class="des">{{{$industry->description}}}</p>
			<div class="dotline"></div>
			<ul class="wiki-expert-list clearfix">
			@foreach($industryUsers as $industryUser)
				<li><a href="{{{$baseURL}}}/user/home/{{{$industryUser->id}}}" @if(! $user || $user->id != $industryUser->id)class="Js_show_user_info_card" @endif data-user-id="{{{$industryUser->id}}}"><img class="avatar" src="{{{App\Common\Utils::getAvatar($industryUser->avatar)}}}" alt=""></a></li>
			@endforeach	
				<li><a class="wlink" @if(count($industryUsers) == 0)style="display: none"@endif href="{{{$baseURL}}}/thinktank">更多专家</a></li>
			</ul>
		</div>
		<div class="height_10"></div>
		<div class="Js_list_tag">
			<div class="list_tag_wrap w1180">
				<div class="list-tags clearfix">
					<p class="tags fl Js_tag">
						@include('common.hot-tags')
					</p>
					<p class="refresh fr"><a class="t Js_refreshtag" style="margin-right:0" href="javascript:" title="刷新">刷新</a></p>
				</div>
				<div class="height_10"></div>
				<div class="match-nav clearfix">
					<ul class="navlist clearfix fl">
						<li>{{{$industry->title}}}行业内有<span class="other-num">{{{$encyclopediasCount}}}</span>个创意</li>
					</ul>
					<a class="publish-btn fr" href="{{$baseURL}}/encyclopedia/publish/{{{$industry->id}}}">展示创意</a>
				</div>
			</div>
		</div>
		<ul class="list_main">
			@include('common.contest-list')
		</ul>
		<input type="hidden" value="{{{$industry->id}}}" class="Js_list_contest_type" />
	</div>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.scrollstop.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.focusMap.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/list.js"></script>
@stop	