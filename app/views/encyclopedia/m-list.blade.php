@if (! isset($page))
    <?php $page = 1?>
@endif
@if(count($lists) || $page > 1)
<ul id="list_content" class="clearfix">
@foreach($lists as $encyclopedia)
<li>
	<div class="list_block_container">
		<div class="list_block project">
			<div class="block_cover">
				<a class="cover" href="{{$baseURL}}/encyclopedia/detail/{{$encyclopedia->id}}"> <img class=""
					src="{{$baseURL}}/{{{$encyclopedia->cover}}}" title="{{{$encyclopedia->title}}}">
				</a>
			</div>
			<div class="block_content animate_border_color">
				<a class="headline animate_color" href="{{$baseURL}}/encyclopedia/detail/{{$encyclopedia->id}}"
					title="{{{$encyclopedia->title}}}">{{{$encyclopedia->title}}}</a>
				<div class="tags">
								<?php $tags = json_decode($encyclopedia->tags)?>
								@if(!empty($tags))
								@foreach($tags as $key => $tag)
									<span>{{{$tag}}}</span>
								@endforeach
								@endif
				</div>
			</div>
			
			<div class="block_bottom animate_border_color clearfix">
			@if($encyclopedia->user)
				<img class="avatar" src="{{{App\Common\Utils::getAvatar($encyclopedia->user->avatar)}}}" alt="" />
				<a class="user animate_color" href="{{$baseURL}}/user/home/{{$encyclopedia->user->id}}">{{{$encyclopedia->user->name}}}</a>
				<span class="participants">{{{$encyclopedia->creative_index}}}</span>
			@endif
			</div>
			
		</div>
	</div>
</li>
@endforeach
</ul>
@if (!empty($pageTotal) && $pageTotal > 1)
	<div class="loadpage"><a class="Js_nextpage loadpage-btn" href="javascript:">加载更多</a></div>
@endif


<input type="hidden" value="{{{$page}}}" class="Js_list_page_value" />


@if (!isset($tagId))
    <?php $tagId = 0;?>
@endif
<input type="hidden" value="{{{$tagId}}}" class="Js_list_tag_id" />


@if (! isset($status))
    <?php $status = 0;?>
@endif
<input type="hidden" value="{{{$status}}}" class="Js_list_status_value" />

@else
<div style="margin-right:20px;">
<div class="no_content">
	<div class="tip">暂无</div>
</div>
</div>
@endif