@extends('common.main')
@section('title')
{{{$encyclopedia->title}}} - 创意百科
@stop
@section('content')
	<div class="detail-top-wap dworks exchange_dworks">
		<div class="detail-top w1180">
			@include('common.like-report')
			<h1 class="pro-title dworks">{{{$encyclopedia->title}}}</h1>
			<div class="work-info">
				<?php $tags = json_decode($encyclopedia->tags)?>
				@if(!empty($tags))
				<p>创意标签：
				    @foreach($tags as $tag)
				    <span class="work-tag">{{{$tag}}}</span>
				    @endforeach
				</p>
				@endif
				<p>上传时间：{{{App\common\Utils::formatDate($encyclopedia->create_time)}}}</p>
				@if($encyclopedia->status != 2)
				<p>创意审核中</p>
				@endif    
			</div>
			<div class="pro-btn dworks">
				<a class="joinbtn" @if(empty($user) || $encyclopedia->user_id != $user->id) style="display:none" @endif href="{{{$baseURL}}}/encyclopedia/sell/{{{$encyclopedia->id}}}">出售这个创意</a>
			</div>
            <div class="pro-breadcrumb">当前位置：<a href="{{{$baseURL}}}/encyclopedia/index">创意世界百科</a>&nbsp;/&nbsp;<a href="{{{$baseURL}}}/encyclopedia/list/{{$encyclopedia->industry_id}}">行业名</a>&nbsp;/&nbsp;百科详情</div>
		</div>
	</div>
	<div class="content w1180 clearfix">
		<div class="detail-content">
			<div class="height_20"></div>
			<div class="pro-detail show-editor">
				<p>{{$encyclopedia->content}}</p>
			</div>
			<?php $creative_index = $encyclopedia->creative_index;?>
			@include('common.creative-score')
			<div class="des-line"></div>
			<!-- 专家点评 -->
			@if($user)
			<?php $showCommentBtnphp = true?>
			@endif
			@include('common.expert-comments',array('showCommentUrl'=>'','commentBtnTitle'=>'发布专家点评','showCommentInputBox'=>true))
			<div class="des-line"></div>
			<!-- 项目交流 -->
            @include('common.comments')
		</div>
		<div class="detail-aside">
			<div class="pro">
				<div class="pro-content swork">
					<strong>创意作者</strong>
					@if(!empty($encyclopedia->user))
					<div class="clearfix content">
						<div class="user-avatar">
							<a href="{{$baseURL}}/user/home/{{$encyclopedia->user->id}}">
                                <img src="{{{App\Common\Utils::getAvatar($encyclopedia->user->avatar)}}}" />
                            </a>
						</div>
						<div class="user-info">
							<div class="user-name">
								<span>{{{$encyclopedia->user->name}}}</span>
								<div class="level">
									<div class="icon {{{App\Common\Utils::getLevelStyle($encyclopedia->user->creative_index)}}}"></div>
									<div class="bar">
										<div class="process" style="width: 22px;"></div>
									</div>
								</div>
							</div>
							<span class="user-city">成都</span>
							<div class="user-mutual">
								<div class="clearfix mutual-btn">
								 @if(! $user || $user->id != $encyclopedia->user->id)
								<?php 
				                  if(isset($encyclopedia->user->is_follow)&& $encyclopedia->user->is_follow) {
				                    $class = "Js_has_followed_btn";
				                    $followname = '已关注';
			                        }else{
                                    $class = "Js_follow_btn";
                                    $followname = '关注';
                                   }
                                   ?>
									<a href="javascript:;" class="{{{$class}}}" user-id = "{{{$encyclopedia->user->id}}}">{{{$followname}}}</a>
									<a href="{{{$baseURL}}}/chat/contact/{{{$encyclopedia->user->id}}}" target="_blank">聊天</a>
								@endif
								</div>
							</div>
						</div>
						<div class="height_10"></div>
						<div class="submit-time clear">
							<p>提交创意时间：{{{App\common\Utils::formatDate($encyclopedia->create_time)}}}</p>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop