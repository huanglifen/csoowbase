@extends('common.main')
@section('title')
创意百科 - 创意世界
@stop
@section('content')
<div class="detail-imgbg bg_baike">
</div>
<div class="content w1200">
    <div class="height_20"></div>
    <div class="wiki_list">
        <ul class="clearfix">
           @foreach($recommendIndustries as $recommendIndustry)
           <li class="fl">
                <div class="content_top">
                    <a href="{{{$baseURL}}}/encyclopedia/list/{{{$recommendIndustry->id}}}">{{{$recommendIndustry->title}}}</a>
                    <p>{{{$recommendIndustry->description}}}</p>
                </div>
                <div class="content_btm">
                @foreach($recommendIndustry->users as $key => $expert)
                    @if($key != 2)
                    <a href="{{{$baseURL}}}/user/home/{{{$expert->id}}}" @if(! $user || $user->id != $expert->id)class="Js_show_user_info_card" @endif data-user-id="{{{$expert->id}}}"><img src="{{{App\Common\Utils::getAvatar($expert->avatar)}}}" alt="{{{$expert->name}}}" title="{{{$expert->name}}}"/></a>
                    @else
                    <a href="{{{$baseURL}}}/user/home/{{{$expert->id}}}" @if(! $user || $user->id != $expert->id)class="Js_show_user_info_card" @endif  data-user-id="{{{$expert->id}}}"><img src="{{{App\Common\Utils::getAvatar($expert->avatar)}}}" alt="{{{$expert->name}}}" title="{{{$expert->name}}}" style="margin-right: 0"/></a>
                    @endif
                @endforeach
                </div>
            </li>
           @endforeach
        </ul>
    </div>
    <div class="catalogue">
        <div class="classified_catalogue">
            <h2>行业分类</h2>
            <ul class="clearfix Js_industry_list">
                <li class="fl" data-id="">
                    <a href="javascript:">全部</a>
                </li>
                @foreach($firstIndustries as $firstIndustry)
                <li class="fl" data-id="{{{$firstIndustry->id}}}">
                    <a href="javascript:">{{{$firstIndustry->title}}}</a>
                </li>
                @endforeach    
            </ul>
        </div>
        <div class="cc_content">
            <ul class="clearfix Js_sub_industry_list">
                @include('encyclopedia.m-industry')
            </ul>
        </div>
    
    </div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/wiki.js"></script>
@stop
