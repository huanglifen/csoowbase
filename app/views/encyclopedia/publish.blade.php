@extends('common.main')
@section('title')
展示创意 - 创意百科
@stop
@section('content')
    <div class="content w1180">
        <div class="height_60"></div>
        <div class="tabs_wraper w980">
            <form id="publish_encyclopedia_form" action="#">
                <div class="tabs_step Js_tabs_step" style="margin-bottom:35px; margin-left:65px;">
                    <div class="hor_line Js_hor_line"><div class="hor_inline Js_hor_inline"></div></div>
                    <ul class="Js_tab">
                        <li class="first focustab">
                            <a href="#step-one">
                                <b class="circle" >1</b>
                                <span class="tips">基本信息</span>
                            </a>
                        </li>
                        <li class="last">
                            <a href="#step-two">
                                <b class="circle">2</b>
                                <span class="tips">确认协议</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class = "tabs_con Js_tabs_content" style="display:block;">
                            <div class="row">
                                <div class="rt" style="width:120px;">创意名称：</div>
                                <div class="rc">
                                    <div class="con" id="title">
                                        <input style="position: relative" type="text" placeholder="" name="title" datatype="e" errormsg="" nullmsg="" @if(isset($encyclopedia->title))value="{{{$encyclopedia->title}}}"@endif>
                                        <input type="hidden" value="@if(isset($encyclopedia->id)){{{$encyclopedia->id}}}@endif" name="tradeId">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="rt" style="width:120px;">上传封面图片：</div>
                                <div class="rc">
                                    <div class="con" id="imagefile">
                                        <div class="upload-files Js_cover">
											<!--[if lte IE 9]>
											<div class="uploadWrap-single dargArea left pr">
												<input type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff"
										       name="imagefile"
										       style="position:absolute; width:103px; height:37px; background:red; left:0; top:0; font-size:100px; border:0; cursor:pointer; filter:alpha(opacity:0);"
										       id="Js_upload_input" class="Js_upload_input_c"/>
												<a class="Js_uploadbox" style="vertical-align: middle">
													<button type="button" class="btn LightGrey"><span class="inner">上传</span></button>
												</a>
                                                <span class="des">请上传jpg、gif、png格式，分辨率大于280×220的图片</span>
                                                <span class="Validform_checktip Validform_wrong" style="display:none"></span>
                                                <input class="Js_file_uploadbox_single upload-input" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" name="cover">
                                            </div>
                                            <div class="height_10"></div>
                                            <iframe src="{{{$baseURL}}}/home/frame" id="Js_c_iframe"
									        class="updateSucImg Js_c_iframe_upload" scrolling="no" frameborder="0"
									        style="display:none;"></iframe>
											<![endif]-->
											
											<!--[if gte IE 10]>
                                            <div class="uploadWrap-single dargArea left">
												<span class="btn lightgrey Js_uploadbox" style="vertical-align: middle">
													<span class="btn lightgrey_l">上传</span>
												</span>
                                                <span class="des">请上传jpg、gif、png格式，分辨率大于280×220的图片</span>
                                                <span class="Validform_checktip Validform_wrong" style="display:none"></span>
                                                <input class="Js_file_uploadbox_single upload-input" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" name="cover">
                                            </div>
                                            <div class="height_10"></div>
                                            <div class="clearfix upload-single">
                                            @if(isset($encyclopedia->cover))
											<div class="updateSucImg">
												<div class="imgwrap">
													<p>
														<img
															src="{{{$baseURL}}}/{{{$encyclopedia->cover}}}">
													</p>
												</div>
												<a class="close Js_file_delete" title="删除"
													href="javascript:;">删除<!--	a--> <input type="hidden"
													value="{{{$encyclopedia->cover}}}"
													class="upload_success_cover" name="imagefile"></a>
											</div>
										    @endif
                                            </div>
											<![endif]-->
											
											<!--[if !IE]><!-->
											<div class="uploadWrap-single dargArea left">
												<a class="Js_uploadbox" style="vertical-align: middle">
													<button type="button" class="btn LightGrey"><span class="inner">上传</span></button>
												</a>
                                                <span class="des">请上传jpg、gif、png格式，分辨率大于280×220的图片</span>
                                                <span class="Validform_checktip Validform_wrong" style="display:none"></span>
                                                <input class="Js_file_uploadbox_single upload-input" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" name="cover">
                                            </div>
                                            <div class="height_10"></div>
                                            <div class="clearfix upload-single">
                                            </div>
											<!--<![endif]-->
											
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="clearfix">
								<div class="row fl">
									<div class="rt" style="width:120px;">创意详细介绍：</div>
									<div class="rc" id="description">
										<script type="text/plain" id="matchDetail" style="width:620px;height:215px;"></script>
									</div>
								</div>
							</div>
							<div class="clearfix">
								<div class="row fl">
									<div class="rt" style="width:120px;">创意标签：</div>
									<div class="rc">
										<div class="con" id="tags">
											<input style="position: relative" type="text" placeholder="" 
											@if(isset($encyclopedia->tags))<?php $tags = json_decode($encyclopedia->tags, true); if(!empty($tags)) {$tags = implode(",",$tags);} else{$tags = '';}?>@endif
											name="tags" datatype="e" errormsg="" nullmsg="" value="@if(isset($tags)){{$tags}}@endif">
										</div>
										<div class="illus" style="font-size: 12px;color: #D15552;">请输入体现创意特点的标签词语，词语间通过逗号分隔开，词语只能包含中文、英文、数字</div>
									</div>
								</div>
							</div>
                            <div class="row">
                                <div class="rc" style="margin-left:128px;">
                                    <a class="Js_goto_step2" href="#step-two">
                                        <button type="button" class="btn DeepBlue">下一步</button>
                                    </a>
                                </div>
                            </div>
                </div>
                <div class = "tabs_con Js_tabs_content" style="display:none; padding-top:0">
                    <div class="agreement">
                        <h1>《创意百科发布协议》</h1>
                        <div class="separator"></div>
                        <div class="height_30"></div>
                        <div class="info_content">
                            <ul>
                                <li>通过创意世界网发布的创意，禁止如下内容：
                                    <ul class="level3">
                                        <li>发布或提交软件破解、程序破解类内容</li>
                                        <li>发布或提交游戏外挂、程序外挂类内容</li>
                                        <li>发布或提交盗取网银账号、游戏账号类内容</li>
                                        <li>发布或提交侵犯第三方知识产权的内容</li>
                                        <li>发布或提交侵犯第三方权利的内容</li>
                                        <li>发布或提交木马、黑客程序等有损网络安全的内容</li>
                                        <li>发布或提交涉黄、赌博等内容</li>
                                        <li>发布或提交其他违反法律、法规、行政规章等相关规定的内容</li>
                                        <li>发布或提交论文代写类内容</li>
                                        <li>发布或提交刷钻、刷信用等内容</li>
                                        <li>发布或提交虚假信息的内容</li>
                                        <li>发布或提交通过链接等方式逃避创意世界及其他用户审核的内容</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="height_30"></div>
                        <a class="Js_goto_step1" href="#step-one" style="margin-left: 15px">
                            <button type="button" class="btn DeepGrey">上一步</button>
                        </a>&nbsp;&nbsp;
                        <a class="Js_encyclopedia_submit" href="javascript:">
                            <button type="button" class="btn DeepBlue">同意</button>
                        </a>
                    </div>
                    <input id="Js_industryId" type="hidden" value="{{{$industryId}}}">
                    <div class="height_20"></div>
                </div>
            </form>
        </div>
        <div class="tabs_wraper w980"></div>
    </div>
	<script>var uploadPath = "encyclopedia/upload-image";</script>
    <script type="text/javascript" src="{{{$baseURL}}}/js/editor/umeditor.config.js" charset="utf-8" ></script>
    <script type="text/javascript" src="{{{$baseURL}}}/js/editor/_examples/editor_api.js" charset="utf-8" ></script>
    <script type="text/javascript" src="{{{$baseURL}}}/js/editor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript" src="{{{$baseURL}}}/js/radioitem.js"></script>
    <script type="text/javascript" src="{{{$baseURL}}}/js/zebra_datepicker.js"></script>
	<!--[if gte IE 10]>
	<script type="text/javascript" src="{{{$baseURL}}}/js/uploader_config_single.js"></script>
	<![endif]-->
	<!--[if !IE]><!-->
	<script type="text/javascript" src="{{{$baseURL}}}/js/uploader_config_single.js"></script>
	<!--<![endif]-->
	<!--[if lte IE 9]>
	<script type="text/javascript" src="{{{$baseURL}}}/js/ajaxfileuploader.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/uploader_frame_single.js"></script>
	<![endif]-->
    <script type="text/javascript" src="{{{$baseURL}}}/js/tabs.js"></script>
    <script type="text/javascript" src="{{{$baseURL}}}/js/publish.js"></script>
	 <script type="text/javascript">
@if(isset($encyclopedia->content))
var ue = UM.getEditor('matchDetail');
ue.setContent('{{$encyclopedia->content}}');
@endif
</script>
@stop
