<h2>《创意世界大赛服务协议》</h2>
<ol class="level1">
	<li><strong>简介</strong>
		<p>您只有在完全同意下列服务条款，并完成注册后，才能成为而且立即成为创意世界的用户并使用创意世界提供的创意世界大赛服务。一旦您浏览或开始使用创意世界大赛服务，即视为您已经完全了解并同意下列条款，包括创意世界对条款做的修改。</p>
	</li>
	<li><strong>服务内容</strong>
		<p>创意世界大赛的服务内容包括创意投资项目比赛、创意任务人才比赛、创意公益明星比赛。</p>
		<ol class="level2">
			<li>创意投资项目比赛
				<p>创意投资项目比赛是创意世界提供的一个允许用户(即“创意发布者”)通过展示创意，以便寻找创意投资者共同实现创意的服务。</p>
				<p>您作为创意发布者：创意投资者将与您订立《创意投资项目比赛合作协议》，为您的创意提供资金的投资，而您则在接受投资后遵照《创意投资项目比赛合作协议》努力实现创意，并行使其他《创意投资项目比赛合作协议》中约定的权利及义务。</p>
				<p>您作为创意投资者：您将与创意发布者订立《创意投资项目比赛合作协议》，明确您的投资方式和投资要求等投资信息，并遵照《创意投资项目比赛合作协议》向创意发布者提供资金的支持，并行使其他《创意投资项目比赛合作协议》中约定的权利及义务。</p>
				<p>创意世界并不是创意发布者和创意投资者中的任何一方。所有《创意投资项目比赛合作协议》约定的内容仅存在于用户和用户之间。</p>
			</li>
			<li>创意任务人才比赛
				<p>创意任务人才比赛是创意世界提供的一个允许用户(即“任务发布者”)通过描述创意任务需求，向任务参赛者发布创意解决方案的服务。</p>
				<p>您作为任务发布者：任务获胜者将与您订立《创意任务人才比赛合作协议》，为您提供创意任务解决方案，您选择满意的创意解决方案后依照《创意任务人才比赛合作协议》中的约定行使您的权利及义务。</p>
				<p>您作为任务获胜者：您将与任务发布者订立《创意任务人才比赛合作协议》，提供创意解决方案给任务发布者，并按照《创意任务人才比赛合作协议》中的约定行使您的权利及义务。</p>
				<p>创意世界并不是任务发布者和任务参赛者中的任何一方。所有《创意任务人才比赛合作协议》约定的内容仅存在于用户和用户之间。</p>
			</li>
			<li>创意公益明星比赛
				<p>创意博爱明星比赛是创意世界提供的一个让用户(即“比赛发布者”)通过提供奖金或其他实物/非实物奖励召集用户（即”比赛参赛者“）参赛并选拔最终获奖者的服务。</p>
				<p>您作为比赛发布者：比赛获奖者将与您订立《创意公益明星比赛合作协议》。</p>
				<p>您作为比赛获奖者：比赛发布者将与您订立《创意公益明星比赛合作协议》。</p>
				<p>创意世界并不是比赛发布者和比赛参赛者中的任何一方。所有《创意公益明星比赛合作协议》约定的内容仅存在于用户和用户之间。</p>
			</li>
		</ol></li>
	<li><strong>禁止行为</strong>
		<ul>
			<li>使用创意世界大赛服务，您须同意并遵守以下禁止行为，包括如下条款：
				<ul class="level3">
					<li>发布或提交软件破解、程序破解类内容</li>
					<li>发布或提交游戏外挂、程序外挂类内容</li>
					<li>发布或提交盗取网银账号、游戏账号类内容</li>
					<li>发布或提交侵犯第三方知识产权的内容</li>
					<li>发布或提交侵犯第三方权利的内容</li>
					<li>发布或提交木马、黑客程序等有损网络安全的内容</li>
					<li>发布或提交涉黄、赌博等内容</li>
					<li>发布或提交其他违反法律、法规、行政规章等相关规定的内容</li>
					<li>发布或提交论文代写类内容</li>
					<li>发布或提交刷创创币、刷创意指数等内容</li>
					<li>发布或提交虚假信息的内容</li>
					<li>发布或提交通过链接等方式逃避创意世界及其他用户审核的内容</li>
				</ul>
			</li>
		</ul></li>
	<li><strong>网站内容的使用</strong>
		<p>一旦您将照片、音频、视频或文字等内容（统称为素材）上传到创意世界大赛，即视为您同意创意世界享有该内容的永久免费使用的权利（并有权在多个层面对该权利进行再授权），创意世界有权免费地在全球范围内复制、发行、展示、演绎和通过信息网络等渠道使用你上传、提供的素材。例如将您上传的肖像素材应用于网络广告、平面媒体、线下推广、企业内刊等场景中。创意世界大赛参与用户对于其所有的，并在创意世界大赛中发布的合法内容享有著作权及其他相关权利。
			创意世界大赛参与用户需确保自己在创意世界大赛中发布或提交的内容具有合法权利或有效授权，不应侵犯他人的肖像权、名誉权、知识产权、隐私权等合法权益，也不会侵犯法人或其他团体的商业秘密等合法权益。如因此引起纠纷，创意世界有权直接删除该内容并要求赔偿。</p>
	</li>
	<li><strong>服务变更，中断或终止</strong>
		<p>如因系统升级或维护而需要暂停或终止服务，创意世界会提前在网站首页张贴公告。</p>
		<ul>
			<li>如遇以下情形，创意世界有权立刻单方面终止向创意世界提供服务，且无需通知创意世界用户：
				<ul class="level3">
					<li>在创意世界大赛中辱骂其他用户</li>
					<li>对其他用户进行人身攻击</li>
					<li>发布黄色，反动内容</li>
					<li>发布广告信息（广告信息是指利用创意世界大赛作为渠道，发布违规信息（如黄色，反动内容），大量发送诱导用户点击网页链接，以及以商业合作为由，大量发送的诱导用户提供个人资料的信息</li>
					<li>盗用他人资料，冒充他人</li>
					<li>创意世界用户违反本协议其他内容</li>
					<li>创意世界用户的行为可能不利于创意世界、创意世界科技有限公司或其会员、其它用户或第三方的合理利益</li>
				</ul>
			</li>
		</ul></li>
	<li><strong>创意世界的责任范围</strong>
		<ol class="level2">
			<li>创意世界保留随时以任何理由取消比赛的权利。</li>
			<li>创意世界有权随时以任何理由拒绝、取消、中断、删除或暂停该比赛。创意世界不因该行为承担任何赔偿。创意世界的政策并非评论此类行为的理由。</li>
			<li>创意世界不承担任何相关回报或使用服务产生的损失或亏损。创意世界无义务介入任何用户之间的纠纷，或用户与其他第三方就服务使用方面产生的纠纷。包括但不限于货物及服务的交付，其他条款、条件、保证或与网站活动相关联的有关陈述。创意世界不负责监督比赛的实现与严格执行。您可授权创意世界、其工作人员、职员、代理人及对损失索赔权的继任者所有已知或未知、公开或秘密的解决争议的方法和服务。</li>
		</ol></li>
</ol>