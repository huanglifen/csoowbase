@extends('common.main')

@section('title')
	@if($type == 'about')
		关于我们 
	@elseif($type == 'help1')
		新手入门
	@elseif($type == 'help2')
		账号设置
	@elseif($type == 'help3')
		网站业务
	@elseif($type == 'help4')
		常见问题
	@elseif($type == 'agreement1')
		创意世界大赛服务协议
	@elseif($type == 'agreement2')
		创意世界用户注册协议
	@elseif($type == 'agreement3')
		创意发布协议
	@elseif($type == 'agreement4')
		加入创意世界智库协议
	@elseif($type == 'employ')
		诚聘英才
	@endif - 创意世界
@stop

@section('content')

<div class="height_50"></div>
<div class="content w1180 clearfix">
	<div class="info_aside">
		<ul class="Js_SideMenu">
			<li><a class="back_home" href="{{$baseURL}}" title="回到首页">首页</a></li>
			<li><a class="level1" href="{{$baseURL}}/home/about">关于我们</a>
				<ul id="about">
					<li  @if($type=='about') class="current" @endif><a class="level2" href="{{$baseURL}}/home/about">关于创意世界</a></li>
					<li><a class="level2" href="">合作伙伴</a></li>
					<li><a class="level2" href="">媒体报道</a></li>
					<li><a class="level2" href="">网站动态</a></li>
					<li><a class="level2" href="">联系我们</a></li>
				</ul> 
				</li>
			<li><a class="level1" href="{{$baseURL}}/home/help1">帮助中心</a>
				<ul id="help">
					<li @if($type=='help1') class="current" @endif><a class="level2" href="{{$baseURL}}/home/help1">新手入门</a></li>
					<li @if($type=='help2') class="current" @endif><a class="level2" href="{{$baseURL}}/home/help2">账号管理</a></li>
					<li @if($type=='help3') class="current" @endif><a class="level2" href="{{$baseURL}}/home/help3">网站业务</a></li>
					<li @if($type=='help4') class="current" @endif><a class="level2" href="{{$baseURL}}/home/help4">常见问题</a></li>
				</ul></li>
			<li><a class="level1" href="{{$baseURL}}/home/agreement1">网站协议</a>
				<ul id="agreement">
					<li @if($type=='agreement1') class="current" @endif><a class="level2" href="{{$baseURL}}/home/agreement1">创意世界大赛服务协议</a></li>
					<li @if($type=='agreement2') class="current" @endif><a class="level2" href="{{$baseURL}}/home/agreement2">创意世界用户注册协议</a></li>
					<li @if($type=='agreement3') class="current" @endif><a class="level2" href="{{$baseURL}}/home/agreement3">创意发布协议</a></li>
					<li @if($type=='agreement4') class="current" @endif><a class="level2" href="{{$baseURL}}/home/agreement4">加入创意世界智库协议</a></li>
				</ul></li>
			<li><a class="level1" href="{{$baseURL}}/home/employ">诚聘英才</a>
				<ul id="employ">
					<li @if($type=='employ') class="current" @endif><a class="level2" href="{{$baseURL}}/home/employ">产品</a></li>
					<li><a class="level2" href="">设计</a></li>
					<li><a class="level2" href="">研发</a></li>
					<li><a class="level2" href="">运营</a></li>
					<li><a class="level2" href="">人事</a></li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="info_content">
		
			@if($type == 'about')
				@include('home.about')
			@elseif($type == 'help1')
				@include('home.help1')
			@elseif($type == 'help2')
				@include('home.help2')
			@elseif($type == 'help3')
				@include('home.help3')
			@elseif($type == 'help4')
				@include('home.help4')
			@elseif($type == 'agreement1')
				@include('home.agreement1')
			@elseif($type == 'agreement2')
				@include('home.agreement2')
			@elseif($type == 'agreement3')
				@include('home.agreement3')
			@elseif($type == 'agreement4')
				@include('home.agreement4')
			@elseif($type == 'employ')
				@include('home.employ')
			@endif
	</div>
</div>
<script>
var homeBottomType =  '{{$type}}';

        function initMenu(){

            $('.Js_SideMenu ul').hide();
            switch(homeBottomType){
            	case 'about':
            		$('#about').show();
	            break;
	            case 'help1':
	            case 'help2':
	            case 'help3':
	            case 'help4':
	            	$('#help').show();
	            break;
	            
	            case 'agreement1':
	            case 'agreement2':
	            case 'agreement3':
	            case 'agreement4':
	            	$('#agreement').show();
	            break;
	            case 'employ':
            		$('#employ').show();
	            break;
            	default: $('.Js_SideMenu ul').hide();
            }
           
            $('.Js_SideMenu li .level1').click(function(){
                var checkElement = $(this).next();
                if((checkElement.is('ul')) && (checkElement.is(':visible')))
                {
                    return false;
                }
                if((checkElement.is('ul')) && (!checkElement.is(':visible')))
                {
                    $('.Js_SideMenu ul:visible').slideUp('normal');
                    checkElement.slideDown('normal');
                    return false;
                }
            });
        }
        $(document).ready(function(){
            initMenu();
        });
</script>
@stop

