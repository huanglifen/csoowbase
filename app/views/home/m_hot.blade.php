<div class="web-nav clearfix">

	<ul class="web-nav-list clearfix">
		<li class="nav-list-item">
			<h2><a href="{{$baseURL}}/encyclopedia"><img src="{{{$baseURL}}}/temp/cybk.png" alt="创意百科" /></a></h2>
			<dl class="clearfix">
				<dt>热门行业</dt>
				<dd>
					<span class="clearfix">
						@for($i = 0; $i < count($hotEncyclopedias) && $i < 9;$i++)
							<a href="{{{$baseURL}}}/encyclopedia/list/{{{$hotEncyclopedias[$i]->id}}}">{{{$hotEncyclopedias[$i]->title}}}</a>
							@if($i%3 == 2)
							</span>
							<span class="clearfix">
							@endif
						@endfor
					</span>
				</dd>
			</dl>
		</li>
		<li class="nav-list-item">
			<h2><a href="{{{$baseURL}}}/project"><img src="{{{$baseURL}}}/temp/cytz.png" alt="创意投资项目比赛" /></a></h2>
			<dl class="clearfix">
				<dt>热门比赛</dt>
				<dd>
					<ul class="new-list-item">
					@foreach($hotProjects as $project)
						<li><a href="{{{$baseURL}}}/project/detail/{{{$project->id}}}">{{{$project->title}}}</a></li>
					@endforeach
					</ul>
				</dd>
			</dl>
		</li>
		<li class="nav-list-item">
			<h2><a href="{{{$baseURL}}}/task"><img src="{{{$baseURL}}}/temp/cyrc.png" alt="创意任务人才比赛" /></a></h2>
			<dl class="clearfix">
				<dt>热门比赛</dt>
				<dd>
					<ul class="new-list-item">
					@foreach ($hotTasks as $task)
						<li><a href="{{{$baseURL}}}/task/detail/{{{$task->id}}}">{{{$task->title}}}</a></li>
					@endforeach
					</ul>
				</dd>
			</dl>
		</li>
		<li class="nav-list-item">
			<h2><a href="{{{$baseURL}}}/hiring"><img src="{{{$baseURL}}}/temp/cymx.png" alt="创意公益明星比赛" /></a></h2>
			<dl class="clearfix">
				<dt>热门比赛</dt>
				<dd>
					<ul class="new-list-item">
					@foreach ($hotHirings as $hiring)
						<li><a href="{{{$baseURL}}}/hiring/detail/{{{$hiring->id}}}">{{{$hiring->title}}}</a></li>
					@endforeach
					</ul>
				</dd>
			</dl>
		</li>
		<li class="nav-list-item">
			<h2><a href="{{$baseURL}}/exchange"><img src="{{{$baseURL}}}/temp/cyjys.png" alt="创意世界交易所" /></a></h2>
			<dl class="clearfix">
				<dt>最新交易</dt>
				<dd>
					<ul class="new-list-item">
					@foreach($hotExchanges  as $trade)
						<li><a href="{{{$baseURL}}}/exchange/detail/{{{$trade->id}}}">{{{$trade->title}}}</a></li>
					@endforeach
					</ul>
				</dd>
			</dl>
		</li>
		<li class="nav-list-item">
			<h2><a href="{{$baseURL}}/thinktank"><img src="{{{$baseURL}}}/temp/cyzk.png" alt="创意世界智库" /></a></h2>
			<dl class="clearfix">
				<dt>人气专家</dt>
				<dd>
					<span class="clearfix">
						@for($i = 0;$i < count($hotExperts) && $i < 12;$i++)
							<a href="{{{$baseURL}}}/user/home/{{{$hotExperts[$i]->id}}}">{{{$hotExperts[$i]->name}}}</a>
							@if($i%3 == 2)
							</span>
							<span class="clearfix">
							@endif
						@endfor
					</span>
				</dd>
			</dl>
		</li>
	</ul>

	<div class="web-news">
		<div class="news-content">
			<div class="news-h">
				<div class="news-title clearfix">
					<h3>实时热点</h3>
					<a href="javascript:;" id="Js_change_ssxw">换一批</a>
				</div>
				<ul class="news-list Js_news_list">
					@include('home.m_news')
				</ul>
			</div>
			<div class="news-app">
				<div class="news-title clearfix">
					<h3>热门应用</h3>
				</div>
				<ul class="app-list clearfix">
					<li><a href="http://www.hao123.com/mail" target="_blank" class="wy"></a></li>
                    <li><a href="http://www.baidu.com/#wd=%E8%AF%8D%E5%85%B8" target="_blank" class="xlwb"></a></li>
                    <li><a href="http://www.baidu.com/#wd=%E9%9F%B3%E4%B9%90" target="_blank" class="rrw"></a></li>
                    <li><a href="http://www.baidu.com/#wd=%E8%82%A1%E7%A5%A8" target="_blank" class="sohu"></a></li>
                    <li><a href="http://www.baidu.com/#wd=%E6%B8%B8%E6%88%8F" target="_blank" class="tb"></a></li>
                    <li><a href="http://www.baidu.com/#wd=%E7%94%B5%E5%BD%B1" target="_blank" class="jd"></a></li>
                    <li><a href="http://news.hao123.com/wangzhi" target="_blank" class="bd"></a></li>
                    <li><a href="http://xiaohua.zol.com.cn/" target="_blank" class="gg"></a></li>
                    <li><a href="http://www.baidu.com/#wd=%E5%A4%A9%E6%B0%94" target="_blank" class="ht"></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>