<div class="height_20"></div>
<div class="cwindex-title">
	<p class="line"></p>
	<p class="ctitle"><span>创意世界帮助您开启创意之旅</span></p>
</div>

<ul class="guide clearfix">
	<li class="guide-one">
		<div>
			<h4>激发创意，让您从创意菜鸟变达人</h4>
			<p>
				创意世界有全球最大的<a href="{{$baseURL}}/encyclopedia">创意百科</a>，这里有无数令人叹为观止的创意。 
			</p>
		</div>
	</li>
	<li class="guide-two">
		<div>
			<h4>展示创意，获创意领袖权威点评</h4>
			<p>			
				展示出您的创意，听听朋友们和<a href="{{$baseURL}}/thinktank">创意世界智库</a>的意见，通常都会让人获得新的思路和想法。
			</p>
		</div>
	</li>
	<li class="guide-three">
		<div>
			<h4>实现创意，同时实现您的人生价值</h4>
			<p>
			<a href="{{$baseURL}}/area" style="padding-left:0;">创意世界地区</a>汇集所有实现创意的妙招，无论是需要资金，还是需要人才，
			通过<a href="{{$baseURL}}/project">创意世界大赛</a>和<a href="{{$baseURL}}/exchange">创意世界交易所</a>都能轻松办到。 
			</p>
		</div>
	</li>
</ul>


