<div class="height_30"></div>
<div class="about_my_friends">
	<div class="circle-content">
		<div class="homepage_module_top clearfix">
			<h3 class="module_tittle fl">最新朋友圈动态</h3>
			<a class="more fr" href="{{{$baseURL}}}/dynamic">查看更多</a>
		</div>
		<ul class="dis-list">
			@include('common.dynamic',array('showOperate'=>false))
		</ul>
	</div>
	<div class="circle-aside">
		<div class="project_wraper">
			<div class="projecthead" style="height: 38px">
				<h3 class="projecttitle">朋友圈创意指数排行榜</h3>
			</div>
			<div class="circle_rank_list">
				<div class="circle_rank_list_tittle clearfix">
					<span class="fl">排名</span>
					<span class="last fr">获得创创币</span>
					<span class="fr">创意指数</span>
				</div>
				<ol class="rank_list">
					@foreach($memberRanks as $key => $rank)
					<li class="@if($key < 3)top3 @endif clearfix">
						<i>@if($key<9)0@endif{{{$key+1}}}</i>
						<a href="{{{$baseURL}}}/user/home/{{{$rank->id}}}"><img src="{{{App\Common\Utils::getAvatar($rank->avatar)}}}" alt="图标"><em>{{{$rank->name}}}</em></a>
						<span class="coin">{{{$rank->coin}}}</span>
						<span class="cw_index">{{{$rank->creative_index}}}</span>
					</li>
					@endforeach
				</ol>
			</div>
		</div>
		<div class="height_30"></div>
		<div class="project_wraper">
			<div class="projecthead">
				@if(!empty($user))
				<h3 class="projecttitle">您的朋友参与了这些创意</h3>
				<p>和他们一起创意成真</p>
				@else
				<h3 class="projecttitle">热门比赛</h3>
				@endif
			</div>
			<ul>
				@foreach($contests as $contest)
				<li class="projectlist">
					<a class="clearfix projectlink" href="{{{App\Common\Utils::getContestDetailUrl($contest->type, $contest->id)}}}">
						<img class="projectavatar" src="{{{$contest->cover}}}">
						<p class="tipone">{{{$contest->title}}}</p>
						<p class="tiptwo"><em>{{{$contest->creative_index}}}</em>点创意指数</p>
					</a>
				</li>
				@endforeach
			</ul>
		</div>
	</div>
	<div class="height_0"></div>
</div>
	