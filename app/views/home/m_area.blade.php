<div class="height_20"></div>
<div class="homepage_guide_img">
	<a class="guide_img area" href="{{{$baseURL}}}/area" title="点击进入创意世界地区"></a>
</div>
<div class="height_20"></div>
<div class="cw_index_rank_lists">
<div class="homepage_module_top clearfix">
	<h3 class="module_tittle fl">创意指数排行榜</h3>
	@if($user)<span class="contribution">您的地区贡献值为<em>{{{$user->creative_index}}}</em></span>@endif
	<a class="more fr" href="{{{$baseURL}}}/area">查看更多</a>
</div>
<ul class="rank_lists clearfix">
<li class="fl">
	<div class="rank_list area">
		<div class="rank_list_tittle">
			<h3>地区创意指数排行榜</h3>
			<span>创意指数</span>
		</div>
		<ol class="rank">
			@if(!empty($regions))
				@foreach($regions as $k=>$region)
				<li class="@if($k<3)top3@endif clearfix">
					<i>{{sprintf('%02d',$k+1)}}</i>
					<a href="{{$baseURL}}/area/region/{{$region->id}}">
					<img src=" @if(!empty($region->logo))
						       {{$region->logo}}
						       @else
						       {{$baseURL}}/images/icon_area_area.png
						       @endif" alt="{{$region->name}}"/><em>{{$region->name}}</em></a>
					<span>{{$region->creative_index}}</span>
				</li>
				@endforeach
			@endif
		</ol>
	</div>
</li>
<li class="fl">
	<div class="rank_list school">
		<div class="rank_list_tittle">
			<h3>学校创意指数排行榜</h3>
			<span>创意指数</span>
		</div>
		<ol class="rank">
			@if(!empty($schools))
				@foreach($schools as $k=>$school)
				<li class="@if($k<3)top3@endif clearfix">
					<i>{{sprintf('%02d',$k+1)}}</i>
					<a href="{{$baseURL}}/area/school/{{$school->id}}">
					<img src=" @if(!empty($school->logo))
						       {{$school->logo}}
						       @else
						       {{$baseURL}}/images/icon_area_school.png
						       @endif" alt="{{$school->name}}"/><em>{{$school->name}}</em></a>
					<span>{{$school->creative_index}}</span>
				</li>
				@endforeach
			@endif
		</ol>
	</div>
</li>
<li class="fl">
	<div class="rank_list gov">
		<div class="rank_list_tittle">
			<h3>行政机构创意指数排行榜</h3>
			<span>创意指数</span>
		</div>
		<ol class="rank">
			@if(!empty($goverments))
				@foreach($goverments as $k=>$goverment)
				<li class="@if($k<3)top3@endif clearfix">
					<i>{{sprintf('%02d',$k+1)}}</i>
					<a href="{{$baseURL}}/area/organization/{{$goverment->id}}">
					<img src=" @if(!empty($goverment->logo))
						       {{$goverment->logo}}
						       @else
						       {{$baseURL}}/images/icon_area_gov.png
						       @endif" alt="{{$goverment->name}}"/><em>{{$goverment->name}}</em></a>
					<span>{{$goverment->creative_index}}</span>
				</li>
				@endforeach
			@endif
		</ol>
	</div>
</li>
<li class="fl">
	<div class="rank_list enterprise">
		<div class="rank_list_tittle">
			<h3>企业创意指数排行榜</h3>
			<span>创意指数</span>
		</div>
		<ol class="rank">
			@if(!empty($coporates))
				@foreach($coporates as $k=>$coporate)
				<li class="@if($k<3)top3@endif clearfix">
					<i>{{sprintf('%02d',$k+1)}}</i>
					<a href="{{$baseURL}}/area/organization/{{$coporate->id}}">
					<img src=" @if(!empty($coporate->logo))
						       {{$coporate->logo}}
						       @else
						       {{$baseURL}}/images/icon_area_enterprise.png
						       @endif" alt="{{$coporate->name}}"/><em>{{$coporate->name}}</em></a>
					<span>{{$coporate->creative_index}}</span>
				</li>
				@endforeach
			@endif
		</ol>
	</div>
</li>
</ul>
</div>
		