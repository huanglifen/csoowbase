<div class="height_20"></div>
<div class="web-join-zk">
	<img src="{{{$baseURL}}}/temp/join_zk.png" alt="创意世界智库" usemap="#think_tank"/>
    <map id="think_tank" name="think_tank">
        <area shape="rect" coords="753,144,1054,196" href="{{{$baseURL}}}/thinktank/guide" title="点击进入申请/邀请加入智库候选人引导页" alt="申请加入智库引导页"/>
        <area shape="rect" coords="0,0,656,240" href="{{{$baseURL}}}/thinktank/index" title="点击进入创意世界智库" alt="创意世界智库"/>
    </map>
</div>
<div class="height_20"></div>
<div class="popular_experts w1180 clearfix">
	<div class="experts_lists fl">
		<div class="homepage_module_top clearfix" style="padding-right: 30px">
			<h3 class="module_tittle fl">创意世界智库人气专家</h3>
			<a class="more fr" href="{{{$baseURL}}}/thinktank/index">更多专家<i></i></a>
		</div>
		<div class="lists">
			<ul class="clearfix">
			@foreach($hotExperts as $hotExpert)
				<li class="fl">
					<div class="u-info">
						<a href="{{{$baseURL}}}/user/home/{{{$hotExpert->id}}}" class="Js_show_user_info_card" data-user-id="{{{$hotExpert->id}}}">
							<img class="avatar" src="{{{App\Common\Utils::getAvatar($hotExpert->avatar)}}}" alt="">
						</a>
						<div class="con">
							<a href="{{{$baseURL}}}/user/home/{{{$hotExpert->id}}}" class="Js_show_user_info_card" data-user-id="{{{$hotExpert->id}}}"><span class="name">{{{$hotExpert->name}}}</span></a>
							<p class="des" style="width:192px; word-break:break-all; height:40px; overflow:hidden">{{{$hotExpert->expert_title}}}</p>
						</div>
					</div>
				</li>
			@endforeach
			</ul>
		</div>
	</div>
	<div class="experts_comments fl">
		<ul>
		@foreach($hotExpertsComments as $key => $hotExpertsComment)
		  @if($key == 0)
			<li class="clearfix">
				<p class="comment_objective">{{{$hotExpertsComment->item->title}}}：</p>
				<p class="comment_content">{{{$hotExpertsComment->content}}}</p>
				@if(!empty($hotExpertsComment->user))
				<a class="expert_name fr" href="{{{$baseURL}}}/user/home/{{{$hotExpertsComment->user->id}}}">{{{$hotExpertsComment->user->name}}}</a>
				@endif
			</li>
		  @else
			<li class="cutting_line"></li>
			<li class="clearfix">
				<p class="comment_objective">{{{$hotExpertsComment->item->title}}}：</p>
				<p class="comment_content">{{{$hotExpertsComment->content}}}</p>
				@if(!empty($hotExpertsComment->user))
				<a class="expert_name fr" href="{{{$baseURL}}}/user/home/{{{$hotExpertsComment->user->id}}}">{{{$hotExpertsComment->user->name}}}</a>
				@endif
			</li>
		  @endif
		  @endforeach
		</ul>
	</div>
</div>
