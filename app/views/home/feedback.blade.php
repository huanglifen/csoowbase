@extends('common.main')

@section('title')
提交反馈 - 创意世界
@stop

@section('content')
<div class="content w1180">
	<div class="height_60"></div>
	<div class="feedback_wraper">
		<p class="greet">亲爱的<em>创意世界</em>用户：</p>
		<p class="tip">欢迎您来到创意世界！您对创意世界有任何意见和建议，或在使用过程中遇到的问题，请在本页面反馈；</p>
		<p class="tip">我们会每天关注您的反馈，不断优化产品，为您提供更好的服务！</p>
		<div class="content Js_feedcontent">
			<p>请留下您对"创意世界"的意见和建议（请填写）</p>
			<textarea class="Js_feedinput"></textarea>
			<input type="hidden" name="url" id="lastPage" value="{{$url}}">
			<a class="tc pub_state Js_pub_state" href="javascript:;">提交反馈</a>
		</div>
	</div>
	<div class="height_60"></div>
</div>
<script>
	var feedbackAjax = null;
    $.fn.pasteEvents = function( delay ) {//粘贴事件
        if (delay == undefined) delay = 20;
        return $(this).each(function() {
            var $el = $(this);
            $el.on("paste", function() {
                $el.trigger("prepaste");
                setTimeout(function() { $el.trigger("postpaste"); }, delay);
            });
        });
    };
	
    $('.Js_feedcontent').on('click keyup','textarea.Js_feedinput',function(){
		if($(this).val() != '')$('.Js_pub_state').addClass('active');
		else $('.Js_pub_state').removeClass('active');
	}).pasteEvents();
	$('.Js_feedinput').on('postpaste',function(){//粘贴事件
		if($(this).val() != '')$('.Js_pub_state').addClass('active');
		else $('.Js_pub_state').removeClass('active');
	}).pasteEvents();
	$('.Js_feedcontent').on('click','.Js_pub_state.active',function(event){
		if($('.Js_feedinput').val() != ''){
			if(feedbackAjax) feedbackAjax.abort();
			feedbackAjax = WG.fn.ajax('home/submit-feedback',{content:$('.Js_feedinput').val(),url:$('#lastPage').val()},function(e){
				if(e.status == 'ok'){
					window.location.href = e.content.url;	
				}
				feedbackAjax = null;
			})	
		}
		return false;
	});
</script>
@stop