@extends('common.main')

@section('title')
{{{$owner->name}}} - 创意世界
@stop

@section('content')

@include('user.user-top')

<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<ul class="navlist clearfix fl">
				<li class="current"><a class="c" href="{{$baseURL}}/user/home/{{{$owner->id}}}">@if(!empty($isMe)) 我@else TA@endif的动态</a>
				</li>
				<li><a class="c" href="{{$baseURL}}/user/contests/{{{$owner->id}}}">@if(!empty($isMe)) 我@else TA@endif的创意世界大赛</a></li>
				<li><a class="c" href="{{$baseURL}}/user/exchanges/{{{$owner->id}}}">@if(!empty($isMe)) 我@else TA@endif的创意交易</a></li>
				<li><a class="c" href="{{$baseURL}}/user/works/{{{$owner->id}}}">@if(!empty($isMe)) 我@else TA@endif的创意作品</a></li>
			</ul>
		</div>

		<div class="sub-nav">
			<ul class="subnav-list clearfix">
				<li
				@if($pageType==0) class="current" @endif><a href="{{$baseURL}}/user/home/{{{$owner->id}}}">全部</a></li>
				<li
				@if($pageType==1) class="current" @endif><a href="{{$baseURL}}/user/home/{{{$owner->id}}}/1">原创</a></li>
				<li
				@if($pageType==2) class="current" @endif><a href="{{$baseURL}}/user/home/{{{$owner->id}}}/2">转发</a></li>
			</ul>
		</div>
		@if(count($datas))
		<ul class="dis-list Js_dis_list_content">
			@include('common.dynamic')
		</ul>
		@else
		<ul class="dis-list Js_dis_list_content">
		</ul>
		<div class="Js_no_dynamic_data">
			<div class="height_20"></div>
			<div class="no_content">
				<div class="tip">暂无</div>
			</div>
		</div>
		@endif
	</div>

	<div class="detail-aside">

		@include('common.hot-contests')

		@include('common.hot-exchanges')

		@include('common.care-people')

		@include('common.complete-info')

	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/friendCircle.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.artZoom.js"></script>
<script>

var shareThisPage = true;
var forwordThisPage = true;
<?php if($pageType == 2 || empty($isMe)){ ?>
shareThisPage = false;
<?php } ?>
<?php if($pageType == 1 || empty($isMe)){ ?>
forwordThisPage = false;
<?php } ?>
jQuery(function ($) {
	$('.artZoom').artZoom({
		preload: true,		// 设置是否提前缓存视野内的大图片
		blur: true,			// 设置加载大图是否有模糊变清晰的效果
		maxWidth: 650,
		// 语言设置
		left: '左旋转',		// 左旋转按钮文字
		right: '右旋转',		// 右旋转按钮文字
		source: '看原图'		// 查看原图按钮文字
	});
});
</script>
@stop