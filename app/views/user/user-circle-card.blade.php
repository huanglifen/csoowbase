<div class="message_box">
   <div class="box_tittle">
        <span class="t fl">关注成功</span>
        <a title="关闭" href="javascript:" class="close fr Js_fancybox_follow_close"></a>
    </div>
    <div class="box_content" data-member-id="{{{$member->id}}}">
        <h4>为<span>{{{$member->name}}}</span>选择圈子</h4>
        <div class="choose_group Js_fancybox_follow_choose_group">
            <ul class="priority_group clearfix">
            @foreach($circles as $key => $circle)
            @if($circle->name == App\Modules\CircleModule::CIRCLE_DEFAULT_NAME)
                <li class="fl" data-circle-id="{{{$circle->id}}}"><label><input type="checkbox" @if($circle->member_id)checked="checked"@endif>{{{$circle->name}}}<span></span></label></li>
                <?php unset($circles[$key]); break;?>
                @endif
             @endforeach
            </ul>
            <ul class="normal_group clearfix Js_follow_circle_list">
            @foreach($circles as $circle)
                <li class="fl" data-circle-id ="{{{$circle->id}}}"><label><input type="checkbox"  @if($circle->member_id)checked="checked"@endif>{{{$circle->name}}}</label></li>
             @endforeach
            </ul>
            <div class="new_group">
                <a style="display: none" href="javascript:">创建新分组</a>
                <label>
                    <input type="text" placeholder="请输入圈子名称">
                    <a href="javascript:" class="new_group_btn Js_follow_createCircle_btn">创建</a>
                    <span class="Js_follow_error_message_span illus"></span>
                </label>
            </div>
        </div>
        <div class="box_btm_btn clearfix">
            <a href="javascript:" class="cancel fr Js_fancybox_follow_close">取消</a>
            <a href="javascript:" class="ok fr Js_fancybox_follow_save" user-id="{{{$member->id}}}">保存</a>
        </div>
    </div>
</div>