@extends('common.main')

@section('title')
个人中心 作品- 创意世界 
@stop

@section('content')

@include('user.user-top')
<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<ul class="navlist clearfix fl">
				<li><a class="c" href="{{$baseURL}}/user/home/{{{$owner->id}}}">@if(!empty($isMe)) 我@else TA@endif的动态</a>
				</li>
				<li><a class="c" href="{{$baseURL}}/user/contests/{{{$owner->id}}}">@if(!empty($isMe)) 我@else TA@endif的创意世界大赛</a></li>
				<li><a class="c" href="{{$baseURL}}/user/exchanges/{{{$owner->id}}}">@if(!empty($isMe)) 我@else TA@endif的创意交易</a></li>
				<li class="current"><a class="c" href="{{$baseURL}}/user/works/{{{$owner->id}}}">@if(!empty($isMe)) 我@else
						TA@endif的创意作品</a></li>
			</ul>
		</div>
		<div class="sub-nav">
			<ul class="subnav-list clearfix">
				<li
				@if($pageType==0) class="current" @endif><a href="{{$baseURL}}/user/works/{{{$owner->id}}}">全部</a></li>
				<li
				@if($pageType==5) class="current" @endif><a href="{{$baseURL}}/user/works/{{{$owner->id}}}/5">创意作品</a></li>
				<li
				@if($pageType==21) class="current" @endif><a href="{{$baseURL}}/user/works/{{{$owner->id}}}/21">任务作品</a></li>
				<li
				@if($pageType==31) class="current" @endif><a href="{{$baseURL}}/user/works/{{{$owner->id}}}/31">明星作品</a></li>
				@if(!empty($isMe))
				<li
				@if($pageType==4) class="current" @endif><a href="{{$baseURL}}/user/works/{{{$owner->id}}}/4">我喜欢的</a></li>
				@endif
			</ul>
		</div>
		<ul class="list_main clearfix">
			@include('common.m-contest')
		@if(!empty($pageTotal) && $pageTotal > 1)
           {{$pageHtml}}
          @endif
		</ul>
	</div>
	<div class="detail-aside">
		@include('common.hot-contests')

		@include('common.hot-exchanges')

		@include('common.care-people')

		@include('common.complete-info')
	</div>
</div>
@stop