@extends('common.main')

@section('title')
注册成功- 创意世界 
@stop


@section('content')
    <div class="content">
        <div class="height_60"></div>

        <div class="regsucc_wraper">
            <div class="tips_one">
                <p class="info">恭喜您，成功注册了创意世界账号！</p>
                <p class="ccz_num">创创号：<span>{{$RegisterCCNo}}</span></p>
            </div>
            <div class="height_30"></div>
            <div class="tips_two">
                <p>验证邮件已发送至您的邮箱<span>{{$RegisterEmail}}</span></p>
                <p>请进入您的邮箱完成激活流程，点击邮件里的确认链接即可登录创意世界</p>
            </div>
            <div class="height_30" ></div>
            <div class="divide"></div>
            <div class="height_20"></div>
            <div class="tips_three">
                <p>没有收到邮件？</p>
                <ul>
                    <li>请到您的邮箱中垃圾邮件目录查找</li>
                    <li><a href="{{$emailUrl}}"  target="_blank">重发验证信</a></li>
                </ul>
            </div>
        </div>

        <div class="height_170"></div>
    </div>

@stop