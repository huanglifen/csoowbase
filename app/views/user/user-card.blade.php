@if( ! isset($postUserCardHtml))
    <?php $postUserCardHtml = false;?>
@endif

@if($postUserCardHtml)
<div class="card Js_show_card_box" data-user-id="{{$userCard->id}}">
@else
<li class="card fl" data-user-id="{{$userCard->id}}">
@endif
	<div class="card-bg"></div>
	<div class="card-container">
		<div class="user-info">
			<div class="user-address  clearfix">
				<a href="{{$baseURL}}/user/home/{{$userCard->id}}" class="fl"><img src="{{{App\Common\Utils::getAvatar($userCard->avatar)}}}" /></a>
				<div class="right-content fl">
					<div class="clearfix card-u">
						<a class="user-name" href="{{$baseURL}}/user/home/{{$userCard->id}}">{{$userCard->name}}</a>
						<div class="level">
							<div class="icon {{{App\Common\Utils::getLevelStyle($userCard->creative_index)}}}"></div>
							<div class="bar">
								<div class="process" style="width: 60%;"></div>
							</div>
						</div>
					@if($userCard->group_id == App\Modules\UserModule::GROUP_EXPERT)
				        <?php $expertTitle = App\Common\Utils::getExpertTitle($userCard->expert_organization_id);
                       $expertStyle = App\Common\Utils::getExpertStyle($userCard->expert_organization_id);
                         ?>
			           <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
				     @endif
					</div>
				<?php
					$userCard->location = $userCard->province_name . $userCard->city_name . $userCard->district_name;
					$separator = '';
					if ($userCard->location && $userCard->school_name) {
						$separator = '|';
					}
				?>
					<p class="card-area"><span>{{{$userCard->location}}}</span><span class="cut">{{{$separator}}}</span><span>{{{$userCard->school_name}}}</span></p>
					<p class="card-area">{{{$userCard->organization_name}}}</p>
				</div>
			</div>
			<div class="user-intro">{{{$userCard->introduction}}}</div>
		</div>
		<div class="card-atten clearfix">
		@if(!($user && $user->id == $userCard->id)) 
			<div class="card-btn-container">
				@if(!empty($invite))
				<a href="javascript:" class="card-btn Js_cancel_invite">取消邀请</a>
				@else
				    
				  <a href="javascript:;" class="card-btn @if(!$userCard->is_follow)Js_follow_btn @else Js_has_followed_btn  @endif" user-id="{{{$userCard->id}}}">@if($userCard->is_follow)已关注 @else关注 @endif</a>
				  <a href="{{{$baseURL}}}/chat/contact/{{{$userCard->id}}}" target = "_blank" class="card-btn">聊天</a> 				   
				@endif
			</div>
	   @endif
			<div class="card-atten-num">
				<em>{{$userCard->follower_count}}</em>
				<span>关注</span>
			</div>
		</div>
	</div>
@if($postUserCardHtml)
</div>
@else
</li>
@endif

