@if(! isset($owner))
   <?php $owner = $user; $isMe = true;?>
@endif
<div class="person-top-wap person-top-bg">
	<div class="detail-top w1180">
		<div class="pro-user">
			<div class="avatar-wrap"><a href="{{$baseURL}}/user/home/{{$owner->id}}">
					<img class="avatar" src="{{{App\Common\Utils::getAvatar($owner->avatar)}}}" alt=""></a>
			</div>
			<div class="user-info">
				<div class="low-name">
					<a class="uname" href="{{$baseURL}}/user/home/{{$owner->id}}">{{{$owner->name}}}</a>

					<div class="level">
						<div class="icon {{{App\Common\Utils::getLevelStyle($owner->creative_index)}}}"></div>
						<div class="bar">
							<div class="process" style="width: 60%;"></div>
						</div>
					</div>
					
			@if($owner->group_id == App\Modules\UserModule::GROUP_EXPERT)
				<?php $expertTitle = App\Common\Utils::getExpertTitle($owner->expert_organization_id);
                  $expertStyle = App\Common\Utils::getExpertStyle($owner->expert_organization_id);
              ?>
			 <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			 @endif
				</div>
				<?php
					$owner->location = $owner->province_name . $owner->city_name . $owner->district_name;
					$separator = '';
					if ($owner->location && $owner->school_name) {
						$separator = '|';
					}
					$delimiter = '';
					if ($owner->school_name && $owner->organization_name) {
						$delimiter = '|';
					}
				?>
				<p class="low-about"><span>{{{$owner->location}}}</span> {{{$separator}}}
					<span>{{{$owner->school_name}}}</span> {{{$delimiter}}} <span>{{{$owner->organization_name}}}</span>
				</p>

				<p class="low-des">{{{$owner->introduction}}}</p>

				<p class="low-btn">
					@if(empty($isMe))
					<?php 
				     if(isset($isFollow) && $isFollow) {
                            $followName = '已关注';
                            $class = "g Js_has_followed_btn";
                     }else{
                            $followName = '关注';
                            $class = "g Js_follow_btn";
                     }
				    ?>
					<a class="{{{$class}}}" href="javascript:;" user-id="{{{$owner->id}}}">{{{$followName}}}</a>
					<a class="g" href="{{$baseURL}}/chat/contact/{{{$owner->id}}}" target="_blank">聊天</a>
					@else
					<a class="g" href="{{$baseURL}}/account/user-info">修改资料</a>
					@endif
				</p>
			</div>
		</div>
		<div class="pro-btn">
			<p class="atnum"><span>{{{$owner->follower_count}}}</span></p>

			<p class="att"><span>人关注</span></p>
		</div>
	</div>
</div>