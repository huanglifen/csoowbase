@extends('common.main')

@section('title')
注册- 创意世界 
@stop


@section('content')
<div class="content">
        <div class="height_60"></div>

        <div class="register-wrap">
            <div class="reg-title clearfix">
                <h3 class="fl">注册新用户</h3>
                <div class="reg-login fr">已有创意世界账号？<a href="{{{$baseURL}}}/user/login">请登录</a></div>
            </div>
            <form id="formname" action="{{{$baseURL}}}/user/register" method="post">
                <ul class="formwrap clear">
                    <li>
                        <div class="row">
                            <div class="rt">账户：</div>
                            <div class="rc">
                                <div class="con">
                                    <input style="position: relative" type="text" placeholder="输入邮箱" name="email" datatype="e" errormsg="请输入正确的邮箱地址" nullmsg="请填写邮箱账号" ajaxurl="{{$baseURL}}/user/verify-email" id="Js_user_account">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="rt">昵称：</div>
                            <div class="rc">
                                <div class="con">
                                    <input type="text" placeholder="输入昵称" name="name" datatype="s" nullmsg="请填写昵称" value="" ajaxurl="{{$baseURL}}/user/verify-nick">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="rt">密码：</div>
                            <div class="rc">
                                <div class="con">
                                    <input type="password" placeholder="输入密码" name="password" datatype="*6-16" nullmsg="请填写密码" id="Js_register_pwd" value="" />
									<div style="display:none;">
										<input type="password" plugin="passwordStrength" id="Js_register_hid" />
									</div>
                                    <span class="Validform_checktip"></span>
                                    <div class="passwordStrength"><span>弱</span><span>中</span><span class="last">强</span></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="rt">确认密码：</div>
                            <div class="rc">
                                <div class="con">
                                    <input type="password" placeholder="输入确认密码" name="password_again" recheck="password"  datatype="*6-22" nullmsg="请填写确认密码" errormsg="两次输入的密码不一致！" id="Js_register_again">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="rt">验证码：</div>
                            <div class="rc">
                                <div class="con">
                                    <input type="text" class="st" placeholder="" ajaxurl="{{$baseURL}}/user/verify-code" name="verify_code" datatype="s" nullmsg="请填写验证码">
                                    <img class="verify-code" src="{{$baseURL}}/user/verify-code" alt="请输入验证码">
                                    <span class="change-code">看不清？<a href="javascript:;" class="change-verify-code">换一张</a></span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="check_agreement">
                            <input id="is_agree_protocol" name="is_agree_protocol" type="checkbox" checked="checked" nullmsg="请同意协议" datatype="*"/>
                            <label for="is_agree_protocol">我已阅读并同意<a href="{{{$baseURL}}}/user/register-agreement" target="_blank">《创意世界用户协议》</a></label>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="rt"></div>
                            <div class="rc">
							<a class="Js_submit">
                                <button type="button" class="btn DeepBlue">立即注册</button>
								<span style="position:absolute; top:0; left:64px; width:5px; height:5px; color:#FFF; display:none;">.</span>
								<span style="position:absolute; top:0; left:73px; width:5px; height:5px; color:#FFF; display:none;">.</span>
                            </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </form>
        </div>

        <div class="height_170"></div>
        </div>
		<script type="text/javascript" src="{{{$baseURL}}}/js/Validform_v5.3.2.js"></script>
		<script type="text/javascript" src="{{{$baseURL}}}/js/passwordStrength_min.js"></script>
		<script type="text/javascript" src="{{{$baseURL}}}/js/login.js"></script>
@stop


