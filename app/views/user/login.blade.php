@extends('common.main')

@section('title')
登录 - 创意世界 
@stop


@section('content')
<div class="content">
	<div class="height_60"></div>
	<div class="login_container">
		<div class="login_wraper">
			<h1 class="title">用户登录</h1>
			<p class="errorMsg"></p>
			<form id="login_form" method="post">
				<div class="row login_input">
					<div class="rc">
						<input class="Js_username_input" type="text" placeholder="邮箱 / 昵称 / 创创号" name="login_name">
					</div>
				</div>
				<div class="row login_input">
					<div class="rc">
						<input class="Js_password_input" type="password" placeholder="密码" name="password">
					</div>
				</div>
				<div class="clearfix">
					<a class="login_btn Js_loginSubmit">
                        <button type="button" class="btn DeepBlue">登录</button>
					</a>
					<a href="{{{$baseURL}}}/user/forget-password" class="forgetpwd">忘记密码？</a>
				</div>
			</form>
			<div class="divide"></div>
			<p class="tips">没有创意世界账号？<a href="{{{$baseURL}}}/user/register">立即注册</a></p>
		</div>
	</div>
	<div class="height_170"></div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/passwordStrength_min.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/login.js"></script>
@stop

