@extends('common.main')

@section('title')
找回密码- 创意世界 
@stop

@section('content')
<div class="content w1180">
    <div class="height_60"></div>
    <div class="tabs_wraper w980 Js_forget_pwd">
        <div class="tabs_step Js_tabs_step" style="margin-bottom:30px; margin-left: 65px;">
            <div class="hor_line Js_hor_line"><div class="hor_inline Js_hor_inline"></div></div>
			<ul class="Js_tab">
                <li class="first focustab">
                    <a href="javascript:;">
                        <b class="circle" >1</b>
                        <span class="tips">填写账户名</span>
                    </a>
                </li>
                <li class="center Js_tab_list">
                    <a href="javascript:;">
                        <b class="circle">2</b>
                        <span class="tips">验证身份</span>
                    </a>
                </li>
                <li class="center Js_tab_list">
                    <a href="javascript:;">
                        <b class="circle">3</b>
                        <span class="tips">设置新密码</span>
                    </a>
                </li>
                <li class="last Js_tab_list">
                    <a href="javascript:;">
                        <b class="circle">4</b>
                        <span class="tips">完成修改</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="height_30"></div>
        <div class="tabs_con Js_tabs_content" style="display:block; padding-left: 220px;">
            <form id="getUserName" action="{{{$baseURL}}}/user/find-cco-email">
                <ul>
                    <li>
                        <div class="row">
                            <div class="rt">账户名：</div>
                            <div class="rc">
                                <div class="con">
                                    <input style="position: relative" type="text" placeholder="输入账号" name="email" datatype="e" errormsg="请输入正确的邮箱地址" nullmsg="请填写邮箱账号" ajaxurl="{{$baseURL}}/user/find-email">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="rt">验证码：</div>
                            <div class="rc">
                                <div class="con">
									<input type="text" class="st" placeholder="" ajaxurl="{{$baseURL}}/user/verify-code" name="verify_code" datatype="s" nullmsg="请填写验证码" style="width: 108px;">
                                    <img class="verify-code" src="{{$baseURL}}/user/verify-code" alt="">
                                    <span class="change-code">看不清？<a href="javascript:;" class="change-verify-code">换一张</a></span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="rt" style="width:112px;"></div>
                            <div class="rc">
                                <a class="Js_goto_step2" href="javascript:">
                                    <button type="button" class="btn DeepBlue">下一步</button>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </form>
        </div>
        <div class="tabs_con Js_tabs_content" style="display:none; padding-left: 235px;">
            <form id="formname" action="#">
                <div class="prepare_send_email">
                    <ul>
                        <li>
                            <div class="row">
                                <div class="desDropDownMenu clearfix">
                                    <label>请选择验证身份方式：</label>
                                    <div class="dropDownMenu w187 fl">
                                        <div class="dropDownMenuInput Js_findPasswordType">
                                            <s class="dropDownBtn"></s>
                                            <span class="promptInfo"></span>
                                            <ul class="dropDownList"></ul>
                                            <select></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="rt" style="width:162px;">创创号：</div>
                                <div class="rc">
                                    <div class="con">
                                        <p style="font-size: 16px" id="Js_ccNo">CC102424</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="Js_phone_t" style="display:none">
                            <div class="row">
                                <div class="rt" style="width:162px;">手机号码：</div>
                                <div class="rc">
                                    <div class="con" id="new_mobile">
                                        <p class="tex" id="Js_ccPhone"></p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="Js_phone_t" style="display:none">
                            <div class="row">
                                <div class="rt" style="width:162px;">短信验证码：</div>
                                <div class="rc">
                                    <div class="con" id="verify_code_mobile">
                                        <input class="Js_phone_code" style="width: 118px" type="text" placeholder="请输入验证码" name="verify_code" value="" ignore="" errormsg="验证码有误！" nullmsg="请填写短信验证码！">
                                        <a class="mobile_phone_auth-edit-btn Js_getCode" data-codenumtype="phone" href="javascript:">免费获取验证码</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="Js_email_t">
                            <div class="row">
                                <div class="rt" style="width:162px;">邮箱地址：</div>
                                <div class="rc">
                                    <div class="con">
                                        <p id="Js_ccEmail"></p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="Js_email_t">
                            <div class="row">
                                <div class="rt" style="width:162px;">邮箱验证码：</div>
                                <div class="rc">
                                    <div class="con" id="verify_code_email">
                                        <input class="Js_email_code" style="width: 118px" type="text" placeholder="请输入验证码" name="verify_code" value="" ignore="" errormsg="验证码有误！" nullmsg="请填写短信验证码！">
                                        <a class="mobile_phone_auth-edit-btn Js_getCode" data-codenumtype="email" href="javascript:">免费获取验证码</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="Js_email_t">
                            <div class="row">
                                <div class="rt" style="width:162px;"></div>
                                <div class="rc">
                                    <a href="javascript:">
                                        <a class="Js_send_email">
                                            <button type="button" class="btn LightBlue">确定</button>
                                        </a>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="Js_phone_t" style="display:none">
                            <div class="row">
                                <div class="rt" style="width:162px;"></div>
                                <div class="rc">
                                    <a href="javascript:">
                                        <a class="Js_send_phone">
                                            <button type="button" class="btn LightBlue">确定</button>
                                        </a>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </form>
        </div>
        <div class="tabs_con Js_tabs_content" style="display:none; padding-left: 230px;">
            <form id="enterNewPwd" action="{{{$baseURL}}}/user/reset-password">
                <ul>
                    <li>
                        <div class="row">
                            <div class="rt">请输入新密码：</div>
                            <div class="rc">
                                <div class="con">
                                    <input type="password" placeholder="输入密码" name="password" datatype="*6-22" nullmsg="请填写密码" errormsg="密码至少6个字符,最多22个字符！">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="rt">再次输入新密码：</div>
                            <div class="rc">
                                <div class="con">
                                    <input type="password" placeholder="输入确认密码" name="password_again" recheck="password"  datatype="*6-22" nullmsg="请填写确认密码" errormsg="两次输入的密码不一致！">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <span style="margin-left: 120px" class="Js_goto_step4">
                            <a href="javascript:;">
                                <a class="">
                                    <button type="button" class="btn DeepBlue">下一步</button>
                                </a>
                            </a>
                        </span>
                    </li>
                </ul>
            </form>
        </div>
        <div class="Js_tabs_content" style="display:none;">
            <div class="fpwd_wraper">
                <div class="tips">
                    <p>恭喜您！您的密码设置成功</p>
                    <small>您的新密码已经生效，您需要重新以新密码登录创意世界</small>
                </div>
                <div style="height:30px;"></div>
                <div class="operate">
                    <a href="{{$baseURL}}/user/login">
                        <button type="button" class="btn DeepBlue">登录创意世界</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="posttrend_btn Js_posttrend_btn">
    <a href="javascript:">发布动态</a>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/tabs.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.dropDownMenu.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/login.js"></script>


@stop
