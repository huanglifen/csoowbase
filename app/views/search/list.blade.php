@if(count($users))
<div class="users">
	<h4>用户</h4>
	<ul>
		@foreach($users as $user)
		<li class="clearfix">
			<a href="{{$baseURL}}/user/home/{{$user->id}}">
				<img class="ava_img fl" src="{{{App\Common\Utils::getAvatar($user->avatar)}}}">
				<div class="tips fl">
					<div class="tipone">
						<span class="name">{{$user->name}}</span>
						<div class="level">
							<div class="icon {{{App\Common\Utils::getLevelStyle($user->creative_index)}}}"></div>
							<div class="bar">
								<div class="process" style="width: 60%;"></div>
							</div>
						</div>
					</div>
					<div class="tiptwo">
						<img class="fl tip_icon" src="{{{$baseURL}}}/images/icon_placeholder_14x14.png">
						<span class="ccznum fl">{{$user->cc_no}}</span>
					</div>
				</div>
			</a>
		</li>
		@endforeach
	</ul>
</div>
@endif

@if(count($items))
<div class="ccz_slist games_exchange">
	<h4>创意世界大赛、创意世界交易所</h4>
	<ul>
		@foreach($items as $item)
		@if($item)
			@if($item->target_type == 1)
			<li><a href="{{$baseURL}}/project/detail/{{$item->target_id}}"  class="games"><image class="ico" src="{{{$baseURL}}}/images/icon_project.png">{{$item->title}}</a></li>
			@elseif($item->target_type == 2)
			<li><a href="{{$baseURL}}/task/detail/{{$item->target_id}}"  class="games"><image class="ico" src="{{{$baseURL}}}/images/icon_task.png">{{$item->title}}</a></li>	
			@elseif($item->target_type == 3)
		    <li><a href="{{$baseURL}}/hiring/detail/{{$item->target_id}}"  class="games"><image class="ico" src="{{{$baseURL}}}/images/icon_hire.png">{{$item->title}}</a></li>
			@else
			<li><a href="{{$baseURL}}/exchange/detail/{{$item->target_id}}"  class="games"><image class="ico" src="{{{$baseURL}}}/images/icon_exchange.png">{{$item->title}}</a></li>
			@endif
		@endif
		@endforeach
	</ul>
</div>
@endif
                
@if(count($areas))
<div class="ccz_slist areas">
	<h4>创意世界地区</h4>
	<ul>
		@foreach($areas as $area)
		@if($area)
			@if($area['item_type'] == 3)
				<li><a href="{{$baseURL}}/area/organization/{{$area->item_id}}" class="games"><image class="ico" @if(!$area->logo)src="{{{$baseURL}}}/images/icon_area_gov.png"@else src="{{{$baseURL}}}/{{{$area->logo}}}"@endif>{{$area->name}}</a></li>
			@elseif($area['item_type'] == 2)
				<li><a href="{{$baseURL}}/area/school/{{$area->item_id}}" class="games"><image class="ico" @if(!$area->logo)src="{{{$baseURL}}}/images/icon_area_school.png"@else src="{{{$baseURL}}}/{{{$area->logo}}}"@endif>{{$area->name}}</a></li>
			@elseif($area['item_type'] == 1)
				<li><a href="{{$baseURL}}/area/region/{{$area->item_id}}" class="games"><image class="ico" @if(!$area->logo)src="{{{$baseURL}}}/images/icon_area_area.png"@else src="{{{$baseURL}}}/{{{$area->logo}}}"@endif>{{$area->name}}</a></li>
		    @else
		        <li><a href="{{$baseURL}}/area/organization/{{$area->item_id}}" class="games"><image class="ico" @if(!$area->logo)src="{{{$baseURL}}}/images/icon_area_enterprise.png"@else src="{{{$baseURL}}}/{{{$area->logo}}}"@endif>{{$area->name}}</a></li>
		    @endif
		@endif
		@endforeach
	</ul>
</div>
@endif

@if(count($encyclopedias))
<div class="ccz_slist wiki">
	<h4>创意百科</h4>
	<ul>
		@foreach($encyclopedias as $encyclopedia)
		<li><a href="{{$baseURL}}/encyclopedia/detail/{{$encyclopedia->id}}" class="games"><image class="ico" src="{{{$baseURL}}}/images/icon_encyclopedia.png">{{$encyclopedia->title}}</a></li>
		@endforeach
	</ul>
</div>
@endif

<a href="{{$baseURL}}/search/index/{{{$keyword}}}" class="more">查看全部搜索结果</a>
