@extends('common.main')

@section('title')
搜索结果 - 创意世界
@stop

@section('content')
    <div class="height_40"></div>
    <div class="content w1180 clearfix">
        <div class="search-content">
            <div class="match-nav clearfix">
                <ul class="navlist clearfix fl">
                    <li @if($type==0|| $type == 1 || $type == 2 || $type == 3)class="current"@endif><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/0">创意世界大赛</a></li>
                    <li @if($type==4)class="current"@endif><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/4">创意世界交易所</a></li>
                    <li @if($type==5)class="current"@endif><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/5">创意百科</a></li>
                    <li @if($type==6||$type==63||$type==64)class="current"@endif><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/6">用户</a></li>
                    <li @if($type==7||$type==71||$type==72||$type==73)class="current"@endif><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/7">创意世界地区</a></li>
                    <li @if($type==9)class="current"@endif><a class="c" href="{{$baseURL}}/search/outer/{{{$keyword}}}">云搜索</a></li>
                </ul>
            </div>
	        @if($type==0|| $type == 1 || $type == 2 || $type == 3)
            <div class="sub-nav">
                <ul class="subnav-list clearfix">
                    <li @if($type==0)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/0">全部</a></li>
                    <li @if($type==1)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/1">创意投资项目比赛</a></li>
                    <li @if($type==2)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/2">创意任务人才比赛</a></li>
                    <li @if($type==3)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/3">创意公益明星比塞</a></li>
                </ul>
            </div>
	        @elseif($type==6||$type==63||$type==64)
	        <div class="sub-nav">
		        <ul class="subnav-list clearfix">
			        <li @if($type==6)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/6">全部</a></li>
			        <li @if($type==63)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/63">专家</a></li>
			        <li @if($type==64)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/64">其他用户</a></li>
		        </ul>
	        </div>
	        @elseif($type==7||$type==71||$type==72||$type==73)
	        <div class="sub-nav">
		        <ul class="subnav-list clearfix">
			        <li @if($type==7)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/7">全部</a></li>
			        <li @if($type==71)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/71">地区</a></li>
			        <li @if($type==72)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/72">学校</a></li>
			        <li @if($type==73)class="current"@endif><a href="{{$baseURL}}/search/index/{{{$keyword}}}/73">政企</a></li>
		        </ul>
	        </div>
	        @endif
            <div class="list_main">
                <ul id="list_content" class="clearfix">
	                @if($type == 0 || $type == 1 || $type == 2 || $type == 3 || $type==4 || $type==5)
	                    @include('common.contest-page')
	                @elseif($type==6||$type==63||$type==64)
	                    @include('area.user-list')
	                @elseif($type==7||$type==71||$type==72||$type==73)
	                    @include('area.area-list')
	                @else
	                	@include('common.contest-page')
	                @endif
                </ul>
                @if($pageTotal > 1)
                <div class="paging">
					{{$pageHtml}}
				</div>
				@endif
            </div>
        </div>
    </div>
    
@stop