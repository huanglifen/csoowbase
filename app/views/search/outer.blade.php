@extends('common.main')

@section('title')
搜索结果 云搜索 - 创意世界
@stop

@section('content')
<div class="height_40"></div>
<div class="content w1180 clearfix">
	<div class="search-content">
		<div class="match-nav clearfix">
			<ul class="navlist clearfix fl">
				<li><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/0">创意世界大赛</a></li>
				<li><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/4">创意世界交易所</a></li>
				<li><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/5">创意百科</a></li>
				<li><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/6">用户</a></li>
				<li><a class="c" href="{{$baseURL}}/search/index/{{{$keyword}}}/7">创意世界地区</a></li>
				<li class="current"><a class="c" href="javascript:">云搜索</a></li>
			</ul>
		</div>

		<div class="height_20"></div>
		<div class="w940">
			@if ($isTimeOut)
			<p class="web-search-r">请求超时</p>
			@else
			<p class="web-search-r">获得约 {{{$result_number}}} 条结果，以下是第 {{{$current_page}}} 页 （用时 {{{$use_time}}} 秒）</p>
			<div>
				@foreach ($result as $res)
				<dl class="web-search">
					<a href="{{{$res['link']}}}" target="_blank"><dt>{{{$res['title']}}}</dt></a>
					<dd style="">
						<span>{{{$res['snippet']}}}</span>
					</dd>
				</dl>
				@endforeach
			</div>

			<p class="search-page">
				<?php for ($i=$current_page - 5; $i< $current_page + 10;$i++) {?>
					@if ($i <= 0)
					<?php $i = 0;continue;?>
					@else
					<a href="{{{$baseURL}}}/search/outer/{{{$keyword}}}/{{$i}}"
				@if($i == $current_page)
				class="on"
				@endif
				>{{{$i}}}</a>
			@endif
			<?php }?>
				@if ($result_number > 0)
				<a href="{{{$baseURL}}}/search/outer/{{{$keyword}}}/{{$current_page + 1}}">>></a>
				@endif
			</p>
			@endif
		</div>

	</div>
</div>

@stop

