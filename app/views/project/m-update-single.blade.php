<!--项目动态-->
<div class="height_20"></div>
<div class="dynamic">
	<div class="header_update">
		<h3>项目动态</h3>
		@if($isPublisher)
		<a class="join Js_update_trend" href="javascript:" data-contest-id="{{{$project->id}}}">更新项目</a>
		@include('common.m-publish-update')
		@endif
	</div>
	@include('common.m-contest-update')
	<div class="des-line"></div>
</div>
<!-- 项目交流-->
@include('common.comments')
