@extends('common.main')
@section('title')
  {{{$project->title}}} - 我要投资
@stop
@section('content')
<div class="content w1180">
	<div class="height_60"></div>
	<div class="tabs_wraper w980">
		<div class="tabs_step Js_tabs_step" style="margin-bottom:30px; margin-left:75px;">
			<div class="hor_line Js_hor_line">
				<div class="hor_inline Js_hor_inline"></div>
			</div>
			<ul class="Js_tab">
				<li class="first focustab">
					<a href="#step-one">
						<b class="circle">1</b>
						<span class="tips">基本信息</span>
					</a>
				</li>
				<li class="last">
					<a href="#step-two">
						<b class="circle">2</b>
						<span class="tips">确认协议</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="tabs_con Js_tabs_content" style="display:block;">
			<form id="publish_project_join_form" action="#">
				<input type="hidden" id="project_id" value="{{{$projectId}}}" name="project_id">
				<input type="hidden" id="Js_target_type" value="1" />
				@if ($invest)
					<input type="hidden" id="invest_id" value="{{{$invest->id}}}" name="invest_id" />
				@endif
				<ul>
					<li>
						<div class="row">
							<div class="rt" style="width:130px;">您要投资的金额：</div>
							<div class="rc">
								<div class="con" id="amount">
									<input type="text" class="Js_amount_input Js_NUMBER"
									@if (isset($invest->amount)) 
									value="{{{$invest->amount}}}"
									@endif
									placeholder="" name="amount" datatype="n" errormsg="" nullmsg="" />
									<span class="des">元</span>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width:130px;">选择投资方式：</div>
							<div class="rc">
								<div class="Js_investment">
									<div class="con">
										<!--如果radio关联了，隐藏类的交互事件添加就Js_check-->
										<span class="item Js_check"><input id="r1" type="radio" name="type" value="1" 
										@if (isset($invest->type) && $invest->type == 1 || !isset($invset->type))
										checked="checked"
										@endif
										><label for="r1">一次性付清</label></span>
										<span class="item Js_check"><input id="r2" type="radio" name="type" value="2"
										@if (isset($invest->type) && $invest->type == 2)
										checked="checked"
										@endif										
										><label for="r2">先付定金再付尾款</label></span>
									</div>
									<div class="inact">
										<!--当前交互内容属于第几个item就在ibox中添加Js_itemN-->
										<div class="ibox clearfix Js_item2" style="display: none;">
											<div class="clearfix" id="deposit_percentage">
												<div class="fl" style="margin-right: 30px; min-width:220px;">
													<p>定金支付比例</p>
													<p><input type="text" class="Js_proportion_input1 Js_NUMBER" name="deposit_percentage" style="width: 55px"
													@if (isset($invest->deposit_percentage))
													value="{{{$invest->deposit_percentage}}}"
													@endif
													/>
													&nbsp;&nbsp;%
													<span>金额</span><i class="Js_pro_num1">
													@if ($invest)
													{{{$invest->deposit_percentage * $invest->amount / 100}}}
													@endif
													</i>元</p>
													<p class="Validform_checktip Validform_wrong" style="display:none"></p>
												</div>
												<div class="fl">
													<p>定金支付时间</p>
													<p>投资申请被接受后3天内</p>
												</div>
											</div>
											<div class="clearfix" id="final_pay_time">
												<div class="fl" style="margin-right: 30px; min-width:220px;">
													<p>尾款支付比例</p>
													<p><input type="text" class="Js_proportion_input2 Js_NUMBER" style="width: 55px"
													@if (isset($invest->deposit_percentage))
													value="{{{100 - $invest->deposit_percentage}}}"
													@endif
													/>&nbsp;&nbsp;%<span>金额</span>
													<i class="Js_pro_num2">
													@if ($invest)
													{{{(100 - $invest->deposit_percentage) * $invest->amount / 100}}}
													@endif
													</i>元</p>
												</div>
												<div class="fl">
													<p>支付尾款时间</p>
													
													<p>投资申请被接受后<input type="text" class="Js_pay_time Js_NUMBER" name="final_pay_time" placeholder="须大于3" style="width: 68px; margin:0 5px"
													@if (isset($invest->final_payment_pay_time)&& $invest->final_payment_pay_time > 0)
													<?php
                                                           $finalPayTime = $invest->final_payment_pay_time > 4 ? $invest->final_payment_pay_time : 4;
                                                    ?>
													value="{{{$finalPayTime}}}"
													@else
													value="4"
													@endif
													/>天内</p>
													<p class="Validform_checktip Validform_wrong" style="display:none"></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width:130px;">要求项目完成期限：</div>
							<div class="rc" id="">
								<div class="Js_finishedtime">
									<div class="con" id="require_complete_time">
										<!--如果radio关联了，隐藏类的交互事件添加就Js_check-->
										<span class="item Js_check"><input id="f1" type="radio" name="finishedtime" 
										@if (isset($invest->require_complete_time) && !$invest->require_complete_time || !isset($invest->require_complete_time))
										checked="checked"
										@endif
										><label for="f1">不作要求</label></span>
										<span class="item Js_check"><input id="f2" type="radio" name="finishedtime"
										@if (isset($invest->require_complete_time) && $invest->require_complete_time)
										checked="checked"
										@endif
										><label for="f2">限制时间</label></span>
									</div>
									<div class="inact">
										<!--当前交互内容属于第几个item就在ibox中添加Js_itemN-->

										<div class="ibox Js_item2" style="display: block;">
											<p class="c">支付款项后<input type="text" class="Js_NUMBER" name="require_complete_time"  style="width: 100px; margin: 0 5px" 
											@if (isset($invest->require_complete_time))
											value="{{{$invest->require_complete_time}}}"
											@endif
											>
											天内</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width:130px;">您的项目要求：</div>
							<div class="rc">
								<div class="con" id="requirement">
									<textarea name="requirement" id="" style="padding:5px; width:400px; height:120px;vertical-align: bottom; overflow:auto">@if (isset($invest->requirement)) {{{$invest->requirement}}} @endif</textarea>
								</div>
							</div>
						</div>
					</li>
					<li class="clearfix">
						<div class="row">
							<div class="rc" style="margin-left:138px;">
								<a class="Js_goto_step2" href="#step-two">
									<button type="button" class="btn DeepBlue">下一步</button>
								</a>
							</div>
						</div>
					</li>
				</ul>
			</form>
		</div>
		<div class="tabs_con Js_tabs_content" style="display:none;">
			<div class="agreement">
				<h1>《创意投资项目比赛合作协议》<i>编号：{{{$project->no}}}</i></h1>
				<div class="separator"></div>
                <p class="preface">本合作协议（“<em>本协议</em>”）由以下双方于{{{date('Y年m月d日', time())}}}签订：</p>
                <div class="definition">
		            <div class="party_ab">
		                <h2><em>甲方</em></h2>
		                <p>创意世界用户名：<em>{{{$publisher->name}}}</em></p>
		                <p>身份证号：<em></em></p>
		                <p>电子邮件地址：<em>{{{$publisher->email}}}</em></p>
		            </div>
		            <div class="party_ab">
		                <h2><em>乙方</em></h2>
		                <p>创意世界用户名：<em>{{{$user->name}}}</em></p>
		                <p>身份证号：<em></em></p>
		                <p>电子邮件地址：<em>{{{$user->email}}}</em></p>
		            </div>
                </div>
                <div class="separator"></div>
                <div class="height_30"></div>
                <div class="info_content">
                    <p>就<em>甲方</em>通过由四川创意世界科技有限公司（“<em>创意世界</em>”）运营管理的<em>创意世界网</em>（域名为<a href="www.csoow.com">www.csoow.com</a>，“<em>创意世界大赛-创意投资项目比赛</em>”）服务向<em>乙方</em>寻求创意项目事宜，双方根据平等、自愿的原则，达成<em>本协议如</em>下：</p>
                    <ol class="level1">
                        <li>
                            <strong>投资金额及投资方式</strong>
                            <ul>
                                <li>
                                    <em>甲方</em>同意通过<em>创意投资项目比赛</em>服务向<em>乙方</em>寻求创意项目投资，<em>乙方</em>同意通过<em>创意投资项目比赛</em>服务向<em>甲方</em>发放创意项目投资款项：
                                    <ul class="level3">
                                        <li>创意项目投资金额：<em class="Js_t1"></em></li>
                                        <li>创意项目投资方式：<em class="Js_t2"></em></li>
                                        <li>完成创意项目的最后期限：<em class="Js_t3"></em></li>
                                        <li>投资要求：<em class="Js_t4"></em></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <strong>创意项目投资流程</strong>
                            <p><em>本协议</em>成立及生效：<em>甲方</em>按照<em>创意投资项目比赛</em>的规则，通过在<em>创意投资项目比赛</em>上对<em>乙方</em>提交的创意项目投资申请点击“接受投资”按钮确认时，<em>本协议</em>立即成立并且生效。</p>
                        <li>
                            <strong>投资资金来源保证</strong>
                            <p><em>乙方</em>保证其所用于投资的资金来源合法，<em>乙方</em>是该资金的合法所有人，如果第三方对资金归属、合法性问题发生争议，由<em>乙方</em>自行负责解决。如<em>乙方</em>未能解决，则放弃享有其所获创意投资项目比赛权益的权利。</p>
                        <li>
                            <strong>逾期完工</strong>
                            <p><em>甲方</em>超过创意项目完成的最后期限完成创意项目，<em>甲方</em>有权要求撤回创意项目投资款项并终止本协议。</p>
                        </li>
                        <li>
                            <strong>变更通知</strong>
                            <ol class="level2">
                                <li><em>本协议</em>签订之日起至<em>乙方</em>确认创意项目完成之日止，<em>甲方</em>有义务在其向<em>乙方</em>提供的任何信息变更3天内通过<em>创意投资项目比赛</em>项目更新后的信息给<em>乙方</em>并提交相应的证明文件，包括但不限于<em>甲方</em>姓名、身份证号码、住址等个人基本信息、其他信息等的变更。</li>
                                <li>若因<em>甲方</em>不及时提供上述变更信息而带来的<em>乙方</em>的调查及诉讼费用将由<em>甲方</em>承担。</li>
                            </ol>
                        </li>
                        <li>
                            <strong>本协议的转让</strong>
                            <p>未经<em>乙方</em>事先书面（包括但不限于电子邮件等方式）同意，<em>甲方</em>不得将<em>本协议</em>项下的任何权利义务转让给任何第三方。</p>
                        </li>
                        <li>
                            <strong>其他</strong>
                            <ol class="level2">
                                <li><em>本协议</em>的任何修改、补充均须以<em>创意世界大赛</em>平台电子文本形式作出。</li>
                                <li>甲乙双方均确认，<em>本协议</em>的签订、生效和履行以不违反法律为前提。如果<em>本协议</em>中的任何一条或多条违反适用的法律，则该条将被视为无效，但该无效条款并不影响<em>本协议</em>其他条款的效力。</li>
                                <li>如果甲乙双方在<em>本协议</em>履行过程中发生任何争议，应友好协商解决；如协商不成，则须提交<em>甲方</em>或<em>乙方</em>所在地人民法院进行诉讼。</li>
                                <li>甲乙双方委托<em>创意世界</em>保管所有与<em>本协议</em>有关的书面文件或电子信息。</li>
                            </ol>
                        </li>
                    </ol>
                </div>
                <div class="height_30"></div>
				<a class="Js_goto_step1" href="#step-one" style="margin-left: 15px">
					<button type="button" class="btn DeepGrey">上一步</button>
				</a>&nbsp;&nbsp;
				<a class="Js_project_join_submit" href="javascript:">
					<button type="button" class="btn DeepBlue">同意</button>
				</a>
			</div>
			<div class="height_20"></div>
		</div>
	</div>
</div>
	<script type="text/javascript" src="{{{$baseURL}}}/js/editor/umeditor.config.js" charset="utf-8" ></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/editor/_examples/editor_api.js" charset="utf-8" ></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/editor/lang/zh-cn/zh-cn.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/radioitem.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/zebra_datepicker.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/tabs.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/publish.js"></script>
@stop