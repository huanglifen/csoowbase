<div class="height_20"></div>
<!-- 项目简介 -->
<div class="pro-summary">
	<div class="pro-summary-top">
		<span class="t">项目简介</span>
	</div>
	<div class="pro-summary-con show-editor">{{$project->description}}</div>
	<div class="pro-more">
		<a href="{{{$baseURL}}}/project/info/{{{$project->id}}}">更多>></a>
	</div>
</div>
<div class="des-line"></div>
<!-- 项目动态 -->
<div class="dynamic">
	<div class="header_update">
		<h3>项目动态</h3>
		@if($isPublisher) <a class="join Js_update_trend" 
			href="javascript:" data-contest-id="{{{$project->id}}}">更新项目</a> 
			@include('common.m-publish-update')
		@endif
	</div>
	@if(count($updates))
	@include('common.m-updates')
	<div class="pro-more">
		<a href="{{{$baseURL}}}/project/update/{{{$project->id}}}">更多>></a>
	</div>
	@else
    <div class="no_content">
	  <div class="tip">暂无</div>
     </div>
   @endif
</div>
<div class="des-line"></div>
<!-- 专家点评 -->
@include('common.expert-comments',array('showCommentUrl'=>$baseURL."/project/expert/".$project->id,'commentBtnTitle'=>'发布专家点评','showCommentInputBox'=>false))
<div class="des-line"></div>
<!-- 最新洽谈投资者 -->
<div class="discuss-wrap">
	<div class="dis-top clearfix">
		<span class="t fl">最新洽谈投资者</span>
		@if($canApply)
		<a class="ebtn fr" href="{{{$baseURL}}}/project/apply/{{{$project->id}}}">我要投资</a>
		@endif
	</div>
    <div class="height_10"></div>
<?php $count = count($invests);?>
@if($count)
	<ul class="expert-list">
		@foreach($invests as  $key => $invest)
		@if(!empty($invest->user))
		<li>
			<div class="list-item clearfix">
				<a href="{{{$baseURL}}}/user/home/{{{$invest->user->id}}}"  @if(! $user || $user->id != $invest->user->id)class="Js_show_user_info_card" @endif data-user-id="{{$invest->user->id}}"><img class="avatar"
					src="{{{App\Common\Utils::getAvatar($invest->user->avatar)}}}"
					alt=""></a>
				<div class="item-t">
					<span class="t"><a href="javascript:;" @if(! $user || $user->id != $invest->user->id)class="Js_show_user_info_card" @endif data-user-id="{{{$invest->user->id}}}">{{{$invest->user->name}}}</a></span>
					<div class="level">
						<div class="icon {{{App\Common\Utils::getLevelStyle($invest->user->creative_index)}}}"></div>
						<div class="bar">
							<div class="process" style="width: 60%;"></div>
						</div>
					</div>
			    @if($invest->user->group_id == App\Modules\UserModule::GROUP_EXPERT)
                <?php $expertTitle = App\Common\Utils::getExpertTitle($invest->user->expert_organization_id);
                      $expertStyle = App\Common\Utils::getExpertStyle($invest->user->expert_organization_id);
                ?>
                <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			    @endif
                <div class="fr">
                    <p class="date">提交投资申请时间：{{{App\Common\Utils::formatDate($invest->create_time)}}}</p>
                    <p class="date">要求项目完工时间：
                    @if($invest->require_complete_time)
                        @if(!empty($winner) && $winner->user_id == $invest->user_id)
                            <?php
                             if($invest->type == App\Modules\ProjectModule::INVEST_TYPE_STAGES) {
                                  $requireCompleteTime = $invest->accept_time + ($invest->final_payment_pay_time + $invest->require_complete_time) * 86400;
                             }else{
                                  $requireCompleteTime = $invest->accept_time + (3 + $invest->require_complete_time) * 86400;
                             }
                              ?>
                        {{{App\Common\Utils::formatDate($requireCompleteTime)}}}
                            @else
                                款项支付后{{{$invest->require_complete_time}}}天内
                            @endif
                        @else
                        不作要求
                   @endif
                </p>
                </div>
				</div>
                @if($key < ($count-1))
                <div class="dotline"></div>
                @endif
			</div>
		</li> 
		@endif
	@endforeach
	</ul>
	<div class="pro-more">
		<a href="{{{$baseURL}}}/project/invest/{{{$project->id}}}">更多>></a>
	</div>
@else
    <div class="no_content">
	<div class="tip">暂无</div>
</div>
@endif
</div>
<div class="des-line"></div>
<!-- 项目交流 -->
@include('common.comments')
