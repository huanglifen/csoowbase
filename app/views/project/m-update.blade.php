<!--项目动态-->
<div class="height_20"></div>
<div class="dynamic">
	<div class="header_update">
		<h3>项目动态</h3>
		@if($isPublisher)
		<a class="join Js_update_trend" href="javascript:" data-contest-id="{{{$project->id}}}">更新项目</a>
		@include('common.m-publish-update')
		@endif
	</div>
 @if(count($updates))
	@include('common.m-updates')
  @else
    <div class="no_content">
	  <div class="tip">暂无</div>
     </div>
 @endif
	<div class="des-line"></div>
</div>
<!-- 项目交流-->
@include('common.comments')
