@extends('common.main') 
@section('title') 
{{{$project->title}}} - 创意投资项目比赛
@stop
@section('content')
<div class="detail-top-wap top-project">
 <?php
    $contest = $project;
    $applyName = "我要投资";
    $linkString = 'project';
?>
@if(!empty($winner) && $isPublisher)
   <?php $showAgreement = true;
?> 
@endif
@if($project->status == App\Modules\ProjectModule::STATUS_END_SUCCESS)
    <?php $statusContent = '项目成功';?>
@elseif($project->status <= 0 && $project->status > -3)
    <?php $statusContent = '已关闭';?>
@elseif($project->status <= -3)
	<?php $statusContent = '审核中';?>
@else
    <?php $statusContent = '项目正在进行中';?>
@endif
@include('common.contest-detail-top')
</div>
<?php $targetType = App\modules\ProjectModule::TYPE_PROJECT; $targetId = $project->id;?>
<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<ul class="navlist clearfix fl">
				<li @if($type== 1) class="current" @endif><a class="c"
					href="{{{$baseURL}}}/project/detail/{{{$project->id}}}">首页</a></li>
				<li @if($type== 2) class="current" @endif><a class="c"
					href="{{{$baseURL}}}/project/info/{{{$project->id}}}"
					data-category="">项目详情</a></li>
				<li @if($type== 3) class="current" @endif><a class="c"
					href="{{{$baseURL}}}/project/expert/{{{$project->id}}}"
					data-category="">专家点评</a><i class="num">{{{$expertCommentNum}}}</i></li>
				<li @if($type== 4) class="current" @endif><a class="c"
					href="{{{$baseURL}}}/project/invest/{{{$project->id}}}"
					data-category="">洽谈投资者</a><i class="num">{{{$investNum}}}</i></li>
				<li @if($type== 5 || $type == 6) class="current" @endif><a class="c"
					href="{{{$baseURL}}}/project/updates/{{{$project->id}}}"
					data-category="">项目动态</a></li>
			</ul>
		</div>
@if($type == 2) 
	@include('project.m-info') 
@elseif($type == 3)
	@include('project.m-expert-comment') 
@elseif($type == 4)
	@include('project.m-invest') 
@elseif($type == 5)
	@include('project.m-update') 
@elseif ($type == 6)
	@include('project.m-update-single')
@else 
    @include('project.m-detail') 
@endif
	</div>
	<div class="detail-aside">
		<div class="invest-money">
			<div class="money">
				<em>¥</em> <span>{{{$project->goal_amount}}}</span>
			</div>
			<p>所需投资金额</p>
		</div>
		<div class="pro">
			<div class="pro-content">
				<strong>项目投资者</strong>
				<div class="clearfix content">
					@if($winner)
					<div class="user-avatar">
						<a href="{{$baseURL}}/user/home/{{$winner->user->id}}"><img
							src="{{{App\Common\Utils::getAvatar($winner->user->avatar)}}}" /></a>
					</div>
					<div class="user-info">
						<div class="user-name">
							<span>{{{$winner->user->name}}}</span>
							<div class="level">
								<div class="icon {{{App\Common\Utils::getLevelStyle($winner->user->creative_index)}}}"></div>
								<div class="bar">
									<div class="process" style="width: 60%;"></div>
								</div>
							</div>
						</div>
						<span class="user-city">{{{$winner->user->province_name.$winner->user->city_name.$winner->user->district_name}}}</span>
						<div class="user-mutual">
							<div class="clearfix mutual-btn">
								@if($isWinner) 
								<a href="{{{$baseURL}}}/project/agreement/{{{$project->id}}}">查看协议</a> 
								@else
					<?php 
				      if(isset($winner->user->is_follow)&& $winner->user->is_follow) {
				        $class = "Js_has_followed_btn";
				        $followname = '已关注';
			           }else{
                            $class = "Js_follow_btn";
                            $followname = '关注';
                       }
				    ?>
								 <a  class ="{{{$class}}}" user-id="{{{$winner->user->id}}}" href="javascript:;">{{{$followname}}}</a> 
								 <a href="{{{$baseURL}}}/chat/contact/{{{$winner->user_id}}}" target="_blank">聊天</a> 
							@endif
							</div>
						</div>
					</div>
					@else
					<div class="user-avatar">
						<a href="#"><img src="{{{App\Common\Utils::getAvatar('')}}}" /></a>
					</div>
					<div class="user-info">
						<div class="user-name">
							<span>虚位以待</span>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
		<div class="pro">
			<div class="pro-content">
				<strong>项目进程</strong>
				<div class="guide">
					<div class="lt"></div>
					<ul class="guideList">
					@if($project->status <= 0 && $project->status > -3)
					<li class="passed"><span>比赛已经关闭</span>
					<p>{{{App\Common\Utils::formatDate($project->close_time)}}}</p>
					</li>
					@elseif($project->status <= -3)
					<li class="focus"><span>审核中</span>
					</li>
					@elseif($project->status == App\Modules\ProjectModule::STATUS_GOING)
						<li class="focus"><span>正在寻找投资</span>
						<p>{{{$project->investment_end_time ? App\Common\Utils::getRemainTime($project->investment_end_time):'长期'}}}</p></li>
					@elseif($project->status == App\Modules\ProjectModule::STATUS_CONFIRM_INVEST)
					       <li class="passed"><span>选定投资者</span>
						   <p>{{{App\Common\Utils::formatDate($winner->accept_time)}}}</p></li>
					    @if($winner->type == App\Modules\ProjectModule::INVEST_TYPE_TOTAL)
					        <li class="focus"><span>即将支付全款</span>
					        <p>{{{App\Common\Utils::getRemainTime($winner->accept_time+3*86400)}}}</p>
					        </li>
					    @elseif($winner->status == App\Modules\ProjectModule::INVEST_STATUS_ACCEPT)
					        <li class="focus"><span>即将支付定金</span>
					        <p>{{{App\Common\Utils::getRemainTime($winner->accept_time+3*86400)}}}</p>
					        </li>
						    @else
						     <li class="passed" ><span>完成定金支付</span>
						      <p>{{{App\Common\Utils::formatDate($project->pay_time)}}}</p>
						     </li>					        
						     <li class="focus"><span>即将支付尾款</span>
						     <p>{{{App\Common\Utils::getRemainTime($winner->accept_time + ($winner->final_payment_pay_time)*86400)}}}</p>
						     </li>
					    @endif						
				   @else
						   @if($winner)
						   <li class="passed" ><span>选定投资者</span>
						       <p>{{{App\Common\Utils::formatDate($winner->accept_time)}}}</p>
						   </li>
						   @endif

						    @if($winner->type == App\Modules\ProjectModule::INVEST_TYPE_TOTAL)
					        <li class="passed" ><span>完成全款支付</span>
					            <p>{{{App\Common\Utils::formatDate($project->pay_time)}}}</p>
					        </li>
					        @else
					        <li class="passed" ><span>完成定金支付</span>
					            <p>{{{App\Common\Utils::formatDate($project->pay_time)}}}</p>
					        </li>
						    <li class="passed" ><span>完成尾款支付</span>
						        <p>{{{App\Common\Utils::formatDate($project->pay_final_time)}}}</p>
						    </li>
						    @endif
				       @if($project->status == App\Modules\ProjectModule::STATUS_PAYED)
						    <li class="focus"><span>项目即将完工</span>
						    @if($winner->require_complete_time)
						    <?php 
						    if($winner->type == App\Modules\ProjectModule::INVEST_TYPE_STAGES) {
                                $winnerRequireCompleteTime = $winner->accept_time + ($winner->final_payment_pay_time + $winner->require_complete_time)*86400;
                             }else{
                                $winnerRequireCompleteTime = $winner->accept_time + (App\Modules\ProjectModule::PAY_LIMIT_TIME + $winner->require_complete_time)*86400;
                             }
						    ?>
						        <p>{{{App\Common\Utils::getRemainTime($winnerRequireCompleteTime)}}}</p>
						     @else
						     <p>不做要求</p>
						     @endif
						    </li>
					    @elseif($project->status == App\Modules\ProjectModule::STATUS_END_SUCCESS)
						    <li class="passed"><span>项目完工</span>
						     <p>{{{App\Common\Utils::formatDate($project->complete_time)}}}</p>
						    </li>
					    @endif
			 @endif
					</ul>
				</div>
			</div>
		</div>
		<div class="pro pro-last">

			@include('common.contest-guess',array('endtime'=>$project->investment_end_time))
		</div>
	</div>
</div>
<input type="hidden" class="Js_contest_id" data-target-type="{{{$project->type}}}" data-target-id="{{{$project->id}}}">
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
@stop
