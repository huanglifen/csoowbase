<!--洽谈的投资者-->
<div class="height_10"></div>
@if(count($invests))
<ul class="investor_wrapper">
<?php $investCount = count($invests);?>
@foreach($invests as $key => $invest)
	@if(!empty($invest->user))
	<li>
		<div class="list-item">
			<a href="#"><img class="avatar" src="{{{App\Common\Utils::getAvatar($invest->user->avatar)}}}"
				alt=""></a>
			<div class="item-t">
				<span class="t"><a href="{{{$baseURL}}}/user/home/{{{$invest->user->id}}}">{{{$invest->user->name}}}</a></span>
				<div class="level">
					<div class="icon {{{App\Common\Utils::getLevelStyle($invest->user->creative_index)}}}"></div>
					<div class="bar">
						<div class="process" style="width: 60%;"></div>
					</div>
				</div>
				@if($invest->user->group_id == App\Modules\UserModule::GROUP_EXPERT)
			<?php $expertTitle = App\Common\Utils::getExpertTitle($invest->user->expert_organization_id);
                  $expertStyle = App\Common\Utils::getExpertStyle($invest->user->expert_organization_id);
            ?>
			<span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
				@endif
				<div class="fr">
					<p class="date">提交投资申请时间：{{{App\Common\Utils::formatDate($invest->create_time)}}}</p>
					
					<p class="date">要求项目完工时间：
					@if($invest->require_complete_time)
					@if(!empty($winner) && $winner->user_id == $invest->user_id)
					<?php 
                     if($invest->type == App\Modules\ProjectModule::INVEST_TYPE_STAGES) {
                      $requireCompleteTime = $invest->accept_time + ($invest->final_payment_pay_time + $invest->require_complete_time) * 86400;
                      }else{
                      $requireCompleteTime = $invest->accept_time + (3 + $invest->require_complete_time) * 86400;
                      }
                      ?>
					{{{App\Common\Utils::formatDate($requireCompleteTime)}}}
					@else
					款项支付后{{{$invest->require_complete_time}}}天内
					@endif
					@else
					不作要求
					@endif
					</p>
				    
				</div>
			</div>
			@if(!$user || $invest->user_id != $user->id)
			<div class="item-con">
				<div class="clearfix mutual-btn">
					<?php 
				      if(isset($invest->user->is_follow)&& $invest->user->is_follow) {
				        $class = "Js_has_followed_btn";
				        $followname = '已关注';
			           }else{
                            $class = "Js_follow_btn";
                            $followname = '关注';
                       }
				     ?>
					<a class="{{{$class}}}" href="javascript:;" user-id="{{{$invest->user_id}}}">{{{$followname}}}</a> 
					<a href="{{{$baseURL}}}/chat/contact/{{{$invest->user_id}}}" target="_blank">聊天</a>
					
				</div>
			</div>
			@endif
			
			@if($isPublisher || ($user && $invest->user->id == $user->id))
			<div class="height_10"></div>
			<div class="detail">
				<div class="investment_style clearfix">
				@if($invest->type == App\Modules\ProjectModule::INVEST_TYPE_STAGES)
					<p class="fl">投资方式：</p>
					<div class="detail_content fl">
						<p class="first_line">先付定金再付尾款</p>
						<p>
							定金支付比例：{{{$invest->deposit_percentage}}}%<i>金额</i><em>{{{ceil(($invest->deposit_percentage*$invest->amount)/100)}}}</em>
						</p>
						<p>投资申请被接受后3天内完成支付</p>
						<p>
							尾款支付比例：{{{100 - $invest->deposit_percentage}}}%<i>金额</i><em>{{{floor(((100-$invest->deposit_percentage)*$invest->amount)/100)}}}</em>
						</p>
						<p class="last_line">投资申请被接受后{{{$invest->final_payment_pay_time}}}天内完成支付</p>
					</div>
				@else
					<p class="fl">投资方式：</p>
					<div class="detail_content fl">
						<p class="first_line">一次性付款</p>
						<p>
						支付比例：100%<i>金额</i><em>{{{$invest->amount}}}</em>
						</p>
						<p>投资申请被接受后3天内完成支付</p>
					</div>
				@endif
				</div>
				<div class="investment_style requirement">
					<p class="fl">投资要求：</p>
					<div class="detail_content fl">
						<div class="detail_box" style="height:25px; overflow:hidden">{{$invest->requirement}}</div><a class="Js_detail_btn" href="javascript:">[详细]</a>
					</div>
				</div>
				<div class="btm_btn">
				@if($isPublisher && $project->status == App\Modules\ProjectModule::STATUS_GOING)								
				    <a href="{{{$baseURL}}}/project/accept-invest/{{{$invest->id}}}">接受投资</a>								
				@elseif($invest->status != App\Modules\ProjectModule::INVEST_STATUS_ACCEPT)
					<a href="{{{$baseURL}}}/project/cancel-invest/{{{$invest->id}}}">撤回申请</a> <a href="{{{$baseURL}}}/project/apply/{{{$invest->project_id}}}/{{{$invest->id}}}">修改申请</a>
				@endif
				</div>
			</div>
			@endif
			@if($key < $investCount - 1)
			<div class="dotline"></div>
			@endif
		</div>
	</li>
	@endif
@endforeach
</ul>
@else
  <div class="no_content">
	<div class="tip">暂无</div>
</div>
@endif
<div class="des-line"></div>


<!-- 项目交流-->
@include('common.comments')
