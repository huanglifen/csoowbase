<div class="discuss-container">
	<a class="close Js_reviewClose_btn" href="javascript:"></a>
	<div class="dis-sendbox clear">
		<textarea class="inputbox Js_trend_comment" rows="10" cols="30"></textarea>
		<div class="dis-bar">
			<div class="dis-retweet fl">
				<input id="retweet" type="checkbox" checked="checked"> <label
					for="retweet">同时转发到我的动态</label>
			</div>
			<a class="dis-subbtn fr Js_release_review" href="javascript:">发表评论</a>
		</div>
	</div>
		<ul class="dis-list Js_user_comments">
	@if(count($comments))
	 @include('common.m-dynamic-comment')
	@else 
	<li>
	<p class="no_comments Js_no_comments">暂无评论！你的评论，是对我最大的支持！</p>
	</li>
	@endif
	</ul>
</div>
