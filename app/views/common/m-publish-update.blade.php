<div class="message_box Js_update_box" style="width: 710px; display:none">
    <div class="box_tittle">
        <span class="t fl">发布更新</span>
        <a class="close fr Js_cancel_update" href="javascript:" title="关闭"></a>
    </div>
    <div class="box_content">
        <ul class="publish_update">
            <li class="update_tittle">
                <div class="row" id="title">
                    <label class="text_tittle" for="update_tittle_content">标题：</label>
                    <input type="text" id="update_tittle_content" placeholder="请输入更新标题"/>
                    <span class="Validform_checktip Validform_wrong" style="display:none; margin-left:40px;"></span>
                </div>
            </li>
            <li class="publish_update_content" id="content">
                <div class="row">
                    <label class="text_tittle fl" >内容：</label>
                    <div class="fl">
                        <script type="text/plain" id="matchDetail" style="width:620px;height:300px; max-height:300px"></script>
                    </div>
                    <span class="Validform_checktip Validform_wrong" style="display:none; margin-left:40px;"></span>
                </div>
            </li>
        </ul>
        <div class="box_btm_btn clearfix">
            <a class="submit fl Js_update_btn" href="javascript:">提交更新</a>
            <a class="cancel fl Js_cancel_update" href="javascript:">取消</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/editor/umeditor.config.js" charset="utf-8"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/editor/_examples/editor_api.js" charset="utf-8"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/editor/lang/zh-cn/zh-cn.js"></script>