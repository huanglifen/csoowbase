@if(! isset($applyName))
    <?php $applyName = '我要投资';?>
@endif
@if(! isset($link_string))
    <?php $linkString = 'project';?>
@endif
@if(! isset($canApply))
    <?php $canApply = true;?>
@endif
@if(! isset($statusContent))
    <?php $statusContent = '项目正在进行中'?>
@endif
@if(! isset($showAgreement))
   <?php $showAgreement = false?>
@endif
<div class="detail-top w1180">

		@include('common.like-report')
		
		<h1 class="pro-title">{{{$contest->title}}}</h1>
		<p class="pro-des">
			<span class="">{{{$statusContent}}}</span><span class="m1">编号：{{{$contest->no}}}</span>
		</p>

		@if($contest->user)
		<div class="pro-user">
			<div class="avatar-wrap">
				<a href="{{$baseURL}}/user/home/{{$contest->user->id}}"><img class="avatar"
					src="{{{App\Common\Utils::getAvatar($contest->user->avatar)}}}" alt=""></a>
			</div>
			<div class="user-info">
				<div class="low-name">
					<a class="uname" href="{{$baseURL}}/user/home/{{$contest->user->id}}">{{{$contest->user->name}}}</a>
					<div class="level">
						<div class="icon {{{App\Common\Utils::getLevelStyle($contest->user->creative_index)}}}"></div>
						<div class="bar">
							<div class="process" style="width: 60%;"></div>
						</div>
					</div>
				@if($contest->user->group_id == App\Modules\UserModule::GROUP_EXPERT)
				<?php $expertTitle = App\Common\Utils::getExpertTitle($contest->user->expert_organization_id);
                  $expertStyle = App\Common\Utils::getExpertStyle($contest->user->expert_organization_id);
                ?>
			     <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			     @endif
				</div>
				<p class="low-about">
				<?php
					$contest->user->location = $contest->user->province_name . $contest->user->city_name . $contest->user->district_name;
					$separator = '';
					if ($contest->user->location && $contest->user->school_name) {
						$separator = '|';
					}
					$delimiter = '';
					if ($contest->user->school_name && $contest->user->organization_name) {
						$delimiter = '|';
					}
				?>
					<span>{{{$contest->user->location}}}</span> {{{$separator}}} <span>{{{$contest->user->school_name}}}</span> {{{$delimiter}}} <span>{{{$contest->user->organization_name}}}</span>
				</p>
				<p class="low-btn">
				@if($showAgreement)
				<a class="g" href="{{{$baseURL}}}/project/agreement/{{{$contest->id}}}">查看协议</a>
				@elseif(!$user || $contest->user_id != $user->id)
					<?php 
				     if(isset($project->user->is_follow) && $project->user->is_follow) {
                            $followName = '已关注';
                            $class = "g Js_has_followed_btn";
                     }else{
                            $followName = '关注';
                            $class = "g Js_follow_btn";
                     }
				    ?>
					<a class="{{{$class}}}" href="javascript:" user-id = "{{{$contest->user_id}}}">{{{$followName}}}</a>
					<a class="g" href="{{{$baseURL}}}/chat/contact/{{{$contest->user_id}}}" target="_blank">聊天</a>
			    @endif
				</p>
			</div>
		</div>
		@endif

		@if($canApply)
		<div class="pro-btn">
			<a class="joinbtn"
				href="{{{$baseURL}}}/{{{$linkString}}}/apply/{{{$contest->id}}}">{{{$applyName}}}</a>
		</div>
	    @endif
        <div class="pro-breadcrumb">当前位置：<a href="{{{$baseURL}}}/project/index">创意投资项目比赛</a>&nbsp;/&nbsp;比赛详情</div>
</div>