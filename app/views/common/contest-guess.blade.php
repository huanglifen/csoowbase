<div class="pro-content">
	<form class="Js_guess_form" action="#" data-target-type="{{$targetType}}" data-target-id="{{$targetId}}">
		<strong>比赛竞猜</strong> <b>这个比赛是否能够成功？</b>
		<p class="pro-time">截止时间：
		@if(isset($project))
		{{\App\Common\Utils::getGuessEndtime($project->investment_end_time, $project->status)}}
		@elseif (isset($task))
		{{\App\Common\Utils::getGuessEndtime($task->end_time, $task->status)}}
		@elseif (isset($hiring))
		{{\App\Common\Utils::getGuessEndtime($hiring->end_time, $hiring->status)}}
		@endif
		</p>
		<div class="pro-gress">
			<span><input id="r1" type="radio" name="item" checked="checked" value="1"><label for="r1">成功</label></span> 
			<span><input id="r2" type="radio" name="item" value="0"><label for="r2">不成功</label></span>
		</div>
		<div class="pro-gressEnterMoney clearfix">
			<input type="text" class="Js_coininput" name="amount" placeholder="投入" /><span>枚创创币</span>
		</div>
		<span class="pro-reward">预计奖励<b class="Js_ccb_coin">0</b>枚创创币
		</span>
		<div class="clearfix mutual-btn">
			<a href="javascript:" class="Js_guess_sub">确认</a>
		</div>
	</form>
</div>
