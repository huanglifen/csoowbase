<?php 
switch ($targetType){
	case 1: $title = '项目交流区';break;
	case 2: $title = '任务交流区';break;
	case 21: $title = '作品交流区';break;
	case 3: $title = '比赛交流区';break;
	case 31: $title = '作品交流区';break;
	case 4: $title = '交易交流区';break;
	case 5: $title = '创意交流区';break;
	default:$title='项目交流区';
}
?>

<div class="discuss-wrap" id="comments" name="comments">
	<div class="dis-top clearfix">
		<span class="t fl">{{{$title}}}</span><span class="i fr">@if(!empty($commentNum)) {{{$commentNum}}}条评论 @endif</span>
	</div>
    <div class="height_10"></div>
	<div class="dis-sendbox clear">
		<textarea class="inputbox Js_inputbox Js_inputbox_comment" disabled="disabled" name="" id="disinputbox" cols="30" rows="10"></textarea>
		<div class="dis-bar">
			<a class="dis-subbtn fr Js_dis_btn" href="javascript:" data-target-type="{{$targetType}}" data-target-id="{{$targetId}}">发表评论</a>
		</div>
	</div>
<ul class="dis-list Js_dis_list">
@include('common.m-comments')
</ul>
@if(!empty($pageHtml))
{{$pageHtml}}
@endif
</div>