@extends('common.main') 
@section('content')
<div class="content w1180 clearfix">
	<div class="height_30"></div>
	<div class="invite-search-content">
		<div class="match-nav clearfix">
			<ul class="navlist w1180 clearfix fl">
				<li class="@if($type==0)current@endif"><a class="c" href="{{$baseURL}}/{{$urlName}}/invite-expert/0/{{$targetType}}/{{$targetId}}/{{$targetSubId}}">搜索站内专家</a></li>
				<li class="@if($type==1)current@endif"><a class="c" href="{{$baseURL}}/{{$urlName}}/invite-expert/1/{{$targetType}}/{{$targetId}}/{{$targetSubId}}">推荐站外专家</a></li>
			</ul>
		</div>
		<div class="height_40"></div>
@if($type == 0)
		<!--搜索站内专家-->
		<div class="in_site Js_invite_search" style="display: block" data-target-sub-id="{{$targetSubId}}" data-target-type="{{$targetType}}" data-target-id="{{$targetId}}">
			<div class="search_bar">
				<input type="text" class="Js_invite_input" id="invite_expert" placeholder="请输入您要搜索专家的名字"/>
				<label for="invite_expert">
					<button class="btn-search" type="submit">搜索</button>
				</label>
				<div class="search_result" style="display: none">
					<ul class="result Js_invite_list">
						<!-- 搜索出来的专家 列表-->
					</ul>
				</div>
			</div>
			<div class="height_20"></div>
			<!--准备邀请的专家-->
			<div class="prepared" style="display: block">
				<div class="cwindex-title" style="display: none">
					<p class="line"></p>
					<p class="ctitle">
						<span style="font-weight: normal">准备邀请的专家</span>
					</p>
				</div>
				<div class="list_main">
					<ul class="clearfix Js_exp_list">
						<!-- 准备邀请的专家列表 -->
					</ul>
				</div>
				<div class="loadpage">
					<a class="Js_inivert_confirm" href="javascript:" style="display: none">
                        <button type="button" class="btn DeepBlue">确认邀请</button>
					</a>
				</div>
			</div>
			<div class="height_60"></div>
		</div>
@else
	<!--推荐站外专家-->
		<div class="off_site" style="display: block">
		<form id="out_invite_form" action="{{$baseURL}}/{{$urlName}}/invite-external-expert" method="post">
			<div class="add_recd_experts">
				<ul>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;">推荐专家姓名：</div>
							<div class="rc">
								<div class="con">
									<input style="position: relative" type="text" placeholder="" name="name" datatype="s" errormsg="" nullmsg="请填写专家姓名!">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;">推荐专家行业：</div>
							<div class="rc">
								<div class="con">
									<input style="position: relative" type="text" placeholder="" name="industry" datatype="s" errormsg="" nullmsg="请填写专家行业!"> 
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;">邮箱：</div>
							<div class="rc">
								<div class="con">
									<input style="position: relative" type="text" placeholder="" name="email" datatype="e" errormsg="" nullmsg="请填写邮箱!">
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;">手机：</div>
							<div class="rc">
								<div class="con">
									<input style="position: relative" type="text" placeholder="" name="mobile_phone_no" datatype="m" errormsg="请填写11位的手机号码！" nullmsg="请填写11位的手机号码！">
									<input type="hidden" name="target_type" value="{{$targetType}}" />
									<input type="hidden" name="target_id" value="{{$targetId}}" />
									<input type="hidden" name="target_sub_id" value="{{$targetSubId}}" />
									<input type="hidden" name="lastPage" value="{{$lastPage}}" />
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class="rt" style="width: 160px;"></div>
							<div class="rc">
								<div class="con">
									<div class="upload-files Js_cover">
										<div class="uploadWrap-single dargArea left">
											<a class="Js_uploadbox" style="vertical-align: middle">
												<button type="submit" class="btn LightGrey Js_out_invite"><span class="inner">添加</span></button>
											</a>
										</div>
										<div class="height_10"></div>
										<div class="clearfix upload-single"></div>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			</form>
			<div class="height_20"></div>
		</div>
@endif	
	
	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/invite.js"></script>
@stop
