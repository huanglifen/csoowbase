@if (! isset($page))
    <?php $page = 1?>
@endif

@if(count($lists) || $page > 1)

@include('common.m-contest')

<input type="hidden" value="{{{$page}}}" class="Js_list_page_value" />

@if (!isset($tagId))
    <?php $tagId = 0;?>
@endif
<input type="hidden" value="{{{$tagId}}}" class="Js_list_tag_id" />


@if (! isset($status))
    <?php $status = 0;?>
@endif
<input type="hidden" value="{{{$status}}}" class="Js_list_status_value" />

@else
<div style="margin-right:20px">
<div class="no_content">
	<div class="tip">暂无</div>
</div>
</div>
@endif
