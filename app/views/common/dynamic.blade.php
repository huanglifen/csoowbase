@if( ! isset($showOperate))
 <?php  $showOperate = true;?>
 @endif
@if(! isset($owner))
   <?php $owner = $user; $isMe = true;?>
@endif
 @foreach($datas as $dynamic)
 <?php $forwardId = $dynamic->id;?>
<li target-id="{{{$dynamic->id}}}">@if($dynamic->target_type == 0)
	<div class="list-item">
		<a href="javascript:;" @if( ! $user || $user->id != $dynamic->user->id)class="Js_show_user_info_card" @endif data-user-id="{{{$dynamic->user->id}}}">
		<img class="avatar" src="{{{App\Common\Utils::getAvatar($dynamic->user->avatar)}}}" alt="{{{$dynamic->user->name}}}"></a>
		<div class="item-t">
			<span class="t"><a href="javascript:;" @if( ! $user || $user->id != $dynamic->user->id)class="Js_show_user_info_card" @endif data-user-id="{{{$dynamic->user->id}}}">{{{$dynamic->user->name}}}</a></span>
			<div class="level">
				<div class="icon {{{App\Common\Utils::getLevelStyle($dynamic->user->creative_index)}}}"></div>
				<div class="bar">
					<div class="process" style="width: 60%;"></div>
				</div>
			</div>
			@if($dynamic->user->group_id == App\Modules\UserModule::GROUP_EXPERT)
			<?php $expertTitle = App\Common\Utils::getExpertTitle($dynamic->user->expert_organization_id);
                  $expertStyle = App\Common\Utils::getExpertStyle($dynamic->user->expert_organization_id);
            ?>
			<span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			@endif
		</div>

		<div class="item-con">{{{$dynamic->content}}}</div>
	@if($dynamic->pic)
		<div class="f-image">
			<img class="artZoom" src="{{{$baseURL}}}/{{{$dynamic->pic}}}_s.jpg" data-artZoom-show="{{{$baseURL}}}/{{{$dynamic->pic}}}" data-artZoom-source="{{{$baseURL}}}/{{{$dynamic->pic}}}" />
		</div>
	@endif 
    @if($dynamic->video)
		<div class="f-video">{{$dynamic->video}}</div>
	@endif
 @if($dynamic->type == App\Modules\DynamicModule::TYPE_FORWARD && isset($dynamic->forward))
		<?php $forwardId = $dynamic->forward->id;?>
		<div class="discuss-wrap">
			<div class="discuss-container dis-zf">
				<ul class="dis-list">
					<li {{{$dynamic->forward->id}}}>
						<div class="list-item">
							<div class="item-t">
								<a href="javascript:;" @if( ! $user || $user->id != $dynamic->forward->user->id)class="Js_show_user_info_card"@endif data-user-id="{{{$dynamic->forward->user->id}}}"><img class="avatar"
									src="{{{App\Common\Utils::getAvatar($dynamic->forward->user->avatar)}}}"
									alt=""></a> <span class="t"><a href="javascript:;" @if( ! $user || $user->id != $dynamic->forward->user->id)class="Js_show_user_info_card" @endif data-user-id="{{{$dynamic->forward->user->id}}}">{{{$dynamic->forward->user->name}}}</a></span>
								<div class="level">
									<div class="icon {{{App\Common\Utils::getLevelStyle($dynamic->forward->user->creative_index)}}}"></div>
									<div class="bar">
										<div class="process" style="width: 60%;"></div>
									</div>
								</div>
								@if($dynamic->forward->user->group_id == App\Modules\UserModule::GROUP_EXPERT)
		                         <?php $expertTitle = App\Common\Utils::getExpertTitle($dynamic->forward->user->expert_organization_id);
                                       $expertStyle = App\Common\Utils::getExpertStyle($dynamic->forward->user->expert_organization_id);
                                   ?>
			                        <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			                    @endif
							</div>
							<div class="item-con">{{{$dynamic->forward->content}}}</div>
						@if($dynamic->forward->pic)
		                    <div class="f-image">
								<img class="artZoom" src="{{{$baseURL}}}/{{{$dynamic->forward->pic}}}_s.jpg" data-artZoom-show="{{{$baseURL}}}/{{{$dynamic->forward->pic}}}" data-artZoom-source="{{{$baseURL}}}/{{{$dynamic->forward->pic}}}" />
							</div>
	                    @endif 
	                    
                        @if($dynamic->forward->video)
		                    <div class="f-video">{{$dynamic->forward->video}}</div>
	                    @endif
							<span class="date">{{{App\Common\Utils::formatTime($dynamic->forward->create_time)}}}</span>
						</div>
					</li>
				</ul>
			</div>
		</div>
	@endif


		<div class="clearfix">
			<span class="date fl">{{{App\Common\Utils::formatTime($dynamic->create_time)}}}</span>
			@if($showOperate)
			<div class="operate-list fr">
				<?php
					if($dynamic->content){
						$bdText = $dynamic->content;
					}elseif($dynamic->pic){
						$bdText = $baseURL.'/'.$dynamic->pic;
					}elseif($dynamic->video){
						$bdText = $dynamic->video;
					}else{
						$bdText = '创意世界';
					}
				?>
				<div id="bdshare" class="bdshare_t bds_tools get-codes-bdshare" data="{'text':'{{$bdText}}','url':'{{$baseURL}}/user/home/{{$owner->id}}',@if($dynamic->pic)'pic':'{{$baseURL}}/{{$dynamic->pic}}'@endif}">
					<a class="bds_qzone"></a>
					<a class="bds_tsina"></a>
					<a class="bds_tqq"></a>
					<a class="bds_renren"></a>
					<span class="bds_more"></span>
				</div>
			 <a href="javascript:;" title="赞一个" class="oper-zan @if(!$dynamic->like)Js_commentZan @endif" @if($dynamic->like)style="background-position: 0px -66px;" @endif>&nbsp;(<span class="Js_commentZanNum">{{{$dynamic->like_count}}}</span>)</a> |
				<a href="javascript:;" class = "Js_forward" data-forward-id = {{{$forwardId}}}>转发</a> | 
				<a href="javascript:;" class="Js_circle_review">评论&nbsp;(<span class="Js_commentNuum">{{{$dynamic->comment_count}}}</span>)</a>
			</div>
			@endif
		</div>
	
	<div class="discuss-wrap Js_dynamic_comment_content">
	</div>
	</div> 

@else
	<div class="list-item l-padding">
		<div class="item-t">
			<span class="t"><a href="#">{{{$dynamic->user->name}}}</a></span>
			<div class="level">
				<div class="icon {{{App\Common\Utils::getLevelStyle($dynamic->user->creative_index)}}}"></div>
				<div class="bar">
					<div style="width: 60%;" class="process"></div>
				</div>
			</div>
		</div>
		<div class="review">{{$dynamic->content}}</div>
		<span class="date">{{{App\Common\Utils::formatTime($dynamic->create_time)}}}</span>
	</div> @endif
</li>
@endforeach
@if(!empty($totalPage) && $totalPage > 1)
{{$pageHtml}}
@endif

@if(!isset($isAddBdShareJs)|| $isAddBdShareJs)
<script type="text/javascript" id="bdshare_js" data="type=tools" ></script>
<script type="text/javascript" id="bdshell_js"></script>
<script type="text/javascript">
	document.getElementById("bdshell_js").src = "http://bdimg.share.baidu.com/static/js/shell_v2.js?cdnversion=" + Math.ceil(new Date()/3600000);
</script>
@endif
