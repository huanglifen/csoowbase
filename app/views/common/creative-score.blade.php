<div class="cynum">
	<div class="r1 clearfix">
		<p class="fl">
			<b class="t">创意价值投票</b><span class="des">既有创意又有实际价值，才称得上具有创意价值</span>
		</p>
		<p class="fr num">
			总创意指数：<span class="x">{{{$creative_index}}}</span>
		</p>
	</div>
	<div class="r2 clearfix">
		<div class="fl">
			@if(!empty($creativeValueRating))
			<div class="Js_common_rateit rateit fl z bigstars" data-rateit-value="{{$creativeValueRating->score}}" data-rateit-starwidth="40" data-rateit-starheight="32" data-rateit-resetable="false" data-rateit-readonly="true"></div>
			<span class="common_text_selected" style="display: inline-block; margin-top: 7px">{{{App\Common\Utils::getCreativeValueRatingScoreText($creativeValueRating->score)}}}</span>
			@else
			<div class="Js_common_rateit rateit fl z bigstars" data-rateit-value="0" data-rateit-starwidth="40" data-rateit-starheight="32" data-rateit-resetable="false" data-rateit-step="1" data-target-type="{{$targetType}}" data-target-id="@if(!empty($targetSubId)){{$targetSubId}}@else{{$targetId}}@endif" ></div>
			@endif
			<span class="common_text_selected Js_cts"></span> 
			<span class="common_text Js_ct"></span>
		</div>
		@if(empty($creativeValueRating))<p class="fr">或<a class="rbtn Js_showdis" href="#disinputbox">提改进建议</a></p>@endif
	</div>
</div>
