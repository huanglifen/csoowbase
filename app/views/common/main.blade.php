<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
<title>
@section('title') 
创意世界 
@show
</title>
	<link rel="stylesheet" type="text/css" href="{{{$baseURL}}}/css/base_reset.css">
    <link rel="stylesheet" type="text/css" href="{{{$baseURL}}}/css/widgets.css">
    <link rel="stylesheet" type="text/css" href="{{{$baseURL}}}/css/edui-editor/css/umeditor.css">
    <link rel="stylesheet" type="text/css" href="{{{$baseURL}}}/css/combine.css">
    <link rel="stylesheet" type="text/css" href="{{{$baseURL}}}/css/index.css">
	<script>
		var baseUrl = '<?php echo $baseURL.'/';?>';
		var globalLastPage = '{{{$lastPage}}}';
		var globalCurrentPage = '{{{$currentPage}}}';
		var moreUploadPath = '';
	    var globalUserInfo = {id:0,name:'',avatar:''};
	    <?php  if($user){?>
	    globalUserInfo.id = '<?php echo $user->id;?>';
	    globalUserInfo.name = '<?php echo $user->name;?>';
	    globalUserInfo.avatar = '<?php $user->avatar;?>';
	    <?php }?>
	</script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/common.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.fancybox.min.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/header.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.html5uploader.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.checktextnumber.js"></script>
</head>
<body>
<div id="wrapper">
<div class="header-wrap">
<div class="header">
<div class="logo">
	<a href="{{{$baseURL}}}">创意世界</a>
</div>
<div class="head-search">
@if($user)
	<div class="head-ccz fl Js_user">
		<div class="avatar_wraper">
			<img class="ava_img fl" src="{{{App\Common\Utils::getAvatar($user->avatar)}}}"/>
			<div class="tips fl">
				<div class="tipone">
					<span class="name">{{$user->name}}</span>
					<div class="level">
						<div class="icon {{{App\Common\Utils::getLevelStyle($user->creative_index)}}}"></div>
						<div class="bar">
							<div class="process" style="width: 22px;"></div>
						</div>
					</div>
				</div>
				<div class="tiptwo">
					<img class="fl tip_icon" src="{{{$baseURL}}}/images/icon_placeholder_14x14.png"/>
					<span class="ccznum fl">{{$user->cc_no}}</span>
				</div>
			</div>
			<div class="drop fr">
				<div class="drop_icon"></div>
			</div>
		</div>
		<div id="Js_login_info" class="login-info">
			<div class="Login">
				<div class="user_data">
					<dl class="clearfix">
						<dt>创意指数</dt>
						<dd><a href="{{$baseURL}}/account/creative-index">{{$user->creative_index}}</a>点</dd>
					</dl>
					<dl class="clearfix">
						<dt>创创币</dt>
						<dd><a href="{{$baseURL}}/account/coin">{{$user->coin}}</a>枚</dd>
					</dl>
				</div>
				<div class="user_btn_logout clearfix">
					<div class="u-btn clearfix">
						<a href="{{{$baseURL}}}/dynamic/index" class="FriendCircle">朋友圈</a>
						<a href="{{{$baseURL}}}/user/home" class="UserHomePage">个人主页</a>
						<a href="{{{$baseURL}}}/account/user-info" class="Settings">设置</a>
					</div>
					<a class="logout" href="{{{$baseURL}}}/user/logout">退出</a>
				</div>
			</div>
		</div>
	</div>
@else
	<div class="head-ccz fl Js_user">
		<div class="avatar_wraper">
			<img class="ava_img fl" src="{{{$baseURL}}}/images/avatar_default.png"/>
			<div class="tips fl">
				<div class="tipone">
					<span class="name logout">游客</span>

					<div class="level">
					</div>
				</div>
				<div class="tiptwo">
				</div>
			</div>
			<div class="drop fr">
				<div class="drop_icon"></div>
			</div>
		</div>

		<div id="Js_login_info" class="login-info">
			<div class="notLogin">
				<div class="user_data">
					<dl class="clearfix">
						<dt>创意指数</dt>
						<dd><a href="">0</a>点</dd>
					</dl>
					<dl class="clearfix">
						<dt>创创币</dt>
						<dd><a href="">0</a>枚</dd>
					</dl>
				</div>
				<div class="login_logout">
					<a href="{{{$baseURL}}}/user/login" class="login">登录</a>
					<a href="{{{$baseURL}}}/user/register" class="logout">注册</a>
				</div>
			</div>
		</div>
	</div>
@endif
	<div class="head-search-box">
		<input class="head-search-text Js_search" type="text" placeholder="所有我要，创创即来" autocomplete="off">

		<div class="cczprompt_wraper Js_cczprompt">
			<div class="tc tip">告诉创创你需要什么?</div>
			<div class="clearfix cczprompt_li Js_cczprompt_li" style="">
				<a class="tc linefirst Js_tcs_list" href="javascript:;" data-href="{{$baseURL}}/exchange/publish">出售创意</a>
				<a class="tc Js_tcs_list" href="javascript:;" data-href="{{$baseURL}}/task/publish">找人帮忙</a>
				<a class="tc Js_tcs_list" href="javascript:;" data-href="{{$baseURL}}/hiring/publish">寻找明星</a>
				<a class="tc Js_tcs_list" href="javascript:;" data-href="{{$baseURL}}/project/publish">寻找资金</a>
				<a class="tc linefirst Js_tcs_list" href="javascript:;" data-href="{{$baseURL}}/encyclopedia/index">寻找创意</a>
				<a class="tc Js_tcs_list" href="javascript:;" data-href="{{$baseURL}}/task/index">出售技能</a>
				<a class="tc Js_tcs_list" href="javascript:;" data-href="{{$baseURL}}/hiring/index">成为明星</a>
				<a class="tc Js_tcs_list" href="javascript:;" data-href="{{$baseURL}}/project/index">寻找项目</a>
			</div>
			<div class="cczonce_wraper Js_cczonce">
				<a class="tc cczonce @if(!empty($user) && empty($user->ccz_used)) Js_use_ccz @endif">创一下</a>
			</div>
		</div>
		<div class="search_result Js_searchResult">
			<div class="Js_searchResult_con">
			<!--这里是查询结果：search.list -->
			</div>
		</div>
	</div>
</div>
<div class="head-friends">
	<div class="FriendsAlerts Js_friendalert">
		<a class="friends" href="{{$baseURL}}/chat" title="朋友圈消息">@if($user) <i class="Js_chatNum" @if($chatNum==0) style="display:none;" @endif>{{$chatNum}}</i> @endif</a>
		<a class="alerts" href="{{$baseURL}}/notification/index/0" title="系统消息">@if($user) <i class="Js_notificationNum" @if($notificationNum==0) style="display:none;" @endif>{{$notificationNum}}</i> @endif</a>
	</div>
	<div class="friendlist_wraper Js_friendlist">
		<!-- 创创召下的5条最新的聊天消息 -->
	</div>

	<div class="infolist_wraper Js_msglist">
		<!-- 创创召下的5条最新的系统消息 -->
	</div>
</div>

<span class="side ls"></span>
<span class="side rs"></span>
</div>
</div>

@section('content')
@show
<?php 
function curNaviPage($name, $curPageName) {
	if ( $curPageName == $name) {
		echo 'current';
	} 
}
?>

<div class="posttrend_btn @if($user)Js_posttrend_btn @endif">
	<a href="@if($user)javascript: @else {{{$baseURL}}}/user/login @endif">发布动态</a>
</div>

<div class="leftnav_wraper leftnav_small_wraper">
	<!-- <h3 class="nav_title Js_leftnav_large" style="display:none">创意世界大赛</h3> -->
	<div class="leftnav_icon"><a class="Js_leftnav_small" href="javascript:"></a></div>
	<ul>
		<!-- 当前状态在li上添加样式名current 添加好后删除此行-->
		<li class="<?php curNaviPage('home', $curPageName)?>" title="首页"><a class="nav_link icon_home" href="{{{$baseURL}}}"><span class="ico"></span>首页</a></li>
		<li class="devide <?php curNaviPage('encyclopedia', $curPageName)?>" title="创意世界百科"><a class="nav_link icon_wiki" href="{{{$baseURL}}}/encyclopedia/index"><span class="ico"></span>创意世界百科</a></li>
		<li class="devide" title="创意世界大赛"><p class="nav_link icon_match" href="javascript:"><span class="ico"></span>创意世界大赛</p></li>
		<li title="创意投资项目比赛" class="<?php curNaviPage('project', $curPageName)?>"><a class="nav_link nav_normal icon_invest" href="{{{$baseURL}}}/project/index"><span class="ico"></span>创意投资项目比赛</a></li>
		<li title="创意任务人才比赛" class="<?php curNaviPage('task', $curPageName)?>"><a class="nav_link nav_normal icon_talent" href="{{{$baseURL}}}/task/index"><span class="ico"></span>创意任务人才比赛</a></li>
		<li title="创意公益明星比赛" class="<?php curNaviPage('hiring', $curPageName)?>"><a class="nav_link nav_normal icon_star" href="{{{$baseURL}}}/hiring/index"><span class="ico"></span>创意公益明星比赛</a></li>
		<li class="devide <?php curNaviPage('exchange', $curPageName)?>" title="创意世界交易所"><a class="nav_link icon_buss" href="{{{$baseURL}}}/exchange/index"><span class="ico"></span>创意世界交易所</a></li>
		<li class="devide <?php curNaviPage('thinktank', $curPageName)?>" title="创意世界智库"><a class="nav_link icon_thk" href="{{{$baseURL}}}/thinktank/index"><span class="ico"></span>创意世界智库</a></li>
		<li class="devide <?php curNaviPage('area', $curPageName)?>" title="创意世界地区"><a class="nav_link icon_area" href="{{{$baseURL}}}/area/index"><span class="ico"></span>创意世界地区</a></li>
		
	</ul>
</div>

<div id="footerPusher"></div>
</div>


<div id="footer">
	<div class="footerContent">
		<div class="info">
			<p class="links">
				<a href="{{$baseURL}}/home/about">关于我们</a>
				<a href="{{$baseURL}}/home/help1">帮助中心</a>
				<a href="{{$baseURL}}/home/agreement1">网站协议</a>
				<a href="{{$baseURL}}/home/employ">诚聘英才</a>
				<a href="http://weibo.com/csoow/">官方微博</a>
			</p>

			<p class="copyright">Copyright © 2003-2014 All Rights Reserved <a href="http://www.miitbeian.gov.cn/">蜀ICP备12025189号-1</a>
			</p>
		</div>
		<div class="certificate clearfix">
			<a href="">
				<img width="36" height="36" src="{{{$baseURL}}}/images/icon_certificate/net_police.gif"/>
			</a>
			<a href="" class="kbq1"></a>
			<a href="" class="kbq2"></a>
			<a href="" class="kbq3"></a>
			<a href="" class="kbq4"></a>
			<a href="" class="kbq5"></a>
		</div>
		<div class="QR_code"></div>
	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/uploader_config_more.js"></script>
</body>
</html>