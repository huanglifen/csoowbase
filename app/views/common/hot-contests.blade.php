<div class="project_wraper">
	<div class="projecthead">
		<h3 class="projecttitle">热门比赛</h3>
	</div>
	<ul>
	@foreach($contests as $contest)
		<li class="projectlist">
			<a class="clearfix projectlink" href="{{{App\Common\Utils::getContestDetailUrl($contest->type,$contest->id)}}}">
				<img class="projectavatar" src="{{{$baseURL}}}/{{{$contest->cover}}}">
				<p class="tipone">{{{$contest->title}}}</p>
				<p class="tiptwo"><em>{{$contest->displayNum}}</em>人参与比赛</p>
			</a>
		</li>
	@endforeach
	</ul>
</div>
