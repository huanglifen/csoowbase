@foreach($comments as $comment)
@if(!empty($comment->user))
<li>
	<div class="list-item">
		<a href="{{{$baseURL}}}/user/home/{{{$comment->user->id}}}" @if( ! $user || $user->id != $comment->user->id)class="Js_show_user_info_card" @endif data-user-id="{{$comment->user->id}}">
		<img class="avatar" src="{{{App\Common\Utils::getAvatar($comment->user->avatar)}}}" alt=""></a>
		<div class="item-t">
			<span class="t"><b><a href="{{{$baseURL}}}/user/home/{{{$comment->user->id}}}" @if( ! $user || $user->id != $comment->user->id)class="Js_show_user_info_card" @endif data-user-id="{{$comment->user->id}}">{{{$comment->user->name}}}</a></b></span>
			<div class="level">
				<div class="icon {{{App\Common\Utils::getLevelStyle($comment->user->creative_index)}}}"></div>
				<div class="bar">
					<div class="process" style="width: 22px;"></div>
				</div>
			</div>
				@if($comment->user->group_id == App\Modules\UserModule::GROUP_EXPERT)
				<?php $expertTitle = App\Common\Utils::getExpertTitle($comment->user->expert_organization_id);
                       $expertStyle = App\Common\Utils::getExpertStyle($comment->user->expert_organization_id);
                 ?>
			      <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
				@endif
			<span class="date fr">{{{App\Common\Utils::formatDate($comment->create_time)}}}</span>
		</div>
		<div class="item-con">{{nl2br($comment->content)}} </div>
	</div>
</li>
@endif
@endforeach
