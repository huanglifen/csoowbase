@foreach($comments as $comment)
	<li>
		<div class="list-item">
			<div class="item-t">
				<a href="javascript:;" @if( ! $user || $user->id != $comment->user->id)class="Js_show_user_info_card" @endif data-user-id="{{{$comment->user->id}}}"><img class="avatar"
					src="{{{App\Common\Utils::getAvatar($comment->user->avatar)}}}" alt=""></a> <span
					class="t"><a href="javascript:;" @if( ! $user || $user->id != $comment->user->id)class="Js_show_user_info_card" @endif data-user-id="{{{$comment->user->id}}}">{{{$comment->user->name}}}</a></span>
				<div class="level">
					<div class="icon {{{App\Common\Utils::getLevelStyle($comment->user->creative_index)}}}"></div>
					<div class="bar">
						<div class="process" style="width: 60%;"></div>
					</div>
				</div>
				@if($comment->user->group_id == App\Modules\UserModule::GROUP_EXPERT)
				<?php $expertTitle = App\Common\Utils::getExpertTitle($comment->user->expert_organization_id);
                       $expertStyle = App\Common\Utils::getExpertStyle($comment->user->expert_organization_id);
                 ?>
			      <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
				@endif
			</div>
			<div class="item-con">{{{$comment->content}}}</div>
			<span class="date">{{{App\Common\Utils::formatTime($comment->create_time)}}}</span>
		</div>
	</li>
@endforeach

<div class="paging">
<?php 
	   $lastPage = $page - 1 > 0 ? $page-1:$page;
	   $nextPage = $page+1 < $totalPage ? $page+1:$totalPage;
	   $pageInfo = App\Common\Page::genePage($page, $totalPage);
	   $min = $pageInfo['min'];
	   $max = $pageInfo['max'];
?>	
<a class="PgUp @if($page > 1) Js_dynamci_comment_page @else disable @endif" href="javascript:;" data-page="{{{$lastPage}}}">上一页</a>
<span class="PgNum">	
    @for($key=$min;$key<=$max;$key++)	
    <?php
    $current = "";
    
    if ($key == $page) {
        $current = "current";
    }
     $class = $current." num Js_dynamci_comment_page";
    ?>			 
<a class="{{{$class}}}" href="javascript:;" data-page = "{{{$key}}}">{{{$key}}}</a>
    @endfor
</span> 
    <a class="PgDn @if($page < $totalPage) Js_dynamci_comment_page @else disable @endif" href="javascript:;" data-page = "{{{$nextPage}}}">下一页</a>
</div>
<div class="height_20"></div>