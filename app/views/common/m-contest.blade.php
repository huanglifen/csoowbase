
@if(!empty($type))
<?php $targetType = $type;?>
@else
<?php $targetType = 0;?>
@endif
@foreach ($lists as $list)
<?php  $targetType = !empty($list['type'])?$list['type']:$targetType; ?>
<li>
	<div class="list_block_container">
		<div class="list_block project">
			<div class="block_cover">
				<a class="cover"
					href="{{{App\Common\Utils::getContestDetailUrl($targetType,$list->id)}}}">
					<img class="" src="{{{$baseURL}}}/{{{$list->cover}}}"
					title="{{{$list->title}}}">
				</a>
			</div>
			<div class="block_content animate_border_color">
				<a class="headline animate_color"
					href="{{{App\Common\Utils::getContestDetailUrl($targetType,$list->id)}}}"
					title="{{{$list->title}}}">{{{$list->title}}}</a>
				<div class="tags">
					<?php $tags = json_decode($list->tags);?>
						@if(!empty($tags))
							@foreach($tags as $tag)
							<span>{{{$tag}}}</span> 
							@endforeach 
						@endif
				</div>
			</div>
			<div class="block_bottom animate_border_color clearfix">
				@if(!empty($list->user))
				<a href="{{$baseURL}}/user/home/{{$list->user->id}}"><img class="avatar" src="{{{App\Common\Utils::getAvatar($list->user->avatar)}}}" alt="{{{$list->user->name}}}" /></a>
				<a class="user animate_color" href="{{$baseURL}}/user/home/{{$list->user->id}}">{{{$list->user->name}}}</a>
				<span class="participants">{{{$list->creative_index}}}</span>
				@endif
			</div>
		</div>
	</div>
</li>
@endforeach
