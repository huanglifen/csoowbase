<div class="project_wraper ntop_border">
	<div class="projecthead">
		<h3 class="projecttitle">你可能感兴趣的人</h3>
	</div>
	<ul class="interest clearfix">
	@foreach($carepeople as $user)
		<li><a href="{{{$baseURL}}}/user/home/{{$user->id}}"><img src="{{{App\Common\Utils::getAvatar($user->avatar)}}}" title="{{{$user->name}}}"></a></li>
	@endforeach
	</ul>
</div>
