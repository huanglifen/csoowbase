 @foreach($expertComments as $key=>$expertComment)
 @if($targetType == 2)
 <?php 
 $showUserInfo = true;
 $showWorkLink = $baseURL.'/task/work/'.$expertComment->target_sub_id; ?>
 @elseif($targetType == 3)
 <?php 
  $showUserInfo = true;
  $showWorkLink = $baseURL.'/hiring/participator/'.$expertComment->target_sub_id; ?>
 @else
 <?php 
    $showUserInfo = false;
   $showWorkLink = ''; ?>
 @endif
 
 @if(!empty($expertComment->user))
<li>
	<div class="list-item">
		<a href="{{{$baseURL}}}/user/home/{{$expertComment->user->id}}" @if( ! $user || $user->id != $expertComment->user->id)class="Js_show_user_info_card"@endif data-user-id="{{{$expertComment->user->id}}}"><img class="avatar"
			src="{{{App\Common\Utils::getAvatar($expertComment->user->avatar)}}}"
			alt="">
		</a>
		<div class="item-t">
			<span class="t"><a href="{{{$baseURL}}}/user/home/{{$expertComment->user->id}}" @if( ! $user || $user->id != $expertComment->user->id)class="Js_show_user_info_card" @endif data-user-id="{{{$expertComment->user->id}}}">{{{$expertComment->user->name}}}</a></span>
			<div class="level">
				<div class="icon {{{App\Common\Utils::getLevelStyle($expertComment->user->creative_index)}}}"></div>
				<div class="bar">
					<div class="process" style="width: 60%;"></div>
				</div>
			</div>
			<?php $expertTitle = App\Common\Utils::getExpertTitle($expertComment->user->expert_organization_id);
                  $expertStyle = App\Common\Utils::getExpertStyle($expertComment->user->expert_organization_id);
              ?>
			 <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			<span class="date fr">{{{App\Common\Utils::formatDate($expertComment->create_time)}}}</span>
		</div>
		@if($showUserInfo && $expertComment->target_user)
		<div class="review">
			点评了<a class="n" href="{{{$baseURL}}}/user/home/{{$expertComment->target_user->id}}">{{{isset($expertComment->target_user->name) ? $expertComment->target_user->name:''}}}</a> 发布的作品：
		</div>
		@endif
		<div class="item-con">{{nl2br($expertComment->content)}}</div>
		@if($showWorkLink)
		<div class="goto">
			<a href="{{$showWorkLink}}">[查看作品]</a>
		</div>
		@endif
		<div class="value clearfix">
			<span class="t fl">创意价值</span>
			<div class="rateit c fl" data-rateit-starwidth="20"
				data-rateit-value="{{{$expertComment->score}}}" data-rateit-resetable="false"
				data-rateit-readonly="true"></div>
			<span class="common_text_selected" style="display: inline-block; margin-top: 0px">{{{App\Common\Utils::getCreativeValueRatingScoreText($expertComment->score)}}}</span>
		</div>
		@if($key < count($expertComments)-1)
		<div class="dotline"></div>
		@endif
	</div> 
</li> 
@endif
@endforeach