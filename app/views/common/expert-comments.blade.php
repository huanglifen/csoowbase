<div class="discuss-wrap">
	<div class="dis-top clearfix">
		<span class="t fl">专家点评</span>
		<a class="ebtn fr" href="{{$baseURL}}/{{App\Common\Utils::getModuleNameByTargetType($targetType)}}/invite-expert/0/{{$targetType}}/{{$targetId}}@if(!empty($targetSubId))/{{$targetSubId}}@endif">邀请专家点评</a>
		@if(!empty($canExpertComment) && empty($showCommentInputBox))
		<a class="ebtn fr" href="{{{$showCommentUrl}}}">{{{$commentBtnTitle}}}</a>
		@endif
	</div>
    <div class="height_10"></div>
	@if(!empty($canExpertComment) && !empty($showCommentInputBox))
	<div class="list-item">
		<a href="{{{$baseURL}}}/user/home/{{$user->id}}"><img class="avatar" src="{{{App\Common\Utils::getAvatar($user->avatar)}}}" alt=""></a>
		<div class="item-t">
			<span class="t"><a href="{{{$baseURL}}}/user/home/{{$user->id}}">{{{$user->name}}}</a></span>
			<div class="level">
				<div class="icon {{{App\Common\Utils::getLevelStyle($user->creative_index)}}}"></div>
				<div class="bar">
					<div class="process" style="width: 60%;"></div>
				</div>
			</div>
					@if($user->group_id == App\Modules\UserModule::GROUP_EXPERT)		
					<?php $expertTitle = App\Common\Utils::getExpertTitle($user->expert_organization_id);
                          $expertStyle = App\Common\Utils::getExpertStyle($user->expert_organization_id);
                     ?>
			         <span class="mark {{{$expertStyle}}}" title ="{{{$expertTitle}}}">{{{$expertTitle}}}</span>
			         @endif
		</div>
		<div class="dis-sendbox clear">
			<textarea class="inputbox Js_inputbox Js_inputbox_expertcomment" name=""  disabled="disabled" id="" placeholder="请输入您的@if(!empty($targetSubId))作品@else项目@endif评语.."></textarea>
			<div class="dis-bar">
				<span class="c fl">创意价值</span>
				<div class="Js_common_rateit2 rateit fl z" data-rateit-value="0" data-rateit-starwidth="20" data-rateit-resetable="false" data-rateit-step="1"></div>
				<span class="common_text_selected Js_cts" style="margin-top: 10px"></span>
				<span class="common_text Js_ct" style="margin-top: 10px"></span> 
				<a class="dis-subbtn fr Js_expdis_btn" href="javascript:" data-target-type="{{$targetType}}" data-target-id="{{$targetId}}" data-target-sub-id="@if(!empty($targetSubId)){{$targetSubId}}@else0@endif">发表点评</a>
			</div>
		</div>
	</div>
   @endif
   @if(count($expertComments))
<ul class="expert-list Js_dis_list">
<?php 
if(!empty($targetSubId)){
	if($targetType == 2){
		$targetType = 21;
	}elseif($targetType == 3){
		$targetType = 31;
	}
}
?>
@include('common.m-expert-comments',array('targetType'=>$targetType))
</ul>
@else
	<ul class="expert-list Js_dis_list">
	</ul>
	<div class="Js_experts_no_data">
		<div class="no_content">
		<div class="tip">暂无</div>
		</div>
	</div>
@endif
@if(!empty($showCommentUrl))
	<div class="pro-more">
		<a href="{{{$showCommentUrl}}}">更多&gt;&gt;</a>
	</div>
@endif
</div>