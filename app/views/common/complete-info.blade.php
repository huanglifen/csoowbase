@if($user && !$user->fill_area_profile)
<div class="noinfo-box">
	<p class="t"><b>请完善您的资料!</b></p>
	<p class="d">完善您的资料，代表您的城市，学校，公司在创意指数排行榜中胜出</p>
	<div class="noinfo-link">
		@if(!$user->fill_only_area_profile)<p><a href="{{$baseURL}}/account/user-info">添加所在地信息</a></p>@endif
		@if(!$user->school_id)<p><a href="{{$baseURL}}/account/user-info">添加学校信息</a></p>@endif
		@if(!$user->organization_id)<p><a href="{{$baseURL}}/account/user-info">添加企业信息</a></p>@endif
	</div>
</div>
@endif