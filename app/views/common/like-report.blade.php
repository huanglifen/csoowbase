<!-- 喜欢和举报 -->
<p class="pro-btns" data-target-type="{{$targetType}}" data-target-id="{{$targetId}}">
	<a class="pro-like @if($isLiked) on @else Js_likethis @endif" href="javascript:" title="@if($isLiked) 已喜欢  @else 喜欢  @endif"></a>
	<a class="pro-war @if($isReported) on @else Js_reportthis @endif " href="javascript:"  title="@if($isReported) 已举报  @else 举报 @endif"></a>
</p>

<div class="report_content message_box Js_report_box" style="display:none; width: 565px">
    <div class="box_tittle">
        <span class="t fl">请您选择举报原因</span>
        <a title="关闭" href="javascript:" class="close fr Js_cancel_report"></a>
    </div>
    <div class="box_content">
        <div class="choose_group">
            <form action="#" id="report-form" class="report_radio">
                <label><input type="radio"  name="type" value="1" checked="checked">广告</label>
                <label><input type="radio"  name="type" value="2">违法</label>
                <label><input type="radio"  name="type" value="3">抄袭</label>
                <label><input type="radio"  name="type" value="4">色情</label>
                <label><input type="radio"  name="type" value="5">恶意操作</label>
                <label style="margin-right: 0"><input type="radio"  name="type" value="6">其它</label>
            </form>
        </div>
        <div class="box_btm_btn report clearfix">
            <a href="javascript:" class="ok fl Js_send_report"  data-target-type="{{$targetType}}" data-target-id="{{$targetId}}" data-target-sub-id="@if(!empty($targetSubId))$targetSubId@endif">提交举报</a>
            <a href="javascript:" class="cancel fl Js_cancel_report">取消</a>
        </div>
    </div>
</div>

<div class="report_result message_box Js_reportSuccess_box" style="display:none">
    <div class="box_tittle">
        <a title="关闭" href="javascript:" class="close fr Js_cancel_report"></a>
    </div>
    <div class="box_content">
        <p style="padding: 10px 0">感谢您对我们的支持！工作人员会尽快核实。</p>
        <div class="box_btm_btn report_success clearfix">
            <a href="javascript:" class="cancel Js_cancel_report">关闭</a>
        </div>
    </div>
</div>

