<div class="project_wraper ntop_border">
	<div class="projecthead">
		<h3 class="projecttitle">热门交易</h3>
	</div>
	<ul>
	@foreach($exchanges as $exchange)
		<li class="projectlist">
			<a class="clearfix projectlink" href="{{{$baseURL}}}/exchange/detail/{{$exchange->id}}">
				<img class="projectavatar" src="{{{$baseURL}}}/{{{$exchange->cover}}}">
				<p class="tipone">{{{$exchange->title}}}</p>
				<p class="tiptwo"><em>{{$exchange->creative_index}}</em>点创意指数</p>
			</a>
		</li>
	@endforeach
	</ul>
</div>
