@foreach($experts as $expert)
<li class="clearfix Js_invite_item"  data-user-id="{{$expert->id}}">
<a href="javascript:;"> 
<img class="ava_img fl" src="{{{App\Common\Utils::getAvatar($expert->avatar)}}}" />
	<div class="tips fl">
		<div class="tipone">
			<span class="name">{{$expert->name}}</span>
			<div class="level">
				<div class="icon {{{App\Common\Utils::getLevelStyle($expert->creative_index)}}}"></div>
				<div class="bar">
					<div class="process" style="width: 60%;"></div>
				</div>
			</div>
		</div>
		<div class="tiptwo">
			<img class="fl tip_icon" src="{{$baseURL}}/images/icon_placeholder_14x14.png"> <span class="ccznum fl">{{$expert->cc_no}}</span>
		</div>
	</div>
</a>
</li>
@endforeach
