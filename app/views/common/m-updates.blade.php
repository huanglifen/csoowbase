<ul class="dynamic_content">
@foreach($updates as $update)
	<li class="clearfix" data-update-id="{{{$update->id}}}">
		<p class="date">{{{App\Common\Utils::formatDate($update->create_time)}}}</p>
		<?php 
		$updateUrl = '';
		if (isset($project)) {
			$updateUrl = 'project/update/' . $project->id . '/' . $update->id;
		} else if (isset($hiring)) {
			$updateUrl = 'hiring/update/' . $hiring->id . '/' . $update->id;
		} else if (isset($task)) {
			$updateUrl = 'task/update/' . $task->id . '/' . $update->id;
		}
		?>
		<p class="detail"><a class="Js_dynamic_list" href="{{{$baseURL}}}/{{{$updateUrl}}}">{{{$update->title}}}</a></p>
	</li> 
@endforeach
</ul>