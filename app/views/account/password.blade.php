@extends('common.main')
@section('title')
账号设置 - 创意世界
@stop
@section('content')

@include('user.user-top')

<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<div class="height_10"></div>
			<ul class="navlist clearfix fl" style="width:100%">
				<li class="current">
						<a class="c" href="{{{$baseURL}}}/account/user-info">个人设置</a>
					</li>
					<li>
						<a class="c" href="{{{$baseURL}}}/account/creative-index">创意指数</a>
					</li>
					<li>
						<a class="c" href="{{{$baseURL}}}/account/coin">创创币</a>
					</li>
			</ul>
		</div>
		<div class="sub-nav">
			<ul class="subnav-list clearfix">
				<li><a href="{{{$baseURL}}}/account/user-info">个人资料</a></li>
				<li class="current"><a href="{{{$baseURL}}}/account/account-safe">账号安全</a></li>
			</ul>
		</div>
		<div class="height_30"></div>
		<form id="changepassword-form" action="#">
			<ul class="person-userinfo clear">
				<li>
					<div class="row">
						<div class="rt" style="width:90px;">原始密码：</div>
						<div class="rc">
							<div class="con" id="current_password">
								<input style="position: relative" type="password" placeholder="" name="current_password" errormsg="请填写6到16位任意字符！" nullmsg="填写原始密码">
								<span class="Validform_checktip Validform_wrong" style="display:none"></span>
							</div>
						</div>
					</div>
				</li>
				<li>
                    <div class="row">
                        <div class="rt" style="width:90px;">新的密码：</div>
                        <div class="rc">
                            <div class="con">
                                <input type="password" placeholder="" name="new_password" plugin="passwordStrength" datatype="*6-22" nullmsg="请填写新密码" errormsg="请填写6到16位任意字符！" class="Validform_error">
                                <span class="Validform_checktip"></span>
                                <div class="passwordStrength"><span class="">弱</span><span>中</span><span class="last">强</span></div>
                            </div>
                        </div>
                    </div>
                </li>
				<li>
					<div class="row">
						<div class="rt" style="width:90px;">确认密码：</div>
						<div class="rc">
							<div class="con">
								<input type="password" placeholder="" name="new_password_again" recheck="new_password" datatype="*6-22" nullmsg="请填写确认密码" errormsg="两次输入的密码不一致！" class="Validform_error">
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="row">
						<div class="rt" style="width:90px;"></div>
						<div class="rc">
							<a class="Js_save_password" href="javascript:">
								<button type="button" class="btn DeepBlue">保存</button>
							</a>
                            <button type="button" class="btn DeepGrey" onclick="window.location.href='{{{$baseURL}}}/account/account-safe'">取消</button>
						</div>
					</div>
				</li>
			</ul>
		</form>
	</div>
</div>
<div class="message_box Js_save_pssword_success" style="display: none; width: 360px">
    <div class="box_tittle">
        <a class="close fr Js_update_phone_email_success_close" href="javascript:" title="关闭"></a>
    </div>
    <div class="box_content">
        <p style="padding: 6px 0">密码修改成功！</p>
        <div class="box_btm_btn report_success clearfix">
            <a class="cancel Js_update_phone_email_success_close" href="javascript:">关闭</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/passwordStrength_min.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/person.js"></script>
@stop
