<ul>
	<li class="c-title">
		<div class="c c1">时间</div>
		<div class="c c2">摘要</div>
		<div class="c c3">金额(枚)</div>
		<div class="c c4">账户余额(枚)</div>
	</li>
	@if(count($coinLogs))
	 @foreach($coinLogs as $coinLog)
	<li class="c-content">
		<div class="c c1">{{{App\Common\Utils::formatTime($coinLog->create_time)}}}</div>
		<div class="c c2">{{{$coinLog->related_info}}}</div>
		<div class="c c3">{{{floor($coinLog->amount)}}}</div>
		<div class="c c4">{{{floor($coinLog->balance)}}}</div>
	</li> @endforeach
	@endif
</ul>
<div class="height_20"></div>
<div class="paging Js_page">{{$pageHtml}}</div>
<div class="height_20"></div>
