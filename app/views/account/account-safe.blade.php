@extends('common.main')
@section('title')
账号设置 - 创意世界
@stop
@section('content')

@include('user.user-top')

<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<div class="height_10"></div>
			<ul class="navlist clearfix fl" style="width:100%">
				<li class="current">
						<a class="c" href="{{{$baseURL}}}/account/user-info">个人设置</a>
					</li>
					<li>
						<a class="c" href="{{{$baseURL}}}/account/creative-index">创意指数</a>
					</li>
					<li>
						<a class="c" href="{{{$baseURL}}}/account/coin">创创币</a>
					</li>
			</ul>
		</div>
		<div class="sub-nav">
			<ul class="subnav-list clearfix">
				<li><a href="{{{$baseURL}}}/account/user-info">个人资料</a></li>
				<li class="current"><a href="{{{$baseURL}}}/account/account-safe">账号安全</a></li>
			</ul>
		</div>
		<div class="height_30"></div>
		<ul class="person-userinfo clear">
			<li>
				<div class="row">
					<div class="rt" style="width:90px;">密码：</div>
					<div class="rc">
						<div class="con">
							<p class="txt">
								<span class="in">****************</span><a class="elink" href="{{{$baseURL}}}/account/user-password">修改</a>
							</p>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="row">
					<div class="rt" style="width:90px;">手机号码：</div>
					<div class="rc">
						<div class="con">
							<p class="txt">
							@if($user->mobile_phone_no)
								<span class="in">{{{substr($user->mobile_phone_no,0,3)}}}******{{{substr($user->mobile_phone_no,9,2)}}}</span><a class="elink" href="{{{$baseURL}}}/account/user-phone">修改</a>
							@else
							    <span class="in">您还没填写手机号，快填写吧！</span><a class="elink" href="{{{$baseURL}}}/account/user-phone">添加</a>
							@endif
							</p>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="row">
					<div class="rt" style="width:90px;">电子邮箱：</div>
					<div class="rc">
						<div class="con">
							<p class="txt">
								<span class="in">{{{App\Common\Utils::getSecretEmail($user->email)}}}</span><a class="elink" href="{{{$baseURL}}}/account/user-email">修改</a>
							</p>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/passwordStrength_min.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/person.js"></script>
@stop

