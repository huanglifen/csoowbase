@extends('common.main')
@section('title')
账号设置 - 创意世界
@stop
@section('content')

@include('user.user-top')

<script type="text/javascript" src="{{{$baseURL}}}/js/person.js"></script>

		<div class="content w1180 clearfix">
			<div class="detail-content">
				<div class="match-nav clearfix">
					<div class="height_10"></div>
					<ul class="navlist clearfix fl" style="width:100%">
						<li>
							<a class="c" href="{{{$baseURL}}}/account/user-info">个人设置</a>
						</li>
						<li class="current">
							<a class="c" href="{{{$baseURL}}}/account/creative-index">创意指数</a>
						</li>
						<li>
							<a class="c" href="{{{$baseURL}}}/account/coin">创创币</a>
						</li>
					</ul>
				</div>
				<div class="height_30"></div>
				<div class="person-exp">
					<div class="person-exp-t"> <b>我的创意指数</b>
					</div>
					<div class="person-exp-top">
						<div class="c">
							<label>创意等级：</label>
							<div class="level">
								<div class="icon {{{App\Common\Utils::getLevelStyle($user->creative_index)}}}"></div>
								<div class="bar">
									<div class="process" style="width: 22px;"></div>
								</div>
							</div>
						</div>
						<p class="c">
							<label>创意指数：</label>
							<i class="num">{{{$user->creative_index}}}</i>
							点
						</p>
						<p class="c">
							<label>未领取的创意指数奖励：</label>
							<i class="num">{{{$user->creative_index_bonus_amount}}}</i>
							枚创创币
							<span><a class="p-btn" href="{{{$baseURL}}}/account/award-creative">立即领取</a></span>
						</p>
					</div>
					<div class="height_30"></div>
					<div class="person-exp-t"> <b>查询</b>
					</div>
					<div class="person-datearea clearfix">
						<div class="fl">
							<input type="text" placeholder="起始日期" class="startdate" name="date1" datatype="*" errormsg="请选择日期" nullmsg="请选择日期">
							&nbsp;&nbsp;至&nbsp;&nbsp;
							<input type="text" placeholder="结束日期" class="enddate" name="date2" datatype="*" errormsg="请选择日期" nullmsg="请选择日期">
						</div>
						<div class="Js_dateselect person-date-item fl">
							<a data-datetype="1" href="javascript:">今天</a>
							<a data-datetype="2" class="current" href="javascript:">最近1个月</a>
							<a data-datetype="3" href="javascript:">3个月</a>
							<a data-datetype="4" href="javascript:">1年</a>
						</div>
					</div>
					<div class="height_20"></div>
					<div class="sub-nav clear">
						<ul class="subnav-list clearfix Js_tabindex">
							<li class="current" data-t-index="0">
								<a href="javascript:">全部</a>
							</li>
							<li data-t-index="1">
								<a href="javascript:">增加指数</a>
							</li>
							<li data-t-index="-1">
								<a href="javascript:">减少指数</a>
							</li>
						</ul>
					</div>
					<div class="person-searchlist Js_s_result">
						@include('account.m-creative-index')
					</div>
				</div>
			</div>
		</div>
	<script type="text/javascript" src="{{{$baseURL}}}/js/zebra_datepicker.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/person.js"></script>
	<script>
		var account_type = "creative"
	</script>
@stop