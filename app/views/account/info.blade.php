@extends('common.main')
@section('title')
账号设置 - 创意世界
@stop
@section('content')

@include('user.user-top')
<div class="content w1180 clearfix">
    <div class="detail-content">
        <div class="match-nav clearfix">
            <div class="height_10"></div>
            <ul class="navlist clearfix fl" style="width:100%">
                <li class="current">
                    <a class="c" href="{{{$baseURL}}}/account/user-info">个人设置</a>
                </li>
                <li>
                    <a class="c" href="{{{$baseURL}}}/account/creative-index">创意指数</a>
                </li>
                <li>
                    <a class="c" href="{{{$baseURL}}}/account/coin">创创币</a>
                </li>
            </ul>
        </div>
        <div class="sub-nav">
            <ul class="subnav-list clearfix">
                <li class="current"><a href="{{{$baseURL}}}/account/user-info">个人资料</a></li>
                <li><a href="{{{$baseURL}}}/account/account-safe">账号安全</a></li>
            </ul>
        </div>
        <div class="height_30"></div>
        <form id="user-info-form" action="#">
            <ul class="person-userinfo clear">
                <li>
                    <div class="row">
                        <div class="rt" style="width:90px;">头像：</div>
                        <div class="rc">
                            <div class="con">
                                <div class="person-upload-avatar fl Js_uploaded_avatar">
                                    <p class="avatar-wrap"><img class="avatar" data-original-src="@if($user->avatar){{{App\Common\Utils::getAvatar($user->avatar)}}}@else{{{$baseURL}}}/images/avatar_upload.png@endif" src="@if($user->avatar){{{App\Common\Utils::getAvatar($user->avatar)}}}@else{{{$baseURL}}}/images/avatar_upload.png@endif" alt=""></p>
                                    <a class="Js_showupload" href="javascript:">上传头像</a>
                                </div>

                                <div class="avatar-upload fl">
                                    <i class="arr"></i>
                                    <div class="fl">
                                        <input class="fl" id="file_upload" name="file_upload" type="file">
                                    </div>
                                    <div class="Js_camera_btn avatar-btn fr" style="display:none">拍照上传</div>
                                    <div class="avatar-viewbox clear">
                                        <p class="s-text">请选择大于120X120像素的图片，保存为头像<br />（仅支持小于5M的jpg，gif，png格式图片）</p>
                                    </div>
                                    <div class="avatar-btnarea">
                                        <span class="Js_avatar_cancel avatar-cbtn">取消</span><span class="Js_avatar_shot avatar-sbtn">拍照</span><span class="Js_camera_btn avatar-reshot avatar-sbtn">重新拍照</span><span class="Js_avatar_confirm avatar-sbtn">保存</span>
                                    </div>
                                    <!---->
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="rt" style="width:90px;">生日：</div>
                        <div class="rc">
                            <div class="con" id="birthday">
                                <input type="text" name="birthday" style="width:200px;" value="{{{App\Common\Utils::formatDate($user->birthday)}}}" class="Js_birthday" errormsg="请选择日期" nullmsg="请选择日期">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="rt" style="width:90px;">性别：</div>
                        <div class="rc">
                            <div class="con" id="gender">
                                <span class="item"><input id="r1" type="radio" name="gender" value="2" @if($user->gender == 2)checked="checked"@endif><label for="r1">男</label></span>
                                <span class="item"><input id="r2" type="radio" value="1" name="gender" @if($user->gender == 1)checked="checked"@endif><label for="r2">女</label></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="rt" style="width:90px;">个人简介：</div>
                        <div class="rc">
                            <div class="con" id="introduction">
                                <textarea name="introduction" id="" style="padding:5px; width:400px; height:120px;vertical-align: bottom; overflow:auto">{{{$user->introduction}}}</textarea>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="rt" style="width:90px;">个人标签：</div>
                        <div class="rc">
                            <div class="con" id="tags">
                                <input style="position: relative" type="text" name="tags" value="{{{$user->tags}}}" errormsg="" nullmsg="">
                            </div>
                            <div class="illus" style="font-size: 12px;color: #D15552;">
                                请输入体现个人特点的标签词语，词语间通过逗号分隔开，词语只能包含中文、英文、数字
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="rt" style="width:90px;">所在地：</div>
                        <div class="rc">
                            @if(!empty($user->district_id))
                               <?php $areaId = substr($user->district_id,0,2).'|'.substr($user->district_id,2,2).'|'.substr($user->district_id,4,2);?>
                            @elseif(!empty($user->city_id))
                               <?php $areaId = substr($user->city_id,0,2).'|'.substr($user->city_id,2,2).'|00';?>
                            @elseif(!empty($user->province_id))
                               <?php $areaId = $user->province_id.'|00|00';?>
                            @else
                               <?php $areaId = 0?>
                            @endif
                            <div class="con" id="area_id">
                                <div class="desDropDownMenu clearfix" style="margin-left:0">
                                    <div class="fl" style="width:120px; margin-right:8px;">
                                        <div class="dropDownMenuInput Js_userarea">
                                            <s class="dropDownBtn"></s>
                                            <span class="promptInfo"></span>
                                            <ul class="dropDownList"></ul>
                                            <select></select>
                                        </div>
                                    </div>
                                    <div class="fl" style="width:120px; margin-right:8px;">
                                        <div class="dropDownMenuInput Js_userarea">
                                            <s class="dropDownBtn"></s>
                                            <span class="promptInfo"></span>
                                            <ul class="dropDownList"></ul>
                                            <select></select>
                                        </div>
                                    </div>
                                    <div class="fl" style="width:120px;">
                                        <div class="dropDownMenuInput Js_userarea">
                                            <s class="dropDownBtn"></s>
                                            <span class="promptInfo"></span>
                                            <ul class="dropDownList"></ul>
                                            <select></select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="Js_area" id="area_selected_id" name="area_id" value="{{{$areaId}}}">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="rt" style="width:90px;">学校：</div>
                        <div class="rc">
                            <div class="con" id="school_id" style="position: relative">
                                <input class="Js_school_input" data-school-id="@if(isset($userInfo->school)){{{$userInfo->school->id}}}@endif" type="text" placeholder="" value="@if(isset($userInfo->school)){{{$userInfo->school->name}}}@endif"  errormsg="" nullmsg="">
                                <div class="banglist Js_infolist" style="display:none">
                                    <ul>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="rt" style="width:90px;">政企：</div>
                        <div class="rc">
                            <div class="con" id="organization_id"  style="position: relative">
                                <input class="Js_organization_input" data-organization-id="@if(isset($userInfo->organization)){{{$userInfo->organization->id}}}@endif" type="text" placeholder="" value="@if(isset($userInfo->organization)){{{$userInfo->organization->name}}}@endif" errormsg="" nullmsg="">
                                <div class="banglist Js_infolist" style="display:none">
                                    <ul>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="rt" style="width:90px;"></div>
                        <div class="rc">
                            <a class="Js_save_userinfo" >
                                <button type="button" class="btn DeepBlue">保存</button>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </form>
    </div>
</div>
<div class="message_box Js_save_userinfo_success" style="display: none; width: 360px">
    <div class="box_tittle">
        <a class="close fr Js_UserInfoSuccess_close" href="javascript:" title="关闭"></a>
    </div>
    <div class="box_content">
        <p style="padding: 6px 0">个人资料修改成功！</p>
        <div class="box_btm_btn report_success clearfix">
            <a class="cancel Js_UserInfoSuccess_close" href="javascript:">关闭</a>
        </div>
    </div>
</div>
<form id="cropform" action="upload-avatar.php" method="post">
    <input type="hidden" id="x" name="start_x" />
    <input type="hidden" id="y" name="start_y" />
    <input type="hidden" id="x2" name="end_x" />
    <input type="hidden" id="y2" name="end_y" />
    <input type="hidden" id="avatar-pic" name="file_name" value="">
</form>
<div id="avatar-camera"></div>
<script type="text/javascript" src="{{{$baseURL}}}/js/CityList.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.dropDownMenu.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/zebra_datepicker.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.uploadify.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.Jcrop.min.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/webcam.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/uploadavatar.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/person.js"></script>
<script>var uploadPath = "account/upload-image";</script>
@stop
