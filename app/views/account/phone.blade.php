@extends('common.main')
@section('title')
账号设置 - 创意世界
@stop
@section('content')

@include('user.user-top')

<div class="content w1180 clearfix">
	<div class="detail-content">
		<div class="match-nav clearfix">
			<div class="height_10"></div>
			<ul class="navlist clearfix fl" style="width:100%">
				<li class="current">
						<a class="c" href="{{{$baseURL}}}/account/user-info">个人设置</a>
					</li>
					<li>
						<a class="c" href="{{{$baseURL}}}/account/creative-index">创意指数</a>
					</li>
					<li>
						<a class="c" href="{{{$baseURL}}}/account/coin">创创币</a>
					</li>
			</ul>
		</div>
		<div class="sub-nav">
			<ul class="subnav-list clearfix">
				<li><a href="{{{$baseURL}}}/account/user-info">个人资料</a></li>
				<li class="current"><a href="{{{$baseURL}}}/account/account-safe">账号安全</a></li>
			</ul>
		</div>
		<div class="height_30"></div>
		<form id="changephone-form" action="#">
			<ul class="person-userinfo clear">
				@if(!empty($user->mobile_phone_no))
				<li>
					<div class="row">
						<div class="rt" style="width:90px;">原手机号码：</div>
						<div class="rc">
							<div class="con" id="old_mobile">
								<input type="text" class="Js_oldnumber" data-numtype="phone" name="old_mobile" >
							</div>
						</div>
					</div>
				</li>
				@endif
				<li>
					<div class="row">
						<div class="rt" style="width:90px;">@if(!empty($user->mobile_phone_no))新@endif手机号码：</div>
						<div class="rc">
							<div class="con" id="new_mobile">
								<input type="text" id="Js_new_mobile" placeholder="" datatype="e" name="new_mobile" errormsg="请填写正确的手机号码" nullmsg="请填写手机号码">
								<span class="Validform_checktip Validform_wrong" style="display:none"></span>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="row">
						<div class="rt" style="width:90px;">短信验证码：</div>
						<div class="rc">
							<div class="con" id="verify_code">
								<input style="width: 118px" type="text" placeholder="请输入验证码" name="verify_code" value="" ignore="" errormsg="验证码有误！" nullmsg="请填写短信验证码！">
								<a class="mobile_phone_auth-edit-btn Js_getCode"  data-codenumtype="phone" href="javascript:">免费获取验证码</a>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="row">
						<div class="rt" style="width:90px;"></div>
						<div class="rc">
							<a class="Js_update_phone_email" data-update-type="phone" href="javascript:" >
								<button type="button" class="btn DeepBlue">保存</button>
							</a>
                            <button type="button" class="btn DeepGrey" onclick="window.location.href='{{{$baseURL}}}/account/account-safe'">取消</button>
						</div>
					</div>
				</li>
			</ul>
		</form>
	</div>
</div>
<div class="message_box Js_update_phone_email_success" style="display: none; width: 360px">
    <div class="box_tittle">
        <a class="close fr Js_update_phone_email_success_close" href="javascript:" title="关闭"></a>
    </div>
    <div class="box_content">
        <p style="padding: 6px 0">手机号码修改成功！</p>
        <div class="box_btm_btn report_success clearfix">
            <a class="cancel Js_update_phone_email_success_close" href="javascript:">关闭</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/passwordStrength_min.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/person.js"></script>
@stop

