<ul class="">
	<li class="c-title">
		<div class="c c1">时间</div>
		<div class="c c2">内容</div>
		<div class="c c3">指数变化(点)</div>
		<div class="c c4">创意指数(点)</div>
	</li> 
	@foreach($creativeIndexLogs as $creativeIndexLog)
	<li class="c-content">
		<div class="c c1">{{{App\Common\Utils::formatTime($creativeIndexLog->create_time)}}}</div>
		<div class="c c2">{{{$creativeIndexLog->related_info}}}</div>
		<div class="c c3">{{{floor($creativeIndexLog->amount)}}}</div>
		<div class="c c4">{{{floor($creativeIndexLog->balance)}}}</div>
	</li>
	@endforeach
</ul>
<div class="height_20"></div>
<div class="paging Js_page">{{$pageHtml}}</div>
<div class="height_20"></div>
