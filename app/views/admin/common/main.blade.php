<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>创意世界后台系统</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="MobileOptimized" content="320">
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="{{$baseURL}}/admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="{{$baseURL}}/admin/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="{{$baseURL}}/admin/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="{{$baseURL}}/admin/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" href="{{$baseURL}}/admin/plugins/data-tables/DT_bootstrap.css" />
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME STYLES -->
	<link href="{{$baseURL}}/admin/css/style-metronic.css" rel="stylesheet" type="text/css"/>
	<link href="{{$baseURL}}/admin/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="{{$baseURL}}/admin/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="{{$baseURL}}/admin/css/plugins.css" rel="stylesheet" type="text/css"/>
	<link href="{{$baseURL}}/admin/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="{{$baseURL}}/admin/css/custom.css" rel="stylesheet" type="text/css"/>
	<!-- END THEME STYLES -->
	<link rel="shortcut icon" href="favicon.ico" />
		<script>
		var baseUrl = '<?php echo $baseURL.'/';?>';
	</script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.js"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
		<a class="navbar-brand" href="index.html">创意世界后台系统</a>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<img src="{{$baseURL}}/admin/img/menu-toggler.png" alt="" />
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<ul class="nav navbar-nav pull-right">
			<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<img alt="" src="{{$baseURL}}/admin/img/avatar1_small.jpg"/>
					<span class="username">{{{$user->name}}}</span>
					<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li><a href="{{{$baseURL}}}/user/logout"><i class="fa fa-key"></i> 退出</a></li>
				</ul>
			</li>
			<!-- END USER LOGIN DROPDOWN -->
		</ul>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="page-sidebar-menu">
			<li>
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler hidden-phone"></div>
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			</li>

			<li class="start @if($curPageName =='admin\admin') active @endif">
				<a href="{{$baseURL}}/admin/index">
					<i class="fa fa-home"></i>
					<span class="title">主面板</span>
					@if($curPageName =='admin\admin')<span class="selected"></span>@endif
				</a>
			</li>

			<li class="start @if($curPageName =='admin\check') active @endif">
					<a href="#">
					<i class="fa fa-home"></i>
					<span class="title">比赛审核</span>
					@if($curPageName =='admin\check')<span class="selected"></span>@endif
				</a>
				<ul class="sub-menu">
					<li>
						<a href="{{$baseURL}}/check/contests"><i class="fa fa-briefcase"></i>创意比赛</a>
					</li>
					<li>
						<a href="{{$baseURL}}/check/exchanges"><i class="fa fa-clock-o"></i><span class="badge badge-info"></span>创意交易</a>
					</li>
					<li>
						<a href="{{$baseURL}}/check/encyclopedias"><i class="fa fa-cogs"></i>创意百科</a>
					</li>
				</ul>
			</li>

			<li class="start @if($curPageName =='admin\expert') active @endif">
				<a href="#">
					<i class="fa fa-home"></i>
					<span class="title">专家审核和编辑</span>
					@if($curPageName =='admin\expert')<span class="selected"></span>@endif
				</a>
				<ul class="sub-menu">
					<li>
						<a href="{{$baseURL}}/expert/user"><i class="fa fa-briefcase"></i>用户列表</a>
					</li>
					<li>
						<a href="{{$baseURL}}/expert/expert"><i class="fa fa-briefcase"></i>专家列表</a>
					</li>
				</ul>
			</li>

			<li class="start @if($curPageName =='admin\industry') active @endif">
				<a href="{{$baseURL}}/industry/index">
					<i class="fa fa-home"></i>
					<span class="title">行业管理</span>
					@if($curPageName =='admin\industry')<span class="selected"></span>@endif
				</a>
			</li>

		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN PAGE -->
	<div class="page-content">
		@section('content')
		@show
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">
		2014 &copy;  创意世界
	</div>
	<div class="footer-tools">
			<span class="go-top">
			<i class="fa fa-angle-up"></i>
			</span>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{$baseURL}}/admin/plugins/respond.min.js"></script>
<script src="{{$baseURL}}/admin/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{$baseURL}}/admin/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="{{$baseURL}}/admin/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="{{$baseURL}}/admin/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{$baseURL}}/admin/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
<script src="{{$baseURL}}/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{$baseURL}}/admin/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{$baseURL}}/admin/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="{{$baseURL}}/admin/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{$baseURL}}/admin/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{$baseURL}}/admin/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{$baseURL}}/admin/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{$baseURL}}/admin/scripts/app.js"></script>
<script src="{{$baseURL}}/admin/scripts/table-managed.js"></script>
<script>
	jQuery(document).ready(function() {
		App.init();
	});
</script>
</body>
<!-- END BODY -->
</html>