@extends('admin.common.main')

@section('content')

<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-reorder"></i>创意世界后台系统</div>
		<div class="tools">
			<a class="collapse" href="javascript:;"></a>
			<a class="config" data-toggle="modal" href="#portlet-config"></a>
			<a class="reload" href="javascript:;"></a>
			<a class="remove" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		<a class="icon-btn" href="{{$baseURL}}/check/contests">
			<i class="fa fa-group"></i>
			<div>创意比赛</div>
			<span class="badge badge-important"></span>
		</a>
		<a class="icon-btn" href="{{$baseURL}}/check/exchanges">
			<i class="fa fa-barcode"></i>
			<div>创意交易</div>
			<span class="badge badge-success"></span>
		</a>
		<a class="icon-btn" href="{{$baseURL}}/check/encyclopedias">
			<i class="fa fa-bar-chart-o"></i>
			<div>创意百科</div>
		</a>
		<a class="icon-btn" href="{{$baseURL}}/expert/expert">
			<i class="fa fa-sitemap"></i>
			<div>专家审核和编辑</div>
		</a>
		<a class="icon-btn" href="{{$baseURL}}/industry/index">
			<i class="fa fa-calendar"></i>
			<div>行业管理</div>
			<span class="badge badge-success"></span>
		</a>
<!--		<a class="icon-btn" href="#">
			<i class="fa fa-envelope"></i>
			<div>Inbox</div>
			<span class="badge badge-info">12</span>
		</a>
		<a class="icon-btn" href="#">
			<i class="fa fa-bullhorn"></i>
			<div>Notification</div>
			<span class="badge badge-important">3</span>
		</a>
		<a class="icon-btn" href="#">
			<i class="fa fa-map-marker"></i>
			<div>Locations</div>
		</a>
		<a class="icon-btn" href="#">
			<i class="fa fa-money"><i></i></i>
			<div>Finance</div>
		</a>
		<a class="icon-btn" href="#">
			<i class="fa fa-plane"></i>
			<div>Projects</div>
			<span class="badge badge-info">21</span>
		</a>
		<a class="icon-btn" href="#">
			<i class="fa fa-thumbs-up"></i>
			<div>Feedback</div>
			<span class="badge badge-info">2</span>
		</a>
		<a class="icon-btn" href="#">
			<i class="fa fa-cloud"></i>
			<div>Servers</div>
			<span class="badge badge-important">2</span>
		</a>
		<a class="icon-btn" href="#">
			<i class="fa fa-globe"></i>
			<div>Regions</div>
		</a>-->

	</div>
</div>

@stop