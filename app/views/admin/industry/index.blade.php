@extends('admin.common.main')


@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-edit"></i>一级行业列表</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover">
					<thead>
					<tr>
						<th>行业ID</th>
						<th>行业名称</th>
						<th>行业简介</th>
						<th>状态</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($datas as $key => $data)
					<tr class="odd gradeX">
						<td>{{{$data->id}}}</td>
						<td><a href="{{$baseURL}}/industry/second-industry/{{$data->id}}" title="查看详情">{{{$data->title}}}</a></td>
						<td>{{{$data->description}}}</td>
						<td>{{{$data->is_recommended}}}</td>
						<td>
							<a href="{{$baseURL}}/industry/edit/{{$data->id}}">编辑</a>

							@if($data->is_recommended)<a href="{{$baseURL}}/industry/recommend/{{{$data->id}}}/0">不推荐</a>
                            @else<a href="{{$baseURL}}/industry/recommend/{{{$data->id}}}/1">推荐</a>
                            @endif
							<a href="{{$baseURL}}/industry/delete/{{$data->id}}">删除</a>

							<a href="{{$baseURL}}/industry/add/0">新增</a>

						</td>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop