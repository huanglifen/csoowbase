@extends('admin.common.main')

@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-edit"></i>用户列表</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover">
					<thead>
					<tr>
						<th>序列</th>
						<th>专家名称</th>
						<th>邮箱</th>
						<th>注册时间</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($datas as $key => $data)
					<tr class="odd gradeX">
						<td>{{{$key + 1}}}</td>
						<td>{{{$data->name}}}</td>
						<td>{{{$data->email}}}</td>
						<td>{{{date('Y-m-d', $data->register_time)}}}</td>
						<td>
							<a href="{{$baseURL}}/expert/expert-edit/{{$data->id}}">编辑</a>
						</td>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
	@if(!empty($totalPage) && $totalPage > 1)
	{{$pageHtml}}
	@endif
</div>
@stop