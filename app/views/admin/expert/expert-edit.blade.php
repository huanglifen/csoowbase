@extends('admin.common.main')


@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="form-horizontal form-bordered">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> 专家信息
					</div>
				</div>
				<div class="portlet-body form">
					<form role="form">
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-3">专家名称：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{{$data->name}}}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">邮箱：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{{$data->email}}}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">用户头像：</label>
								<div class="col-md-9">
									<p class="form-control-static"><img src="{{{App\Common\Utils::getAvatar($data->avatar)}}}" alt=""></p>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">用户介绍：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{$data->introduction}}</p>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-horizontal form-bordered">
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> 专家信息
					</div>
				</div>
				<div class="portlet-body form">
					<form role="form">
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-3">组织：</label>
								<div class="col-md-4">
									<select class="form-control" name="expert_organization_id">
										<option value="0">--请选择组织--</option>
										<option value="1">创意世界智库主席团</option>
										<option value="2">创意世界智库理事会</option>
										<option value="3">创意世界智库专家顾问团</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">职务：</label>
								<div class="col-md-4">
									<select class="form-control" name="expert_duty_id">
										<option value="0">--请选择职务--</option>
										<option value="1">创意世界智库主席团</option>
										<option value="2">创意世界智库理事会</option>
										<option value="3">创意世界智库专家顾问团</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">邮箱：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{{$data->email}}}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">用户头像：</label>
								<div class="col-md-9">
									<p class="form-control-static"><img src="{{{App\Common\Utils::getAvatar($data->avatar)}}}" alt=""></p>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">用户介绍：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{$data->introduction}}</p>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@stop