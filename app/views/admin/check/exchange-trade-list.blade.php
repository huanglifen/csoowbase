@extends('admin.common.main')


@section('content')
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-1 control-label">审核状态：</label>
			<div class="col-md-5">
				<select class="form-control" onchange="self.location.href=options[selectedIndex].value">
				   <option value="{{{$baseURL}}}/check/exchanges" @if($status == '') selected="selected" @endif>默认</option>
					<option value="{{{$baseURL}}}/check/exchanges/-3" @if($status == -3) selected="selected" @endif>待审核</option>
					<option value="{{{$baseURL}}}/check/exchanges/-4" @if($status == -4) selected="selected" @endif>需要修改</option>
					<option value="{{{$baseURL}}}/check/exchanges/-5" @if($status == -5) selected="selected" @endif>再次审核</option>
					<option value="{{{$baseURL}}}/check/exchanges/2" @if($status == 2) selected="selected" @endif>进行中</option>
				</select>
			</div>
		</div>
	</div>

	<div class="margin-bottom-20 clearfix"></div>
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-edit"></i>比赛列表</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover">
					<thead>
					<tr>
						<th>序列</th>
						<th>交易名称</th>
						<th>交易发布时间</th>
						<th>发布者</th>
						<th>审核状态</th>
					</tr>
					</thead>
					<tbody>
					@foreach($lists as $key => $list)
					<?php 						
						$check = '';
						
						switch ($list->status) {
							case App\Modules\ItemModule::STATUS_WAITING_CHECK : $check = '待审核'; $class="label-info";break;
							case App\Modules\ItemModule::STATUS_NEED_UPDATE : $check = '未通过';$class="label-danger";break;
							case App\Modules\ItemModule::STATUS_CHECK_AGAIN : $check = '再次审核';$class="label-info";break;
							case App\Modules\ItemModule::STATUS_GOING : $check= '进行中';$class="label-success";break;
							case App\Modules\ItemModule::STATUS_END_SUCCESS : $check= '成功';$class="label-success";break;
							default:$check="待审核";$class="label-info";
						}
					?>
					<tr class="odd gradeX">
						<td>{{{$key+1}}}</td>
						<td><a href="{{{$baseURL}}}/check/exchange-detail/{{{$list->id}}}">{{{$list->title}}}</a></td>
						<td>{{{date("Y/m/d", $list->start_time)}}}</td>
						<td><a href="{{{$baseURL}}}/user/home/{{{$list->user->id}}}" target="_blank">{{{$list->user->name}}}</a></td>
						<td><span class="label label-sm {{{$class}}}">{{{$check}}}</span></td>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
@if(!empty($totalPage)&&$totalPage > 1)
{{$pageHtml}}
@endif
</div>
<!-- END PAGE CONTENT -->
@stop