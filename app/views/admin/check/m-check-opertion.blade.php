<div class="col-md-5">
	<div class="form-horizontal form-bordered">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i> 比赛审核
				</div>
			</div>
			<div class="portlet-body form">
				<form role="form">
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-3 control-label">通用原因：</label>
							<div class="col-md-9">
								<select class="form-control" id="Js_check_default">
									<option value="广告">广告</option>
									<option value="违法">违法</option>
									<option value="抄袭">抄袭</option>
									<option value="色情">色情</option>
									<option value="恶意操作">恶意操作</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">审核信息：</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="check_remark" id="check_remark"></textarea>
							</div>
						</div>

					</div>
					<div class="form-actions right">
						<button type="button" class="btn default Js_check_btn" value="2">不通过</button>
						<button type="button" class="btn blue Js_check_btn" value="1">通过</button>
						<input type="hidden" id="check_id" value="{{{$check_id}}}">
						<input type="hidden" id="check_type" value="{{$check_type}}">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	//选择通用原因，填充审核信息字段
	$("#Js_check_default").change(function() {
		var remark = $("#Js_check_default").find("option:selected").text();
		$("#check_remark").val(remark);
	});

	//点击通过、不通过的事件
	$(".Js_check_btn").on('click', function() {
		var check_type = $("#check_type").val();
		var check_id = $("#check_id").val();
		var status = $(this).val();
		var reason = $("#check_remark").val()?$("#check_remark").val():'';

		//return false;
		var data = {'type':check_type,'id':check_id, 'status': status, 'reason' : reason};
		$.ajax({
			type :'POST',
			dataType : 'JSON',
			data : data,
			url : baseUrl+'check/check-opertion',
			success: function(result) {
				if(result.status == 'ok') {
					window.location.href = baseUrl+'check/' + check_type + 's';
				}
			}
		});
	});

</script>