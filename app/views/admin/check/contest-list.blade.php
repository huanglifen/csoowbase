@extends('admin.common.main')


@section('content')
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-1 control-label">审核状态：</label>
			<div class="col-md-5">
				<select class="form-control" onchange="self.location.href=options[selectedIndex].value">
					<option value="{{{$baseURL}}}/check/contests" @if($status == -'') selected="selected" @endif>默认</option>
					<option value="{{{$baseURL}}}/check/contests/-3" @if($status == -3) selected="selected" @endif>待审核</option>
					<option value="{{{$baseURL}}}/check/contests/-4" @if($status == -4) selected="selected" @endif>需要修改</option>
					<option value="{{{$baseURL}}}/check/contests/-5" @if($status == -5) selected="selected" @endif>再次审核</option>
					<option value="{{{$baseURL}}}/check/contests/2" @if($status == 2) selected="selected" @endif>进行中</option>
				</select>
			</div>
		</div>
	</div>
		<div class="margin-bottom-20 clearfix"></div>
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-edit"></i>比赛列表</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover">
					<thead>
					<tr>
						<th>序列</th>
						<th>比赛名称</th>
						<th>比赛发布时间</th>
						<th>比赛类型</th>
						<th>发布者</th>
						<th>审核状态</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($contests as $key => $contest)
					<?php 
						$typeStr = '';
						
						$check = '';
						switch ($contest->type) {
							case 1:$typeStr = '投资项目比赛';break;
							case 2:$typeStr = '任务人才比赛';break;
							case 3:$typeStr = '公益明星比赛';break;
							default:$typeStr = '投资项目比赛';
						}
						
						switch ($contest->status) {
							case -3:$check = '待审核';break;
							case -4:$check = '未通过';break;
							case -5:$check = '再次审核';break;
						}
					?>
					<tr class="odd gradeX">
						<td>{{{$key + 1}}}</td>
						<td><a href="{{{$baseURL}}}/check/contest-detail/{{{$contest->id}}}">{{{$contest->title}}}</a></td>
						<td>{{{date('Y-m-d', $contest->start_time)}}}</td>
						<td>{{{$typeStr}}}</td>
						<td><a href="{{{$baseURL}}}/user/home/{{{$contest->user->id}}}">{{{$contest->user->name}}}</td>
						<td><span class="label label-sm label-info">{{{$check}}}</span></td>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
@if(!empty($totalPage)&&$totalPage > 1)
{{$pageHtml}}
@endif
</div>
<!-- END PAGE CONTENT -->
@stop