@extends('admin.common.main')


@section('content')

<div class="row">
	<div class="col-md-7">
		<div class="form-horizontal form-bordered">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> 比赛信息
					</div>
				</div>
				<div class="portlet-body form">
					<form role="form">
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-3">比赛名称：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{{$encyclopedia->title}}}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">发布者：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{{$encyclopedia->user->name}}}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">封面图片：</label>
								<div class="col-md-9">
									<p class="form-control-static"><img src="{{{$baseURL}}}/{{{$encyclopedia->cover}}}" style="width:280px; height:220px " alt=""></p>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">创意项目详情介绍：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{$encyclopedia->content}}</p>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	@include('admin.check.m-check-opertion',array('check_type'=>'encyclopedia','check_id'=>$encyclopedia->id))

</div>

@stop