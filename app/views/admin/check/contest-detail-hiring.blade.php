@extends('admin.common.main')


@section('content')

<div class="row">
	<div class="col-md-7">
		<div class="form-horizontal form-bordered">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> 比赛信息
					</div>
				</div>
				<div class="portlet-body form">
					<form role="form">
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-3">比赛名称：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{{$contest->title}}}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">发布者：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{{$contest->user->name}}}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">封面图片：</label>
								<div class="col-md-9">
									<p class="form-control-static"><img src="{{{$baseURL}}}/{{{$contest->cover}}}" style="width:280px; height:220px " alt=""></p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">奖项：</label>
								<div class="col-md-9">
								@foreach ($contest->prizes as $prize)
									<table class="table table-bordered table-hover">
										<tbody>
										<tr>
											<td width="20%">头衔</td>
											<td>{{{$prize->title}}}</td>
										</tr>
										<tr>
											<td>奖金</td>
											<td>{{{$prize->bonus_amount}}}</td>
										</tr>
										@if ($prize->award)
										<tr>
											<td>奖品</td>
											<td>{{{$prize->award}}}</td>
										</tr>
										@endif
										</tbody>
									</table>
								@endforeach
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">寻找明星期限：</label>
								<div class="col-md-9">
									<p class="form-control-static">
									@if ($contest->info->end_time)
									{{{date('Y-m-d', $contest->info->end_time)}}}
									@else
									长期
									@endif							
									</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">创意比赛详情介绍：</label>
								<div class="col-md-9">
									<p class="form-control-static">{{$contest->description}}</p>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@include('admin.check.m-check-opertion',array('check_type'=>'contest','check_id'=>$contest->id))
</div>
@stop