@extends('common.main') 

@section('title')
交易所 - 创意世界
@stop

@section('content')
<?php $list_contest_type = 4 ?>
@include('common.list-top')
<div class="list-page content w1180">
	<div class="height_20"></div>
	<div class="cwindex-rank">
		<div class="cwindex-title">
			<p class="line"></p>
			<p class="ctitle">
				<span>创意指数排行榜</span>
			</p>
		</div>
		<div class="height_10"></div>
		<ul class="clearfix">
			@foreach($ranks as $key => $rank)
			<li>
				<div class="cwindex-rank-box">
					<a href="{{{$baseURL}}}/exchange/detail/{{{$rank->id}}}">
						<p class="pic">
							<img src="{{{$baseURL}}}/{{{$rank->cover}}}" alt=""> <i
								class="num">{{{$key+1}}}</i>
						</p>
						<div class="context c1">
							<h5 class="title">{{{$rank->title}}}</h5>
							<p class="bar">
								<span class="n fl">{{{$rank->user->name}}}</span> <span
									class="z fr">{{{$rank->creative_index}}}</span>
							</p>
						</div>
					</a>
				</div>
			</li> 
			@endforeach
		</ul>
	</div>
	<div class="height_10"></div>
	<div class="Js_list_tag">
		<div class="list_tag_wrap w1180">
			<div class="list-tags clearfix">
				<p class="tags fl Js_tag">
					@include('common.hot-tags')
				</p>
				<p class="refresh fr">
					<a class="t Js_refreshtag" href="javascript:" title="刷新">刷新</a>
				</p>
			</div>
			<div class="height_10"></div>
			<div class="match-nav clearfix">
				<ul class="navlist clearfix fl Js_list_tab">
					<li class="current" ><a class="c" href="javascript:" data-category="1">热门创意交易</a></li>
					<li><a class="c" href="javascript:" data-category="2">正在进行的创意交易</a></li>
				</ul>
				<a class="publish-btn fr" href="{{{$baseURL}}}/exchange/publish">发布交易</a>
			</div>
		</div>
	</div>
	<div class="list_main">
		<ul id="list_content" class="clearfix">
		@include('common.contest-list')
		</ul>
	</div>
	<input type="hidden" value="4" class="Js_list_contest_type" />
</div>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.scrollstop.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.focusMap.js"></script>
	<script type="text/javascript" src="{{{$baseURL}}}/js/list.js"></script>
@stop
