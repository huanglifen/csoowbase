<ul id="list_content" class="clearfix">
@foreach($lists as $trade)
<li>
	<div class="list_block_container">
		<div class="list_block project">
			<div class="block_cover">
				<a class="cover"
					href="{{{$baseURL}}}/exchange/detail/{{{$trade->id}}}"> <img
					class="" src="{{{$baseURL}}}/{{{$trade->cover}}}"
					title="{{{$trade->title}}}">
				</a>
			</div>
			<div class="block_content animate_border_color">
				<a class="headline animate_color"
					href="{{{$baseURL}}}/exchange/detail/{{{$trade->id}}}"
					title="{{{$trade->title}}}">{{{$trade->title}}}</a>
				<div class="tags">
					<?php $tags = json_decode($trade->tags);?>
					@if(!empty($tags))
					@foreach ($tags as $tag)
					<span>{{{$tag}}}</span>
					@endforeach
					@endif
				</div>
			</div>
			<div class="block_bottom animate_border_color clearfix">
				<img class="avatar"
					src="{{{App\Common\Utils::getAvatar($trade->user->avatar)}}}"
					alt="" /> <a class="user animate_color" href="#">{{{$trade->user->name}}}</a>
				<span class="participants">{{{App\Common\Utils::formatDate($trade->start_time)}}}</span>
			</div>
		</div>
	</div>
</li>
@endforeach
</ul>
@if (!empty($pageTotal) && $pageTotal > 1)
		<div class="loadpage"><a class="Js_nextpage loadpage-btn" href="javascript:">加载更多</a></div>
@endif
@if (! isset($page))
    <?php $page = 1?>
@endif
<input type="hidden" value="{{{$page}}}" class="Js_list_page_value" />


@if (!isset($tagId))
    <?php $tagId = 0;?>
@endif
<input type="hidden" value="{{{$tagId}}}" class="Js_list_tag_id" />


@if (! isset($status))
    <?php $status = 0;?>
@endif
<input type="hidden" value="{{{$status}}}" class="Js_list_status_value" />