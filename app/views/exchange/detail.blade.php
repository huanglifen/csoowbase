@extends('common.main')	
@section('title')
{{{$trade->title}}} - 创意世界交易所
@stop
@section('content')	
	<div class="detail-top-wap dworks exchange_dworks">
		<div class="detail-top w1180">
			@include('common.like-report')
			<h1 class="pro-title dworks">{{{$trade->title}}}</h1>
			<div class="work-info">
				<?php $tags = json_decode($trade->tags)?>
				@if(!empty($tags))
				<p>交易标签：
                    @foreach($tags as $tag)
                    <span class="work-tag">{{{$tag}}}</span>
                    @endforeach
				</p>
				@endif
				<p>上传时间：{{{App\Common\Utils::formatDate($trade->start_time)}}}</p>
				@if(in_array($trade->status,array(App\Modules\ItemModule::STATUS_WAITING_CHECK,App\Modules\ItemModule::STATUS_NEED_UPDATE,App\Modules\ItemModule::STATUS_CHECK_AGAIN)))
	               <p>审核中</p>
	            @endif
			</div>
			<div class="pro-btn dworks">
				<em class="yuan">¥</em><span class="num">{{{$trade->price}}}</span><a class="joinbtn" id="Js_buy_btn" href="javascript:;">可购买</a>
			</div>
            <div class="pro-breadcrumb">当前位置：<a href="{{{$baseURL}}}/exchange/index">创意世界交易所</a>&nbsp;/&nbsp;交易详情</div>
		</div>
	</div>
	<div class="content w1180 clearfix">
		<div class="detail-content">
			<div class="height_20"></div>
			<div class="pro-detail show-editor">
				{{$trade->description}}
			</div>
			<?php $creative_index = $trade->creative_index;?>
			@include('common.creative-score')
			<div class="des-line"></div>
			<!-- 专家点评 -->
			@include('common.expert-comments',array('showCommentUrl'=>'','commentBtnTitle'=>'发布专家点评','showCommentInputBox'=>true))
			<div class="des-line"></div>
			<!-- 项目交流 -->
			@include('common.comments')
		</div>
		<div class="detail-aside">
			<div class="pro">
				<div class="pro-content swork">
					<strong>发布者</strong>
					<div class="clearfix content">
						<div class="user-avatar">
							<a href="{{{$baseURL}}}/user/home/{{{$trade->user->id}}}"><img src="{{{App\Common\Utils::getAvatar($trade->user->avatar)}}}" /></a>
						</div>
						<div class="user-info">
							<div class="user-name">
							<a href="{{{$baseURL}}}/user/home/{{{$trade->user->id}}}">	<span>{{{$trade->user->name}}}</span></a>
								<div class="level">
									<div class="icon {{{App\Common\Utils::getLevelStyle($trade->user->creative_index)}}}"></div>
									<div class="bar">
										<div class="process" style="width: 60%;"></div>
									</div>
								</div>
							</div>
							<span class="user-city">{{{$trade->user->city_name}}}</span>
							<div class="user-mutual">
								<div class="clearfix mutual-btn">
								    @if(! $user || $user->id != $trade->user->id)
								    <?php
				                    if(isset($trade->user->is_follow)&& $trade->user->is_follow){
				                    $class = "Js_has_followed_btn";
				                    $followname = '已关注';
			                        }else{
                                    $class = "Js_follow_btn";
                                    $followname = '关注';
                                    }
				                    ?>
									<a href="javascript:;" class="{{{$class}}}" user-id="{{{$trade->user->id}}}">{{{$followname}}}</a>
									<a href="{{{$baseURL}}}/chat/contact/{{{$trade->user->id}}}" target="_blank">聊天</a>
								@endif
								</div>
							</div>
						</div>
						<div class="height_10"></div>
						<div class="submit-time clear">
							<p>提交交易时间：{{{App\Common\Utils::formatDate($trade->start_time)}}}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="{{{$baseURL}}}/js/jquery.rateit.js"></script>
<script type="text/javascript" src="{{{$baseURL}}}/js/detail.js"></script>
<script>
$("#Js_buy_btn").on('click', function() {
	alert('请与发布者联系');
})
</script>
@stop