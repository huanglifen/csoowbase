<ul>
@foreach($chats as $chat)
	@if($chat->user)
	<li class="friendlist">
		<a class="friendlink" href="{{$baseURL}}/user/home/{{$chat->user_id}}">
			<div class="avatar_wraper">
				<img class="ava_img fl" src="{{{App\Common\Utils::getAvatar($chat->user->avatar)}}}"/>

				<div class="tips fl">
					<div class="tipone">
						<span class="name">{{$chat->user->name}}</span>

						<div class="level">
							<div class="icon"></div>
							<div class="bar">
								<div class="process" style="width: 22px;"></div>
							</div>
						</div>
					</div>
					<div class="tiptwo">
						<img class="fl tip_icon" src="{{$baseURL}}/images/icon_placeholder_14x14.png"/>
						<span class="ccznum fl">{{$chat->user->cc_no}}</span>
					</div>
				</div>
			</div>
			<span class="promptnum">99</span>
		</a>
	</li>
	@endif
@endforeach	
	<li class="friendlist checkmore">
		<a href="{{$baseURL}}/chat" class="friend_checkmore">查看更多</a>
	</li>
</ul>