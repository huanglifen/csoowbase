<?php

trait ExpertInvite {

	public function commonColumns($table) {
		$table->increments('id');
		$table->integer('user_id')->unsigned(); //用户ID
		$table->tinyInteger('target_type')->unsigned(); //邀请对象类型，1 创意投资项目比赛 2 创意任务人才比赛 3 创意公益明星比赛 4 创意世界交易所
		$table->integer('target_id')->unsigned(); //邀请对象ID
		$table->integer('target_sub_id')->unsigned(); //邀请子对象ID
		$table->integer('create_time')->unsigned(); //邀请时间
	}

}