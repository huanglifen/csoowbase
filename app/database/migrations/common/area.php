<?php

trait Area {

	public function commonColumns($table) {
		$table->string('name'); //名称
		$table->string('logo'); //标志
		$table->integer('creative_index')->unsigned(); //创意指数
		$table->text('intro'); //简介

		// INDEX
		$table->index('name');
		$table->index('creative_index');
	}

	public function locationNotContainDistrict($table) {
		$table->integer('nation_id')->unsigned(); //国家ID
		$table->integer('province_id')->unsigned(); //省ID
		$table->integer('city_id')->unsigned(); //市ID
		// INDEX
		$table->index('nation_id');
		$table->index('province_id');
		$table->index('city_id');
	}

	public function locationContainDistrict($table) {
		$this->locationNotContainDistrict($table);
		$table->integer('district_id')->unsigned(); //区ID
	}

	public function areaCount($table) {
		$table->integer('contests_count')->unsigned(); //创意世界大赛计数
		$table->integer('trades_count')->unsigned(); //创意交易计数
		$table->integer('experts_count')->unsigned(); //智库专家计数
		$table->integer('users_count')->unsigned(); //注册用户计数
		$table->integer('likes_count')->unsigned(); //喜欢用户计数
	}

}