<?php

trait Item {

	public function commonColumns($table) {
		$table->increments('id'); //比赛/交易ID
		$table->string('no'); //比赛/交易编号
		$table->integer('user_id')->unsigned(); //用户ID
		$table->string('title'); //比赛/交易名称
		$table->longtext('description'); //比赛/交易详细介绍
		$table->string('cover'); //比赛/交易封面
		$table->string('tags'); //标签，数组
		$table->string('check_remark');//审核备注

		$this->locations($table);

		$table->tinyInteger('status'); //比赛/交易状态
		$table->integer('start_time')->unsigned(); //开始时间
		$table->integer('creative_index')->unsigned(); //创意指数
		$table->integer('guess_total_amount')->unsigned(); //奖池总创创币数量
		$table->integer('guess_success_amount')->unsigned(); //猜比赛能成功的创创币数量

		// INDEX
		$table->index(['user_id', 'status']);
		$table->index('status');
	}

	public function locations($table) {
		$table->integer('province_id')->unsigned(); //省ID
		$table->integer('city_id')->unsigned(); //市ID
		$table->integer('district_id')->unsigned(); //区ID
		$table->integer('school_id')->unsigned(); //学校ID
		$table->integer('organization_id')->unsigned(); //政企ID
		$table->index('province_id');
		$table->index('city_id');
		$table->index('district_id');
		$table->index('school_id');
		$table->index('organization_id');
	}

}