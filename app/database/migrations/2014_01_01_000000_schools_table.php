<?php

use Illuminate\Database\Migrations\Migration;

class SchoolsTable extends Migration {
	use Area;

	private $tableName = 'schools';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id'); //学校ID

			$this->commonColumns($table);

			$table->tinyInteger('type')->unsigned(); //学校类型，1 大学 2 中学 3 小学

			$this->locationContainDistrict($table);

			$this->areaCount($table);

			$table->index('type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}