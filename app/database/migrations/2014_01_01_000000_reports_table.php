<?php

use Illuminate\Database\Migrations\Migration;

class ReportsTable extends Migration {

	private $tableName = 'reports';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->tinyInteger('target_type')->unsigned(); //对象类型，1 创意投资项目比赛  21 创意任务人才比赛作品 31 创意公益明星比赛参赛明星作品 4 创意世界交易所 5 创意百科
			$table->integer('target_id')->unsigned(); //对象ID
			$table->tinyInteger('type')->unsigned(); //举报类型，1 广告 2 违法 3 抄袭 4 色情 5 恶意灌水 6其他
			$table->tinyInteger('status'); //状态，0 待确认 1 不属实 2 属实
			$table->integer('cleared_user_id')->unsigned(); //创意指数被清 0 的用户ID
			$table->integer('cleared_creative_index')->unsigned(); //创意指数被清 0 前的值
			$table->integer('create_time')->unsigned(); //创建时间

			// INDEX
			$table->index(['user_id', 'target_type', 'target_id']);
			$table->index('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}