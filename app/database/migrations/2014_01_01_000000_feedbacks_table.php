<?php

use Illuminate\Database\Migrations\Migration;

class FeedbacksTable extends Migration {

	private $tableName = 'feedbacks';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->text('content'); //反馈内容
			$table->string('url');//来源地址
			$table->integer('create_time')->unsigned(); //反馈时间

			// INDEX
			$table->index('user_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}