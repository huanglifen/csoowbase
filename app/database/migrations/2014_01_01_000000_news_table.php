<?php

use Illuminate\Database\Migrations\Migration;

class NewsTable extends Migration {

	private $tableName = 'news';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
        Schema::create($this->tableName, function($table) {
            $table->increments('id');
            $table->string('title');//标题
            $table->text('content')->nullable();//内容
            $table->string('cover');//封面
            $table->smallInteger('type');//新闻类型：1普通，2头条
            $table->smallInteger('category');//新闻分类：1热门，2创意，3政治，4经济，5体育，6军事，7科技
            $table->integer('area_id');//所属地区ID
            $table->string('source');//来源
            $table->string('source_url');//来源链接
	        $table->integer('create_time')->unsigned(); //创建时间

	        //INDEX
	        $table->index('source_url');
	        $table->index('area_id');
	        $table->index(['category','type']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}