<?php

use Illuminate\Database\Migrations\Migration;

class HiringParticipatorsTable extends Migration {

	private $tableName = 'hiring_participators';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('hiring_id')->unsigned(); //比赛ID
			$table->integer('user_id')->unsigned(); //参赛者用户ID
			$table->string('cover'); //作品封面图片
			$table->text('works'); //作品
			$table->text('declaration'); //公益宣言
			$table->tinyInteger('status')->unsigned(); //状态，0 撤回 1 提交 2 获胜
			$table->integer('creative_index')->unsigned(); //创意指数
			$table->string('title'); //明星比赛标题
			
			$table->integer('create_time')->unsigned(); //参赛时间

			// INDEX
			$table->index(['hiring_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}