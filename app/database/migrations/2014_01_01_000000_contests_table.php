<?php

use Illuminate\Database\Migrations\Migration;

class ContestsTable extends Migration {
	use Item;

	private $tableName = 'contests';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$this->commonColumns($table);

			$table->tinyInteger('type')->unsigned(); //比赛类型
			$table->text('info'); //比赛信息，JSON格式
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}