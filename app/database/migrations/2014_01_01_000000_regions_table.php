<?php

use Illuminate\Database\Migrations\Migration;

class RegionsTable extends Migration {
	use Area;

	private $tableName = 'regions';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->integer('id')->unsigned()->primary(); //地区ID

			$this->commonColumns($table);

			$table->tinyInteger('type')->unsigned(); //地区类型，1 国家 2 省 3 市 4 区

			$this->locationNotContainDistrict($table);

			$this->areaCount($table);

			$table->index('type');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}