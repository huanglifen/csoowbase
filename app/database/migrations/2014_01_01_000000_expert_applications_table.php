<?php

use Illuminate\Database\Migrations\Migration;

class ExpertApplicationsTable extends Migration {

	private $tableName = 'expert_applications';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->string('name'); //姓名
			$table->string('id_card_number'); //身份证号码
			$table->string('industries'); //行业，逗号隔开
			$table->string('enterprise_name'); //公司名称
			$table->string('enterprise_duty'); //公司职务
			$table->tinyInteger('organization_id')->unsigned(); //组织，1 创意世界智库主席团，2 创意世界智库理事会，3 创意世界智库专家顾问团
			$table->tinyInteger('duty_id')->unsigned(); //职务，1 主席，2 副主席 | 11 理事长，12 常务理事，13 理事 | 21 专家
			$table->text('attachments'); //证明材料，JSON格式
			$table->text('explain'); //申请说明
			$table->integer('create_time')->unsigned(); //提交时间
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}