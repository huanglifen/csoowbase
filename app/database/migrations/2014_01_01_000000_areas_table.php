<?php

use Illuminate\Database\Migrations\Migration;

class AreasTable extends Migration {

	private $tableName = 'areas';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->tinyInteger('item_type')->unsigned(); //类型，1 地区 2 学校 3 行政机构 4企业
			$table->integer('item_id')->unsigned(); //地区/学校/政企ID
			$table->string('name'); //地区/学校/政企名称
			$table->string('logo'); //标志

			// INDEX
			$table->index('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}