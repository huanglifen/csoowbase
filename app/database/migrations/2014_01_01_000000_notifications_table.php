<?php

use Illuminate\Database\Migrations\Migration;

class NotificationsTable extends Migration {

	private $tableName = 'notifications';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->text('content'); //消息内容
			$table->tinyInteger('target_type')->unsigned();//消息对象的类型
			$table->string('info');//附加信息，如比赛ID
			$table->tinyInteger('status')->unsigned(); //状态，0 未读，1 已读
			$table->integer('create_time')->unsigned(); //创建时间

			// INDEX
			$table->index(['user_id', 'status']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}