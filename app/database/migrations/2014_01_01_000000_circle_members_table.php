<?php

use Illuminate\Database\Migrations\Migration;

class CircleMembersTable extends Migration {

	private $tableName = 'circle_members';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');

			$table->integer('circle_id')->unsigned(); //圈子ID
			$table->integer('circle_owner_user_id')->unsigned(); //圈子拥有者用户ID
			$table->integer('user_id')->unsigned(); //成员ID

			$table->integer('create_time')->unsigned(); //创建时间

			// INDEX
			$table->index('circle_id');
			$table->index('user_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}