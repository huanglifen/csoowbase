<?php

use Illuminate\Database\Migrations\Migration;

class ProjectInvestmentsTable extends Migration {

	private $tableName = 'project_investments';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('project_id')->unsigned(); //比赛ID
			$table->integer('user_id')->unsigned(); //用户ID
			$table->integer('amount')->unsigned(); //投资金额
			$table->tinyInteger('type')->unsigned(); //投资方式，1 一次性付清 2 先付定金再付尾款
			$table->integer('deposit_percentage')->unsigned(); //定金支付比例，100 为 一次性付清
			$table->integer('final_payment_pay_time')->unsigned(); //支付尾款时间
			$table->integer('require_complete_time')->unsigned(); //要求项目完成期限
			$table->text('requirement'); //投资要求
			$table->tinyInteger('status')->unsigned(); //投资状态，0 撤回 1 申请 2 已接受
			$table->integer('accept_time')->unsigned(); //接受时间，状态为已接受时有效
			$table->integer('create_time')->unsigned(); //创建时间

			// INDEX
			$table->index(['project_id', 'user_id']);
			$table->index(['project_id', 'status']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}