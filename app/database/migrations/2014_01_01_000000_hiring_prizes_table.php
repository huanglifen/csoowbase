<?php

use Illuminate\Database\Migrations\Migration;

class HiringPrizesTable extends Migration {

	private $tableName = 'hiring_prizes';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('hiring_id')->unsigned(); //比赛ID
			$table->string('title'); //头衔
			$table->integer('bonus_amount')->unsigned(); //奖金
			$table->text('award'); //奖品
			$table->integer('winner_user_id')->unsigned(); //获奖明星用户ID
			$table->integer('winner_participator_id')->unsigned(); //获奖明星参赛者ID
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}