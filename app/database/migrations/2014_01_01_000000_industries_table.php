<?php

use Illuminate\Database\Migrations\Migration;

class IndustriesTable extends Migration {
	use Item;

	private $tableName = 'industries';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->string('title'); //行业名称
			$table->integer('class_id')->unsigned(); //行业归属的大类，本身是大类填 0
			$table->string('description'); //行业简介
			$table->boolean('is_recommended'); //是否推荐

			// INDEX
			$table->index('class_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}