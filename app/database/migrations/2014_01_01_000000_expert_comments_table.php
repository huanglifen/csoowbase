<?php

use Illuminate\Database\Migrations\Migration;

class ExpertCommentsTable extends Migration {

	private $tableName = 'expert_comments';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //专家ID
			$table->tinyInteger('target_type')->unsigned(); //对象类型，1 创意投资项目比赛 2 创意任务人才比赛 3 创意公益明星比赛 4 创意世界交易所 5 创意百科
			$table->integer('target_id')->unsigned(); //对象ID
			$table->integer('target_sub_id')->unsigned(); //子对象ID
			$table->text('score'); //点评评价，1/2/3
			$table->text('content'); //点评评语

			$table->integer('create_time')->unsigned(); //点评时间

			// INDEX
			$table->index(['target_type', 'target_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}