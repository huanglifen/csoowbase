<?php

use Illuminate\Database\Migrations\Migration;

class TaskWorksTable extends Migration {

	private $tableName = 'task_works';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('task_id')->unsigned(); //比赛ID
			$table->integer('user_id')->unsigned(); //用户ID
			$table->string('cover'); //作品封面图片
			$table->text('works'); //作品
			$table->integer('promise_complete_time')->unsigned(); //承诺完成任务的时间，0 不承诺
			$table->tinyInteger('status')->unsigned(); //状态，0 撤回 1 提交 2 获胜
			$table->integer('creative_index')->unsigned(); //创意指数
			$table->integer('winning_time')->unsigned(); //获胜时间，状态为获胜时有效
			$table->integer('create_time')->unsigned(); //创建时间
			$table->string('title'); //人才比赛title
			// INDEX
			$table->index(['task_id', 'user_id']);
			$table->index(['task_id', 'status']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}