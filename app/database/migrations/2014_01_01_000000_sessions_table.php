<?php

use Illuminate\Database\Migrations\Migration;

class SessionsTable extends Migration {

	private $tableName = 'sessions';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->string('id')->primary();
			$table->text('payload');
			$table->integer('last_activity')->unsigned();

			//INDEX
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}