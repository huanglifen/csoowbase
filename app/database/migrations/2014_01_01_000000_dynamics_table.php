<?php

use Illuminate\Database\Migrations\Migration;

class DynamicsTable extends Migration {

	private $tableName = 'dynamics';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->text('content'); //内容
			$table->text('pic'); //图片
			$table->text('video'); //视频
            $table->tinyInteger('type')->unsigned(); //类型 1 原创 2 转发 11 关注 12 发布 13 参与 14 发布动态  15评论  16喜欢 21 专家点评 22成为 23 加入
            $table->tinyInteger('target_type')->unsigned(); //对象类型，1 创意投资项目比赛 2 创意任务人才比赛 3 创意公益明星比赛  4创意世界交易所  21任务作品  31明星作品  5百科  6用户 71地区 72学校 73政企  8智库
            $table->integer('target_id')->unsigned(); //对象ID
            $table->integer('like_count')->unsigned(); //赞数目
            $table->integer('forward_count')->unsigned(); //转发数目
            $table->integer('comment_count')->unsigned(); //评论数目
			$table->integer('create_time')->unsigned(); //发布时间

			// INDEX
			$table->index(['user_id', 'type']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}