<?php

use Illuminate\Database\Migrations\Migration;

class EncyclopediasTable extends Migration {
	use Item;

	private $tableName = 'encyclopedias';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->tinyInteger('item_type')->unsigned(); //百科类型，1 网站采集的创意 2 用户分享的创意 3 发布的创意交易 4 创意投资项目比赛 21 创意任务人才比赛作品 31 创意公益明星比赛获作品
			$table->integer('item_id')->unsigned(); //百科ID
			$table->string('title'); //百科名称
			$table->string('cover'); //封面
			$table->longtext('content'); //内容
			$table->integer('industry_id')->unsigned(); //行业
			$table->string('tags'); //标签，数组
			$table->string('check_remark');//审核备注

			$this->locations($table);

			$table->integer('status'); //发布状态
			$table->integer('creative_index'); //创意指数
			$table->integer('create_time')->unsigned(); //发布时间

			// INDEX
			$table->index('title');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}