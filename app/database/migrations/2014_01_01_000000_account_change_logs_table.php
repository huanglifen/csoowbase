<?php

use Illuminate\Database\Migrations\Migration;

class AccountChangeLogsTable extends Migration {

	private $tableName = 'account_change_logs';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->tinyInteger('account_type')->unsigned(); //帐户类型，1 创意指数 2 创创币
			$table->decimal('amount', 20, 2); //帐户变更数额，正负表示加减
			$table->decimal('balance', 20, 2); //帐户变更后的余额
			$table->tinyInteger('reason'); //帐户变更原因 1:用户评分 2：专家点评
			$table->text('related_info'); //相关信息
			$table->integer('create_time')->unsigned(); //创建时间

			// INDEX
			$table->index(['user_id', 'account_type']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}