<?php

use Illuminate\Database\Migrations\Migration;

class CirclesTable extends Migration {

	private $tableName = 'circles';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');

			$table->integer('user_id')->unsigned(); //用户ID
			$table->string('name'); //圈子名称
			$table->integer('member_count')->unsigned(); //成员计数

			$table->integer('create_time')->unsigned(); //创建时间
			$table->boolean('is_default'); //是否为默认圈子

			// INDEX
			$table->index('user_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}