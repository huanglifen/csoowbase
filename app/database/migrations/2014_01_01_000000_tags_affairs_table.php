<?php

use Illuminate\Database\Migrations\Migration;

class TagsAffairsTable extends Migration {

	private $tableName = 'tags_affairs';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('tag_id')->unsigned(); //标签ID
			$table->integer('affair_type')->unsigned(); //事务类型
			$table->integer('affair_id')->unsigned(); //事务ID

			// INDEX
			$table->index(['tag_id', 'affair_type']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}