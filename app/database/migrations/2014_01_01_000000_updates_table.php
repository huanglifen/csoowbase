<?php

use Illuminate\Database\Migrations\Migration;

class UpdatesTable extends Migration {

	private $tableName = 'updates';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->tinyInteger('target_type')->unsigned(); //更新对象类型，1 创意投资项目比赛 2 创意任务人才比赛 3 创意公益明星比赛 4 创意世界交易所
			$table->integer('target_id')->unsigned(); //更新对象ID
			$table->string('title'); //标题
			$table->text('content'); //内容
			$table->integer('create_time')->unsigned(); //创建时间

			// INDEX
			$table->index(['target_type', 'target_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}