<?php

use Illuminate\Database\Migrations\Migration;

class TagsTable extends Migration {

	private $tableName = 'tags';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->string('name', 32); //标签名
			$table->integer('type')->unsigned(); //标签归属类型

			//INDEX
			$table->index(array('type','name'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}