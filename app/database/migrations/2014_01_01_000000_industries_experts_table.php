<?php

use Illuminate\Database\Migrations\Migration;

class IndustriesExpertsTable extends Migration {
	use Item;

	private $tableName = 'industries_experts';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('industry_id')->unsigned(); //1级行业
			$table->integer('industry_sub_id')->unsigned(); //2级行业
			$table->integer('user_id')->unsigned(); //用户ID

			// INDEX
			$table->index(['industry_id', 'user_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}