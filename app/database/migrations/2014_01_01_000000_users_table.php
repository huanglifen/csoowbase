<?php

use Illuminate\Database\Migrations\Migration;

class UsersTable extends Migration {

	private $tableName = 'users';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->string('name'); //用户名
			$table->integer('cc_no')->unsigned(); //创创号
			$table->string('email'); //邮箱
			$table->boolean('email_is_verified'); //邮箱是否验证，0 未验证 1 已验证
			$table->string('email_verify_code'); //邮箱验证码
			$table->string('password'); //密码
			$table->string('password_reset_code'); //密码重设码
			$table->string('activation_code'); //激活码
			$table->tinyInteger('group_id')->unsigned(); //组ID，0 未激活，1 普通用户 2 专家 10管理员
			$table->string('avatar'); //头像

			$table->integer('creative_index')->unsigned(); //创意指数
			$table->integer('coin')->unsigned(); //创创币
			$table->integer('creative_index_bonus_amount')->unsigned(); //未领取的创意指数奖励

			$table->tinyInteger('gender')->unsigned(); //性别，0 未知 1 女 2 男
			$table->integer('birthday'); //出生日期，timestamp
			$table->text('introduction'); //个人介绍
			$table->text('tags'); //个人标签
			$table->string('mobile_phone_no'); //手机号码
			$table->boolean('mobile_phone_is_verified'); //手机是否验证，0 未验证 1 已验证
			$table->string('mobile_phone_verify_code'); //手机验证码
			$table->integer('nation_id')->unsigned(); //国家ID
			$table->integer('province_id')->unsigned(); //省ID
			$table->integer('city_id')->unsigned(); //市ID
			$table->integer('district_id')->unsigned(); //区ID
			$table->integer('school_id')->unsigned(); //学校ID
			$table->integer('organization_id')->unsigned(); //政企ID

			$table->integer('expert_organization_id')->unsigned(); //专家组织，1 创意世界智库主席团 2 创意世界智库理事会 3 创意世界智库专家顾问团
			$table->integer('expert_duty_id')->unsigned(); //专家职务，11 主席 12 副主席 21 理事长 22 常务理事 23 理事
			$table->integer('expert_field_id')->unsigned(); //专家领域，1 领导人 2 科学家 3 艺术家 4 企业家 5 慈善家 6 其它
			$table->string('expert_industries'); //专家行业，可多个
			$table->string('expert_title'); //专家头衔

			$table->integer('follower_count')->unsigned(); //关注者数目
			$table->integer('contest_publish_count')->unsigned(); //发布比赛数目
			$table->integer('trade_publish_count')->unsigned(); //发布交易数目

			$table->integer('register_time')->unsigned(); //注册时间, timestamp
			$table->string('register_ip'); //注册IP地址

			$table->text('info'); //其它信息，JSON格式，

			// INDEX
			$table->index('name');
			$table->index('cc_no');
			$table->index('email');
			$table->index('password_reset_code');
			$table->index('creative_index');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}