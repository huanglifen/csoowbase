<?php

use Illuminate\Database\Migrations\Migration;

class ExpertInvitesExternalTable extends Migration {
	use ExpertInvite;

	private $tableName = 'expert_invites_external';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$this->commonColumns($table);

			$table->string('name'); //姓名
			$table->string('industry'); //行业
			$table->string('email'); //邮箱地址
			$table->string('mobile_phone_no'); //手机号码
			$table->string('contact_info'); //联系方式
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}