<?php

use Illuminate\Database\Migrations\Migration;

class ExpertInvitesInternalTable extends Migration {
	use ExpertInvite;

	private $tableName = 'expert_invites_internal';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$this->commonColumns($table);

			$table->integer('expert_user_id')->unsigned(); //专家用户ID
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}