<?php

use Illuminate\Database\Migrations\Migration;

class CommentsTable extends Migration {

	private $tableName = 'comments';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->tinyInteger('target_type')->unsigned(); //对象类型，1 创意投资项目比赛 2 创意任务人才比赛 3 创意公益明星比赛 4 创意世界交易所 5 创意百科  71地区 72学校 73政企
			$table->integer('target_id')->unsigned(); //对象ID
			$table->text('content'); //评论内容
			$table->integer('create_time')->unsigned(); //创建时间

			// INDEX
			$table->index(['target_type', 'target_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}