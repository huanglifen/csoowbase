<?php

use Illuminate\Database\Migrations\Migration;

class ExchangeTradesTable extends Migration {
	use Item;

	private $tableName = 'exchange_trades';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$this->commonColumns($table);

			$table->integer('price')->unsigned(); //售价
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}