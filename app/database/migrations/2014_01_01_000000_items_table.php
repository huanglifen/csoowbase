<?php

use Illuminate\Database\Migrations\Migration;

class ItemsTable extends Migration {

	private $tableName = 'items';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
            $table->integer('user_id')->unsigned(); //用户ID
            $table->tinyInteger('action_type')->unsigned(); // 1 发布 2 参与 3 喜欢
            $table->tinyInteger('target_type')->unsigned(); //对象类型，1 创意投资项目比赛 2 创意任务人才比赛 3 创意公益明星比赛 4 创意世界交易所 21 任务作品 31 明星作品 5 创意作品(百科) 61 动态  71地区 72学校  73政企
			$table->integer('target_id')->unsigned(); //对象ID
			$table->string('title'); //比赛/交易名称
            $table->integer('create_time')->unsigned(); //发生时间

			// INDEX
			$table->index('title');
			$table->index(['user_id', 'action_type','target_type','target_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}