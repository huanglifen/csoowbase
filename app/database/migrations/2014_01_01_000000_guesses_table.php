<?php

use Illuminate\Database\Migrations\Migration;

class GuessesTable extends Migration {

	private $tableName = 'guesses';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->tinyInteger('target_type')->unsigned(); //对象类型，1 创意投资项目比赛 2 创意任务人才比赛 3 创意公益明星比赛
			$table->integer('target_id')->unsigned(); //对象ID
			$table->tinyInteger('item')->unsigned(); //竞猜项，0 不能成功 1 能成功
			$table->integer('amount')->unsigned(); //竞猜创创币数量
			$table->integer('create_time')->unsigned(); //竞猜时间

			// INDEX
			$table->index(['target_type', 'target_id', 'item']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}