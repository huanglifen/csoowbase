<?php

use Illuminate\Database\Migrations\Migration;

class CreativeValueRatingsTable extends Migration {

	private $tableName = 'creative_value_ratings';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //用户ID
			$table->tinyInteger('target_type')->unsigned(); //对象类型，1 创意投资项目比赛 21 创意任务人才比赛作品 31 创意公益明星比赛参赛明星作品 4 创意世界交易所交易 5 创意百科
			$table->integer('target_id')->unsigned(); //对象ID
			$table->tinyInteger('score')->unsigned(); //评分，1 - 5 星
			$table->integer('create_time')->unsigned(); //创建时间

			// INDEX
			$table->index(['user_id','target_type', 'target_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}