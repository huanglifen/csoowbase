<?php

use Illuminate\Database\Migrations\Migration;

class ChatsTable extends Migration {

	private $tableName = 'chats';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id');
			$table->string('session_id'); //会话ID
			$table->integer('user_id')->unsigned(); //发送用户ID
			$table->integer('to_user_id')->unsigned(); //接收用户ID
			$table->text('content'); //消息内容
			$table->tinyInteger('status')->unsigned(); //状态，0 未读，1 已读
			$table->integer('create_time')->unsigned(); //创建时间

			// INDEX
			$table->index('session_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}