<?php

use Illuminate\Database\Migrations\Migration;

class OrganizationsTable extends Migration {
	use Area;

	private $tableName = 'organizations';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->tableName, function ($table) {
			$table->increments('id'); //政企ID

			$this->commonColumns($table);

			$table->tinyInteger('type')->unsigned(); //类型，1行政机构 2 企业 

			$this->locationContainDistrict($table);

			$this->areaCount($table);

			$table->index('type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->tableName);
	}

}