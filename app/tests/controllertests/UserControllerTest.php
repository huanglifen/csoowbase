<?php

class UserControllerTest extends BaseControllerTest {

	public function setUp() {
		parent::setUp();
		DB::table('users')->truncate();
		DB::table('likes')->truncate();
	}

	public function testPostRegister() {
		$url = '/user/register';

		$tests = [
		'' => '{"status":"param_error","error":{"email":"\u8bf7\u586b\u5199\u90ae\u7bb1","name":"\u8bf7\u586b\u5199\u6635\u79f0","password":"\u8bf7\u8f93\u5165\u5bc6\u7801","password_again":"\u8bf7\u518d\u6b21\u8f93\u5165\u5bc6\u7801","verify_code":"\u8bf7\u8f93\u5165\u9a8c\u8bc1\u7801","is_agree_protocol":"\u60a8\u8fd8\u672a\u63a5\u53d7\u300a\u521b\u610f\u4e16\u754c\u7528\u6237\u6ce8\u518c\u534f\u8bae\u300b\u53ca\u300a\u7248\u6743\u58f0\u660e\u300b"}}'
		];
		$this->doPost($url, $tests);
		Session::set('verify_code', 'S48i');
		$tests = [
			'{"email":"leon@leon.com","name":"leon","is_agree_protocol":1}' => '{"status":"param_error","error":{"password":"\u8bf7\u8f93\u5165\u5bc6\u7801","password_again":"\u8bf7\u518d\u6b21\u8f93\u5165\u5bc6\u7801","verify_code":"\u8bf7\u8f93\u5165\u9a8c\u8bc1\u7801"}}',
			'{"email":"leon@leon.com","name":"leon","password":"superstar","is_agree_protocol":1}' => '{"status":"param_error","error":{"password_again":"\u4e24\u6b21\u5bc6\u7801\u4e0d\u4e00\u81f4","verify_code":"\u8bf7\u8f93\u5165\u9a8c\u8bc1\u7801"}}',
			'{"email":"leon@leon.com","name":"leon","password":"superstar","password_again":"superstar","verify_code":"SDD8","is_agree_protocol":1}' => '{"status":"param_error","error":{"verify_code":"\u9a8c\u8bc1\u7801\u9519\u8bef"}}',
			'{"email":"leon@leon.com","name":"leon","password":"superstar","password_again":"superstar","verify_code":"S48i","is_agree_protocol":1}' => '{"status":"ok"}',
		];


		$this->doPost($url, $tests);
	}

	public function testPostActivateAccount() {
	    $url = '/user/activate-account';
		$this->prepareUserData();
	    $tests = [
	        '' => '{"status":"param_error","error":{"user_id":"\u8bf7\u586b\u5199\u7528\u6237ID"}}',
	        '{"user_id":1}' => '{"status":"ok"}',
	    ];
	    $this->doPost($url, $tests);

	}
	public function testGetActivateAccountSuccess() {
	    $url = '/user/activate-account-success/1/IVUUZ-HK71U-WEX4M-KYR54-0Z47M';
		$this->prepareUserData();
	    $result = $this->call('GET',$url);
	    $this->assertResponseOk();
	}
	public function testPostLogin() {
		$url = '/user/login';

		$this->prepareUserData();
		$tests = [
			'' => '{"status":"param_error","error":{"login_name":"\u8bf7\u586b\u5199\u90ae\u7bb1\/\u521b\u521b\u53f7\/\u6635\u79f0","password":"\u8bf7\u8f93\u5165\u5bc6\u7801"}}',
		    '{"login_name":"leon1","password":"asdf"}' => '{"status":"ok"}'
		];

		$this->doPost($url, $tests);
	}

	public function testPostVerifyEmail() {
	    $url = '/user/verify-email';
		$this->prepareUserData();
	    $tests = [
	        '' => '{"status":"param_error","error":{"email":"\u8bf7\u586b\u5199\u90ae\u7bb1","verify_code":"\u8bf7\u8f93\u5165\u9a8c\u8bc1\u7801"}}',
	    ];

	    $this->doPost($url, $tests);

	    Session::set('verify_code', 'S48i');
	    $tests = [
	        '{"email":"12345","verify_code":"S48i"}' => '{"status":"param_error","error":{"email":"\u8bf7\u586b\u5199\u6b63\u786e\u7684\u90ae\u7bb1"}}',
	        '{"email":"514128561@qq.com","verify_code":"S48i"}' => ''
	    ];

	    $this->doPost($url, $tests);
	}

	public function testPostFindPassword() {
		$url = '/user/find-password';

		$this->prepareUserData();
		$tests = [
			'{"email":"529909847@qq.com"}' => '{"status":"ok"}'
		];

		$this->doPost($url, $tests);
	}

	public function testGetResetPassword() {
	    $url = '/user/reset-password/1/IVUUZ-HK71U-WEX4M-KYR54-0Z47M';
		$this->prepareUserData();
	    $result = $this->call('GET',$url);
	    $this->assertResponseOk();
	}

	public function testPostResetPassword() {
		$url = '/user/reset-password';

		$this->prepareUserData();

		$tests = [
			'' => '{"status":"param_error","error":{"user_id":"\u8bf7\u586b\u5199\u7528\u6237ID","password_reset_code":"\u8bf7\u586b\u5199\u5bc6\u7801\u91cd\u8bbe\u7801","password":"\u8bf7\u8f93\u5165\u5bc6\u7801","password_again":"\u8bf7\u518d\u6b21\u8f93\u5165\u5bc6\u7801"}}',
		    '{"user_id":1,"password_reset_code":"IVUUZ-HK71U-WEX4M-KYR54-0Z47M","password":"123456"}' => '{"status":"param_error","error":{"password_again":"\u4e24\u6b21\u5bc6\u7801\u4e0d\u4e00\u81f4"}}',
		    '{"user_id":2,"password_reset_code":"IVUUZ-HK71U-WEX4M-KYR54-0Z47M","password":"123456","password_again":"123456"}' => '{"status":"ok","content":false}',
		    '{"user_id":2,"password_reset_code":"IVUUZ-HK71U-WEX4M-KYR54-0AR43","password":"123456","password_again":"123456"}' => '{"status":"ok","content":false}',
		    '{"user_id":1,"password_reset_code":"IVUUZ-HK71U-WEX4M-KYR54-0Z47M","password":"123456","password_again":"123456"}' => '{"status":"ok","content":true}'
		];

		$this->doPost($url, $tests);
	}

	protected function prepareUserData(){

        $data = [
			[
				'name' => 'leon1',
				'cc_no' => 100001,
				'email' => '514128561@qq.com',
				'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
				'password_reset_code' => 'IVUUZ-HK71U-WEX4M-KYR54-0Z47M',
				'activation_code' => 'IVUUZ-HK71U-WEX4M-KYR54-0Z47M',
				'group_id' => 1,
				'avatar' => '',
				'creative_index' => 0,
				'coin' => 0,
				'creative_index_bonus_amount'=>0,
				'gender' => 0,
				'birthday' => 0,
				'introduction' => 0,
				'tags'=>'tags,php,jquery',
				'mobile_phone_no'=>'12345678911',
				'province_id'=>'1',
				'city_id'=>'1',
				'district_id'=>'1',
				'school_id'=>'1',
				'organization_id'=>'1',
				'expert_organization_id'=>'1',
				'expert_duty_id'=>'1',
				'expert_industry_id'=>'1',
				'expert_title'=>'专家头衔--主席',
				'follower_count'=>'100',
				'liker_count'=>'500',
				'register_time'=>time(),
				'register_ip'=>'127.0.0.1',
			]
		];

		DB::table('users')->insert($data);

	}

}