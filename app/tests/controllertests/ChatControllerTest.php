<?php

use App\Modules\ChatModule;
class ChatControllerTest extends BaseControllerTest{ 

    public function __construct(){ 
        parent::__construct();
        include_once 'app/tests/controllertests/ExchangeControllerTest.php';
        include_once 'app/tests/moduletests/ExchangeModuleTest.php';
    }
    public function setUp() {
        parent::setUp();
        DB::table('circles')->truncate();
        DB::table('circle_members')->truncate();
        DB::table('users')->truncate();
        DB::table('chats')->truncate();
        Session::start();
    }
    
    public function testGetCczChat() {
        $this->prepareUsers();
        $this->prepareChat();
        $url = '/chat/ccz-chat';
        
        //case 1
        $response = $this->call("GET", $url);
        $this->assertEquals('{"status":"not_login"}', $response->getContent());
        
        //case 2
        Session::set('user_id', 1);
        $response = $this->call("GET", $url);
        $this->assertResponseOk();
        
        $result = json_decode($response->getContent(), true);
        $content = $result['content'];
        $expect = array(1, 1);
        $actual = array();
        foreach($content as $c) {
            $actual[] = $c['num'];
        }
        
        $this->assertEquals($expect, $actual);
    }

    public function testGetIndex() {
        $this->prepareUsers();
        $this->prepareChat();
        $url = '/chat';
        
        //case 1
        $response = $this->call("GET", $url);
        $this->assertRedirectedTo('/user/login');
        //case 2
        Session::set('user_id', 1);
        $response = $this->call('GET', $url);
        $this->assertResponseOk();
        $this->assertViewHas('notifications');
        
        $view = $response->original;
        $notifications = $view['notifications'];
        
        foreach($notifications as $notification) {
            $this->assertObjectHasAttribute('user',$notification);
        }
    }
 
    public function testGetMessage() {
        $this->prepareUsers();
        $this->prepareChat();
        $url = '/chat/message';
        
        //case 1
        $response = $this->call("GET", $url);
        $this->assertEquals('{"status":"not_login"}', $response->getContent());
        
        //case 2
        Session::set('user_id', 1);
        $inputs = array(
            'type' => ChatModule::STATUS_UNREAD,
            'page' => 1,
        );
        
        $response = $this->call("GET", $url, $inputs);
        $this->assertResponseOk();
        $this->assertViewHas('notifications');
        
    }

    public function testGetContact() {
        $this->prepareData();
        $url = '/chat/contact';
        //case 1
        $response = $this->call("GET", $url);
        $this->assertRedirectedTo('/user/login');
        //case 2
        Session::set('user_id', 1);
        $response = $this->call('GET', $url);
        $this->assertResponseOk();
        
        $this->assertViewHas('members');
        $this->assertViewHas('circles');
        $this->assertViewHas('chats');
        
        //case 3
        Session::set('user_id', 1);
        $response = $this->call('GET', $url."/2");
        $this->assertResponseOk();
    }

    public function testGetContactInner() {
        $this->prepareData();
        $url = '/chat/contact-inner';
        //case 1
        $response = $this->call("GET", $url);
        $this->assertEquals('{"status":"not_login"}', $response->getContent());
        //case 2
        Session::set('user_id', 1);
        $inputs = array(
            'to_user_id' => 2
        );
        $response = $this->call('GET', $url, $inputs);
        $this->assertResponseOk();
        
        $this->assertViewHas('chats');
    }

    public function testGetCircleMembers() {
        $this->prepareUsers();
        $this->prepareCircleData();
        $this->prepareCircleMemberData();
        $this->prepareChat();
        
        Session::set('user_id', 1);
        $url = '/chat/circle-members';
        
        //case 1
        $inputs = array(
            'circle_id' => 1
        );
        
        $response = $this->call('GET', $url, $inputs);
        $this->assertResponseOk();
        $result = json_decode($response->getContent(), true);
        $members = $result['content'];

        $expect = array(2, 3);
        $actual = array();
        foreach($members as $m) {
            $actual[] = $m['id'];
        }
        $this->assertEquals($expect, $actual);
        
        //case 2
        $inputs = array(
            'circle_id' => 0
        );
        $response = $this->call('GET', $url, $inputs);
        $this->assertResponseOk();
        $result = json_decode($response->getContent(), true);
        $members = $result['content'];
        $expect = array(4);
        $actual = array();
        foreach($members as $m) {
            $actual[] = $m['id'];
        }
        $this->assertEquals($expect, $actual);
        
        //case 3
        $inputs = array(
                'circle_id' => -1
        );
        $response = $this->call('GET', $url, $inputs);
        $this->assertResponseOk();
        $result = json_decode($response->getContent(), true);
        $members = $result['content'];
        $expect = array(2,3, 4);
        $actual = array();
        foreach($members as $m) {
            $actual[] = $m['id'];
        }
        $this->assertEquals($expect, $actual);
    }
  
    public function testPostSendMessage() {
        Session::clear();
        $url = '/chat/send-message';
        //case 1
        $tests = [
        '' => '{"status":"not_login"}'
        ];
        $this->doPost($url, $tests);
        
        //case 2
        Session::set('user_id', 1);
        $content = '';
        for($i=0; $i<=1000; $i++) {
            $content .='哈';
        }
        $tests = [
        '{"to_user_id":"haha","content":"'.$content.'"}' => '{"status":"param_error","error":{"to_user_id":"\u8bf7\u586b\u5199common.to_user_id","content":"\u6d88\u606f\u5185\u5bb9\u4e0d\u80fd\u8d85\u8fc71000\u5b57"}}',
        '{"to_user_id":1,"content":"haha"}' => '{"status":"ok","content":1}',
        ];
        $this->doPost($url, $tests);
        
    }
    
    public function testGetChat() {
        $this->prepareChat();
        $url = '/chat/chat';
        
        //case 1
        $response = $this->call("GET", $url);
        $this->assertEquals('{"status":"not_login"}', $response->getContent());
        //case 2
        Session::set('user_id', 1);
        $inputs = array();
        $response = $this->call('GET', $url, $inputs);
        $result = $response->getContent();
        $this->assertEquals('{"status":"param_error","error":{"to_user_id":"\u8bf7\u586b\u5199common.to_user_id","top_chat_id":"\u8bf7\u586b\u5199common.top_chat_id"}}', $result);
        //case 3
        $inputs = array(
          'to_user_id' => 2,
            'top_chat_id'  => 4, 
        );
        
        $response = $this->call('GET', $url, $inputs);
        $result = json_decode($response->getContent(), true);
        
        $content = $result['content'];
        
        $expect = array(2, 1);
        $actual = array();
        foreach($content as $c) {
            $actual[] = $c['id'];
        }
        $this->assertEquals($expect, $actual);
    }

    public function testGetCurrentChat() {
        $this->prepareChat();
        $this->prepareNewChat();
        $url = '/chat/current-chat';
        
        //case 1
        $response = $this->call("GET", $url);
        $this->assertEquals('{"status":"not_login"}', $response->getContent());
        
        //case 2
        Session::set('user_id', 1);
        
        $inputs = array(
                'send_user_id' => 2,
                'last_chat_id'  => 4,
        );
        $response = $this->call("GET", $url, $inputs);
        $result = json_decode($response->getContent(), true);
        
        $content = $result['content'];
        
        $expect = array(7);
        $actual = array();
        foreach($content as $c) {
            $actual[] = $c['id'];
        }
        $this->assertEquals($expect, $actual);
    }

    public function prepareNewChat() {
        $inserts = array(
                array(
                        'session_id' => '1-2',
                        'user_id' => 2,
                        'to_user_id' => 1,
                        'content' => 'content',
                        'status' => ChatModule::STATUS_UNREAD,
                        'create_time' => time()
                ),
            );

        DB::table('chats')->insert($inserts);
    }
    public function prepareData() {
        $this->prepareUsers();
        $this->prepareCircleData();
        $this->prepareCircleMemberData();
        $this->prepareChat();
    }
    public function prepareChat() {
        $inserts = array(
                array(
                        'session_id' => '1-2',
                        'user_id' => 2,
                        'to_user_id' => 1,
                        'content' => 'content',
                        'status' => ChatModule::STATUS_UNREAD,
                        'create_time' => time()
                ),
                array(
                        'session_id' => '1-2',
                        'user_id' => 2,
                        'to_user_id' => 1,
                        'content' => 'content',
                        'status' => ChatModule::STATUS_READED,
                        'create_time' => time()
                ),
                array(
                        'session_id' => '1-3',
                        'user_id' => 3,
                        'to_user_id' => 1,
                        'content' => 'content',
                        'status' => ChatModule::STATUS_UNREAD,
                        'create_time' => time()
                ),
                array(
                        'session_id' => '1-2',
                        'user_id' => 1,
                        'to_user_id' => 2,
                        'content' => 'content',
                        'status' => ChatModule::STATUS_UNREAD,
                        'create_time' => time()
                ),
                array(
                        'session_id' => '1-4',
                        'user_id' => 4,
                        'to_user_id' => 1,
                        'content' => 'content',
                        'status' => ChatModule::STATUS_READED,
                        'create_time' => time()
                ), array(
                        'session_id' => '1-4',
                        'user_id' => 4,
                        'to_user_id' => 1,
                        'content' => 'content',
                        'status' => ChatModule::STATUS_READED_ON_LINE,
                        'create_time' => time()
                )
        );
    
        DB::table('chats')->insert($inserts);
    }
    protected function prepareCircleData() {
        $circles = array(
                array(
                        'user_id' => 1,
                        'name' => '亲人',
                        'member_count' => 2,
                        'create_time' => time()
                ),
                array(
                        'user_id' => 2,
                        'name' => '朋友',
                        'member_count' => 1,
                        'create_time' => time()
                ),
                array(
                        'user_id' => 1,
                        'name' => '家人',
                        'member_count' => 2,
                        'create_time' => time()
                ),array(
                        'user_id' => 5,
                        'name' => '朋友',
                        'member_count' => 1,
                        'create_time' => time()
                ),array(
                        'user_id' => 2,
                        'name' => '家人',
                        'member_count' => 1,
                        'create_time' => time()
                )
        );
        DB::table("circles")->insert($circles);
    }
    
    protected function prepareCircleMemberData() {
        $members = array(
                array(
                        'circle_id' => 1,
                        'circle_owner_user_id' => 1,
                        'user_id' => 2,
                        'create_time' => time()
                ),
                array(
                        'circle_id' => 1,
                        'circle_owner_user_id' => 1,
                        'user_id' => 3,
                        'create_time' => time()
                ),
                array(
                        'circle_id' => 2,
                        'circle_owner_user_id' => 2,
                        'user_id' => 1,
                        'create_time' => time()
                ),
                array(
                        'circle_id' => 3,
                        'circle_owner_user_id' => 1,
                        'user_id' => 3,
                        'create_time' => time()
                ),array(
                        'circle_id' => 4,
                        'circle_owner_user_id' => 5,
                        'user_id' => 1,
                        'create_time' => time()
                ),array(
                        'circle_id' => 5,
                        'circle_owner_user_id' => 2,
                        'user_id' => 1,
                        'create_time' => time()
                )
        );
        DB::table('circle_members')->insert($members);
    }
    protected function prepareUsers() { 
        $no = 1;
        //准备用户数据
        $insertUsers = array(
            array(
                'name' => 'cc1',
                'cc_no' => $no ++,
                'email' => 'cc1@163.com'
            )
            ,
            array(
                'name' => 'cc2',
                'cc_no' => $no ++,
                'email' => 'cc2@163.com'
            ),
            array(
                'name' => 'cc3',
                'cc_no' => $no ++,
                'email' => 'cc3@163.com'
            ),
            array(
                'name' => 'cc4',
                'cc_no' => $no ++,
                'email' => 'cc4@163.com'
            ),
            array(
                'name' => 'cc5',
                'cc_no' => $no ++,
                'email' => 'cc5@163.com'
            )
        );
        $fills = new ExchangeControllerTest();
        $fills->fillOtherField($insertUsers);
        
        DB::table('users')->insert($insertUsers);
    }
}
