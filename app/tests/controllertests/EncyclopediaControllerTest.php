<?php
class EncyclopediaControllerTest extends BaseControllerTest {
    
    public function setUp() {
        parent::setUp();
        DB::table('encyclopedias')->truncate();
        Session::clear();
    }
    
    public function testPostShareEncyclopedia() {
        $url = '/encyclopedia/share-encyclopedia';
        
        $tests = [
        '' => '{"status":"not_login"}'
        ];
        
        $this->doPost($url, $tests);
        
        \Session::set('user_id', 1);
        
        $tests = [
        '' => '{"status":"param_error","error":{"name":"\u8bf7\u8f93\u5165\u521b\u610f\u540d\u79f0","cover":"\u8bf7\u4e0a\u4f20\u5c01\u9762\u56fe\u7247","content":"\u8bf7\u586b\u5199\u521b\u610f\u8be6\u7ec6\u4ecb\u7ecd"}}',
        '{"name":"hijacker"}' => '{"status":"param_error","error":{"cover":"\u8bf7\u4e0a\u4f20\u5c01\u9762\u56fe\u7247","content":"\u8bf7\u586b\u5199\u521b\u610f\u8be6\u7ec6\u4ecb\u7ecd"}}',
        '{"name":"hijacker","cover":"data/image/apple.jpg"}' => '{"status":"param_error","error":{"content":"\u8bf7\u586b\u5199\u521b\u610f\u8be6\u7ec6\u4ecb\u7ecd"}}',
        '{"name":"hijacker","cover":"data/image/apple.jpg","content":"nice"}' => '{"status":"ok"}'
        ];
        
        $this->doPost($url, $tests);
    }
    
    public function testGetDetail() {
        $url = '/encyclopedia/detail/1';
        
        $data = [
        [
        'user_id' => 1,
        'item_type' => 4,
        'item_id' => 0,
        'name' => '总之也还好',
        'cover' => 'data/image/apple.jpg',
        'content' => 'nice',
        'tags' => '{"apple":"delicious"}',
        'creative_index' => 20,
        'create_time' => time()
        ],
        [
        'user_id' => 2,
        'item_type' => 4,
        'item_id' => 0,
        'name' => '还好没错过',
        'cover' => 'data/image/apple.jpg',
        'content' => 'nice',
        'tags' => '{"apple":"delicious"}',
        'creative_index' => 20,
        'create_time' => time()
        ]
        ];
        DB::table('encyclopedias')->insert($data);
        
        $data = [
        [
        'name' => 'leon1',
        'cc_no' => 100001,
        'email' => '529909847@qq.com',
        'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
        'password_reset_code' => '',
        'activation_code' => '',
        'group_id' => 1,
        'avatar' => '',
        'creative_index' => 0,
        'coin' => 0,
        'creative_index_bonus_amount' => 0,
        'gender' => 0,
        'birthday' => 0,
        'introduction' => '',
        'tags' => '',
        'mobile_phone_no' => '',
        'province_id' => 1,
        'city_id' => 2,
        'district_id' => 3,
        'school_id' => 4,
        'organization_id' => 5,
        'expert_organization_id' => 6,
        'expert_duty_id' => 7,
        'expert_industry_id' => 8,
        'expert_title' => '',
        'follower_count' => 0,
        'liker_count' => 0,
        'register_time' => time(),
        'register_ip' => ''
            ]
            ];
        DB::table('users')->insert($data);
        
        $result = $this->call('GET',$url);
        $this->assertResponseOk();
    }
}