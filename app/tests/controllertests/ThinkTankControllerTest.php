<?php

class ThinkTankControllerTest extends BaseControllerTest {

    public function setUp() {
        parent::setUp();
        DB::table('expert_applications')->truncate();
        DB::table('users')->truncate();
        Session::clear();
    }
    
    public function prepareDatas() {
        $data = [
        [
        'name' => 'leon1',
        'cc_no' => 100001,
        'email' => '529909847@qq.com',
        'email_is_verified' => 0,
        'email_verify_code' => '',
        'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
        'password_reset_code' => '',
        'activation_code' => '',
        'group_id' => 2,
        'avatar' => '',
        'creative_index' => 0,
        'coin' => 0,
        'creative_index_bonus_amount' => 10,
        'gender' => 0,
        'birthday' => 0,
        'introduction' => '',
        'tags' => '',
        'mobile_phone_no' => '',
        'mobile_phone_is_verified' => 0,
        'mobile_phone_verify_code' => '',
        'province_id' => 1,
        'city_id' => 2,
        'district_id' => 3,
        'school_id' => 4,
        'organization_id' => 5,
        'expert_organization_id' => 6,
        'expert_duty_id' => 7,
        'expert_field_id' => 9,
        'expert_industry_id' => 8,
        'expert_title' => '',
        'follower_count' => 0,
        'register_time' => 10000,
        'register_ip' => '',
        'info' => ''
            ],
        [
        'name' => 'leon2',
        'cc_no' => 100002,
        'email' => '514128561@qq.com',
        'email_is_verified' => 0,
        'email_verify_code' => '',
        'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
        'password_reset_code' => '',
        'activation_code' => '',
        'group_id' => 2,
        'avatar' => '',
        'creative_index' => 0,
        'coin' => 0,
        'creative_index_bonus_amount' => 10,
        'gender' => 0,
        'birthday' => 0,
        'introduction' => '',
        'tags' => '',
        'mobile_phone_no' => '',
        'mobile_phone_is_verified' => 0,
        'mobile_phone_verify_code' => '',
        'province_id' => 1,
        'city_id' => 2,
        'district_id' => 4,
        'school_id' => 4,
        'organization_id' => 5,
        'expert_organization_id' => 7,
        'expert_duty_id' => 7,
        'expert_field_id' => 9,
        'expert_industry_id' => 8,
        'expert_title' => '',
        'follower_count' => 0,
        'register_time' => 10000,
        'register_ip' => '',
        'info' => ''
            ]
            ];
        DB::table('users')->insert($data);
    }
    
    public function testGetIndex() {
        $this->prepareDatas();
        
        $url = '/thinktank/index';
        
        $result = $this->call('GET',$url);
        $this->assertResponseOk();
    }
    
    public function testGetApplyCandidate() {
        $url = '/thinktank/apply-candidate';
        
        $result = $this->call('GET',$url);
        $this->assertResponseOk();
    }
    
    public function testPostIdentify() {
        $url = '/thinktank/identify';
        $tests = [
            '{"name":"hijacker","id_card_number":"510523654123650321"}' => '{"status":"not_login"}'
        ];
        $this->doPost($url, $tests);
        
        \Session::set('user_id', 1);
        
        $tests = [
            '' => '{"status":"param_error","error":{"name":"\u8bf7\u586b\u5199\u540d\u5b57","id_card_number":"\u8bf7\u8f93\u5165\u8eab\u4efd\u8bc1\u53f7\u7801"}}',
            '{"name":"!@$!@","id_card_number":"512510186536423650"}' => '{"status":"param_error","error":{"name":"\u540d\u5b57\u4e0d\u80fd\u5305\u542b\u975e\u6cd5\u5b57\u7b26"}}', 
            '{"name":"hijacker","id_card_number":"512510186536423650"}' => '{"status":"ok","content":1}'
        ];
        $this->doPost($url, $tests);
    }
    
    public function testPostApplyCandidate() {
        $data = [
        [
        'user_id' => 1,
        'name' => 'hijacker',
        'id_card_number' => '24567453412315',
        'industry_id' => 0,
        'enterprise_name' => '',
        'enterprise_duty' => '',
        'organization_id' => 0,
        'duty_id' => 0,
        'attachments' => '',
        'explain' => '',
        'create_time' => time()
        ]
        ];
        
        DB::table('expert_applications')->insert($data);
        
        $url = '/thinktank/apply-candidate';
        $tests = [
            '{"industry_id":1,"enterprise_name":"hijacker","enterprise_duty":"apple","organization_id":1,"duty_id":2,"attachments":"image/god.jpg","explain":"nice"}' => '{"status":"not_login"}'
        ];
        $this->doPost($url, $tests);
        
        \Session::set('user_id', 2);
        $tests = [
        '{"industry_id":1,"enterprise_name":"hijacker","enterprise_duty":"apple","organization_id":1,"duty_id":2,"attachments":"image/god.jpg","explain":"nice","is_agree_protocol":1}' => '{"status":"ok","content":false}'
            ];
        $this->doPost($url, $tests);
        
        \session::set('user_id', 1);
        $tests = [
        '' => '{"status":"param_error","error":{"industry_id":"\u8bf7\u9009\u62e9\u884c\u4e1a","enterprise_name":"\u8bf7\u586b\u5199\u516c\u53f8\u540d\u79f0","enterprise_duty":"\u8bf7\u586b\u5199\u516c\u53f8\u804c\u52a1","organization_id":"\u8bf7\u9009\u62e9\u7ec4\u7ec7","duty_id":"\u8bf7\u9009\u62e9\u804c\u52a1","attachments":"\u8bf7\u4e0a\u4f20\u8bc1\u660e\u6750\u6599","explain":"\u8bf7\u586b\u5199\u7533\u8bf7\u8bf4\u660e","is_agree_protocol":"common.error_not_accept_agreement"}}',
        '{"industry_id":1,"enterprise_name":"hijacker","enterprise_duty":"apple","organization_id":1,"duty_id":2,"attachments":"image/god.jpg","explain":"nice","is_agree_protocol":1}' => '{"status":"ok","content":true}'
            ];
        $this->doPost($url, $tests);
    }
    
    public function testPostSearchExperts() {
        $this->prepareDatas();
        $url = '/thinktank/search-experts';
        
        $tests = [
            '' => '{"status":"ok","content":[{"id":2,"name":"leon2","cc_no":100002,"email_is_verified":0,"email_verify_code":"","activation_code":"","group_id":2,"avatar":"","creative_index":0,"creative_index_bonus_amount":10,"gender":0,"birthday":0,"introduction":"","tags":"","mobile_phone_no":"","mobile_phone_is_verified":0,"mobile_phone_verify_code":"","province_id":1,"city_id":2,"district_id":4,"school_id":4,"organization_id":5,"expert_organization_id":7,"expert_duty_id":7,"expert_field_id":9,"expert_industry_id":8,"expert_title":"","follower_count":0,"contest_publish_count":0,"trade_publish_count":0,"register_time":10000,"register_ip":"","info":""},{"id":1,"name":"leon1","cc_no":100001,"email_is_verified":0,"email_verify_code":"","activation_code":"","group_id":2,"avatar":"","creative_index":0,"creative_index_bonus_amount":10,"gender":0,"birthday":0,"introduction":"","tags":"","mobile_phone_no":"","mobile_phone_is_verified":0,"mobile_phone_verify_code":"","province_id":1,"city_id":2,"district_id":3,"school_id":4,"organization_id":5,"expert_organization_id":6,"expert_duty_id":7,"expert_field_id":9,"expert_industry_id":8,"expert_title":"","follower_count":0,"contest_publish_count":0,"trade_publish_count":0,"register_time":10000,"register_ip":"","info":""}]}',
            '{"province_id":1,"city_id":2,"district_id":3}' => '{"status":"ok","content":[{"id":1,"name":"leon1","cc_no":100001,"email_is_verified":0,"email_verify_code":"","activation_code":"","group_id":2,"avatar":"","creative_index":0,"creative_index_bonus_amount":10,"gender":0,"birthday":0,"introduction":"","tags":"","mobile_phone_no":"","mobile_phone_is_verified":0,"mobile_phone_verify_code":"","province_id":1,"city_id":2,"district_id":3,"school_id":4,"organization_id":5,"expert_organization_id":6,"expert_duty_id":7,"expert_field_id":9,"expert_industry_id":8,"expert_title":"","follower_count":0,"contest_publish_count":0,"trade_publish_count":0,"register_time":10000,"register_ip":"","info":""}]}',
            '{"province_id":1,"city_id":2,"district_id":4,"expert_organization_id":7,"expert_duty_id":7,"expert_industry_id":9,"expert_field_id":8}' => '{"status":"ok","content":[{"id":2,"name":"leon2","cc_no":100002,"email_is_verified":0,"email_verify_code":"","activation_code":"","group_id":2,"avatar":"","creative_index":0,"creative_index_bonus_amount":10,"gender":0,"birthday":0,"introduction":"","tags":"","mobile_phone_no":"","mobile_phone_is_verified":0,"mobile_phone_verify_code":"","province_id":1,"city_id":2,"district_id":4,"school_id":4,"organization_id":5,"expert_organization_id":7,"expert_duty_id":7,"expert_field_id":9,"expert_industry_id":8,"expert_title":"","follower_count":0,"contest_publish_count":0,"trade_publish_count":0,"register_time":10000,"register_ip":"","info":""}]}',
            '{"province_id":2,"city_id":2,"district_id":4,"expert_organization_id":7,"expert_duty_id":7,"expert_industry_id":9,"expert_field_id":8}' => '{"status":"ok","content":[]}'
        ];
        $this->doPost($url, $tests);
    }
    
}