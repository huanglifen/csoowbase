<?php

use App\Modules\ItemModule;
class ProjectControllerTest extends BaseControllerTest {
	
	public function setUp() {
		parent::setUp();
		//DB::table('contests')->truncate();
		DB::table('updates')->truncate();
		DB::table('project_investments')->truncate();
	}
	
	public function testPostPublish() {
		$url = '/project/publish';
		
		$tests = ['{"title":"this is title","cover":"projectcover","description":"this is description","investment_end_time":"20","goal_amount":"1000","reward_give_time":"123333333","reward_description":"this is reward description","reward_pics":"rewardpic1"}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
				
		Session::set('user_id', 1);
		
		$tests = [
			'' => '{"status":"param_error","error":{"title":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u540d\\u79f0","description":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u8be6\\u7ec6\\u4ecb\\u7ecd","cover":"\\u8bf7\\u4e0a\\u4f20\\u5c01\\u9762\\u56fe\\u7247","investment_end_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u7684\\u9650\\u5236\\u65f6\\u95f4","goal_amount":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u91d1\\u989d","reward_give_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u56de\\u62a5\\u5151\\u73b0\\u7684\\u65f6\\u95f4","reward_description":"\\u8bf7\\u586b\\u5199\\u6295\\u8d44\\u56de\\u62a5\\u8be6\\u7ec6\\u4ecb\\u7ecd","reward_pics":"\\u8bf7\\u4e0a\\u4f20\\u6295\\u8d44\\u56de\\u62a5\\u56fe\\u7247"}}',
			'{"title":"this is title"}' => '{"status":"param_error","error":{"description":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u8be6\\u7ec6\\u4ecb\\u7ecd","cover":"\\u8bf7\\u4e0a\\u4f20\\u5c01\\u9762\\u56fe\\u7247","investment_end_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u7684\\u9650\\u5236\\u65f6\\u95f4","goal_amount":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u91d1\\u989d","reward_give_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u56de\\u62a5\\u5151\\u73b0\\u7684\\u65f6\\u95f4","reward_description":"\\u8bf7\\u586b\\u5199\\u6295\\u8d44\\u56de\\u62a5\\u8be6\\u7ec6\\u4ecb\\u7ecd","reward_pics":"\\u8bf7\\u4e0a\\u4f20\\u6295\\u8d44\\u56de\\u62a5\\u56fe\\u7247"}}',
 			'{"title":"this is title","cover":"projectcover"}' => '{"status":"param_error","error":{"description":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u8be6\\u7ec6\\u4ecb\\u7ecd","investment_end_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u7684\\u9650\\u5236\\u65f6\\u95f4","goal_amount":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u91d1\\u989d","reward_give_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u56de\\u62a5\\u5151\\u73b0\\u7684\\u65f6\\u95f4","reward_description":"\\u8bf7\\u586b\\u5199\\u6295\\u8d44\\u56de\\u62a5\\u8be6\\u7ec6\\u4ecb\\u7ecd","reward_pics":"\\u8bf7\\u4e0a\\u4f20\\u6295\\u8d44\\u56de\\u62a5\\u56fe\\u7247"}}',
			'{"title":"this is title","cover":"projectcover","description":"this is description"}' => '{"status":"param_error","error":{"investment_end_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u7684\\u9650\\u5236\\u65f6\\u95f4","goal_amount":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u91d1\\u989d","reward_give_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u56de\\u62a5\\u5151\\u73b0\\u7684\\u65f6\\u95f4","reward_description":"\\u8bf7\\u586b\\u5199\\u6295\\u8d44\\u56de\\u62a5\\u8be6\\u7ec6\\u4ecb\\u7ecd","reward_pics":"\\u8bf7\\u4e0a\\u4f20\\u6295\\u8d44\\u56de\\u62a5\\u56fe\\u7247"}}',			
			'{"title":"this is title","cover":"projectcover","description":"this is description","investment_end_time":"20"}' => '{"status":"param_error","error":{"goal_amount":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u91d1\\u989d","reward_give_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u56de\\u62a5\\u5151\\u73b0\\u7684\\u65f6\\u95f4","reward_description":"\\u8bf7\\u586b\\u5199\\u6295\\u8d44\\u56de\\u62a5\\u8be6\\u7ec6\\u4ecb\\u7ecd","reward_pics":"\\u8bf7\\u4e0a\\u4f20\\u6295\\u8d44\\u56de\\u62a5\\u56fe\\u7247"}}',
			'{"title":"this is title","cover":"projectcover","description":"this is description","investment_end_time":"20","goal_amount":"1000"}' => '{"status":"param_error","error":{"reward_give_time":"\\u8bf7\\u586b\\u5199\\u9879\\u76ee\\u6295\\u8d44\\u56de\\u62a5\\u5151\\u73b0\\u7684\\u65f6\\u95f4","reward_description":"\\u8bf7\\u586b\\u5199\\u6295\\u8d44\\u56de\\u62a5\\u8be6\\u7ec6\\u4ecb\\u7ecd","reward_pics":"\\u8bf7\\u4e0a\\u4f20\\u6295\\u8d44\\u56de\\u62a5\\u56fe\\u7247"}}',
			'{"title":"this is title","cover":"projectcover","description":"this is description","investment_end_time":"20","goal_amount":"1000","reward_give_time":"123333333"}' => '{"status":"param_error","error":{"reward_description":"\\u8bf7\\u586b\\u5199\\u6295\\u8d44\\u56de\\u62a5\\u8be6\\u7ec6\\u4ecb\\u7ecd","reward_pics":"\\u8bf7\\u4e0a\\u4f20\\u6295\\u8d44\\u56de\\u62a5\\u56fe\\u7247"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","investment_end_time":100,"goal_amount":100,"reward_give_time":123213,"reward_description":"des","reward_pics":"dfd","tags":"eru,\u7684\u53d1\u7684_"}' => '{"status":"param_error","error":{"tags":"\u8bf7\u8f93\u5165\u53ea\u542b\u6709\u4e2d\u6587\uff0c\u82f1\u6587\uff0c\u6570\u5b57\u7684\u6807\u7b7e"}}',
			'{"title":"this is title","cover":"projectcover","description":"this is description","investment_end_time":"20","goal_amount":"1000","reward_give_time":"123333333","reward_description":"this is reward description"}' => '{"status":"param_error","error":{"reward_pics":"\\u8bf7\\u4e0a\\u4f20\\u6295\\u8d44\\u56de\\u62a5\\u56fe\\u7247"}}',
			'{"title":"this is title","cover":"projectcover","description":"this is description","investment_end_time":"20","goal_amount":"1000","reward_give_time":"123333333","reward_description":"this is reward description","reward_pics":"rewardpic1"}' => '{"status":"ok"}',			
		];

		$this->doPost($url, $tests);
	}
	/**
	public function testPostUpdate() {
		$url = '/project/update';
		
		$tests = [
			'' => '{"status":"param_error","error":{"project_id":"\u8bf7\u586b\u5199common.project_id","title":"\u8bf7\u586b\u5199\u6807\u9898","content":"\u8bf7\u586b\u5199\u66f4\u65b0\u5185\u5bb9"}}',
			'{"project_id":"this is string"}' => '{"status":"param_error","error":{"project_id":"\u8bf7\u586b\u5199common.project_id","title":"\u8bf7\u586b\u5199\u6807\u9898","content":"\u8bf7\u586b\u5199\u66f4\u65b0\u5185\u5bb9"}}',
			'{"project_id":"1"}' => '{"status":"param_error","error":{"title":"\u8bf7\u586b\u5199\u6807\u9898","content":"\u8bf7\u586b\u5199\u66f4\u65b0\u5185\u5bb9"}}',
			'{"project_id":"1","title":"this is title"}' => '{"status":"param_error","error":{"content":"\u8bf7\u586b\u5199\u66f4\u65b0\u5185\u5bb9"}}',
			'{"project_id":"1","title":"this is title","content":"this is content"}' => '{"status":"not_login"}'
		];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = ['{"project_id":"1","title":"this is title","content":"this is content"}' => '{"status":"ok"}'];
	
		$this->doPost($url, $tests);
	}
	
	public function testPostInvest() {
		$this->insertProject();
		$url = '/project/invest';
		
		$tests = [
			'' => '{"status":"not_login"}'
		];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
			'' => '{"status":"param_error","error":{"project_id":"\u8bf7\u586b\u5199common.project_id","amount":"\u8bf7\u586b\u5199\u6295\u8d44\u91d1\u989d","type":"\u8bf7\u9009\u62e9\u6295\u8d44\u7c7b\u578b","requirement":"\u8bf7\u586b\u5199\u60a8\u7684\u6295\u8d44\u8ff0\u6c42"}}',
			'{"project_id":1,"amount":200,"type":1,"requirement":"requirement"}' => '{"status":"ok"}',
		];	
		
		$this->doPost($url, $tests);
	}
	
	public function testPostUpdateInvest() {
		$this->insertInvest();
		
		$url = '/project/update-invest';
		
		$tests = [
			'' => '{"status":"not_login"}'
		];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 2);
		
		$tests = [
			'{}' => '{"status":"param_error","error":{"invest_id":"\u8bf7\u586b\u5199common.invest_id"}}',
			'{"invest_id":1,"type":1}' => '{"status":"ok"}'
		];
	}
	
	public function testPostCancelInvest() {
		$this->insertInvest();
		
		$url = '/project/cancel-invest';
		
		$tests = [
		'' => '{"status":"not_login"}'
				];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 2);
		
		$tests = ['{"invest_id":1}' => '{"status":"ok"}'];
		
		$this->doPost($url, $tests);
	}
	
	public function testPostAcceptInvest() {
		$this->insertProject();
		$this->insertInvest();
		
		$url = '/project/accept-invest';
		
		$tests = [
			'' => '{"status":"not_login"}'
		];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = ['{"invest_id":1}' => '{"status":"ok"}'];
		
		$this->doPost($url, $tests);
	}
	**/
	protected function insertProject() {
		$data = [[
		'no' => 'P23123213',
		'user_id' => 1,
		'title' => 'title',
		'description' => 'description',
		'cover' => 'cover',
		'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		'province_id' => 0,
		'city_id' => 0,
		'status' => 1,
		'start_time' => 1233131234,
		'school_id' => 0,
		'creative_index' => 0,
		'organization_id' => 0,
		'type' => 1,
		'info' => json_encode(array('investment_end_time' => 100, 'goal_amount' => 100)),
		'creative_index'=>0,
		]];
		DB::table('contests')->insert($data);
	
		$data = [
		['name' => 'test','type' => 1 ],
		['name' => '的发的', 'type' => 1]
		];
		DB::table('tags')->insert($data);
	
		$data = [
		['tag_id' => 1, 'affair_type' => 1, 'affair_id' => 1],
		['tag_id' => 2, 'affair_type' => 1, 'affair_id' => 1],
		];
		DB::table('tags_affairs')->insert($data);
	}
	
	protected function insertInvest() {
		$data = [[
			'project_id' => 1,
			'user_id' => 2,
			'amount' => 200,
			'type' => 1,
			'deposit_percentage' => 20,
			'final_payment_pay_time' => 1234124,
			'require_complete_time' => 42134,
			'requirement' => 'requirement',
			'status' => 1,
			'create_time' => time(),
 		]];
		DB::table('project_investments')->insert($data);
	}
	
}

include_once dirname(__FILE__) . '/../ItemAbFix.php';