<?php
use App\Modules\ItemModule;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Modules\BaseModule;
class ExchangeControllerTest extends BaseControllerTest {
    const TABLE = 'exchange_trades';
    
	public function setUp() {
		parent::setUp();
		DB::table('exchange_trades')->truncate();
		DB::table('tags')->truncate();
		DB::table('tags_affairs')->truncate();
		DB::table('users')->truncate();
		$this->prepareUsers();
	}

	public function testGetPublish() {

	    //case 1
	    $url = '/exchange/publish';
	    
	    $response = $this->call('GET', $url);
	    $this->assertRedirectedTo('/user/login');
	    
	    //case 2
	    Session::set('user_id', 1);
	    $response = $this->call('GET', $url);
	    $this->assertResponseOk();
	}
	
	public function testPostPublish() {
	    //case 1
		$url = '/exchange/publish';
		
		$tests = ['{"title":"this is title","cover":"cover","description":"this is description"}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		// case 2
		Session::set('user_id', 1);
		
		$tests = [
			'{}' => '{"status":"param_error","error":{"title":"\u8bf7\u586b\u5199\u4ea4\u6613\u540d\u79f0","description":"\u8bf7\u586b\u5199\u4ea4\u6613\u8be6\u7ec6\u4ecb\u7ecd","cover":"\u8bf7\u4e0a\u4f20\u5c01\u9762\u56fe\u7247","price":"\u8bf7\u586b\u5199\u552e\u4ef7"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","price":"haha"}' => '{"status":"param_error","error":{"price":"\u552e\u4ef7\u5fc5\u987b\u5927\u4e8e\u96f6"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","price":1}' => '{"status":"ok"}',
			'{"title":"this is title","cover":"cover","description":"this is description","price":1,"tags":"eru,tags"}' => '{"status":"ok"}',
			'{"title":"this is title","cover":"cover","description":"this is description","price":1,"tags":"eru~~,\u7684\u53d1\u7684_"}' => '{"status":"param_error","error":{"tags":"\u8bf7\u8f93\u5165\u53ea\u542b\u6709\u4e2d\u6587\uff0c\u82f1\u6587\uff0c\u6570\u5b57\u7684\u6807\u7b7e"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","price":1,"tags":"iamverylonglo,\u7684\u53d1\u7684"}' => '{"status":"param_error","error":{"tags":"\u8bf7\u8f93\u51652-12\u5b57\u7b26\u7684\u6807\u7b7e"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","price":1,"tags":"eru,eru,eru,eru,eru,eru"}' => '{"status":"param_error","error":{"tags":"error_tags_number_cant_bigger_than_five"}}',
		];
		
		$this->doPost($url, $tests);
	}
	
	public function testGetIndex() {
	    $this->prepareData();	    
	    $url = '/exchange';
	    //case 1
	    $result = $this->call('GET', $url);
	    $this->assertResponseOk();
	    
	    $this->assertViewHas('trades');
	    $this->assertViewHas('ranks');
	    $this->assertViewHas('tags');
	}
	
	public function testGetList() {
	    $this->prepareData();
	    $url = '/exchange/list';	     

	    $input = array(
	            'isAjax' => true,
	            'type' => 1,
	            'tag_id' => 1,
	            'page' => 1,
	    );
	    $result = $this->call('GET', $url, $input);
	    $this->assertResponseOk();
	    
	    $this->assertViewHas('trades');
	}
	
	public function testGetDetail() {
	    $this->prepareData();	 
	    $this->prepareComments();
	    $this->prepareExpertComments();   	   
	    $url = '/exchange/detail';	    
	    //case1
	    try{
	    $result = $this->call('GET', $url);
	    $this->assertResponseStatus(404);
	    }catch(NotFoundHttpException $e) {
	        $this->assertNotEmpty($e);
	    }
	    
	    //case2
	    $id = 1;
	    $result = $this->call('GET', $url.'/'.$id);
	    $this->assertResponseOk();
	    
	    $this->assertViewHas('trade');
	    $this->assertViewHas('comments');
	    $this->assertViewHas('expertComments');    
	}

	public function testPostPublishComment() {
	    $url = '/exchange/publish-comment';
	    
	    $tests = ['{}' => '{"status":"not_login"}'];
	    $this->doPost($url, $tests);
	    
	    Session::set('user_id', 1);
	    
	    $tests = [
	    '{}' => '{"status":"param_error","error":{"target_type":"\u8bf7\u586b\u5199common.target_type","target_id":"\u8bf7\u586b\u5199\u5fc5\u987b\u662f\u81ea\u7136\u6570","content":"\u8bf7\u586b\u5199common.content"}}',
	    '{"target_id":"1","content":"this is content","target_type":4}' => '{"status":"ok"}',
	      ];
	    $this->doPost($url, $tests);
	}

	public function testGetComments() {
	    $this->prepareComments();
	    $inputs = array(
	        'target_id' => 1,
	        'page' => 1,
	        'target_type' => BaseModule::TYPE_EXCHANGE,
	    );
	    
	    $url = '/exchange/comments';
	    
	    $result = $this->call('GET', $url, $inputs);
	    $this->assertResponseOk();
	    $this->assertViewHas('comments');
	     
	    $view = $result->original;	    
	    $comments = $view['comments'];
	    
	    foreach($comments as $c) {
	        $this->assertEquals(BaseModule::TYPE_EXCHANGE, $c->target_type);
	        $this->assertEquals($inputs['target_id'], $c->target_id);
	    }

	}
	
	public function testPostPublishExpertComment() {
	    $url = '/exchange/publish-expert-comment';
	     
	    $tests = ['{}' => '{"status":"not_login"}'];
	    $this->doPost($url, $tests);
	     
	    Session::set('user_id', 1);
	     
	    $tests = [
	    '{}' => '{"status":"param_error","error":{"target_type":"\u8bf7\u586b\u5199common.target_type","target_id":"\u8bf7\u586b\u5199\u5fc5\u987b\u662f\u81ea\u7136\u6570","content":"\u8bf7\u586b\u5199common.content","score":"\u8bf7\u586b\u5199common.score"}}',
	    '{"target_id":1,"score":2,"content":"this is content","target_type":4}' => '{"status":"ok"}',
	    ];
	    $this->doPost($url, $tests);
	}

    public function testGetExpertComment()
    {
        $this->prepareExpertComments();
        
        $url = 'exchange/expert-comment';
        $inputs = array(
            'target_id' => 1,
            'page' => 1,
            'target_type' => BaseModule::TYPE_EXCHANGE,
        );
        
        $result = $this->call('GET', $url, $inputs);
        $this->assertResponseOk();
        $this->assertViewHas('expertComments');
        
        $view = $result->original;
        $expertComments = $view['expertComments'];
        foreach ($expertComments as $c) {
            $this->assertEquals(BaseModule::TYPE_EXCHANGE, $c->target_type);
            $this->assertEquals($inputs['target_id'], $c->target_id);
        }
    }
    
    public function testPostInviteInternalExpert() {   
        DB::table('expert_invites_internal')->truncate();     
        $url = '/exchange/invite-internal-expert';
        
        $tests = ['{}' => '{"status":"not_login"}'];
        $this->doPost($url, $tests);
         
        Session::set('user_id', 1);
         
        $tests = [
        '{}' => '{"status":"param_error","error":{"target_type":"\u8bf7\u586b\u5199common.target_type","target_id":"\u8bf7\u586b\u5199\u5fc5\u987b\u662f\u81ea\u7136\u6570","expert_user_id":"\u8bf7\u586b\u5199common.expert_user_id"}}',
        '{"target_id":1,"expert_user_id":"1","target_type":4}' => '{"status":"ok","content":1}',
        ];
        $this->doPost($url, $tests);
    }
    
    public function testPostDeleteInternalExpert() {
        $this->prepareInviteExperts();
        $url = '/exchange/delete-internal-expert';
        
        $tests = ['{}' => '{"status":"not_login"}'];
        $this->doPost($url, $tests);
         
        Session::set('user_id', 1);
         
        $tests = [
        '{}' => '{"status":"param_error","error":{"id":"\u8bf7\u586b\u5199common.id"}}',
        '{"id":1}' => '{"status":"ok"}',
        ];
        $this->doPost($url, $tests);
    }
    
    public function testPostInviteExternalExpert() {
        DB::table('expert_invites_external')->truncate();
        $url = '/exchange/invite-external-expert';
        
        $tests = ['{}' => '{"status":"not_login"}'];
        $this->doPost($url, $tests);
         
        Session::set('user_id', 1);
         
        $tests = [
        '{}' => '{"status":"param_error","error":{"target_type":"\u8bf7\u586b\u5199common.target_type","target_id":"\u8bf7\u586b\u5199\u5fc5\u987b\u662f\u81ea\u7136\u6570","name":"\u8bf7\u586b\u5199common.name","industry":"\u8bf7\u586b\u5199common.industry","email":"\u8bf7\u586b\u5199\u90ae\u7bb1","mobile_phone_no":"\u8bf7\u586b\u5199common.mobile_phone_no","contact_info":"\u8bf7\u586b\u5199common.contact_info"}}',
        '{"target_id":1, "name":"test", "email":"huang","mobile_phone_no":"222haha","contact_info":"contact", "industry":"1","target_type":4}' => '{"status":"param_error","error":{"email":"\u8bf7\u586b\u5199\u90ae\u7bb1","mobile_phone_no":"\u8bf7\u586b\u5199common.mobile_phone_no"}}',
        '{"target_id":1, "name":"test", "email":"huang@163.com","mobile_phone_no":"13260185470","contact_info":"contact","industry":"2","target_type":4}' => '{"status":"ok","content":1}',
        ];
        $this->doPost($url, $tests);
    }

    public function testPostLike() {
        DB::table('items')->truncate();
        $url = '/exchange/like';
        //case 1
        $tests = ['{}' => '{"status":"not_login"}'];
        $this->doPost($url, $tests);
         
        //case 2
        Session::set('user_id', 1);        
        $tests = [
        '{}' => '{"status":"param_error","error":{"target_type":"\u8bf7\u586b\u5199common.target_type","target_id":"\u8bf7\u586b\u5199\u5fc5\u987b\u662f\u81ea\u7136\u6570"}}',
        '{"target_id":1,"target_type":4}' => '{"status":"ok","content":1}',
        ];
        $this->doPost($url, $tests);
    }
    
    public function testPostReport() {
        DB::table('reports')->truncate();       
        $url = '/exchange/report';
        //case 1
        $tests = ['{}' => '{"status":"not_login"}'];
        $this->doPost($url, $tests);
        //case 2
        Session::set('user_id', 1);
        $tests = [
        '{}' => '{"status":"param_error","error":{"target_type":"\u8bf7\u586b\u5199common.target_type","target_id":"\u8bf7\u586b\u5199\u5fc5\u987b\u662f\u81ea\u7136\u6570","type":"\u8bf7\u586b\u5199common.type"}}',
        '{"target_id":1,"type":1,"target_type":4}' => '{"status":"ok","content":1}',
        ];
        $this->doPost($url, $tests);
    }

	protected  function prepareComments() {
	    DB::table('comments')->truncate();
	    $inserts = array(
	      array(
	          'user_id' => 1,
	          'target_type' => BaseModule::TYPE_EXCHANGE,
	          'target_id' => 1,
	          'content' => 'hahaha',
	          'create_time' => time(),
	    ),  array(
	          'user_id' => 2,
	          'target_type' => BaseModule::TYPE_EXCHANGE,
	          'target_id' => 1,
	          'content' => 'hahaha',
	          'create_time' => time(),
	    ),array(
	          'user_id' => 1,
	          'target_type' => 3,
	          'target_id' => 1,
	          'content' => 'hahaha',
	          'create_time' => time(),
	    ),array(
	          'user_id' => 1,
	          'target_type' => BaseModule::TYPE_EXCHANGE,
	          'target_id' => 2,
	          'content' => 'hahaha',
	          'create_time' => time(),
	    ),
	    );
	    
	    DB::table('comments')->insert($inserts);
	}

    protected function prepareExpertComments()
    {
        DB::table('expert_comments')->truncate();
        $inserts = array(
            array(
                'user_id' => 1,
                'target_type' => BaseModule::TYPE_EXCHANGE,
                'target_id' => 1,
                'score' => 1,
                'content' => 'hahaha',
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'target_type' => BaseModule::TYPE_EXCHANGE,
                'target_id' => 1,
                'score' => 1,
                'content' => 'hahaha',
                'create_time' => time()
            ),
            array(
                'user_id' => 2,
                'target_type' => BaseModule::TYPE_EXCHANGE,
                'target_id' => 2,
                'score' => 2,
                'content' => 'hahaha',
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'target_type' => 3,
                'target_id' => 1,
                'score' => 1,
                'content' => 'hahaha',
                'create_time' => time()
            )
        );
        
        DB::table('expert_comments')->insert($inserts);
    }
    
    protected function prepareInviteExperts() {
        DB::table('expert_invites_internal')->truncate();
        $insertInter = array(
            array(
                'user_id' => 1,
                'target_type' => 11,
                'target_id' => 1,
                'target_sub_id' => 0,
                'create_time' => time(),
                'expert_user_id' => 2
        ),array(
                'user_id' => 2,
                'target_type' => 11,
                'target_id' => 1,
                'target_sub_id' => 0,
                'create_time' => time(),
                'expert_user_id' => 3
        ),array(
                'user_id' => 3,
                'target_type' => 11,
                'target_id' => 2,
                'target_sub_id' => 0,
                'create_time' => time(),
                'expert_user_id' => 2
        ),
        );
        DB::table('expert_invites_internal')->insert($insertInter);
    }
	
    protected function prepareUsers() {
        $no = 1;
        //准备用户数据
        $insertUsers = array(
                array(
                        'name' => 'cc1',
                        'cc_no' => $no++,
                        'email' => 'cc1@163.com',
        
                ),array(
                        'name' => 'cc2',
                        'cc_no' => $no++,
                        'email' => 'cc2@163.com',
                ),array(
                        'name' => 'cc3',
                        'cc_no' => $no++,
                        'email' => 'cc3@163.com',
                ),
        );
        
        $this->fillOtherField($insertUsers);
        DB::table('users')->insert($insertUsers);
    }
    protected function prepareData()
    {       
        //准备标签数据
        $insertTags = array(
            array(
                'name' => '文艺范',
                'type' => 11
            ),
            array(
                'name' => '创意',
                'type' => 11
            ),
            array(
                'name' => '非主流',
                'type' => 11
            ),
            array(
                'name' => '手工制作',
                'type' => 2
            ),
            array(
                'name' => '非主流',
                'type' => 1
            )
        );
        DB::table('tags')->insert($insertTags);
        
        //准备事务数据
        $insertAffairs = array(
            array(
                'tag_id' => 1,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 1
            ),
            array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 2
            ),
            array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 3
            ),
            array(
                'tag_id' => 2,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 4
            ),
            array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 5
            ),
            array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 4
            ),
            array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 1
            )
        );
        DB::table('tags_affairs')->insert($insertAffairs);
        //准备交易所数据
        $i = 1;
        $inserts = array(
            array(
                'no' => $i ++,
                'user_id' => 1,
                'title' => '交易所',
                'description' => 'des des des 这里是描述信息',
                'cover' => '1.jpg',
                'tags' => json_encode(array(
                    '1' => '文艺范',
                    '3' => '非主流'
                )),
                'province_id' => 0,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'status' => ItemModule::STATUS_GOING,
                'start_time' => time(),
                'creative_index' => 20,
                'price' => 2
            )
            ,
            array(
                'no' => $i ++,
                'user_id' => 2,
                'title' => '交易所',
                'description' => 'des des des 这里是描述信息',
                'cover' => '1.jpg',
                'tags' => json_encode(array(
                    '3' => '非主流'
                )),
                'province_id' => 0,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'status' => ItemModule::STATUS_GOING,
                'start_time' => time(),
                'creative_index' => 30,
                'price' => 3
            )
            ,
            array(
                'no' => $i ++,
                'user_id' => 3,
                'title' => '交易所',
                'description' => 'des des des 这里是描述信息',
                'cover' => '1.jpg',
                'tags' => json_encode(array(
                    '3' => '非主流'
                )),
                'province_id' => 0,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'status' => ItemModule::STATUS_GOING,
                'start_time' => time(),
                'creative_index' => 10,
                'price' => 3
            )
            ,
            array(
                'no' => $i ++,
                'user_id' => 3,
                'title' => '交易所',
                'description' => 'des des des 这里是描述信息',
                'cover' => '1.jpg',
                'tags' => json_encode(array(
                    '3' => '非主流'
                )),
                'province_id' => 0,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'status' => ItemModule::STATUS_END_SUCCESS,
                'start_time' => time(),
                'creative_index' => 5,
                'price' => 3
            )
            ,
            array(
                'no' => $i ++,
                'user_id' => 2,
                'title' => '交易所',
                'description' => 'des des des 这里是描述信息',
                'cover' => '1.jpg',
                'tags' => json_encode(array(
                    '3' => '非主流'
                )),
                'province_id' => 0,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'status' => 0,
                'start_time' => time(),
                'creative_index' => 10,
                'price' => 3
            )
            ,
            array(
                'no' => $i ++,
                'user_id' => 1,
                'title' => '交易所',
                'description' => 'des des des 这里是描述信息',
                'cover' => '1.jpg',
                'tags' => json_encode(array()),
                'province_id' => 0,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'status' => ItemModule::STATUS_GOING,
                'start_time' => time(),
                'creative_index' => 100,
                'price' => 30
            )
            
        );
        DB::table(self::TABLE)->insert($inserts);
    }
    
    public function fillOtherField(&$insertUsers) {
        foreach($insertUsers as &$user) {
            $info =    array(
                    'email_is_verified' => 0,
                    'email_verify_code' => 0,
                    'password' => 'eeeeeeeeee',
                    'password_reset_code' => '',
                    'activation_code' => '',
                    'group_id' => 1,
                    'avatar' => '1.jpg',
                    'creative_index' => 1,
                    'coin' => 0,
                    'creative_index_bonus_amount' => 0,
                    'gender' => 1,
                    'birthday' => 123456,
                    'introduction' => 'dd',
                    'tags' => '[]',
                    'mobile_phone_no' => '123',
                    'mobile_phone_is_verified' => 0,
                    'mobile_phone_verify_code' => 0,
                    'province_id' => 0,
                    'city_id' => 0,
                    'district_id' => 0,
                    'school_id' => 0,
                    'organization_id' => 0,
                    'expert_organization_id' => 0,
                    'expert_duty_id' => 0,
                    'expert_industries' => 0,
                    'expert_field_id' => 0,
                    'expert_title' => 'haha',
                    'follower_count' => 0,
                    'contest_publish_count' => 0,
                    'trade_publish_count' => 0,
                    //'liker_count' => 0,
                    'register_time' => time(),
                    'register_ip' => '',
                    'info' => '',

            ) ;
            $user = array_merge($info, $user);
        }
    }
	}
 