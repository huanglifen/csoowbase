<?php

class AccountControllerTest extends BaseControllerTest {
    
    public function testPostSchool(){
        $url = '/account/school';
        $tests = [
        '{"school":"成都"}' => '{"status":"not_login"}'
        ];
        
        $this->doPost($url, $tests);
        
        \Session::set('user_id', 1);
        
        $tests = [
        '{"school":"成都"}' => '{"status":"ok","content":[{"id":1,"name":"\u6210\u90fd\u4e03\u4e2d","logo":"","creative_index":0,"intro":"","type":0,"nation_id":0,"province_id":0,"city_id":0,"district_id":0,"contests_count":0,"trades_count":0,"experts_count":0,"users_count":0,"likes_count":0}]}'
            ];
        
        $this->doPost($url, $tests);
    }
    
    public function testPostOrganization(){
        $url = '/account/organization';
        $tests = [
        '{"organization":"泸州"}' => '{"status":"not_login"}'
            ];
    
        $this->doPost($url, $tests);
    
        \Session::set('user_id', 1);
    
        $tests = [
        '{"organization":"泸州"}' => '{"status":"ok","content":[{"id":1,"name":"\u6cf8\u5dde\u8001\u7a96","logo":"","creative_index":0,"intro":"","type":0,"nation_id":0,"province_id":0,"city_id":0,"district_id":0,"contests_count":0,"trades_count":0,"experts_count":0,"users_count":0,"likes_count":0}]}'
            ];
    
        $this->doPost($url, $tests);
    }
}