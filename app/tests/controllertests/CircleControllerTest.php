<?php

use App\Modules\CircleModule;
class CircleControllerTest extends BaseControllerTest {
    
    public function __construct(){
        parent::__construct();
        include_once 'app/tests/controllertests/ExchangeControllerTest.php';
    }
    public function setUp() {
        parent::setUp();
        DB::table('circles')->truncate();
        DB::table('circle_members')->truncate();
        DB::table('users')->truncate();
        Session::start();
    }

    public function testGetMyCircle() {     
        $this->prepareData();
        $url ='/circle/my-circle';       
        //case 1
        $response = $this->call('GET', $url);
        $this->assertRedirectedTo('/user/login');
        //case 2
        Session::put('user_id', 1);       
        $response = $this->call('GET', $url);
        $this->assertResponseOk();
        $view = $response->original;
        
        $this->assertViewHas('circles');
        $this->assertViewHas('members');
        
        $members = $view['members'];
        
        foreach($members as $member) {
            $this->assertObjectHasAttribute('user', $member);
        }
    }
    
    public function testGetMyCircleInner() {
        $this->prepareData();        
        $url = '/circle/my-circle-inner';             
        //case 1
        $inputs = array(
            'page' => 0,
        );
        Session::put('user_id', 1);
        $response = $this->call('GET', $url, $inputs);
        $this->assertEquals('{"status":"param_error","error":{"type":"\u8bf7\u586b\u5199common.type"}}', $response->getContent());
        //case 2
        $inputs = array(
            'type' => CircleModule::TYPE_MY_CIRCLE,
            'circle_id' => 0,
        );
        $response = $this->call('GET', $url, $inputs);
        $this->assertResponseOk();
        $this->assertViewHas('members');  
        $view = $response->original;
        $members = $view['members'];
        $this->assertCount(3, $members);    
        //case 3 
        $inputs = array(
            'type' => CircleModule::TYPE_MY_CIRCLE,
            'circle_id' => 1
        );
        $response = $this->call('GET', $url, $inputs);
        $this->assertResponseOk();
        $this->assertViewHas('members');
        
        $view = $response->original;
        $members = $view['members'];
        $this->assertCount(2, $members);
        //case 4
        $inputs = array(
            'type' => CircleModule::TYPE_CIRCLE_ME
        );
        $response = $this->call('GET', $url, $inputs);
        $this->assertResponseOk();
        $this->assertViewHas('members');
        $view = $response->original;
        $members = $view['members'];
        $this->assertCount(2, $members);
    }

    public function testPostCreateCircle() {
       Session::put('user_id', 1);
       $url = '/circle/create-circle';
       $tests = [
       '{}' => '{"status":"param_error","error":{"name":"\u8bf7\u586b\u5199common.name"}}',
       '{"name":"english"}' => '{"status":"param_error","error":{"name":"\u8bf7\u8f93\u5165\u4e2d\u6587"}}',
       '{"name":"我的圈子名字很长啊很长啊很长啊很"}' => '{"status":"param_error","error":{"name":"\u540d\u5b57\u957f\u5ea6\u5fc5\u987b\u5c0f\u4e8e15"}}',
       '{"name":"你见过这样标准的十五个中文字吗"}' => '{"status":"ok","content":1}',
       ];
       $this->doPost($url, $tests);
    }
    
    public function testPostDeleteCircle() {
        $this->prepareCircleData();
        Session::put('user_id', 1);
        $url = '/circle/delete-circle';
        $tests = [
        '{}' => '{"status":"param_error","error":{"id":"\u8bf7\u586b\u5199common.id"}}',
        '{"id":1}' => '{"status":"ok","content":true}',
        ];
        $this->doPost($url, $tests);
    }
    
    public function testPostEditCircleName() {
        $this->prepareCircleData();
        Session::put('user_id', 1);
        $url = '/circle/edit-circle-name';
        $tests = [
        '{}' => '{"status":"param_error","error":{"id":"\u8bf7\u586b\u5199common.id","name":"\u8bf7\u586b\u5199common.name"}}',
        '{"name":"你见过这样标准的十五个中文字吗", "id":1}' => '{"status":"ok","content":true}',
        ];
        $this->doPost($url, $tests);
    }
    
    public function testPostAddMember() {
        $this->prepareCircleData();
        
        Session::put('user_id', 1);
        $url = '/circle/add-member';
        $tests = [
        '{}' => '{"status":"param_error","error":{"circle_id":"\u8bf7\u586b\u5199common.circle_id","user_id":"\u8bf7\u586b\u5199\u7528\u6237ID"}}',
        '{"circle_id":1, "user_id":5}' => '{"status":"ok","content":true}',
        ];
        $this->doPost($url, $tests);
    }

    public function testPostRemoveMember() {
        $this->prepareCircleData();
        $this->prepareCircleMemberData();
        Session::put('user_id', 1);
        
        $url = '/circle/remove-member';
        $tests = [
        '{}' => '{"status":"param_error","error":{"circle_id":"\u8bf7\u586b\u5199common.circle_id","user_id":"\u8bf7\u586b\u5199\u7528\u6237ID"}}',
        '{"circle_id":1, "user_id":2}' => '{"status":"ok","content":true}',
        ];
        $this->doPost($url, $tests);
    }

    public function testGetMemberCircleInfo() {
        $this->prepareCircleData();
        $this->prepareCircleMemberData();
        
        $circles = array(
                array(
                        'user_id' => 1,
                        'name' => '这个圈子乜有朋友',
                        'member_count' => 2,
                        'create_time' => time()
                )
        );
        DB::table("circles")->insert($circles);
        Session::put('user_id', 1);        
        $url = '/circle/member-circle-info';
        //case 1
        $inputs = array(
            
        );
        
        $response = $this->call('GET', $url, $inputs);
        
         $this->assertEquals('{"status":"param_error","error":{"user_id":"\u8bf7\u586b\u5199\u7528\u6237ID"}}', $response->getContent());
        
        //case 2
        $inputs = array(
            'user_id' => 3
        );
        
        $response = $this->call('GET', $url, $inputs);
        
        $result = json_decode($response->getContent(), true);
        $content = $result['content'];
        $this->assertCount(3, $content);
    }
    protected  function prepareData() {
        $this->prepareUsers();
        $this->prepareCircleData();
        $this->prepareCircleMemberData();
    }
    protected function prepareCircleData() {
        $circles = array(
            array(
                'user_id' => 1,
                'name' => '亲人',
                'member_count' => 2,
                'create_time' => time()
            ),
            array(
                'user_id' => 2,
                'name' => '朋友',
                'member_count' => 1,
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'name' => '家人',
                'member_count' => 2,
                'create_time' => time()
            ),array(
                'user_id' => 5,
                'name' => '朋友',
                'member_count' => 1,
                'create_time' => time()
        ),array(
                'user_id' => 2,
                'name' => '家人',
                'member_count' => 1,
                'create_time' => time()
        )
        );
        DB::table("circles")->insert($circles);
    }
    
    protected function prepareCircleMemberData() {
        $members = array(
                array(
                        'circle_id' => 1,
                        'circle_owner_user_id' => 1,
                        'user_id' => 2,
                        'create_time' => time()
                ),
                array(
                        'circle_id' => 1,
                        'circle_owner_user_id' => 1,
                        'user_id' => 3,
                        'create_time' => time()
                ),
                array(
                        'circle_id' => 2,
                        'circle_owner_user_id' => 2,
                        'user_id' => 1,
                        'create_time' => time()
                ),
                array(
                        'circle_id' => 3,
                        'circle_owner_user_id' => 1,
                        'user_id' => 3,
                        'create_time' => time()
                ),
                array(
                        'circle_id' => 3,
                        'circle_owner_user_id' => 1,
                        'user_id' => 4,
                        'create_time' => time()
                ),array(
                        'circle_id' => 4,
                        'circle_owner_user_id' => 5,
                        'user_id' => 1,
                        'create_time' => time()
                ),array(
                        'circle_id' => 5,
                        'circle_owner_user_id' => 2,
                        'user_id' => 1,
                        'create_time' => time()
                )
        );
        DB::table('circle_members')->insert($members);
    }
    
    protected function prepareUsers() {
        $no = 1;
        //准备用户数据
        $insertUsers = array(
                array(
                        'name' => 'cc1',
                        'cc_no' => $no++,
                        'email' => 'cc1@163.com',
        
                ),array(
                        'name' => 'cc2',
                        'cc_no' => $no++,
                        'email' => 'cc2@163.com',
                ),array(
                        'name' => 'cc3',
                        'cc_no' => $no++,
                        'email' => 'cc3@163.com',
                ),array(
                        'name' => 'cc4',
                        'cc_no' => $no++,
                        'email' => 'cc4@163.com',
                ),array(
                        'name' => 'cc5',
                        'cc_no' => $no++,
                        'email' => 'cc5@163.com',
                ),
        );
        $fills = new ExchangeControllerTest();
        $fills->fillOtherField($insertUsers);

        DB::table('users')->insert($insertUsers);
    }
}
