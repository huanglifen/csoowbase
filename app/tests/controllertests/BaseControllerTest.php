<?php

abstract class BaseControllerTest extends TestCase {

	public function doPost($url, array &$tests) {
		foreach ($tests as $paramString => $result) {
			$paramArray = json_decode($paramString, true);
			if (! $paramArray) {
				$paramArray = [];
			}
			$response = $this->call('POST', $url, $paramArray);
			$this->assertEquals($result, $response->getContent(), $paramString);
		}
	}

}