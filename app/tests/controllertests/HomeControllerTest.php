<?php

class HomeControllerTest extends BaseControllerTest {

    public function setUp() {
        parent::setUp();
        DB::table('feedbacks')->truncate();
        Session::clear();
    }
    
    public function testPostSubmitFeedback() {
        $url ='/home/submit-feedback';
        $tests = [
        '' => '{"status":"not_login"}'
        ];
        $this->doPost($url, $tests);
        
        \Session::put('user_id', 1);
        $tests = [
        '' => '{"status":"param_error","error":{"content":"\u8bf7\u586b\u5199\u610f\u89c1\u53cd\u9988\u5185\u5bb9"}}',
        '{"content":"nice to meet you!"}' => '{"status":"ok","content":1}'
        ];
        $this->doPost($url, $tests);
    }
}