<?php
use App\Modules\ItemModule;
class TaskControllerTest extends BaseControllerTest {
	
	public function setUp() {
		parent::setUp();
		DB::table('contests')->truncate();
		DB::table('task_works')->truncate();
	}
	
	public function testPostPublish() {
		$url = '/task/publish';
		
		$tests = ['{"title":"title"}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
			'' => '{"status":"param_error","error":{"title":"\u8bf7\u586b\u5199\u4efb\u52a1\u540d\u79f0","description":"\u8bf7\u586b\u5199\u4efb\u52a1\u8be6\u7ec6\u4ecb\u7ecd","cover":"\u8bf7\u4e0a\u4f20\u5c01\u9762\u56fe\u7247","submit_work_end_time":"\u8bf7\u586b\u5199\u627f\u8bfa\u5b8c\u6210\u4efb\u52a1\u65f6\u95f4"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","submit_work_end_time":0,"bonus_amount":100}' => '{"status":"param_error","error":{"submit_work_end_time":"\u8bf7\u8bbe\u7f6e\u5929\u6570\u4e3a1-9999\u5929\u7684\u65f6\u95f4"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","submit_work_end_time":10000,"bonus_amount":100}' => '{"status":"param_error","error":{"submit_work_end_time":"\u8bf7\u8bbe\u7f6e\u5929\u6570\u4e3a1-9999\u5929\u7684\u65f6\u95f4"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","submit_work_end_time":100,"bonus_amount":12,"tags":"eru,\u7684\u53d1\u7684_"}' => '{"status":"param_error","error":{"tags":"\u8bf7\u8f93\u5165\u53ea\u542b\u6709\u4e2d\u6587\uff0c\u82f1\u6587\uff0c\u6570\u5b57\u7684\u6807\u7b7e"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","submit_work_end_time":99,"bonus_amount":100}' => '{"status":"ok"}'
 		];
		
		$this->doPost($url, $tests);
	}
	
	public function testPostSubmitWork() {
		$url = '/task/submit-work';
		
		$tests = ['{"task_id":"1"}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
			'' => '{"status":"param_error","error":{"task_id":"\u8bf7\u586b\u5199common.task_id","works":"\u8bf7\u586b\u5199\u4efb\u52a1\u4f5c\u54c1\u63cf\u8ff0","cover":"\u8bf7\u4e0a\u4f20\u4efb\u52a1\u4f5c\u54c1\u5c01\u9762","end_time":"\u8bf7\u586b\u5199\u627f\u8bfa\u5b8c\u6210\u4efb\u52a1\u65f6\u95f4"}}',
			'{"task_id":1,"works":"works","cover":"cover","end_time":111}' => '{"status":"ok"}'	
		];
		
		$this->doPost($url, $tests);
	}
	
	public function testPostCancelWork() {
		$url = '/task/cancel-work';
		
		$tests = ['{"work_id":"1"}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
			'' => '{"status":"param_error","error":{"work_id":"\u8bf7\u586b\u5199common.work_id"}}',
			'{"work_id":1}' => '{"status":"ok"}',
		];
		
		$this->doPost($url, $tests);
	}
	
	public function testPostUpdateWork() {
		$url = '/task/update-work';
		
		$tests = ['{"work_id":"1"}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
		'' => '{"status":"param_error","error":{"work_id":"\u8bf7\u586b\u5199common.work_id","works":"\u8bf7\u586b\u5199\u4efb\u52a1\u4f5c\u54c1\u63cf\u8ff0","cover":"\u8bf7\u4e0a\u4f20\u4efb\u52a1\u4f5c\u54c1\u5c01\u9762","end_time":"\u8bf7\u586b\u5199\u627f\u8bfa\u5b8c\u6210\u4efb\u52a1\u65f6\u95f4"}}',
		'{"work_id":1,"works":"works","cover":"cover","end_time":111}' => '{"status":"ok"}'
				];
		
		$this->doPost($url, $tests);
	}
	
	public function testPostChooseWinner() {
		$url = '/task/choose-winner';
		
		$tests = ['{"work_id":"1"}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
		'' => '{"status":"param_error","error":{"work_id":"\u8bf7\u586b\u5199common.work_id"}}',
		'{"work_id":1}' => '{"status":"ok"}',
		];
		
		$this->doPost($url, $tests);
	}
	
	public function testPostUpdate() {
		$url = '/task/update';
		
		$tests = ['{}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
			'' => '{"status":"param_error","error":{"task_id":"\u8bf7\u586b\u5199common.task_id","title":"\u8bf7\u586b\u5199\u52a8\u6001\u6807\u9898","content":"\u8bf7\u586b\u5199\u52a8\u6001\u5185\u5bb9"}}',
			'{"task_id":1,"title":"title","content":"content"}' => '{"status":"ok"}',
		];
		
		$this->doPost($url, $tests);
	}
	
}

include_once dirname(__FILE__) . '/../ItemAbFix.php';