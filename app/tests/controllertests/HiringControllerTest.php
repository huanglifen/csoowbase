<?php

class HiringControllerTest extends BaseControllerTest {
	
	public function setUp() {
		parent::setUp();
		DB::table('contests')->truncate();
		DB::table('hiring_prizes')->truncate();
		DB::table('expert_invites_internal')->truncate();
	}
	
	public function testGetIndex() {
		$this->insertHiring();
	    
	    $url = '/hiring/index/1';
	    
	    $this->call('GET', $url);
	    $this->assertResponseOk();
	}
	public function testPublish() {
		$url = '/hiring/publish';
		
		$tests = ['{"title":"this is title"}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
			'{"title":"this is title"}' => '{"status":"param_error","error":{"description":"\u8bf7\u586b\u5199\u6bd4\u8d5b\u8be6\u7ec6\u4ecb\u7ecd","cover":"\u8bf7\u4e0a\u4f20\u5c01\u9762\u56fe\u7247","end_time":"\u8bf7\u586b\u5199\u5bfb\u627e\u516c\u76ca\u660e\u661f\u7684\u9650\u5236\u65f6\u95f4"}}',
			'{"title":"this is title","description":"this is description","cover":"cover","hiring_type":"1","end_time":"fasf"}' => '{"status":"param_error","error":{"end_time":"\u8bf7\u8bbe\u7f6e\u5929\u6570\u4e3a1-9999\u5929\u7684\u65f6\u95f4"}}',
			'{"title":"this is title","cover":"cover","description":"this is description","end_time":100,"prizes":[{"title":"title","amount":10,"award":"fdsf"}],"tags":"eru,\u7684\u53d1\u7684_"}' => '{"status":"param_error","error":{"tags":"\u8bf7\u8f93\u5165\u53ea\u542b\u6709\u4e2d\u6587\uff0c\u82f1\u6587\uff0c\u6570\u5b57\u7684\u6807\u7b7e"}}',
			'{"title":"this is title","description":"this is description","cover":"cover","hiring_type":1,"end_time":123,"prizes":[{"title":"title","amount":100,"award":"award"}]}' => '{"status":"ok"}'
		];
		
		$this->doPost($url, $tests);
	}
	
	public function testApply() {
		$url = '/hiring/apply';
		
		$tests = ['{"hiring_id":1,"works":"works","cover":"cover","declaration":"dfas"}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
			'{"hiring_id":1,"works":"works","cover":"cover","declaration":"dfas"}' => '{"status":"ok"}',
			'' => '{"status":"param_error","error":{"hiring_id":"\u8bf7\u586b\u5199common.hiring_id","works":"\u8bf7\u586b\u5199\u4ee3\u8868\u4f5c\u54c1","cover":"\u8bf7\u4e0a\u4f20\u4f5c\u54c1\u5c01\u9762","declaration":"\u8bf7\u586b\u5199\u4e00\u53e5\u8bdd\u516c\u76ca\u5ba3\u8a00"}}',		
		];
		
		$this->doPost($url, $tests);
	}
	
	public function testCancelApply() {
		$url = '/hiring/cancel-apply';
		
		$tests = ['{"participator_id":1}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = ['' => '{"status":"param_error","error":{"participator_id":"\u8bf7\u586b\u5199common.participator_id"}}', '{"participator_id":1}' => '{"status":"ok"}'];
		
		$this->doPost($url, $tests);
	}
	
	public function testUpdateParticipator() {
		$url = '/hiring/update-participator';
		
		$tests = ['{"participator_id":1}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = [
			'{"participator_id":1,"works":"works","cover":"data\/cover\/ddfasd.jpg","declaration":"declaration"}' => '{"status":"ok"}',
			'' => '{"status":"param_error","error":{"participator_id":"\u8bf7\u586b\u5199common.participator_id","works":"\u8bf7\u586b\u5199\u4ee3\u8868\u4f5c\u54c1","cover":"\u8bf7\u4e0a\u4f20\u4f5c\u54c1\u5c01\u9762","declaration":"\u8bf7\u586b\u5199\u4e00\u53e5\u8bdd\u516c\u76ca\u5ba3\u8a00"}}',
		];
		
		$this->doPost($url, $tests);
	}
	
	public function testChooseWinner() {
		$url = '/hiring/choose-winner';
		
		$tests = ['{"participator_id":1}' => '{"status":"not_login"}'];
		
		$this->doPost($url, $tests);
		
		Session::set('user_id', 1);
		
		$tests = ['' => '{"status":"param_error","error":{"participator_id":"\u8bf7\u586b\u5199common.participator_id"}}', '{"participator_id":1}' => '{"status":"ok"}'];
		
		$this->doPost($url, $tests);
	}
	
	public function testPostChooseTag() {
		$this->insertHiring();

	    $data = [
	        [
	            'tag_id' => 1,
	            'affair_type' => 3,
	            'affair_id' => 1
	        ]
	    ];
	    
	    DB::table('tags_affairs')->insert($data);
	    
	    $url = '/hiring/choose-tag';
	    
	    $tests = [
	        '' => '{"status":"param_error","error":{"tag_id":"\u8bf7\u586b\u5199common.tag_id"}}',
	        '{"tag_id":1}' => '{"status":"ok"}'
	    ];
	    
	    $this->doPost($url, $tests);
	}
	
	protected function insertHiring() {
		$data = [
		[
		'no' => 'P23123213',
		'user_id' => 1,
		'title' => 'title',
		'description' => 'description',
		'cover' => 'cover',
		'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		'province_id' => 0,
		'city_id' => 0,
		'status' => 1,
		'start_time' => 1233131234,
		'school_id' => 0,
		'creative_index' => 0,
		'organization_id' => 0,
		'type' => 3,
		'info' => json_encode(array('end_time' => 100)),
		'creative_index'=>0,
		],
		[
		'no' => 'P23123213',
		'user_id' => 1,
		'title' => 'title',
		'description' => 'description',
		'cover' => 'cover',
		'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		'province_id' => 0,
		'city_id' => 0,
		'status' => 1,
		'start_time' => 1233131234,
		'school_id' => 0,
		'creative_index' => 0,
		'organization_id' => 0,
		'type' => 3,
		'info' => json_encode(array('end_time' => 100)),
		'creative_index'=>0,
		]
		];
		 
		DB::table('contests')->insert($data);
	}
}

include_once dirname(__FILE__) . '/../ItemAbFix.php';