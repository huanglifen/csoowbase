<?php
use App\Modules\NotificationModule;

class NotificationControllerTest extends BaseControllerTest {

    public function __construct() {
        parent::__construct();
        include_once 'app/tests/controllertests/ExchangeControllerTest.php';
    }

    public function setUp() {
        parent::setUp();
        DB::table('notifications')->truncate();
        Session::start();
    }

    public function testGetCczNotification() {
        $this->prepareNotifications();
        $url = '/notification/ccz-notification';
        //case 1
        $response = $this->call('GET', $url);
        $this->assertEquals('{"status":"not_login"}', $response->getContent());
        //case 2
        Session::set('user_id', 1);
        $response = $this->call('GET', $url);
        $result = $response->original;
        $content = $result['notifications'];
        $expect = array(
            3,
            2,
            1
        );
        $actual = array();
        foreach ($content as $c) {
            $actual[] = $c['id'];
        }
        $this->assertEquals($expect, $actual);
    }

    public function testGetIndex() {
        $this->prepareNotifications();
        $this->prepareUsers();
        
        $url = '/notification';
        //case 1
        $response = $this->call('GET', $url);
        $this->assertRedirectedTo('/user/login');
        //case 2
        Session::set('user_id', 1);
        $response = $this->call("GET", $url);
        $this->assertResponseOk();
        $this->assertViewHas('notifications');
        $this->assertViewHas('contests');
        $this->assertViewHas('exchanges');
        $this->assertViewHas('carepeople');
    }

    public function testGetNotificationInner() {
        $this->prepareNotifications();
        $this->prepareUsers();
        $url = '/notification/notification-inner';
        //case 1
        $response = $this->call('GET', $url);
        $this->assertEquals('{"status":"not_login"}', $response->getContent());
        //case 2
        Session::set('user_id', 1);
        $response = $this->call('GET', $url);
        $this->assertEquals('{"status":"param_error","error":{"page":"\u8bf7\u586b\u5199common.page","type":"\u8bf7\u586b\u5199common.type"}}', $response->getContent());
        
        $inputs = array(
            'page' => 1,
            'type' => NotificationModule::STATUS_READED
        );
        $response = $this->call('GET', $url, $inputs);
        $this->assertResponseOk();
        $this->assertViewHas('notifications');
    }

    public function prepareNotifications() {
        $inserts = array(
            array(
                'user_id' => 1,
                'content' => 'content',
                'target_type' => 1,
                'info' => 1,
                'status' => NotificationModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'content' => 'content',
                'target_type' => 2,
                'info' => 1,
                'status' => NotificationModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'content' => 'content',
                'target_type' => 1,
                'info' => 1,
                'status' => NotificationModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'user_id' => 2,
                'content' => 'content',
                'target_type' => 1,
                'info' => 1,
                'status' => NotificationModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'content' => 'content',
                'target_type' => 1,
                'info' => 1,
                'status' => NotificationModule::STATUS_READED,
                'create_time' => time()
            )
        );
        DB::table('notifications')->insert($inserts);
    }

    protected function prepareUsers() {
        $no = 1;
        //准备用户数据
        $insertUsers = array(
            array(
                'name' => 'cc1',
                'cc_no' => $no ++,
                'email' => 'cc1@163.com'
            )
        );
        $fills = new ExchangeControllerTest();
        $fills->fillOtherField($insertUsers);
        
        DB::table('users')->insert($insertUsers);
    }
}
