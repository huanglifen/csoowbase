<?php

use App\Modules\BaseModule;
class DynamicControllerTest extends BaseControllerTest {

    public function setUp() {
        parent::setUp();
        DB::table('dynamics')->truncate();
        DB::table('items')->truncate();
        DB::table('comments')->truncate();
        Session::start();
        include_once 'app/tests/controllertests/ExchangeControllerTest.php';
    }
 
    public function testGetIndex() {
        $this->prepareData();
        $url = '/dynamic';
        
        $response = $this->call("GET", $url);
        $this->assertRedirectedTo('user/login');
        
        Session::set('user_id', 1);
        
        $response = $this->call('GET', $url);
        
        $this->assertResponseOk();
        
        $this->assertViewHas('dynamics');
        $this->assertViewHas('circles');
    }
   
    public function testGetDynamic() {
        $this->prepareData();
        $url = '/dynamic/dynamic';
        $response = $this->call("GET", $url);
        $this->assertEquals('{"status":"not_login"}', $response->getContent());
        
        Session::set('user_id', 1);
        
        $inputs = array(
            'circle_id' => 1,
            'page' => 1
        );
        $response = $this->call('GET', $url, $inputs);

        $this->assertResponseOk();
        
        $this->assertViewHas('dynamics');
        

        $inputs = array(
                'circle_id' => 0,
                'page' => 1
        );
        $response = $this->call('GET', $url, $inputs);
        
        $this->assertResponseOk();
        
        $this->assertViewHas('dynamics');
    }
    public function testPostPublishDynamic() {
        $url = '/dynamic/publish-dynamic';
        $tests = [
        '' => '{"status":"not_login"}'
        ];
        $this->doPost($url, $tests);
        
        \Session::put('user_id', 1);
        $tests = [
        '' => '{"status":"param_error","error":{"content":"\u8bf7\u8f93\u5165\u5185\u5bb9"}}',
        '{"content":"nice","attachment_type":1,"pic":"1.jpg"}' => '{"status":"ok","content":1}',
        '{"content":"asd","attachment_type":2,"video":"youku.com"}' => '{"status":"ok","content":2}'
        ];
        $this->doPost($url, $tests);
    }
    
    public function testPostLike() {
        $this->prepareDynamic();
        
        $url = '/dynamic/like';
        $tests = [
        '' => '{"status":"not_login"}'
                ];
        $this->doPost($url, $tests);
        
        Session::set('user_id', 1);
        
        $tests = [
        '' => '{"status":"param_error","error":{"target_id":"\u4e0d\u80fd\u4e3a\u7a7a"}}',
        '{"target_id":"nice"}' => '{"status":"param_error","error":{"target_id":"\u5fc5\u987b\u662f\u81ea\u7136\u6570"}}',
        '{"target_id":1}' => '{"status":"ok","content":1}'
                ];
        $this->doPost($url, $tests);
    }

    public function testPostComment() {
        $this->prepareDynamic();
        
        $url = '/dynamic/comment';
        $tests = [
        '' => '{"status":"not_login"}'
                ];
        $this->doPost($url, $tests);
        
        Session::set('user_id', 1);
        
        $tests = [
        '' => '{"status":"param_error","error":{"content":"\u8bf7\u586b\u5199common.content","target_id":"\u4e0d\u80fd\u4e3a\u7a7a","is_forward":"\u8bf7\u586b\u5199common.is_forward"}}',
        '{"target_id":1,"content":"content","is_forward": 0}' => '{"status":"ok","content":1}',
        '{"target_id":1,"content":"content","is_forward":1}' => '{"status":"ok","content":2}'
                ];
        $this->doPost($url, $tests);
    }
    
    public function testPostForward() {
        $this->prepareDynamic();
        
        $url = '/dynamic/forward';
        $tests = [
        '' => '{"status":"not_login"}'
                ];
        $this->doPost($url, $tests);
        
        Session::set('user_id', 1);
        
        $tests = [
        '' => '{"status":"param_error","error":{"forward_id":"\u8bf7\u586b\u5199common.forward_id","content":"\u8bf7\u586b\u5199common.content"}}',
        '{"forward_id":1,"content":"content"}' => '{"status":"ok","content":5}',
                ];
        $this->doPost($url, $tests);
    }

    public function testGetComments() {
        $this->prepareDynamic();
        $this->prepareComments();
        
        $url = '/dynamic/comments';
        $inputs = array(
            'target_id' => 1,
        );
        
        $response = $this->call("GET", $url, $inputs);
        $this->assertResponseOk();
        $result = json_decode($response->getContent(), true);
        $content = $result['content'];

        $this->assertCount(3, $content);
        
        $inputs = array(
            'target_id' => 2,
            'page' => 1
        );
        $response = $this->call("GET", $url, $inputs);
        $this->assertResponseOk();
        
        $result = json_decode($response->getContent(), true);
        $content = $result['content'];
        $this->assertCount(1, $content);
    }

    public function PostParseVideoUrl() {
        $url = '/dynamic/parse-video-url';
        $tests = [
        '' => '{"status":"param_error","error":{"url":"\u8bf7\u586b\u5199common.url"}}',
        '{"url":"http://news.163.com/14/0217/13/9L9QB2QH00014JB6.html?youdaodict=true"}' => '{"status":"parse_url_fail","error":{"url":"\u4e0d\u80fd\u89e3\u6790\u8be5\u89c6\u9891\uff0c\u8bf7\u6362\u4e00\u4e2a\u8bd5\u8bd5"}}',
        //'{"url":"http://v.youku.com/v_show/id_XNjc0MDQ2NDQ0.html"}' => '{"status":"ok","content":"<embed width=\"465\" height=\"400\" align=\"center\" allowfullscreen=\"true\" allowscriptaccess=\"always\" menu=\"false\"\n\t\tloop=\"false\" play=\"false\" wmode=\"transparent\" src=\"http:\/\/player.youku.com\/player.php\/sid\/XNjc0MDQ2NDQ0\" pluginspage=\"http:\/\/www.macromedia.com\/go\/getflashplayer\"\n\t\tclass=\"edui-faked-video\" type=\"application\/x-shockwave-flash\">"}'
        ];
        $this->doPost($url, $tests);
    }
    
    protected  function prepareData() {
        $this->prepareUsers();
        $this->prepareCircleData();
        $this->prepareCircleMemberData();
        $this->prepareComments();
        $this->prepareDynamic();
    }
    protected function prepareDynamic() {
        $inserts = array(
            array(
                'user_id' => 1,
                'content' => 'content',
                'pic' => '1.jpg',
                'video' => '1.mkv',
                'type' => 1,
                'target_type' => 0,
                'target_id' => 0,
                'like_count' => 0,
                'forward_count' => 0,
                'comment_count' => 0,
                'create_time' => time()
            ),
            array(
                'user_id' => 2,
                'content' => 'content2',
                'pic' => '12.jpg',
                'video' => '12mkv',
                'type' => 2,
                'target_type' => 0,
                'target_id' => 0,
                'like_count' => 0,
                'forward_count' => 0,
                'comment_count' => 0,
                'create_time' => time()
            ),
            array(
                'user_id' => 3,
                'content' => 'content',
                'pic' => '1.jpg',
                'video' => '1.mkv',
                'type' => 1,
                'target_type' => 0,
                'target_id' => 0,
                'like_count' => 0,
                'forward_count' => 0,
                'comment_count' => 0,
                'create_time' => time()
            ),
            array(
                'user_id' => 3,
                'content' => 'content',
                'pic' => '1.jpg',
                'video' => '1.mkv',
                'type' => 1,
                'target_type' => 0,
                'target_id' => 0,
                'like_count' => 0,
                'forward_count' => 0,
                'comment_count' => 0,
                'create_time' => time()
            )
        );
        
        DB::table('dynamics')->insert($inserts);
    }

    protected function prepareComments() {
        $inserts = array(
            array(
                'user_id' => 1,
                'target_type' => BaseModule::TYPE_DYNAMIC,
                'target_id' => 1,
                'content' => 'content',
                'create_time' => time()
            ),
            array(
                'user_id' => 2,
                'target_type' => BaseModule::TYPE_DYNAMIC,
                'target_id' => 1,
                'content' => 'content',
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'target_type' => BaseModule::TYPE_DYNAMIC,
                'target_id' => 2,
                'content' => 'content',
                'create_time' => time()
            ),
            array(
                'user_id' => 3,
                'target_type' => BaseModule::TYPE_PROJECT,
                'target_id' => 1,
                'content' => 'content',
                'create_time' => time()
            ),
            array(
                'user_id' => 2,
                'target_type' => BaseModule::TYPE_DYNAMIC,
                'target_id' => 1,
                'content' => 'content',
                'create_time' => time()
            )
        );
        
        DB::table('comments')->insert($inserts);
    }

    protected function prepareCircleData() {
        DB::table("circles")->truncate();
        $circles = array(
                array(
                        'user_id' => 1,
                        'name' => '亲人',
                        'member_count' => 2,
                        'create_time' => time()
                ),
                array(
                        'user_id' => 2,
                        'name' => '朋友',
                        'member_count' => 1,
                        'create_time' => time()
                ),
                array(
                        'user_id' => 1,
                        'name' => '家人',
                        'member_count' => 2,
                        'create_time' => time()
                )
        );
        DB::table("circles")->insert($circles);
    }
    
    protected function prepareCircleMemberData() {
        DB::table('circle_members')->truncate();
        $members = array(
                array(
                        'circle_id' => 1,
                        'circle_owner_user_id' => 1,
                        'user_id' => 2,
                        'create_time' => time()
                ),
                array(
                        'circle_id' => 1,
                        'circle_owner_user_id' => 1,
                        'user_id' => 3,
                        'create_time' => time()
                ),
                array(
                        'circle_id' => 2,
                        'circle_owner_user_id' => 2,
                        'user_id' => 1,
                        'create_time' => time()
                )
        );
        DB::table('circle_members')->insert($members);
    }
    
    protected function prepareUsers() {
        DB::table('users')->truncate();
        $no = 1;
        //准备用户数据
        $insertUsers = array(
            array(
                'name' => 'cc1',
                'cc_no' => $no ++,
                'email' => 'cc1@163.com'
            ),
            array(
                'name' => 'cc2',
                'cc_no' => $no ++,
                'email' => 'cc2@163.com'
            ),
            array(
                'name' => 'cc3',
                'cc_no' => $no ++,
                'email' => 'cc3@163.com'
            )
        );
        $fills = new ExchangeControllerTest();
        $fills->fillOtherField($insertUsers);
        
        DB::table('users')->insert($insertUsers);
    }
}

