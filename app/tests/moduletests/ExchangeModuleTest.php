<?php
use App\Modules\ExchangeModule;
use App\Modules\ItemModule;
use App\Models\ExchangeTrade;
use App\Models\Tags;
use App\Models\TagsAffairs;
use App\Modules\BaseModule;

class ExchangeModuleTest extends TestCase {
    const TABLE = 'exchange_trades';
    
	public function setUp() {
		parent::setUp();
		DB::table('exchange_trades')->truncate();
		DB::table('tags')->truncate();
		DB::table('tags_affairs')->truncate();
	}
	
	public function testPublish() {
	    //case 1
	    $common = array('title' => '分享我的创意之路', 'cover' => '1.jgp', 'description' => '人的一生中会有无数的机遇，但是真正属于你的可能一个都没有哦，哈哈哈');
	    $price = 2;
	    $tagNames = array('创意', '文艺范','相当好');
	    $userId = 1;
		$tradeId = ExchangeModule::publish($common, $price, $tagNames, $userId);
		$trade = ExchangeTrade::find($tradeId);

		$this->assertNotEmpty($trade);
		
		$tags = Tags::count();
		$this->assertEquals(3, $tags);
		
		$affairs = TagsAffairs::where('affair_type',BaseModule::TYPE_EXCHANGE)->where('affair_id', $tradeId)->count();
		$this->assertEquals(3, $affairs);
		
		//case 2
		$common = array('title' => 'title', 'cover' => '1.jgp', 'description' => 'desc desc');
		$price = 2;
		$tagNames = array('创意','手工制作');
		$userId = 2;
		$tradeId2 = ExchangeModule::publish($common, $price, $tagNames, $userId);
		$trade = ExchangeTrade::find($tradeId);
		
		$this->assertNotEmpty($trade);
		
		$tags = Tags::count();
		$this->assertEquals(4, $tags);
		
		$affairs = TagsAffairs::where('affair_type',BaseModule::TYPE_EXCHANGE)->where('affair_id', $tradeId2)->count();
		$this->assertEquals(2, $affairs);
		
		//case 3
		$common = array('title' => 'title', 'cover' => '1.jgp', 'description' => 'desc desc');
		$price = 2;
		$tagNames = array();
		$userId = 2;
		$tradeId2 = ExchangeModule::publish($common, $price, $tagNames, $userId);
		$trade = ExchangeTrade::find($tradeId);
		
		$this->assertNotEmpty($trade);
		
		$tags = Tags::count();
		$this->assertEquals(4, $tags);
		
		$affairs = TagsAffairs::where('affair_type',BaseModule::TYPE_EXCHANGE)->where('affair_id', $tradeId2)->count();
		$this->assertEquals(0, $affairs);
	}
	
	public function testGetListsByStatus() {
	    $this->prepareData();
	    
	    //case 1
	    $trades = ExchangeModule::getListsByStatus(0);
	    
	    $expect = array(6, 4, 3, 2, 1);	    
	    $this->assertExpect($expect, $trades);
	    
	    //case 2
	    $trades = ExchangeModule::getListsByStatus(ItemModule::STATUS_GOING);
	    
	    $expect = array(6,3, 2, 1);
	    $this->assertExpect($expect, $trades);
	}
	

	public function testGetListsByTagIdAndType() {
	    $this->prepareData();
	    
	    // case1
	    $type = 0;
	    $tag_id = 0;
	    $trades = ExchangeModule::getListsByTagIdAndType($type, $tag_id);
	    
	    $expect = array(6, 4, 3, 2, 1);
	    $this->assertExpect($expect, $trades);
	    

	    //case 2
	    $type = ExchangeModule::LIST_SEARCH_TYPE_GOING;
	    $tag_id = 0;
	    $trades = ExchangeModule::getListsByTagIdAndType($type, $tag_id);
	     
	    $expect = array(6, 3, 2, 1);
	    $this->assertExpect($expect, $trades);
	     
	    //case 3
	    $type = 0;
	    $tag_id = 3;
	    $trades = ExchangeModule::getListsByTagIdAndType($type, $tag_id);
	     
	    $expect = array(4, 3, 2, 1);
	    $this->assertExpect($expect, $trades);
	     
	    //case 4
	    $type = ExchangeModule::LIST_SEARCH_TYPE_HOT;
	    $tag_id = 3;
	    $trades = ExchangeModule::getListsByTagIdAndType($type, $tag_id);
	     
	    $expect = array(2, 1, 3, 4);
	   
	    foreach($trades as $t) {
	        $this->assertTrue(in_array($t->id, $expect));
	    }

	}
	
	public function testGetTradeById() {
	    $this->prepareData();
	
	    //case 1
	    $id = 1;
	    $trade = ExchangeModule::getTradeById($id);
	    $this->assertEquals($id, $trade->id);
	
	    //case 2
	    $id = 100;
	    $trade = ExchangeModule::getTradeById($id);
	    $this->assertEmpty($trade);
	}


	public function testGetTradeRank() {
	    $this->prepareData();
	    
	    $trades = ExchangeModule::getTradeRank();
	    
	    $expect = array(6, 2, 1, 3, 4);
	    
	    $this->assertExpect($expect, $trades);
	}

	public function assertExpect($expect, $items, $property = 'id') {
	    $actual = array();
	    foreach($items as $i) {
	        $actual[] = $i->$property;
	    }
	    
	    $this->assertEquals($expect, $actual);
	}
	
	public  function prepareData() {
	    //准备标签数据
	    $insertTags = array(
	        array(
	         'name' => '文艺范',
	         'type' => 11,   
	    ),array(
	        'name' => '创意',
	        'type' => 11,
	    ),array(
	        'name' => '非主流',
	        'type' => 11,
	    ),array(
	        'name' => '手工制作',
	        'type' => 2
	    ),array(
	        'name' => '非主流',
                'type' => 1
            )
        );
        DB::table('tags')->insert($insertTags);
            
            //准备事务数据
        $insertAffairs = array(
            array(
                'tag_id' => 1,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 1
            ),
            array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 2
            ),
            array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 3
            ),
            array(
                'tag_id' => 2,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 4
            ),
            array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 5
            ), array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 4
        ),array(
                'tag_id' => 3,
                'affair_type' => BaseModule::TYPE_EXCHANGE,
                'affair_id' => 1
        )
        );
        DB::table('tags_affairs')->insert($insertAffairs);
	    //准备交易所数据
	    $i = 1;
	     $inserts = array(
	         array(
	             'no' => $i++,
	             'user_id' => 1,
	             'title' => '交易所',
	             'description' => 'des des des 这里是描述信息',
	             'cover' => '1.jpg',
	             'tags' => json_encode(array('1' => '文艺范', '3' => '非主流')),
	             'province_id' => 0,
	             'city_id' => 0,
	             'district_id' => 0,
	             'school_id' => 0,             
	             'status' => ItemModule::STATUS_GOING,
	             'start_time' => time(),
	             'creative_index' => 20,
	             'price' => 2,
          
	     ),array(
	             'no' => $i++,
	             'user_id' => 2,
	             'title' => '交易所',
	             'description' => 'des des des 这里是描述信息',
	             'cover' => '1.jpg',
	             'tags' => json_encode(array('3' => '非主流')),
	             'province_id' => 0,
	             'city_id' => 0,
	             'district_id' => 0,
	             'school_id' => 0,             
	             'status' => ItemModule::STATUS_GOING,
	             'start_time' => time(),
	             'creative_index' => 30,
	             'price' => 3,
          
	     ),array(
	             'no' => $i++,
	             'user_id' => 3,
	             'title' => '交易所',
	             'description' => 'des des des 这里是描述信息',
	             'cover' => '1.jpg',
	             'tags' => json_encode(array('3' => '非主流')),
	             'province_id' => 0,
	             'city_id' => 0,
	             'district_id' => 0,
	             'school_id' => 0,	             
	             'status' => ItemModule::STATUS_GOING,
	             'start_time' => time(),
	             'creative_index' => 10,
	             'price' => 3,
          
	     ),array(
	             'no' => $i++,
	             'user_id' => 3,
	             'title' => '交易所',
	             'description' => 'des des des 这里是描述信息',
	             'cover' => '1.jpg',
	             'tags' => json_encode(array('3' => '非主流')),
	             'province_id' => 0,
	             'city_id' => 0,
	             'district_id' => 0,
	             'school_id' => 0,             
	             'status' => ItemModule::STATUS_END_SUCCESS,
	             'start_time' => time(),
	             'creative_index' => 5,
	             'price' => 3,
          
	     ),array(
	             'no' => $i++,
	             'user_id' => 2,
	             'title' => '交易所',
	             'description' => 'des des des 这里是描述信息',
	             'cover' => '1.jpg',
	             'tags' => json_encode(array('3' => '非主流',)),
	             'province_id' => 0,
	             'city_id' => 0,
	             'district_id' => 0,
	             'school_id' => 0,            
	             'status' => 0,
	             'start_time' => time(),
	             'creative_index' => 10,
	             'price' => 3,
          
	     ),array(
	             'no' => $i++,
	             'user_id' => 1,
	             'title' => '交易所',
	             'description' => 'des des des 这里是描述信息',
	             'cover' => '1.jpg',
	             'tags' => json_encode(array()),
	             'province_id' => 0,
	             'city_id' => 0,
	             'district_id' => 0,
	             'school_id' => 0,             
	             'status' => ItemModule::STATUS_GOING,
	             'start_time' => time(),
	             'creative_index' => 100,
	             'price' => 30,
          
	     )
	     );
	     DB::table(self::TABLE)->insert($inserts);
	}
	
}

include_once dirname(__FILE__) . '/../ItemAbFix.php';