<?php
use App\Modules\ExpertModule;
use App\Models\ExpertInvitesInternal;
use App\Models\ExpertApplication;
class ExpertModuleTest extends TestCase {

    public function setUp() {
        parent::setUp();
        DB::table('expert_applications')->truncate();
        DB::table('expert_invites_internal')->truncate();
        DB::table('expert_invites_external')->truncate();
        DB::table('users')->truncate();
    }
    
    public function prepareDatas() {
        $data = [
        [
        'name' => 'leon1',
        'cc_no' => 100001,
        'email' => '529909847@qq.com',
        'email_is_verified' => 0,
        'email_verify_code' => '',
        'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
        'password_reset_code' => '',
        'activation_code' => '',
        'group_id' => 2,
        'avatar' => '',
        'creative_index' => 0,
        'coin' => 0,
        'creative_index_bonus_amount' => 10,
        'gender' => 0,
        'birthday' => 0,
        'introduction' => '',
        'tags' => '',
        'mobile_phone_no' => '',
        'mobile_phone_is_verified' => 0,
        'mobile_phone_verify_code' => '',
        'province_id' => 1,
        'city_id' => 2,
        'district_id' => 3,
        'school_id' => 4,
        'organization_id' => 5,
        'expert_organization_id' => 6,
        'expert_duty_id' => 7,
        'expert_field_id' => 9,
        'expert_industry_id' => 8,
        'expert_title' => '',
        'follower_count' => 0,
        'register_time' => time(),
        'register_ip' => '',
        'info' => ''
            ]
            ];
        DB::table('users')->insert($data);
    }
    
    public function testIdentify() {
        $userId = 1;
        $name = 'hijacker';
        $identityCard = '510534563112365360';
        
        $result = ExpertModule::identify($userId, $name, $identityCard);
        $this->assertEquals($result, 1);
    }
    
    public function testApplyExpert() {
        
        $data = [
        [
        'user_id' => 1,
        'name' => 'hijacker',
        'id_card_number' => '24567453412315',
        'industry_id' => 0,
        'enterprise_name' => '',
        'enterprise_duty' => '',
        'organization_id' => 0,
        'duty_id' => 0,
        'attachments' => '',
        'explain' => '',
        'create_time' => time()
        ]
        ];
        
        DB::table('expert_applications')->insert($data);
        
        $candidateInfo = array(
            'industry_id' => 1,
            'enterprise_name' => 'school',
            'enterprise_duty' => 'teacher',
            'organization_id' => 2,
            'duty_id' => 3,
            'attachments' => 'app/image/haha.jpg',
            'explain' => 'so easy!',
            );
        $userId = 10;
        $result = ExpertModule::applyExpert($userId, $candidateInfo);
        $this->assertFalse($result);
        
        
        $userId = 1;
        $result = ExpertModule::applyExpert($userId, $candidateInfo);
        $this->assertTrue($result);
    }
    
    public function testInviteInternalExpert() {
        $userId = 1;
        $targetType = 1;
        $targetId = 1;
        $targetSubId = 2;
        $experUserId = 1;
        $result = ExpertModule::inviteInternalExpert($userId, $targetType, $targetId, $targetSubId, $experUserId);
        $this->assertEquals(1, $result);
    }
    
    public function testDeleteInternalExpert() {
        $data = [
        [
        'user_id' => 1,
        'target_type' => 1,
        'target_id' => 1,
        'target_sub_id' => 2,
        'create_time' => time(),
        'expert_user_id' => 1
        ]
        ];
        DB::table('expert_invites_internal')->insert($data);

        ExpertModule::deleteInternalExpert(1);
        
        $result = ExpertInvitesInternal::find(1);
        $this->assertEmpty($result);
    }
    
    public function testInviteExternalExpert() {
        $userId = 1;
        $targetType = 1;
        $targetId = 1;
        $targetSubId = 2;
        $expertInfo = array(
            'name' => 'hijacker',
            'industry' => 'aa',
            'email' => '1432@1234.com',
            'mobile_phone_no' => 1232141556,
            'contact_info' => 'dasfa'
            );
        $result = ExpertModule::inviteExternalExpert($userId, $targetType, $targetId, $expertInfo, $targetSubId);
        $this->assertEquals(1, $result);
    }
    
    public function testSearchApplicationByID() {
        $data = [
        [
        'user_id' => 1,
        'name' => 'hijacker',
        'id_card_number' => '24567453412315',
        'industry_id' => 0,
        'enterprise_name' => '',
        'enterprise_duty' => '',
        'organization_id' => 0,
        'duty_id' => 0,
        'attachments' => '',
        'explain' => '',
        'create_time' => time()
        ]
        ];
        
        DB::table('expert_applications')->insert($data);
        
        $result = ExpertModule::searchApplicationByID(1);
        $this->assertEquals(1, count($result));
    }
    
    public function testGetExpertsByCondition() {
        $this->prepareDatas();
        $result = ExpertModule::getExpertsByCondition();
        $this->assertEquals(1, count($result));
        
        $result = ExpertModule::getExpertsByCondition(2,2,3);
        $this->assertEquals(0, count($result));
        
        $result = ExpertModule::getExpertsByCondition(1,2,3,6,7,9,8);
        $this->assertEquals(1, count($result));
    }
    
    public function testCountexperts() {
        $this->prepareDatas();
        $this->prepareDatas();
        
        $result = ExpertModule::countexperts();
        $this->assertEquals(2, $result);
    }
    
    public function testCanApplyExpert() {
        $data = [
        [
        'name' => 'leon1',
        'cc_no' => 100001,
        'email' => '529909847@qq.com',
        'email_is_verified' => 0,
        'email_verify_code' => '',
        'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
        'password_reset_code' => '',
        'activation_code' => '',
        'group_id' => 2,
        'avatar' => 'image/apple.jpg',
        'creative_index' => 1000000,
        'coin' => 0,
        'creative_index_bonus_amount' => 10,
        'gender' => 0,
        'birthday' => 0,
        'introduction' => '',
        'tags' => '',
        'mobile_phone_no' => '',
        'mobile_phone_is_verified' => 1,
        'mobile_phone_verify_code' => '',
        'province_id' => 1,
        'city_id' => 2,
        'district_id' => 3,
        'school_id' => 4,
        'organization_id' => 5,
        'expert_organization_id' => 6,
        'expert_duty_id' => 7,
        'expert_field_id' => 9,
        'expert_industry_id' => 8,
        'expert_title' => '',
        'follower_count' => 0,
        'register_time' => time(),
        'register_ip' => '',
        'info' => ''
            ]
            ];
        DB::table('users')->insert($data);
        
        $this->prepareDatas();
        
        $result = ExpertModule::canApplyExpert(1);
        $this->assertTrue($result);
    }
}