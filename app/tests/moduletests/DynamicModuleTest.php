<?php
use App\Modules\DynamicModule;
use App\Models\Dynamic;
use App\Modules\BaseModule;

class DynamicModuleTest extends TestCase {
    
    public function setUp() {
        parent::setUp();
        DB::table('dynamics')->truncate();
        DB::table('comments')->truncate();
        include_once 'app/tests/moduletests/ExchangeModuleTest.php';
    }
    
    public function testCreateDynamic() {
        $userId = 1;
        $content = 'content';
        $pic = 'pic.jpg';
        $video = 'youku.html';
        
        $result = DynamicModule::createDynamic($userId, $content, $pic, $video);
        
        $this->assertTrue($result == 1);
        
        $count = Dynamic::count();
        $this->assertEquals(1, $count);
    }
    
    public function testCreateForward() {
        $this->prepareDynamic();
        //case 1
        $userId = 1;
        $forwardId = 100;
        $content = '转发';
        
        $result = DynamicModule::createForward($userId, $forwardId, $content);
        $this->assertFalse($result);
        
        //case 2
        $userId = 1;
        $forwardId = 1;
        $content = '转发';
        
        $result = DynamicModule::createForward($userId, $forwardId, $content);
        
        
        $this->assertTrue(is_numeric($result));
        
        $dynamic = Dynamic::find($forwardId);
        
        $this->assertEquals(1, $dynamic->forward_count);
    }
    
    public function testCreateDynamicComment() {
        $this->prepareDynamic();
        //case 1
        $userId = 2;
        $content = 'haoya';
        $targetId = 100;
        
        $result = DynamicModule::createDynamicComment($userId, $targetId, $content);
        $this->assertFalse($result);
        //case 2
        $userId = 2;
        $content = 'haoya';
        $targetId = 1;
        
        $result = DynamicModule::createDynamicComment($userId, $targetId, $content);
        $this->assertTrue(is_numeric($result));
        
        $dynamic = Dynamic::find($targetId);
        $this->assertEquals(1, $dynamic->comment_count);
    }
    
    public function testSaveLike() {
        $this->prepareDynamic();
        //case 1
        $userId = 1;
        $targetId = 100;
        
        $result = DynamicModule::saveLike($userId, $targetId);
        $this->assertFalse($result);
        
        //case 2
        $userId = 1;
        $targetId = 1;
        $result = DynamicModule::saveLike($userId, $targetId);
        $this->assertTrue(is_numeric($result));
        
        $dynamic = Dynamic::find($targetId);
        $this->assertEquals(1, $dynamic->like_count);
    }
    
    public function testGetDynamcicsByUserIds() {
        $this->prepareDynamic();
        $this->prepareDynamic();
        $this->prepareDynamic();//多准备几条数据
        
        //case 1
        $userIds = array(1, 3);
        $results = DynamicModule::getDynamcicsByUserIds($userIds);
        $expects = array(12, 11, 9, 8, 7, 5, 4, 3, 1);
        
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $results);
        
        //case 2
        $userIds = 3;
        $results = DynamicModule::getDynamcicsByUserIds($userIds);
        $expects = array(12, 11, 8, 7, 4, 3);
        
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $results);
        
        //case 3
        $userIds = array(1, 2, 3);
        $results = DynamicModule::getDynamcicsByUserIds($userIds, 0, 30, 2);
        $expects = array(10, 6, 2);
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $results);
    }
    
    public function testGetDynamicComments() {
        $this->prepareComments();
        
        $targetId = 1;
        $results = DynamicModule::getDynamicComments($targetId);

        $expects = array(5, 2, 1);
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $results);
    }
    protected function prepareDynamic() {
        $inserts = array(
            array(
             'user_id' => 1,
                'content' => 'content',
                'pic' => '1.jpg',
                'video' => '1.mkv',
                'type' => 1,
                'target_type' => 0,
                'target_id' => 0,
                'like_count' => 0,
                'forward_count' => 0,
                'comment_count' => 0,
                'create_time' => time(),   
        ), array(
             'user_id' => 2,
                'content' => 'content2',
                'pic' => '12.jpg',
                'video' => '12mkv',
                'type' => 2,
                'target_type' => 0,
                'target_id' => 0,
                'like_count' => 0,
                'forward_count' => 0,
                'comment_count' => 0,
                'create_time' => time(),   
        ), array(
             'user_id' => 3,
                'content' => 'content',
                'pic' => '1.jpg',
                'video' => '1.mkv',
                'type' => 1,
                'target_type' => 0,
                'target_id' => 0,
                'like_count' => 0,
                'forward_count' => 0,
                'comment_count' => 0,
                'create_time' => time(),   
        ),array(
             'user_id' => 3,
                'content' => 'content',
                'pic' => '1.jpg',
                'video' => '1.mkv',
                'type' => 1,
                'target_type' => 0,
                'target_id' => 0,
                'like_count' => 0,
                'forward_count' => 0,
                'comment_count' => 0,
                'create_time' => time(),   
        ),
        );
        
        DB::table('dynamics')->insert($inserts);
    }
    
    protected function prepareComments() {
        $inserts = array(
            array(
                'user_id' => 1,
                'target_type' => BaseModule::TYPE_DYNAMIC,
                'target_id' => 1,
                'content' => 'content',
                'create_time' => time()
        ),array(
                'user_id' => 2,
                'target_type' => BaseModule::TYPE_DYNAMIC,
                'target_id' => 1,
                'content' => 'content',
                'create_time' => time()
        ),array(
                'user_id' => 1,
                'target_type' => BaseModule::TYPE_DYNAMIC,
                'target_id' => 2,
                'content' => 'content',
                'create_time' => time()
        ),array(
                'user_id' => 3,
                'target_type' => BaseModule::TYPE_PROJECT,
                'target_id' => 1,
                'content' => 'content',
                'create_time' => time()
        ),array(
                'user_id' => 2,
                'target_type' => BaseModule::TYPE_DYNAMIC,
                'target_id' => 1,
                'content' => 'content',
                'create_time' => time()
        )
        );
        
        DB::table('comments')->insert($inserts);
    }
}
