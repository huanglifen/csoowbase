<?php
use App\Modules\ProjectModule;
use App\Modules\ItemModule;
use App\Models\ProjectInvestments;

class ProjectModuleTest extends TestCase {
	
	public function setUp() {
		parent::setUp();
		DB::table('contests')->truncate();
		DB::table('updates')->truncate();
	}
	
	public function testPublish() {
		$result = ProjectModule::publish(
				array('title' => 'this is title', 'cover' => 'cover', 'description' => 'this is description'), 
				1, 
				100, 
				100, 
				111111111, 
				'this is reward description', 
				'pics',
				array('PHP','Mysql')
		);
		
		$this->assertEquals(1, $result);
		
		$project = ProjectModule::getProjectById(1);
		
		$this->assertEquals(1, $project->id);
	}
	
	public function testCreateProjectUpdate() {
		$result = ProjectModule::createProjectUpdate(1, 'this is title', 'this is content', 1);
		
		$this->assertEquals(false, $result);
		
		ProjectModule::publish(array('title' => 'this is title', 'cover' => 'cover', 'description' => 'this is description'), 1, 1, 1, 1, 'this is reward description', 'pics',array('PHP','Mysql'));
		
		$result = ProjectModule::createProjectUpdate(1, 'this is title', 'this is content', 1);
		
		$this->assertEquals(1, $result);
	}
	
	public function testInvest() {
		$this->insertProject();
		
		$result = ProjectModule::invest(1, 1, 100, 2, 20, 1231231231, 0, 'requirement');
		
		$this->assertEquals(true, $result);
		
		$result = ProjectModule::invest(2, 1, 100, 2, 20, 1231231231, 0, 'requirement');
		
		$this->assertEquals(false, $result);
	}
	
	public function testAcceptInvest() {
		$this->insertProject();
		$this->insertInvest();
		
		$result = ProjectModule::acceptInvest(1, 1);
		
		$this->assertEquals(true, $result);
		
		$result = ProjectModule::acceptInvest(1, 2);
		
		$this->assertEquals(false, $result);
	}
	
	public function testCancelInvest() {
		$this->insertProject();
		$this->insertInvest();
		
		$result = ProjectModule::cancelInvest(1, 2);
		
		$this->assertEquals(true, $result);
		
		$result = ProjectModule::cancelInvest(1, 1);
		
		$this->assertEquals(false, $result);
	}
	
	public function testUpdateInvest() {
		$this->insertInvest();
		$this->insertProject();
		
		$result = ProjectModule::updateInvest(1, 2, array('amount' => 200, 'type' => '', 'deposit_percentage' => '','final_payment_pay_time' => '','require_complete_time' => '', 'requirement' => 'updateInvest'));
		
		$invest = ProjectInvestments::find(1);
		
		$this->assertEquals('updateInvest', $invest->requirement);
	}
	
	protected function insertProject() {
		$data = [[
		'no' => 'P23123213',
		'user_id' => 1,
		'title' => 'title',
		'description' => 'description',
		'cover' => 'cover',
		'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		'province_id' => 0,
		'city_id' => 0,
		'status' => 1,
		'start_time' => 1233131234,
		'school_id' => 0,
		'creative_index' => 0,
		'organization_id' => 0,
		'type' => 1,
		'info' => json_encode(array('investment_end_time' => 100, 'goal_amount' => 100)),
		'creative_index'=>0,
		]];
		DB::table('contests')->insert($data);
	
		$data = [
		['name' => 'test','type' => 1 ],
		['name' => '的发的', 'type' => 1]
		];
		DB::table('tags')->insert($data);
	
		$data = [
		['tag_id' => 1, 'affair_type' => 1, 'affair_id' => 1],
		['tag_id' => 2, 'affair_type' => 1, 'affair_id' => 1],
		];
		DB::table('tags_affairs')->insert($data);
	}
	
	protected function insertInvest() {
		$data = [[
		'project_id' => 1,
		'user_id' => 2,
		'amount' => 200,
		'type' => 1,
		'deposit_percentage' => 20,
		'final_payment_pay_time' => 1234124,
		'require_complete_time' => 42134,
		'requirement' => 'requirement',
		'status' => 1,
		'create_time' => time(),
		]];
		DB::table('project_investments')->insert($data);
	}
	
}

include_once dirname(__FILE__) . '/../ItemAbFix.php';