<?php
use App\Modules\CreativeValueRatingModule;
use App\Modules\CreativeIndexModule;
class CreativeValueRatingModuleTest extends TestCase {
	public function setUp() {
		parent::setUp ();
		DB::table ( 'creative_value_ratings' )->truncate ();
		DB::table ( 'users' )->truncate ();
		DB::table ( 'contests' )->truncate ();
		DB::table ( 'task_works' )->truncate ();
		DB::table ( 'regions' )->truncate ();
		DB::table ( 'schools' )->truncate ();
		DB::table ( 'organizations' )->truncate ();
	}
	public function testsaveCreativeValueRating() {
		$this->prepareRegionData ();
		$this->prepareSchoolData ();
		$this->prepareOrganiztionData ();
		$this->prapareContestData ();
		$this->prepareTaskWorkData ();
		$this->prepareUserData ();
		
		CreativeIndexModule::listenEvent ();
		
		$result = CreativeValueRatingModule::saveCreativeValueRating ( 1, 1, 1, 5 );
		$this->assertEquals ( 1, $result );
		
		$result = CreativeValueRatingModule::saveCreativeValueRating ( 1, 21, 1, 4 );
		$this->assertEquals ( 2, $result );
	}
	public function testisExist() {
		CreativeValueRatingModule::saveCreativeValueRating ( 1, 1, 1, 5 );
		$result = CreativeValueRatingModule::isExist ( 1, 1, 1 );
		$this->assertEquals ( true, $result );
		$result = CreativeValueRatingModule::isExist ( 1, 1, 2 );
		$this->assertEquals ( false, $result );
	}
	protected function prepareRegionData() {
		$inserts = array (
				array (
						'id' => 32,
						'name' => '四川',
						'logo' => 'logo',
						'creative_index' => 0,
						'intro' => '1.mkv',
						'type' => 2,
						'nation_id' => 1,
						'province_id' => 0,
						'city_id' => 0,
						'contests_count' => 350,
						'trades_count' => 470,
						'experts_count' => 980,
						'users_count' => 5650 
				),
				array (
						'id' => 3201,
						'name' => '成都',
						'logo' => 'logo',
						'creative_index' => 0,
						'intro' => '1.mkv',
						'type' => 3,
						'nation_id' => 1,
						'province_id' => 32,
						'city_id' => 0,
						'contests_count' => 50,
						'trades_count' => 60,
						'experts_count' => 40,
						'users_count' => 650 
				),
				array (
						'id' => 320101,
						'name' => '武侯区',
						'logo' => 'logo',
						'creative_index' => 0,
						'intro' => '1.mkv',
						'type' => 4,
						'nation_id' => 1,
						'province_id' => 32,
						'city_id' => 3201,
						'contests_count' => 10,
						'trades_count' => 170,
						'experts_count' => 20,
						'users_count' => 250 
				),
				array (
						'id' => 320103,
						'name' => '金牛区',
						'logo' => 'logo',
						'creative_index' => 0,
						'intro' => '1.mkv',
						'type' => 4,
						'nation_id' => 1,
						'province_id' => 32,
						'city_id' => 3201,
						'contests_count' => 10,
						'trades_count' => 170,
						'experts_count' => 20,
						'users_count' => 250 
				) 
		);
		
		DB::table ( 'regions' )->insert ( $inserts );
	}
	protected function prepareSchoolData() {
		$inserts = array (
				array (
						'name' => '四川大学',
						'logo' => 'logo',
						'creative_index' => 0,
						'intro' => '1.mkv',
						'type' => 1,
						'nation_id' => 1,
						'province_id' => 32,
						'city_id' => 3201,
						'district_id' => 320101,
						'contests_count' => 350,
						'trades_count' => 470,
						'experts_count' => 980,
						'users_count' => 5650 
				),
				array (
						'name' => '电子科技大学',
						'logo' => 'logo',
						'creative_index' => 0,
						'intro' => '1.mkv',
						'type' => 1,
						'nation_id' => 1,
						'province_id' => 32,
						'city_id' => 3201,
						'district_id' => 320101,
						'contests_count' => 50,
						'trades_count' => 60,
						'experts_count' => 40,
						'users_count' => 650 
				) 
		);
		
		DB::table ( 'schools' )->insert ( $inserts );
	}
	protected function prepareOrganiztionData() {
		$inserts = array (
				array (
						'name' => '四川大学控股有限公司',
						'logo' => 'logo',
						'creative_index' => 0,
						'intro' => '1.mkv',
						'type' => 1,
						'nation_id' => 1,
						'province_id' => 32,
						'city_id' => 3201,
						'district_id' => 320101,
						'contests_count' => 350,
						'trades_count' => 470,
						'experts_count' => 980,
						'users_count' => 5650 
				),
				array (
						'name' => '电子科技大学控股有限公司',
						'logo' => 'logo',
						'creative_index' => 0,
						'intro' => '1.mkv',
						'type' => 1,
						'nation_id' => 1,
						'province_id' => 32,
						'city_id' => 3201,
						'district_id' => 320101,
						'contests_count' => 50,
						'trades_count' => 60,
						'experts_count' => 40,
						'users_count' => 650 
				) 
		);
		
		DB::table ( 'organizations' )->insert ( $inserts );
	}
	protected function prapareContestData() {
		$data = [ 
				[ 
						'no' => 'P23123213',
						'user_id' => 1,
						'title' => 'title',
						'description' => 'description',
						'cover' => 'cover',
						'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
						'province_id' => 32,
						'city_id' => 3201,
						'status' => 1,
						'start_time' => 1233131234,
						'school_id' => 1,
						'creative_index' => 0,
						'organization_id' => 1,
						'type' => 1,
						'info' => json_encode ( array (
								'investment_end_time' => 100,
								'goal_amount' => 100 
						) ),
						'creative_index' => 0 
				],
				[ 
						'no' => 'T23123213',
						'user_id' => 1,
						'title' => '任务人才title',
						'description' => 'description',
						'cover' => 'cover',
						'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
						'province_id' => 32,
						'city_id' => 3201,
						'status' => 1,
						'start_time' => 1233131234,
						'school_id' => 1,
						'creative_index' => 0,
						'organization_id' => 1,
						'type' => 2,
						'info' => json_encode ( array (
								'investment_end_time' => 100,
								'goal_amount' => 100 
						) ),
						'creative_index' => 0 
				] 
		];
		DB::table ( 'contests' )->insert ( $data );
	}
	protected function prepareTaskWorkData() {
		$data = [ 
				[ 
						'task_id' => 2,
						'user_id' => 1,
						'cover' => 'cover',
						'works' => 'works1',
						'promise_complete_time' => 20000,
						'status' => 1,
						'create_time' => time () 
				],
				[ 
						'task_id' => 2,
						'user_id' => 1,
						'cover' => 'cover',
						'works' => 'works2',
						'promise_complete_time' => 20000,
						'status' => 1,
						'create_time' => time () 
				] 
		];
		
		DB::table ( 'task_works' )->insert ( $data );
	}
	protected function prepareUserData() {
		$data = [ 
				[ 
						'name' => 'leon1',
						'cc_no' => 100001,
						'email' => '529909847@qq.com',
						'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
						'password_reset_code' => 'IVUUZ-HK71U-WEX4M-KYR54-0Z47M',
						'activation_code' => '',
						'group_id' => 1,
						'avatar' => '',
						'creative_index' => 0,
						'coin' => 0,
						'gender' => 0,
						'birthday' => 0,
						'introduction' => '川大学生',
						'province_id' => 32,
						'city_id' => 3201,
						'district_id' => 320101,
						'school_id' => 1,
						'organization_id' => 1 
				],
				[ 
						'name' => 'leon1',
						'cc_no' => 100001,
						'email' => '529909847@qq.com',
						'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
						'password_reset_code' => 'IVUUZ-HK71U-WEX4M-KYR54-0Z47M',
						'activation_code' => '',
						'group_id' => 2,
						'avatar' => '',
						'creative_index' => 0,
						'coin' => 0,
						'gender' => 1,
						'birthday' => 0,
						'introduction' => '电子科大专家',
						'province_id' => 32,
						'city_id' => 3201,
						'district_id' => 320103,
						'school_id' => 2,
						'organization_id' => 2 
				] 
		];
		DB::table ( 'users' )->insert ( $data );
	}
}
