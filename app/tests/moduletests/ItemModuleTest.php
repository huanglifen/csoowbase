<?php

use App\Modules\ItemModule;
class ItemModuleTest extends TestCase {
    
    public function setUp() {
        parent::setUp();
        DB::table('items')->truncate();
    }
    
    public function testsaveItem() {

		$result = ItemModule::saveItem(1, 1, 1, 1,'发布投资项目比赛1');
	    $this->assertEquals(1, $result);
	    
	    $result = ItemModule::saveItem(1, 2, 1, 1,'参与投资项目比赛1');
	    $this->assertEquals(2, $result);
	    
	    
	    $result = ItemModule::saveItem(1, 3, 1, 1,'喜欢投资项目比赛1');
	    $this->assertEquals(3, $result);
	    
	    
	    $result = ItemModule::saveItem(2, 1, 2, 1,'发布任务人才1');
	    $this->assertEquals(4, $result);
	    

    }

}
