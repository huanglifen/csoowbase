<?php
use App\Modules\GuessModule;
use App\Models\Project;
use App\Models\Task;
use App\Models\Hiring;

class GuessModuleTest extends TestCase {
   
    public function setUp() {
        parent::setUp();
        DB::table('guesses')->truncate();
        DB::table('projects')->truncate();
	    DB::table('tasks')->truncate();
	    DB::table('hirings')->truncate();
    }
    
    public function testPlayGuess() {

        $this->prepareData();

	    $result = GuessModule::playGuess(1,1,1,1,100);
        $this->assertEquals(1, $result);

		$result = Project::find(1);
	    $toal = $result->guess_total_amount;
	    $success = $result->guess_success_amount;
	    $this->assertEquals(100, $toal);
	    $this->assertEquals(100, $success);


	    $result = GuessModule::playGuess(1,1,1,0,50);
	    $this->assertEquals(2, $result);

	    $result = Project::find(1);
	    $toal = $result->guess_total_amount;
	    $success = $result->guess_success_amount;
	    $this->assertEquals(100, $success);
	    $this->assertEquals(150, $toal);

	    $result = GuessModule::playGuess(1,2,1,0,50);
	    $this->assertEquals(3, $result);
	    $result = Task::find(1);
	    $toal = $result->guess_total_amount;
	    $success = $result->guess_success_amount;
	    $this->assertEquals(50, $toal);
	    $this->assertEquals(0, $success);

	    $result = GuessModule::playGuess(1,3,1,1,50);
	    $this->assertEquals(4, $result);
	    $result = Hiring::find(1);
	    $toal = $result->guess_total_amount;
	    $success = $result->guess_success_amount;
	    $this->assertEquals(50, $toal);
	    $this->assertEquals(50, $success);

    }

	public function testExpectAward() {
		$this->prepareAwardData();

		$result = GuessModule::expectAward(1,1,0,100);
		$this->assertEquals(195, $result);

		$result = GuessModule::expectAward(1,1,1,100);
		$this->assertEquals(195, $result);

		$result = GuessModule::expectAward(2,1,0,50);
		$this->assertEquals(51, $result);

		$result = GuessModule::expectAward(3,1,1,50);
		$this->assertEquals(51, $result);

	}

    protected function prepareData() {

	    $data = [[
		    'no' => 'P23123213',
		    'user_id' => 1,
		    'title' => 'title',
		    'description' => 'description',
		    'cover' => 'cover',
		    'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		    'province_id' => 0,
		    'city_id' => 0,
		    'district_id'=>0,
		    'school_id' => 0,
		    'government_enterprise_id' => 0,
		    'status' => 1,
		    'start_time' => 1233131234,
		    'creative_index' => 0,
		    'guess_total_amount'=>0,
		    'guess_success_amount'=>0,
		    'investment_end_time' => 100,
		    'goal_amount' => 0,
	    ]];
	    DB::table('projects')->insert($data);

	    $data = [[
		    'no' => 'P23123213',
		    'user_id' => 1,
		    'title' => 'title',
		    'description' => 'description',
		    'cover' => 'cover',
		    'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		    'province_id' => 0,
		    'city_id' => 0,
		    'district_id'=>0,
		    'school_id' => 0,
		    'government_enterprise_id' => 0,
		    'status' => 1,
		    'start_time' => 1233131234,
		    'creative_index' => 0,
		    'guess_total_amount'=>0,
		    'guess_success_amount'=>0,
		    'end_time' => 0,
		    'award' => '一瓶牛奶',
	    ]];
	    DB::table('tasks')->insert($data);

	    $data = [[
		    'no' => 'P23123213',
		    'user_id' => 1,
		    'title' => 'title',
		    'description' => 'description',
		    'cover' => 'cover',
		    'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		    'province_id' => 0,
		    'city_id' => 0,
		    'district_id'=>0,
		    'school_id' => 0,
		    'government_enterprise_id' => 0,
		    'status' => 1,
		    'start_time' => 1233131234,
		    'creative_index' => 0,
		    'guess_total_amount'=>0,
		    'guess_success_amount'=>0,
		    'end_time' => 0,
	    ]];
	    DB::table('hirings')->insert($data);
    }


	protected function prepareAwardData(){

		$data = [[
			'no' => 'P23123213',
			'user_id' => 1,
			'title' => 'title',
			'description' => 'description',
			'cover' => 'cover',
			'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
			'province_id' => 0,
			'city_id' => 0,
			'district_id'=>0,
			'school_id' => 0,
			'government_enterprise_id' => 0,
			'status' => 1,
			'start_time' => 1233131234,
			'creative_index' => 0,
			'guess_total_amount'=>200,
			'guess_success_amount'=>100,
			'investment_end_time' => 100,
			'goal_amount' => 0,
		]];
		DB::table('projects')->insert($data);

		$data = [[
			'no' => 'P23123213',
			'user_id' => 1,
			'title' => 'title',
			'description' => 'description',
			'cover' => 'cover',
			'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
			'province_id' => 0,
			'city_id' => 0,
			'district_id'=>0,
			'school_id' => 0,
			'government_enterprise_id' => 0,
			'status' => 1,
			'start_time' => 1233131234,
			'creative_index' => 0,
			'guess_total_amount'=>50,
			'guess_success_amount'=>50,
			'end_time' => 0,
			'award' => '一瓶牛奶',
		]];
		DB::table('tasks')->insert($data);

		$data = [[
			'no' => 'P23123213',
			'user_id' => 1,
			'title' => 'title',
			'description' => 'description',
			'cover' => 'cover',
			'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
			'province_id' => 0,
			'city_id' => 0,
			'district_id'=>0,
			'school_id' => 0,
			'government_enterprise_id' => 0,
			'status' => 1,
			'start_time' => 1233131234,
			'creative_index' => 50,
			'guess_total_amount'=>0,
			'guess_success_amount'=>0,
			'end_time' => 0,
		]];
		DB::table('hirings')->insert($data);
	}

}
