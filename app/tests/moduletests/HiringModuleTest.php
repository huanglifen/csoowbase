<?php
use App\Modules\HiringModule;
use App\Modules\ItemModule;

class HiringModuleTest extends TestCase {
	
	public function setUp() {
		parent::setUp();
		DB::table('contests')->truncate();
		DB::table('hiring_prizes')->truncate();
		DB::table('expert_invites_internal')->truncate();
		DB::table('hiring_participators')->truncate();
	}
	
	public function testPublish() {
		$result = HiringModule::publish(array('title' => 'title', 'cover' => 'cover', 'description' => 'des'), 20, 1, array(array('title' => 'title', 'amount' => 100, 'award' => 'award')),array('apple'), 1);
		
		$this->assertEquals(1, $result);
		
		$hiring = HiringModule::getHiringById(1);
		
		$this->assertEquals(1, $hiring->id);
	}
	
	
	public function testApplyHiringParticipator() {
		HiringModule::publish(array('title' => 'title', 'cover' => 'cover', 'description' => 'des'), 20, 1, array(array('title' => 'title', 'amount' => 100, 'award' => 'award')),array('apple'), 1);
		
		$result = HiringModule::applyHiringParticipator(array(
			'works' => 'work', 'cover' => 'cover', 'declaration' => 'declaration'
		), 1, 1);
		
		$this->assertEquals(1, $result);
	}
	
	public function testCancelApply() {
		$this->insertHiring();
		$this->insertParticipator();
		
		$result = HiringModule::cancelApply(1, 2);
		
		$this->assertEquals(true, $result);
		
		$result = HiringModule::cancelApply(1, 1);
		
		$this->assertEquals(false, $result);
	}
	
	public function testUpdateParticipator() {
		$this->insertHiring();
		$this->insertParticipator();
		
		$result = HiringModule::updateParticipator(1, array('works' => 'works', 'cover' => 'cover', 'declaration' => 'update declaration'), 2);
		
		$this->assertEquals(true, $result);
		
		$result = HiringModule::updateParticipator(1, array('works' => 'works', 'cover' => 'cover', 'declaration' => 'update declaration'), 1);
		
		$this->assertEquals(false, $result);
	}
	
	public function testChooseWinner() {
		$this->insertHiring();
		$this->insertParticipator();
		
		$result = HiringModule::chooseWinner(1, 1);
		
		$this->assertEquals(true, $result);
		
		$result = HiringModule::chooseWinner(1, 2);
		
		$this->assertEquals(false, $result);
	}
	
	public function testSearchHirings() {
	    $this->insertHiring();
	    
	    $result = HiringModule::searchHirings('');
	    $this->assertEquals(2, count($result));
	    
	    $result = HiringModule::searchHirings(1);
	    $this->assertEquals(1, count($result));
	    foreach ($result as $value) {
	        $this->assertEquals(1, $value->status);
	    }
	    
	    $result = HiringModule::searchHirings(-1);
	    $this->assertFalse($result);
	}
	
	public function testGetListsByTagId() {
		$this->insertHiring();
	    
	    $data = [
	    [
	    'tag_id' => 1,
	    'affair_type' => 3,
	    'affair_id' => 1
	    ]
	    ];
	     
	    DB::table('tags_affairs')->insert($data);
	    
	    $result = HiringModule::getListsByTagId(1);
	    $actual = array();
	    foreach ($result as $value) {
	        $actual[] = $value->id;
	    }
	    $this->assertEquals(array(1), $actual);
	}
	
	protected function insertHiring() {
		$data = [
		[
		'no' => 'P23123213',
		'user_id' => 1,
		'title' => 'title',
		'description' => 'description',
		'cover' => 'cover',
		'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		'province_id' => 0,
		'city_id' => 0,
		'status' => 1,
		'start_time' => 1233131234,
		'school_id' => 0,
		'creative_index' => 0,
		'organization_id' => 0,
		'type' => 3,
		'info' => json_encode(array('end_time' => 100)),
		'creative_index'=>0,
		],
		[
		'no' => 'P23123213',
		'user_id' => 1,
		'title' => 'title',
		'description' => 'description',
		'cover' => 'cover',
		'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		'province_id' => 0,
		'city_id' => 0,
		'status' => 2,
		'start_time' => 1233131234,
		'school_id' => 0,
		'creative_index' => 0,
		'organization_id' => 0,
		'type' => 3,
		'info' => json_encode(array('end_time' => 100)),
		'creative_index'=>0,
		]
		];
		 
		DB::table('contests')->insert($data);
	}
	
	protected function insertParticipator() {
		$data = [[
	    	'hiring_id' => 1,
	    	'user_id' => 2,
	    	'cover' => 'cover',
	    	'works' => 'works',
	    	'declaration' => 'declaration',
	    	'status' => 1,
	    	'create_time' => time()
		]];
		
		DB::table('hiring_participators')->insert($data);
	}
}

include_once dirname(__FILE__) . '/../ItemAbFix.php';