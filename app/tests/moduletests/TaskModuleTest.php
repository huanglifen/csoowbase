<?php
use App\Modules\TaskModule;
use App\Modules\ItemModule;

class TaskModuleTest extends TestCase {
	
	public function setUp() {
		parent::setUp();
		DB::table('contests')->truncate();
		DB::table('task_works')->truncate();
	}
	
	public function testPublish() {
		$result = TaskModule::publish(array('title' => 'title', 'cover' => 'cover', 'description' => 'des'), 20, array('apple'), 1);
		
		$this->assertEquals(1, $result);
		
		$task = TaskModule::getTaskById(1);
		
		$this->assertEquals('title', $task->title);
	}
	
	public function testSubmitWork() {
		$this->insertTask();
		
		$result = TaskModule::submitWork(1, 'cover', 'works', 100000, 2);

		$this->assertEquals(1, $result);
	}
	
	public function testCancelWork() {
		$this->insertTask();
		$this->insertWork();
		
		$result = TaskModule::cancelWork(1, 1);
		
		$this->assertFalse($result);
		
		$result = TaskModule::cancelWork(1, 2);
		
		$this->assertTrue($result);
	}
			
	public function testSearchTasks() {	    
		$this->insertTask();
	    
	    $result = TaskModule::searchTasks(1);
	    
	    $this->assertEquals(2, count($result));
	    
	    foreach ($result as $value) {
	        $this->assertEquals(1, $value->status);
	    }
	    
	    $result = TaskModule::searchTasks('');
	    
	    $this->assertEquals(3, count($result));
	    
	    $result = TaskModule::searchTasks(2);
	     
	    $this->assertEquals(1, count($result));
	     
	    foreach ($result as $value) {
	        $this->assertEquals(2, $value->status);
	    }
	    
/* 	    $result = TaskModule::searchTasks(3);
	    
	    $this->assertEquals(0, count($result));
	    
	    foreach ($result as $value) {
	        $this->assertEquals(3, $value->status);
	    } */
	}
	
	public function testGetListsByTagId() {
		$this->insertTask();
	    
	    $data = [
	    [
	    'name' => 'leon1',
	    'cc_no' => 100001,
	    'email' => '529909847@qq.com',
	    'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
	    'password_reset_code' => '',
	    'activation_code' => '',
	    'group_id' => 1,
	    'avatar' => '',
	    'creative_index' => 0,
	    'cash' => '0.00',
	    'coin' => 0,
	    'gender' => 0,
	    'birthday' => 0,
	    'height' => 0,
	    'weight' => 0,
	    'bust' => 0,
	    'waist' => 0,
	    'hip' => 0,
	    'introduction' => ''
	        ]
	        ];
	    DB::table('users')->insert($data);
	    
	    $data = [
	        [
	            'tag_id' => 1,
	            'affair_type' => 2,
	            'affair_id' => 1
	        ]
	    ];
	    
	    DB::table('tags_affairs')->insert($data);
	    
	    $result = TaskModule::getListsByTagId(1, 0, 5);
	    $this->assertEquals(1, $result[0]->id);
	}
	
	protected function insertTask() {
		$data = [
		[
		'no' => 'P23123213',
		'user_id' => 1,
		'title' => 'title',
		'description' => 'description',
		'cover' => 'cover',
		'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		'province_id' => 0,
		'city_id' => 0,
		'status' => 1,
		'start_time' => 1233131234,
		'school_id' => 0,
		'creative_index' => 0,
		'organization_id' => 0,
		'type' => 2,
		'info' => json_encode(array('end_time' => 100, 'bonus_amount' => 100)),
		'creative_index'=>0,
		],
		[
		'no' => 'P23123213',
		'user_id' => 1,
		'title' => 'title',
		'description' => 'description',
		'cover' => 'cover',
		'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		'province_id' => 0,
		'city_id' => 0,
		'status' => 1,
		'start_time' => 1233131234,
		'school_id' => 0,
		'creative_index' => 0,
		'organization_id' => 0,
		'type' => 2,
		'info' => json_encode(array('end_time' => 100, 'bonus_amount' => 100)),
		'creative_index'=>0,
		],
		[
		'no' => 'P23123213',
		'user_id' => 1,
		'title' => 'title',
		'description' => 'description',
		'cover' => 'cover',
		'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
		'province_id' => 0,
		'city_id' => 0,
		'status' => 2,
		'start_time' => 1233131234,
		'school_id' => 0,
		'creative_index' => 0,
		'organization_id' => 0,
		'type' => 2,
		'info' => json_encode(array('end_time' => 100, 'bonus_amount' => 100)),
		'creative_index'=>0,
		]
		 
		];
		DB::table('contests')->insert($data);
	}
	
	protected function insertWork() {
		$data = [
			[
			'task_id' => 1,
			'user_id' =>2,
			'cover' => 'cover',
			'works' => 'works',
			'promise_complete_time' => 20000,
			'status' => 1,
			'create_time' => time()
			]
		];
		
		DB::table('task_works')->insert($data);
	}
}

include_once dirname(__FILE__) . '/../ItemAbFix.php';