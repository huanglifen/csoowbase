<?php
use App\Modules\FeedBackModule;

class FeedBackModuleTest extends TestCase {
    
    public function setUp() {
        parent::setUp();
        DB::table('feedbacks')->truncate();
    }
    
    public function testCreateFeedback() {
        $userId = 1;
        $content = 'nice';
        $result = FeedBackModule::createFeedback($userId, $content);
        $this->assertEquals(1, $result);
    }
}
