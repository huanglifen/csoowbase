<?php
use App\Modules\ChatModule;
use App\Models\Chat;

class ChatModuleTest extends TestCase {

    public function __construct() {
        parent::__construct();
        include_once 'app/tests/moduletests/ExchangeModuleTest.php';
    }

    public function setUp() {
        parent::setUp();
        DB::table('chats')->truncate();
    }

    public function testSendChat() {
        $userId = 1;
        $toUserId = 2;
        $content = "您好！周末有空出来玩玩啊！！！~~~~";
        
        $result = ChatModule::sendChat($userId, $toUserId, $content);
        
        $this->assertTrue($result == 1);
        
        $count = Chat::count();
        $this->assertEquals(1, $count);
    }
    
    public function testGetChats() {  
        $this->prepareChat();      
        //case 1
        $userId = 1;
        $toUserId = 2;
        $chats = ChatModule::getChats($userId, $toUserId);
        $expects = array(4, 2, 1);
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $chats);
        
        //case 2
        $userId = 1;
        $toUserId = 2;
        $topChatId = 4;
        $chats = ChatModule::getChats($userId, $toUserId, $topChatId);
        $expects = array(2, 1);
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $chats);
        
    }
    
    public function testGetUnreadChatNum() {
        $this->prepareChat();
        
        $userId = 1;
        
        $num = ChatModule::getUnreadChatNum($userId);
        
        $this->assertEquals(2, $num);
    }
    
    public function testGetChatsByStatus() {
        $this->prepareChat();
        
        $toUserId = 1;
        $chats = ChatModule::getChatsByStatus($toUserId, ChatModule::STATUS_UNREAD);
        $expects = array(3, 1);
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $chats);
        
        $chats = ChatModule::getChatsByStatus($toUserId, ChatModule::STATUS_READED);
        $expects = array(5, 2);
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $chats);
    }
    
    public function testUpdateChatReaded() {
        $this->prepareChat();
        //case 1
        $sendUserId = 2;
        $toUserId = 1;
        $result = ChatModule::updateChatReaded($sendUserId, $toUserId);
        $this->assertEquals(1, $result);
        
        $chat = Chat::find(1);
        
        $this->assertEquals(ChatModule::STATUS_READED, $chat->status);
        
        //case 2
        $sendUserId = 4;
        $toUserId = 1;
        $result = ChatModule::updateChatReaded($sendUserId, $toUserId);
        $this->assertEquals(0, $result);
        
        $chat = Chat::find(5);       
        $this->assertEquals(ChatModule::STATUS_READED, $chat->status);
        $chat = Chat::find(6);
        $this->assertEquals(ChatModule::STATUS_READED_ON_LINE, $chat->status);
    }
    
    public function testGetCurrentChat() {
        $this->prepareChat();

        //case 1
        $userId = 2;
        $toUserId = 1;
        $chats = ChatModule::getCurrentChat($userId, $toUserId);
        $expects = array(1);
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $chats);
        foreach($chats as $chat) {  
            $message = Chat::find($chat->id);         
            $this->assertEquals($message->status, ChatModule::STATUS_READED_ON_LINE);
        }
            
        //case 2
        $inserts = array(
            array(
                'session_id' => '1-2',
                'user_id' => 2,
                'to_user_id' => 1,
                'content' => 'content',
                'status' => ChatModule::STATUS_UNREAD,
                'create_time' => time()
            )
        );
        DB::table('chats')->insert($inserts);
        
        $lastChatId = 1;
        $userId = 2;
        $toUserId = 1;
        
        $chats = ChatModule::getCurrentChat($userId, $toUserId, $lastChatId);
        $expects = array(
            7
        );
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expects, $chats);
        foreach ($chats as $chat) {
            $message = Chat::find($chat->id);
            $this->assertEquals($message->status, ChatModule::STATUS_READED_ON_LINE);
        }
    }
    
    public function testGetAllChaters() {
        $this->prepareChat();
        
        $userId = 1;
        $chaters = ChatModule::getAllChaters($userId);
        $expects = array(4, 2, 3);
        
        $this->assertEquals($expects, $chaters);
    }
    
    public function testGetStranger() {
        DB::table('circle_members')->truncate();
        //case 1
        $this->prepareChat();
        
        $userId = 1; 
        $strangers = ChatModule::getStranger($userId);
        
        $expects = array(4, 2, 3);
        $this->assertEquals($expects, $strangers);
        
        //case 2
        $this->prepareCircleMemberData();
        
        $userId = 1;
        $strangers = ChatModule::getStranger($userId);
        
        $expects = array(4);
        $this->assertEquals($expects, $strangers);
    }

    public function testGetUnreadChatByUser() {
        $this->prepareChat();
        $inserts = array(
            array(
                'session_id' => '1-2',
                'user_id' => 2,
                'to_user_id' => 1,
                'content' => 'content',
                'status' => ChatModule::STATUS_UNREAD,
                'create_time' => time()
            )
        );
        DB::table('chats')->insert($inserts);
        $userId = 1;
        $result = ChatModule::getUnreadChatByUser($userId);
        $expect = array(
            2,
            1
        );
        $actual = array();
        foreach ($result as $r) {
            $actual[] = $r->num;
        }
        $this->assertEquals($expect, $actual);
    }

    public function prepareChat() {
        $inserts = array(
            array(
                'session_id' => '1-2',
                'user_id' => 2,
                'to_user_id' => 1,
                'content' => 'content',
                'status' => ChatModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'session_id' => '1-2',
                'user_id' => 2,
                'to_user_id' => 1,
                'content' => 'content',
                'status' => ChatModule::STATUS_READED,
                'create_time' => time()
            ),
            array(
                'session_id' => '1-3',
                'user_id' => 3,
                'to_user_id' => 1,
                'content' => 'content',
                'status' => ChatModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'session_id' => '1-2',
                'user_id' => 1,
                'to_user_id' => 2,
                'content' => 'content',
                'status' => ChatModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'session_id' => '1-4',
                'user_id' => 4,
                'to_user_id' => 1,
                'content' => 'content',
                'status' => ChatModule::STATUS_READED,
                'create_time' => time()
            ),
            array(
                'session_id' => '1-4',
                'user_id' => 4,
                'to_user_id' => 1,
                'content' => 'content',
                'status' => ChatModule::STATUS_READED_ON_LINE,
                'create_time' => time()
            )
        );
        
        DB::table('chats')->insert($inserts);
    }

    protected function prepareCircleMemberData() {
        $members = array(
            array(
                'circle_id' => 1,
                'circle_owner_user_id' => 1,
                'user_id' => 3,
                'create_time' => time()
            ),
            array(
                'circle_id' => 3,
                'circle_owner_user_id' => 1,
                'user_id' => 2,
                'create_time' => time()
            )
        );
        DB::table('circle_members')->insert($members);
    }
}
