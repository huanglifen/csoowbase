<?php
use App\Modules\NotificationModule;
use App\Models\Notification;
use App\Modules\CircleModule;
use App\Modules\CommentModule;
use App\Modules\BaseModule;
use App\Modules\DynamicModule;
use App\Modules\ExpertModule;
use App\Modules\ItemModule;
use App\Modules\ProjectModule;
use App\Modules\ExpertCommentModule;
use App\Modules\TaskModule;
use App\Modules\HiringModule;

class NotificationModuleTest extends TestCase {

    public function __construct() {
        parent::__construct();
        include_once 'app/tests/moduletests/ExchangeModuleTest.php';
        include_once 'app/tests/controllertests/ExchangeControllerTest.php';
    }

    public function setUp() {
        parent::setUp();
        DB::table('notifications')->truncate();
        DB::table('contests')->truncate();
        NotificationModule::listenEvent();
    } 

    public function testSendNotification() {
        $userId = 1;
        $content = '恭喜您注册成功！';
        $targetType = 1;
        $info = '哈哈';
        
        $result = NotificationModule::sendNotification($userId, $content, $targetType, $info);
        
        $this->assertTrue($result == 1);
    }

    public function testUpdateNotificationToReaded() {
        $this->prepareNotifications();

        $id = 4;
        $result = NotificationModule::updateNotificationToReaded($id);
        $this->assertTrue($result == 1);
        $notification = Notification::find($id);
        $this->assertEquals(NotificationModule::STATUS_READED, $notification->status);
    }

    public function testGetUnreadNotificationNum() {
        $this->prepareNotifications();
        //case 1
        $userId = 1;
        $num = NotificationModule::getUnreadNotificationNum($userId);
        $this->assertEquals(3, $num);
    }

    public function testGetNotifications() {
        $this->prepareNotifications();
        //case 1
        $userId = 1;
        $notifications = NotificationModule::getNotifications($userId);
        $assert = new ExchangeModuleTest();
        $expect = array(
            3,
            2,
            1
        );
        $assert->assertExpect($expect, $notifications);
    }


    public function testFollowUser() {
        $insert = array(
            'user_id' => 1,
            'name' => '亲人',
            'member_count' => 0,
            'create_time' => time()
        );
        DB::table('circles')->insert($insert);
        $circleId = 1;
        $ownerUserId = 1;
        $userId = 2;
        $result = CircleModule::addCircleMember($circleId, $ownerUserId, $userId);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }


    public function testDynamicComment() {
        DB::table('dynamics')->truncate();
        $insert = array(
            'user_id' => 1,
            'content' => 'content',
            'pic' => '',
            'video' => '',
            'type' => 1,
            'target_type' => 0,
            'target_id' => 0,
            'like_count' => 0,
            'forward_count' => 0,
            'comment_count' => 0,
            'create_time' => time()
        )
        ;
        DB::table('dynamics')->insert($insert);
        $userId = 2;
        $targetType = BaseModule::TYPE_DYNAMIC;
        $targetId = 1;
        $content = '测试消息提醒';
        $result = CommentModule::publishComment($userId, $targetType, $targetId, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }
    
    public function testDynamicForward() {
        DB::table('dynamics')->truncate();
        $insert = array(
            'user_id' => 1,
            'content' => 'content',
            'pic' => '',
            'video' => '',
            'type' => 1,
            'target_type' => 0,
            'target_id' => 0,
            'like_count' => 0,
            'forward_count' => 0,
            'comment_count' => 0,
            'create_time' => time()
        )
        ;
        DB::table('dynamics')->insert($insert);
        
        $userId = 2;
        $fowardId = 1;
        $content = '测试消息提醒';
        $result = DynamicModule::createForward($userId, $fowardId, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }
    
    public function testProjectApply() {
        $this->prepareProject();
        $projectId = 1;
        $userId = 2;
        $amount = 200;
        $type = 1;
        $depositPercentage = 20;
        $finalPaymentPayTime = time()+3*3600*24;
        $requireCompleteTime = time() + 3*3600*24;
        $requirement = 'hahaahaha';
        $result = ProjectModule::invest($projectId, $userId, $amount, $type, $depositPercentage, $finalPaymentPayTime, $requireCompleteTime, $requirement);
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
    
    public function testProjectAcceptInvest() {
        $this->prepareProject();
        $this->prepareInvests();
        $this->prepareUsers();
        $investId = 1;
        $userId = 1;
        $result = ProjectModule::acceptInvest($investId, $userId);
        $count = Notification::count();
        $this->assertEquals(4, $count);
    }
   
    public function testProjectUpdate() {
        $this->prepareProject();
        $this->prepareInvests();
        
        $projectId = 1;
        $title = '终于有人投资我啦';
        $content = "等了好久终于等到今天";
        $userId = 1;
        $result = ProjectModule::createProjectUpdate($projectId, $title, $content, $userId);
        $count = Notification::count();
        $this->assertEquals(3, $count);
    }
   
    public function testProjectGetInvite() {
        $this->prepareUsers();
        $this->prepareProject();
        $userId = 1;
        $targetType = BaseModule::TYPE_PROJECT;
        $targetId = 1;
        $expertUserId = 2;
        $result = ExpertModule::inviteInternalExpert($userId, $targetType, $targetId, $expertUserId);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }
   
    public function testProjectExpertComment() {
        $this->prepareUsers();
        $this->prepareProject();
        DB::table('project_investments')->truncate();
        
        $data = [
        [
        'user_id' => 3,
        'target_type' => BaseModule::TYPE_PROJECT,
        'target_id' => 1,
        'target_sub_id' => 0,
        'create_time' => time(),
        'expert_user_id' => 2,
        ],[
        'user_id' => 1,
        'target_type' => BaseModule::TYPE_PROJECT,
        'target_id' => 1,
        'target_sub_id' => 0,
        'create_time' => time(),
        'expert_user_id' => 2,
        ],
        ];
        DB::table('expert_invites_internal')->truncate();
        DB::table('expert_invites_internal')->insert($data);
        
        //case 1
        $userId = 3;
        $targetType = BaseModule::TYPE_PROJECT;
        $targetId = 1;
        $score = 5;
        $content = '丝丝润滑';
        $result = ExpertCommentModule::publishExpertComment($userId, $targetType, $targetId, $score, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
        
        //case 2
        $this->prepareInvests();

        $userId = 2;
        $targetType = BaseModule::TYPE_PROJECT;
        $targetId = 1;
        $score = 5;
        $content = '丝丝润滑';
        $result = ExpertCommentModule::publishExpertComment($userId, $targetType, $targetId, $score, $content);
        $count = Notification::count();
        $this->assertEquals(6, $count);
        
        //case 3
        $userId = 2;
        $targetType = BaseModule::TYPE_PROJECT;
        $targetId = 1;
        $score = 5;
        $content = '丝丝润滑';
        $result = ExpertCommentModule::publishExpertComment($userId, $targetType, $targetId, $score, $content);
        $count = Notification::count();
        $this->assertEquals(8, $count);
    }
    
    public function testProjectComment() {
        $this->prepareProject();
        $userId = 2;
        $targetType = BaseModule::TYPE_PROJECT;
        $targetId = 1;
        $content = '有没有肉末酸豆角啊，好像吃啊！！';
        $result = CommentModule::publishComment($userId, $targetType, $targetId, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }
    
        public function testProjectRemainThree() {
        $data = [
        [
        'no' => 'P23123213',
        'user_id' => 1,
        'title' => 'title',
        'description' => 'description',
        'cover' => 'cover',
        'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
        'province_id' => 0,
        'city_id' => 0,
        'district_id' => 0,
        'school_id' => 0,
        'organization_id' => 0,
        'status' => ProjectModule::STATUS_GOING,
        'start_time' => 1233131234,
        'creative_index' => 0,
        'guess_total_amount' => 100,
        'guess_success_amount' => 0,
        'creative_index' => 0,
        'type' => 1,
        'info' => json_encode(array('investment_end_time'=>time()+3*86400-5)),
        ]
        ];
        DB::table('contests')->insert($data);
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(1, $count);
        
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(1, $count);
        
    }
   
    public function testProjectPastDue() {
        $this->prepareInvests();
        $data = [
        [
        'no' => 'P23123213',
        'user_id' => 1,
        'title' => 'title',
        'description' => 'description',
        'cover' => 'cover',
        'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
        'province_id' => 0,
        'city_id' => 0,
        'district_id' => 0,
        'school_id' => 0,
        'organization_id' => 0,
        'status' => ProjectModule::STATUS_GOING,
        'start_time' => 1233131234,
        'creative_index' => 0,
        'guess_total_amount' => 100,
        'guess_success_amount' => 0,
        'creative_index' => 0,
        'type' => 1,
        'info' => json_encode(array('investment_end_time'=>time()-5)),
        ]
        ];
        DB::table('contests')->insert($data);
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(4, $count);
        
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(4, $count);
    }
    
    //支付全款和项目完工24小时提醒
    public function testProjectPayTotalAndFinish() {
        DB::table('project_investments')->truncate();
        $data = [
        [
        'project_id' => 1,
        'user_id' => 2,
        'amount' => 200,
        'type' => ProjectModule::INVEST_TYPE_TOTAL,
        'deposit_percentage' => 20,
        'final_payment_pay_time' => 1234124,
        'require_complete_time' => time()+86400-5,
        'requirement' => 'requirement',
        'status' => ProjectModule::INVEST_STATUS_ACCEPT,
        'accept_time' => time() - 86400*2 -5,
        'create_time' => time()
        ],
        [
        'project_id' => 1,
        'user_id' => 3,
        'amount' => 200,
        'type' => ProjectModule::INVEST_TYPE_STAGES,
        'deposit_percentage' => 20,
        'final_payment_pay_time' => 1234124,
        'require_complete_time' => 42134,
        'requirement' => 'requirement',
        'status' => ProjectModule::INVEST_STATUS_SUBMIT,
        'create_time' => time(),
        'accept_time' => 0,
        ]
        ];
        DB::table('project_investments')->insert($data);
        
        $data = [
        [
        'no' => 'P23123213',
        'user_id' => 1,
        'title' => 'title',
        'description' => 'description',
        'cover' => 'cover',
        'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
        'province_id' => 0,
        'city_id' => 0,
        'district_id' => 0,
        'school_id' => 0,
        'organization_id' => 0,
        'status' => ProjectModule::STATUS_CONFIRM_INVEST,
        'start_time' => time(),
        'creative_index' => 0,
        'guess_total_amount' => 100,
        'guess_success_amount' => 0,
        'creative_index' => 0,
        'type' => 1,
        'info' => json_encode(array('investment_end_time'=>time()-5)),
        ]
        ];
        DB::table('contests')->insert($data);
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(4, $count);
        
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(4, $count);
    }
    
    public function testProjectPayStage() {
        DB::table('project_investments')->truncate();
        $data = [
        [
        'project_id' => 1,
        'user_id' => 2,
        'amount' => 200,
        'type' => ProjectModule::INVEST_TYPE_STAGES,
        'deposit_percentage' => 20,
        'final_payment_pay_time' => time()+86400*3,
        'require_complete_time' => time()+86400*2,
        'requirement' => 'requirement',
        'status' => ProjectModule::INVEST_STATUS_ACCEPT,
        'accept_time' => time() - 86400*2 -5,
        'create_time' => time()
        ],
        [
        'project_id' => 1,
        'user_id' => 3,
        'amount' => 200,
        'type' => ProjectModule::INVEST_TYPE_STAGES,
        'deposit_percentage' => 20,
        'final_payment_pay_time' => 1234124,
        'require_complete_time' => 42134,
        'requirement' => 'requirement',
        'status' => ProjectModule::INVEST_STATUS_SUBMIT,
        'create_time' => time(),
        'accept_time' => 0,
        ]
        ];
        DB::table('project_investments')->insert($data);
        
        $data = [
        [
        'no' => 'P23123213',
        'user_id' => 1,
        'title' => 'title',
        'description' => 'description',
        'cover' => 'cover',
        'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
        'province_id' => 0,
        'city_id' => 0,
        'district_id' => 0,
        'school_id' => 0,
        'organization_id' => 0,
        'status' => ProjectModule::STATUS_CONFIRM_INVEST,
        'start_time' => time(),
        'creative_index' => 0,
        'guess_total_amount' => 100,
        'guess_success_amount' => 0,
        'creative_index' => 0,
        'type' => 1,
        'info' => json_encode(array('investment_end_time'=>time()-5)),
        ]
        ];
        DB::table('contests')->insert($data);
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(2, $count);
        
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
    
    public function testPrjojectPayFinal() {
        DB::table('project_investments')->truncate();
        $data = [
        [
        'project_id' => 1,
        'user_id' => 2,
        'amount' => 200,
        'type' => ProjectModule::INVEST_TYPE_STAGES,
        'deposit_percentage' => 20,
        'final_payment_pay_time' => time()+86400-5,
        'require_complete_time' => time()+86400*2,
        'requirement' => 'requirement',
        'status' => ProjectModule::INVEST_STATUS_ACCEPT,
        'accept_time' => time() - 86400*3 -5,
        'create_time' => time()
        ],
        [
        'project_id' => 1,
        'user_id' => 3,
        'amount' => 200,
        'type' => ProjectModule::INVEST_TYPE_STAGES,
        'deposit_percentage' => 20,
        'final_payment_pay_time' => 1234124,
        'require_complete_time' => 42134,
        'requirement' => 'requirement',
        'status' => ProjectModule::INVEST_STATUS_SUBMIT,
        'create_time' => time(),
        'accept_time' => 0,
        ]
        ];
        DB::table('project_investments')->insert($data);
        
        $data = [
        [
        'no' => 'P23123213',
        'user_id' => 1,
        'title' => 'title',
        'description' => 'description',
        'cover' => 'cover',
        'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
        'province_id' => 0,
        'city_id' => 0,
        'district_id' => 0,
        'school_id' => 0,
        'organization_id' => 0,
        'status' => ProjectModule::STATUS_CONFIRM_INVEST,
        'start_time' => time(),
        'creative_index' => 0,
        'guess_total_amount' => 100,
        'guess_success_amount' => 0,
        'creative_index' => 0,
        'type' => 1,
        'info' => json_encode(array('investment_end_time'=>time()-5)),
        ]
        ];
        DB::table('contests')->insert($data);
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(2, $count);
        
        ProjectModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
   
    public function testTaskApply() {
        $this->prepareTask();
        //case 1
        $taskId = 1;
        $cover = '1.jjpg';
        $works = 'haha';
        $endTime = time() + 3 * 24 * 3600;
        $userId = 2;
        $result = TaskModule::submitWork($taskId, $cover, $works, $endTime, $userId);
        $count = Notification::count();
        $this->assertEquals(2, $count);
        //case 2
        $taskId = 1;
        $cover = '1.jjpg';
        $works = 'haha';
        $endTime = time() + 3 * 24 * 3600;
        $userId = 2;
        $result = TaskModule::submitWork($taskId, $cover, $works, $endTime, $userId);
        $count = Notification::count();
        $this->assertEquals(3, $count);
        //case 3
        Notification::where('id',1)->update(array('status' => NotificationModule::STATUS_READED));
        $taskId = 1;
        $cover = '1.jjpg';
        $works = 'haha';
        $endTime = time() + 3 * 24 * 3600;
        $userId = 2;
        $result = TaskModule::submitWork($taskId, $cover, $works, $endTime, $userId);
        $count = Notification::count();
        $this->assertEquals(5, $count);
    }
   
   
    public function testTaskChooseWinner() {
        $this->prepareUsers();
        $this->prepareTask();
        $this->prepareTaskWork();
        
        $userId = 1;
        $workId = 1;
        $result = TaskModule::chooseWinner($workId, $userId);
        $count = Notification::count();
        $this->assertEquals(3, $count);
       
    } 
   
    public function testTaskUpdate() {
        $this->prepareTask();
        $this->prepareTaskWork();
        
        $taskId = 1;
        $userId = 1;
        $title = '测试是一项费体力的活';
        $content = '所谓单元测试，就是一个单元一个单元的测试';
        $result = TaskModule::createTaskUpdate($taskId, $userId, $title, $content);
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
   
    public function testTaskExpertComment() {
        $this->prepareTask();
        $this->prepareTaskWork();

        $userId = 3;
        $targetType = BaseModule::TYPE_TASK_WORK;
        $targetId = 1;
        $score = 5;
        $content = '丝丝润滑';
        $result = ExpertCommentModule::publishExpertComment($userId, $targetType, $targetId, $score, $content);
        $count = Notification::count();
        $this->assertEquals(2, $count);
        
    }

   
    public function testTaskComment() {
        $this->prepareTask();
        $this->prepareTaskWork();
        
        //case 1
        $userId = 3;
        $targetType = BaseModule::TYPE_TASK;
        $targetId = 1;
        $content = '任务比赛还能发表评论呢！';
        $result = CommentModule::publishComment($userId, $targetType, $targetId, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
        //case 2
        $userId = 3;
        $targetType = BaseModule::TYPE_TASK_WORK;
        $targetId = 1;
        $content = '任务比赛还能发表评论呢！';
        $result = CommentModule::publishComment($userId, $targetType, $targetId, $content);
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
   
    public function testTaskRemainThree() {
        $data = [
            [
                'no' => 'asdf',
                'user_id' => 1,
                'title' => 'a',
                'description' => 'apple',
                'cover' => 'app/image/apple.jpg',
                'tags' => '{"a":"b"}',
                'province_id' => 11,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'organization_id' => 0,
                'status' => TaskModule::STATUS_GOING,
                'start_time' => 1233131234,
                'creative_index' => 0,
                'guess_total_amount' => 100,
                'guess_success_amount' => 0,
                'creative_index' => 0,
                'type' => 2,
                'info' => json_encode(array('end_time' => time()+86400*3-5)),
            ]
        ];
        DB::table('contests')->insert($data);
        TaskModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(1, $count);
    
    }
   
    public function testTaskPastDue() {
        $this->prepareTaskWork();
        $data = [
        [
        'no' => 'asdf',
        'user_id' => 1,
        'title' => 'a',
        'description' => 'apple',
        'cover' => 'app/image/apple.jpg',
        'tags' => '{"a":"b"}',
        'province_id' => 11,
        'city_id' => 0,
        'district_id' => 0,
        'school_id' => 0,
        'organization_id' => 0,
        'status' => TaskModule::STATUS_GOING,
        'start_time' => 1233131234,
        'creative_index' => 0,
        'guess_total_amount' => 100,
        'guess_success_amount' => 0,
        'creative_index' => 0,
        'type' => 2,
        'info' => json_encode(array('end_time' => time()-5)),
        ]
        ];
        DB::table('contests')->insert($data);
        TaskModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(3, $count);
    }
   
    public function testTaskFinish() {
        $data = [[
        'task_id' => 1,
        'user_id' => 2,
        'cover' => '1.jpg',
        'works' => 'haha',
        'promise_complete_time' => time()+86400-5,
        'status' => TaskModule::TASK_WORK_STATUS_WINNER,
        'create_time' => time(),
        'creative_index' => 0,
        'winning_time' => 0,
        ],[
        'task_id' => 1,
        'user_id' => 2,
        'cover' => '1.jpg',
        'works' => 'haha',
        'promise_complete_time' => time()+86400*3,
        'status' => TaskModule::TASK_WORK_STATUS_SUBMIT,
        'create_time' => time(),
        'creative_index' => 0,
        'winning_time' => 0,
        ],[
        'task_id' => 1,
        'user_id' => 3,
        'cover' => '1.jpg',
        'works' => 'haha',
        'promise_complete_time' => time()+86400*3,
        'status' => TaskModule::TASK_WORK_STATUS_SUBMIT,
        'create_time' => time(),
        'creative_index' => 0,
        'winning_time' => 0,
        ]];
        DB::table('task_works')->truncate();
        DB::table('task_works')->insert($data);
        
        $data = [
        [
        'no' => 'asdf',
        'user_id' => 1,
        'title' => 'a',
        'description' => 'apple',
        'cover' => 'app/image/apple.jpg',
        'tags' => '{"a":"b"}',
        'province_id' => 11,
        'city_id' => 0,
        'district_id' => 0,
        'school_id' => 0,
        'organization_id' => 0,
        'status' => TaskModule::STATUS_WAITING_CONFIRM,
        'start_time' => 1233131234,
        'creative_index' => 0,
        'guess_total_amount' => 100,
        'guess_success_amount' => 0,
        'creative_index' => 0,
        'type' => 2,
        'info' => json_encode(array('end_time' => time()-5)),
        ]
        ];
        DB::table('contests')->insert($data);
        TaskModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
    
    public function testHiringApply() {
        $this->prepareHiring();
        
        $hiringId = 1;
        $userId = 2;
        $participatorInfo = array(
            'works' => 'haha',
            'declaration' => 'ahah',
            'cover' => '1.jpg',
            'creative_index' => 0,
        );
        $result = HiringModule::applyHiringParticipator($participatorInfo, $hiringId, $userId);
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
    
    public function testHiringChooseWinner() {
        $this->prepareUsers();
        $this->prepareHiring();
        $this->prepareParticipator();
        
        $participatorId = 1;
        $userId = 1;
        $result = HiringModule::chooseWinner($participatorId, $userId);
        $count = Notification::count();
        $this->assertEquals(3, $count);
    }
    
    public function testHiringUpdate() {
        $this->prepareHiring();
        $this->prepareParticipator();
        
        $hiringId = 1;
        $userId = 1;
        $title = '哈哈哈，更新来了';
        $content = '哈哈';
        $result = HiringModule::addUpdate($hiringId, $userId, $title, $content);
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
    
    public function testHiringExpertComment() {
        $this->prepareUsers();
        $this->prepareHiring();
        $this->prepareParticipator();
        $userId = 2;
        $targetType = BaseModule::TYPE_HIRING_WORK;
        $targetId = 1;
        $score = 5;
        $content = '太美啦';
        $result = ExpertCommentModule::publishExpertComment($userId, $targetType, $targetId, $score, $content);
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
    
    public function testHiringComment() {
        $this->prepareHiring();
        $this->prepareParticipator();
        //case 1
        $userId = 2;
        $targetType = BaseModule::TYPE_HIRING;
        $targetId = 1;
        $content = '比赛评论';
        $result = CommentModule::publishComment($userId, $targetType, $targetId, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
        //case 2
        $userId = 2;
        $targetType = BaseModule::TYPE_HIRING_WORK;
        $targetId = 1;
        $content = '比赛评论';
        $result = CommentModule::publishComment($userId, $targetType, $targetId, $content);
        $count = Notification::count();
        $this->assertEquals(2, $count);
    }
    
    public function testHiringRemainThree() {
        $data = [
        [
        'no' => 'qwer',
        'user_id' => 1,
        'title' => 'apple',
        'description' => 'water',
        'cover' => 'app/image/apple.jpg',
        'tags' => '{"a":"b"}',
        'province_id' => 11,
        'city_id' => 0,
        'district_id' => 0,
        'school_id' => 0,
        'organization_id' => 0,
        'status' => HiringModule::STATUS_GOING,
        'start_time' => 1233131234,
        'creative_index' => 0,
        'guess_total_amount' => 100,
        'guess_success_amount' => 0,
        'creative_index' => 0,
        'type' => 3,
        'info' => json_encode(array('end_time' => time()+86400*3-5)),
        ]
        ];
        
        DB::table('contests')->insert($data);
        HiringModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }
   
    public function testHiringPastDue() {
        $this->prepareParticipator();
        $data = [
        [
        'no' => 'qwer',
        'user_id' => 1,
        'title' => 'apple',
        'description' => 'water',
        'cover' => 'app/image/apple.jpg',
        'tags' => '{"a":"b"}',
        'province_id' => 11,
        'city_id' => 0,
        'district_id' => 0,
        'school_id' => 0,
        'organization_id' => 0,
        'status' => HiringModule::STATUS_GOING,
        'start_time' => 1233131234,
        'creative_index' => 0,
        'guess_total_amount' => 100,
        'guess_success_amount' => 0,
        'creative_index' => 0,
        'type' => 3,
        'info' => json_encode(array('end_time' => time()-5)),
        ]
        ];
        
        DB::table('contests')->insert($data);
        HiringModule::processExpiration();
        $count = Notification::count();
        $this->assertEquals(3, $count);
        
    }
     
    
    public function testExchangeExpertComment() {
        $this->prepareExchange();
        $userId = 2;
        $targetType = BaseModule::TYPE_EXCHANGE;
        $targetId = 1;
        $score = 5;
        $content = '我是专家';
        $result = ExpertCommentModule::publishExpertComment($userId, $targetType, $targetId, $score, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }
   
    public function testExchangeComment() {
        $this->prepareExchange();

        $userId = 2;
        $targetType = BaseModule::TYPE_EXCHANGE;
        $targetId = 1;
        $content = '比赛评论';
        $result = CommentModule::publishComment($userId, $targetType, $targetId, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }
    
    public function testEncyExpertComment() {
        $this->prepareEncy();
        $userId = 2;
        $targetType = BaseModule::TYPE_ENCYCLOPEDIA;
        $targetId = 1;
        $score = 5;
        $content = '我是专家';
        $result = ExpertCommentModule::publishExpertComment($userId, $targetType, $targetId, $score, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }
   
    public function testEncyComment() {
        $this->prepareEncy();
        $userId = 2;
        $targetType = BaseModule::TYPE_ENCYCLOPEDIA;
        $targetId = 1;
        $content = '比赛评论';
        $result = CommentModule::publishComment($userId, $targetType, $targetId, $content);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }
   
    public function testExpertApply() {
        $this->prepareUsers();
        DB::table('expert_applications')->truncate();
        $userId = 1;
        $candidateInfo = array(
            'industry_id' => 1,
            'enterprise_name' => 'pingguo',
            'enterprise_duty' => 'hulianwang',
            'organization_id' => 1,
            'duty_id' => 1,
            'attachments' => 'aaaa',
            'explain' => 'hahah',
        );
        $result = ExpertModule::applyExpert($userId, $candidateInfo);
        $count = Notification::count();
        $this->assertEquals(1, $count);
    }

    public function prepareNotifications() {
        $inserts = array(
            array(
                'user_id' => 1,
                'content' => 'content',
                'target_type' => 1,
                'info' => 1,
                'status' => NotificationModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'content' => 'content',
                'target_type' => 2,
                'info' => 1,
                'status' => NotificationModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'content' => 'content',
                'target_type' => 1,
                'info' => 1,
                'status' => NotificationModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'user_id' => 2,
                'content' => 'content',
                'target_type' => 1,
                'info' => 1,
                'status' => NotificationModule::STATUS_UNREAD,
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'content' => 'content',
                'target_type' => 1,
                'info' => 1,
                'status' => NotificationModule::STATUS_READED,
                'create_time' => time()
            )
        );
        DB::table('notifications')->insert($inserts);
    }

    protected function prepareUsers() {
        DB::table('users')->truncate();
        $no = 1;
        //准备用户数据
        $insertUsers = array(
            array(
                'name' => 'cc1',
                'cc_no' => $no ++,
                'email' => 'cc1@163.com'
            ),
            array(
                'name' => 'cc2',
                'cc_no' => $no ++,
                'email' => 'cc2@163.com',
                'group_id' => 2
            ),
            array(
                'name' => 'cc3',
                'cc_no' => $no ++,
                'email' => 'cc3@163.com',
                'group_id' => 2
            ),
            array(
                'name' => 'cc4',
                'cc_no' => $no ++,
                'email' => 'cc4@163.com'
            ),
            array(
                'name' => 'cc5',
                'cc_no' => $no ++,
                'email' => 'cc5@163.com'
            )
        );
        $fills = new ExchangeControllerTest();
        $fills->fillOtherField($insertUsers);
        
        DB::table('users')->insert($insertUsers);
    }

    protected function prepareProject() {
        $data = [
            [
                'no' => 'P23123213',
                'user_id' => 1,
                'title' => 'title',
                'description' => 'description',
                'cover' => 'cover',
                'tags' => '{"1":"test","2":"\u7684\u53d1\u7684"}',
                'province_id' => 0,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'organization_id' => 0,
                'status' => 1,
                'start_time' => 1233131234,
                'creative_index' => 0,
                'guess_total_amount' => 100,
                'guess_success_amount' => 0,
                'creative_index' => 0,
                'type' => 1,
                'info' => '',
            ]
        ];
        DB::table('contests')->insert($data);
    }

    protected function prepareInvest() {
        $data = [
            [
                'project_id' => 1,
                'user_id' => 2,
                'amount' => 200,
                'type' => 1,
                'deposit_percentage' => 20,
                'final_payment_pay_time' => 1234124,
                'require_complete_time' => 42134,
                'requirement' => 'requirement',
                'status' => 1,
                'create_time' => time()
            ]
        ];
        DB::table('project_investments')->insert($data);
    }

    protected function prepareInvests() {
        DB::table('project_investments')->truncate();
        $data = [
            [
                'project_id' => 1,
                'user_id' => 2,
                'amount' => 200,
                'type' => 1,
                'deposit_percentage' => 20,
                'final_payment_pay_time' => 1234124,
                'require_complete_time' => 42134,
                'requirement' => 'requirement',
                'status' => 1,
                'create_time' => time(),
                'accept_time' => 0,
            ],
            [
                'project_id' => 1,
                'user_id' => 2,
                'amount' => 200,
                'type' => 2,
                'deposit_percentage' => 20,
                'final_payment_pay_time' => 1234124,
                'require_complete_time' => 42134,
                'requirement' => 'requirement',
                'status' => 1,
                'create_time' => time(),
                'accept_time' => 0,
            ],
            [
                'project_id' => 1,
                'user_id' => 3,
                'amount' => 200,
                'type' => 1,
                'deposit_percentage' => 20,
                'final_payment_pay_time' => 1234124,
                'require_complete_time' => 42134,
                'requirement' => 'requirement',
                'status' => 1,
                'create_time' => time(),
                'accept_time' => 0,
            ],
            [
                'project_id' => 1,
                'user_id' => 4,
                'amount' => 200,
                'type' => 1,
                'deposit_percentage' => 50,
                'final_payment_pay_time' => 1234124,
                'require_complete_time' => 42134,
                'requirement' => 'requirement',
                'status' => 1,
                'create_time' => time(),
                'accept_time' => 0,
            ]
        ];
        DB::table('project_investments')->insert($data);
    }

    protected function prepareTask() {
        $data = [
            [
                'no' => 'asdf',
                'user_id' => 1,
                'title' => 'a',
                'description' => 'apple',
                'cover' => 'app/image/apple.jpg',
                'tags' => '{"a":"b"}',
                'province_id' => 11,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'organization_id' => 0,
                'status' => 1,
                'start_time' => 1233131234,
                'creative_index' => 0,
                'guess_total_amount' => 100,
                'guess_success_amount' => 0,
                'creative_index' => 0,
                'type' => 2,
                'info' => '',
            ]
        ];
        DB::table('contests')->insert($data);
    }
    
    protected  function prepareTaskWork() {
        $data = [[
        'task_id' => 1,
        'user_id' => 2,
        'cover' => '1.jpg',
        'works' => 'haha',
        'promise_complete_time' => time()+86400*3,
        'status' => TaskModule::TASK_WORK_STATUS_SUBMIT,
        'create_time' => time(),
        'creative_index' => 0,
        'winning_time' => 0,
        ],[
        'task_id' => 1,
        'user_id' => 2,
        'cover' => '1.jpg',
        'works' => 'haha',
        'promise_complete_time' => time()+86400*3,
        'status' => TaskModule::TASK_WORK_STATUS_SUBMIT,
        'create_time' => time(),
        'creative_index' => 0,
        'winning_time' => 0,
        ],[
        'task_id' => 1,
        'user_id' => 3,
        'cover' => '1.jpg',
        'works' => 'haha',
        'promise_complete_time' => time()+86400*3,
        'status' => TaskModule::TASK_WORK_STATUS_SUBMIT,
        'create_time' => time(),
        'creative_index' => 0,
        'winning_time' => 0,
        ]];
        DB::table('task_works')->truncate();
        DB::table('task_works')->insert($data);
    }

    protected function prepareHiring() {
        $data = [
            [
                'no' => 'qwer',
                'user_id' => 1,
                'title' => 'apple',
                'description' => 'water',
                'cover' => 'app/image/apple.jpg',
                'tags' => '{"a":"b"}',
                'province_id' => 11,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'organization_id' => 0,
                'status' => 1,
                'start_time' => 1233131234,
                'creative_index' => 0,
                'guess_total_amount' => 100,
                'guess_success_amount' => 0,
                'creative_index' => 0,
                'type' => 3,
                'info' => '',
            ]
        ];
        
        DB::table('contests')->insert($data);
    }

    protected function prepareParticipator() {
        $data = [
            [
                'hiring_id' => 1,
                'user_id' => 2,
                'cover' => 'cover',
                'works' => 'works',
                'declaration' => 'declaration',
                'status' => HiringModule::PARTICIPATOR_STATUS_APPLY,
                'create_time' => time(),
                'creative_index' => 0,
            ],[
                'hiring_id' => 1,
                'user_id' => 3,
                'cover' => 'cover',
                'works' => 'works',
                'declaration' => 'declaration',
                'status' => HiringModule::PARTICIPATOR_STATUS_APPLY,
                'create_time' => time(),
                'creative_index' => 0,
            ]
        ];
        DB::table('hiring_participators')->truncate();
        DB::table('hiring_participators')->insert($data);
    }

    public function prepareExchange() {
        //准备交易所数据
        $i = 1;
        $inserts = array(
            array(
                'no' => $i ++,
                'user_id' => 1,
                'title' => '交易所',
                'description' => 'des des des 这里是描述信息',
                'cover' => '1.jpg',
                'tags' => json_encode(array(
                    '1' => '文艺范',
                    '3' => '非主流'
                )),
                'province_id' => 0,
                'city_id' => 0,
                'district_id' => 0,
                'school_id' => 0,
                'organization_id' => 0,
                'status' => ItemModule::STATUS_GOING,
                'start_time' => time(),
                'creative_index' => 20,
                'guess_total_amount' => 20,
                'guess_success_amount' => 20,
                'price' => 2
            )
            
        );
        DB::table('exchange_trades')->truncate();
        DB::table('exchange_trades')->insert($inserts);
    }

    protected function prepareEncy() {
        $data = [
            [
                'user_id' => 1,
                'item_type' => 4,
                'item_id' => 0,
                'title' => '总之也还好',
                'cover' => 'data/image/apple.jpg',
                'content' => 'nice',
                'tags' => '{"apple":"delicious"}',
                'province_id' => 1,
                'city_id' => 1,
                'district_id' => 1,
                'school_id' => 1,
                'organization_id' => 1,
                'creative_index' => 20,
                'create_time' => time()
            ]
        ];
        DB::table('encyclopedias')->insert($data);
    }
}