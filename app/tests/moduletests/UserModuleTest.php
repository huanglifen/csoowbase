<?php
use App\Modules\UserModule;

class UserModuleTest extends TestCase {

	public function setUp() {
		parent::setUp();
		DB::table('users')->truncate();
	}

	public function testRegister() {
		$result = UserModule::register('leon1', 'leon1@leon.com', 'asdf');

		$this->assertEquals(1, $result);
		// Check CC NO
		$user = UserModule::getUserById(1);

		$this->assertEquals(100001, $user->cc_no);
		// Another one
		$result = UserModule::register('leon2', 'leon2@leon.com', 'asdf');

		$this->assertEquals(2, $result);
	}

	public function testLogin() {
		$data = [
			[
				'name' => 'leon1',
				'cc_no' => 100001,
				'email' => 'leon1@leon.com',
				'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
			],
			[
				'name' => 'leon2',
				'cc_no' => 100002,
				'email' => 'leon2@leon.com',
				'password' => 'veAHdaTUp9ZmVByub79d3ffcce4643a284aca37a9195c20560f7dd468e3cf8de0809ad30d67347be',
			]
		];

		$data = array_map(function($user) {
			$new = [
				'password_reset_code' => '',
				'activation_code' => 1,
				'group_id' => 1,
				'avatar' => '',
				'creative_index' => 0,
				'coin' => 0,
				'gender' => 0,
				'birthday' => 0,
				'introduction' => ''
			];
			return array_merge($user, $new);
		}, $data);

		DB::table('users')->insert($data);

		$user = UserModule::login('leon2', 'asdf');
		$this->assertEquals(2, $user->id);

		$user = UserModule::login('leon1@leon.com', 'asdf');
		$this->assertEquals(1, $user->id);

		$user = UserModule::login('100002', 'asdf');
		$this->assertEquals(2, $user->id);

		$user = UserModule::login('leon2', 'asdf1');
		$this->assertFalse($user);
	}
	
	public function testActivateAccount() {
	    $data = [
	    [
	    'name' => 'leon1',
	    'cc_no' => 100001,
	    'email' => '529909847@qq.com',
	    'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
	    'password_reset_code' => '',
	    'activation_code' => '',
	    'group_id' => 0,
	    'avatar' => '',
	    'creative_index' => 0,
	    'coin' => 0,
	    'gender' => 0,
	    'introduction' => ''
	        ]
	        ];
	    DB::table('users')->insert($data);
	    $result = UserModule::activateAccount(1);
	    $this->assertTrue($result);
	    $result = UserModule::activateAccount(2);
	    $this->assertFalse($result);
	}
	
	public function testActivateAccountSuccess() {
	    $data = [
	    [
	    'name' => 'leon1',
	    'cc_no' => 100001,
	    'email' => '529909847@qq.com',
	    'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
	    'password_reset_code' => '',
	    'activation_code' => '',
	    'group_id' => 0,
	    'avatar' => '',
	    'creative_index' => 0,
	    'coin' => 0,
	    'gender' => 0,
	    'birthday' => 0,
	    'introduction' => ''
	        ]
	        ];
	    DB::table('users')->insert($data);
	    $result = UserModule::activateAccountSuccess(1);
	    $this->assertTrue($result);
	    $result = UserModule::activateAccountSuccess(2);
	    $this->assertFalse($result);
	}
	
	public function testFindPassword() {
	    $data = [
	    [
	    'name' => 'leon1',
	    'cc_no' => 100001,
	    'email' => '529909847@qq.com',
	    'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
	    'password_reset_code' => '',
	    'activation_code' => '',
	    'group_id' => 1,
	    'avatar' => '',
	    'creative_index' => 0,
	    'coin' => 0,
	    'gender' => 0,
	    'birthday' => 0,
	    'introduction' => ''
	        ]
	        ];
	    DB::table('users')->insert($data);
	    $result = UserModule::findPassword('ret@qq.com');
	    $this->assertFalse($result);
	    $result = UserModule::findPassword('529909847@qq.com');
	    $this->assertTrue($result);
	}

	public function testResetPassword() {
	    $data = [
	    [
	    'name' => 'leon1',
	    'cc_no' => 100001,
	    'email' => '529909847@qq.com',
	    'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
	    'password_reset_code' => 'IVUUZ-HK71U-WEX4M-KYR54-0Z47M',
	    'activation_code' => '',
	    'group_id' => 1,
	    'avatar' => '',
	    'creative_index' => 0,
	    'coin' => 0,
	    'gender' => 0,
	    'birthday' => 0,
	    'introduction' => ''
	        ]
	        ];
	    DB::table('users')->insert($data);
	    $result = UserModule::resetPassword(2, 'IVUUZ-HK71U-WEX4M-KYR54-0Z47M', '123456');
	    $this->assertFalse($result);
	    $result = UserModule::resetPassword(1, 'IVUUZ-HK71U-WEX4M-KYR54-0Z47Q', '123456');
	    $this->assertFalse($result);
	    $result = UserModule::resetPassword(1, 'IVUUZ-HK71U-WEX4M-KYR54-0Z47M', '123456');
	    $this->assertTrue($result);
	}
}