<?php
use App\Modules\RegionModule;
class AreaModuleTest extends TestCase {
	public function setUp() {
		parent::setUp ();
		DB::table ( 'areas' )->truncate ();
		DB::table ( 'regions' )->truncate ();
		DB::table ( 'schools' )->truncate ();
		DB::table ( 'organizations' )->truncate ();
	}
	public function testgetRegionInfoById() {
		$this->prepareRegionData ();
		
		$result = RegionModule::getRegionInfoById ( 10 );
		$this->assertEquals ( '北京', $result->name );
		
		$result = RegionModule::getRegionInfoById ( 3201 );
		$this->assertEquals ( 3, $result->type );
	}
	public function testgetRegionsById() {
		$this->prepareRegionData ();
		
		$result = RegionModule::getRegionsById ( 10 );
		$this->assertEquals ( 1, count ( $result ) );
		
		$result = RegionModule::getRegionsById ( 32 );
		$this->assertEquals ( 2, count ( $result ) );
		
		$result = RegionModule::getRegionsById ( 3201 );
		$this->assertEquals ( 1, count ( $result ) );
		
		$result = RegionModule::getRegionsById ( 320103 );
		$this->assertEquals ( 0, count ( $result ) );
	}
	protected function prepareRegionData() {
		$inserts = array (
				array (
						'id' => 10,
						'name' => '北京',
						'logo' => 'logo',
						'creative_index' => '12345',
						'intro' => '1.mkv',
						'type' => 2,
						'nation_id' => 1,
						'province_id' => 0,
						'city_id' => 0,
						'contests_count' => 50,
						'trades_count' => 670,
						'experts_count' => 450,
						'users_count' => 1050 
				),
				array (
						'id' => 1001,
						'name' => '东城区',
						'logo' => 'logo',
						'creative_index' => '1235',
						'intro' => '1.mkv',
						'type' => 3,
						'nation_id' => 1,
						'province_id' => 10,
						'city_id' => 0,
						'contests_count' => 5,
						'trades_count' => 60,
						'experts_count' => 40,
						'users_count' => 150 
				),
				array (
						'id' => 32,
						'name' => '四川',
						'logo' => 'logo',
						'creative_index' => '12345',
						'intro' => '1.mkv',
						'type' => 2,
						'nation_id' => 1,
						'province_id' => 0,
						'city_id' => 0,
						'contests_count' => 350,
						'trades_count' => 470,
						'experts_count' => 980,
						'users_count' => 5650 
				),
				array (
						'id' => 3201,
						'name' => '成都',
						'logo' => 'logo',
						'creative_index' => '12345',
						'intro' => '1.mkv',
						'type' => 3,
						'nation_id' => 1,
						'province_id' => 32,
						'city_id' => 0,
						'contests_count' => 50,
						'trades_count' => 60,
						'experts_count' => 40,
						'users_count' => 650 
				),
				array (
						'id' => 320103,
						'name' => '金牛区',
						'logo' => 'logo',
						'creative_index' => '12345',
						'intro' => '1.mkv',
						'type' => 4,
						'nation_id' => 1,
						'province_id' => 32,
						'city_id' => 3201,
						'contests_count' => 10,
						'trades_count' => 170,
						'experts_count' => 20,
						'users_count' => 250 
				) 
		);
		
		DB::table ( 'regions' )->insert ( $inserts );
	}
}
