<?php
use App\Modules\EncyclopediaModule;

class EncyclopediaModuleTest extends TestCase {

    public function setUp() {
        parent::setUp();
        DB::table('encyclopedias')->truncate();
    }

    public function testsearchEncyclopediasByKeyword() {
        $data = [
            [
                'user_id' => 1,
                'item_type' => 4,
                'item_id' => 0,
                'title' => '总之也还好',
                'cover' => 'data/image/apple.jpg',
                'content' => 'nice',
                'tags' => '{"apple":"delicious"}',
                'creative_index' => 20,
                'create_time' => time()
            ],
            [
                'user_id' => 2,
                'item_type' => 4,
                'item_id' => 0,
                'title' => '还好没错过',
                'cover' => 'data/image/apple.jpg',
                'content' => 'nice',
                'tags' => '{"apple":"delicious"}',
                'creative_index' => 20,
                'create_time' => time()
            ]
        ];
        DB::table('encyclopedias')->insert($data);
        
        $result = EncyclopediaModule::searchEncyclopediasByKeyword('还好',0,4);
        
        $this->assertEquals(2, count($result));
    }
    
    public function testgetHotEncyclopedias() {
        $data = [
        [
        'user_id' => 1,
        'item_type' => 4,
        'item_id' => 0,
        'title' => '总之也还好',
        'cover' => 'data/image/apple.jpg',
        'content' => 'nice',
        'tags' => '{"apple":"delicious"}',
        'creative_index' => 20,
        'create_time' => time()
        ],
        [
        'user_id' => 2,
        'item_type' => 4,
        'item_id' => 0,
        'title' => '还好没错过',
        'cover' => 'data/image/apple.jpg',
        'content' => 'nice',
        'tags' => '{"apple":"delicious"}',
        'creative_index' => 20,
        'create_time' => time()
        ]
        ];
        DB::table('encyclopedias')->insert($data);
        
        $result = EncyclopediaModule::getHotEncyclopedias(2);
        
        $this->assertEquals(2, count($result));
    }
    
    public function testshareEncyclopedia() {
        
        $userId = 1;
        $title = 'hijacker';
        $cover = 'data/image/apple.jpg';
        $content = 'nice to meet you!';
        $tagsNames = ['a'=> 'b'];
        $result = EncyclopediaModule::shareEncyclopedia($userId, $title, $cover, $content, $tagsNames);
        
        $this->assertEquals(1, $result);
        
    }
    
    public function testgetEncyclopediaById() {
        $data = [
        [
        'user_id' => 1,
        'item_type' => 4,
        'item_id' => 0,
        'title' => '总之也还好',
        'cover' => 'data/image/apple.jpg',
        'content' => 'nice',
        'tags' => '{"apple":"delicious"}',
        'creative_index' => 20,
        'create_time' => time()
        ],
        [
        'user_id' => 2,
        'item_type' => 4,
        'item_id' => 0,
        'title' => '还好没错过',
        'cover' => 'data/image/apple.jpg',
        'content' => 'nice',
        'tags' => '{"apple":"delicious"}',
        'creative_index' => 20,
        'create_time' => time()
        ]
        ];
        DB::table('encyclopedias')->insert($data);
        
        $result = EncyclopediaModule::getEncyclopediaById(2);
        $this->assertEquals(2, $result->id);
        
        $result = EncyclopediaModule::getEncyclopediaById(3);
        $this->assertEmpty($result);       
    }
}