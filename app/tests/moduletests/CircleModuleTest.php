<?php

use App\Modules\CircleModule;
use App\Models\Circle;
use App\Models\CircleMember;
use App\Models\User;
use App\Modules\NotificationModule;

class CircleModuleTest extends TestCase {
   public function __construct() {
       parent::__construct();
       include_once 'app/tests/moduletests/ExchangeModuleTest.php';
       include_once 'app/tests/controllertests/ExchangeControllerTest.php';
   }
    public function setUp() {
        parent::setUp();
        DB::table('circles')->truncate();
        DB::table('circle_members')->truncate();
    }

    public function testCreateCircle() {       
        //case 1
        $userId = 1;
        $name = '朋友';
        $result = CircleModule::createCircle($userId, $name);
        
        $this->assertTrue($result == 1);
        $count = Circle::count();
        $this->assertEquals(1, $count);
        
        $userId = 1;
        $name = '朋友';
        $result = CircleModule::createCircle($userId, $name);
        
        $this->assertFalse($result);
        $count = Circle::count();
        $this->assertEquals(1, $count);
    }
    
    public function testModifyCircleName() {
        $this->prepareData();
        //case 1 圈子不存在
        $id = 100;
        $name = '朋友';
        $userId = 1;
        $result = CircleModule::modifyCircleName($id, $name, $userId);
        $this->assertFalse($result);
        //case 2 没有权限
        $id = 2;
        $name = '亲人';
        $userId = 1;
        $result = CircleModule::modifyCircleName($id, $name, $userId);
        $this->assertFalse($result);
        //case 3 圈子名和以前一样
        $id = 1;
        $name = '亲人';
        $userId = 1;
        $result = CircleModule::modifyCircleName($id, $name, $userId);
        $this->assertTrue($result);
        //case 4 已有圈子名
        $id = 1;
        $name = '家人';
        $userId = 1;
        $result = CircleModule::modifyCircleName($id, $name, $userId);
        $this->assertFalse($result);
       
        //case 5 修改成功
        $id = 1;
        $name = '朋友';
        $userId = 1;
        $result = CircleModule::modifyCircleName($id, $name, $userId);
        $this->assertTrue($result);
        $circle = Circle::find($id);
        
        $this->assertEquals($name, $circle->name);
    }
    
    public function testGetCircle() {
        $this->prepareData();
        $id = 1;
        $circle = CircleModule::getCircle($id);
        $this->assertEquals($id, $circle->id);
    }
    
    public function testGetCirclesByUserId() {
        $this->prepareData();
        $userId = 1;
        $circles = CircleModule::getCirclesByUserId($userId);
        $this->assertEquals(2, count($circles));
        foreach($circles as  $circle) {
            $this->assertEquals($userId, $circle->user_id);
        }
    }
    
    public function testGetCircleAuthority() {
        $this->prepareData();
        $userId = 1;
        //case 1
        $circleId = 1;
        $result = CircleModule::getCircleAuthority($circleId, $userId);
        $this->assertTrue($result != false);
        $this->assertEquals($circleId, $result->id);
        
        //case 2
        $circleId = 2;
        $result = CircleModule::getCircleAuthority($circleId, $userId);
        $this->assertFalse($result);
    }

    public function testAddCircleMember() {
        $this->prepareCircleData();   
        $this->prepareUsers();     
        //case 1 无权限       
        $circleId = 2;
        $ownerUserId = 1;
        $userId = 2;
        $result = CircleModule::addCircleMember($circleId, $ownerUserId, $userId);
        $this->assertFalse($result);
        
        $count = CircleMember::count();
        $this->assertEquals(0, $count);
        
        //case 2 添加成功       
        $circleId = 1;
        $ownerUserId = 1;
        $userId = 5;
       
        $circle = Circle::find($circleId);
        $num = $circle->member_count;
        
        $result = CircleModule::addCircleMember($circleId, $ownerUserId, $userId);
        $this->assertTrue($result);
        
        $circleAfter = Circle::find($circleId);
        $numAfter = $circleAfter->member_count;
        $this->assertEquals($num+1,$numAfter);
        
        $count = CircleMember::count();
        $this->assertEquals(1, $count);
        
        //case 3 重复添加
        $circleId = 1;
        $ownerUserId = 1;
        $userId = 5;
        $result = CircleModule::addCircleMember($circleId, $ownerUserId, $userId);
        $this->assertTrue($result);
        
        $circleNow = Circle::find($circleId);
        $numNow = $circleNow->member_count;
        $this->assertEquals($numAfter, $numNow);
        
        $count = CircleMember::count();
        $this->assertEquals(1, $count);
        
    }
 
    public function testRemoveCircleMember() {
        $this->prepareData();
        $this->prepareUsers();
        
        //case 1 无权限
        $circleId = 2;
        $ownerUserId = 1;
        $userId = 5;
        $result = CircleModule::removeCircleMember($circleId, $ownerUserId, $userId);
        $this->assertFalse($result);
        
        //case 2 删除成功
        $circleId = 1;
        $ownerUserId = 1;
        $userId = 3;
        
        $circle = Circle::find($circleId);
        $result = CircleModule::removeCircleMember($circleId, $ownerUserId, $userId);
        $this->assertTrue($result);
        $circleOne = Circle::find($circleId);
        $this->assertEquals($circle->member_count - 1, $circleOne->member_count);
        $user = User::find($userId);
        $this->assertEquals(2, $user->follower_count);
        
        //case 3 要删除成员不存在
        $result = CircleModule::removeCircleMember($circleId, $ownerUserId, $userId);
        $this->assertFalse($result);
        $circleTwo = Circle::find($circleId);
        $this->assertEquals($circleOne->member_count, $circleTwo->member_count);
        
        //case 4
        $circleId = 3;
        $ownerUserId = 1;
        $userId = 3;
        $result = CircleModule::removeCircleMember($circleId, $ownerUserId, $userId);
        $this->assertTrue($result);
        $user = User::find($userId);
        $this->assertEquals(0, $user->follower_count);
    }
  
    public function testDeleteCircleAndMembers() {
        $this->prepareData();
        $this->prepareUsers();
        //case 1 无权限
        $userId = 1;
        $circleId = 2;
        $result = CircleModule::deleteCircleAndMembers($circleId, $userId);
        $this->assertFalse($result);
        
        //case 2 圈子不存在
        $userId = 1;
        $circleId = 100;
        $result = CircleModule::deleteCircleAndMembers($circleId, $userId);
        $this->assertFalse($result);
        
        //case 3 删除成功
        $userId = 1;
        $circleId = 1;
        $result = CircleModule::deleteCircleAndMembers($circleId, $userId);
        $this->assertTrue($result);
        $circle = Circle::find($circleId);
        $this->assertEmpty($circle);
        
        $member = CircleMember::where('circle_id', $circleId)->get();
        $this->assertCount(0, $member);
    }

    public function testGetAllMembersByUserId() {
        $this->prepareData();
        
        $userId = 1;       
        $result = CircleModule::getAllMembersByUserId($userId);
        
        $this->assertCount(3, $result);
        
        $expect = array(2, 3, 4);
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expect, $result, 'user_id');
    }
    
    public function testGetMembersByCircleId() {
        $this->prepareData();
        
        $circleId = 1;
        $result = CircleModule::getMembersByCircleId($circleId);
        
        $this->assertCount(2, $result);

        $expect = array(1, 2);
        $assert = new ExchangeModuleTest();
        $assert->assertExpect($expect, $result);
    }

    public function testGetMembersAndCircleInfoByCircleId() {
        $this->prepareData();
        
        $userId = 1;
        $circleId = 1;
        $result = CircleModule::getMembersAndCircleInfoByCircleId($circleId, $userId);
        $this->assertCount(2, $result);
        
        $expect = array(
            array(
                'circle_num' => 1,
                'name' => '亲人',
                'user_id' => 2,
                'circle_id' => 1
            ),
            array(
                'circle_num' => 2,
                'name' => '亲人',
                'user_id' => 3,
                'circle_id' => 1
            )
        );

        foreach ($result as $key => $r) {
            $this->assertEquals($r->circle_num, $expect[$key]['circle_num']);
            $this->assertEquals($r->name, $expect[$key]['name']);
            $this->assertEquals($r->user_id, $expect[$key]['user_id']);
            $this->assertEquals($r->circle_id, $expect[$key]['circle_id']);
        }
    }
    
    public function testGetAllMembersAndCircleInfo() {
        $this->prepareData();
        
        $userId = 1;
        $result = CircleModule::getAllMembersAndCircleInfo($userId);
        $this->assertCount(3, $result);
        
        $expect = array(
                array(
                        'circle_num' => 1,
                        'name' => '亲人',
                        'user_id' => 2,
                        'circle_id' => 1
                ),
                array(
                        'circle_num' => 2,
                        'name' => '亲人',
                        'user_id' => 3,
                        'circle_id' => 1
                ),array(
                        'circle_num' => 1,
                        'name' => '家人',
                        'user_id' => 4,
                        'circle_id' => 3
                ),
        );
        
        foreach ($result as $key => $r) {
            $this->assertEquals($r->circle_num, $expect[$key]['circle_num']);
            $this->assertEquals($r->name, $expect[$key]['name']);
            $this->assertEquals($r->user_id, $expect[$key]['user_id']);
            $this->assertEquals($r->circle_id, $expect[$key]['circle_id']);
        }
    }

    public function testGetMemberAndCircleInfoByMemberUserId() {
        $this->prepareData();
        
        $ownerUserId = 1;
        $memberUserId = 3;
        
        $result = CircleModule::getMemberAndCircleInfoByMemberUserId($memberUserId, $ownerUserId);
        $this->assertCount(2, $result);
        
        $expect = array(
            array(
                'id' => 1,
                'name' => '亲人'
            ),
            array(
                'id' => 3,
                'name' => '家人'
            )
        );
        
        foreach ($result as $key => $r) {
            $this->assertEquals($r->id, $expect[$key]['id']);
            $this->assertEquals($r->name, $expect[$key]['name']);
        }
    }
    
    public function testGetCircleMe() {
        $this->prepareData();
        
        $userId = 1;
        
        $result = CircleModule::getCircleMe($userId);
        
        $expect = array(5, 2);
        
        $assertCircleUsers = new ExchangeModuleTest();
        $assertCircleUsers->assertExpect($expect, $result, 'user_id');
    }
    protected function prepareCircleData() {
        $circles = array(
            array(
                'user_id' => 1,
                'name' => '亲人',
                'member_count' => 2,
                'create_time' => time()
            ),
            array(
                'user_id' => 2,
                'name' => '朋友',
                'member_count' => 1,
                'create_time' => time()
            ),
            array(
                'user_id' => 1,
                'name' => '家人',
                'member_count' => 2,
                'create_time' => time()
            ),array(
                'user_id' => 5,
                'name' => '朋友',
                'member_count' => 1,
                'create_time' => time()
        ),array(
                'user_id' => 2,
                'name' => '家人',
                'member_count' => 1,
                'create_time' => time()
        )
        );
        DB::table("circles")->insert($circles);
    }

    protected function prepareCircleMemberData() {
        $members = array(
            array(
                'circle_id' => 1,
                'circle_owner_user_id' => 1,
                'user_id' => 2,
                'create_time' => time()
            ),
            array(
                'circle_id' => 1,
                'circle_owner_user_id' => 1,
                'user_id' => 3,
                'create_time' => time()
            ),
            array(
                'circle_id' => 2,
                'circle_owner_user_id' => 2,
                'user_id' => 1,
                'create_time' => time()
            ),
            array(
                'circle_id' => 3,
                'circle_owner_user_id' => 1,
                'user_id' => 3,
                'create_time' => time()
            ),
            array(
                'circle_id' => 3,
                'circle_owner_user_id' => 1,
                'user_id' => 4,
                'create_time' => time()
            ),array(
                'circle_id' => 4,
                'circle_owner_user_id' => 5,
                'user_id' => 1,
                'create_time' => time()
        ),array(
                'circle_id' => 5,
                'circle_owner_user_id' => 2,
                'user_id' => 1,
                'create_time' => time()
        )
        );
        DB::table('circle_members')->insert($members);
    }

    protected function prepareUsers() {
        DB::table('users')->truncate();
        $no = 1;
        //准备用户数据
        $insertUsers = array(
            array(
                'name' => 'cc1',
                'cc_no' => $no ++,
                'email' => 'cc1@163.com',
                'follower_count' => 3,
            )
            ,
            array(
                'name' => 'cc2',
                'cc_no' => $no ++,
                'email' => 'cc2@163.com',
                'follower_count' => 1,
            ),
            array(
                'name' => 'cc3',
                'cc_no' => $no ++,
                'email' => 'cc3@163.com',
                'follower_count' => 2,
            ),
            array(
                'name' => 'cc4',
                'cc_no' => $no ++,
                'email' => 'cc4@163.com',
                'follower_count' => 1,
            ),
            array(
                'name' => 'cc5',
                'cc_no' => $no ++,
                'email' => 'cc5@163.com',
                'follower_count' => 0,
            )
        );
        $fills = new ExchangeControllerTest();
        $fills->fillOtherField($insertUsers);
    
        DB::table('users')->insert($insertUsers);
    }

    protected function prepareData() {
        $this->prepareCircleData();
        $this->prepareCircleMemberData();
    }
}
