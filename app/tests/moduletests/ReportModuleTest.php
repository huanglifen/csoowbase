<?php
use App\Modules\ReportModule;

class ReportModuleTest extends TestCase {
    
    public function setUp() {
        parent::setUp();
        DB::table('reports')->truncate();
    }
    
    public function testSaveReport() {

		$result = ReportModule::saveReport(1,1,1,1);
	    $this->assertEquals(1, $result);


	    $result = ReportModule::saveReport(1,21,1,2);
	    $this->assertEquals(2, $result);

    }



	public function testisExist() {

		ReportModule::saveReport(1,1,1,1);
		$result = ReportModule::isExist(1,1,1);
		$this->assertEquals(true, $result);

		$result = ReportModule::isExist(1,1,2);
		$this->assertEquals(false, $result);

	}


}
