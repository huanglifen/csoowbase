<?php
use App\Modules\AccountModule;
use App\Models\User;
use App\Models\AccountChangeLog;

class AccountModuleTest extends TestCase {
    
    public function setUp() {
        parent::setUp();
        DB::table('users')->truncate();
        DB::table('account_change_logs')->truncate();
    }
    
    public function prepareDatas() {
        $data = [
        [
        'name' => 'leon1',
        'cc_no' => 100001,
        'email' => '529909847@qq.com',
        'email_is_verified' => 0,
        'email_verify_code' => '',
        'password' => 'R446xUHwii92gr6u3b47815c926e096f424678e421528413a788f0942947655d36b4ec8ab672604c',
        'password_reset_code' => '',
        'activation_code' => '',
        'group_id' => 1,
        'avatar' => '',
        'creative_index' => 0,
        'coin' => 0,
        'creative_index_bonus_amount' => 10,
        'gender' => 0,
        'birthday' => 0,
        'introduction' => '',
        'tags' => '',
        'mobile_phone_no' => '',
        'mobile_phone_is_verified' => 0,
        'mobile_phone_verify_code' => '',
        'province_id' => 1,
        'city_id' => 2,
        'district_id' => 3,
        'school_id' => 4,
        'organization_id' => 5,
        'expert_organization_id' => 6,
        'expert_duty_id' => 7,
        'expert_field_id' => 9,
        'expert_industry_id' => 8,
        'expert_title' => '',
        'follower_count' => 0,
        'register_time' => time(),
        'register_ip' => ''
            ]
            ];
        DB::table('users')->insert($data);
    }
    public function testUpdateUserPassword() {
        
        $this->prepareDatas();
        
        $userId = 2;
        $currentPassword = '123456';
        $newPassword = '1234567';
        
        $result = AccountModule::updateUserPassword($currentPassword, $newPassword, $userId);
        
        $this->assertFalse($result);
        
        $userId = 1;
        $currentPassword = '123456';
        $newPassword = '1234567';
        
        $result = AccountModule::updateUserPassword($currentPassword, $newPassword, $userId);
        
        $this->assertFalse($result);
        
        $userId = 1;
        $currentPassword = 'asdf';
        $newPassword = '123456';
        
        $result = AccountModule::updateUserPassword($currentPassword, $newPassword, $userId);
        
        $this->assertTrue($result);
    }
    
    public function testUpdateUserBasicInfo() {
        $this->prepareDatas();
        
        $userId = 2;
        $userBasicInfo = array(
                'birthday' => 123456,
                'gender' => 0,
                'mobile_phone_no' => '12345678900',
                'email' => '123@134.com',
                'tags' => 'apple',
                'province_id' => 11,
                'city_id' => 22,
                'district_id' => 33,
                'school_id' => 44,
                'organization_id' => 55
            );
        $result = AccountModule::updateUserBasicInfo($userBasicInfo, $userId);
        $this->assertFalse($result);
        
        $userId = 1;
        $userBasicInfo = array(
            'birthday' => 123456,
            'gender' => 0,
            'mobile_phone_no' => '12345678900',
            'email' => '123@134.com',
            'tags' => 'apple',
            'province_id' => 11,
            'city_id' => 22,
            'district_id' => 33,
            'school_id' => 44,
            'organization_id' => 55
        );
        $result = AccountModule::updateUserBasicInfo($userBasicInfo, $userId);
        $this->assertTrue($result);
    }
    
    public function testUpdateAvatar() {
        $this->prepareDatas();
        
        $userId = 2;
        $newUserAvatar = 'data/beautiful.jpg';
        $result = AccountModule::updateAvatar($userId, $newUserAvatar);
        $this->assertFalse($result);
        
        $userId = 1;
        $newUserAvatar = 'data/beautiful.jpg';
        $result = AccountModule::updateAvatar($userId, $newUserAvatar);
        $this->assertTrue($result);
        
    }
    
    public function testGetUserCreativeIndexAward() {
        $this->prepareDatas();
        $userId = 2;
        $result = AccountModule::getUserCreativeIndexAward($userId);
        
        $this->assertFalse($result);
        
        $userId = 1;
        $result = AccountModule::getUserCreativeIndexAward($userId);
        $this->assertTrue($result);

        $user = User::find(1);
        $this->assertEquals(10, $user->coin);
        $this->assertEquals(0, $user->creative_index_bonus_amount);
        
        $log = AccountChangeLog::find(1);
        $this->assertNotEmpty($log);
    }
    
    public function testSearchAccountLogByUserID() {
        $data = [
        [
        'user_id' => 1,
        'account_type' => 1,
        'amount' => 20,
        'balance' => 100,
        'reason' => 1,
        'related_info' => 'aaa',
        'create_time' => 100000
        ],
        [
        'user_id' => 1,
        'account_type' => 1,
        'amount' => -20,
        'balance' => 80,
        'reason' => 1,
        'related_info' => 'aaa',
        'create_time' => 200000
        ],
        [
        'user_id' => 1,
        'account_type' => 1,
        'amount' => -20,
        'balance' => 80,
        'reason' => 1,
        'related_info' => 'aaa',
        'create_time' => 200000
        ]
        ];
        DB::table('account_change_logs')->insert($data);
        
        $userId = 1;
        $result = AccountModule::searchAccountLogByUserID($userId, 1);
        $this->assertEquals(3,count($result));
        
        $userId = 1;
        $result = AccountModule::searchAccountLogByUserID($userId, 1, 20);
        $this->assertEquals(1,count($result));
        
        $userId = 1;
        $result = AccountModule::searchAccountLogByUserID($userId, 1, -20);
        $this->assertEquals(2,count($result));
        
        $userId = 1;
        $result = AccountModule::searchAccountLogByUserID($userId, 1, 0, 90000, 100000);
        $this->assertEquals(1,count($result));
    }
}