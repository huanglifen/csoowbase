<?php namespace App\Models;

abstract class BaseModel extends \Eloquent {

	public $timestamps = false;

	protected function buildInfo($name, $value) {
		$info = [];
		if (isset($this->attributes['info'])) {
			$info = json_decode($this->attributes['info'], true);
		}

		$info[$name] = $value;
		$this->attributes['info'] = json_encode($info);
	}

	protected function getInfo($name) {
		$info = json_decode($this->attributes['info'], true);

		return isset($info[$name]) ? $info[$name] : null;
	}

}