<?php namespace App\Models;

class Chat extends BaseModel{

	protected $table = 'chats';
	
	public static function getChatsByStatus($toUserId, $status, $offset, $limit) {
	    $sql = "select * from (select * from chats ";
	    $sql .= "where to_user_id = " .$toUserId." and status = ". $status." order by id desc) as chat ";
	    $sql .="group by user_id order by id desc limit ".$offset.",".$limit;
	    return \DB::select($sql);;
	}
	
	public static function getLateChatByUserIds(array $userIds, $toUserId) {
	    $userIdString = '';
	    foreach($userIds as $key => $userId) {
	        if($key == 0) {
	            $userIdString .=$userId;
	        }else{
	        $userIdString .= ','.$userId;
	        }
	    }

	    $sql = "select * from (select * from chats ";
	    $sql .="where to_user_id= ".$toUserId." and user_id in (".$userIdString.") order by id desc) as chat ";
	    $sql .="group by user_id";
	    
	    return \DB::select($sql);
	}

}