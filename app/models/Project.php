<?php namespace App\Models;

class Project extends ContestModel {

	protected static $contestType = 1;

	const INVESTMENT_END_TIME = 'investment_end_time'; //项目投资结束时间，天数
	const GOAL_AMOUNT = 'goal_amount'; //寻找项目投资金额
	const PAY_DEPOSIT_REMIND = 'pay_deposit_remind'; //支付定金剩余时间X小时提醒
	const PAY_FINAL_REMIND = 'pay_final_remind'; //支付全款/尾款剩余时间X小时提醒
	const FINISH_REMIND = 'finish_remind'; //项目完工剩余时间X小时提醒
	
	const PAY_TIME = 'pay_time'; //定金或者全款支付时间
	const PAY_FINAL_TIME = 'pay_final_time'; //尾款支付时间
	const COMPLETE_TIME = 'complete_time'; //项目完工时间
	const CLOSE_TIME = 'close_time'; //项目获得成功时间或者项目失败关闭时间

	public function setInvestmentEndTimeAttribute($value) {
		$this->buildInfo(static::INVESTMENT_END_TIME, $value);
	}

	public function setGoalAmountAttribute($value) {
		$this->buildInfo(static::GOAL_AMOUNT, $value);
	}

	public function setPayDepositRemindAttribute($value) {
		$this->buildInfo(static::PAY_DEPOSIT_REMIND, $value);
	}

	public function setPayFinalRemindAttribute($value) {
		$this->buildInfo(static::PAY_FINAL_REMIND, $value);
	}

	public function setFinishRemindAttribute($value) {
		$this->buildInfo(static::FINISH_REMIND, $value);
	}
	
	public function setPayTimeAttribute($value) {
	    $this->buildInfo(static::PAY_TIME, $value);
	}
	
	public function setPayFinalTimeAttribute($value) {
	    $this->buildInfo(static::PAY_FINAL_TIME, $value);
	}
	
	public function setCompleteTimeAttribute($value) {
	    $this->buildInfo(static::COMPLETE_TIME, $value);
	}
	public function setCloseTimeAttribute($value) {
	    $this->buildInfo(static::CLOSE_TIME, $value);
	}

	public function getInvestmentEndTimeAttribute() {
		return $this->getInfo(static::INVESTMENT_END_TIME);
	}

	public function getGoalAmountAttribute() {
		return $this->getInfo(static::GOAL_AMOUNT);
	}

	public function getPayDepositRemindAttribute() {
		return $this->getInfo(static::PAY_DEPOSIT_REMIND);
	}

	public function getPayFinalRemindAttribute() {
		return $this->getInfo(static::PAY_FINAL_REMIND);
	}

	public function getFinishRemindAttribute() {
		return $this->getInfo(static::FINISH_REMIND);
	}
	
	public function getPayTimeAttribute($value) {
	    return $this->getInfo(static::PAY_TIME);
	}
	
	public function getPayFinalTimeAttribute($value) {
	    return $this->getInfo(static::PAY_FINAL_TIME);
	}
	
	public function getCompleteTimeAttribute($value) {
	    return $this->getInfo(static::COMPLETE_TIME);
	}
	public function getCloseTimeAttribute($value) {
	    return $this->getInfo(static::CLOSE_TIME);
	}

}