<?php namespace App\Models;

class Organization extends BaseModel {

	protected $table = 'organizations';
	
	const TYPE_GOVERNMENT = 1; //行政机构
	const TYPE_CORPORATE = 2; //企业
}