<?php


namespace App\Models;

use App\Common\Utils;
class User extends BaseModel {
	const CCZ_USED = 'ccz_used'; // 已使用创创召
	const LAST_LOGIN_TIME = 'last_login_time'; // 最后登录时间
	const CONSISTENT_LOGIN_DAYS = 'consistent_login_days'; // 连续登录天数
	const FILL_AREA_PROFILE = 'fill_area_profile'; // 完善地区学校政企资料
	const FILL_ONLY_AREA_PROFILE = 'fill_only_area_profile'; //只完善地区资料
	const LEVEL = 'level';//用户等级
	const EXPERT_AVATAR = 'expert_avatar';
	const EXPERT_INTRODUCTION = 'expert_introduction';
	const EMAIL_MODIFY_CODE = 'email_modify_code';//修改邮箱验证码
	const EMAIL_MODIFY_TIME = 'email_modify_time';//修改邮箱验证时间
	const NEW_MODIFY_EMAIL = 'new_modify_email';//接收邮件新邮箱

	const MOBILE_MODIFY_CODE = 'mobile_modify_code';//修改手机验证码
	const MOBILE_MODIFY_TIME = 'mobile_modify_time';//修改手机验证时间
	const MOBILE_MODIFY_NEW = 'mobile_modify_new';//接收邮件新手机

	protected $table = 'users';
	protected $hidden = [ 
			'email',
			'password',
			'password_reset_code',
			'coin' 
	];
	public function setCczUsedAttribute($value) {
		$this->buildInfo ( static::CCZ_USED, $value );
	}
	public function setLastLoginTimeAttribute($value) {
		$this->buildInfo ( static::LAST_LOGIN_TIME, $value );
	}
	public function setConsistentLoginDaysAttribute($value) {
		$this->buildInfo ( static::CONSISTENT_LOGIN_DAYS, $value );
	}
	public function setFillAreaProfileAttribute($value) {
		$this->buildInfo ( static::FILL_AREA_PROFILE, $value );
	}
	public function setFillOnlyAreaProfileAttribute($value) {
	    $this->buildInfo ( static::FILL_ONLY_AREA_PROFILE, $value );
	}
	public function setExpertAvatarAttribute($value) {
	    $this->buildInfo ( static::EXPERT_AVATAR, $value);
	}
	public function setExpertIntroductionAttribute($value) {
	    $this->buildInfo( static::EXPERT_INTRODUCTION, $value);
	}
	public function setEmailModifyCodeAttribute($value) {
	    $this->buildInfo( static::EMAIL_MODIFY_CODE, $value);
	}
	public function setEmailModifyTimeAttribute($value) {
	    $this->buildInfo( static::EMAIL_MODIFY_TIME, $value);
	}
	public function setNewModifyEmailAttribute($value) {
	    $this->buildInfo( static::NEW_MODIFY_EMAIL, $value);
	}

	public function setMobileModifyCodeAttribute($value) {
		$this->buildInfo( static::MOBILE_MODIFY_CODE, $value);
	}

	public function setMobileModifyTimeAttribute($value) {
		$this->buildInfo( static::MOBILE_MODIFY_TIME, $value);
	}

	public function setMobileModifyNewAttribute($value) {
		$this->buildInfo( static::MOBILE_MODIFY_NEW, $value);
	}

	public function getCczUsedAttribute() {
		return $this->getInfo ( static::CCZ_USED );
	}
	public function getLastLoginTimeAttribute() {
		return $this->getInfo ( static::LAST_LOGIN_TIME );
	}
	public function getConsistentLoginDaysAttribute() {
		return $this->getInfo ( static::CONSISTENT_LOGIN_DAYS );
	}
	public function getFillAreaProfileAttribute() {
		return $this->getInfo ( static::FILL_AREA_PROFILE );
	}
	public function getFillOnlyAreaProfileAttribute() {
	    return $this->getInfo ( static::FILL_ONLY_AREA_PROFILE );
	}
	public function getExpertAvatarAttribute() {
	    return $this->getInfo( static::EXPERT_AVATAR);
	}
	public function getExpertIntroductionAttribute() {
	    return $this->getInfo( static::EXPERT_INTRODUCTION)?$this->getInfo( static::EXPERT_INTRODUCTION):'';
	}
	 public function setLevelAttribute($value){
	 	$value = Utils::getCreativeLevel($value);
		$this->buildInfo(static::LEVEL, $value);
	}
	public function getLevelAttribute(){
		return $this->getInfo(static::LEVEL);
	} 
	
	public function getEmailModifyCodeAttribute(){
	    return $this->getInfo(static::EMAIL_MODIFY_CODE);
	}
	
	public function getEmailModifyTimeAttribute(){
	    return $this->getInfo(static::EMAIL_MODIFY_TIME);
	}
	
	public function getNewModifyEmailAttribute(){
	    return $this->getInfo(static::NEW_MODIFY_EMAIL);
	}

	public function getMobileModifyCodeAttribute(){
		return $this->getInfo(static::MOBILE_MODIFY_CODE);
	}

	public function getMobileModifyTimeAttribute(){
		return $this->getInfo(static::MOBILE_MODIFY_TIME);
	}

	public function getMobileModifyNewAttribute(){
		return $this->getInfo(static::MOBILE_MODIFY_NEW);
	}

}