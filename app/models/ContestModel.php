<?php namespace App\Models;

abstract class ContestModel extends BaseModel {

	protected $table = 'contests';

	const END_SOON_REMIND = 'end_soon_remind'; //比赛到期还剩X天提醒

	protected static $contestType;

	public static function __callStatic($method, $parameters) {
		$query = parent::__callStatic($method, $parameters);
		return $query->where('type', static::$contestType);
	}

	public function __construct() {
		$this->attributes['type'] = static::$contestType;
	}

	public function setEndSoonRemindAttribute($value) {
		$this->buildInfo(static::END_SOON_REMIND, $value);
	}

	public function getEndSoonRemindAttribute() {
		return $this->getInfo(static::END_SOON_REMIND);
	}

}