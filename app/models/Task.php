<?php namespace App\Models;

class Task extends ContestModel {

	protected static $contestType = 2;

	const END_TIME = 'end_time'; //限制时间，天数
	const AWARD = 'awards'; //完成任务的奖励
	const FINISH_REMIND = 'finish_remind'; //任务完工剩余时间X小时提醒

	public function setEndTimeAttribute($value) {
		$this->buildInfo(static::END_TIME, $value);
	}

	public function setAwardsAttribute($value) {
		$this->buildInfo(static::AWARD, $value);
	}

	public function setFinishRemindAttribute($value) {
		$this->buildInfo(static::FINISH_REMIND, $value);
	}

	public function getEndTimeAttribute() {
		return $this->getInfo(static::END_TIME);
	}

	public function getAwardsAttribute() {
		return $this->getInfo(static::AWARD);
	}

	public function getFinishRemindAttribute() {
		return $this->getInfo(static::FINISH_REMIND);
	}

}