<?php namespace App\Models;

class Hiring extends ContestModel {

	protected static $contestType = 3;

	const END_TIME = 'end_time'; //限制时间，天数
	const CHOOSE_TIME = 'choose_time'; //获胜明星选出时间

	public function setEndTimeAttribute($value) {
		$this->buildInfo(static::END_TIME, $value);
	}
	
	public function setChooseTimeAttribute($value) {
		$this->buildInfo(static::CHOOSE_TIME, $value);
	}

	public function getEndTimeAttribute() {
		return $this->getInfo(static::END_TIME);
	}
	
	public function getChooseTimeAttribute() {
		return $this->getInfo(static::CHOOSE_TIME);
	}

}