///import core
///commands 预览
///commandsName  Preview
///commandsTitle  预览
/**
 * 预览
 * @function
 * @name UM.execCommand
 * @param   {String}   cmdName     preview预览编辑器内容
 */
UM.commands['preview'] = {
    execCommand : function(){
        var w = window.open('', '_blank', ''),
            d = w.document;
		//添加代码非原始代码
		var width = $(this)[0].container.clientWidth,
			height = $(this)[0].container.clientHeight,
			border = this.getContent(null,null,true) ? 'border:1px solid #d4d4d4;' : 'border:none;',
			css = 'style="width:680px; padding:0 10px 0; word-break:break-all; '+border+' min-height:'+height+'px;"'
        d.open();
        //原始代码
		//d.write('<html><head></head><body><div>'+this.getContent(null,null,true)+'</div></body></html>');
		d.write('<html><head></head><body><div '+css+'>'+this.getContent(null,null,true)+'</div></body></html>');
		
        d.close();
    },
    notNeedUndo : 1
};
