var EDITORMOTHED = EDITORMOTHED || {};
EDITORMOTHED.package = {
	getContent : function(editor){
		return editor.getContent();
	},
	
	reset : function(editor){
		var ue = editor;
		ue.setContent('');
		ue.reset();
	},
	
	insertHtml : function(editor,val){
		var ue = editor;
		ue.setContent('');
		ue.focus();
		ue.execCommand('insertHtml', val);
		ue.reset();
	}
}