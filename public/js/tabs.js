WG.Tabs = {
	init : function(){
		var marginwidth = this.setting.marginwidth,
			marginarray = this.setting.marginarray,//中间的margin宽度
			tabnum = $('.Js_tab').length,
			tablength,
			href = location.href,
			T = WG.Tabs,
			//step = location.href.split('#');
			step = location.hash;
			step = step.replace("#","");

		step !== "" ? step : step = 'step-one';
		
		$('.Js_tab').data('index',0);
		
		for(var i = 0; i < tabnum; i++){
			tablength = $('.Js_tab:eq(' + i + ') li').length;
			marginwidth = (T.setting.totallength - T.setting.endwidth * 2 - T.setting.liwidth * tablength)/(tablength - 2 + 1)/2;
			marginarray.push(marginwidth);
			$('.Js_tab:eq(' + i + ') li.first').attr('style','margin-right:'+marginwidth+'px');
			$('.Js_tab:eq(' + i + ') li.last').attr('style','margin-left:'+marginwidth+'px');
			$('.Js_tab:eq(' + i + ') li.center').attr('style','margin-left:'+marginwidth+'px;'+'margin-right:'+marginwidth+'px');
			$('.Js_hor_inline:eq(' + i + ')').width(T.setting.endwidth + T.setting.liwidth + marginwidth);
		}
		$('.Js_tab li').on('click',this.changeTab);
		$('.Js_tab li a[href=#' + step + ']').click();
	},
	setting : {
		endwidth : 54,//两端的宽度
		totallength : 850,//总长度
		liwidth : $('.Js_tab:eq(0) li').width(),
		marginwidth : "",
		marginarray : []
	},
	changeTab : function(){
		var tab_length = $(this).parent().children().length,
			index = $(this).index(),//li的index
			tab_index = 0,
			linewidth,
			T = WG.Tabs;
		var marginwidth = T.setting.marginwidth,
			marginarray = T.setting.marginarray;//中间的margin宽度
		
		if($(this).hasClass('Js_tab_list')){
			index = $('.Js_tab').data('index');	
		}
		
		$('.Js_tab').data('index',index);
		//alert(tab_index);
		$('.tabs_wraper').each(function(i){
			$(this).attr("data-index", i)
		});
		tab_index = $(this).parents('.tabs_wraper').data("index");
		marginwidth = marginarray[tab_index];
		linewidth = T.setting.endwidth + T.setting.liwidth * ( index + 1 ) + marginwidth * ( ( index + 1 ) * 2 - 1 );
		if(index == tab_length - 1){
			linewidth = linewidth - marginwidth + T.setting.endwidth;
		}
		$(this).parent().prevAll('.Js_hor_line').children('.Js_hor_inline').width(linewidth);
		for(var i = 0 ;i <= index; i++){
			$(this).parent().children(':eq(' + i + ')').addClass('focustab');
		}
		for(var i = index + 1; i < tab_length ;i++){
			$(this).parent().children(':eq(' + i + ')').removeClass('focustab');
		}
		$(this).parents('.Js_tabs_step').nextAll('.Js_tabs_content').hide();
		$(this).parents('.Js_tabs_step').nextAll('.Js_tabs_content:eq(' + index + ')').show();
		if($(this).hasClass('Js_tab_list')){
			return false	
		} 
		//return false;
	}
}