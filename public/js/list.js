$(function() {
	$(".Js_list_tag").scrollstop();

	/*$('.Js_scroll').imgWheelShow({
		type: 'sola',
		focusBtnType: 'click',
		time: 5000,
		width: 'full'
	});*/
})

/**
 * [LoadNextPage 列表页分页，关联tag和类别]
 * @type {Object}
 */
WG.LoadNextPage = {
	init: function() {
		//通用语列表页的tag点击，比赛分类点击，下一页点击事件
		$(".Js_nextpage").on("click", {
			t: "page"
		}, this.loadNextPage);
		$(".Js _tag .t").on("click", {
			t: "tag"
		}, this.loadNextPage);
		$(".Js_nextpage .c").on("click", {
			t: "category"
		}, this.loadNextPage);

		//tag刷新
		$(".Js_refreshtag").on("click", this.refreshTag);
		//tag筛选
		$(".Js_tag").on('click', '.t', this.tagClick);
		//tab筛选
		$(".Js_list_tab li").on('click', this.tabClick);
	},
	tabClick: function() {
		var T = WG.LoadNextPage;
		$(this).parent().find("li").removeClass('current');
		$(this).addClass('current');
		T.showList();
	},
	tagClick: function() {
		var T = WG.LoadNextPage;
		$(this).parent().find(".t").removeClass('current');
		$(this).addClass('current');
		T.showList();
	},
	showList: function() {
		var _type_id = $('.Js_list_contest_type').val();
		var _mt = WG.fn.getMatchtype(_type_id);

		var T = WG.LoadNextPage;
		var	_tag_id = $(".Js_tag a.current").data("tagid"),
			_status = $(".Js_list_tab li.current a").data("category"),
			_page = 1;
		var _data = {tag_id: _tag_id, targetType : _type_id,  page: _page, status: _status};

		WG.fn.ajax(_mt + "/index", _data, T.loadListCallback);
	},
	loadListCallback: function(data) {
		if(data.status == "ok"){
			var html = data.content;
			$("#list_content").html(html);
		}
	},
	refreshTag: function() {
		var _type_id = $('.Js_list_contest_type').val();
		var _mt = WG.fn.getMatchtype(_type_id);

		var data = {targetType:_type_id};

		WG.fn.ajax(_mt + "/hot-tags", data, function(data){
			if(data.status == "ok"){
				$(".Js_tag").html(data.content);
				//调整tag外围高度
				var _tagH = $(this).parents(".list_tag_wrap").height();
				$(".Js_list_tag").height(_tagH);
			}
		})
	},
	loadNextPage: function(event) {
		var $this = $(this),
			T = WG.LoadNextPage,
			n = 1; //默认第一页

		var _type_id = $('.Js_list_contest_type').val();
		var _mt = WG.fn.getMatchtype(_type_id);

		//判断事件类型
		if (event.data.t == "page") {}
		$this.text("加载中…");
		$(".Js_nextpage").off("click", T.loadNextPage);
		var F = WG.fn;
		var	_tag_id = $(".Js_tag a.current").data("tagid"),
			_status = $(".Js_list_tab li.current a").data("category"),
			_page = Number($('.Js_list_page_value').last().val())+1;
		var _data = {tag_id: _tag_id, targetType : _type_id,  page: _page, status: _status};

		F.ajax(_mt + "/index", _data, function(data){
			if(data.status=="ok"){
				if(data.page == 0) $(".Js_nextpage").parent().remove();
				$("#list_content").append(data.content);
				$(".Js_nextpage").on("click", {	t: "page"}, T.loadNextPage);
				$this.text("加载更多");;
			}
		})
	}
}