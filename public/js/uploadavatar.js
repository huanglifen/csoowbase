/*
 * Upload Avatar Plugin for jQuery
 * Author: AnceLee
 * Version: 0.0.1
 * Date: 2014.1.8
 */
$(function() {
	function checkhHtml5() {
		if (typeof(Worker) !== "undefined") {
			alert("支持HTML5");
		} else {
			alert("不支持HTML5");
		}
	}

	//checkhHtml5()
	var jcrop_api, boundx, boundy;
	$(".Js_avatar_shot").hide();
	$(".avatar-reshot").hide();
	$(".Js_avatar_confirm").hide();
	$('#file_upload').uploadify({
		'fileTypeDesc' : '支持jpg,gif,png图片文件',
		'fileTypeExts' : '*.gif; *.jpg; *.png',
		'fileSizeLimit' : '5MB',
		'buttonText': "本地上传",
		'buttonClass': "avatar-btn",
		'width': 158,
		'height': 32,
		'multi': false,
		'swf': WG.baseUrl+'js/uploadify.swf', //上传组件
		'uploader': WG.baseUrl+'account/upload-image', //上传php文件
		'onUploadSuccess': function(file, data, response) {
			datas = jQuery.parseJSON(data);
			if(datas.status == "ok"){
				$(".avatar-viewbox").html('<img id="upload-thumborg" src="'+ baseUrl + datas.content + '">');
				$("#avatar-pic").val(datas.content);
				$(".Js_avatar_shot").hide();
				$(".avatar-reshot").hide();
				$(".Js_avatar_confirm").show();
				UP.initJcrop(datas.content);
			}else if(datas.status == "param_error"){
				alert(datas.error)
				//var _msg = datas.error;
				//UP.showTip(_msg)

			}
		}
	});

	var UP = {
		initJcrop: function(url) {
			$('.Js_uploaded_avatar .avatar').attr("src", baseUrl+url);
			$('#upload-thumborg').Jcrop({
				onChange: UP.updatePreview,
				onSelect: UP.updatePreview,
				onRelease: UP.canclePreview,
				setSelect: [0, 0, 120, 120],
				aspectRatio: 1
			}, function() {
				// Use the API to get the real image size
				var bounds = this.getBounds();
				boundx = bounds[0];
				boundy = bounds[1];
				// Store the API in the jcrop_api variable
				jcrop_api = this;
				UP.updatePreview({
					h: 120,
					w: 120,
					x: 0,
					x2: 100,
					y: 0,
					y2: 100
				});

				if ($(".jcrop-holder").length > 0) {
					var co = $(".jcrop-holder");
					var jh = co.height(),
						jw = co.width(),
						ah = co.parent().height(),
						aw = co.parent().width();
					//if(jh < ah) $(".jcrop-holder").css("top",(ah-jh)/2);
					//if(jw < aw) $(".jcrop-holder").css("left",(aw-jw)/2);
					co.parent().css({
						"border": "none",
						"height": "auto"
					});
				}
				//this.release()
			});
		},
		updatePreview: function(c) {
			if (parseInt(c.w) > 0) {
				var rx = 120 / c.w;
				var ry = 120 / c.h;
				$('.Js_uploaded_avatar .avatar').css({
					width: Math.round(rx * boundx) + 'px',
					height: Math.round(ry * boundy) + 'px',
					marginLeft: '-' + Math.round(rx * c.x) + 'px',
					marginTop: '-' + Math.round(ry * c.y) + 'px'
				});
			}

			$('#x').val(c.x);
			$('#y').val(c.y);
			$('#x2').val(c.x2);
			$('#y2').val(c.y2);
		},
		canclePreview: function() {
			$('#x,#y,#x2,#y2').removeAttr("value");
		},
		camera: function() {

		},
		checkCoords: function() {
			if (parseInt($('#y2').val())) {
				return true;
			} else {
				var _msg = "请先选择图片区域";
				this.showTip(_msg);
				return false;
			}
		},
		updateAvatarSuccess: function(data) {
			$('.Js_uploaded_avatar .avatar').attr({"src": baseUrl+data.content, "data-original-src": baseUrl+data.content, "style":""});
			var _msg = "保存成功";
			this.showTip(_msg)
		},
		showTip: function(msg) {
			$(".avatar-viewbox").append("<div class='shot-tip'>" + msg + "</div>");
			setTimeout(function() {
				$(".shot-tip").fadeOut("1000", function() {
					$(this).remove()
				});
				$(".avatar-upload").hide();
				UP.resetBox();

			}, 1000);
		},
		resetBox: function(type){
			$(".avatar-upload").hide();
			$(".avatar-viewbox").html('<p class="s-text">请选择大于120X120像素的图片，保存为头像<br>（仅支持小于5M的jpg，gif，png格式图片）</p>').css({"border": "","height": ""});
			$(".Js_avatar_confirm").hide();
			var org_src = $('.Js_uploaded_avatar .avatar').data("original-src");
			if(type) $('.Js_uploaded_avatar .avatar').attr({"src": org_src, "style":""});
		}
	};


	var saveAvatar = function (){
		var _cr = UP.checkCoords();
		if (_cr) {
			$(".Js_avatar_confirm").addClass("disable").text("保存中").off("click", saveAvatar)
			$.ajax({
				url: baseUrl + "account/cut-avatar",
				dataType: "json",
				type: "POST",
				data: $("#cropform").serialize(),
				success: function(data) {
					if(data.status == "ok"){
						UP.updateAvatarSuccess(data);
						$(".Js_avatar_confirm").removeClass("disable").text("保存").on("click", saveAvatar)
					}					
				}
			})
		}
	};

	$(".Js_showupload").on("click", function(){
		$(".avatar-upload").show();
	});

	$(".Js_avatar_cancel").on("click", function(){UP.resetBox(true)});

	//保存已经选择区域的头像
	$(".Js_avatar_confirm").on("click", saveAvatar)

	//拍照上传
	$(".Js_camera_btn").on("click", function() {
		$(".avatar-viewbox").css({
			"border": "none",
			"height": "auto"
		});
		$(".avatar-reshot").hide();
		$(".Js_avatar_confirm").hide();
		webcam.set_api_url('u2/test.php');
		webcam.set_quality(100); // JPEG quality (1 - 100)
		webcam.set_shutter_sound(true); // play shutter click sound
		$(".avatar-viewbox").html(webcam.get_html(388, 280));

		webcam.set_hook('onComplete', function(msg) {
			// extract URL out of PHP output
			if (msg.match(/(http\:\/\/\S+)/)) {
				var image_url = RegExp.$1;
				// show JPEG image in page
				var t = '<img id="upload-thumborg" src="' + image_url + '">';
				$(".avatar-viewbox").append(t);
				// reset camera for another shot
				webcam.reset();
				$("#webcam_movie").remove();
				$(".shot-tip").remove();
				$(".Js_avatar_shot").hide();
				$(".avatar-reshot").show();
				$(".Js_avatar_confirm").show();
				UP.initJcrop(msg)
			} else alert("PHP Error: " + msg);
		});
		webcam.set_hook('onLoad', function(msg) {
			$(".Js_avatar_shot").show();
		});

		webcam.set_hook('onError', function(msg) {
			$(".Js_avatar_shot").hide();
			$(".avatar-viewbox").append("<div class='shot-tip'>没有找到摄像头</div>");
		});
	});

	$(".Js_avatar_shot").on("click", function() {
		// take snapshot and upload to server
		$(".avatar-viewbox").append("<div class='shot-tip'>拍照中</div>");
		webcam.snap();
	});
})