$(function(){
	var c_awards = UM.getEditor('awards');
	var data = [];
	var addAwardsItemIndex = null;
	
	$('.Js_addAwardsItem').on('click',function(){
		$('.Js_adding').show();
		clearFormData();
	});
	
	$('.Js_addItemBtn').on('click',function(){
		var tx = $('#Js_tx').val();
		var subFlag = true;
		if(tx == "") {
			if($('#Js_tx').parent().find('.Validform_wrong').length <= 0){
				$('#Js_tx').parent().append('<span class="Validform_checktip Validform_wrong">请填写头衔！</span>')
			}
			subFlag = false;
		}else{
			$('#Js_tx').parent().find('.Validform_wrong').remove();	
		}
		var jj = $('#Js_jj').val();
		if(jj == "") {
			if($('#Js_jj').parent().find('.Validform_wrong').length <= 0){
				$('#Js_jj').parent().append('<span class="Validform_checktip Validform_wrong">请填写奖金！</span>')
			}
			subFlag = false;
		}else{
			$('#Js_jj').parent().find('.Validform_wrong').remove();	
		}
		
		if(!subFlag) return;
		
		var jpHtml = $.trim($('#awards2').val());
		var chtml = '<div class="added">'
				+'			<p class="line1 clearfix">'
				+'				<span class="title">头衔：<em>'+WG.fn.htmlspecialchars(tx)+'</em></span>'
				+'				<a class="cancle eBtn Js_awardsCancle" href="javascript:;" target_id="" target_a_flag="1"></a>'
				+'				<a class="edit eBtn Js_awardsEdit" href="javascript:;"></a>'
				+'				<span class="money">奖金：<em>'+WG.fn.htmlspecialchars(jj)+'</em>元</span>'
				+'			</p>'
				+'			<div class="line2 clearfix"><div class="awardsContent"><span class="jpName">奖品：</span>'+jpHtml+'</div></div>'
				+'		</div>';
		var o = {};
		o.tx = tx;
		o.jj = jj;
		o.jpHtml = jpHtml;
		if($(this).data('target_flag')){
			data.splice(addAwardsItemIndex,1,o);
			$('.Js_addItemDiv').children().eq(addAwardsItemIndex).replaceWith(chtml);
		}else{
			data.push(o);
			WG.TempData = data;
			$('.Js_addItemDiv').append(chtml);	
		}
		$('.Js_initAddDiv').hide();
		$('.Js_hasItemAddBtn').show();
		$('.Js_cancleBtn').click();
	}).data('target_flag',false);
	
	$('.Js_cancleBtn').on('click',function(){
		$('.Js_adding').hide();
		clearFormData();
	});
	
	$('.Js_hasItemAddBtn').on('click',function(){
		$('.Js_adding').show();
	});
	
	$('.Js_awardsEdit').live('click',function(){
		var index = $('.Js_awardsEdit').index($(this));
		var d = data[index];
		addAwardsItemIndex = index;
		$('.Js_adding').show();
		$('#Js_tx').val(d.tx);
		$('#Js_jj').val(d.jj);
		$('#awards2').val(d.jpHtml);
		$('.Js_addItemBtn').data('target_flag',true).children().text('修改');
	});
	
	$('.Js_awardsCancle').live('click',function(){
		var self = $(this);
		if(self.attr('target_a_flag')=='1'){
			self.attr('target_a_flag','0');
			window.setTimeout(function(){
				var index = $('.Js_awardsCancle').index(self);
				$('.Js_addItemDiv').children().eq(index).remove();
				data.splice(index,1);
				if(index == addAwardsItemIndex){
					$('.Js_cancleBtn').click();
				}else if(index < addAwardsItemIndex){
					addAwardsItemIndex--;	
				}
				if(data.length == 0){
					$('.Js_initAddDiv').show();
					$('.Js_hasItemAddBtn').hide();
				}
			},300);
		}
	});
	
	function clearFormData(){
		$('#Js_tx,#Js_jj,#awards2').val('');
		addAwardsItemIndex = null;
		$('.Js_addItemBtn').data('target_flag',false).children().text('添加');	
	}
})