/*!
 * 一级及多级菜单美化和原生控件
 * 
 * 作者：姚晴
 *
 * 时间: 2014-01-13
 
 * 所传参数格式为一个Json格式
 
 *	{
	 
	 data : '传入一个数组，数组的每个元素为json格式的数据，json中有三个属性值{name:'格式为字符串，代表下拉菜单的名字',attr:'格式为json，即选择下拉菜单的每个元素后回调传回的属性值。',nextLevel:'格式与data格式相同，这个参数代表当前选择的下拉菜单是否有联动的菜单'}',
	 format : '传入一个布尔值，true为美化版本的下拉菜单，如果不美化则可以传入false或者不填',
	 id	: '传入一个字符串，改字符串格式为 num|num|num 字符串中的num代表data中的array索引，从零开始。"|"则表示下级的意思',
	 level : '传入字符串型的数字，表示本来数组中的实际索引和所填入的num之间的差距数。',
	 callback : '传入一个function，为点击每个下拉菜单的元素后的回调函数。可以传一个ev参数，回调时可以得到改下拉元素的所有属性值。'
	 
	 }
	 
	 下拉菜单无论是单个下拉还是层级下拉，可以在第一个下拉菜单中加入标签属性data-area则可以设置数据，设置的格式为
	 如果为1个层级为0
	 如果为2个层级为0|0
	 以此类推
 
 */

(function($) {
	var methods = {
		init : function(){
			var settings = arguments[0];
			var d = settings.data;
			var self = this;
			if(settings.callback) settings.callback(privateMethods.getFirstData(settings));
			this.each(function(){
				$(this).change(function(){
					privateMethods.addEventMenuChange.call(this,settings,self);
				})
				d = privateMethods.isShowSel(d,$(this));
			})
			var listId = privateMethods.getId(this,settings);
			if(listId)
				privateMethods.haveIdInit(self,settings,privateMethods.transformation(listId,settings.level));
			return this;
		},
		
		initFormat : function(){
			var settings = arguments[0];
			var d = settings.data;
			var my = this;
			var self = my.find('select');
			var mPao = false;
			//if(settings.callback) settings.callback(privateMethods.getFirstData(settings));
			if(settings.callback&&settings.firstCallback) settings.callback(privateMethods.getFirstData(settings)); //修改菜单页面刷新初始化时不执行callback函数
			$(document).bind('click.dropDownMenu',function(){
				if(!mPao) my.find('ul').hide();
				mPao = false;
			})
			this.each(function(n){
				$(this).click(function(){
					my.find('ul').hide();
					$(this).find('ul').show();
					mPao = true;
				}).mouseenter(function(){
					$(this).find('s').addClass('focus');	
				}).mouseleave(function(){
					$(this).find('s').removeClass('focus');	
				})
				$(this).find('select').select(function(){
					privateMethods.addEventMenuChange.call(this,settings,self);
				})
				d = privateMethods.isShowSel(d,$(this).find('select'),settings.format);
			})
			var listId = privateMethods.getId(this,settings);
			if(listId)
				privateMethods.haveIdInit(self,settings,privateMethods.transformation(listId,settings.level));
			return this;
		},
		
		val:function(n){
			return methods.num;
		}
	}
	
	var privateMethods = {	//不美化的插件私有属性
		transformation : function(id,level){
			var ids = id.split('|');
			var levels = level ? level.split('|') : '';
			var arr = [];
			for(var i = 0; i<ids.length; i++){
				var s = levels[i] ? levels[i] : 0;
				var l = parseInt(ids[i],10) - s >= 0 ? parseInt(ids[i],10) - s : 0;
				arr.push(l);
			}
			return arr;
		},
	
		haveIdInit : function(self,settings,arr){
			var type = settings.format ? 'select' : 'change';
			self.each(function(n){
				if($(this).children().length && arr[n]){
					$(this).children().eq(arr[n])[0].selected = true;
					$(this).trigger(type);
				}
			})
		},
		
		addEventMenuChange : function(settings,self){
			var index = self.index($(this));
			var changeData = settings.data;
			var changeMenu = self.filter(':gt('+index+')');
			var notGtThis = self.not(':gt('+index+')');
			var selectBlur = [];//用户选择某一个菜单时小于等于当前的select中option所选择的index存储的数组
			notGtThis.each(function(){
				selectBlur.push($(this).children().index($(this).children(':selected')));
				if(settings.format) $(this).prev().prev().text($(this).children().filter(':selected').text());
			})
			if(index < self.length-1){
				for(var i=0; i<selectBlur.length; i++){
					changeData = changeData[selectBlur[i]]['nextLevel'];
				}
				changeMenu.each(function(){
					$(this).children().remove();
					if(settings.format) $(this).prev().children().remove();
					changeData = privateMethods.isShowSel(changeData,$(this),settings.format);
				})
			}	
			if(settings.callback) settings.callback($(this).children(':selected').data('ev')); //传给调用者获取的数据
		},
		
		addItem : function(data,obj,flag){//向select中添加option
			for(var i=0; i<data.length; i++){
				var item = data[i];
				var o = $('<option>'+item.name+'</option>');
				o.data('ev',item.attr);
				obj.append(o);
				
				if(flag){
					var formatSel = obj.prev();
					var li = $('<li>'+item.name+'</li>');
					formatSel.append(li);
					li.click(function(){
						if($(this).parent().hide().prev().text()===$(this).text()) return false;
						var index = formatSel.find('li').index($(this));
						$(this).parent().hide().prev().text($(this).text());
						obj.children().eq(index)[0].selected = true;
						obj.trigger('select');
						return false;
					});
					formatSel.prev().text(obj.children().filter(':selected').text());
				}
			}	
		},
		
		setData : function(data,obj){//改变各个select中所传入的数据
			var index = obj.children().index(obj.children(':selected'));
			if(data[index].nextLevel){
				data = data[index].nextLevel;
			}else{
				data = [];	
			}
			return data;
		},
		
		isShowSel : function(data,obj,flag){//判断当前是否有数据，没有数据则隐藏掉。
			var hideObj = flag ? obj.parent() : obj;
			if(data && data.length > 0){
				this.addItem(data,obj,flag);
				hideObj.show();
			}else{
				hideObj.hide();
				return data;	
			}
			
			return this.setData(data,obj);
		},
		
		getFirstData : function(settings){
			return !settings.data[0].attr ? '' : settings.data[0].attr;
		},
		
		getId : function(obj,settings){
			var id = obj.eq(0).attr("data-area") ? obj.eq(0).attr("data-area") : settings.id;
			return id;
		}
	}
	
	$.fn.dropDownMenu = function(options) {
		var method = arguments[0];
        if(methods[method]) {
            method = methods[method];
            arguments = Array.prototype.slice.call(arguments, 1);
        } else if( typeof(method) == 'object' || !method ) {
			if(method.format){
				method = methods.initFormat;
			}else{
            	method = methods.init;
			}
        } else {
            $.error( 'Method ' +  method + ' does not exist on' );
            return this;
        }
		
        return method.apply(this, arguments);
	}
})(jQuery);;