$(function() {
	var fileTemplate= ['<div class="Js_updateImg"><div class="updateImgLoadDiv">',
						'	<div class="uploadloadwrap">',
						'		<a href="javascript:;" class="close Js_upload_delete"></a>',
						'		<div class="updateImgLoad">',
						'			<span class="sc">上传中</span>',
						'			<span class="process-text">0%</span>',
						//'		<span class="scName"></span>',
						'			<div class="processDiv">',
						'				<div class="opacityBorder"></div>',
						'				<div class="process" style="width:0%">',
						'				</div>',
						'			</div>',
						'		</div>',
						'	</div>',
						'</div>',
						'<div class="height_10"></div></div>'].join("");

	$(".uploadWrap-single , .Js_file_uploadbox_single").html5Uploader({
		name: 'imagefile',
		postUrl: WG.baseUrl + uploadPath,
		onClientAbort: function(){
			alert("上传中断，请重新上传");
		},
		onClientLoadStart: function(e, file, xhr) {
			var obj = $(fileTemplate)
			/*if(upload_single.find('.updateSucImg').length>0){
				upload_single.find('.updateSucImg').eq(0).before(obj);
			}else{
				upload_single.append(obj);
			}*/
			upload_single.html(obj);
			uploadObjArr.push(obj);
		},
		onClientLoad: function(e, file, xhr) {
			for(var i=0; i<xhr_arr.length; i++){
				if(xhr===xhr_arr[i]){
					var xhrIndex = 	i;
				}	
			}
		},
		onServerLoadStart: function(e, file, xhr) {
			if(typeof FileReader == 'undefined'){
				var obj = $(fileTemplate)
				if(upload_single.find('.updateSucImg').length>0){
					upload_single.find('.updateSucImg').eq(0).before(obj);
				}else{
					upload_single.append(obj);
					upload_single.parents('.upload-files').find('.uploadWrap-single').hide();
				}
				uploadObjArr.push(obj);
			}
		},
		onServerProgress: function(e, file, xhr) {
			for(var i=0; i<xhr_arr.length; i++){
				if(xhr===xhr_arr[i]){
					var xhrIndex = 	i;
				}	
			}
			
			var percentComplete = e.loaded / e.total;
			var precentCompleteNum = parseInt((parseInt(e.loaded) / parseInt(e.total))*100);
			uploadObjArr[xhrIndex].find(".process").css("width",precentCompleteNum.toString()+"%");
			uploadObjArr[xhrIndex].find(".process-text").text(precentCompleteNum.toString()+"%");
			if(typeof FileReader == 'undefined'){
				uploadObjArr[xhrIndex].find('.scName').text(file.name+'(' + parseInt(e.total / 1024) + 'KB)');
			}
		},
		onServerLoad: function(e, file) {
			
		},
		onSuccess:function(e, file, xhr, data){
			var data = eval ('('+ data +')')
			var status = data.status;
			if(status== "ok"){
				switch (data.ext){
				   	case 'doc':
				     	break
				   	case 'pdf':
				     	break
				   	default:
						for(var i=0; i<xhr_arr.length; i++){
							if(xhr===xhr_arr[i]){
								var xhrIndex = 	i;
							}	
						}
						uploadObjArr[xhrIndex].remove();
						xhr_arr.splice(xhrIndex,1);
						uploadObjArr.splice(xhrIndex,1);
						if(typeof FileReader == 'undefined') fileName.splice(xhrIndex,1);
						var path = data.content;
						var tpic =['<div class="updateSucImg">',
								'	<div class="imgwrap">',
								'		<p><img src="'+WG.baseUrl+path+'"></p>',	
								'	</div>',
								'	<a href="javascript:;" title="删除" class="close Js_file_delete">删除</	a>',
								'	<input name="imagefile" class="upload_success_cover" type="hidden" value="'+path+'" />',
								'</div>'].join("");
						upload_single.html(tpic);
				}
			}else{
				if(data.error){
					if(data.error=="error_image_too_small") alert("文件太小，请重新上传！")
					else alert(data.error);//这里出要弹出意外信息，比如不支持的上传文件格式
					$(".Js_updateImg").remove();
				}else{
					alert("上传文件格式不正确，请从新上传")
				}
				upload_single.parents('.upload-files').find('.uploadWrap-single').show();
				//upload_single.parents('.upload-files').find('.upload-single').children().remove();
			}
		}
	});
	
	$('.Js_upload_delete').live('click',function(){
		for(var i=0; i<uploadObjArr.length; i++){
			if($(this).parents('.Js_updateImg').get(0)===uploadObjArr[i].get(0)){
				var objIndex = 	i;
			}	
		}
		
		if(xhr_arr[objIndex] != '' && xhr_arr[objIndex] != null ) {
			xhr_arr[objIndex].abort();
			xhr_arr.splice(objIndex,1);
			uploadObjArr.splice(objIndex,1);	
		}

		if(typeof FileReader == 'undefined') fileName.splice(objIndex,1);
		$(this).parent().next().remove();
		$(this).parents('.upload-files').find('.uploadWrap-single').show();
		$(this).parents(".updateImgLoadDiv").remove();
	})
	
	$(".Js_uploadbox").live('click',function(){
		if($(this).parents('.upload-files').hasClass('Js_cover')) {
			upload_single = $(this).parents('.Js_cover').find('.upload-single');
		}
		$(this).parent().find(".Js_file_uploadbox_single").click();
	});

    $(".tuploadbox").click(function(){
      $(".Js_file_uploadbox_single").click();
    });

	
	$(".upload-single .Js_file_delete").die().live("click",function(){
		var that = $(this);
		var file_id = null;//文件id
		var match_id = null//比赛id
		var fdata = {task_id:file_id,match_id:match_id}
		//ajax_(WG.baseUrl+'task/removeFinalWork',fdata,delete_file_c,that);  //发送Ajax数据
		$(".upload_success_cover")
		delete_file_c(that)//执行删除成功

	})
	//删除文件成功回调
	function delete_file_c(obj){
		obj.parents('.upload-files').find('.uploadWrap-single').show();
		$(".uploadWrap-single").parents(".updateSucImg").remove();
		var ie = (navigator.appVersion.indexOf("MSIE")!=-1);//IE
		if(ie){
			//$(".Js_file_uploadbox_single").parent()[0].reset();
		}else{
			$(".Js_file_uploadbox_single").val("");
		}
		obj.parent().remove();
		return false
	}
});