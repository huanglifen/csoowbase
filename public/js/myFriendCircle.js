/**
 * [Msg 我的朋友圈]
 * @type {Object}
 */
WG.MyCircle = {
	init: function(T) {
		this.createCircle();
		this.editCircle();
		this.delCircle();
		this.closeContent();
		this.showCard();
		this.joinCircle();
		this.cardCreate();
	},
	
	userItem : null,
	showCardAjax : null,
	showCardTimer : null,
	showCard : function(){
		var self = this;
		$('.Js_circle_card').mouseenter(function(ev){
			var data_id = $(this).attr('data-user-id');
			var x = ev.pageX + 5;
			var y = ev.pageY + 10;
			window.clearTimeout(self.showCardTimer);
			self.showCardTimer = null;
			self.userItem = $(this);
			$('.Js_addFriend_box').remove();
			self.showCardAjax = WG.fn.ajax('circle/member-circle-info',{user_id:data_id,type:2},function(e){
				if(e.status == 'ok'){
					var tpl = $(e.content);
					tpl.css({left:x,top:y});
					$('body').append(tpl);
				}
			});	
		}).mouseleave(function(){
			if(self.showCardAjax) self.showCardAjax.abort();
			if(!self.showCardTimer){
				self.showCardTimer = window.setTimeout(function(){
					$('.Js_addFriend_box').remove();	
					self.showCardTimer = null;
					self.userItem = null;
				},133);
			}
			
		})
		
		$('.Js_addFriend_box').live('mouseenter',function(){
			window.clearTimeout(self.showCardTimer);
			self.showCardTimer = null;
		}).live('mouseleave',function(){
			if(!self.showCardTimer){
				self.showCardTimer = window.setTimeout(function(){
					$('.Js_addFriend_box').remove();
					self.showCardTimer = null;
				},133);
			}
		})
	},
	
	cardCreate : function(){
		var _this = this;
		$('.Js_addBtn').live('click',function(){
			var self = $(this);
			var val = self.prev().val();
			WG.fn.ajax('circle/create-add-member',{name:val,user_id:$('.Js_addFriend_box').attr('data-member-id')},function(e){
				if(e.status == 'ok'){
					var tpl = '<li class="clearfix" data-circle-id="'+e.content.id+'">'
							+'		<label class="circle_name" data-type="py"><input type="checkbox" checked="checked">'+e.content.name+'</label>'
							+'	</li>'	
					
					$('#Js_circleList').append(tpl);
					self.prev().val('');
					_this.userItem.find('.Js_circle_number').text($('#Js_circleList').find(':checked').length);	
				}
			})	
		})	
	},
	
	joinCircle : function(){
		var _this = this;
		
		$('#Js_circleList').find('label').live('click',function(ev){
			var self = $(this);
			if(!self.find(':checked').length){
				WG.fn.ajax('circle/add-member',{circle_id:$(this).parent().attr('data-circle-id'),user_id:$('.Js_addFriend_box').attr('data-member-id')},function(e){
					if(e.status == 'ok'){
						if(e.content){
							self.find(':checkbox').attr('checked',true);
							_this.userItem.find('.Js_circle_number').text($('#Js_circleList').find(':checked').length);	
						}
					}
				})
			}else{
				WG.fn.ajax('circle/remove-member',{circle_id:$(this).parent().attr('data-circle-id'),user_id:$('.Js_addFriend_box').attr('data-member-id')},function(e){
					if(e.status == 'ok'){
						if(e.content){
							self.find(':checkbox').attr('checked',false);
							_this.userItem.find('.Js_circle_number').text($('#Js_circleList').find(':checked').length);	
						}
					}
				})
			}
			return false;
		})
		$('#Js_circleList').find(':checkbox').live('click',function(ev){
			var self = $(this);
			if(self.is(':checked')){
				WG.fn.ajax('circle/add-member',{circle_id:$(this).parents('label').parent().attr('data-circle-id'),user_id:$('.Js_addFriend_box').attr('data-member-id')},function(e){
					if(e.status == 'ok'){
						if(e.content){
							self.attr('checked',true);
							_this.userItem.find('.Js_circle_number').text($('#Js_circleList').find(':checked').length);	
						}
					}
				})
			}else{
				WG.fn.ajax('circle/remove-member',{circle_id:$(this).parents('label').parent().attr('data-circle-id'),user_id:$('.Js_addFriend_box').attr('data-member-id')},function(e){
					if(e.status == 'ok'){
						if(e.content){
							self.attr('checked',false);
							_this.userItem.find('.Js_circle_number').text($('#Js_circleList').find(':checked').length);	
						}
					}
				})	
			}
			ev.stopPropagation();
		})
	},
	
	closeContent: function(){
		$('.Js_close_content').live('click',function(){
			$.fancybox.close();	
		})
	},
	
	createCircle : function(){
		var tpl = $('.Js_create_content').remove();
		$('.Js_create_circle_a').on('click',fn);
		$(".Js_create_circle_btn").live('click',WG.MyCircle.createCircleCallback);
		function fn(){
			
			$.fancybox({
				fitToView	: false,
				autoSize	: true,
				padding     : 0,
				closeBtn    : false,
				content     : tpl.html()
			});
		}	
	},
	
	createCircleCallback : function() {
		var name = $(this).prev().val();

		var url = 'circle/create-circle';
		WG.fn.ajax(url, {"name": name}, function(ev) {
			if(ev.status == 'ok') {
				if(ev.content.id){
					window.location.href = baseUrl+'circle/index/1/'+ev.content.id;
				}else{
					$(".Js_show_create_circle_error span").text('圈子名不能重复');
					$(".Js_show_create_circle_error").show();
				}
			}else{
				$(".Js_show_create_circle_error").find('span').text(ev.error.name);
				$(".Js_show_create_circle_error").show();
			}
		})
		
	},
	
	editCircleCallback : function(){
		var val = $('.Js_edit_circle_name').val();
		var circle_id = $(this).attr('data-circle-id');
		var url = 'circle/edit-circle';
		WG.fn.ajax(url, {"name": val,"id":circle_id}, function(ev) {
			if(ev.status == 'ok') {
				if(ev.content.id){
					window.location.reload();
				}else{
					$(".Js_show_create_circle_error span").text('圈子名不能重复');
					$(".Js_show_create_circle_error").show();
				}
			}else{
				$(".Js_show_create_circle_error").find('span').text(ev.error.name);
				$(".Js_show_create_circle_error").show();
			}
		})
	},
	
	editCircle : function() {
		var tpl = $('.Js_edit_content').remove();
		
		$('.Js_edit_circle_a').on('click', fn);
		$(".Js_edit_circle_name_btn").live('click',WG.MyCircle.editCircleCallback);
		function fn() {
		   $.fancybox({
				fitToView	: false,
				autoSize	: true,
				padding     : 0,
				closeBtn    : false,
				content     : tpl.html(),
				afterShow  : function(){}
			});
		}
	},
	
	delCircle : function() {
		$('.Js_del_circle_a').on('click', fn);
		
		function fn() {
			var url = 'circle/delete-circle';
			var circle_id = $(this).attr("data-circle-id");
			WG.fn.ajax(url, {"id": circle_id}, function(ev) {
				if(ev.status == 'ok') {
					if(ev.content){
					window.location.href = baseUrl+'circle';
					}else{
						alert('没有权限删除该圈子!');
					}
				}else{
					alert('删除失败!');
				}
				});
		}
	}
};