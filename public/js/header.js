/**
 * [UserInfo 用户登录，用户信息]
 */
WG.UserInfo = {
	init: function(){
		var T = WG.UserInfo;
		$(".Js_user").on("mouseenter", T.showLoginInfo);
		$(".Js_user").on("mouseleave", T.hideLoginInfo)
	},
	showLoginInfo: function(){
		var $iobj = $("#Js_login_info");
		$iobj.fadeIn(100);
	},
	hideLoginInfo: function(){
		var $iobj = $("#Js_login_info");
		$iobj.fadeOut(100);
	}
};

/**
 * [Ccz 创创召搜索]
 */
WG.Ccz = {
	init: function(){
		var T = WG.Ccz;
		$(".Js_search").on("click", T.showCcz);
		$(".Js_cczprompt,.Js_friendlist,.Js_msglist").on("click", function(e){WG.stopBubble(e)});
		$(".Js_friend_btn,.Js_alerts_btn").on("click", T.showFriends);
		$(window).on("resize",T.windowResize)
		//propertychange IE8及之前有效
		//input IE9及之后有效，但IE9.IE10中删除输入框内容时不触发input事件
		var search_val = $('.Js_search').val();
		window.setInterval(function(){
			if($('.Js_search').val()!=search_val){
				T.showSearchList();
			}
			search_val = $('.Js_search').val();
		},100);
		
		$(".Js_searchResult").live('click',function(e){WG.stopBubble(e);})
		//$('.Js_search').on("keyup", T.showSearchList);
		//$(".Js_search").on("blur", T.hideCcz)
		
		if($('.Js_chatNum').length > 0 || $('.Js_notificationNum').length > 0 ){
			window.setInterval(function(){
				WG.fn.ajax('notification/notification',{},function(e){
					if(e.status == 'ok'){
						$('.Js_chatNum').text(e.content.chatNum);
						$('.Js_notificationNum').text(e.content.notificationNum);
						if(e.content.chatNum > 0){
							$('.Js_chatNum').show();	
						}else{
							$('.Js_chatNum').hide();	
						}
						
						if(e.content.notificationNum > 0){
							$('.Js_notificationNum').show();	
						}else{
							$('.Js_notificationNum').hide();	
						}
					}
				})	
			},5000)
		}
	},
	showCcz: function(e){
		//WG.stopBubble(e);
		var v = $.trim($(".Js_search").val()),
			T = WG.Ccz;
		if(v == ""){
			if($(".Js_cczprompt").is(":hidden")){
				$(".Js_cczprompt").fadeIn(150);
				$('.Js_cczprompt_li a').on('click', function(e){T.tellCcz(e,$(this))})
				$('.Js_cczonce').on('click', 'a.active', function(e) {T.callCcz(e)});
				$('.Js_tcs_list').on('click',function(){
					$('.Js_cczonce').attr('url',$(this).attr('data-href'));	
				})
			} 
		}else{
			if($(".Js_searchResult").is(":hidden")) $(".Js_searchResult").show();
		}

		if($(".Js_friendlist").is(":visible")) $(".Js_friendlist").hide();
		if($(".Js_msglist").is(":visible")) $(".Js_msglist").hide();
		$(".Js_friendalert a").removeClass('on');

		$(document).one("click", T.hideCcz);
		if(e) WG.stopBubble(e);
	},
	hideCcz: function(){
		var T = WG.Ccz;
		$('.Js_cczonce').off('click', 'a.active');//注销召一下点击
		$(".Js_cczprompt_li a").off("click");//注销创创召里面的块点击
		if($(".Js_cczprompt").is(":visible")) $(".Js_cczprompt").hide();//未输出字的创创召
		if($(".Js_searchResult").is(":visible")) $(".Js_searchResult").hide();//搜索列表
		if($(".Js_friendlist").is(":visible")) $(".Js_friendlist").hide();//关闭朋友列表
		if($(".Js_msglist").is(":visible")) $(".Js_msglist").hide();//关闭消息列表
		$(".Js_friendalert a").removeClass('on');//取消朋友和消息图标的当前状态
	},
	searchAjax : null,
	showSearchList: function(){
		var T = WG.Ccz,
			$that = $('.Js_search');
		var v = $.trim($that.val());
		if(WG.Ccz.searchAjax) WG.Ccz.searchAjax.abort();
		if(v !== ""){
			$(".Js_cczprompt").hide();
			WG.Ccz.searchAjax = WG.fn.ajax('search/list',{keyword:v},function(e){
				if(e.status == 'ok'){
					T.hideCcz();
					if(e.content.length > 0){
						$(".Js_searchResult_con").html(e.content);
					}else{
						$(".Js_searchResult_con").html('<div style="height:170px; line-height:170px; text-align:center">未搜索到内容。</div>');
					}
					$(".Js_searchResult").show();
					WG.Ccz.searchAjax = null;
				}
			});
		}else{
			$(".Js_searchResult").hide();
			T.showCcz();
		}
		$(document).one("click", T.hideCcz);
		//WG.stopBubble(e)
	},
	tellCcz: function(e,obj){
		var $this = obj;
		var index = $this.index();
		$('.Js_cczprompt_li a').not(':eq(' + index + ')').removeClass('active');
		$this.toggleClass('active');
		if ($('.Js_cczprompt_li a[class*=active]').length > 0) $('.Js_cczonce .cczonce').addClass('active');
		else $('.Js_cczonce .cczonce').removeClass('active');
		WG.stopBubble(e)
	},
	callCcz: function(e){
		if($('.Js_cczonce').find('.Js_use_ccz').length > 0){
			WG.fn.ajax('home/ccz',{},function(e){
				if(e.status=='ok'){
					window.location.href = $('.Js_cczonce').attr('url');	
				}
			});
		}else{
			window.location.href = $('.Js_cczonce').attr('url');
		}
		WG.stopBubble(e)
	},
	showFriends: function(e){
		var T = WG.Ccz;
		if($(".Js_cczprompt").is(":visible") || $(".Js_searchResult").is(":visible"))  T.hideCcz();
		
		$(".Js_friendalert a").not($(this)).removeClass("on");
		var index = $(this).index();
		var $obj,$obj2;
		if(index==0){
			$obj = $(".Js_friendlist");
			$obj2 = $(".Js_msglist")
		}else{
			$obj = $(".Js_msglist");
			$obj2 = $(".Js_friendlist")
		};
		
		if($obj.is(":hidden")){
			$(this).addClass('on');
			$obj.show();
			$obj2.hide();
		}else{
			$(this).removeClass('on');
			$obj.hide();
			$obj2.hide();
		}

		$(document).one("click", T.hideCcz);
		WG.stopBubble(e)
	},
	windowResize: function(){
		//创创召搜索框列表高度计算
		var _objH = $(".Js_searchResult_con").height(); 
		var _winH = $(window).height();
		if($(".Js_searchResult").length>0) {
			var _h = _winH-60;
			_h < 200 ? _h = 200 : _h;
			_winH > _objH+50 ? _h = _objH : _h
			$(".Js_searchResult").height(_h);
		}
	}
}