WG.InviteExpert = {
    init: function(){
        $(".Js_invite_input").on("keyup",this.showUserList);//输入关键词
        $(".Js_invite_list").on("click",".Js_invite_item",this.showExpList);//点击搜索到的专家
        $(".Js_exp_list").on("click",".Js_cancel_invite",this.cancelInvite);//取消邀请
        $(".Js_inivert_confirm").on("click",this.confirmList);//确认邀请
        //$(".Js_out_invite")on("clcik",function(){$(".Js_out_invite_form").submit()}
        //表单验证
        if($("#out_invite_form").length>0){
            $("#out_invite_form").Validform({
                tiptype:3,
                showAllError:true,
                ajaxPost:false
            });
        }
    },
    showUserList: function(){
        var $this = $(this),
	        _target_type = $(".Js_invite_search").data("target-type");
        var _mt = WG.fn.getMatchtype(_target_type);
        var url = _mt +'/search-internal-expert',
            val = $.trim($(".Js_invite_input").val());
            data = { keyword: val};
        WG.fn.ajax(url, data, function(data){
            if(data.status == "ok"){
                $(".Js_invite_list").parent().show();
                $(".Js_invite_list").html(data.content)
            }
        })
    },
    showExpList: function(){
    	var $this = $(this),
    		_target_type = $(".Js_invite_search").data("target-type");
        var _mt = WG.fn.getMatchtype(_target_type);
        var url = _mt +'/select-internal-expert',
            userid = $.trim($(".Js_invite_item").data("user-id"));
            data = {user_id: userid};
        WG.fn.ajax(url, data, function(data){
            if(data.status == "ok"){
                $(".Js_exp_list").append(data.content);
                $(".cwindex-title,.Js_inivert_confirm").show();
                $this.hide().parent().parent().hide();
                $this.hide().parent().children().remove();
                $(".Js_invite_input").val("");
            }
        })      
    },
    confirmList: function(){
    	var $this = $(this),
            _target_type = $(".Js_invite_search").data("target-type"),
            _target_sub_id = $(".Js_invite_search").data("target-sub-id"),
            _target_id = $(".Js_invite_search").data('target-id');
        var _mt = WG.fn.getMatchtype(_target_type);

        var userids = "";

        $.each($(".Js_exp_list li"), function(index, val) {
        	var $this = $(this);
        	var id = $this.data("user-id");
        	userids += id + ","; 
        });

        var url = _mt +'/invite-internal-expert',
            data = {target_type:_target_type, target_id: _target_id, target_sub_id:_target_sub_id, user_ids: userids}
        WG.fn.ajax(url, data, function(data){
            if(data.status == "ok"){
                //$(".Js_exp_list").append(data.content)
                window.location.reload();
            }
        })      
    },
    cancelInvite: function(){
    	$(this).parents("li").remove();
        if($(".Js_exp_list li").length == 0) 
        $(".Js_inivert_confirm").hide(); 
        $(".cwindex-title").hide()
    }

}