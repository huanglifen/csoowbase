/**
 * [个人信息修改]
 * @type {Object}
 */
WG.PersonInfo = {
	init: function(){
		if ($(".Js_birthday").length > 0) {
			$("input.Js_birthday").Zebra_DatePicker({
				format: "Y/m/d",
				offset: [-200, 251],
				show_clear_date: false,
				show_select_today: false,
				direction: false
			});
		};

		if ($(".Js_userarea").length > 0) {
			var _areaid = $("#area_selected_id").val();
			if(_areaid!=='0'){
				$('.Js_userarea').dropDownMenu({
					data: cityList,
					id: _areaid,
					format: true,
					level: '09',
					callback: function(ev) {
						$(".Js_area").val(ev.id)
					}
				});
			}else{
				$('.Js_userarea').dropDownMenu({
					data: cityList,
					format: true,
					callback: function(ev) {
						$(".Js_area").val(ev.id)
					}
				});
			}
		};

		//表单验证
		if($("#user-info-form").length>0){
			$("#user-info-form").Validform({
				tiptype:3,
				showAllError:true,
				ajaxPost:false
			});
		}

		//修改手机
		$(".Js_showmobile").on("click",this.showMobile);
		//修改邮箱
		$(".Js_showemail").on("click",this.showEmail);
		//保存个人信息
		$(".Js_save_userinfo").on("click",this.saveInfo);
        //保存个人信息成功弹窗的关闭
        $(".Js_UserInfoSuccess_close").on("click", this.closeUserInfoSuccessBox);
		//学校信息
		$(".Js_school_input").on("keyup",this.showSchoolList);
		//点击获取学校
		$(".Js_infolist").on("click","li", this.setCurrentInfo);
		//政企信息
		$(".Js_organization_input").on("keyup",this.showOrganizationList);
		//获取手机验证码
		$(".Js_getCode").on("click",this.getNumCode);
		//验证原手机号码
		$(".Js_oldnumber").on("focusout",this.checkNum);
		//更新手机，Email
		$(".Js_update_phone_email").on("click",this.updatePhoneEmail);
		//更新Email后 提示成功弹窗的关闭
		$(".Js_update_phone_email_success_close").on("click",this.updatePhoneEmailSuccess);
	},
	updatePhoneEmail: function(){
		var $this = $(this), url, data,
			T = WG.PersonInfo;
		var tp = $this.data("update-type");
		if(tp == "email"){
			url = "account/update-email";
			data = $("#changeemail-form").serialize();
		}else if(tp == "phone"){
			url = "account/update-mobile";
			data = $("#changephone-form").serialize()
		}
		$this.find("button").text("保存中...");
		$(".Js_update_email").off("click",T.updatePhoneEmail);
		WG.fn.ajax(url , data, function(data){
			if(data.status == "ok"){
                $.fancybox({
                    fitToView: false,
                    autoSize: true,
                    padding: 0,
                    closeBtn: false,
                    content: $(".Js_update_phone_email_success")
                });
                setTimeout(function(){
                    $.fancybox.close();
                    setTimeout(function(){
                        window.location.href = WG.baseUrl + "account/account-safe"
                    }, 460)
                }, 5000);
			}else if(data.status == "param_error"){
				var step3 = ['new_email','old_email','verify_code'];
				var step = "";
				step = WG.fn.checkError(data.error, step, step3);
				$(".Js_update_email").on("click",T.updatePhoneEmail);
				$this.find("button").text("保存");
			}
		})
	},
    updatePhoneEmailSuccess: function(){
        $.fancybox.close();
        setTimeout(function(){
            window.location.href = WG.baseUrl + "account/account-safe"
        }, 460)
    },
	checkNum: function(){
		var $this = $(this);
		var url,data,error;
		var type = $this.data("numtype");
		if(type=="phone"){
			url = "account/old-mobile";
			data = {old_mobile: $(this).val()};
		}else if(type=="email"){
			url = "account/old-email";
			data = {email: $(this).val()};
		}
		WG.fn.ajax(url,data,function(data){
			var $info = $this.find(".Validform_checktip");
			var $right_info = $this.parent().find(".Validform_right");
			var $error_info = $this.parent().find(".Validform_wrong");

			if(data.status == "ok"){
				$info.remove();
				if($right_info.length>0){
					$right_info.show()
				}else{
					$error_info.remove();
					$this.after($('<span class="Validform_checktip Validform_right"></span>'))
				}
			}else if( data.status == "param_error"){
				if(type=="phone"){
					error = data.error.old_mobile;
				}else if(type=="email"){
					error = data.error.email;
				}

				$info.remove();
				if($error_info.length>0){
					$error_info.show().text(error)
				}else{
					$right_info.remove();
					$this.after($('<span class="Validform_checktip Validform_wrong">'+error+'</span>'))
				}
			}
		})
	},
	getNumCode: function(){
		var T = WG.PersonInfo,
			$this = $(this),
			time = 60;
		var url,data;
		var ctype = $this.attr("data-codenumtype");
		if(ctype =="phone"){
			url = "account/send-mobile";
			data = {new_mobile:$("#Js_new_mobile").val()};
		}else if(ctype =="email"){
		 	url = "account/send-email";
		 	data = {new_email:$("#Js_user_account").val()}
		}
		$(".Js_getCode").off("click",T.getNumCode);
		$this.addClass("disable").text("验证码发送中…");
		WG.fn.ajax(url,data,function(data){
			if(data.status == "ok"){
				$this.addClass("disable").text("60秒后可重新发送");
				if($(".Js_new_mobile").length>0){
					$(".Js_new_mobile").parent().find(".Validform_wrong").remove()
				}
				var st = setInterval(function(){
					time--;
					$this.text(time+"秒后可重新发送");
					if(time == 0){
						window.clearInterval(st);
						$(".Js_getCode").on("click",T.getNumCode);
						$this.removeClass("disable").text("免费获取验证码");
					}
				},1000);
			}else if(data.status == "param_error"){
				var step3;
				if(ctype =="phone"){
					step3 = ['new_mobile'];
				}else if(ctype =="email"){
				 	step3 = ['new_email'];
				};
				var step = "";
				step = WG.fn.checkError(data.error, step, step3);
				$this.removeClass("disable").text("免费获取验证码");
				$(".Js_getCode").on("click",T.getNumCode);
			}
		})
	},
    closeUserInfoSuccessBox: function(){
        $.fancybox.close();
        setTimeout("window.location.reload();",460)
    },
	setCurrentInfo: function(){
		//WG.stopBubble();
		var $this = $(this);
		if($this.parents("#school_id").length>0){
			var _schoolid = $this.data("school-id"),
				_school = $this.text();
			$(".Js_school_input").val(_school).attr("data-school-id",_schoolid);
		}else{
			var _organizationid = $this.data("organization-id"),
				_organization = $this.text();
			$(".Js_organization_input").val(_organization).attr("data-organization-id",_organizationid);
		}
		$this.parents(".Js_infolist").hide();
	},
	showOrganizationList: function(){
		var $this = $(this),
			val = $.trim($this.val());
		var url="account/organization",
			data = {organization:$this.val()};
		if(val == ""){
			$this.attr("data-organization-id","");
			return;
		}
		WG.fn.ajax(url, data, function(data){
			if(data.status == "ok"){
				if(data.content == "") return;
				$this.next(".Js_infolist").show();
				$this.next(".Js_infolist").find('ul').html(data.content)
			}
		});
		$(document).on("click",function(){
			WG.PersonInfo.hideInfoList($this)
		})
	},
	showSchoolList: function(){
		var $this = $(this);
		var val = $.trim($this.val());
		var url="account/school",
			data = {school:$this.val()};
		if(val == ""){
			$this.attr("data-school-id","");
			return;
		}
		WG.fn.ajax(url, data, function(data){
			if(data.status == "ok"){
				if(data.content == "") return;
				$this.next(".Js_infolist").show();
				$this.next(".Js_infolist").find('ul').html(data.content)
			}
		});
		$(document).on("click",function(){
			WG.PersonInfo.hideInfoList($this)
		})
	},
	hideInfoList: function($this){
		if($(".Js_infolist:visible").length > 0) $(".Js_infolist").hide();
		if($this.attr("data-school-id")=="" || $this.attr("data-organization-id")=="") $this.val("")
	},
	saveInfo: function(){
		var url = "account/personal";
		data = $("#user-info-form").serialize();
		data += "&school_id=" + $(".Js_school_input").data("school-id") + "&organization_id=" + $(".Js_organization_input").data("organization-id");
        $(this.children).text("保存中...");
		WG.fn.ajax(url, data, function(data){
			if(data.status == "ok"){
                $.fancybox({
                    fitToView: false,
                    autoSize: true,
                    padding: 0,
                    closeBtn: false,
                    content: $(".Js_save_userinfo_success")
                });
                $(".Js_save_userinfo").children().text("保存");
                setTimeout(function(){
                    $.fancybox.close();
                    setTimeout("window.location.reload()", 440)
                }, 5000)
			}else if(data.status == "param_error"){
				var step3 = ['introduction','tags'];
				var step = "";
				step = WG.fn.checkError(data.error, step, step3);
                $(".Js_save_userinfo").children().text("保存");
			}
		})
	},
	//显示手机号码
	showMobile: function(){
		var $this = $(this);
		$this.parent().hide();
		$(".Js_mobilenumber_input").show();
		$(".Js_canceledit").on("click",function(){
			$(this).parent().next().show();
			$(this).parent().hide();
		})
	},
	//显示邮箱
	showEmail: function(){
		var $this = $(this);
		$this.parent().hide();
		$(".Js_email_input").show();
		$(".Js_canceledit").on("click",function(){
			$(this).parent().next().show();
			$(this).parent().hide();
		})
	}
};

/**
 * [修改密码]
 * @type {Object}
 */

WG.ChangePassword = {
	init: function(){
		if ($("#changepassword-form").length > 0) {
			//表单验证
			var s = $("#changepassword-form").Validform({
				tiptype: 3,
				showAllError: true,
				ajaxPost: false,
				usePlugin: {
					passwordstrength: {
						minLen: 6,
						maxLen: 22
					}
				},
				beforeCheck: function(){
					//alert(1)
				},
				beforeSubmit: function(){
				}
			})
		}
		$(".Js_save_password").on("click",function(){
			var pl = s.check();
			if(pl == true){
				$(".Js_save_password").off("click").on("click",WG.ChangePassword.saveChangePassword);
				$(".Js_save_password").click();
				return
			}
		})
		//$(".Js_save_password").on("click",this.saveChangePassword)
	},
	saveChangePassword: function(){
		var $this = $(this), 
			T = WG.ChangePassword,
			url = "account/password",
			data = $("#changepassword-form").serialize();
		$this.find("span").text("保存中...");
		$("Js_save_pssword").off("click", T.saveChangePassword);

		WG.fn.ajax(url, data, function(data2){
			if(data2.status == "ok"){
				//console.log(data2);
				$("Js_save_pssword").on("click", T.saveChangePassword);
				$this.find("span").text("保存");
				$("#changepassword-form")[0].reset();
                $.fancybox({
                    fitToView: false,
                    autoSize: true,
                    padding: 0,
                    closeBtn: false,
                    content: $(".Js_save_pssword_success")
                });
                setTimeout(function(){
                    $.fancybox.close();
                    setTimeout(function(){
                        window.location.href = WG.baseUrl + "account/account-safe"
                    }, 460)
                }, 5000);
			}else if(data2.status == "param_error"){
				if(data2.error && data2.error.current_password){
					$("#current_password .Validform_checktip").show().text(data2.error.current_password)
				}
				$this.find("span").text("保存");
				$("Js_save_pssword").on("click", T.saveChangePassword);
			}
		})
	}
};

/**
 * [个人页面]
 * @type {Object}
 */
WG.Person = {
	init: function() {
		this.funCall();
	},
	funCall: function() {
		var T = WG.Person;
		if ($(".startdate").length > 0) {
			$('input.startdate').Zebra_DatePicker({
				format: "Y/m/d",
				offset: [-169,251],
				pair: $('input.enddate'),
				show_clear_date: false,
				show_select_today: false,
				direction: false,
				onSelect: function(){
					T.ajaxFn("date-select")
				}
			});
		}

		if ($(".enddate").length > 0) {
			$('input.enddate').Zebra_DatePicker({
				format: "Y/m/d",
				offset: [-169,251],
				parents: $('input.startdate'),
				show_clear_date: false,
				show_select_today: false,
				onSelect: function(){
					T.ajaxFn("date-select")
				}
			});
		}

		//选择筛选方式
		$(".Js_tabindex li").on("click",this.changeTab);
		//日期区域选择 最近如：1个月，3个月
		$(".Js_dateselect a").on("click",this.changeDate);
		//选择分页
		$(".Js_s_result").on("click",".Js_paging",this.changePage);

		$(".Js_submit").on("click",function(){
			$("#formname").submit()
		});
	},
	changeDate: function(){
		var T = WG.Person;
		$(this).parent().find("a").removeClass('current');
		$(this).addClass('current');
		T.ajaxFn();
	},
	changeTab: function(){
		var T = WG.Person;
		$(this).parent().find("li").removeClass('current');
		$(this).addClass('current');
		T.ajaxFn();
	},
	changePage: function(){
		var T = WG.Person;
		$(this).parent().find(".num").removeClass('current');
		$(this).addClass('current');
		T.ajaxFn("page");//page代表是切换分页，ajax会返回总分页数
	},
	ajaxFn: function(view){
		if(view == "date-select"){
			$(".Js_dateselect a.current").removeClass('current');
		}
		var $this = $(this),
			T = WG.Person;

		var _way = $(".Js_tabindex li.current").data("t-index");
		var page;
		view == "page" ? _page = $(".Js_page .current").attr("data_id") : _page="1";
		_startTime = $(".startdate").val();
		_endTime = $(".enddate").val();
		_type_time = $(".Js_dateselect a.current").data("datetype");
		if(!_type_time) _type_time = "";


		var url, 
        data = { startTime:_startTime, endTime:_endTime, type_time: _type_time, way:_way, page:_page};
		url = "account/"+ account_type;
		WG.fn.ajax(url,data,T.showList);
	},
	showList : function(data){
		if(data.status == "ok"){
			var sd = data.startTime,
				ed = data.endTime;//返回的开始和结束日期
			$(".startdate").val(sd);
			$(".enddate").val(ed);
			var d1 = $(".startdate").data('Zebra_DatePicker');
			d1.update({
				direction: [false, ed]
			});
			var d2 = $(".enddate").data('Zebra_DatePicker');
			d2.update({
				direction: [sd, false]
			});
			$(".Js_s_result").html(data.content)
		}

/*		var html = [
				'<li class="c-title">',
				'	<div class="c c1">时间</div>',
				'	<div class="c c2">内容</div>',
				'	<div class="c c3">指数变化(点)</div>',
				'	<div class="c c4">创意指数(点)</div>',
				'</li>',
				'<li class="c-content">',
				'	<div class="c c1">2014-01-22 06:15:14</div>',
				'	<div class="c c2">发表专家点评</div>',
				'	<div class="c c3">+20</div>',
				'	<div class="c c4">155</div>',
				'</li>',
				'<li class="c-content">',
				'	<div class="c c1">2014-01-22 06:15:14</div>',
				'	<div class="c c2">发表专家点评</div>',
				'	<div class="c c3">+20</div>',
				'	<div class="c c4">155</div>',
				'</li>',
				'<li class="c-content">',
				'	<div class="c c1">2014-01-22 06:15:14</div>',
				'	<div class="c c2">发表专家点评</div>',
				'	<div class="c c3">+6</div>',
				'	<div class="c c4">155</div>',
				'</li>',
				'<li class="c-content">',
				'	<div class="c c1">2014-01-22 06:15:14</div>',
				'	<div class="c c2">发表专家点评</div>',
				'	<div class="c c3">+6</div>',
				'	<div class="c c4">155</div>',
				'</li>',
				'<li class="c-content">',
				'	<div class="c c1">2014-01-22 06:15:14</div>',
				'	<div class="c c2">发表专家点评</div>',
				'	<div class="c c3">+6</div>',
				'	<div class="c c4">155</div>',
				'</li>'].join("");
		}
*/
	}
};