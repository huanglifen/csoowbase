/*!
 * jQuery HTML5 Uploader 1.0b
 * http://www.igloolab.com/jquery-html5-uploader
 */
var fileName = [];
var xhr_arr = [];
var uploadObjArr = [];
(function($) {
	$.fn.html5Uploader = function(options) {
		var crlf = '\r\n';
		var boundary = "***";
		var dashes = "--";

		var settings = {
			"name": "uploadedFile",
			"postUrl": "Upload.aspx",
			"filetype": "gif,png,jpg,jpeg",
			"onClientAbort": null,
			"onClientError": null,
			"onClientLoad": null,
			"onClientLoadEnd": null,
			"onClientLoadStart": null,
			"onClientProgress": null,
			"onServerAbort": null,
			"onServerError": null,
			"onServerLoad": null,
			"onServerLoadStart": null,
			"onServerProgress": null,
			"onServerReadyStateChange": null
		};

		if (options) {
			$.extend(settings, options);
		}
		return this.each(function(options) {
			var $this = $(this);
			if ($this.is("[type='file']")) {
				$this.bind("change", function(e) {
					var o = $(this).clone(true);
					$(this).after(o);
					$(this).remove();
					var files = this.files;

					var fileArr = [];//可以上传的文件队列
					var missArr = [];//当前不支持上传的文件队列

					for (var i = 0; i < files.length; i++) {
						var fname = files[i].name;
						var pos = fname.lastIndexOf(".");
						//截取点之后的字符串
						var extension = fname.substring(pos+1);
						if(settings.filetype.indexOf(extension)<0){
							missArr.push(files[i].name);
						}else{
							fileArr.push(files[i])
						}

						if(i == files.length - 1 && missArr.length > 0){
							var t="";
							for(var x=0; x<missArr.length; x++){
								t +=  "文件："+missArr[x]+"  格式不支持\n"
							}
							var missfiles = '以下文件上传不成功：\n\n'+ t;
							alert(missfiles);
						}

						if(i == files.length - 1 && fileArr.length > 0){
							for(var t = 0; t < fileArr.length; t++){
								if ($this.hasClass('Js_upload_userPhone')) {
									fileHandler(fileArr[t], 0);
								} else {
									fileHandler(fileArr[t], 1);
								}
								fileName.push(fileArr[t]);
							}
						}
					}
				});
			} else {
				$this.bind("dragenter dragover", function(e) {
					return false;
				}).bind("drop", function(e) {
					var upload_drop = $(e.currentTarget);
					if (upload_drop.parent().find('.upload-more').length > 0) {
						upload = upload_drop.parent().find('.upload-more');
					} else if (upload_drop.parent().find('.upload-single').length > 0) {
						upload_single = upload_drop.parent().find('.upload-single');
					}
					var files = e.originalEvent.dataTransfer.files;
					if (files.length > 1 && !$(this).find('input').attr('ismul')) {
						alert('只能上传一张图片');
						return false;
					}
					for (var i = 0; i < files.length; i++) {
						if (typeof FileReader == 'undefined') {
							if (files[i].type.indexOf('image') < 0) {
								alert(files[i].fileName + '：上传格式不正确');
								return;
							}
						}
						fileHandler(files[i], 1);
						fileName.push(files[i]);
					}
					return false;
				});
			}
		});

		function fileHandler(file, n) {
			var xmlHttpRequest = new XMLHttpRequest();
			if (n == 1) {
				xhr_arr.push(xmlHttpRequest);
			}
			if (typeof FileReader != 'undefined') {
				var fileReader = new FileReader();
				fileReader.onabort = function(e) {
					if (settings.onClientAbort) {
						settings.onClientAbort(e, file);
					}
				};
				fileReader.onerror = function(e) {
					if (settings.onClientError) {
						settings.onClientError(e, file);
					}
				};
				fileReader.onload = function(e) {
					if (settings.onClientLoad) {
						settings.onClientLoad(e, file, xmlHttpRequest);
					}
				};
				fileReader.onloadend = function(e) {
					if (settings.onClientLoadEnd) {
						settings.onClientLoadEnd(e, file);
					}
				};
				fileReader.onloadstart = function(e) {
					if (settings.onClientLoadStart) {
						settings.onClientLoadStart(e, file, xmlHttpRequest);
					}
				};
				fileReader.onprogress = function(e) {
					if (settings.onClientProgress) {
						settings.onClientProgress(e, file);
					}
				};
				fileReader.readAsDataURL(file);
			}
			xmlHttpRequest.upload.onabort = function(e) {
				if (settings.onServerAbort) {
					settings.onServerAbort(e, file);
				}
			};
			xmlHttpRequest.upload.onerror = function(e) {
				if (settings.onServerError) {
					settings.onServerError(e, file);
				}
			};
			xmlHttpRequest.upload.onload = function(e) {
				if (settings.onServerLoad) {
					settings.onServerLoad(e, file);
				}
			};
			xmlHttpRequest.upload.onloadstart = function(e, file) {
				if (settings.onServerLoadStart) {
					settings.onServerLoadStart(e, file, xmlHttpRequest);
				}
			};
			xmlHttpRequest.upload.onprogress = function(e) {
				if (settings.onServerProgress) {
					settings.onServerProgress(e, file, xmlHttpRequest);
				}
			};
			xmlHttpRequest.onreadystatechange = function(e) {
				if (settings.onServerReadyStateChange) {
					settings.onServerReadyStateChange(e, file, newname, xmlHttpRequest.readyState);
				}
				if (settings.onSuccess && xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200) {
					settings.onSuccess(e, file, xmlHttpRequest, xmlHttpRequest.responseText);
				}
			};
			xmlHttpRequest.open("POST", settings.postUrl, true);
			if (file.getAsBinary) {
				var data = dashes + boundary + crlf + "Content-Disposition: form-data;" + "name=\"" + settings.name + "\";" + "filename=\"" + unescape(encodeURIComponent(file.name)) + "\"" + crlf + "Content-Type: application/octet-stream" + crlf + crlf + file.getAsBinary() + crlf + dashes + boundary + dashes;
				xmlHttpRequest.setRequestHeader("Content-Type", "multipart/form-data;boundary=" + boundary);
				xmlHttpRequest.sendAsBinary(data);
			} else if (window.FormData) {
				var formData = new FormData();
				formData.append(settings.name, file);
				xmlHttpRequest.send(formData);
			}
		}
	};
})(jQuery);