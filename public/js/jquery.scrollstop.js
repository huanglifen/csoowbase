/**
 * [ScrollStop 元素滚动到顶部悬停]
 * 使用方法: $(".demo").scrollstop()
 * auther:liyonggang
 */
(function($) {
	$.fn.scrollstop = function() {
		return this.each(function() {
			$.fn.scrollstop.init($(this));
		});
	};

	$.fn.scrollstop.init = function(obj) {
		var $this = obj;
		var offsettop = $this.offset().top;
		

		$(window).on("scroll",function(event) {
			var _d = offsettop - $(this).scrollTop();
			var _h = $this.height();
			if (offsettop <= $(this).scrollTop()) {
				$this.height(_h)
				$this.find(".list_tag_wrap").addClass('static');
			} else {
				event.stopPropagation();
				$this.find(".list_tag_wrap").removeClass('static');
			}
			//console.log(_d + '*' + offsettop + '*' + $(document).scrollTop())
		});
	};
})(jQuery);