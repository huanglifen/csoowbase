/**
 * [Guess 竞猜]
 * @type {Object}
 */
WG.Guess = {
	init: function() {
		$(".Js_coininput").on("keyup", this.showGuessCoin); //竞猜输入创创币
		$(".Js_guess_sub").on("click", this.sendGuess); //竞猜
	},
	sendGuess: function() {
		var $this = $(this),
			_target_type = $(".Js_guess_form").data("target-type"),
			_target_id = $(".Js_guess_form").data('target-id');
		var _mt = WG.fn.getMatchtype(_target_type);
		var url = _mt + '/sure-guess',
			data = $(".Js_guess_form").serialize();
		data += "&target_type=" + _target_type + "&target_id=" + _target_id;
		if ($(".Js_coininput").val() == "") {
			$(".guessinfo").remove();
			$this.after(' <span class="guessinfo" style="color: #D15552; margin:3px 0 0 15px; display:inline-block">请投入创创币</span>');
			return
		};
		$(".Js_guess_sub").off("click", WG.Guess.sendGuess); //竞猜
		WG.fn.ajax(url, data, function(data) {
			if (data.status == "ok") {
				$(".guessinfo").remove();
				$this.after(' <span class="guessinfo" style="color:#7ba567; margin:3px 0 0 15px; display:inline-block">竞猜成功</span>');
				setTimeout(function(){
					$(".guessinfo").hide();
					$(".Js_coininput").val("");
					$(".Js_ccb_coin").text("")
				}, 2000)
			} else if (data.status == "param_error") {
				$(".guessinfo").remove();
				$this.after(' <span class="guessinfo" style="color: #D15552; margin:3px 0 0 15px; display:inline-block">' + data.error.amount + '</span>');
				$(".Js_guess_sub").on("click", WG.Guess.sendGuess); //竞猜
			}
		})
	},
	showGuessCoin: function() {
		var $this = $(this),
			_target_type = $(".Js_guess_form").data("target-type"),
			_target_id = $(".Js_guess_form").data('target-id');
		var _mt = WG.fn.getMatchtype(_target_type);
		var url = _mt + '/expect-award',
			data = $(".Js_guess_form").serialize();
		data += "&target_type=" + _target_type + "&target_id=" + _target_id;

		WG.fn.ajax(url, data, function(data) {
			if (data.status == "ok") {
				$(".Js_ccb_coin").text(data.content)
			}
		})
	}
};

/**
 * [LikeReport 喜欢和举报]
 * @type {Object}
 */
WG.LikeReport = {
	init: function() {
		//喜欢
		$(".Js_likethis").on("click", this.likeThis);
		//举报
		$(".Js_reportthis").on("click", this.reportThis);
		//发送举报
		$(".Js_send_report").on("click", this.sendReport);
		//取消举报
		$(".Js_cancel_report").on("click", this.cancelReport)
	},
	cancelReport: function() {
		$.fancybox.close()
	},
	sendReport: function() {
		var $this = $(this);
		_target_type = $(this).data('target-type');
		_target_id = $(this).data('target-id');
		var _mt = WG.fn.getMatchtype(_target_type);
		var url = _mt + '/report',
			data = $("#report-form").serialize();
		data += "&target_type=" + _target_type + "&target_id=" + _target_id;
		WG.fn.ajax(url, data, function(data) {
			if (data.status == "ok") {
				$(".Js_reportthis").addClass('on');
				$(".Js_reportthis").off("click", WG.Discuss.reportThis);
                //alert("举报成功");
				$.fancybox.close();
                $.fancybox({
                    fitToView: false,
                    autoSize: true,
                    padding: 0,
                    closeBtn: false,
                    content: $(".Js_reportSuccess_box")
                });
			}
		})
	},
	reportThis: function() {
		$.fancybox({
			fitToView: false,
			autoSize: true,
			padding: 0,
			closeBtn: false,
			content: $(".Js_report_box")
		});
	},
	likeThis: function() {
		var $this = $(this);
		_target_type = $(this).parent().data('target-type'),
		_target_id = $(this).parent().data('target-id')
		var _mt = WG.fn.getMatchtype(_target_type);
		var url = _mt + '/like',
			data = {
				target_type: _target_type,
				target_id: _target_id
			};
		WG.fn.ajax(url, data, function(data) {
			if (data.status == "ok") {
				if (_mt == "area") {
					$this.attr("title", "已喜欢").find("span").text("已喜欢");
                    window.location.reload()
				} else {
					$this.addClass('on');
				}
				$(".Js_likethis").off("click", WG.Discuss.likeThis)
			}
		})
	}
};

/**
 * [Discuss 讨论区]
 * @type {Object}
 */
WG.Discuss = {
	init: function() {
		$(".Js_dis_btn").on("click", this.sendDis);
		$(".Js_dis_list").on("click", ".Js_area_page", this.showAreaPage);
		$(".Js_expdis_btn").on("click", {
			exp: true
		}, this.sendDis);
		$(".Js_showdis").on("click", function() {
			$(".Js_inputbox").focus();
		});
		$(".Js_inputbox_expertcomment").checkTextNumber({
			maxNum: 400, //可输入的最大字符
			numClassName: "ot" //显示字数的样式名称
		});
		$(".Js_inputbox_comment").checkTextNumber({
			maxNum: 1000, //可输入的最大字符
			numClassName: "ot" //显示字数的样式名称
		});
		$(".Js_inputbox_area_comment").checkTextNumber({
			maxNum: 140, //可输入的最大字符
			textClassName: "textnumbox area-t",
			numClassName: "ot" //显示字数的样式名称
		});
	},
	showAreaPage: function(){
		var _target_type = $(this).data("target-type"),
			_target_id = $(this).data("target-id"),
			_page = $(this).data("page");

		var _mt = WG.fn.getMatchtype(_target_type);
		var data = {page:_page, target_type: _target_type ,target_id : _target_id},
			url = _mt + "/comments";
		WG.fn.ajax(url, data, function(data){
			if(data.status == "ok"){
				$(".Js_dis_list").html(data.content)
			}
		})
	},
	sendDis: function(e) {
		var T = WG.Discuss;
		var _target_type = $(this).data("target-type"),
			_target_id = $(this).data("target-id"),
			_score = $(this).parent().find(".Js_common_rateit2").rateit('value'),
			_target_sub_id = $(this).data("target-sub-id"),
			_content = $.trim($(this).parents(".dis-sendbox").find(".Js_inputbox").val());
		var data, url;
		var _mt = WG.fn.getMatchtype(_target_type);
		var t_text = $(this).text();

		if (e.data && e.data.exp) {
			data = {
				target_type: _target_type,
				target_id: _target_id,
				target_sub_id: _target_sub_id,
				content: _content,
				score: _score
			};
			url = _mt + '/publish-expert-comment';
			sbj = {
				obj: $(this),
				t: "expert",
				txt: t_text
			}
		} else {
			data = {
				target_type: _target_type,
				target_id: _target_id,
				content: _content
			};
			url = _mt + '/publish-comment';
			sbj = {
				obj: $(this),
				txt: t_text
			}
		}

		if (e.data && e.data.exp && _score == "" || _content == "" || $(".Js_num_error").length > 0) {
			if (_score == "") $(this).parent().find(".Js_ct").html('<span style="color:#d15552">请选择创意价值！</span>')
			return;
		}
		$(this).text("发表中...");
		$(".Js_expdis_btn").off("click", WG.Discuss.sendDis);
		WG.fn.ajax(url, data, T.showDisList, null, null, sbj);
	},
	showDisList: function(data, sbj) {
		if (data.status == "ok") {
			if($('.Js_experts_no_data').length > 0){
				$('.Js_experts_no_data').remove();	
			}
			var Obj = sbj.obj;
			Obj.parents(".dis-sendbox").find(".Js_inputbox").val("")
			var _obj = Obj.parents(".discuss-wrap").find('.Js_dis_list');
			_obj.prepend(data.content);
			if ($(".Js_common_rateit2").length > 0) {
				$(".Js_common_rateit2").rateit('reset');
				$(".Js_cts").hide();
				$(".rateit").rateit();
			}
			$(".Js_expdis_btn").on("click", {
				exp: true
			}, WG.Discuss.sendDis);
			Obj.text(sbj.txt);
			$(".textnumbox").remove();
			if (sbj.t && sbj.t == "expert") {
				Obj.parents(".list-item").remove()
			}
		}
	}
};

/**
 * [Rated 评分]
 * @type {Object}
 */
WG.Rated = {
	init: function() {
		var T = WG.Rated;
		$(".Js_common_rateit,.Js_common_rateit2").on('over', function(event, value) {
			T.rated($(this), value)
		});
		$(".Js_common_rateit").on('rated', function(event, value) {
			//$('.common_text').text('You\'ve rated it: ' + value);
			T.rated($(this), value, 2)
		});
		$(".Js_common_rateit2").on('rated', function(event, value) {
			T.rated($(this), value, 1)
		});
	},
	rated: function(obj, value, n) {
		var t;
		switch (value) {
			case 1:
				t = "有一点";
				break;
			case 2:
				t = "一般";
				break;
			case 3:
				t = "良好";
				break;
			case 4:
				t = "优秀";
				break;
			case 5:
				t = "颠覆性的创新和巨大的价值";
				break;
			case null:
				t = ""
		}
		if (n == 1) {
			t !== null ? obj.parent().find('.Js_cts').show().text(t) : obj.parent().find('.Js_cts').hide().text(t);
		};
		if (n == 2) {
			t !== null ? obj.parent().find('.Js_cts').show().text(t) : obj.parent().find('.Js_cts').hide().text(t);
			obj.rateit('readonly', true);
			var _mt = WG.fn.getMatchtype(obj.data("target-type")),
				_target_type = obj.data("target-type"),
				_target_id = obj.data("target-id"),
				_score = obj.rateit('value');
			var url = _mt + '/score-item',
				data = {
					target_type: _target_type,
					target_id: _target_id,
					score: _score
				};

			WG.fn.ajax(url, data, function(data) {
				if (data.status == "ok") {
					if (data.content == "") {
						obj.rateit('readonly', false);
					}
				}
			})
		} else {
			obj.parent().find('.Js_ct').text(t);
		}
	}
};

/**
 * [发布更新]
 */
WG.ProjectUpdate = {
	init: function() {
		$(".Js_update_trend").on("click", this.projectUpdate); //更新动态
		$(document).on("click", ".Js_cancel_update", this.projectUpdateCancel) //取消发布动态;
		$(".Js_update_btn").on("click", this.saveProjectUpdate); //动态保存
		//$(".Js_dynamic_list").on("click", this.showDynamicDetail); //显示动态详情
		if($("#matchDetail").length>0) UM.getEditor('matchDetail');
	},
	projectUpdate: function() {
		$.fancybox({
			fitToView: false,
			autoSize: true,
			padding: 0,
			closeBtn: false,
			content: $(".Js_update_box")
		});
	},
	/*弹层显示动态*/
	/*
	showDynamicDetail: function() {
		var _type = $(".Js_contest_id").data("target-type");
		var _mt = WG.fn.getMatchtype(_type);
		var _update_id = $(this).parents("li").data("update-id"),
			url = _mt + "/update-by-id",
			data = {
				update_id: _update_id
			}

		WG.fn.ajax(url, data, function(data) {
			if (data.status == "ok") {
				var _html = [
					'<div class="message_box Js_update_box" style="width: 558px;">',
					'	<div class="box_tittle">',
					'		<span class="t fl">动态</span>',
					'		<a class="close fr Js_cancel_update" href="javascript:" title="关闭"></a>',
					'	</div>',
					'	<div class="box_content" style="font-size:14px;">',
					'		<div><b>' + data.content.title + '</b></div></br>',
					'		<div style="color:#666; line-height:1.6; word-break:break-all;word-wrap:break-word;">' + data.content.content + '</div>',
					'	</div>',
					'</div>'
				].join("");

				$.fancybox({
					fitToView: false,
					autoSize: true,
					padding: 0,
					closeBtn: false,
					content: _html
				});
			}
		})

	},
	*/
	saveProjectUpdate: function() {
		var T = WG.ProjectUpdate;
		var ue = UM.getEditor('matchDetail');
		var _title = $.trim($("#update_tittle_content").val()),
			_content = ue.getContent();
		var _id = $(".Js_contest_id").data("target-id"),
			_type = $(".Js_contest_id").data("target-type");
		var _mt = WG.fn.getMatchtype(_type);

		var url = _mt + "/update",
			data = {
				contest_id: _id,
				title: _title,
				content: _content
			}
		$(".Js_update_btn").off("click", T.saveProjectUpdate); 
		WG.fn.ajax(url, data, function(data) {
			if (data.status == "ok") {
				$("#update_tittle_content").val("");
				$("#update_content").val("");
				alert("发布更新成功！");
				window.location.reload();
			} else if (data.status == "param_error") {
				var step1 = ['title', 'content'];
				var step = "";
				step = WG.fn.checkError(data.error, step, step1);
				$(".Js_update_btn").on("click", T.saveProjectUpdate); 
			}
		})
	},
	projectUpdateCancel: function() {
		$.fancybox.close()
	}
};

/**
 * [ProjectShowDetail 投资项目显示详情]
 * @type {Object}
 */
WG.ProjectShowDetail = {
	init: function() {
		$(".Js_detail_btn").on("click", this.showDetail); //投资详情
	},
	showDetail: function() {
		var $this = $(this);
		var target = $this.data("opened");
		if (target && target == 1) {
			$this.parent().find(".detail_box").css({
				"height": "25px",
				"overflow": "hidden"
			})
			$this.data("opened", "").text("[详细]");
		} else {
			$this.parent().find(".detail_box").css({
				"height": "auto",
				"overflow": "auto"
			});
			$this.data("opened", 1).text("[收起]");
		}
	}
};

WG.Winner = {
	init: function(){
		$(".Js_select_winner").on("click", this.showWorks);
		$(".Js_confirm_winner").on("click", this.confirmWinner);
		$(".Js_close_btn").on("click",function(){$.fancybox.close()})
	},
	showWorks: function(){
		$.fancybox({
			fitToView: false,
			autoSize: true,
			padding: 0,
			closeBtn: false,
			content: $(".Js_message_box")
		});
	},
	confirmWinner: function(){
		var url = "hiring/choose-winner",
			data = {};
			var _prize_id = [];
		var sel = $(".Js_message_box input[type=checkbox]:checked");
		if( !sel.length > 0){
			return;
		}else{
			for(var i = 0; i < sel.length ; i++){
				_prize_id.push(sel[i].value)
			};
		};
		data = {participator_id:$("[name=participator_id]").val() , prize_id:_prize_id};
		WG.fn.ajax(url,data,function(data){
			if(data.status == "ok"){
				window.location.reload();
			}
		})
	}
}