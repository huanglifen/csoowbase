WG.TTab = {
	param : {area_id:'0',expert_organization_id:'0',expert_duty_id:'0',expert_industry_id:'0',expert_filed_id:'0',page:0},
	init: function () {
		var self = this;
		$(".Js_tinktankTab li").on("click",this.changeTabs);
		
		$('.Js_filter_btn').on('click',this.filter)
		.on('mouseenter',function(){if(!$(this).attr('data-flag')) $(this).css('color','#0046F7')})
		.on('mouseleave',function(){if(!$(this).attr('data-flag')) $(this).css('color','#555')});
		
		$('.Js_show_org').on('click',this.orgShow);
		
		self.changePage();
		
		$('.Js_mao_more').on('click',function(){
			$('body,html').animate({scrollTop:$('#Js_mao_more').offset().top},800);
		})
		
		$('.Js_dropPos').dropDownMenu({
			data : cityList,
			format : true,
			callback : function(ev){
				self.param.area_id = ev.id;
				WG.fn.ajax('thinktank/search-experts',self.param,function(e){
					if(e.status == 'ok'){
						$('.Js_filter_result').html(e.content);	
					}
				})
			}	
		});
	},
	
	changePage : function(){
		var self = this;
		
		$('.Js_paging').live('click',fn);
		
		function fn(){
			var param = getParam();
			param['page'] = $(this).attr('data_id');
			WG.fn.ajax('thinktank/search-experts',param,function(e){
				if(e.status == 'ok'){
					$('.Js_filter_result').html(e.content);	
				}
			})	
		}
		
		function getParam(){
			var param = {};
			for(var i in self.param){
				if(i!=='page'){
					param[i] = self.param[i];	
				}
			}
			
			return param;
		}
	},
	
	changeTabs: function(){
		var _index = $(this).index(),$this = $(this);
		$(".Js_tinktankTab li").not($this).removeClass("current");
		$this.addClass('current')
		if($(".Js_thinktankCon").length>0 && $(".Js_thinktankCon .tt-rec-con").length>_index){
			$(".Js_thinktankCon .tt-rec-con").not(_index).hide();
			$(".Js_thinktankCon .tt-rec-con").eq(_index).show();
		}
	},
	filter : function(){
		if($(this).attr('data-flag')) return;
		$(this).attr('data-flag',true).parent().siblings().find('a').removeAttr('data-flag');
		var num = $('.Js_filter_list').index($(this).parents('.Js_filter_list'));
		$(this).parents('p').find('.Js_filter_btn').css('color','#555').on('mouseleave',function(){$(this).css('color','#555')});
		$(this).css({'color':'#0046F7'}).unbind('mouseleave');
		switch(num){
			case 0 :
				WG.TTab.param.expert_organization_id = $(this).attr('data-expert-organization-id');
				WG.TTab.param.expert_duty_id = 0;
				break;
			case 1 :
				WG.TTab.param.expert_duty_id = $(this).attr('data-expert-duty-id');
				break;
			case 2 :
				WG.TTab.param.expert_filed_id = $(this).attr('data-expert-field-id');
				break;
			case 3 :
				WG.TTab.param.expert_industry_id = $(this).attr('data-expert-industry-id');
				break;
		}
		WG.fn.ajax('thinktank/search-experts',WG.TTab.param,function(e){
			if(e.status == 'ok'){
				$('.Js_filter_result').html(e.content);	
			}
		})
	},
	orgShow : function(){
		var index = $('.Js_show_org').index($(this))-1;
		$('.Js_org_item').hide();
		if(index >= 0){
			$('.Js_org_item').eq(index).show();
		}
		$('.Js_org_item').each(function(){
			$(this).find('.Js_filter_btn').css('color','#555').removeAttr('data-flag').eq(0).attr('data-flag',true).css('color','#0046F7');
		})
	}
}