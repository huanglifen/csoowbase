/**
 * [Msg 朋友圈消息]
 * @type {Object}
 */
WG.Msg = {
	init: function(T) {
		this.bindFun();
		$(".Js_dialoglist").scrollTop(99999);
	},
	bindFun: function() {
		//切换圈子
		$('.Js_industryMenu').dropDownMenu({
			data: industryJson,
			format: true,
			callback: this.showContacts
		});
		//点击联系人
		$(".Js_contact_list").on("click", '.Js_contact_user', this.showDialog);
		//消息发送按钮
		$(".Js_sendMsgBtn").on("click", this.sendMsg);
		//聊天-模拟定时接受消息
		$('.Js_chat_checkmore').live('click',this.getMoreMsg);
		window.setInterval(this.receiveMsg, 3000);
	},
	
	getMoreMsg : function(){
		WG.fn.ajax('chat/chat',{to_user_id:$('#Js_toUser').val(), top_chat_id:$('.Js_get_more_li:first').attr('chat-id')}, function(ev){
			if(ev.status=='ok'){
				var lastSendTime = $('.Js_get_more_li:first').attr('create-time');
				if(ev.content.create_time == 0 || lastSendTime != ev.content.create_time) {
					var lastSpendDay = '<li><div class="msg"><span class="time" style="margin-left:400px;">'+lastSendTime+'</span></div></li>';
					$('.Js_get_more_li:first').before(lastSpendDay);
					$('.Js_get_more_li:first').prev().before(ev.content.content);
				}else{
					$('.Js_get_more_li:first').before(ev.content.content);
				}				
				if(!ev.content.show){
					$(".Js_chat_checkmore").remove();
				}
			}
		});
	},
	
	showContacts: function(e) {
		var $this = $(this),
			T = WG.Msg;

		WG.fn.ajax('chat/circle-members', {circle_id:e.id}, function(ev){T.contactsCallback(ev)})
	},
	receiveMsg: function() {
		var $this = $(this),
			T = WG.Msg;
		var url = "chat/current-chat",
			data = {send_user_id:$('#Js_toUser').val(),last_chat_id:$('.Js_to_user_chat:last').parent().attr('chat-id')};
			
		WG.fn.ajax(url, data, function(ev){
			if(ev.status == 'ok'){
				var s = ev.content;
	
				var h1 = $(".Js_dialoglist")[0].scrollTop,
					h2 = $(".Js_dialoglist")[0].clientHeight,
					h3 = $(".Js_dialoglist")[0].scrollHeight;
				if (h1 + h2 == h3) {
					$(".Js_dialoglist ul").append(s);
					$(".Js_dialoglist")[0].scrollTop = $(".Js_dialoglist ul").height();
				} else {
					$(".Js_dialoglist ul").append(s);
				}
			}
		})
	},
	showDialog: function() {
		var $this = $(this),
			T = WG.Msg;
		$this.parents('.contact-list').find('li').not($this).removeClass('current');
		$this.addClass('current');
		var url = "chat/chat",
			data = {to_user_id:$this.attr('member-id'),top_chat_id:0};
		WG.fn.ajax(url,data,function(ev){
			T.dialogListCallback(ev);
		})
	},
	sendMsg: function() {
		var $this = $(this),
			$input = $this.parents('.circle-inputbox').find(".Js_textarea"),
			T = WG.Msg;
		if ($.trim($input.val()) == '') return;

		var url = "chat/send-message",
			data = {to_user_id:$('#Js_toUser').val(),content:$('.Js_textarea').val()};
		WG.fn.ajax(url, data, function(ev){
			if(ev.status=='ok'){
				var s = T.dialogSendCallback(ev);
				$(".Js_dialoglist ul").append(s);
				$(".Js_dialoglist")[0].scrollTop = $(".Js_dialoglist ul").height();
				$input.val("");
			}
		})
	},
	dialogListCallback: function(e) {
		if(e.status == 'ok'){
			$('.Js_contact_chat').html(e.content.content);	
			if(e.content.show){
				if($('.Js_chat_checkmore').length <= 0){
					$('.Js_contact_chat').before('<div class="chat-more-container"><a class="chat-more Js_chat_checkmore" href="javascript:" title="点击查看更多" style="display: block">查看更多聊天记录</a></div>');
				}
			}else{
				if($('.Js_chat_checkmore').length > 0){
					$(".Js_chat_checkmore").remove();
				}
			}
		}else{
			$('.Js_contact_chat').html('');	
		}
		$(".Js_dialoglist").scrollTop(99999);
	},
	dialogSendCallback: function(e) {
		var spendTimeLi = "";
		var lastSpendTime = $(".Js_get_more_li:last").attr('create-time');
		if(lastSpendTime != e.content.create_time) {
			spendTimeLi = '<li><div class="msg"><span class="time" style="margin-left:400px;">'+e.content.create_time+'</span></div></li>';
		}
		var html = [
		    spendTimeLi,
			'<li class="Js_get_more_li" chat-id='+e.content.chatId+' create-time='+e.content.create_time+'>',
			'	<div class="msg left-msg fl">',
			'		<span class="name">'+globalUserInfo.name+'</span>',
			'       <span class="time">'+e.content.send_time+'</span>',
			'		<p class="con">'+WG.fn.htmlspecialchars(e.content.content)+'</p>',
			'		<i class="arr"></i>',
			'	</div>',
			'</li>'
		].join("");
		return html;
	},
	contactsCallback: function(e){
		if(e.status == 'ok'){
			$('.Js_contact_list').html(e.content.list);
			if(e.content.show){
				if($('.Js_chat_checkmore').length <= 0){
					$('.Js_contact_chat').before(' <div class="chat-more-container"><a class="chat-more Js_chat_checkmore" href="javascript:" title="点击查看更多" style="display: block">查看更多聊天记录</a></div>');
				}
			}else{
				if($('.Js_chat_checkmore').length > 0){
					$(".Js_chat_checkmore").remove();
				}
			}
			$('.Js_contact_chat').html(e.content.content);
			$(".Js_dialoglist").scrollTop(99999);
		}
	}
};