/**
 * [百科]
 */
WG.Wiki = {
	init: function() {
		$(".Js_industry_list").on("click","li",this.showIndustryList);
	},
	showIndustryList: function(){
		var T = WG.Wiki,$this=$(this);
		var data = {id: $this.data("id")};
		WG.fn.ajax('encyclopedia/industry',data,function(data){
			if(data.status == "ok"){
				$(".Js_sub_industry_list").html(data.content)
			}
		})
	}
};
