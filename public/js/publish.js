/**
 * 发布比赛页面，此js是比赛通用，修改请注意是否影响其他比赛调用
 */
$(function() {
	//地区下拉
	if($('.Js_dropDownMenu').length > 0){
		$('.Js_dropDownMenu').dropDownMenu({
			data : cityList,
			format : true,
			callback : function(ev){
				//console.log(ev)
			}
		});
	}

	//智库行业下拉
	if($('.Js_industryMenu').length>0){
		$('.Js_industryMenu').dropDownMenu({
			data : industryJson,
			format : true,
			callback : function(ev){
				//console.log(ev)
			}
		});
	}

	//智库申请组织和职务
	if($('.Js_orgAndDutyMenu').length>0){
		$('.Js_orgAndDutyMenu').dropDownMenu({
			data : orgAndDutyJson,
			format : true,
			callback : function(ev){
				//console.log(ev)
				$(".Js_organization_duty").val(ev.id)
			}
		});
	}

	//比赛发布的下一步按钮
	$(".Js_goto_step1").on("click",function(){
		$(".Js_tab li").eq(0).click();
	});
	$(".Js_goto_step2").on("click",function(){
		$(".Js_tab li").eq(1).click();
		//return false;
	});

	$(".Js_goto_step3").on("click",function(){
		if($(".Js_thinktank_apply").length>0){
			$(".Js_tab li").eq(0).addClass('Js_tab_list');
			$(".Js_tab li").eq(1).addClass('Js_tab_list');
		}
		$(".Js_tab li").eq(2).click();
	});

	$(".Js_goto_step4").on("click",function(){
		$(".Js_tab li").eq(3).click();
	})

	//radio组
	if($(".Js_radio1").length>0) $(".Js_radio1").radioItem();
	//明星比赛radio组
	if($(".Js_findHiring").length>0) $(".Js_findHiring").radioItem();
	if($(".Js_matchType").length>0) $(".Js_matchType").radioItem();

	//投资项目我要参赛
	if($(".Js_investment").length>0) $(".Js_investment").radioItem();
	if($(".Js_finishedtime").length>0) $(".Js_finishedtime").radioItem();

	//编辑器
	if($("#matchDetail").length>0) UM.getEditor('matchDetail');
	if($("#matchDetail2").length>0) UM.getEditor('matchDetail2');
});

/**
 * [PublishProject 发布比赛]
 * @type {Object}
 */
WG.PublishProject = {
	init: function(){
		//日期控件
		if($('input.enddate').length>0){
			$('input.enddate').Zebra_DatePicker({
				format: "Y/m/d",
				offset: [-260,251],
				direction: true,
				show_clear_date: false,
				show_select_today: false
			});
		}

		$(".Js_project_submit").on("click",this.saveProjectForm);
		$(".Js_task_submit").on("click",this.saveTaskForm);
		$(".Js_hire_submit").on("click",this.saveHireForm);
		$(".Js_exchange_submit").on("click",this.saveExchangeForm);
		$(".Js_encyclopedia_submit").on("click",this.saveEncyclopediaForm);
		//任务提交作品
		$(".Js_task_apply_submit").on("click",this.saveTaskApplyForm);
		//明星我要参赛
		$(".Js_hire_apply_submit").on("click",this.saveHireApplyForm);
		//我要投资
		$(".Js_project_join_submit").on("click",this.saveProjectJoinForm);
		//申请专家
		$(".Js_thinktank_submit").on("click",this.saveThinkTankForm);
		//认证
		$(".Js_authenticate").on("click",this.authenticateIdentity);
		//投资金额输入
		$(".Js_amount_input").on("keyup",function(){WG.PublishProject.showAmount(1)});
		//输入比例
		$(".Js_proportion_input1").on('keyup',function(){WG.PublishProject.showAmount(2)});
		//输入比例
		$(".Js_proportion_input2").on('keyup',function(){WG.PublishProject.showAmount(3)});
		//检测地址栏变化
		$(window).on("hashchange", function() {	WG.Tabs.init()});
		$(".Js_NUMBER").numeral({'scale': 10});
		//切换时填充协议内容
		$(".Js_tab li").on("click", this.checkMatchType);
	},
	checkMatchType: function(){
		var _target_type = $("#Js_target_type").val(),
			_mt = WG.fn.getMatchtype(_target_type);

		if(_target_type == "1"){
			var amount_num = $(".Js_amount_input").val(),
				type = "",
				day = "",
				des = "";

			var type_num = $(".Js_investment input[type='radio']:checked").val();
			if(type_num == 2 && $(".Js_proportion_input1").val() !== ""){
				type = "先付定金再付尾款（先支付定金："+$(".Js_proportion_input1").val()+"%）";
			}else{
				type = "一次性付清"
			};
			var $dd = $("input[name='require_complete_time']");
			if( !$dd.length > 0 && $dd !== ""){
				day = "不做要求";
			}else{
				day = "支付款项后"+ $dd.val() +"天";
			};
			des = $("textarea[name = 'requirement']").val();

			//console.log(+"|"+type+"|"+day+"|"+des);
			$(".Js_t1").text(amount_num);
			$(".Js_t2").text(type);
			$(".Js_t3").text(day);
			$(".Js_t4").text(des);
		}else if (_target_type == "2"){
			var $endtime = $("[name='end_time']");
			if(!$endtime.length > 0 && $endtime !== ""){
				var _time = "不承诺"
			}else{
				var _time = "任务报名被接受后"+ $endtime.val() +"天内"
			}
			$(".Js_t1").text(_time);
			//console.log(_time);

		}
	},
	authenticateIdentity: function(){
		var url = "thinktank/identify",
			data = {id_card_number : $(".Js_indentify").val(), name : $(".Js_person_name").val()}
		WG.fn.ajax(url, data, function(data){
			if(data.status == "ok"){
				//alert("认证成功")
				$(".Js_tab li").eq(2).removeClass('Js_tab_list').click();
			}else if(data.status == "param_error"){
				var step3 = ['name','id_card_number'];
				var step = "";
				step = WG.fn.checkError(data.error, step, step3);

				//if("")
				//<span class="Validform_checktip Validform_wrong">错误信息</span>
			}
		})
	},
	saveThinkTankForm: function(){
		/*var url = "thinktank/apply-candidate"
		var data = $("#publish_thinktank_form").serialize();
		var ue = UM.getEditor('matchDetail');
		data += "&explain=" + ue.getContent();
		*/
		var url = "thinktank/apply-candidate";
		var ue = UM.getEditor('matchDetail');
		var indus = $('[name=industries]').val();
		var enter = $('[name=enterprise_name]').val();
		var duty = $('[name=enterprise_duty]').val();
		var duty_id = $('[name=organization_duty_id]').val();
		var file = $('.Js_c_iframe_upload').length > 0 ? $('#Js_c_iframe').contents().find('#img').find('input').val() : $('[name=imagefile]').val();
		var explain = ue.getContent();
		
		var data = {industries:indus,enterprise_name:enter,enterprise_duty:duty,organization_duty_id:duty_id,imagefile:file,explain:explain};
		
		WG.fn.ajax(url,data,function(data){
			if(data.status == "param_error"){
				var step3 = ['enterprise_name','enterprise_duty','imagefile','explain','industries','organization_duty_id'];
				var step = "";
				step = WG.fn.checkError(data.error, step, step3, 'three');

				if (step != '') {
					//window.location.href = '#step-' + step;
					//$(".Js_goto_step3").click();
					$('.Js_tab li').eq(2).click();
				}
			}else if(data.status == "ok"){
				window.location.href = WG.baseUrl + 'thinktank/index';
			}
		})
	},
	showAmount: function(type){
		//1:投资金额,2:定金比例,3:尾款比例
		var amount = 0, proportion = 0;
		if(type == 1){
			amount = $(".Js_amount_input").val();
			proportion = Number($(".Js_proportion_input1").val());
		}else if(type == 2){
			amount = Number($(".Js_amount_input").val()) == "" ? 0 : Number($(".Js_amount_input").val());
			proportion = $(".Js_proportion_input1").val();
		}else if(type == 3){
			amount = Number($(".Js_amount_input").val());
			proportion = 100-($(".Js_amount_input2").val() == "" ? 0 : Number($(".Js_amount_input2").val()))
		};

		$(".Js_amount_input").val(amount);
		$(".Js_proportion_input1").val(proportion);
		$(".Js_proportion_input2").val(100-proportion);
		$(".Js_pro_num1").text(amount*proportion/100);
		$(".Js_pro_num2").text(amount*(100-proportion)/100);
	},
	saveProjectJoinForm: function(){
		var data = $("#publish_project_join_form").serialize();
		var ue = UM.getEditor('matchDetail');
		if($.trim($("#invest_id").val())!==""){
			url = "project/update-invest";//修改
		}else{
			url = "project/invest";//发布
		};
		$(".Js_project_join_submit").off("click",WG.PublishProject.saveProjectJoinForm);
		WG.fn.ajax(url,data,function(data){
			if(data.status == "param_error"){
				var step1 = ['amount','type','require_complete_time','deposit_percentage','final_pay_time','requirement']
				var step = "";
				step = WG.fn.checkError(data.error, step, step1, 'one');

				if (step != '') {
					window.location.href = '#step-' + step;
				}
			$(".Js_project_join_submit").on("click",WG.PublishProject.saveProjectJoinForm);
			}else if(data.status == "ok"){
				window.location.href = WG.baseUrl + 'project/invest/'+$("#project_id").val();
			}
		})
	},
	saveHireApplyForm: function(){
		var data = $("#publish_hire_apply_form").serialize(),
			url;
		var ue = UM.getEditor('matchDetail');
		if($.trim($(".Js_participator_id").val())!==""){
			url = "hiring/update-participator";//修改
			data = data + '&participator_id='+ $(".Js_participator_id").val() +'&works='+ ue.getContent();
		}else{
			url = "hiring/apply";//发布
			data = data +'&works='+ ue.getContent();
		};
		$(".Js_hire_apply_submit").off("click",WG.PublishProject.saveHireApplyForm);
		WG.fn.ajax(url,data,function(data){
			if(data.status == "param_error"){
				var step1 = ['imagefile','declaration','works']
				var step = "";
				step = WG.fn.checkError(data.error, step, step1, 'one');

				if (step != '') {
					window.location.href = '#step-' + step;
				}
				$(".Js_hire_apply_submit").off("click",WG.PublishProject.saveHireApplyForm);
			}else if(data.status == "ok"){
				window.location.href = WG.baseUrl + 'hiring/participator/'+data.content
			}
		})
	},
	saveTaskApplyForm: function(){
		var data = $("#publish_task_apply_form").serialize();
		var ue = UM.getEditor('matchDetail');
		data = data +'&works='+ ue.getContent();
		if($.trim($(".Js_work_id").val())!==""){
			url = "task/update-work";//修改
		}else{
			url = "task/submit-work";//发布
		};
		$(".Js_task_submit").off("click",WG.PublishProject.saveTaskApplyForm);
		WG.fn.ajax(url,data,function(data){
			if(data.status == "param_error"){
				var step1 = ['imagefile','end_time','works']
				var step = "";
				step = WG.fn.checkError(data.error, step, step1, 'one');

				if (step != '') {
					window.location.href = '#step-' + step;
				}
			$(".Js_task_submit").on("click",WG.PublishProject.saveTaskApplyForm);
			}else if(data.status == "ok"){
				window.location.href = WG.baseUrl + 'task/work/'+data.content
			}
		})
	},
	saveEncyclopediaForm: function(){
		/*var data = $("#publish_encyclopedia_form").serialize();
		var ue = UM.getEditor('matchDetail');
		var _id = $("#Js_industryId").val();
		data = data +'&description='+ ue.getContent()+'&industry_id=' + _id;*/
		
		var tit = $('[name=title]').val();
		var file = $('.Js_c_iframe_upload').length > 0 ? $('#Js_c_iframe').contents().find('#img').find('input').val() : $('[name=imagefile]').val();
		var tags = $('[name=tags]').val();
		var des = UM.getEditor('matchDetail').getContent();
		var indus = $('#Js_industryId').val();
		
		var data = {title:tit,imagefile:file,tags:tags,description:des,industry_id:indus};
		
		$(".Js_encyclopedia_submit").off("click",WG.PublishProject.saveEncyclopediaForm);
		
		WG.fn.ajax('encyclopedia/publish',data,function(data){
			if(data.status == "param_error"){
				var step1 = ['title','tags','imagefile','description']
				var step = "";
				step = WG.fn.checkError(data.error, step, step1, 'one');

				if (step != '') {
					window.location.href = '#step-' + step;
				}
				$(".Js_encyclopedia_submit").on("click",WG.PublishProject.saveEncyclopediaForm);
			}else if(data.status == "ok"){
				window.location.href = WG.baseUrl + 'encyclopedia/detail/'+data.content
			}
		})
	},
	saveProjectForm: function(){
		var tit = $('[name=title]').val();
		var endtime = $('[name=investment_end_time]').val();
		var amout = $('[name=goal_amount]').val();
		var file = $('.Js_c_iframe_upload').length > 0 ? $('#Js_c_iframe').contents().find('#img').find('input').val() : $('[name=imagefile]').val();
		var tags = $('[name=tags]').val();
		var des = UM.getEditor('matchDetail').getContent();
		
		var data = {title:tit,investment_end_time:endtime,goal_amount:amout,imagefile:file,tags:tags,description:des}
		
		$(".Js_project_submit").off("click",WG.PublishProject.saveProjectForm);
		WG.fn.ajax('project/publish',data,function(data){
			if(data.status == "param_error"){
				var step1 = ['title','investment_end_time','goal_amount','tags','imagefile','description']
				var step = "";
				step = WG.fn.checkError(data.error, step, step1, 'one');

				if (step != '') {
					window.location.href = '#step-' + step;
				}
				$(".Js_project_submit").on("click",WG.PublishProject.saveProjectForm);
			}else if(data.status == "ok"){
				window.location.href = WG.baseUrl + 'project/detail/'+data.content
			}
		})
	},
	saveTaskForm: function(){
		var tit = $('[name=title]').val();
		var endtime = $('[name=submit_work_end_time]').val();
		var amout = $('[name=bonus_amount]').val();
		var file = $('.Js_c_iframe_upload').length > 0 ? $('#Js_c_iframe').contents().find('#img').find('input').val() : $('[name=imagefile]').val();
		var tags = $('[name=tags]').val();
		var des = UM.getEditor('matchDetail').getContent();

		var data = {title:tit,submit_work_end_time:endtime,bonus_amount:amout,imagefile:file,tags:tags,description:des}
		
		$(".Js_task_submit").off("click",WG.PublishProject.saveTaskForm);
		WG.fn.ajax('task/publish',data,function(data){
			if(data.status == "param_error"){
				var step1 = ['title','submit_work_end_time','bonus_amount','tags','imagefile','description']
				var step = "";
				step = WG.fn.checkError(data.error, step, step1, 'one');

				if (step != '') {
					window.location.href = '#step-' + step;
				}
				$(".Js_task_submit").on("click",WG.PublishProject.saveTaskForm);
			}else if(data.status == "ok"){
				window.location.href = WG.baseUrl + 'task/detail/'+data.content
			}

		})
	},
	saveExchangeForm: function(){
		var tit = $('[name=title]').val();
		var price = $('[name=price]').val();
		var file = $('.Js_c_iframe_upload').length > 0 ? $('#Js_c_iframe').contents().find('#img').find('input').val() : $('[name=imagefile]').val();
		var tags = $('[name=tags]').val();
		var id = $('[name=tradeId]').val();
		var des = UM.getEditor('matchDetail').getContent();

		var data = {title:tit,price:price,imagefile:file,tags:tags,description:des,id:id}
		
		$(".Js_exchange_submit").off("click",WG.PublishProject.saveExchangeForm);
		WG.fn.ajax('exchange/publish',data,function(data){
			if(data.status == "param_error"){
				var step1 = ['title','price','tags','imagefile','description']
				var step = "";
				step = WG.fn.checkError(data.error, step, step1, 'one');

				if (step != '') {
					window.location.href = '#step-' + step;
				}
				$(".Js_exchange_submit").on("click",WG.PublishProject.saveExchangeForm);
			}else if(data.status == "ok"){
				window.location.href = WG.baseUrl + 'exchange/detail/'+data.content
			}
		})
	},
	saveHireForm: function(){
		var tit = $('[name=title]').val();
		var type = $('[name=end_time_type]').val();
		var file = $('.Js_c_iframe_upload').length > 0 ? $('#Js_c_iframe').contents().find('#img').find('input').val() : $('[name=imagefile]').val();
		var tags = $('[name=tags]').val();
		var ue = UM.getEditor('matchDetail');
		var dataObj={title:tit,end_time_type:type,imagefile:file,tags:tags}; 
		
		if(WG.TempData){
			var hireArr=[];
			for(var i = 0; i < WG.TempData.length; i++){
				var hireObj = new Object;
				hireObj.title = WG.TempData[i].tx;
				hireObj.amount = WG.TempData[i].jj;
				hireObj.award = WG.TempData[i].jpHtml;
				hireArr.push(hireObj);
			};
			dataObj.prizes = hireArr;
		}else{
			dataObj.prizes = '';
		}

		dataObj.description = ue.getContent();
		$(".Js_hire_submit").off("click",WG.PublishProject.saveHireForm);

		WG.fn.ajax('hiring/publish',dataObj,function(data){
			if(data.status == "param_error"){
				var step1 = ['title','tags','end_time','imagefile','description'];
				var step2 = ['prizes'];
				var step = "";
				step = WG.fn.checkError(data.error, step, step1, 'one');
				step = WG.fn.checkError(data.error, step, step2, 'two');

				if (step != '') {
					window.location.href = '#step-' + step;
				}
				$(".Js_hire_submit").on("click",WG.PublishProject.saveHireForm);
			}else if(data.status == "ok"){
				window.location.href = WG.baseUrl + 'hiring/detail/'+data.content
			}
		})
	},
}


/**
 * [PublishProjectUpdate 发布更新页面]
 * @type {Object}
 */
WG.PublishProjectUpdate = {
	init: function(){
		$(".Js_publish_update").on("click",this.run)
	},
	editor : null,
	run: function(){
		var T = WG.PublishProjectUpdate;
		var random = Math.ceil(Math.random()*10000000);
		var html = [
			'<div class="publishupdate">',
			'	<div class="title">',
			'		<h2>发布项目更新</h2>',
			'		<a class="Js_close_update"></a>',
			'	</div>',
			'	<div class="main">',
			'		<div class="con">',
			'			<p><input class="" style="width:100%" type="text" placeholder="标题"></p>',
			'			<div class="height_30"></div>',
			'			<div style="height:276px; width:530px" id="loadBefore"></div>',
			'			<p><script type="text/plain" id="publishupdate-text" style="width:530px;height:215px;"></script></p>',
			'			<div class="height_20"></div>',
			'			<div>',
			'				<span class="btn deepgrey">',
			'					<a class="btn deepgrey_l Js_close_update" href="javascript:">取消更新</a>',
			'				</span>&nbsp;&nbsp;',
			'				<span class="btn deepblue">',
			'					<a class="btn deepblue_l" href="javascript:">提交更新</a>',
			'				</span>',
			'			</div>',
			'		</div>',
			'	</div>',
			'</div>'].join("");

		$.fancybox({
			fitToView	: false,
			autoSize	: true,
			padding     : 0,
			closeBtn    : false,
			content     : html,
			afterShow  : function(){T.regFun()},
			afterClose : function(){
					T.editor.destroy();
					
			}
		});
	},
	regFun: function(){
		//编辑器配置
		if($("#publishupdate-text").length>0){
			$('#loadBefore').remove();
			this.editor = UM.getEditor('publishupdate-text');
		} 
		$(".Js_close_update").on("click",function(){$.fancybox.close()})
	}
}