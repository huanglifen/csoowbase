WG.Login = {
	init: function(){
		this.loginFn();
		this.verifyFn();
		this.registFn();
		this.forgetFn();
	},
	loginFn: function(){
		if($(".login_wraper").length>0){
			$('.Js_username_input,.Js_password_input').on('keyup',function(e){
				if (e.keyCode==13 && $('.Js_username_input').val()!=="" && $('.Js_password_input').val()!==""){
					$('.Js_loginSubmit').click();
				}
			});
		    $('.Js_loginSubmit').on('click',function(){
		        var $username = $('#login_form input[name=login_name]'),
		                $pwd = $('#login_form input[name=password]'),
		                $this = $('.Js_loginSubmit');
		        var errormsg = '';
		        if($username.val() == ''){
		            errormsg = '请输入用户名';
		        }
		        if($pwd.val() == ''){
		            errormsg = '请输入密码'
		        }
		        if($username.val() == '' && $pwd.val() == ''){
		            errormsg = '请输入用户名和密码';
		        }
		        $('.errorMsg').text(errormsg);
		        if($('.errorMsg').text() != ''){
		            return;
		        }else{
		        	$this.find(".DeepBlue").text("登录中...");
					var data = $('#login_form').serialize();
					WG.fn.ajax('user/login',data,function(e){
						if(e.status=='param_error'){
							for(var i in e.error){
								$('.errorMsg').text(e.error[i]);	
							}
							$this.find(".DeepBlue").text("登录");
						}else if(e.status == 'ok'){
							window.location.href = 	globalLastPage;
						}
					});	
				}
		    });
		}
	},
	
	verifyFn : function(){
		if($('.change-verify-code').length > 0){
			$('.change-verify-code').bind('click',function(){
				$('.verify-code').attr('src',baseUrl + 'user/verify-code?'+ Math.random()*10000);
				$('[name=verify_code]').val('');
				$(this).parent().next().removeClass('Validform_wrong Validform_right').text('');
			});
		}	
	},
	
	registFn: function(){
		if($(".register-wrap").length>0){
			$('#Js_register_pwd').blur(function(){
				$(this).val($.trim($(this).val()));	
				$('#Js_register_hid').val($(this).val());
				$('#Js_register_hid').keyup();
			}).keyup(function(){
				$('#Js_register_hid').val($(this).val());
				$('#Js_register_hid').keyup();
			})
			
			$('#Js_user_account').blur(function(){
				$(this).val($.trim($(this).val()));
			})
	        $(".Js_submit").on("click",function(){
				$("#formname").submit();
			});
			
			$('#Js_register_again').blur(function(){
				$(this).val($.trim($(this).val()));
			})
			var num = 0;
			var arr = ['.','..','...'];
	        //表单验证
	        var _vf = $("#formname").Validform({
	            tiptype:3,
	            showAllError:true,
				ajaxPost : true,
				callback : function(data){
					if(data.status=='ok'){
						$(".Js_submit span").eq(0).text('注册中.');
						$(".Js_submit").css({'padding-right':'29px','position':'relative'}).find('span').css('padding-left','20px');
						window.setInterval(function(){
							if(num%arr.length==0){
								$(".Js_submit").find('span').eq(1).show();
							}else if(num%arr.length == 1){
								$(".Js_submit").find('span').eq(2).show();
							}else if(num%arr.length == 2){
								$(".Js_submit").find('span').eq(1).hide();
								$(".Js_submit").find('span').eq(2).hide();
							}
							num++;
						},500);
						window.location.href = baseUrl + 'user/register-success';	
					}
				},
				usePlugin:{
	                passwordstrength:{}
	            }
	        })
		}
	},
	
	forgetFn : function(){
		if($(".Js_forget_pwd").length>0){
			$(".Js_goto_step2").on("click",function(){
				$("#getUserName").submit();
			});
			
	        //表单验证
	        var _vf = $("#getUserName").Validform({
	            tiptype:3,
	            showAllError:true,
				ajaxPost : true,
				callback : function(data){
					if(data.status == 'ok'){
						$('#Js_ccNo').text(data.content.cc_no);
						$('#Js_ccEmail').text(data.content.email);
						$('#Js_ccPhone').text(data.content.mobile);
						$(".Js_tab li").eq(0).addClass('Js_tab_list');
						$(".Js_tab li").eq(1).removeClass('Js_tab_list').click();
					}
					init_select(data);
				}
	        })

	        function init_select(data){
	        	var findPasswordType = [];
	        	if(data.content.mobile == ""){
		        	findPasswordType = [
						{name:'邮箱',"attr": {"id": 0}}
					]
	        	}else{
		        	findPasswordType = [
						{name:'邮箱',"attr": {"id": 0}},{name:'手机号',"attr": {"id": 1}}
					]
	        	}
				$('.Js_findPasswordType').dropDownMenu({
					data : findPasswordType,
					format : true,
					callback : function(ev){
				        if(ev && ev.id == 0){
				            $(".Js_email_t").show();
				            $(".Js_phone_t").hide();
				        }else if(ev && ev.id == 1){
				            $(".Js_email_t").hide();
				            $(".Js_phone_t").show();
				        }
					}
				});
	        }
			/*
			if(isSendSuc == 1){
				$(".Js_tab li").eq(0).addClass('Js_tab_list');
				$(".Js_tab li").eq(1).addClass('Js_tab_list');
				$(".Js_tab li").eq(2).removeClass('Js_tab_list').click();	
			}
			*/
			var timer = null;
			$(".Js_goto_step3").on("click",function(){
				var my = $(this);
				var _this = $(this).find('.Js_send_email');
				var num = 0;
				var arr = ['.','..','...'];
				
				if(!timer){
					_this.find('span').text('邮件发送中.');
					timer = window.setInterval(function(){
						if(num%arr.length==0){
							_this.find('span').text('邮件发送中..');
						}else if(num%arr.length == 1){
							_this.find('span').text('邮件发送中...');
						}else if(num%arr.length == 2){
							_this.find('span').text('邮件发送中.');
						}
						num++;
					},500);
					
					WG.fn.ajax('user/send-email',{},function(e){
						window.clearInterval(timer);
						timer = null;
						if(e.status=='ok'){
							_this.find('span').text('邮件发送成功！');
							my.unbind('click');
						}
					});
				}
			});

			$(".Js_send_phone").on("click",function(){
				WG.fn.ajax('user/findpwd-submit-mobile-code',{ 'verify_code': $(".Js_phone_code").val()},function(e){
					if(e.status=='ok'){
						$(".Js_tab li").eq(0).addClass('Js_tab_list');
						$(".Js_tab li").eq(1).addClass('Js_tab_list');
						$(".Js_tab li").eq(2).removeClass('Js_tab_list').click();	
					}else if(e.status == "param_error"){
						var step3 = ['verify_code_mobile'];
						var step = "";
						step = WG.fn.checkError(e.error, step, step3);
					}
				});
			});
			$(".Js_send_email").on("click",function(){
				WG.fn.ajax('user/findpwd-submit-email-code',{ 'verify_code': $(".Js_email_code").val()},function(e){
					if(e.status=='ok'){
						$(".Js_tab li").eq(0).addClass('Js_tab_list');
						$(".Js_tab li").eq(1).addClass('Js_tab_list');
						$(".Js_tab li").eq(2).removeClass('Js_tab_list').click();	
					}else if(e.status == "param_error"){
						var step3 = ['verify_code_email'];
						var step = "";
						step = WG.fn.checkError(e.error, step, step3);
					}
				});
			});

			$(".Js_getCode").on("click", WG.Login.getNumCode)

			$(".Js_goto_step4").on("click",function(){
				$("#enterNewPwd").submit();
			});
			
			var _vf1 = $("#enterNewPwd").Validform({
	            tiptype:3,
	            showAllError:true,
				ajaxPost : true,
				callback : function(data){
					if(data.status=='ok'){
						$(".Js_tab li").eq(2).addClass('Js_tab_list');
						$(".Js_tab li").eq(3).removeClass('Js_tab_list').click();	
					}
				}
	        })
		}
	},

	getNumCode: function(){
		var T = WG.Login,
			$this = $(this),
			time = 60;
		var url,data;
		var ctype = $this.attr("data-codenumtype");
		if(ctype =="phone"){
			url = "user/findpwd-send-mobile-code",
			data = {new_mobile:$("#Js_new_mobile").val()};
		}else if(ctype =="email"){
		 	url = "user/findpwd-send-email-code",
		 	data = {new_email:$("#Js_user_account").val()}
		};
		$(".Js_getCode").off("click",T.getNumCode);
		$this.addClass("disable").text("验证码发送中…");
		WG.fn.ajax(url,data,function(data){
			if(data.status == "ok"){
				$this.addClass("disable").text("60秒后可重新发送");
				if($(".Js_new_mobile").length>0){
					$(".Js_new_mobile").parent().find(".Validform_wrong").remove()
				};
				var st = setInterval(function(){
					time--;
					$this.text(time+"秒后可重新发送");
					if(time == 0){
						window.clearInterval(st);
						$(".Js_getCode").on("click",T.getNumCode);
						$this.removeClass("disable").text("免费获取验证码");
					} 
				},1000);
			}else if(data.status == "param_error"){
				var step3;
				if(ctype =="phone"){
					step3 = ['new_mobile'];
				}else if(ctype =="email"){
				 	step3 = ['new_email'];
				};
				var step = "";
				step = WG.fn.checkError(data.error, step, step3);
				$this.removeClass("disable").text("免费获取验证码");
				$(".Js_getCode").on("click",T.getNumCode);
			}
		})
	}
/*
$(".Js_goto_step2").on("click",function(){
	$(".Js_tab li").eq(1).click();
});
$(".Js_goto_step3").on("click",function(){
	$(".Js_tab li").eq(2).click();
})
$(".Js_goto_step4").on("click",function(){
	$(".Js_tab li").eq(3).click();
})*/
	
}

