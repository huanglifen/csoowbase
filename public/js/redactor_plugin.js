if (typeof RedactorPlugins === 'undefined') var RedactorPlugins = {};

RedactorPlugins.undo = {

	init: function()
	{
		this.buttonAddBefore('bold', 'undo', '后退', function()
		{
			//this.insertHtml('It\'s awesome! ');
			if (this.opts.buffer.length) this.bufferUndo();
			else{
				this.document.execCommand('undo', false, false);
			} 
		});
		this.buttonAddSeparatorBefore('bold');
	}
}
RedactorPlugins.redo = {
	init: function()
	{
		this.buttonAddAfter('undo', 'redo', '前进', function()
		{
			//this.insertHtml('It\'s awesome! ');
			if (this.opts.rebuffer.length != 0) this.bufferRedo();
			else this.document.execCommand('redo', false, false);
		});
	}
}
RedactorPlugins.expression = {
	init: function()
	{
		var etitle = [
			'哈哈笑', '江南style', '得意地笑', '转发', '挤火车', '泪流满面', '喜爱', '火车', '转', '鼓掌', '顶',
			'上镜', '萌', '神马', '浮云', '给力', '围观', '威武', '熊猫', '兔子', '奥特曼', '囧',
			'互粉', '礼物', '呵呵', '嘻嘻', '哈哈', '可爱', '可怜', '挖鼻屎', '吃惊', '害羞', '挤眼',
			'闭嘴', '鄙视', '爱你', '泪', '偷笑', '亲亲', '生病', '太开心', '懒得理你', '右哼哼', '左哼哼',
			'嘘', '衰', '吐', '委屈', '抱抱', '拜拜', '思考','打哈欠',
			'钱', '酷', '开心', 'OK', 'Good',  '不要', '赞','弱'
		];

		this.buttonAddSeparator();
		//for (var i = 0; i < 2; i++)
		//{
			var name = 'expression';

			var $dropdown = $('<div class="redactor_dropdown redactor_dropdown_box_' + name + '" style="display: none; width: 270px;">');

			this.pickerBuild($dropdown, name, etitle);
			$(this.$toolbar).append($dropdown);

			this.buttonAdd(name, '表情', $.proxy(function(btnName, $button, btnObject, e)
			{
				this.dropdownShow(e, btnName);

			}, this));
		//}
	},
	pickerBuild: function($dropdown, name, etitle)
	{
		var _self = this;
		var onSwatch = function(e)
		{
			e.preventDefault();

			var $this = $(this);
			_self.pickerSet($this.attr('data-num'));
		}

		var len = etitle.length;
		for (var z = 0; z < len; z++)
		{
			var color = etitle[z];

			var $swatch = $('<a title="'+color+'" data-num="'+(z+1)+'" href="#" style="float: left; font-size: 0; border: 2px solid #fff; padding: 0; margin: 2px; width: 22px; height: 22px;"><img src="../../images/face/'+(z+1)+'.gif"/></a>');
			$swatch.css('background-color', color);
			$dropdown.append($swatch);
			$swatch.on('click', onSwatch);
		}
	},
	pickerSet: function(url)
	{
		this.bufferSet();

		this.$editor.focus();

		this.insertHtml('<img src="../../images/face/'+url+'.gif" />');
		/*
		this.inlineRemoveStyle(rule);
		if (type !== false) this.inlineSetStyle(rule, type);
		if (this.opts.air) this.$air.fadeOut(100);
		*/
		this.sync();
	}
};
