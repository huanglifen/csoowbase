WG.UploadMore = {
	init: function(opt){
		var fileTemplate= ['<div class="Js_updateImg fl"><div class="updateImgLoadDiv">',
							'	<div class="uploadloadwrap">',
							'		<a href="javascript:;" class="close Js_upload_delete"></a>',
							'		<div class="updateImgLoad">',
							'			<span class="sc">上传中</span>',
							'			<span class="process-text">0%</span>',
							//'		<span class="scName"></span>',
							'			<div class="processDiv">',
							'				<div class="opacityBorder"></div>',
							'				<div class="process" style="width:0%">',
							'				</div>',
							'			</div>',
							'		</div>',
							'	</div>',
							'</div>',
							'</div>'].join("");

		if(window.navigator.userAgent.indexOf('Safari')>=0 && window.navigator.userAgent.indexOf('Chrome')<0){
			$('.upload-input').removeAttr('multiple');
		}
		var currentObj;
		if(opt && opt.obj){
			currentObj = opt.obj;//供外部调用，传递的上传对象
		}else{
			currentObj = ".uploadWrap-more , .Js_file_uploadbox_more";
		} 

		$(currentObj).html5Uploader({
			name: 'imagefile',
			postUrl: WG.baseUrl + moreUploadPath,
			onClientAbort: function(){
				alert("上传中断，请重新上传")
			},
			onClientLoadStart: function(e, file, xhr) {
				var obj = $(fileTemplate);
				upload.append(obj);
				uploadObjArr.push(obj);
			},
			onClientLoad: function(e, file, xhr) {
			},
			onServerLoadStart: function(e, file, xhr) {
				if(typeof FileReader == 'undefined'){
					var obj = $(fileTemplate);
					if(upload.find('.updateSucImg').length>0){
						upload.find('.updateSucImg').eq(0).before(obj);
					}else{
						upload.append(obj);
					}
					uploadObjArr.push(obj);
				}
			},
			onServerProgress: function(e, file, xhr) {
				for(var i=0; i<xhr_arr.length; i++){
					if(xhr===xhr_arr[i]){
						var xhrIndex = 	i;
					}	
				}
				var percentComplete = e.loaded / e.total;
				var precentCompleteNum = parseInt((parseInt(e.loaded) / parseInt(e.total))*100);
				uploadObjArr[xhrIndex].find(".process").css("width",precentCompleteNum.toString()+"%");
				uploadObjArr[xhrIndex].find(".process-text").text(precentCompleteNum.toString()+"%");
				if(typeof FileReader == 'undefined'){
					if($('.scName').length>0){
						uploadObjArr[xhrIndex].find('.scName').text(fileName[xhrIndex].name+'(' + parseInt(e.total / 1024) + 'KB)');
					}
				}
			},
			onServerLoad: function(e, file) {

			},
			onSuccess:function(e, file, xhr, data){
				var data = eval ('('+ data +')');
				var status = data.status;
				if(status== 'ok'){
					for(var i=0; i<xhr_arr.length; i++){
						if(xhr===xhr_arr[i]){
							var xhrIndex = 	i;
						}	
					}
					uploadObjArr[xhrIndex].remove();
					xhr_arr.splice(xhrIndex,1);
					uploadObjArr.splice(xhrIndex,1);
					if(typeof FileReader == 'undefined') fileName.splice(xhrIndex,1);
					var path = data.content;
					var tpic =['<div class="updateSucImg">',
							'	<div class="imgwrap">',
							'		<p><img src="'+WG.baseUrl+path+'"></p>',	
							'	</div>',
							'	<a href="javascript:;" title="删除" class="close Js_file_delete">删除</	a>',
							'	<input name="cover" class="upload_success_cover" type="hidden" value="'+path+'" />',
							'</div>'].join("");
					upload.append(tpic);
				}else{
					for(var i=0; i<xhr_arr.length; i++){
						if(xhr===xhr_arr[i]){
							var xhrIndex = 	i;
						}	
					}
					uploadObjArr[xhrIndex].remove();
					xhr_arr.splice(xhrIndex,1);
					uploadObjArr.splice(xhrIndex,1);
					if(typeof FileReader == 'undefined') fileName.splice(xhrIndex,1);
					if(data.error){
						alert(data.error);//这里出要弹出意外信息，比如不支持的上传文件格式
					}else{
						alert("上传文件格式不正确，请从新上传")
					}
					upload.parents('.rowblock').find('.Js_updateImg').children().eq(xhrIndex).remove();
				}
				//判断当前是否有回调文件
				if(opt && opt.callback && typeof opt.callback == "function"){
					opt.callback.call(this)
				}
			}
		});
	}
};

$('.Js_upload_delete_more').live('click',function(){
	for(var i=0; i<uploadObjArr.length; i++){
		if($(this).parents('.Js_updateImg').get(0)===uploadObjArr[i].get(0)){
			var objIndex = 	i;
		}	
	}
	if(xhr_arr[objIndex]){
		xhr_arr[objIndex].abort();
		xhr_arr.splice(objIndex,1);
		uploadObjArr.splice(objIndex,1);
		if(typeof FileReader == 'undefined') fileName.splice(objIndex,1);
		$(this).parent().next().remove();
		$(this).parent().remove();
	}
})

$(".Js_uploadbox_more").live('click',function(e){
	$(this).parent().find('.upload-input').click();
	
	if($(this).parents('.upload-files').hasClass('Js_cover')) {
		upload = $(this).parents('.Js_cover').find('.upload-more');
	}
	e.stopPropagation();
});
$(".upload-input").live('click',function(e){e.stopPropagation();});

$(".upload-more .Js_file_delete").die().live("click",function(){
	var that = $(this);
	var file_id = null;//文件id
	var match_id = null;//比赛id
	var fdata = {task_id:file_id,match_id:match_id};
	//ajax_(WG.baseUrl+'task/removeFinalWork',fdata,delete_file_c,that);  //发送Ajax数据
	delete_file_c(that);//删除成功回调
})
//删除文件成功回调
function delete_file_c(obj){
	obj.parents(".updateSucImg").remove();
	var ie = (navigator.appVersion.indexOf("MSIE")!=-1);//IE
	if(ie){
		//$(".upload-input").parent()[0].reset();
	}else{
		$(".upload-input").val("");
	}
	obj.parent().remove();
	return false
}
