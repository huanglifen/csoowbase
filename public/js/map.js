WG.map = {
	timer : null,
	city : null,
	cityAjax : null,
	init : function(){
		var self = this,
			cityList = $('#Js_map_city a');
			
		cityList.mouseenter(function(){
			window.clearTimeout(self.timer);
			self.timer = null;
			self.city = $(this);
			cityList.removeClass('hover');
			$(this).addClass('hover');
			$('#Js_city_tag').remove();
			
			self.cityAjax = WG.fn.ajax('area/region-card',{id:self.city.attr('data-id')},function(ev){
				if(ev.status == 'ok')
					fn(ev);
			})
			
			function fn(e){
				if($('#Js_city_tag').length <= 0){
					var x = self.city.position().left;
					var y = self.city.position().top;
					var tpl = e.content;
					
					$('#Js_map_city').parent().append(tpl);
					$('#Js_city_tag').css({
						position : 'absolute',
						left : (x - $('#Js_city_tag').outerWidth() / 2) + (self.city.width()/2),
						top : (y - $('#Js_city_tag').outerHeight())-9
					})
				}
			}
			
		}).mouseleave(function(ev){
			var _this = $(this);
			if(self.cityAjax) self.cityAjax.abort();
			if(!self.timer){
				self.timer = window.setTimeout(function(){
					$('#Js_city_tag').remove();
					self.timer = null;
					self.city = null;
					_this.removeClass('hover');
				},133);
			}
		})
		
		$('#Js_city_tag').live('mouseenter',function(){
			window.clearTimeout(self.timer);
			self.timer = null;
			self.city.addClass('hover');
		}).live('mouseleave',function(){
			if(!self.timer){
				self.timer = window.setTimeout(function(){
					$('#Js_city_tag').remove();
					self.timer = null;
					self.city.removeClass('hover');
				},133);
			}
		}).live('click',function(){
			window.clearTimeout(self.timer);
			$('#Js_city_tag').remove();
			self.timer = null;
			self.city.removeClass('hover');	
		})
	}
}