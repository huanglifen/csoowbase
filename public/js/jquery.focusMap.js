(function($){
	var methods = {
		init : function(){
			var settings = arguments[0];
			this.each(function(){
				wheelType[settings.type].init.call(this,settings);
			})
			return this;
		}
	}
	
	var wheelType = {
		sola : {
			timer : {t1:null,t2:null,t3:null},
			
			init : function(settings){
				var timer = {t1:null,t2:null,t3:null},
					self = $(this),
					scrollDom = $(this).find('[scroll]'),
					scrollBtnItems = $(this).find('[scroll_btn]').children(),
					scrollDomItemsLen = scrollDom.children().children().length;
				
				
				scrollDom.children().css('width',100*scrollDomItemsLen+'%').children().css('width',100/scrollDomItemsLen+'%').eq(0).css('position','relative');
				scrollDom.data('index',0);
				scrollDom.data('showType',settings.width);
				timer.t2 = window.setInterval(wheelType.sola.showImgCarousel.apply(this,[false,0,timer,settings]),settings.time);
				$(window).resize(function(){
					if(!timer.t1){
						var index = scrollDom.data('index'),
							spacing = settings.spacing ? settings.spacing : 0,
							wd = scrollDom.data('showType') == 'full' ? $(window).width() : self.width();
						
						scrollDom.scrollLeft(index*(wd-spacing));
					}
				})
				
				wheelType.sola.eventFn.call(this,scrollBtnItems,timer,settings);
			},
			
			eventFn : function(o,timer,settings){
				var self = this,
					scrollDom = $(this).find('[scroll]'),
					scrollBtnItems = $(this).find('[scroll_btn]').children(),
					scrollDomItemsLen = scrollDom.children().children().length,
					leftBtn = $(this).find('[leftBtn]'),
					rightBtn = $(this).find('[rightBtn]'),
					index = 0;
					
				$(this).mouseenter(function(){
					leftBtn.show();
					rightBtn.show();
				}).mouseleave(function(){
					leftBtn.hide();
					rightBtn.hide();
				})
				
				if(settings.focusBtnType == 'click'){
					o.click(function(){
						index = o.index($(this));
						clickFn(index);
					})
				}else if(settings.focusBtnType == 'mouseenter'){
					o.mouseenter(function(){
						window.clearInterval(timer.t2);
						timer.t2 = null;
						index = o.index($(this));
						timer.t3 = window.setTimeout(wheelType.sola.showImgCarousel.apply(self,[true,index,timer,settings,null,'mouseenter']),33);	
					}).mouseleave(function(){
						window.clearTimeout(timer.t3);
						timer.t3 = null;
						if(!timer.t2){
							index = o.index(o.filter('.current'));
							timer.t2 = window.setInterval(wheelType.sola.showImgCarousel.apply(self,[false,index,timer,settings]),settings.time);
						}
					})
				}
				
				
				
				leftBtn.click(function(){
					index = scrollDom.data('index');
					index = index == 0 ? scrollDomItemsLen - 1 : index - 1;
					clickFn(index);
				})
				
				rightBtn.click(function(){
					index = scrollDom.data('index');
					index = index == scrollDomItemsLen - 1 ? 0 : index + 1;
					clickFn(index);
				})
				
				function clickFn(num){
					window.clearInterval(timer.t1); timer.t1 = null;
					window.clearInterval(timer.t2); timer.t2 = null;
					window.clearInterval(timer.t3); timer.t3 = null;
					wheelType.sola.showImgCarousel.apply(self,[true,num,timer,settings,function(){
						timer.t2 = window.setInterval(wheelType.sola.showImgCarousel.apply(self,[false,num,timer,settings]),settings.time);
					}])();
				}
			},
			
			
			
			showImgCarousel : function(flag,n,timer,settings,fn,type){
				var self = $(this),
					scrollDom = $(this).find('[scroll]'),
					scrollBtnItems = $(this).find('[scroll_btn]').children(),
					scrollDomItems = scrollDom.children().children(),
					index = n,
					num = scrollDomItems.length;
				return function(){
					if(type=='mouseenter'){
						window.clearInterval(timer.t1);
						timer.t1 = null;	
					}
					if(!timer.t1){
						if(flag){index=n}else{index++}
						var s = scrollDom.scrollLeft();
						var itemIndex = index == scrollDomItems.length ? 0 : index;
						scrollDom.data('index',itemIndex);
						timer.t1 = window.setInterval(function(){
							var spacing = settings.spacing ? settings.spacing : 0;
							if(scrollDom.data('showType')=='full'){
								var wd = $(window).width() - spacing;
							}else{
								var wd = self.width() - spacing;
							}
							var l = (index*wd - s)>0 ? Math.ceil((index*wd - s)/2) : Math.floor((index*wd - s)/2);
							s += l;
							scrollDom.scrollLeft(s);
							scrollBtnItems.removeClass('current');
							scrollBtnItems.eq(index%num).addClass('current');
							if(index == num){
								scrollDomItems.eq(0).css('left', wd*num);	
							}else if(index < num - 1){
								scrollDomItems.eq(0).css('left', 0);	
							}
							if(s == index*wd){
								if(fn) fn();
								window.clearInterval(timer.t1);
								timer.t1 = null;
								if(index%num == 0){
									scrollDomItems.eq(0).css('left','0px');
									index = 0;
									scrollDom.scrollLeft(0);	
								}
							}
						},33)
					}	
				}	
			}	
		}	
	}
	
	$.fn.imgWheelShow = function(options) {
		var method = arguments[0];
        if(methods[method]) {
            method = methods[method];
            arguments = Array.prototype.slice.call(arguments, 1);
        } else if( typeof(method) == 'object' || !method ) {
			method = methods.init;
        } else {
            $.error( 'Method ' +  method + ' does not exist on' );
            return this;
        }
		
        return method.apply(this, arguments);
	}
})(jQuery);