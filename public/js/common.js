$(window).load(function(){
	WG.init();
	//从document.ready改为load主要是修改ie10、ie9中关于js加载顺序的问题。
	//如果用document.ready的话，无法加载各个比赛发布也中的tab.js造成步骤失效。	
})
/**
 * [WG 主对象]
 */
window.WG = {
	init: function(T) {
		T = T || this;
		for (var p in T) {
			T[p] && T[p].init && T[p].init();
		};
	},
	stopBubble: function(e){  
		// 如果传入了事件对象，那么就是非ie浏览器  
		if(e&&e.stopPropagation){  
			//因此它支持W3C的stopPropagation()方法  
			e.stopPropagation();  
		}else{  
			//否则我们使用ie的方法来取消事件冒泡  
			window.event.cancelBubble = true;  
		}  
	},
	baseUrl: baseUrl //网站主域名配置，修改成自己的根域名（目录）
};

/**
 * [fn 通用函数]
 * @type {Object}
 */
WG.fn = {
	ajax: function(businessInterface, data, successFunction, completeFun, property, object) {
		request = {
			url : WG.baseUrl + businessInterface,
			data : data,
			dataType : "json",
			type : "POST",
			success : function(data) {
				if(data.status == "not_login"){
					window.location.href = baseUrl + 'user/login';
				}else{
					successFunction(data, object)
				}
			},
			complete : function(a,b){
				if(completeFun)
					completeFun(a,b);
			}
		};

		$.extend(request, property);
		var r_ajax = $.ajax(request);
		return r_ajax;
	},
	checkError: function(error, previousStep, steps, stepPage) {
	    var hasError = false;
	    for (var i in steps) {
	        id = steps[i];
	        if (error.hasOwnProperty(id)) {
	            //$('#' + id + ' .tools').addClass('error');
	            var errhtml = ''
	            if($('#' + id +" .Validform_checktip").length>0){
	            	$('#' + id +" .Validform_checktip").addClass('Validform_wrong').show().text(error[id])
	            }else{
		            $('#' + id ).append('<span class="Validform_checktip Validform_wrong">'+ error[id] +'</span>');
	            }
	            hasError = true;
	        } else {
	            $('#' + id ).find(".Validform_checktip").hide();
	        }
	    }

	    if (previousStep != '') {
	        return previousStep;
	    }

	    return hasError ? stepPage : '';
	},
	htmlspecialchars: function(str){  
		str = str.replace(/&/g, '&amp;');
		str = str.replace(/</g, '&lt;');
		str = str.replace(/>/g, '&gt;');
		str = str.replace(/"/g, '&quot;');
		str = str.replace(/'/g, '&#039;');
		return str;
	},
	getMatchtype: function(id){
		var _mt;
		if (id == 1){
			_mt = "project";
		}else if(id == 2 || id == 21) {
			_mt = "task"
		}else if (id == 3 || id == 31) {
			_mt = "hiring"
		}else if (id == 4) {
			_mt = "exchange"
		}else if(id == 71 || id == 72 || id == 73){
			_mt = "area"
		}else{
		    _mt = "encyclopedia"
		};
		return _mt;
	}
};

/**
 * [限制input类型]
 */
WG.InputType = {
	init: function(){
		var T = WG.InputType;
		var input_arr = [];
		$.each(input_arr, function(index, val) {
			 var _t = $(this).data("text-type");
			 T.checkType(_t);
		});
	},
	checkType: function(val){

	}
}

//浏览器版本判断
/*
WG.Version = {
	init: function(){
		if (!$.support.leadingWhitespace) { //判断到IE9及以上
			alert("请使用最新浏览器")
		}
	}
}
*/

/**
 * [PostTrend 发布动态组件]
 */
WG.PostTrend = {
	share_vodie : '',
	share_vodie_ajax : null,
	init: function () {
		WG.PostTrend.postTrend();
		$(".Js_posttrend_btn").on("click",this.showPostTrendBox);
	},
	postTrend:function(){
		//$('body').on('click','.Js_pt_btn',function(){
			var T = WG.PostTrend;
			var content = $('#Js_posttrend_text').val();
			var pic = $('.updateSucImg').eq(0).find('img').attr('src') ? $('.updateSucImg').eq(0).find('img').attr('src').replace(baseUrl,'') : '';
			var video = $('.Js_show_video').html() ? $('.Js_show_video').html() : '';
			var err = $(this).parents(".posttrend").find(".Js_num_error")
			//console.log(content + ':' + pic + ":" + video)
			if(err.length>0) return;
			if(content || pic || video ){
				$("body").off('click','.Js_pt_btn',T.postTrend);
				WG.fn.ajax('dynamic/publish-dynamic',{content:content,pic:pic,video:video},function(e){
					if(typeof(shareThisPage) != 'undefined' && shareThisPage){
						if(e.status == 'ok'){
							if($('.Js_no_dynamic_data').length > 0){
								$('.Js_no_dynamic_data').remove();	
							}
							$('.Js_dis_list_content').prepend(e.content);
						}else{
							$("body").on('click','.Js_pt_btn',T.postTrend);	
						}
					}
					$.fancybox.close();
				});
			}
		//})
		
		window.setInterval(function(){
			if($('.Js_pt_videourl_text').length > 0){
				if($('.Js_pt_videourl_text').val()!=T.share_vodie && $.trim($('.Js_pt_videourl_text').val())!=''){
					var url = 'dynamic/parse-video-url';
					var data = {url:$('.Js_pt_videourl_text').val()};
					T.share_vodie = $('.Js_pt_videourl_text').val();
					if(T.share_vodie_ajax) T.share_vodie_ajax.abort();
					
					T.share_vodie_ajax = WG.fn.ajax(url,data,function(res){
						if(res.status == 'ok'){
							if(!res.content){
								$('.Js_show_video').html('没有找到该视频，请换一个地址').show();	
							}else{
								$('.Js_show_video').html(res.content).show();
							}
						}
					})
				}
			}
		},300)
	},
	showPostTrendBox: function(){
		var T = WG.PostTrend;
		var html = [
			'<div class="posttrend">',
			'   <div class="pt-title">',
			'       <span class="t fl">发布动态</span>',
			'       <a class="pt-close Js_pt_closebtn fr" href="javascript:" title="关闭">关闭</a>',
			'   </div>',
			'   <div class="pt-content">',
			'       <div class="texton">',
			'           <textarea name="" id="Js_posttrend_text" cols="30" rows="10" placeholder="请输入你要发布的内容"></textarea>',
			'       </div>',
			'       <div class="pt-bar">',
			'           <a class="abtn pt-pic Js_pt_uploadimg" href="javascript:" title="插入图片">插入图片</a>',
			'           <a class="abtn pt-video Js_pt_videourl" href="javascript:" title="插入视频">插入视频</a>',
			'       </div>',
			'       <div class="pt-video-url">',
			'           <input class="Js_pt_videourl_text video-url-text" type="text" placeholder="请输入你要发布视频的链接地址">',
			'       </div>',
			'		<div class="Js_show_video" style="width:574px; height: 363px; display:none; li"></div>',
			'		<div class="upload-files Js_cover">',
			'			<div class="uploadWrap-more-ccz dargArea left">',
			'				<span class="btn lightgrey hide Js_uploadbox_more">',
			'               <span class="btn lightgrey_l" title="上传">上传</span>',
			'				</span>',
			'				<input class="Js_file_uploadbox_more_ccz upload-input hide " type="file" multiple="multiple" accept="image/gif,image/jpeg,image/x-png,image/tiff" name="cover">',
			'       	</div>',
			'       	<div class="clearfix upload-more"></div>',
			'		</div>',
			'   </div>',
			'   <div class="pt-btnarea clearfix">',
			'       <span class="sblue-btn disable Js_pt_btn" title="发布动态">发布动态</span>',
			'   </div>',
			'</div>'].join("");

		$.fancybox({
			fitToView	: false,
			autoSize	: true,
			padding     : 0,
			closeBtn    : false,
			content     : html,
			afterClose  : T.close,
			afterShow   : T.run
		});
		return false
	},
	run: function (){
		var T = WG.PostTrend;
		$("#Js_posttrend_text").checkTextNumber({
			maxNum: 140, //可输入的最大字符
			numClassName: "ot", //显示字数的样式名称
			textClassName: "textnumbox ptrend" //绑定显示数字的元素，可以是jquery对象，$(document)
		});

		if($(".posttrend").length>0) T.setTex = setInterval(T.checkTextarea, 300);
		$("body").on('click','.Js_pt_btn', T.postTrend);
		if(!T.st){
			$("body").on("click", ".Js_pt_uploadimg", T.upLoadimg );
			$("body").on("click", ".Js_pt_videourl", T.videoUrl );
			$("body").on("click", ".Js_pt_closebtn", T.closeTrendBox );
			T.st = true;
		}
		moreUploadPath = 'dynamic/upload-image';
		WG.UploadMore.init({
			obj: ".uploadWrap-more-ccz, .Js_file_uploadbox_more_ccz",
			//多文件上传的回调函数
			callback: function(){
				$.fancybox.reposition()
			}
		});
	},
	close : function(){
		var T = WG.PostTrend;
		if(T.share_vodie_ajax) T.share_vodie_ajax.abort();
		T.share_vodie_ajax = null;
		T.share_vodie = '';
	},
	checkTextarea: function(){
		var $obj = $("#Js_posttrend_text");
		if($.trim($obj.val()) !== ""){
			$(".Js_pt_btn").removeClass('disable');
		}else{
			if($(".posttrend .updateSucImg").length>0){
				$(".Js_pt_btn").removeClass('disable');
				$(".Js_pt_uploadimg").addClass('on');
			}else{
				if($(".Js_pt_videourl_text").is(":visible") && $.trim($(".Js_pt_videourl_text").val()) !== ""){
					$(".Js_pt_btn").removeClass('disable');
				}else{
					$(".Js_pt_btn").addClass('disable');
					$(".Js_pt_uploadimg").removeClass('on');
				}
			}
		}
	},
	upLoadimg: function(){
		var $target = $(".Js_pt_uploadimg").parents('.pt-content').find(".Js_uploadbox_more");
		$target.click();
	},
	videoUrl: function(){
		if($(".pt-video-url").is(":hidden")){
			$(".pt-video-url").show();
			$(this).addClass('on')
		}else{
			$(".pt-video-url").hide();
			$(this).removeClass('on')
		}
	},
	closeTrendBox: function(){
		$.fancybox.close()
	}
};

/**
 * [LeftNav 左侧导航]
 */
WG.LeftNav = {
	init: function(){
		var T = WG.LeftNav;
		$(".Js_leftnav_small").on('click', T.changeNav);
		this.checkWindowWidth();
		$(window).on("resize",this.checkWindowWidth);
	},
	changeNav: function(){
		var $obj = $(".leftnav_wraper");
		$obj.removeClass('leftnav_small_wraper');
		$(this).parent().hide();
		$(".Js_leftnav_large").show()
	},
	checkWindowWidth: function(){
		var _W = $(window).width();
		if(_W <= 1540){
			$(".leftnav_wraper").addClass("leftnav_small_wraper");
			$(".leftnav_icon").show();
			$(".Js_leftnav_large").hide();

		}else{
			$(".leftnav_wraper").removeClass("leftnav_small_wraper");
			$(".leftnav_icon").hide();
			$(".Js_leftnav_large").show();
		}
	}
};

/**
 * [GoToTop 返回到顶部组件]
 */
WG.GoToTop = {
	init: function(){
		this.render();
	},
	opt: {
		topdistance: 200//到顶部的距离，达到此距离显示回到顶部按钮
	},
	render: function(){
		var gohtml = [
		'<div class="side_tools">',
			'<a id="go_top"></a>',
			'<a class="feedback" href='+baseUrl +'home/feedback?url='+globalCurrentPage+'></a>',
		'</div>'].join("");
		$('body').append(gohtml);
		if($(".side_tools").length > 0){
			var go_top = $("#go_top");
			if ($(window).scrollTop() < this.opt.topdistance) {
				go_top.hide();
			}
			$(window).scroll(this.scrollDom);
			go_top.on("click",this.scrollToTop)
		}
	},
	scrollDom: function(){
		var go_top = $("#go_top"),T = WG.GoToTop;
		if ($(window).scrollTop() < T.opt.topdistance) {
			go_top.fadeOut("fast");
		} else {
			go_top.fadeIn("fast");
		}
	},
	scrollToTop: function(){
		$("html,body").animate({
			scrollTop: 0
		}, 300);
		return false
	}
};

WG.userFollow = {
	init : function(){
		this.follow();
		this.followCreateCircle();
		this.followSaveCircle();
		this.followFancyboxClose();
		this.followShowGroup();
	},
	
	followAjax : null,
	memberInfoAjax : null,
	follow : function(){
		var _this = this;
		$('.Js_follow_btn').live('click',function(){
			var self = $(this);
			var user_id = self.attr('user-id');
			
			if(_this.followAjax) _this.followAjax.abort();
			_this.followAjax = WG.fn.ajax('circle/follow-user',{user_id:user_id},function(e){
				if(e.status == 'ok'){
					$.fancybox({
						fitToView	: false,
						autoSize	: true,
						padding     : 0,
						closeBtn    : false,
						afterShow : function(){
							_this.memberInfoAjax = WG.fn.ajax('circle/member-circle-info',{user_id:user_id},function(ev){
								if(ev.status == 'ok'){
									$('#Js_fancybox_follow_content').html(ev.content);
								}
							})
						},
						beforeClose : function(){
							if(_this.memberInfoAjax) _this.memberInfoAjax.abort();	
						},
						content : '<div id="Js_fancybox_follow_content"><div style="width:400px; height:281px; text-align:center; line-height:281px; border: 1px solid #CCC;background: #FFF;font-family: SimSun, serif;font-size: 12px;"><a style="position:absolute; top:7px; right:16px; width:20px; height:20px; background:url('+baseUrl+'images/icon_ptclosebtn.png) no-repeat 4px 4px" href="javascript:" title="关闭" class="Js_fancybox_follow_close"></a><img src="'+baseUrl+'images/fancybox_loading.gif" /></div></div>'
					});	
					self.text('已关注');
					self.addClass('Js_has_followed_btn').removeClass('Js_follow_btn');
				}	
			});
		})
	},
	
	followShowGroup : function() {
		var _this = this;
		$('.Js_has_followed_btn').live('click',function(){
			var self = $(this);
			var user_id = self.attr('user-id');
			$.fancybox({
				fitToView	: false,
				autoSize	: true,
				padding     : 0,
				closeBtn    : false,
				afterShow : function(){
					_this.memberInfoAjax = WG.fn.ajax('circle/member-circle-info',{user_id:user_id},function(ev){
						if(ev.status == 'ok'){
							$('#Js_fancybox_follow_content').html(ev.content);
						}
					})
				},
				beforeClose : function(){
					if(_this.memberInfoAjax) _this.memberInfoAjax.abort();	
				},
				content : '<div id="Js_fancybox_follow_content"><div style="width:400px; height:281px; text-align:center; line-height:281px; border: 1px solid #CCC;background: #FFF;font-family: SimSun, serif;font-size: 12px;"><a style="position:absolute; top:7px; right:16px; width:20px; height:20px; background:url('+baseUrl+'images/icon_ptclosebtn.png) no-repeat 4px 4px" href="javascript:" title="关闭" class="Js_fancybox_follow_close"></a><img src="'+baseUrl+'images/fancybox_loading.gif" /></div></div>'
			});	
		})
	},
	
	followCreateCircle : function(){
		$('.Js_follow_createCircle_btn').live('click',function(){
			$('.Js_follow_error_message_span').text('');
			var self = $(this);
			WG.fn.ajax('circle/create-circle',{name:$(this).prev().val()},function(e){
				if(e.status == 'ok'){
					var tpl = '<li class="fl" data-circle-id="'+e.content.id+'"><label><input type="checkbox" checked="checked">'+e.content.name+'</label></li>'	
					$('.Js_follow_circle_list').append(tpl);
					self.prev().val('');
				}else if(typeof(e.error.name) != 'undefined'){
					$('.Js_follow_error_message_span').text(e.error.name);
				}else{
					$('.Js_follow_error_message_span').text('圈子名称须小于20字符');
				}
			});	
		})
	},
	
	followSaveCircle : function(){
		$('.Js_fancybox_follow_save').live('click',function(){
			var chk = $('.Js_fancybox_follow_choose_group').find(':checkbox').filter(':checked');
			var arr = [];
			chk.each(function(){
				arr.push($(this).parents('li').attr('data-circle-id'));
			})
			
			WG.fn.ajax('circle/choose-group',{user_id:$(this).attr('user-id'),circle_ids:arr},function(e){
				if(e.status == 'ok'){
					$.fancybox.close();
				}
			});	
		})
	},
	
	followFancyboxClose : function(){
		$('.Js_fancybox_follow_close').live('click',function(){
			$.fancybox.close();	
		})
	}
}

WG.showUserCard = {
	init : function() {
		this.showCard();
	},
	userItem : null,
	showCardAjax : null,
	showCardTimer : null,
	showCard : function() {
		var self = this;
		var x,y;
		
		$('.Js_show_user_info_card').live('mouseenter',function() {
			var data_id = $(this).attr('data-user-id');
			$(this).mousemove(function(ev){
				x = ev.pageX + 5;
				y = ev.pageY + 10;
			})
			
			window.clearTimeout(self.showCardTimer);
			self.showCardTimer = null;
			self.userItem = $(this);
			$('.Js_show_card_box').remove();
			self.showCardAjax = WG.fn.ajax('user/user-card', {
				id : data_id,
			}, function(e) {
				if (e.status == 'ok') {
					var tpl = $(e.content);
					
					self.userItem.unbind('mousemove');
					
					$('body').append(tpl);
					
					var obj = {position : 'absolute',zIndex:1000};
					
					if(x+$('.Js_show_card_box').width() > $(window).width()){
						obj['right'] = 0;
					}else{
						obj['left'] = x;
					}
					
					if(y+$('.Js_show_card_box').height() > $(window).scrollTop() + $(window).height()){
						obj['top'] = $(window).scrollTop() + $(window).height() - $('.Js_show_card_box').outerHeight();
					}else{
						obj['top'] = y;
					}
					tpl.css(obj);
				}
			});
		}).live('mouseleave', function() {
			if (self.showCardAjax)
				self.showCardAjax.abort();
			if (!self.showCardTimer) {
				self.showCardTimer = window.setTimeout(function() {
					$('.Js_show_card_box').remove();
					self.showCardTimer = null;
					self.userItem = null;
				}, 133);
			}
		})
		
	 $('.Js_show_card_box').live('mouseenter',function(){
			window.clearTimeout(self.showCardTimer);
			self.showCardTimer = null;
		}).live('mouseleave',function(){
			if(!self.showCardTimer){
				self.showCardTimer = window.setTimeout(function(){
					$('.Js_show_card_box').remove();
					self.showCardTimer = null;
				},133);
			}
		})
	}
}

////多行文本溢出显示省略号(...)
//$(document).ready(function(){
////限制字符个数
//    $(".figCaption").each(function(){
//        var maxwidth=155;
//        if($(this).text().length>maxwidth){
//            $(this).text($(this).text().substring(0,maxwidth));
//            $(this).html($(this).html()+'…');
//        }
//    });
//});