WG.index = {
	init : function(){
		var self = this;
		self.posCCb();
		$('#Js_focusImg_ccz_btn').show();
		
		
		$('#Js_focusImg_ccz_btn').on('click',function(){
			$('.Js_search').click().focus();
			return false;
		})
		
		self.changeList();
		
		$(window).resize(function(){
			self.posCCb();
		})
		
		$('.Js_scroll').imgWheelShow({
			type: 'sola',
			focusBtnType: 'click',
			time: 5000,
			width: 'full'
		});
	},
	
	posCCb : function(){
		if($(window).width() <= 1180){
			$('#Js_focusImg_ccz_btn').css('left',855);
		}else{
			var n = $(window).width() > 1180 ? $(window).width() - 1180 : 1180;
			var l = (n / 2) + 855;
			$('#Js_focusImg_ccz_btn').css('left',l);	
		}
	},
	
	changeBtn : true,
	isEnterFlag : false,
	changeList : function(){
		var self = this;
		$('#Js_change_ssxw').on('click',click_fn).on('mouseenter',mouseenter_fn).on('mouseleave',mouseleave_fn);
		
		function click_fn(){
			var _this = $(this);
			if(self.changeBtn){
				WG.fn.ajax('home/news',{},function(e){
					if(e.status == 'ok'){
						$('.Js_news_list').html(e.content);
					}
					_this.removeClass('loading');
					_this.text('换一批');
					self.changeBtn = true;
					if(self.isEnterFlag){
						_this.addClass('hover');
					}
				});
				$(this).removeClass('hover');
				_this.addClass('loading');
				_this.text('');
				self.changeBtn = false;
			}
		}
		
		function mouseenter_fn(){
			if(self.changeBtn){
				$(this).addClass('hover');
				self.isEnterFlag = true;
			}
		}
		
		function mouseleave_fn(){
			$(this).removeClass('hover');
			self.isEnterFlag = false;
		}
	}
}