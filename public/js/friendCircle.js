WG.friendCircle = {
	init : function(){
		this.review();
		this.releaseReview();
		this.commentsClose();
		this.commentZan();
		this.commentForward();
		this.commentsPage();
	},
	
	review : function(){
		var reviewAjax = null;
		$('.Js_circle_review').live('click',fn);
		
		function fn(){
			
			var dom = $(this).parents('li').eq(0),
				target_id = dom.attr('target-id');
			
			if($.trim(dom.find('.Js_dynamic_comment_content').html())!="") return;
			if(reviewAjax) reviewAjax.abort();
			reviewAjax = WG.fn.ajax('dynamic/comments',{target_id:target_id},function(e){
				dom.find('.Js_dynamic_comment_content').html(e.content);
				dom.find('.Js_trend_comment').checkTextNumber({
					maxNum: 140, //可输入的最大字符
					numClassName: "ot", //显示字数的样式名称
					textClassName: "textnumbox", //绑定显示数字的元素，可以是jquery对象，$(document),
					btnObj: ".btn" //绑定按钮对象，可以是jquery对象，$(document)
				});
				reviewAjax = null;
			});
		}	
	},
	
	releaseReview : function(){
		$('.Js_release_review').live('click',fn);
		
		function fn(){
			var releaseAjax = null,
				self = $(this),
				content = self.parents(".dis-sendbox").find(".Js_trend_comment").val(),
				target_id = self.parents('li').attr('target-id'),
				is_forward = self.prev().find('input:checked').length;
			var err = self.parents(".dis-sendbox").find(".Js_num_error");
			if(!$.trim(content) || err.length > 0) return;
			if(releaseAjax) releaseAjax.abort();
			releaseAjax = WG.fn.ajax('dynamic/comment',{content:content,target_id:target_id,is_forward:is_forward},function(e){
				var imgSrc = e.content.user.avatar == '' ? baseUrl + 'images/default.png' : baseUrl + e.content.user.avatar;
				var tpl = '<li>'
				+'		<div class="list-item">'
				+'			<div class="item-t">'
				+'				<a href="'+baseUrl+'user/home/'+e.content.user.id+'"><img class="avatar" src="'+imgSrc+'" alt="'+e.content.user.name+'"></a> <span class="t"><a href="'+baseUrl+'user/home/'+e.content.user.id+'">'+e.content.user.name+'</a></span>'
				+'				<div class="level">'
				+'					<div class="icon ' + e.content.level + '"></div>'
				+'					<div class="bar">'
				+'						<div class="process" style="width: 60%;"></div>'
				+'					</div>'
				+'				</div>'
				+'			</div>'
				+'			<div class="item-con">'+WG.fn.htmlspecialchars(e.content.content)+'</div>'
				+'			<span class="date">'+e.content.create_time+'</span>'
				+'		</div>'
				+'	</li>'
				if(e.status == 'ok'){
					self.parents(".dis-sendbox").find(".Js_trend_comment").val("").next().find('b').text('140');
					var domItem = self.parents('.Js_dynamic_comment_content').prev().find('.Js_commentNuum');
					domItem.text(parseInt(domItem.text(),10)+1);
					if(self.parents('li').find('.Js_no_comments').length > 0){
						self.parents('li').find('.Js_user_comments').html(tpl);
					}else{
						self.parents('li').find('.Js_user_comments').prepend(tpl);
					}
				}
			})
			
		}
	},
	
	commentsClose : function(){
		$('.Js_reviewClose_btn').live('click',function(){
			$(this).parents('.Js_dynamic_comment_content').html('');	
		})	
	},
	
	commentZan : function(){
		$('.Js_commentZan').live('click',fn);
		function fn(){
			var self = $(this);
				dom = self.parents('li').eq(0),
				target_id = dom.attr('target-id');
			WG.fn.ajax('dynamic/like',{target_id:target_id},function(e){
				if(e.status == 'ok'){
					var o = self.find('.Js_commentZanNum');
					o.text(parseInt(o.text(),10)+1);
					self.css('background-position','0 -66px');
				}	
			});	
		}	
	},
	
	commentsPage : function(){
		$('.Js_dynamci_comment_page').live('click',function(){
			var dom = $(this).parents('li'),
			target_id = dom.attr('target-id');
			page = $(this).attr('data-page');
			 WG.fn.ajax('dynamic/comments',{target_id:target_id,page:page, type:true},function(e){
				 if(e.status == 'ok'){
					dom.find('.Js_user_comments').html(e.content);
				 }
			});
		})
	},
	
	commentForward : function(){
		var st,setTex;
		$('.Js_forward').live('click',fn);
		$('.Js_close_forward').live('click',f_close);
		$('.Js_forward_btn').live('click',send);
		
		function fn(){
			if(globalUserInfo.id == 0) {
				window.location.href = baseUrl+'user/login';
				return;
			}
			var html = [
			'<div class="posttrend Js_forword_fancybox" data-id="'+$(this).attr('data-forward-id')+'">',
			'   <div class="pt-title">',
			'       <span class="t fl">转发</span>',
			'       <a class="pt-close Js_close_forward fr" href="javascript:">关闭</a>',
			'   </div>',
			'   <div class="pt-content">',
			'       <div class="texton">',
			'           <textarea name="" id="Js_forward_text" cols="30" rows="10" placeholder="请输入转发评论"></textarea>',
			'       </div>',
			'   </div>',
			'   <div class="pt-btnarea clearfix">',
			'       <span class="sblue-btn disable Js_forward_btn">发布动态</span>',
			'   </div>',
			'</div>'].join("");
			
			$.fancybox({
				fitToView	: false,
				autoSize	: true,
				padding     : 0,
				closeBtn    : false,
				content     : html,
				afterShow   : showRun
			});	
		}
		
		function f_close(){
			$.fancybox.close();	
		}
		
		function send(){
			var err = $(this).parents(".Js_forword_fancybox").find(".Js_num_error");
			if(err.length>0){
				alert('输入字数超出限制范围');
				return;	
			}
			if(!$('#Js_forward_text').val()) return;
			
			if(!$(this).hasClass('disable')){
				WG.fn.ajax('dynamic/forward',{forward_id:$('.Js_forword_fancybox').attr('data-id'),content:$('#Js_forward_text').val()},function(e){
					$.fancybox.close();
					if(forwordThisPage){
						if(e.status == 'ok'){
							$('.Js_dis_list_content').prepend(e.content);	
						}
					}
				})
			}
		}
		
		function showRun(){
			$("#Js_forward_text").checkTextNumber({
				maxNum: 140, //可输入的最大字符
				numClassName: "ot", //显示字数的样式名称
				textClassName: "textnumbox ptrend" //绑定显示数字的元素，可以是jquery对象，$(document)
			});
	
			if($(".Js_forword_fancybox").length>0) setTex = setInterval(checkTextarea, 300);
			if(!st){
				st = true;
			}
		}
		
		function checkTextarea(){
			var $obj = $("#Js_forward_text");
			if($.trim($obj.val()) !== ""){
				var err = $('.Js_forward_btn').parents(".Js_forword_fancybox").find(".Js_num_error");
				if(err.length>0){
					$(".Js_forward_btn").addClass('disable').removeAttr('style');
				}else{
					$(".Js_forward_btn").removeClass('disable').css('cursor','pointer');
				}
			}else{
				$(".Js_forward_btn").addClass('disable').removeAttr('style');
			}
		}
	}
}