WG.frameSingle = {
	init : function(){
		this.eventSingle();
	},
	
	eventSingle : function(){
		$('.Js_upload_input_c').live('change',fn);
		function fn(){
			var self = $(this);
			var id = self.attr('id');
			var frame_id = self.parent().nextAll().filter('iframe').attr('id');
			var img = $('#'+frame_id).contents().find('#img');
			var showImg = $('#'+frame_id).contents().find('#img').find('img');
			var mask = $('#'+frame_id).contents().find('#Js_frame_mask');
			var content = '';
			
			
			$('#'+frame_id).show();
			mask.show();
			showImg.eq(0).show();
			if(!showImg.eq(1).attr('src')){
				showImg.eq(1).hide();
			}
			showImg.eq(1).unbind('load').bind('load',function(){
				mask.hide();
				$(this).prev().hide();
				$(this).show();
				img.find('input').val(content);
			})
			
			img.find('a').off('click').on('click',function(){
				img.find('input').val('');
				showImg.eq(1).attr('src','').hide();
				mask.show();
				showImg.eq(0).show();
				$('#'+frame_id).hide();
			})
			$.ajaxFileUpload({
				url : baseUrl + uploadPath,
				secureuri :false,
				fileElementId  : id,
				dataType : 'json',
				success : function(data){
					if(data.status == 'ok'){
						showImg.eq(1).attr('src',WG.baseUrl + data.content);
						content = data.content;
					}else if(data.status==1){
						if(data.error=='error_image_too_small'){
							alert('文件尺寸太小，请重新上传！');	
						}else if(data.error=='error_not_support_image_format'){
							alert('文件格式不支持，请重新上传！');
						}else if(data.error=='error_invalid_image_format'){
							alert('文件大小不符，请重新上传！');
						}
						if(showImg.eq(1).attr('src')){
							mask.hide();
							showImg.eq(0).hide();
							showImg.eq(1).show();
						}else{
							img.find('input').val('');
							showImg.eq(1).attr('src','').hide();
							mask.show();
							showImg.eq(0).show();
							$('#'+frame_id).hide();
						}
					}
				}
			})
		}
	}
}