<div class="map-img-parent">
				<div class="map-img" id="Js_map_city">
					<img src="../../images/map/hainan.png" />
					<a href="./3001" class="hainan_haikou" data-id="3001">海口</a>
					<a href="./3002" class="hainan_sanya" data-id="3002">三亚</a>
					<a href="./3003" class="hainan_wuzhishan" data-id="3003">五指山</a>
					<a href="./3004" class="hainan_qionghai" data-id="3004">琼海</a>
					<a href="./3005" class="hainan_danzhou" data-id="3005">儋州</a>
					<a href="./3006" class="hainan_wenchang" data-id="3006">文昌</a>
					<a href="./3007" class="hainan_wanning" data-id="3007">万宁</a>
					<a href="./3008" class="hainan_dongfang" data-id="3008">东方</a>
					<a href="./3009" class="hainan_dingan" data-id="3009">定安</a>
					<a href="./3010" class="hainan_tunchang" data-id="3010">屯昌</a>
					<a href="./3011" class="hainan_chengmai" data-id="3011">澄迈</a>
					<a href="./3012" class="hainan_lingao" data-id="3012">临高</a>
					<a href="./3013" class="hainan_baisha" data-id="3013">白沙</a>
					<a href="./3014" class="hainan_changjiang" data-id="3014">昌江</a>
					<a href="./3015" class="hainan_ledong" data-id="3015">乐东</a>
					<a href="./3016" class="hainan_lingshui" data-id="3016">陵水</a>
					<a href="./3017" class="hainan_baoting" data-id="3017">保亭</a>
					<a href="./3018" class="hainan_qiongzhong" data-id="3018">琼中</a>
				</div>
			</div>