<div class="map-img-parent">
				<div class="map-img" id="Js_map_city">
					<img src="../../images/map/hubei.png" />
					<a href="./2601" class="hubei_wuhan" data-id="2601">武汉</a>
					<a href="./2602" class="hubei_huangshi" data-id="2602">黄石</a>
					<a href="./2603" class="hubei_shiyan" data-id="2603">十堰</a>
					<a href="./2604" class="hubei_yichang" data-id="2604">宜昌</a>
					<a href="./2605" class="hubei_xiangyang" data-id="2605">襄阳</a>
					<a href="./2606" class="hubei_ezhou" data-id="2606">鄂州</a>
					<a href="./2607" class="hubei_jingmen" data-id="2607">荆门</a>
					<a href="./2608" class="hubei_xiaogan" data-id="2608">孝感</a>
					<a href="./2609" class="hubei_jingzhou" data-id="2609">荆州</a>
					<a href="./2610" class="hubei_huanggang" data-id="2610">黄冈</a>
					<a href="./2611" class="hubei_xianning" data-id="2611">咸宁</a>
					<a href="./2612" class="hubei_suizhou" data-id="2612">随州</a>
					<a href="./2613" class="hubei_enshi" data-id="2613">恩施</a>
					<a href="./2614" class="hubei_xiantao" data-id="2614">仙桃</a>
					<a href="./2615" class="hubei_qianjiang" data-id="2615">潜江</a>
					<a href="./2616" class="hubei_tianmen" data-id="2616">天门</a>
					<a href="./2617" class="hubei_shennongjialinqu" data-id="2617">神农架林区</a>
				</div>
			</div>