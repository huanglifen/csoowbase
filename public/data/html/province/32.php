<div class="map-img-parent">
	<div class="map-img" id="Js_map_city">
		<img src="../../images/map/sichuan.png" />
		<a href="./3201" class="sichuan_chengdu" data-id="3201">成都</a>
		<a href="./3202" class="sichuan_zigong" data-id="3202">自贡</a>
		<a href="./3203" class="sichuan_panzhihua" data-id="3203">攀枝花</a>
		<a href="./3204" class="sichuan_luzhou" data-id="3204">泸州</a>
		<a href="./3205" class="sichuan_deyang" data-id="3205">德阳</a>
		<a href="./3206" class="sichuan_mianyang" data-id="3206">绵阳</a>
		<a href="./3207" class="sichuan_guangyuan" data-id="3207">广元</a>
		<a href="./3208" class="sichuan_suining" data-id="3208">遂宁</a>
		<a href="./3209" class="sichuan_neijiang" data-id="3209">内江</a>
		<a href="./3210" class="sichuan_leshan" data-id="3210">乐山</a>
		<a href="./3211" class="sichuan_nanchong" data-id="3211">南充</a>
		<a href="./3212" class="sichuan_meishan" data-id="3212">眉山</a>
		<a href="./3213" class="sichuan_yibin" data-id="3213">宜宾</a>
		<a href="./3214" class="sichuan_guangan" data-id="3214">广安</a>
		<a href="./3215" class="sichuan_dazhou" data-id="3215">达州</a>
		<a href="./3216" class="sichuan_yaan" data-id="3216">雅安</a>
		<a href="./3217" class="sichuan_bazhong" data-id="3217">巴中</a>
		<a href="./3218" class="sichuan_ziyang" data-id="3218">资阳</a>
		<a href="./3219" class="sichuan_aba" data-id="3219">阿坝</a>
		<a href="./3220" class="sichuan_ganzhi" data-id="3220">甘孜</a>
		<a href="./3221" class="sichuan_liangshan" data-id="3221">凉山</a>
	</div>
</div>