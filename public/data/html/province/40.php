<div class="map-img-parent">
	<div class="map-img" id="Js_map_city">
		<img src="../../images/map/xinjiang.png" />
		<a href="./4001" class="xinjiang_wulumuqi" data-id="4001">乌鲁木齐</a>
		<a href="./4002" class="xinjiang_kelamayi" data-id="4002">克拉玛依</a>
		<a href="./4003" class="xinjiang_tulufan" data-id="4003">吐鲁番</a>
		<a href="./4004" class="xinjiang_hami" data-id="4004">哈密</a>
		<a href="./4005" class="xinjiang_changji" data-id="4005">昌吉</a>
		<a href="./4006" class="xinjiang_boertala" data-id="4006">博尔塔拉</a>
		<a href="./4007" class="xinjiang_bayinguoleng" data-id="4007">巴音郭楞</a>
		<a href="./4008" class="xinjiang_akesu" data-id="4008">阿克苏</a>
		<a href="./4009" class="xinjiang_kezilesukeerkezi" data-id="4009">克孜勒苏柯尔克孜</a>
		<a href="./4010" class="xinjiang_kashi" data-id="4010">喀什</a>
		<a href="./4011" class="xinjiang_hetian" data-id="4011">和田</a>
		<a href="./4012" class="xinjiang_yili" data-id="4012">伊犁</a>
		<a href="./4013" class="xinjiang_tacheng" data-id="4013">塔城</a>
		<a href="./4014" class="xinjiang_aletai" data-id="4014">阿勒泰</a>
		<a href="./4015" class="xinjiang_shihezi" data-id="4015">石河子</a>
		<a href="./4015" class="xinjiang_alaer" data-id="4016">阿拉尔</a>
		<a href="./4015" class="xinjiang_tumushuke" data-id="4017">图木舒克</a>
		<a href="./4015" class="xinjiang_wujiaqu" data-id="4018">五家渠</a>
	</div>
</div>