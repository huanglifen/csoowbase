<div class="map-img-parent">
				<div class="map-img" id="Js_map_city">
					<img src="../../images/map/yunnan.png" />
					<a href="./3401" class="yunnan_kunming" data-id="3401">昆明</a>
					<a href="./3402" class="yunnan_qujing" data-id="3402">曲靖</a>
					<a href="./3403" class="yunnan_yuxi" data-id="3403">玉溪</a>
					<a href="./3404" class="yunnan_baoshan" data-id="3404">保山</a>
					<a href="./3405" class="yunnan_zhaotong" data-id="3405">昭通</a>
					<a href="./3406" class="yunnan_lijiang" data-id="3406">丽江</a>
					<a href="./3407" class="yunnan_puer" data-id="3407">普洱</a>
					<a href="./3408" class="yunnan_lincang" data-id="3408">临沧</a>
					<a href="./3409" class="yunnan_chuxiong" data-id="3409">楚雄</a>
					<a href="./3410" class="yunnan_honghe" data-id="3410">红河</a>
					<a href="./3411" class="yunnan_wenshan" data-id="3411">文山</a>
					<a href="./3412" class="yunnan_xishuangbanna" data-id="3412">西双版纳</a>
					<a href="./3413" class="yunnan_dali" data-id="3413">大理</a>
					<a href="./3414" class="yunnan_dehong" data-id="3414">德宏</a>
					<a href="./3415" class="yunnan_nujiang" data-id="3415">怒江</a>
					<a href="./3416" class="yunnan_diqing" data-id="3416">迪庆</a>
				</div>
			</div>